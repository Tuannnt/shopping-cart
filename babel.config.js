module.exports = {
  presets: ["module:@react-native/babel-preset"],
  plugins: [
      [
          "module-resolver",
          {
              root: ["."], // <- this is the same as the baseUrl
              extensions: [
                  ".ios.ts",
                  ".ios.tsx",
                  ".android.ts",
                  ".android.tsx",
                  ".ts",
                  ".tsx",
                  ".png"
              ],
              alias: {
                  "@": "./src/", // <- this is absolute (different from tsconfig)
                  '@theme': './src/theme',
                  '@component': './src/component',
                  '@redux': './src/redux',
                  '@utils': './src/utils',
                  '@bussiness': './src/bussiness',
                  '@network': './src/network',
                  '@error': './src/error',
                  '@i18n': './src/i18n',
                  '@images': './src/images',
                  '@navigation': './src/navigation',
                  '@screen': './src/screen',
                  '@Home': './src/screen/main',
                  '@Component': './src/screen/component',
                  '@Component': './src/screen/component',
                  '@config': './src/config',
              },
          },
      ],
      'react-native-reanimated/plugin',
  ],
};
