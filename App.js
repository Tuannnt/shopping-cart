/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { View, BackHandler, SafeAreaView , Text} from 'react-native';
import { AppStyles } from '@theme';
import Toast from 'react-native-simple-toast';
import { connect, Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from './src/redux';
import { Router } from 'react-native-router-flux';
import thunk from 'redux-thunk';
const RouterWithRedux = connect()(Router);
let middlewares = [
  thunk, // Allows action creators to return functions (not just plain objects)
];
if (__DEV__) {
  // Dev-only middleware
  const { logger } = require(`redux-logger`);
  middlewares.push(logger);
}
const store = createStore(rootReducer)
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backClickCount: 0,
    };
  }
  _spring() {
    Toast.showWithGravity(
      'Nhấn lần nữa để thoát ứng dụng!',
      Toast.SHORT,
      Toast.CENTER,
    );
  }
  handleBackButton = () => {
    if (this.state.backClickCount === 1) {
      BackHandler.exitApp();
    } else {
      this.setState({ backClickCount: 1 }, () => {
        this._spring();
      });
    }
    return true;
  };
  render() {
    return (
      <View style={[AppStyles.appContainer]}>
        <Provider store={store}></Provider>
        <View style={{ flexDirection: 'row', flex: 0 }}>

          <Text>Hello world 2</Text>
          <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }} />
        </View>
      </View>
    );
  }
  componentDidMount() {
    // Register the alert located on this master page
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  componentWillUnmount() {
    // Remove the alert located on this master page from the manager
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
}
