/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { View, BackHandler, SafeAreaView } from 'react-native';
import { applyMiddleware, compose, createStore } from 'redux';
import { connect, Provider } from 'react-redux';
import { Router } from 'react-native-router-flux';
import thunk from 'redux-thunk';

import rootReducer from './src/redux';
import AppRoutes from './src/navigation';
import config from './src/config';
import { AppStyles } from '@theme';
import { Progress, ProgressManager } from '@component';
import Toast from 'react-native-simple-toast';
const RouterWithRedux = connect()(Router);
let middlewares = [
  thunk, // Allows action creators to return functions (not just plain objects)
];
if (__DEV__) {
  // Dev-only middleware
  const { logger } = require(`redux-logger`);
  middlewares.push(logger);
}
// Init redux store (using the given reducer & middleware)
const store = compose(applyMiddleware(...middlewares))(createStore)(
  rootReducer,
);
type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      backClickCount: 0,
    };
  }
  _spring() {
    Toast.showWithGravity(
      'Nhấn lần nữa để thoát ứng dụng!',
      Toast.SHORT,
      Toast.CENTER,
    );
  }
  handleBackButton = () => {
    if (this.state.backClickCount === 1) {
      BackHandler.exitApp();
    } else {
      this.setState({ backClickCount: 1 }, () => {
        this._spring();
      });
    }
    return true;
  };
  render() {
    return (
      <View style={[AppStyles.appContainer]}>
        <View style={{ flex: 1 }}>
          <Provider store={store}>
            <RouterWithRedux scenes={AppRoutes} style={AppStyles.appContainer} />
          </Provider>
          <Progress ref="progress" />
        </View>
        <View style={{ flexDirection: 'row', flex: 0 }}>
          <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }} />
        </View>
      </View>
    );
  }
  componentDidMount() {
    // Register the alert located on this master page
    ProgressManager.register(this.refs.progress);
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  componentWillUnmount() {
    // Remove the alert located on this master page from the manager
    ProgressManager.unregister();
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
}
