export function format(s: string, ...args) {
  let content = s;
  for (let i = 0; i < args.length; i++) {
    const replacement = '{' + i + '}';
    content = content.replace(replacement, args[i]);
  }
  return content;
}
