import { API } from '@network';
import React, { Component } from 'react';
import { View, Text, Button, Alert, Platform, Keyboard } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import OneSignal from 'react-native-onesignal';
import configApp from '../configApp';
import signalr from 'react-native-signalr';
import Toast from 'react-native-simple-toast';

export function RouterLink(u, o) {
  if (u !== undefined && u !== null) {
    if (u.split('?').length > 1) {
      var _l = u.split('?')[1].split('&');
      var _p = {};
      for (var i = 0; i < _l.length; i++) {
        _p[_l[i].split('=')[0].toUpperCase()] = _l[i].split('=')[1];
      }
      //check module tren mobile khong ho tro
      if (!_p.MODULEID || _p.MODULEID === 'null') {
        Toast.showWithGravity(
          'Chức năng này không hỗ trợ',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
      //
      o[_p.MODULEID]({ RecordGuid: _p.RECORDGUID });
    }
  }
}
export function Data_Offline(c) {
  NetInfo.fetch().then(s => {
    // console.log("Connection type", s.type);
    // console.log("Is connected?", s.isConnected);
    c(s.isConnected);
    return s.isConnected;
  });
}
export const Data_Offline_Set = async (key, data) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(data));
  } catch (ex) {
    await AsyncStorage.setItem(key, {});
  }
};
export const Data_Offline_Get = async key => {
  var _d = await AsyncStorage.getItem(key);
  return JSON.parse(_d);
};
export class SignalRCommon {
  SIGNALR_object = {
    SIGNALR: {},
    REVICE_MESS: {},
    USER: {},
    IS_AUTHEN: false,
    IS_SIGNALR: false,
    reviceMess: () => { },
  };
  init(c) {
    OneSignal.init(configApp.onesignal_guid);
    if (this.SIGNALR_object) {
      if (
        !this.SIGNALR_object.SIGNALR ||
        !this.SIGNALR_object.SIGNALR.connection ||
        this.SIGNALR_object.SIGNALR.connection.state != 1
      ) {
        var connection = signalr.hubConnection(configApp.url_signalr_mb);
        connection.logging = true;
        this.SIGNALR_object.SIGNALR = connection.createHubProxy('ChatHub');
        this.SIGNALR_object.SIGNALR.on(
          'ReceiveMessChat',
          (LoginName, Comment) => {
            this.reviceMess(LoginName, Comment);
          },
        );
        connection
          .start()
          .done(() => {
            console.log('Start connect signalr success');
            this.SIGNALR_object.IS_SIGNALR = true;
            this.initName(c);
          })
          .fail(() => {
            console.log('Start connect signalr fail');
          });
        connection.connectionSlow(function (e) {
          console.log('Connect to signalr slow');
        });
        connection.disconnected(function (e) {
          console.log('Lost connect to signalr');
          this.init();
        });
        connection.error(function (e) {
          console.log('Connect to signalr error');
          this.init();
        });
      }
    } else {
      setTimeout(() => {
        console.log('Start reconnect to server');
        this.init();
      }, 5000);
    }
  }
  checkInternet(c) {
    if (
      !this.SIGNALR_object.SIGNALR ||
      !this.SIGNALR_object.SIGNALR.connection ||
      this.SIGNALR_object.SIGNALR.connection.state != 1
    ) {
      Toast.showWithGravity(
        'Mất kết nối đến máy chủ. thử lại sau ít phút',
        Toast.LONG,
        Toast.CENTER,
      );
      this.init();
    } else {
      NetInfo.fetch().then(d => {
        if (d.isConnected == true) {
          if (c) {
            this._sendMess(c.d);
          }
        } else {
          this.init(c);
        }
      });
    }
  }
  initName(c) {
    var _obj = {
      LoginName: this.SIGNALR_object.USER.LoginName,
      OrganizationGuid: this.SIGNALR_object.USER.OrganizationGuid,
      Device: 'MOBILE',
    };
    this.SIGNALR_object.SIGNALR.invoke('ping', _obj);
    if (c) {
      this._sendMess(c.d);
    }
  }
  addReviceMess = d => {
    this.SIGNALR_object.reviceMess = d;
  };
  reviceMess(l, d) {
    try {
      this.SIGNALR_object.reviceMess(l, d);
      if (typeof this.SIGNALR_object.REVICE_MESS === 'function') {
        this.SIGNALR_object.REVICE_MESS(l, d);
      }
    } catch (error) {
      console.log(error);
    }
    try {
      //trongtq
      if (d !== undefined) {
        var CommentString = JSON.parse(d);
        if (CommentString.Code == 'NOTI_MOBILE') {
          var Message = CommentString.Message;
          if (CommentString.Type === 'I') {
            Message = FuncCommon.customMessByType(CommentString);
          }
          showMessage({
            message: CommentString.FullName,
            description: Message,
            autoHide: false,
            backgroundColor: AppColors.Maincolor,
            style: { borderRadius: 15, margin: 10 },
            onPress: () =>
              Actions.viewItemMessange({
                RecordGuid: CommentString.ChatboxGroupGuid,
              }),
          });
        }
      }
      // end
    } catch (error) {
      console.log(error);
    }
  }
  sendMess(d) {
    this.checkInternet({ d: d });
  }
  _sendMess(d) {
    this.SIGNALR_object.SIGNALR.invoke('sendMessChat', d);
  }
  ping(d) {
    this.SIGNALR_object.SIGNALR.invoke('ping', d);
  }
}
//0 11/11/2017
//1 11/11/2017 22:22
//2 123123123
//3 type date
//4 type data year month day
//5 20201003
//6 17:02:50
export function ConDate(_data, number, type) {
  let data = _data;
  if (type === 'iso') {
    data = `${_data}+07:00`;
    // convert iso time from web to vi-time
  }
  if (data == null || data == '') {
    return '';
  }
  if (data !== null && data != '' && data != undefined) {
    try {
      if (data.indexOf('SA') != -1 || data.indexOf('CH') != -1) {
        if (data.indexOf('SA') != -1) {
        }
        if (data.indexOf('CH') != -1) {
        }
      }

      if (data.indexOf('Date') != -1) {
        data = data.substring(data.indexOf('Date') + 5);
        data = parseInt(data);
      }
    } catch (ex) { }
    var newdate = new Date(data);
    if (number == 3) {
      return newdate;
    }
    if (number == 2) {
      return '/Date(' + newdate.getTime() + ')/';
    }
    var month = newdate.getMonth() + 1;
    var day = newdate.getDate();
    var year = newdate.getFullYear();
    var hh = newdate.getHours();
    var mm = newdate.getMinutes();
    let _mm = newdate.getMinutes();
    var ss = newdate.getSeconds();
    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if (number == 0) {
      return (todayDate = day + '/' + month + '/' + year);
    }
    if (number == 1) {
      return (todayDate = day + '/' + month + '/' + year + ' ' + hh + ':' + mm);
    }
    if (number == 4) {
      return new Date(year, month - 1, day);
    }
    if (number == 5) {
      return year * 10000 + parseInt(month) * 100 + parseInt(day);
    }
    if (number == 6) {
      return hh + ':' + mm + ':' + ss;
    }
    if (number == 7) {
      return hh + ':' + mm;
    }
    //convert hh:mm:ss to type Date time
    if (number === 8) {
      let d = new Date();
      let [hours, minutes, seconds] = data.split(':');
      d.setHours(+hours);
      d.setMinutes(minutes);
      d.setSeconds(seconds);
      return d;
    }
    if (number === 9) {
      return '' + year + month + day;
    }
    if (number == 10) {
      return (todayDate =
        day +
        '/' +
        month +
        '/' +
        year +
        ' ' +
        `${hh < 10 ? '0' + hh : hh}:${mm < 10 ? '0' + mm : mm}:${ss < 10 ? '0' + ss : ss
        }`);
    }
    if (number == 11) {
      return (todayDate = year + '-' + month + '-' + day + ' ' + hh + ':' + mm);
    }
    if (number == 12) {
      // change YYYYMMDD to js date normal
      var year12 = data.substring(0, 4);
      var month12 = data.substring(4, 6);
      var day12 = data.substring(6, 8);
      return new Date(year12, month12 - 1, day12);
    }
    if (number == 13) {
      return `${newdate
        .getDate()
        .toString()
        .padStart(2, '0')}/${(newdate.getMonth() + 1)
          .toString()
          .padStart(2, '0')}/${newdate
            .getFullYear()
            .toString()
            .padStart(4, '0')} ${newdate
              .getHours()
              .toString()
              .padStart(2, '0')}:${newdate
                .getMinutes()
                .toString()
                .padStart(2, '0')}:${newdate
                  .getSeconds()
                  .toString()
                  .padStart(2, '0')}`;
    }
    if (number == 15) {
      // helper change iso time to vi timezone
      return '' + `${year}-${month}-${day}`;
    }
    if (number == 16) {
      // helper change iso time to vi timezone
      return '' + `${year}${month}${day}`;
    }
    if (number == 75) {
      return (todayDate =
        day +
        '/' +
        month +
        '/' +
        year +
        ' ' +
        `${hh < 10 ? '0' + hh : hh}:${_mm < 10 ? '0' + _mm : _mm}:${ss < 10 ? '0' + ss : ss
        }`);
    }
    if (number == 76) {
      console.log(
        `${+hh < 10 ? '0' + hh : hh}:${+_mm < 10 ? '0' + _mm : _mm}:${+ss < 10 ? '0' + ss : ss
        }`,
      );
      return `${+hh < 10 ? '0' + hh : hh}:${+_mm < 10 ? '0' + _mm : _mm}:${+ss < 10 ? '0' + ss : ss
        }`;
    }
    if (number == 77) {
      return (todayDate = year + month + day);
    }
    // convert h:mm:s to date time js normal
    if (number === 88) {
      if (!data.includes(':')) {
        return null;
      }
      let _data = data.split(':');
      if (+_data[0] < 10) {
        _data[0] = '0' + _data[0];
      }
      if (+_data[1] < 10) {
        _data[1] = '0' + _data[1];
      }
      if (+_data[2] < 10) {
        _data[2] = '0' + _data[2];
      }
      return _data.join(':');
    }

    // convert dd/mm/yyyy to date time js normal
    if (number === 99) {
      let arr = data.split('/');
      return new Date(+arr[2], arr[1] - 1, +arr[0]);
    }
  } else {
    return '';
  }
}
//Split string from length input
export function ConMiniTitle(_title, _length) {
  if (_title === null || _title === undefined) {
    _title = '';
  }
  if (_title !== '' && _title.length > _length) {
    _title = _title.substring(0, _length) + '...';
  }
  return _title;
}
export function DataProcess() {
  return [
    {
      value: 'N',
      name: 'Chờ xử lý',
      class: 'notstart',
      classbefore: 'before-notstart',
      classProcess: 'notstart',
      classDrop: 'iconfont-users dropdown-item__icon',
      classSearch: 'mdi mdi-buffer',
      data: [],
      show: true,
      kanban: true,
    },
    {
      value: 'P',
      name: 'Đang xử lý',
      class: 'process',
      classbefore: 'before-process',
      classProcess: 'process',
      classDrop: 'iconfont-user-circle dropdown-item__icon',
      classSearch: 'mdi mdi-check-all',
      data: [],
      show: true,
      kanban: true,
    },
    {
      value: 'C',
      name: 'Hoàn thành',
      class: 'complete',
      classbefore: 'before-complete',
      classProcess: 'complete',
      classDrop: 'iconfont-building-b dropdown-item__icon',
      classSearch: 'mdi mdi-briefcase-check',
      data: [],
      show: true,
      kanban: true,
    },
    {
      value: 'W',
      name: 'Chờ người khác',
      class: 'waitsomeone',
      classbefore: 'before-waitsomeone',
      classProcess: 'waitsomeone',
      classDrop: 'iconfont-building-b dropdown-item__icon',
      classSearch: 'mdi mdi-account-check',
      data: [],
      show: true,
      kanban: true,
    },
    {
      value: 'D',
      name: 'Hoãn lại',
      class: 'deferred',
      classbefore: 'before-deferred',
      classProcess: 'deferred',
      classDrop: 'iconfont-deal dropdown-item__icon',
      classSearch: 'mdi mdi-flag',
      data: [],
      show: true,
      kanban: true,
    },
  ];
}

//#region Trongtq@esvn.com.vn

//API User login Information
export function UserInformation() {
  API.getProfile()
    .then(rs => {
      return rs.data;
    })
    .catch(error => {
      console.log('error:', error);
      Alert.alert('Thông báo', 'Có lỗi khi lấy thông tin người đăng nhập');
    });
}
//#endregion

export const getDiffTime = (beginTime, endTime) => {
  let res = '';
  res = moment.duration(moment(endTime).diff(moment(beginTime))).asHours();
  if (parseInt(res) < 0) {
    res = 24 + res;
  }
  return res;
};
export const getDiffDay = (beginTime, endTime) => {
  let res = '';
  res = moment.duration(moment(endTime).diff(moment(beginTime))).asDays();
  return Number(Math.abs(res)) + 1;
};
export const getDiffDayCompare = (beginTime, endTime) => {
  let res = moment.duration(moment(endTime).diff(moment(beginTime))).asDays();
  return res;
};
export const handleLongText = (text, maxlimit) => {
  if (!text) {
    return '';
  }
  if (!maxlimit) {
    return text;
  }
  return (text || '').length > maxlimit
    ? text.substring(0, maxlimit - 3) + '...'
    : text;
};
export const addPeriod = nStr => {
  if (nStr !== null && nStr !== '' && nStr !== undefined) {
    nStr = Math.round(parseFloat(nStr) * 100) / 100;
    nStr += '';
    if (nStr.indexOf('.') >= 0) {
      var x = nStr.split('.');
    } else {
      var x = nStr.split(',');
    }
    var x1 = x[0];
    var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    var Total = x1 + x2;
    return Total;
  } else {
    return 0;
  }
};

export const getListTime = () => {
  let DropdownMonth = [];
  let DropdownYear = [];
  for (var i = 1; i <= 12; i++) {
    DropdownMonth.push({ value: i, text: i });
  }
  for (var i = 2020; i <= 2040; i++) {
    DropdownYear.push({ value: i, text: i });
  }
  return {
    DropdownMonth,
    DropdownYear,
  };
};
export const addPeriodTextInput = value => {
  if (!value) {
    return;
  }
  let res = value
    .split('.')
    .join('')
    .replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  return res;
};
export const ConvertDateSearch = type => {
  var newdate = new Date();
  var month = newdate.getMonth() + 1;
  var monthBefore = newdate.getMonth();
  var year = newdate.getFullYear();
  var day = newdate.getDate();
  if (month < 10) {
    month = '0' + month;
  }
  if (monthBefore < 10) {
    monthBefore = '0' + monthBefore;
  }
  if (day < 10) {
    day = '0' + day;
  }
  if (type === 1) {
    return year + '-' + monthBefore + '-' + day;
  } else {
    return year + '-' + month + '-' + day;
  }
};
export const convertMoneyDecimal = d => {
  if (d === undefined || d === null || d === '') {
    return 0;
  }
  d = +d;
  //Created by habx@esvn.com.vn
  if (typeof d === 'number') {
    d = replaceAll_ForNumber({ d: d.toString(), k: '.', r: ',' });
  }
  //Created by habx@esvn.com.vn
  var d = d.toString();
  var x1 = d;
  if (x1 === undefined || x1 === null) {
    return 0;
  }
  x1 = replaceAll_ForNumber({ d: x1, k: '.', r: '' });
  var _x2 = '';
  if (x1.split(',').length > 1) {
    _x2 = x1.split(',')[1];
    if (_x2.length > 2) {
      _x2 = _x2.substring(0, NUMBER_DECIMAL);
    }
    x1 = x1.split(',')[0];
  }
  x1 = parseInt(x1).toString();
  var i1 = x1.toString();
  i1 = i1
    .split('')
    .reverse()
    .join('');
  var arr = [];
  while (i1.length > 0) {
    arr.push(i1.substr(0, 3));
    i1 = i1.slice(3, i1.length);
  }
  for (var i = 0; i < arr.length; i++) {
    if (i === arr.length - 1) {
      if (arr[i] === '-') {
        arr[i] === '0-';
      }
      i1 += arr[i];
    } else {
      i1 += arr[i] + '.';
    }
  }
  i1 = i1
    .split('')
    .reverse()
    .join('');
  if (_x2 !== undefined && _x2 !== null && d.split(',').length > 1) {
    x1 = i1 + ',' + _x2;
  } else {
    x1 = i1;
  }

  return x1;
};
const replaceAll_ForNumber = d => {
  var _d = d.d;
  while (_d.indexOf(d.k) != -1) {
    _d = _d.replace(d.k, d.r);
  }
  return _d;
};
