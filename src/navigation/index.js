/**
 * App Navigation
 */
import React from 'react';
import {
  Actions,
  Scene,
  ActionConst,
  Stack,
  Lightbox,
  Overlay,
  Drawer,
} from 'react-native-router-flux';

// Import all screens here
import {
  MainComponent,
  LoginComponent,
  SplashComponent,
  HomeComponent,
  FakeHome,
  SignupComponent,
  DrawerComponent,
  Edit_PassWord_Component,
  Notification_Detail_Components,
  Dialog,
  ListOrdersComponent,
  ViewOrderComponent,
  ListOrderDetailsComponent,
  OrdersAttachmentComponent,
  HrComponent,
  EmployeesComponent,
  EmpViewItemComponent,
  OrderDetailsSXComponent,
  OpenOrderDetailsSXComponent,
  //begin using for tm
  // TasksComponent,
  // TasksHomeComponent,
  // TasksDetailComponent,
  // TasksAddChildComponent,
  // TasksAttachmentComponent,
  // TasksCommentComponent,
  // TasksForwardComponent,
  // TasksProcessComponent,
  //CommentWF
  CommentWFComponent,
  // TinhLV
  CheckinHistoryComponent,
  indexComponent,
  addComponent,
  openComponent,
  indexCompensationSlipComponent,
  addCompensationSlipComponent,
  openCompensationSlipComponent,
  indexAdvancesAM,
  openAdvancesAM,
  addAdvances,
  indexAdvances,
  openAdvances,
  openContracts,
  openInsurances,
  openSalaries,
  indexCandidates,
  openCandidates,
  indexTrainingPlans,
  addTrainingPlans,
  openTrainingPlans,
  indexQuotations,
  openQuotations,
  openQuotationDetails,
  openTrainingPlanDetails,
  indexChartDashboard,
  indexDashboard,
  ConfirmPayment,
  ConfirmPayments,
  InvoiceOrdersComponent,
  GetInvoiceOrdersComponent,
  OpenApplyOutsideDetails,
  ListProjects,
  OpenProject,
  ListProjectItems,
  OpenProjectItems,
  openHistories,
  openRewards,
  openCertificates,
  // thanhtt@esvn.com.vn
  WorkShiftChangeComponent,
  AddWorkShiftChangeComponent,
  GetWorkShiftChangeComponent,
  QuotationRequestComponent,
  AddQuotationRequestComponent,
  GetQuotationRequestComponent,
  QuotationRequestProComponent,
  AddQuotationRequestProComponent,
  GetQuotationRequestProComponent,
  RegisterEatsComponent,
  AddEatsComponent,
  GetEatComponent,
  ResignationFormsComponent,
  GetResignationForms,
  AddResignationForms,
  RecruitmentPlansComponent,
  GetRecruitmentPlans,
  RecruitmentRequestsComponent,
  GetRecruitmentRequests,
  TrainingResultsComponent,
  GetTrainingResults,
  RecruitmentSchedulesComponent,
  GetRecruitmentSchedules,
  ViewSalaryComponent,
  GetViewSalary,
  ChartDashboardAMComponent,
  UniformsComponent,
  GetUniforms,
  ViewInsurancesComponent,
  GetInsurances,
  RewardsComponent,
  GetRewards,
  AddEmployeeCheckIn,
  TimeKeepingsConfirmComponent,
  GetTimeKeepingsConfirm,
  timeKeepingsComfirm_Add,
  ProfileComponent,
  ViewSalaryByMeComponent,
  UniformsByMeComponent,
  InsurancesByMeComponent,
  TimeKeepingsByMeComponent,
  ApplyOverTimesByMeComponent,
  ApplyOverTimesItemByMe_Component,
  ApplyLeavesByMeComponent,
  ApplyOutsidesByMeComponent,
  ApplyOutsidesItemByMe_Component,
  GetRewardsByMe,
  DisciplineByMeComponent,
  MaintenanceRequestComponent,
  GetMaintenanceRequest,
  openMaintenanceManagement,
  index_ToDoByMe_Component,
  MeetingCalendarsComponent,
  GetMeetingCalendars,
  AddMeetingCalendars,
  // trongtq@esvn.com.vn
  View_VouChers_Component,
  Open_VouChers_Component,
  ApplyOutsidesListAll_Compoment,
  ProductionOrdersComponent,
  ApplyOutsidesItem_Component,
  ApplyOutsidesAdd_Component,
  ApplyLeavesComponent,
  ApplyLeaveViewItemComponent,
  ViewBottonHrComponent,
  ApplyLeavesAddComponent,
  TimeKeepingsComponent,
  TimeKeepingbyMonthComponent,
  ApplyOverTimesListAll_Component,
  ApplyOverTimesItem_Component,
  ApplyOverTimesAdd_Component,
  Information_Component,
  //sontd@esvn.com.vn---BM
  ItemProposedPurchaseComponent,
  ListProposedPurchaseComponent,
  ListProposedPurchasesComponent,
  ItemProposedPurchasesComponent,
  ItemProposedPurchaseDetailsComponent,
  AddProposedPurchase,
  ListPurchaseOrdersComponent,
  ItemPurchaseOrdersComponent,
  ItemPurchaseOrderDetailsComponent,
  CheckComponent,
  ListPPCancelRequestsComponent,
  ItemPPCancelRequestsComponent,
  ListCheckRequestsComponent,
  CheckRequestsComponent,
  ListPOPaymentsAfterComponent,
  ItemPOPaymentsAfterComponent,
  ListPOPaymentsAfterBMComponent,
  ItemPOPaymentsAfterBMComponent,
  ItemPOPaymentsBeforeBMComponent,
  ListPOPaymentsBeforeBMComponent,
  ListRequirePaymentsComponent,
  ViewRequirePaymentsComponent,
  ListPOPaymentsBeforeComponent,
  ItemPOPaymentsBeforeComponent,
  ListPOCancelRequestsComponent,
  ItemPOCancelRequestsComponent,
  DashBoardComponent,
  ListPOComponent,
  ListTicketRequestsComponent,
  AddTicketRequestsComponent,
  ItemTicketRequestsComponent,
  ItemTicketRequestsDetailsComponent,
  OpenCheckRequestsComponent,
  //sontd@esvn.com.vn---HR
  ListAnnouncementsComponent,
  ItemAnnouncementsComponent,
  ListTrainingRequestsComponent,
  ItemTrainingRequestsComponent,
  ListTicketRequestsImportComponent,
  ViewTicketRequestsImportComponent,
  ListTicketRequestsDetailsImportComponent,
  AddTicketRequestsImportComponent,
  ListTicketRequestsExportComponent,
  ViewTicketRequestsExportComponent,
  ListTicketRequestsDetailsExportComponent,
  CommentComponent,
  ProductionOrderItem_Component,
  ListAll_Messange_Component,
  ViewItem_Messange_Component,
  AddGroup_Component,
  //sontd@esvn.com.vn---AM
  ListItemsComponent,
  ViewItemsComponent,
  ListPOContractsComponent,
  ItemPOContractsComponent,
  ListContractsComponent,
  ItemContractsComponent,
  ListObjectProvidersComponent,
  ItemObjectProvidersComponent,
  ListTicketCheckingComponent,
  ViewTicketCheckingComponent,
  AddTicketCheckingComponent,
  ListPOPaymentsCustomerComponent,
  ItemPOPaymentsCustomerComponent,
  //sontd@esvn.com.vn---SM
  ListCustomersComponent,
  ItemCustomersComponent,
  ListCompaignsComponent,
  ItemCompaignsComponent,
  KanbanLeadsComponet,
  ItemLeadsComponent,
  Dashboard_KPI,
  Customers_Add_Component,
  //sontd@esvn.com.vn---PRO
  ListProductionResultsComponent,
  ItemProductionResultComponent,
  //habx@esvn.com.vn
  ProductionFollowComponent,
  //sontd@esvn.com.vn---TM
  ListDocumentComponent,
  AddFolderComponent,
  //begin using for Attachment
  AttachmentComponent,
  //end using for Attachment
  ViewAllGroupByLoginName_Compoment,
  ViewDetailAllUser_Component,
  AddUserOfGroup_Component,
  ViewAllImage_Component,
  TestSound_Component,
  GetAllPaging_Component,
  VoucherAM_ViewItem_Component,
  Transfer_ListAll_Component,
  Transfer_ViewItem_Component,
  VoucherAM_ViewDetail_Component,
  Task_ListAll_Component,
  Task_Add_Component,
  Task_Detail_Component,
  Task_Comment_Component,
  Calendar_Detail_Component,
  Calendar_Add_Component,
  AttendancesComponent,
  Task_UpdateProcess_Component,
  QRCode,
  QRCodeV2,
  //Begin Todo
  index_ToDo_Component,
  MyDay_ToDo_Component,
  open_ToDo_Component,
  ListData_ToDo_Component,
  //The end Todo
  CheckInComponent,
  //begin Email
  Mail_index_Component,
  Mail_Message_Component,
  Mail_open_Component,
  //the end Email
  ListCalendars_Component,
  //begin trongtq PRO
  PlanTotal_List_Component,
  ProductionPlanDay_List_Component,
  ProductionOrders_Open_Component,
  PlanTotal_Open_Component,
  ProductionPlanDay_Open_Component,
  ProductionPlanDayUpdateComponent,
  ListPuschaseOderProgress2Component,
  ItemPuschaseOderProgress2Component,
  ListProposedPurchasesProComponent,
  ItemProposedPurchasesProComponent,
  BomCategories_List_Component,
  BomCategories_Open_Component,
  ProductionPlan_List_Component,
  ProductionPlan_Open_Component,
  ProductionPlanCNC_List_Component,
  ProductionPlanCNC_Open_Component,
  Operator_index_Component,
  Operator_Employee_Component,
  Operator_MachinesTools_Component,
  Operator_Supplies_Component,
  Operator_Document_Component,
  Operator_ItemPrint_Component,
  Operator_Print_Component,
  Operator_ItemJobCard_Component,
  Operator_QCCheck_Component,
  Operator_Warehouses_Component,
  Operator_UpdateMachidnes_Component,
  Operator_addOverTime_Component,
  Operator_addEmployee_Component,
  Operator_sendBCEmployee_Component,
  Operator_EditTools_Component,
  Operator_AlertMachines_Component,
  Operator_addReportPlan_Component,
  Operator_ListPlanJobCards_Component,
  Operator_StopProduction_Component,
  Operator_addTicketRequests_Component,
  Operator_indexSupplies_Component,
  Operator_addTicketRequestsExport_Component,
  Operator_addMaterialErrors_Component,
  Operator_addTicketRequestsExportVT_Component,
  Operator_indexDocument_Component,
  ListOrderProgessComponent,
  ItemOrderProgessComponent,
  QCCheckComponent,
  Quotations_Form_Component,
  Orders_Form_Component,

  //the end trongtq PRO
} from '@screen';
import { AppStyles } from '@theme';

/* Routes ==================================================================== */
export default Actions.create(
  <Overlay>
    <Scene key="main" component={MainComponent} />
    <Lightbox>
      <Stack key="root" {...AppStyles.navbarProps} hideNavBar>
        <Scene key="splash" component={SplashComponent} />
        {/* <Scene key="tmtask" component={TasksComponent} /> */}
        {/* <Scene key="tmtaskdetail" component={TasksDetailComponent} />
        <Scene key="tmtaskaddchild" component={TasksAddChildComponent} />
        <Scene key="tmtaskAttachment" component={TasksAttachmentComponent} />
        <Scene key="tmtaskcomment" component={TasksCommentComponent} />
        <Scene key="tmtaskforward" component={TasksForwardComponent} />
        <Scene key="tmtaskprocess" component={TasksProcessComponent} /> */}
        <Scene key="login" component={LoginComponent} />
        <Scene key="signup" component={SignupComponent} />
        {/* đường đẫn đến các mục */}
        <Scene key="Operator" component={Operator_index_Component} />
        <Scene
          key="ProductionPlanCNC"
          component={ProductionPlanCNC_List_Component}
        />
        <Scene
          key="PuschaseOderProgress2"
          component={ListPuschaseOderProgress2Component}
        />
        <Scene
          key="ItemPuschaseOderProgress2"
          component={ItemPuschaseOderProgress2Component}
        />
        <Scene
          key="ProposedPurchasesPro"
          component={ListProposedPurchasesProComponent}
        />
        <Scene
          key="ItemProposedPurchasesPro"
          component={ItemProposedPurchasesProComponent}
        />
        <Scene key="ProductionPlan" component={ProductionPlan_List_Component} />
        <Scene key="BomCategories" component={BomCategories_List_Component} />
        <Scene key="PlanTotal" component={PlanTotal_List_Component} />
        <Scene
          key="ProductionPlanDay"
          component={ProductionPlanDay_List_Component}
        />
        <Scene key="checkinHistory" component={CheckinHistoryComponent} />
        <Scene key="RegisterCars" component={indexComponent} />
        <Scene
          key="CompensationSlip"
          component={indexCompensationSlipComponent}
        />
        <Scene key="AdvancesAM" component={indexAdvancesAM} />
        <Scene key="Advances" component={indexAdvances} />
        <Scene key="CheckIn" component={CheckInComponent} />
        <Scene key="ProjectItems" component={ListProjectItems} />
        <Scene key="Projects" component={ListProjects} />
        <Scene key="Candidates" component={indexCandidates} />
        <Scene key="TrainingPlans" component={indexTrainingPlans} />
        <Scene key="addTrainingPlans" component={addTrainingPlans} />
        <Scene key="Quotations" component={indexQuotations} />
        <Scene key="ChartDashboard_SM" component={indexDashboard} />
        <Scene key="ConfirmPayment" component={ConfirmPayment} />
        <Scene key="ConfirmPayments" component={ConfirmPayments} />
        <Scene key="InvoiceOrders" component={InvoiceOrdersComponent} />
        <Scene key="GetInvoiceOrders" component={GetInvoiceOrdersComponent} />
        <Scene key="ChartDashboard" component={indexChartDashboard} />
        <Scene key="ApplyOutsides" component={ApplyOutsidesListAll_Compoment} />
        <Scene
          key="ApplyOverTimes"
          component={ApplyOverTimesListAll_Component}
        />
        <Scene key="MeetingCalendars" component={MeetingCalendarsComponent} />
        <Scene key="GetMeetingCalendars" component={GetMeetingCalendars} />
        <Scene key="AddMeetingCalendars" component={AddMeetingCalendars} />
        <Scene key="Employees" component={EmployeesComponent} />
        <Scene key="home2" component={FakeHome} />
        <Scene key="Orders" component={ListOrdersComponent} />
        <Scene key="Task" component={Task_ListAll_Component} />
        <Scene key="ProductionOrders" component={ProductionOrdersComponent} />
        <Scene
          key="ProposedPurchases"
          component={ListProposedPurchasesComponent}
        />
        <Scene
          key="ListProposedPurchases"
          component={ListProposedPurchaseComponent}
        />
        <Scene
          key="itemListProposedPurchases"
          component={ItemProposedPurchaseComponent}
        />
        <Scene key="TicketRequests" component={ListTicketRequestsComponent} />
        <Scene key="AddTicketRequests" component={AddTicketRequestsComponent} />
        <Scene key="PurchasingOrder" component={ListPurchaseOrdersComponent} />
        <Scene key="ApplyLeaves" component={ApplyLeavesComponent} />
        <Scene key="TimeKeepings" component={TimeKeepingsComponent} />
        <Scene
          key="TimeKeepingbyMonth"
          component={TimeKeepingbyMonthComponent}
        />
        <Scene key="email" component={Mail_index_Component} />
        <Scene
          key="TicketRequestsImport"
          component={ListTicketRequestsImportComponent}
        />
        <Scene key="CancelRequests" component={ListPPCancelRequestsComponent} />
        <Scene
          key="TicketRequestsExport"
          component={ListTicketRequestsExportComponent}
        />
        <Scene key="CheckRequests" component={ListCheckRequestsComponent} />
        <Scene key="POPaymentsAfter" component={ListPOPaymentsAfterComponent} />
        <Scene
          key="POPaymentsBefore"
          component={ListPOPaymentsBeforeComponent}
        />
        <Scene
          key="amitemPOPaymentsBefore"
          component={ItemPOPaymentsBeforeComponent}
        />
        <Scene
          key="amitemPOPaymentsAfter"
          component={ItemPOPaymentsAfterComponent}
        />
        <Scene
          key="amitemPOPaymentsBefore"
          component={ItemPOPaymentsBeforeComponent}
        />
        <Scene key="RequirePayments" component={ListRequirePaymentsComponent} />
        <Scene
          key="ViewRequirePayments"
          component={ViewRequirePaymentsComponent}
        />

        <Scene key="Announcements" component={ListAnnouncementsComponent} />
        <Scene
          key="POCancelRequests"
          component={ListPOCancelRequestsComponent}
        />
        <Scene key="Items" component={ListItemsComponent} />
        <Scene key="POContracts" component={ListPOContractsComponent} />
        <Scene key="Contracts" component={ListContractsComponent} />
        <Scene
          key="TrainingRequests"
          component={ListTrainingRequestsComponent}
        />
        <Scene
          key="POPaymentsCustomer"
          component={ListPOPaymentsCustomerComponent}
        />
        <Scene
          key="GetPOPaymentsCustomer"
          component={ItemPOPaymentsCustomerComponent}
        />
        <Scene key="ObjectProviders" component={ListObjectProvidersComponent} />
        <Scene key="Customers" component={ListCustomersComponent} />
        <Scene key="Transfer" component={Transfer_ListAll_Component} />
        <Scene key="VouchersAM" component={GetAllPaging_Component} />
        <Scene key="DashBoard" component={DashBoardComponent} />
        <Scene key="ListPurchaseOrders" component={ListPOComponent} />
        <Scene
          key="ProductionResult"
          component={ListProductionResultsComponent}
        />
        {/* Begein created by habx@esvn.com.vn */}
        <Scene key="ProductionFollow" component={ProductionFollowComponent} />
        {/* End created by habx@esvn.com.vn */}
        <Scene key="Document" component={ListDocumentComponent} />
        <Scene key="TicketChecking" component={ListTicketCheckingComponent} />
        <Scene key="Calendars" component={ListCalendars_Component} />
        <Scene key="Compaigns" component={ListCompaignsComponent} />
        <Scene key="Dashboard_KPI" component={Dashboard_KPI} />
        <Scene key="Leads" component={KanbanLeadsComponet} />
        <Drawer key="app" contentComponent={DrawerComponent} drawerWidth={300}>
          <Scene key="root" hideNavBar drawerLockMode="locked-closed">
            <Scene key="home" hideNavBar component={HomeComponent} />
          </Scene>
        </Drawer>
        <Scene key="Message" component={ListAll_Messange_Component} />
        <Scene key="viewItemMessange" component={ViewItem_Messange_Component} />
        {/* end */}
        <Scene key="editpass" component={Edit_PassWord_Component} />
        <Scene
          key="notificationDetail"
          component={Notification_Detail_Components}
        />
        <Scene key="customers_Add" component={Customers_Add_Component} />
        <Scene key="viewOrder" component={ViewOrderComponent} />
        <Scene key="listOrderDetails" component={ListOrderDetailsComponent} />
        <Scene key="ordersAttachment" component={OrdersAttachmentComponent} />
        <Scene key="OrderDetailsSX" component={OrderDetailsSXComponent} />
        <Scene
          key="OpenOrderDetailsSX"
          component={OpenOrderDetailsSXComponent}
        />
        <Scene key="quotations_Form" component={Quotations_Form_Component} />
        <Scene
          key="applyLeaveViewItem"
          component={ApplyLeaveViewItemComponent}
        />
        <Scene key="applyLeavesadd" component={ApplyLeavesAddComponent} />
        <Scene key="hrmain" component={HrComponent} />
        <Scene key="empviewitem" component={EmpViewItemComponent} />
        <Scene key="viewBottonHr" component={ViewBottonHrComponent} />
        <Scene key="Attendances" component={AttendancesComponent} />
        <Scene key="commentWF" component={CommentWFComponent} />
        <Scene key="addRegistercar" component={addComponent} />
        <Scene key="openRegistercar" component={openComponent} />
        <Scene
          key="addCompensationSlip"
          component={addCompensationSlipComponent}
        />
        <Scene
          key="openCompensationSlip"
          component={openCompensationSlipComponent}
        />
        <Scene key="openAdvancesAM" component={openAdvancesAM} />
        <Scene key="openAdvances" component={openAdvances} />
        <Scene key="openContracts" component={openContracts} />
        <Scene key="openInsurances" component={openInsurances} />
        <Scene key="openSalaries" component={openSalaries} />
        <Scene key="openHistories" component={openHistories} />
        <Scene key="openRewards" component={openRewards} />
        <Scene key="openCertificates" component={openCertificates} />
        <Scene key="openCandidates" component={openCandidates} />
        <Scene key="openTrainingPlans" component={openTrainingPlans} />
        <Scene
          key="openTrainingPlanDetails"
          component={openTrainingPlanDetails}
        />
        <Scene key="openQuotations" component={openQuotations} />
        <Scene key="openQuotationDetails" component={openQuotationDetails} />
        <Scene
          key="OpenApplyOutsideDetails"
          component={OpenApplyOutsideDetails}
        />
        <Scene key="OpenProject" component={OpenProject} />
        <Scene key="OpenProjectItems" component={OpenProjectItems} />
        <Scene key="commentWF" component={CommentWFComponent} />
        <Scene key="addRegistercar" component={addComponent} />
        <Scene key="openRegistercar" component={openComponent} />
        <Scene key="addAdvances" component={addAdvances} />
        <Scene key="openContracts" component={openContracts} />
        <Scene key="openInsurances" component={openInsurances} />
        <Scene key="openSalaries" component={openSalaries} />
        <Scene key="openHistories" component={openHistories} />
        <Scene key="openRewards" component={openRewards} />
        <Scene key="openCertificates" component={openCertificates} />
        <Scene key="openCandidates" component={openCandidates} />
        <Scene key="openTrainingPlans" component={openTrainingPlans} />
        <Scene
          key="openTrainingPlanDetails"
          component={openTrainingPlanDetails}
        />
        <Scene key="openQuotations" component={openQuotations} />
        <Scene key="openQuotationDetails" component={openQuotationDetails} />
        <Scene
          key="OpenApplyOutsideDetails"
          component={OpenApplyOutsideDetails}
        />
        <Scene key="OpenProject" component={OpenProject} />
        <Scene key="OpenProjectItems" component={OpenProjectItems} />
        <Scene key="WorkShiftChange" component={WorkShiftChangeComponent} />
        <Scene
          key="AddWorkShiftChange"
          component={AddWorkShiftChangeComponent}
        />
        <Scene
          key="GetWorkShiftChange"
          component={GetWorkShiftChangeComponent}
        />
        <Scene key="QuotationRequest" component={QuotationRequestComponent} />
        <Scene
          key="AddQuotationRequest"
          component={AddQuotationRequestComponent}
        />
        <Scene
          key="GetQuotationRequest"
          component={GetQuotationRequestComponent}
        />
        <Scene
          key="QuotationRequestPro"
          component={QuotationRequestProComponent}
        />
        <Scene
          key="AddQuotationRequestPro"
          component={AddQuotationRequestProComponent}
        />
        <Scene
          key="GetQuotationRequestPro"
          component={GetQuotationRequestProComponent}
        />
        <Scene key="Registereats" component={RegisterEatsComponent} />
        <Scene key="addregistereats" component={AddEatsComponent} />
        <Scene key="getEatComponent" component={GetEatComponent} />
        <Scene key="ResignationForms" component={ResignationFormsComponent} />
        <Scene key="GetResignationForms" component={GetResignationForms} />
        <Scene key="AddResignationForms" component={AddResignationForms} />
        <Scene key="AddResignationForms" component={AddResignationForms} />
        <Scene
          key="GetRecruitmentSchedules"
          component={GetRecruitmentSchedules}
        />
        <Scene
          key="RecruitmentSchedules"
          component={RecruitmentSchedulesComponent}
        />
        <Scene key="RecruitmentPlans" component={RecruitmentPlansComponent} />
        <Scene key="GetRecruitmentPlans" component={GetRecruitmentPlans} />
        <Scene
          key="GetRecruitmentRequests"
          component={GetRecruitmentRequests}
        />
        <Scene
          key="RecruitmentRequests"
          component={RecruitmentRequestsComponent}
        />
        <Scene key="TrainingResults" component={TrainingResultsComponent} />
        <Scene key="GetTrainingResults" component={GetTrainingResults} />
        <Scene key="ViewSalary" component={ViewSalaryComponent} />
        <Scene key="getViewSalary" component={GetViewSalary} />
        <Scene key="ChartDashboardAM" component={ChartDashboardAMComponent} />
        <Scene key="Uniforms" component={UniformsComponent} />
        <Scene key="GetUniforms" component={GetUniforms} />
        <Scene key="Insurances" component={ViewInsurancesComponent} />
        <Scene key="GetInsurances" component={GetInsurances} />
        <Scene key="Rewards" component={RewardsComponent} />
        <Scene key="GetInsurances" component={GetInsurances} />
        <Scene key="GetRewards" component={GetRewards} />
        <Scene key="AddEmployeeCheckIn" component={AddEmployeeCheckIn} />
        <Scene
          key="TimeKeepingsConfirm"
          component={TimeKeepingsConfirmComponent}
        />
        <Scene
          key="GetTimeKeepingsConfirm"
          component={GetTimeKeepingsConfirm}
        />
        <Scene
          key="timeKeepingsComfirm_Add"
          component={timeKeepingsComfirm_Add}
        />

        <Scene key="Profile" component={ProfileComponent} />
        <Scene key="ViewSalaryByMe" component={ViewSalaryByMeComponent} />
        <Scene key="UniformsByMe" component={UniformsByMeComponent} />
        <Scene key="InsurancesByMe" component={InsurancesByMeComponent} />
        <Scene key="TimeKeepingByMe" component={TimeKeepingsByMeComponent} />
        <Scene
          key="ApplyOverTimeByMe"
          component={ApplyOverTimesByMeComponent}
        />
        <Scene
          key="ApplyOverTimesItemByMe"
          component={ApplyOverTimesItemByMe_Component}
        />
        <Scene key="ApplyLeaveByMe" component={ApplyLeavesByMeComponent} />
        <Scene key="ApplyOutsideByMe" component={ApplyOutsidesByMeComponent} />
        <Scene
          key="ApplyOutsidesItemByMe"
          component={ApplyOutsidesItemByMe_Component}
        />
        <Scene key="ToDoByMe" component={index_ToDoByMe_Component} />
        <Scene key="RewardsByMe" component={GetRewardsByMe} />
        <Scene key="DisciplineByMe" component={DisciplineByMeComponent} />
        <Scene
          key="MaintenanceRequest"
          component={MaintenanceRequestComponent}
        />
        <Scene key="GetMaintenanceRequest" component={GetMaintenanceRequest} />
        <Scene
          key="openMaintenanceManagement"
          component={openMaintenanceManagement}
        />
        {/*trongtq@esvn.com.vn*/}
        <Scene key="viewvouchers" component={View_VouChers_Component} />
        <Scene key="openvouchers" component={Open_VouChers_Component} />
        <Scene
          key="applyOutsidesItem"
          component={ApplyOutsidesItem_Component}
        />
        <Scene key="applyOutsidesAdd" component={ApplyOutsidesAdd_Component} />
        <Scene
          key="applyOverTimesItem"
          component={ApplyOverTimesItem_Component}
        />
        <Scene
          key="applyOverTimesAdd"
          component={ApplyOverTimesAdd_Component}
        />
        <Scene key="AddGroup" component={AddGroup_Component} />
        <Scene key="Information" component={Information_Component} />
        <Scene
          key="viewAllGroupByLoginName"
          component={ViewAllGroupByLoginName_Compoment}
        />
        <Scene
          key="viewDetailAllUser"
          component={ViewDetailAllUser_Component}
        />
        <Scene key="addUserOfGroup" component={AddUserOfGroup_Component} />
        <Scene key="viewAllImage" component={ViewAllImage_Component} />
        <Scene key="testSound" component={TestSound_Component} />
        <Scene
          key="voucherAMViewItem"
          component={VoucherAM_ViewItem_Component}
        />
        <Scene
          key="voucherAMViewDetail"
          component={VoucherAM_ViewDetail_Component}
        />
        <Scene key="transferViewItem" component={Transfer_ViewItem_Component} />
        <Scene key="taskAdd" component={Task_Add_Component} />
        <Scene key="taskDetail" component={Task_Detail_Component} />
        <Scene key="taskComment" component={Task_Comment_Component} />
        <Scene
          key="taskUpdateProcess"
          component={Task_UpdateProcess_Component}
        />
        <Scene key="calendarDetail" component={Calendar_Detail_Component} />
        <Scene key="calendarAdd" component={Calendar_Add_Component} />
        <Scene key="qrCode" component={QRCode} />
        <Scene key="qrCodeV2" component={QRCodeV2} />
        {/* Begin PRO_V2 */}
        <Scene key="PlanTotal_Open" component={PlanTotal_Open_Component} />
        <Scene
          key="ProductionPlanDay_Open"
          component={ProductionPlanDay_Open_Component}
        />
        <Scene
          key="ProductionPlanDayUpdate"
          component={ProductionPlanDayUpdateComponent}
        />
        <Scene
          key="BomCategories_Open"
          component={BomCategories_Open_Component}
        />
        <Scene
          key="ProductionPlan_Open"
          component={ProductionPlan_Open_Component}
        />
        <Scene
          key="ProductionPlanCNC_Open"
          component={ProductionPlanCNC_Open_Component}
        />
        <Scene
          key="Operator_Employee"
          component={Operator_Employee_Component}
        />
        <Scene
          key="Operator_MachinesTools"
          component={Operator_MachinesTools_Component}
        />
        <Scene
          key="Operator_Supplies"
          component={Operator_Supplies_Component}
        />
        <Scene
          key="Operator_Document"
          component={Operator_Document_Component}
        />
        <Scene key="Operator_Print" component={Operator_Print_Component} />
        <Scene
          key="Operator_ItemJobCard"
          component={Operator_ItemJobCard_Component}
        />
        <Scene
          key="Operator_ItemPrint"
          component={Operator_ItemPrint_Component}
        />
        <Scene key="Operator_QCCheck" component={Operator_QCCheck_Component} />
        <Scene
          key="Operator_Warehouses"
          component={Operator_Warehouses_Component}
        />
        <Scene
          key="Operator_UpdateMachidnes"
          component={Operator_UpdateMachidnes_Component}
        />
        <Scene
          key="Operator_addOverTime"
          component={Operator_addOverTime_Component}
        />
        <Scene
          key="Operator_addEmployee"
          component={Operator_addEmployee_Component}
        />
        <Scene
          key="Operator_sendBCEmployee"
          component={Operator_sendBCEmployee_Component}
        />
        <Scene
          key="Operator_EditTools"
          component={Operator_EditTools_Component}
        />
        <Scene
          key="Operator_AlertMachines"
          component={Operator_AlertMachines_Component}
        />
        <Scene
          key="Operator_addReportPlan"
          component={Operator_addReportPlan_Component}
        />
        <Scene
          key="Operator_ListPlanJobCards"
          component={Operator_ListPlanJobCards_Component}
        />
        <Scene
          key="Operator_StopProduction"
          component={Operator_StopProduction_Component}
        />
        <Scene
          key="Operator_addTicketRequests"
          component={Operator_addTicketRequests_Component}
        />
        <Scene
          key="Operator_indexSupplies"
          component={Operator_indexSupplies_Component}
        />
        <Scene
          key="Operator_addTicketRequestsExport"
          component={Operator_addTicketRequestsExport_Component}
        />
        <Scene
          key="Operator_addMaterialErrors"
          component={Operator_addMaterialErrors_Component}
        />
        <Scene
          key="Operator_addTicketRequestsExportVT"
          component={Operator_addTicketRequestsExportVT_Component}
        />
        <Scene
          key="Operator_indexDocument"
          component={Operator_indexDocument_Component}
        />
        <Scene key="OrderProgress" component={ListOrderProgessComponent} />
        <Scene key="ItemOrderProgress" component={ItemOrderProgessComponent} />
        <Scene key="QCCheck" component={QCCheckComponent} />
        {/* The end PRO_V2 */}
        {/* Begin Todo */}
        <Scene key="ToDo" component={index_ToDo_Component} />
        <Scene key="myDayToDo" component={MyDay_ToDo_Component} />
        <Scene key="openToDo" component={open_ToDo_Component} />
        <Scene key="listDataToDo" component={ListData_ToDo_Component} />
        {/* The end Todo */}
        {/* Begin Mail */}
        <Scene key="mailMessage" component={Mail_Message_Component} />
        <Scene key="mailOpen" component={Mail_open_Component} />
        {/* The end Mail */}
        <Scene
          key="ProductionOrders_Open"
          component={ProductionOrders_Open_Component}
        />
        <Scene
          key="bmitemProposedPurchases"
          component={ItemProposedPurchasesComponent}
        />
        <Scene
          key="bmitemPPDetails"
          component={ItemProposedPurchaseDetailsComponent}
        />
        <Scene
          key="POPaymentsAfterBM"
          component={ListPOPaymentsAfterBMComponent}
        />
        <Scene
          key="POPaymentsBeforeBM"
          component={ListPOPaymentsBeforeBMComponent}
        />
        <Scene
          key="bmitemPOPaymentsAfter"
          component={ItemPOPaymentsAfterBMComponent}
        />
        <Scene
          key="bmitemPOPaymentsBefore"
          component={ItemPOPaymentsBeforeBMComponent}
        />
        <Scene
          key="bmitemTicketRequests"
          component={ItemTicketRequestsComponent}
        />
        <Scene
          key="bmitemTRDetails"
          component={ItemTicketRequestsDetailsComponent}
        />
        <Scene key="OpenCheckRequests" component={OpenCheckRequestsComponent} />
        <Scene
          key="bmitemAddProposedPurchase"
          component={AddProposedPurchase}
        />
        <Scene
          key="bmitemPurchaseOrders"
          component={ItemPurchaseOrdersComponent}
        />
        <Scene
          key="bmitemPODetails"
          component={ItemPurchaseOrderDetailsComponent}
        />
        <Scene key="bmCheck" component={CheckComponent} />
        <Scene
          key="bmitemPPCancelRequests"
          component={ItemPPCancelRequestsComponent}
        />

        <Scene
          key="hrItemAnnouncements"
          component={ItemAnnouncementsComponent}
        />
        <Scene
          key="bmitemPOCancelRequests"
          component={ItemPOCancelRequestsComponent}
        />
        <Scene key="bmCheckRequests" component={CheckRequestsComponent} />
        <Scene key="amViewItems" component={ViewItemsComponent} />
        <Scene key="amitemPOContracts" component={ItemPOContractsComponent} />
        <Scene key="amitemContracts" component={ItemContractsComponent} />
        <Scene
          key="hritemTrainingRequests"
          component={ItemTrainingRequestsComponent}
        />
        <Scene
          key="amitemObjectProviders"
          component={ItemObjectProvidersComponent}
        />
        <Scene key="smitemCustomers" component={ItemCustomersComponent} />
        <Scene
          key="proitemProductionResult"
          component={ItemProductionResultComponent}
        />
        <Scene key="tmaddFolder" component={AddFolderComponent} />
        <Scene
          key="amViewTicketChecking"
          component={ViewTicketCheckingComponent}
        />
        <Scene
          key="amAddTicketChecking"
          component={AddTicketCheckingComponent}
        />
        <Scene key="smitemCompaigns" component={ItemCompaignsComponent} />
        <Scene key="smitemLeads" component={ItemLeadsComponent} />
        <Scene
          key="viewTicketRequestsImportComponent"
          component={ViewTicketRequestsImportComponent}
        />
        <Scene
          key="listTicketRequestsDetailsImportComponent"
          component={ListTicketRequestsDetailsImportComponent}
        />
        <Scene
          key="viewTicketRequestsExportComponent"
          component={ViewTicketRequestsExportComponent}
        />
        <Scene
          key="listTicketRequestsDetailsExportComponent"
          component={ListTicketRequestsDetailsExportComponent}
        />
        <Scene
          key="addTicketRequests"
          component={AddTicketRequestsImportComponent}
        />
        <Scene key="commentComponent" component={CommentComponent} />
        <Scene key="attachmentComponent" component={AttachmentComponent} />
      </Stack>
      <Scene key="dialog" component={Dialog} />
      <Scene key="orders_Form" component={Orders_Form_Component} />
    </Lightbox>
  </Overlay>,
);
// hideDrawerButton hideNavBar
