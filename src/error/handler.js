import {Alert} from 'react-native';
import {MessageBarManager} from 'react-native-message-bar';
import {messages} from '@i18n';
import {AppSizes, AppColors} from '@theme';
import _ from 'lodash';

export default {
  handle(errorData, usingSnackbar) {
    console.log('errorData', errorData);
    console.log('usingSnackbar', usingSnackbar);
    var message = errorData && errorData.message;
    message = _.isEmpty(message) ? messages.error.unknown_error : message;
    if (usingSnackbar) {
      MessageBarManager.showAlert({
        viewTopInset: AppSizes.statusBarHeight,
        alertType: 'warning',
        message: message,
        messageStyle: {alignSelf: 'center'},
      });
    } else {
      Alert.alert(messages.error_title, message);
    }
  },
  handleError(errorData, usingSnackbar) {
    console.log('errorData', errorData);
    console.log('usingSnackbar', usingSnackbar);
    if (usingSnackbar) {
      MessageBarManager.showAlert({
        viewTopInset: AppSizes.statusBarHeight,
        alertType: 'warning',
        message: 'Đăng ',
        messageStyle: {alignSelf: 'center'},
      });
    } else {
      Alert.alert('', errorData);
    }
  },
};
