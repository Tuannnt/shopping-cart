/**
 * App Messages
 */

export default {
  email: 'Email',
  _password: 'Mật khẩu',
  retype_password: 'Xác nhận mật khẩu',
  signup: 'Đăng ký',
  error_title: 'Có lỗi',
  cancel: 'Huỷ bỏ',
  share: 'Chia sẻ',
  add: 'Thêm',
  group: 'Nhóm',
  device: 'Thiết bị',
  detail: 'Chi tiết',
  yes: 'Có',
  no: 'Không',
  ok: 'OK',
  update: 'Chỉnh sửa',

  splash: {
    esvn: 'Phát triển bởi Công ty Cổ phần Eastern Sun Việt nam',
    copyright: 'Copyright 2020 ESVN., JSC. All rights reserved',
    slogan: 'Giải pháp quản trị doanh nghiệp 4.0',
  },

  login: {
    login: 'Đăng nhập',
    signup: 'Đăng ký',
    forgot_password: 'Quên mật khẩu?',
    login_facebook: 'Đăng nhập với Facebook',
    password: 'Mật khẩu',
    UserName: 'Tài khoản',
  },

  register: {
    name: 'Họ tên',
  },

  empty: {
    empty_devices: 'Chưa có thiết bị nào',
  },

  main: {
    devices: 'Thiết bị',
    multi_devices: 'Đa thiết bị',
    scenes: 'Kịch bản',
    rules: 'Luật',
    language: 'Ngôn ngữ - Language',
    change_password: 'Đổi mật khẩu',
    user_info: 'Thông tin cá nhân',
    setting: 'Cài đặt',
    logout: 'Đăng xuất',
    server: 'Server',
  },
  devices: {
    no_group: 'Các thiết bị không thuộc nhóm nào',
    share_group: 'Các thiết bị được chia sẻ',
    detail: 'Chi tiết',
    share: 'Chia sẻ',
    pair: 'Pair',
    delete: 'Xóa thiết bị',
    cancel: 'Thoát',
    share_devices: 'Chia sẻ thiết bị',
    check: 'Kiểm tra',
    select_accout_to_share: 'Chọn 1 tài khoản để chia sẻ thiết bị',
    account_to_share_info: 'Thông tin tài khoản được chia sẻ',
    permission: 'Quyền',
    permission_items: ['Chỉ xem', 'Điều khiển'],
    group_name: 'Tên nhóm',
    group_description: 'Miêu tả',
    group_add: 'Thêm nhóm',
    group_delete: 'Xóa nhóm',
    group_delete_confirm: 'Bạn có chắc muốn xoá nhóm "{0}" không?',
  },
  alarm: {
    alarm: 'Hẹn giờ',
    alarm_list: 'Danh sách hẹn giờ',
  },

  actionsheet: {
    cancel: 'Thoát',
  },

  error: {
    unknown_error: 'Lỗi không xác định',
    email_invalid: 'Vui lòng nhập email',
    password_invalid: 'Vui lòng nhập mật khẩu',
    group_name_invalid: 'Please enter a name',
    user_invalid: 'Vui lòng nhập tên đăng nhập',
  },
};
