import Config from 'react-native-config';
export default {
  url: 'https://gmt-login.easternsun.vn',
  url_sso: 'https://gmt-login.easternsun.vn',
  url_api: 'http://gmt-api.easternsun.vn',
  url_api_mb: 'https://gmt-mbapi.easternsun.vn',
  url_api_cm: 'http://gmt-apicm.easternsun.vn',
  url_icon_chat: 'https://gmt-static.easternsun.vn',
  link_type_icon: '/img/fileicon/',
  onesignal_guid: '3459ec28-1ef3-45d5-914a-81716539984a',
  url_signalr_mb: 'https://gmt-cb.easternsun.vn/chathub',
  url_hubConnection: 'https://gmt-chat.essolution.net/',
  url_ProFollow: 'http://gmt-mss.easternsun.vn/',
  grant_type_login: 'password',
  grant_type_refresh: 'refresh_token',
  client_id: 'mobileapp',
  client_secret: 'secret',
  scope: 'app_read_api',
  configHeaders: {
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data',
  },
  MB_PAGES: 'MB_PAGES',
  url_Image_chat:
    'https://gmt-mbapi.easternsun.vn/api/Messenger/Messenger_DownloadAttachment_Thumb/',
  url_View_Image:
    'https://gmt-mbapi.easternsun.vn/api/Messenger/Messenger_DownloadAttachment/',
  url_View_Thumb_Photo:
    'https://gmt-mbapi.easternsun.vn/api/Items/GetItems_Thumb/',
  url_Document_Download:
    'https://gmt-mbapi.easternsun.vn/api/Document/Download/',
  url_Task_ViewImg:
    'https://gmt-mbapi.easternsun.vn/api/Tasks/Task_DownloadAttachment_Thumb/',
  url_Task_DownloadImg:
    'https://gmt-mbapi.easternsun.vn/api/Tasks/Task_DownloadAttachment/',
  url_Task_Downloadfile:
    'https://gmt-mbapi.easternsun.vn/api/Tasks/DownloadAttachment/',
  url_Attachment_Download:
    'https://gmt-mbapi.easternsun.vn/api/Attachments/Download/',
  url_Download_mail: 'http://gmt-mail.easternsun.vn/',
  url_pro_image: 'https://gmt-pro.easternsun.vn',
};

// export default {
//   url: 'https://gmt-login.easternsun.vn',
//   url_sso: 'https://gmt-login.easternsun.vn',
//   url_api: 'http://gmt-api.easternsun.vn',
//   onesignal_guid: '3459ec28-1ef3-45d5-914a-81716539984a',
//   url_api_mb: 'http://localhost:5002',
//   url_api_cm: 'http://gmt-apicm.easternsun.vn',
//   url_icon_chat: 'https://gmt-static.easternsun.vn',
//   link_type_icon: '/img/fileicon/',
//   url_signalr_mb: 'https://gmt-cb.easternsun.vn/chathub',
//   url_hubConnection: 'https://gmt-chat.essolution.net/',
//   url_ProFollow: 'http://gmt-mss.easternsun.vn/',
//   grant_type_login: 'password',
//   grant_type_refresh: 'refresh_token',
//   client_id: 'mobileapp',
//   client_secret: 'secret',
//   scope: 'app_read_api',
//   configHeaders: {
//     Accept: 'application/json',
//     'Content-Type': 'multipart/form-data',
//   },
//   MB_PAGES: 'MB_PAGES',
//   url_Image_chat:
//     'http://localhost:5002/api/Messenger/Messenger_DownloadAttachment_Thumb/',
//   url_View_Image:
//     'http://localhost:5002/api/Messenger/Messenger_DownloadAttachment/',
//   url_View_Thumb_Photo: 'http://localhost:5002/api/Items/GetItems_Thumb/',
//   url_Document_Download: 'http://localhost:5002/api/Document/Download/',
//   url_Task_ViewImg:
//     'http://localhost:5002/api/Tasks/Task_DownloadAttachment_Thumb/',
//   url_Task_DownloadImg:
//     'http://localhost:5002/api/Tasks/Task_DownloadAttachment/',
//   url_Task_Downloadfile: 'http://localhost:5002/api/Tasks/DownloadAttachment/',
//   url_Attachment_Download: 'http://localhost:5002/api/Attachments/Download/',
//   url_Download_mail: 'http://gmt-mail.easternsun.vn/',
//   url_pro_image: 'https://gmt-pro.easternsun.vn',
// };
//BGD01
//HCNS-2103-031
