import {combineReducers} from 'redux';
import {LocalStorage} from '@utils';

// Our custom reducers
// We need to import each one here and add them to the combiner at the bottom
import router from '@redux/router/reducer';
import user from '@redux/user/reducer';
import TM from '@redux/TM/reducer';
import MS from '@redux/MS/reducer';
import device from '@redux/device/reducer';
import notification from '@redux/notification/reducer';
// Combine all
const appReducer = combineReducers({
  router,
  user,
  device,
  TM,
  MS,
  notification,
});

// Setup root reducer
const rootReducer = (state, action) => {
  let newState = action.type === 'RESET' ? undefined : state;
  newState = action.type === 'USER_LOGOUT' ? {} : newState;
  if (__DEV__ && action.type === 'USER_LOGOUT') {
    const keys = [
      'user',
      'devices',
      'groups',
      'config',
      'TM',
      'MS',
      'notification',
    ];
    keys.forEach((value, index, array) => {
      LocalStorage.remove(value);
    });
  }
  return appReducer(newState, action);
};

export default rootReducer;
