/**
 * User Reducer
 */

// Set initial state
const initialState = {};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case 'DEVICES': {
      return {
        ...state, devices: action.devices
      };
    }

    case 'GROUPS': {
      return {
        ...state, groups: action.groups
      };
    }

    default:
      return state;
  }
}