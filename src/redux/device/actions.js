import {API} from '@network';
import {AccessTokenManager} from '@bussiness';

export function getDevices() {
  return async (dispatch) => {
    return API.getDevices()
      .then((res) => {
        if (API.hasError(res.data)) {
          return Promise.reject(res.data);
        }
        return dispatch({
          type: 'DEVICES',
          devices: res.data.devices || [],
        });
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  };
}

export function getGroups() {
  return async (dispatch) => {
    return API.getGroups()
      .then((res) => {
        if (API.hasError(res.data)) {
          return Promise.reject(res.data);
        }
        return dispatch({
          type: 'GROUPS',
          groups: res.data.groups || [],
        });
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  };
}
