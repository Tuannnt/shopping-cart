/**
 * User Actions
 */
import { API } from '@network';
import { AccessTokenManager } from '@bussiness';

export function getProfile() {
  return async (dispatch) => {
    console.log('_______________________________________________')
    return API.getProfile()
      .then((res) => {
        if (API.hasError(res.data)) {
          return Promise.reject(res.data);
        }
        return dispatch({
          type: 'USER_PROFILE',
          account: res.data.account,
        });
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  };
}

export function logout() {
  AccessTokenManager.clear();
  return async (dispatch) => {
    API.logout();
    return dispatch({
      type: 'USER_LOGOUT'
    });
  };
}

export function applications(data) {
  return async (dispatch) => {
    return dispatch({
      type: 'USER_APPLICATIONS',
      data: data
    });
  }
}
