/**
 * User Reducer
 */

// Set initial state
const initialState = {};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case 'USER_PROFILE': {
      return {
        ...state, account: action.account
      };
    }
    case 'USER_APPLICATIONS':{
      return{
        ...state,data: action.data,
      }
    }

    default:
      return state;
  }
}