/**
 * User Reducer
 */

// Set initial state
const initialState = {};

export default function TMReducer(state = initialState, action) {
  switch (action.type) {
    case 'ADD_ITEM_TM': {
      return {
        ...state, paraTM: action.data
      };
    }
    case 'ADD_TEMPORARY_TM': {
      return {
        ...state, paraTemporary: action.data
      };
    }
    default:
      return state;
  }
}