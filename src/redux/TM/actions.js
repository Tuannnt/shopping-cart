/**
 * User Actions
 */
export function addTM(para) {
  return (dispatch) => {
    return dispatch({
      type: 'ADD_ITEM_TM',
      data: para,
    });
  }
}
export function addTempTM(para) {
  return (dispatch) => {
    return dispatch({
      type: 'ADD_TEMPORARY_TM',
      data: para,
    });
  }
}