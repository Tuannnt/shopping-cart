/**
 * Actions
 */
export function GetCountNotification(para) {
  return dispatch => {
    return dispatch({
      type: 'GET_COUNT_NOTIFICATION',
      data: para,
    });
  };
}
