/**
 * User Reducer
 */

// Set initial state
const initialState = {
  CountNotification: 0,
};

export default function NotiReducer(state = initialState, action) {
  switch (action.type) {
    case 'GET_COUNT_NOTIFICATION': {
      return {
        ...state,
        CountNotification: action.data,
      };
    }

    default:
      return state;
  }
}
