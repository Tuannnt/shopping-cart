export function backtolist(data) {
  return (dispatch) => {
    return dispatch({
      type: 'BACKTOLIST',
      data: data
    });
  }
}
