/**
 * MS Reducer
 */

// Set initial state
const initialState = {};

export default function MSReducer(state = initialState, action) {
  switch (action.type) {
    case 'BACKTOLIST':{
      return{
        ...state,data: action.data,
      }
    }
    default:
      return state;
  }
}