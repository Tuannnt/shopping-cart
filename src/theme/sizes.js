/**
 * App Theme - Sizes
 */

import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');
const screenHeight = width < height ? height : width;
const screenWidth = width < height ? width : height;

export default {
  navbarHeight: (Platform.OS === 'ios') ? 64 : 54,
  statusBarHeight: (Platform.OS === 'ios') ? 16 : 0,
  tabbarHeight: 51,

  padding: 16,
  paddingSmall: 8,
  paddingXSmall: 4,

  margin: 16,
  marginLarge: 24,
  marginSmall: 8,
  marginXSmall: 4,

  textSmall: 12,
  textMedium: 14,
  textLarge: 20,

  borderRadius: 2,
  borderWith: 1,

  itemHeight: 40,

  textTitle: 16,
  textInputHeight: 40,
  buttonHeight: 40,
  // Begin custom trongtq@esvn.com.vn
  Home_SizeIcon_Top: 40,
  Text_default: 14,
  Label_default: 14,
  Title_default: 17,
  TabBar_SizeIcon: 20,
  Login_Size_Icon: 15,
  Text_Small:12,
  Text_Message:15,
  TextInput_Error:12,
  Text_link:14
  //End custom trongtq@esvn.com.vn
};