/**
 * App Styles
 */
import Colors from './colors';
import Fonts from './fonts';
import Sizes from './sizes';
import { Platform, Dimensions } from 'react-native';
import { AppColors } from './index';
import fonts from './fonts';
import colors from './colors';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const CheckScreenIos = (W, H) => {
  let S = H - W;
  if (S >= 437) {
    return 15
  }
  return 0
}
const appStylesBase = {
  // Give me padding
  padding: {
    paddingVertical: Sizes.padding,
    paddingHorizontal: Sizes.padding,
  },
  paddingHorizontal: {
    paddingHorizontal: Sizes.padding,
  },
  paddingVertical: {
    paddingVertical: Sizes.padding,
  },

  // Give me margin
  margin: {
    margin: Sizes.margin,
  },
  marginHorizontal: {
    marginLeft: Sizes.margin,
    marginRight: Sizes.margin,
  },
  marginVertical: {
    marginTop: Sizes.margin,
    marginBottom: Sizes.margin,
  },
};

export default {
  // Import app styles base
  ...appStylesBase,
  appContainer: {
    backgroundColor: Colors.white,
    height: '100%',
  },
  AndroidSafeArea: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: Platform.OS == 'ios' ? CheckScreenIos(SCREEN_WIDTH, SCREEN_HEIGHT) : 0
  },
  baseText: {
    fontFamily: Fonts.base.family,
    fontSize: Fonts.base.size,
  },

  boldItalicText: {
    fontFamily: Fonts.boldItalic.family,
    fontSize: Fonts.base.size,
  },

  boldText: {
    fontFamily: Fonts.bold.family,
    fontSize: Fonts.base.size,
  },

  semiboldText: {
    fontFamily: Fonts.base.family,
    fontSize: Fonts.base.size,
    fontWeight: '500',
  },

  italicText: {
    fontFamily: Fonts.italic.family,
    fontSize: Fonts.base.size,
  },

  blackText: {
    fontFamily: Fonts.black.family,
    fontSize: Fonts.base.size,
  },

  lightText: {
    fontFamily: Fonts.light.family,
    fontSize: Fonts.base.size,
  },

  // navbar
  navbarProps: {
    hideNavBar: false,
  },

  headerProps: {
    backgroundColor: Colors.white,
  },
  headerImage: {
    backgroundColor: '#000000',
  },
  headerStyle: {
    color: AppColors.textGreen,
    fontSize: Sizes.textTitle,
    fontFamily: Fonts.base.family,
  },

  textInput: {
    fontFamily: Fonts.base.family,
    fontSize: Fonts.base.size,
    color: Colors.textPrimary,
  },

  textInputProps: {
    autoCapitalize: 'none',
    underlineColorAndroid: 'transparent',
    placeholderTextColor: Colors.textSecondary,
    multiline: false,
  },

  textInputGrayBorder: {
    borderWidth: 0.75,
    borderColor: Colors.lightGray,
  },

  statusbarProps: {
    hidden: false,
    translucent: false,
    animated: true,
    backgroundColor: AppColors.white,
    barStyle: 'dark-content',
  },

  h1: {
    fontFamily: Fonts.h1.family,
    fontSize: Fonts.h1.size,
    color: AppColors.textPrimary,
  },
  h2: {
    fontFamily: Fonts.h2.family,
    fontSize: Fonts.h2.size,
    color: AppColors.textPrimary,
  },
  h3: {
    fontFamily: Fonts.h3.family,
    fontSize: Fonts.h3.size,
    color: AppColors.textPrimary,
  },
  h4: {
    fontFamily: Fonts.h4.family,
    fontSize: Fonts.h4.size,
    color: Colors.textPrimary,
  },
  h5: {
    fontFamily: Fonts.h5.family,
    fontSize: Fonts.h5.size,
    color: Colors.textPrimary,
  },
  //------------------------------------------------ Begin custom trongtq@esvn.com.vn
  // Default
  container: {
    position: 'relative',
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.white,
  },
  containerCentered: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  FormInput: {
    flexDirection: 'row',
    borderWidth: 1,
    padding: 10,
    borderColor: AppColors.gray,
    borderRadius: 5,
    // shadowColor: "#000",
    //     shadowOffset: {
    //         width: 0,
    //         height:1,
    //     },
    //     shadowOpacity: 0.2,
    //     elevation:1,
  },
  FormInput_custom: {
    flexDirection: 'row',
    borderWidth: 1,
    paddingVertical: 7,
    paddingHorizontal: 5,
    borderColor: AppColors.gray,
    borderRadius: 5,
    // shadowColor: "#000",
    //     shadowOffset: {
    //         width: 0,
    //         height:1,
    //     },
    //     shadowOpacity: 0.2,
    //     elevation:1,
  },
  // Text Styles
  Textsmall: {
    fontFamily: Fonts.Text_Small.family,
    fontSize: Fonts.Text_Small.size,
  },
  TextInputError: {
    fontFamily: Fonts.TextInput_Error.family,
    fontSize: Fonts.TextInput_Error.size,
    color: colors.ColorDelete,
  },
  TextMessage: {
    fontFamily: Fonts.Text_Message.family,
    fontSize: Fonts.Text_Message.size,
  },
  Textdefault: {
    fontFamily: Fonts.Text_default.family,
    fontSize: Fonts.Text_default.size,
  },
  TextTable: {
    fontFamily: Fonts.Text_default.family,
    fontSize: Fonts.Text_default.size,
    margin: 5,
    padding: 5,
  },
  Labeldefault: {
    fontFamily: Fonts.Label_default.family,
    fontSize: Fonts.Label_default.size,
  },
  Titledefault: {
    fontFamily: Fonts.Title_default.family,
    fontSize: Fonts.Title_default.size,
    fontWeight: 'bold',
  },
  TitleMSS: {
    fontFamily: Fonts.Title_default.family,
    fontSize: 14,
    fontWeight: 'bold',
  },
  TextLink: {
    fontFamily: Fonts.Text_Link.family,
    fontSize: Fonts.Text_Link.size,
    color: Colors.ColorLink,
  },
  // Aligning items
  leftAligned: {
    alignItems: 'flex-start',
  },
  centerAligned: {
    alignItems: 'center',
  },
  rightAligned: {
    alignItems: 'flex-end',
  },
  //Form Search
  FormSearchBar: {
    backgroundColor: 'white',
    height: 50,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0, //no effect
    shadowColor: 'white', //no effect
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
    paddingTop: 5,
  },
  //custom backgroundColor và color theo màu mặc định
  backgroundColordefault: {
    backgroundColor: Colors.Maincolor,
  },
  Colorsdefault: {
    color: colors.Maincolor,
  },
  //trang chủ
  Home_container_Top: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20,
    width: '90%',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: '#fff',
    marginTop: 40,
  },
  Home_container_Detail: {
    flex: 1,
    backgroundColor: '#fff',
    width: SCREEN_WIDTH,
  },
  Home_Container_Item_top: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
  },
  Home_Container_Item_Detail: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 18,
  },
  Home_ImageBackground: {
    width: '100%',
    height: '100%',
  },
  Home_Logo_Icon: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-between',
    padding: 5,
  },
  Home_IconFName: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  Home_Badge: {
    position: 'absolute',
    right: 27,
    top: 17,
  },
  StyleClickitem: {
    color: Colors.ColorClickitem,
  },
  //menu góc trái trang chủ
  Drawer_HeaderContainer: {
    padding: 20,
    borderBottomWidth: 0.5,
    borderColor: AppColors.gray,
    flexDirection: 'row',
  },
  // list button bottom
  TabNavigator_Container: {
    height: 60,
    backgroundColor: '#ffffff'
  },
  TabNavigator_Item: {
    marginTop: 10,
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  TabNavigator_Icon: {
    fontWeight: '600',
    fontSize: 20,
    color: 'black',
  },
  TabNavigator_IconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: Colors.Maincolor,
  },
  //tabbar
  TabbarTop_left: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    height: 50,
    paddingLeft: 5,
  },
  TabbarTop_Right: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  TabbarTop_Center: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  StyleTabvarBottom: {
    height: 50,
    borderTopWidth: 0.5,
    borderTopColor: Colors.BorderColorTabbarBottom,
    bottom: 0,
  },
  StyleBadgeTabbar: {
    position: 'absolute',
    right: 10,
    top: 17,
  },
  //
  //Message
  ImageStyle: {
    width: 100,
    height: 100,
    borderRadius: 15,
  },
  //task
  //lọc theo lịch
  StyleDateTop: {
    color: AppColors.gray,
    borderRadius: 50,
    padding: 3,
  },
  StyleContentDateTop: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  //Loading
  Loading: {
    position: 'absolute',
    zIndex: 9999,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },

  //table
  table_th: {
    borderWidth: 0.5,
    borderColor: AppColors.gray,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E6E6E6',
  },
  table_td: {
    borderWidth: 0.5,
    borderColor: AppColors.gray,
    padding: 5,
    // alignItems: 'center',
    justifyContent: 'center',
  },
  table_td_custom: {
    borderWidth: 0.5,
    borderColor: AppColors.gray,
    padding: 2,
    // alignItems: 'center',
    justifyContent: 'center',
  },
  table_foot: {
    borderWidth: 0.5,
    borderColor: AppColors.gray,
    padding: 5,
    // alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E6E6E6',
  },
  //End custom trongtq@esvn.com.vn
  FormInputPicker: {
    borderWidth: 1,
    borderColor: AppColors.gray,
    padding: 0,
    marginRight: 2,
    borderRadius: 5,
  },
  FormInputPickerSmall: {
    borderWidth: 1,
    borderColor: AppColors.gray,
    padding: 0,
    marginRight: 2,
  },
};
