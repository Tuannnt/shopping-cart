/**
 * App Theme - Colors
 */
const colors = {
  transparent: '#00000000',
  white: '#FFFFFF',
  black: '#111111',
  green: '#3BA753',
  greenDark: '#078137',
  blue: '#428bca',
  red: '#D0021B',
  gray: '#B5B7B8',
  lightGray: '#BCBCBC',
  textGreen: '#007D2A',
  textLighgreen: '#49B865',
  textLight: '#FFFFFF',
  textGray: '#979797',
  textDark: '#111111',
  textPrimary: '#111111',
  textSecondary: '#9B9B9B',
  iconGreen: '#518500',
  blueFacebook: '#3B5A94',
  primary: '#3BA753',
  primaryDark: '#078137',
  secondary: '#D0021B',
  // ------------------------------------------Begin custom trongtq@esvn.com.vn
  //Màu chủ đạo
  Maincolor: '#EE0000',
  // '#f6b801',
  //Màu của trạng thái Đã Duyệt
  AcceptColor: '#00AA00',
  //Màu của trạng thái Chờ duyệt
  PendingColor: '#FF6600',
  //Màu của trạng thái Không duyệt
  UntreatedColor: '#EE0000',
  //icon mũi tên
  Iconcolor: '#888888',
  IconcolorTabbar: '#888888',
  BorderColorTabbarBottom: '#CCCCCC',
  TextColorMSS: '#444444',
  ColorClickitem: '#3B5A94',
  //chờ giao
  StatusTask_CG: '#9e9e9e',
  //chờ xử lý
  StatusTask_CXL: '#0094f2',
  //Đang thực hiện
  StatusTask_DTH: '#078415',
  //hoàn thành
  StatusTask_HT: '#24c79f',
  //theo dõi
  StatusTask_TD: '#613705',
  //đã giao
  StatusTask_DG: '#ffffff',
  //công việc lưu
  StatusTask_CVL: '#8a8c8d',
  //chờ người khác
  StatusTask_CNK: '#e58b20',
  //hoãn lại
  StatusTask_HL: '#ff5722',
  //quá hạn
  StatusTask_QH: '#eb3b48',

  ColorAdd: '#0066FF',
  ColorEdit: '#00BB00',
  ColorDelete: '#FF0000',
  ColorDetail: '#0033FF',
  ColorDowload: '#00BB00',
  ColorPrimary: '#777777',

  ColorInput: '#999999',
  ColorLink: '#0066FF',
  ColorButtonSubmit: '#045A70',
  //-------------------------------------------End custom trongtq@esvn.com.vn
};

export default {
  ...colors,
};
