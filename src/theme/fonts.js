/**
 * App Theme - Fonts
 */
import { Platform } from 'react-native';
import Sizes from './sizes';
function lineHeight(fontSize) {
  const multiplier = (fontSize > 20) ? 0.1 : 0.33;
  return parseInt(fontSize + (fontSize * multiplier), 10);
}

const base = {
  size: 14,
  family: 'Lato-Regular',
};

const boldItalic = {
  size: 26,
  family: 'Lato-BoldItalic',
}

const italic = {
  size: 14,
  family: 'Lato-Italic',
}

const bold = {
  size: 14,
  family: 'Lato-Bold',
};

const black = {
  size: 14,
  family: 'Lato-Black',
};

const light = {
  size: 14,
  family: 'Lato-Light',
};
//Begin custom trongtq@esvn.com.vn
const Text_default= {
  size:Sizes.Text_default,
  family: 'Lato-Regular',
};
const Label_default={
  size:Sizes.Label_default,
  family: 'Lato-Bold',
};
const Title_default={
  size:Sizes.Title_default,
  family: 'Lato-Regular',
};
const Text_Small={
  size:Sizes.Text_Small,
  family: 'Lato-Regular',
};
const Text_Message={
  size:Sizes.Text_Message,
  family: 'Lato-Regular',
};
const TextInput_Error={
  size:Sizes.TextInput_Error,
  family: 'Lato-Italic',
}
const Text_Link={
  size:Sizes.Text_link,
  family: 'Lato-Regular',
}
//end custom trongtq@esvn,.com.vn
export default {
  base: { ...base },
  bold: { ...bold },
  italic: { ...italic },
  boldItalic: {...boldItalic},
  black: { ...black },
  light: { ...light },
  h1: { ...bold, size: base.size * 1.75 },
  h2: { ...bold, size: base.size * 1.5 },
  h3: { ...bold, size: base.size * 1.25 },
  h4: { ...base, size: base.size * 1.1 },
  h5: { ...base },
  // lineHeight: (fontSize) => lineHeight(fontSize),
  //Begin custom trongtq@esvn.com.vn
  Text_default: { ...Text_default },
  Label_default: { ...Label_default },
  Title_default: { ...Title_default },
  Text_Small: { ...Text_Small },
  Text_Message:{...Text_Message},
  TextInput_Error:{...TextInput_Error},
  Text_Link:{...Text_Link},
  //end custom trongtq@esvn,.com.vn
};
