/**
 * Name: Float Button Manager
 * Description: A manager to show/hide float button
 */
'use strict'

module.exports = {
  _progress: null,

  register(progress) {
    this._progress = progress;
  },

  unregister() {
    this._progress = null;
  },

  show() {
    if (this._progress === null) {
      return;
    }

    this._progress.show();
  },

  hide() {
    if (this._progress !== null) {
      this._progress.hide();
    }
  },
}
