import React, {Component} from 'react';
import {Overlay} from 'react-native-elements';
import {ActivityIndicator} from 'react-native';
import {AppColors} from '@theme';

export default class Progress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    }
  }

  show() {
    this.setState({visible: true});
  }

  hide() {
    this.setState({visible: false});
  }

  render() {
    return (
      <Overlay isVisible={this.state.visible}
               width={100}
               height={100}
               overlayStyle={{alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator size="large"/>
      </Overlay>
    );
  }
}