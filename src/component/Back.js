import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {AppColors} from '@theme';

class Back extends Component {

  render() {
    return (
      <TouchableOpacity style={{height: '100%', width: 24, alignItems: 'center', justifyContent: 'center'}}>
        <Icon name='ios-arrow-back' size={28} color={AppColors.greenDark}/>
      </TouchableOpacity>
    );
  }
}

export default Back;
