/*
 * https://github.com/facebook/react-native
 * Copyright cuiyueshuai
 * @author cuiyueshuai<850705402@qq.com>
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {LayoutAnimation, SectionList, TouchableOpacity,} from 'react-native';
import _ from 'lodash';

class ExpandableSectionList extends Component {
  constructor(props) {
    super(props);
    this.layoutStore = [];
    this.state = {
      openMap: props.openMap || {},
    }
  }

  static propTypes = {
    sections: PropTypes.array.isRequired,
    sectionKey: PropTypes.func.isRequired,
    keyExtractor: PropTypes.func,
    refreshing: PropTypes.bool,
    onRefresh: PropTypes.func,
    renderItem: PropTypes.func,
    renderSectionHeader: PropTypes.func,
    renderSectionFooter: PropTypes.func,
    stickySectionHeadersEnabled: PropTypes.bool,
    openMap: PropTypes.objectOf(PropTypes.bool),
  };

  scrollToLocation = (params) => this.sectionList.scrollToLocation(params);
  recordInteraction = () => this.sectionList.recordInteraction();
  flashScrollIndicators = () => this.sectionList.flashScrollIndicators();

  setSectionState = (section, state) => {
    const openMap = this.state.openMap;
    openMap[this.props.sectionKey(section)] = state;
    this.setState({openMap});
    LayoutAnimation.easeInEaseOut();
  }

  toggleSectionState = (section) => {
    const openMap = this.state.openMap;
    openMap[this.props.sectionKey(section)] = !openMap[this.props.sectionKey(section)];
    this.setState({openMap});
    LayoutAnimation.easeInEaseOut();
  }

  _onPress = ({section}) => {
    this.toggleSectionState(section);
  };

  _renderSectionHeader = (section) => {
    const {
      renderSectionHeader,
    } = this.props;

    return (
      <TouchableOpacity onPress={() => this._onPress(section)}>
        {renderSectionHeader ? renderSectionHeader(section) : null}
      </TouchableOpacity>
    );
  };

  _itemLayout = (e) => {
    const target = e.nativeEvent.target;
    if (this.layoutStore.filter(item => item.target === target)[0]) {
      this.layoutStore = this.layoutStore.map((item, index) => {
        if (item.target === target) return e.nativeEvent;
        return item;
      });
    } else {
      this.layoutStore.push(e.nativeEvent);
      this.layoutStore.length === this.props.sections.length &&
      this.layoutStore.sort((v1, v2) => v1.target - v2.target);
    }
  };

  render() {

    const {
      sections
    } = this.props;

    const newSections = _.transform(sections, (result, section) => {
      const collapse = this.state.openMap[this.props.sectionKey(section)];
      const newSection = {...section, data: collapse ? [] : section.data};
      result.push(newSection);
    }, []);

    return (
      <SectionList
        keyExtractor={this.props.keyExtractor}
        {...this.props}
        sections={newSections}
        getItemLayout={this._getItemLayout}
        ref={instance => this.sectionList = instance}
        renderSectionHeader={section => this._renderSectionHeader(section)}
      />
    );
  }
}

export default ExpandableSectionList;