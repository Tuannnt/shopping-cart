import React, {Component} from 'react';
import {Image, View} from 'react-native';

export default class FullWidthImage extends Component {
  constructor() {
    super();

    this.state = {
      width: 0,
      height: 0
    };
  }

  _onLayout(event) {
    const containerWidth = event.nativeEvent.layout.width;

    if (this.props.ratio) {
      this.setState({
        width: containerWidth,
        height: containerWidth * this.props.ratio
      });
    } else if (this.props.source) {
      if (this.props.source.uri)
        Image.getSize(this.props.source.uri, (width, height) => {
          this.setState({
            width: containerWidth,
            height: containerWidth * height / width
          });
        });
      else {
        const size = Image.resolveAssetSource(this.props.source)
        this.setState({
          width: containerWidth,
          height: containerWidth * size.height / size.width
        });
      }
    }
  }

  render() {
    return (
      <View onLayout={this._onLayout.bind(this)}>
        <Image
          {...this.props}
          style={{
            width: this.state.width,
            height: this.state.height
          }}/>
      </View>
    );
  }
}