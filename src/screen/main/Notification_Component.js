import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, SearchBar} from 'react-native-elements';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  View,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../theme/fonts';
import TabNavigator from 'react-native-tab-navigator';
import {API, API_HR} from '@network';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
} from 'native-base';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../component/TabBar';
import {TouchableOpacity} from 'react-native-gesture-handler';

class Notification_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LocationId:
        props.LocationId !== null && props.LocationId !== ''
          ? props.LocationId
          : 'NV',
      model: {},
      list: [],
      isFetching: false,
      staticParam: {
        CurrentPage: 1,
        Length: 15,
        QueryOrderBy: 'CreatedDate DESC',
        StartDate: '2020-02-01T02:13:13.779Z',
        EndDate: '2020-02-29T02:13:13.779Z',
      },
    };
  }

  GetAll = () => {
    this.setState({refreshing: true});
    API.GetAlertsByUser(this.state.staticParam)
      .then(res => {
        console.log(res);
        this.state.list =
          res.data.data != 'null'
            ? this.state.list.concat(JSON.parse(res.data.data))
            : [];
        this.setState({list: this.state.list});
        this.setState({refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };

  componentDidMount(): void {
    this.GetAll();
  }

  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có tin nhắn</Text>
      </View>
    );
  };
  updateSearch = text => {
    this.state.staticParam.Search = text;
    this.setState({
      list: [],
      staticParam: {
        CurrentPage: 1,
        Length: this.state.staticParam.Length,
        QueryOrderBy: this.state.staticParam.QueryOrderBy,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
      },
    });
    console.log('keyword: ' + this.state.staticParam.Search);
    this.GetAll();
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar title={'Tin nhắn'} OffBack={true} />
        <View style={{flex: 9}}>
          <FlatList
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            //ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={item => item.AlertGuid}
            data={this.state.list}
            renderItem={({item, index}) => (
              <ListItem
                avatar
                style={{
                  width: '100%',
                  height: 80,
                  fontSize: 13,
                  fontFamily: Fonts.base.family,
                  backgroundColor: '#fff',
                }}
                onPress={() => this.openurl(item)}>
                <Left>
                  <Thumbnail
                    style={{width: 40, height: 40}}
                    source={API_HR.GetPicApplyLeaves(item.SenderGuid)}
                  />
                </Left>
                <Body>
                  {item.IsRead == false ? (
                    <View style={{flexDirection: 'row', paddingRight: 20}}>
                      <View style={{flex: 12}}>
                        <Text
                          style={(styles.subtitleStyle, {fontWeight: 'bold'})}>
                          {item.Title}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Icon
                          iconStyle={{color: '#5882FA'}}
                          name={'controller-record'}
                          type="entypo"
                          size={15}
                        />
                      </View>
                    </View>
                  ) : (
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <Text
                          style={(styles.subtitleStyle, {fontWeight: 'bold'})}>
                          {item.Title}
                        </Text>
                      </View>
                    </View>
                  )}
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 3}}>
                      <Text style={styles.subtitleStyle}>
                        {item.Description.substring(0, 50)}
                      </Text>
                    </View>
                    <View style={{flex: 1}}>
                      <Text style={styles.subtitleStyle}>
                        {this.customDate(item.CreatedDate)}
                      </Text>
                    </View>
                  </View>
                </Body>
              </ListItem>
            )}
            onEndReachedThreshold={0.5}
            onEndReached={({distanceFromEnd}) => {
              this.nextPage();
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
        </View>
      </View>
    );
  }

  nextPage() {
    this.state.staticParam.CurrentPage++;
    this.setState({
      staticParam: {
        CurrentPage: this.state.staticParam.CurrentPage,
        Length: this.state.staticParam.Length,
        QueryOrderBy: this.state.staticParam.QueryOrderBy,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
      },
    });
    this.GetAll();
  }
  onPressHome() {
    Actions.app();
  }
  //Xem
  openurl = item => {
    if (item.MobileUrl != null && item.RecordGuid != '') {
      var module = item.MobileUrl.split('=')[1].split('&')[0];
      if (!module || module === 'null') {
        Toast.showWithGravity(
          'Chức năng này không hỗ trợ',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
      Actions[module]({RecordGuid: item.RecordGuid});
      API.UpdateIsRead(item.AlertGuid)
        .then(res => {})
        .catch(error => {});
    }
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  renderLeftMenu = () => {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'chevron-thin-left'}
          type="entypo"
          size={25}
          onPress={() => Actions.app()}
        />
      </View>
    );
  };
  renderRightMenu = () => {
    return (
      <View>
        <Icon
          style={{padding: 5, margin: 0}}
          name="plus-square"
          size={20}
          color="#fff"
        />
      </View>
    );
  };
}

const styles = StyleSheet.create({
  itemName: {
    fontSize: 13,
    color: '#231F20',
    fontWeight: '600',
    textAlign: 'center',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  titleStyle: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  searchcontainer: {
    backgroundColor: 'white',
    height: 50,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0, //no effect
    shadowColor: 'white', //no effect
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  IsReadT: {
    backgroundColor: '#FFFFFF',
  },
  IsReadF: {
    backgroundColor: '#C9DEFC',
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification_Component);
