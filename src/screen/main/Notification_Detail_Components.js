import React, {Component} from 'react';
import {
  Dimensions,
  PermissionsAndroid,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  View,
  Animated,
  ScrollView,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {ActionConst, Actions} from 'react-native-router-flux';
import * as UserActions from '@redux/user/actions';
import {Divider, Icon, SearchBar} from 'react-native-elements';
import HTML from 'react-native-render-html';
import {FuncCommon} from '@utils';
import {TabBar_Title, CustomView} from '@Component';
import {AppStyles, AppSizes, AppColors} from '@theme';
import {listCombobox, controller} from '@Home';
import {LoadingComponent} from '@Component';
import {WebView} from 'react-native-webview';
import configApp from '../../configApp';
import RNFetchBlob from 'rn-fetch-blob';
import {Container} from 'native-base';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const TimeOut = 300;
class Notification_Detail_Components extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      DataView: null,
      AnnouncementGuid: props.RecordGuid,
      ListAtt: [],
    };
    this.state.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    this.GetItem();
  }
  GetItem = () => {
    controller.GetItems_Announcements(this.state.AnnouncementGuid, rs => {
      if (rs !== null) {
        var obj = {
          ModuleId: 19,
          ParentGuid: null,
          RecordGuid: this.state.AnnouncementGuid,
          Keyword: '',
        };
        controller.GetAttList(obj, res => {
          this.setState({ListAtt: res});
        });
        this.setState({DataView: rs, loading: false});
      }
    });
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor="#fff" />
    ) : (
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <View style={[{flexDirection: 'row'}]}>
            <TouchableOpacity
              style={[
                {
                  width: DRIVER.width / 2,
                  padding: 10,
                  alignItems: 'flex-start',
                },
              ]}
              onPress={() => this.callBack()}>
              <Icon
                name={'arrow-left'}
                type="feather"
                size={AppSizes.TabBar_SizeIcon}
                color={AppColors.black}
                iconStyle={{paddingLeft: 5, paddingTop: 10}}
              />
            </TouchableOpacity>
          </View>
          <View style={[{paddingLeft: 15}]} onPress={() => this.callBack()}>
            <Text style={[{fontWeight: 'bold', fontSize: 26}]}>
              {this.state.DataView.Title}
            </Text>
          </View>
          <View style={[{flexDirection: 'row'}]}>
            <TouchableOpacity
              style={[{flex: 1, padding: 10, flexDirection: 'row'}]}
              onPress={() => this.CallbackFormAttachment()}>
              <Icon
                name="attachment"
                type="entypo"
                color={
                  this.state.ListAtt.length > 0
                    ? AppColors.blue
                    : AppColors.gray
                }
                size={16}
              />
              <View style={[{justifyContent: 'center', paddingLeft: 10}]}>
                <Text
                  style={[
                    AppStyles.Labeldefault,
                    {
                      color:
                        this.state.ListAtt.length > 0
                          ? AppColors.blue
                          : AppColors.gray,
                    },
                  ]}>
                  Đính kèm
                </Text>
              </View>
            </TouchableOpacity>
            <View
              style={[
                {
                  padding: 10,
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                },
              ]}>
              <Text style={[AppStyles.Textdefault, {color: AppColors.gray}]}>
                {FuncCommon.ConDate(this.state.DataView.CreatedDate, 0)}
              </Text>
            </View>
          </View>
          <Divider />
          <View style={{flex: 1, padding: 20}}>{this.CustomViewHTML()}</View>
          <CustomView eOpen={this.openCustom}>
            <View
              style={{
                height: 200,
                width: DRIVER.width - 50,
                padding: 10,
              }}>
              <View style={{alignItems: 'center', padding: 10}}>
                <Text style={AppStyles.Labeldefault}>
                  Danh sách file đính kèm
                </Text>
              </View>
              <Divider />
              <ScrollView>
                {this.state.ListAtt.length > 0
                  ? this.state.ListAtt.map((para, i) => (
                      <TouchableOpacity
                        key={i}
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          padding: 5,
                        }}
                        onPress={() => this.OnDownload(para)}>
                        <View
                          style={[
                            AppStyles.containerCentered,
                            {marginRight: 10},
                          ]}>
                          <Image
                            style={{
                              width: 50,
                              height: 50,
                              borderRadius: 10,
                            }}
                            source={{
                              uri: this.state.FileAttackments.renderImage(
                                para.FileName,
                              ),
                            }}
                          />
                        </View>
                        <View style={{flex: 1}}>
                          <Text
                            style={[AppStyles.Textdefault, {color: '#1769ff'}]}>
                            {para.FileName}
                          </Text>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {color: AppColors.gray},
                            ]}>
                            size:
                            {this.customFloat(para.FileSize / 1024)} MB
                          </Text>
                        </View>
                        <View style={{justifyContent: 'center'}}>
                          <Icon
                            name={'download'}
                            type={'feather'}
                            iconStyle={{color: '#1769ff'}}
                          />
                        </View>
                      </TouchableOpacity>
                    ))
                  : null}
              </ScrollView>
            </View>
          </CustomView>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  CustomViewHTML = () => {
    return (
      <ScrollView>
        <TouchableHighlight>
          <HTML
            source={{html: this.state.DataView.Contents}}
            contentWidth={DRIVER.width}
          />
        </TouchableHighlight>
      </ScrollView>
    );
  };

  _openCustom() {}
  openCustom = d => {
    this._openCustom = d;
  };
  CallbackFormAttachment = () => {
    var obj = {
      ModuleId: '19',
      RecordGuid: this.props.RecordGuid || this.props.RowGuid,
      notEdit: true,
    };
    Actions.attachmentComponent(obj);
    // this._openCustom();
  };
  callBack() {
    this.setState({EventBoxIcon: true});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'notification',
      ActionTime: new Date().getTime(),
    });
  }
  OnDownload(attachObject) {
    Alert.alert(
      //title
      'Thông báo',
      //body
      'Bạn muốn tải file đính kèm về máy',
      [
        {text: 'Tải về', onPress: () => this.DownloadFile(attachObject)},
        {text: 'Huỷ', onPress: () => console.log('No Pressed')},
      ],
      {cancelable: true},
    );
  }
  DownloadFile(attachObject) {
    let dirs = RNFetchBlob.fs.dirs;
    if (Platform.OS !== 'ios') {
      RNFetchBlob.config({
        addAndroidDownloads: {
          useDownloadManager: true, // <-- this is the only thing required
          // Optional, override notification setting (default to true)
          notification: true,
          // Optional, but recommended since android DownloadManager will fail when
          // the url does not contains a file extension, by default the mime type will be text/plain
          mime: '/',
          description: 'File downloaded by download manager.',
          //title: new Date().toLocaleString() + ' - test.xlsx',
          //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
          path: 'file://' + dirs.DownloadDir + '/' + attachObject.FileName, //using for android
        },
      })
        .fetch(
          'GET',
          configApp.url_Download_Notification + attachObject.AttachmentGuid,
          {},
        )
        .then(resp => {
          Alert.alert(
            //title
            'Thông báo',
            //body
            'Tải thành công',
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
          // the path of downloaded file
          resp.path();
        });
    } else {
      RNFetchBlob.config({
        path: dirs.DocumentDir + '/' + attachObject.FileName,
        addAndroidDownloads: {
          useDownloadManager: true, // <-- this is the only thing required
          // Optional, override notification setting (default to true)
          notification: true,
          IOSDownloadTask: true,
          mime: '/',
          description: 'File downloaded by download manager.',
          title: new Date().toLocaleString() + '-' + attachObject.FileName,
          path:
            dirs.DocumentDir +
            '/' +
            new Date().toLocaleString() +
            '-' +
            attachObject.FileName, //using for ios
          //using for android
        },
      })
        .fetch(
          'GET',
          configApp.url_Download_Notification + attachObject.AttachmentGuid,
          {},
        )
        .then(resp => {
          console.log('88888888888:', resp.data);
          RNFetchBlob.ios.openDocument(resp.data);
        });
    }
  }
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
      console.log(err);
    }
  }

  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null) {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        return valview + ',' + string[1].substring(0, 2);
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
}
//Redux
const mapStateToProps = state => ({
  user: state.user.account,
});

const mapDispatchToProps = {
  getProfile: UserActions.getProfile,
  logout: UserActions.logout,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification_Detail_Components);
