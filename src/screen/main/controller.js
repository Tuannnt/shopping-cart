import {API, API_Announcements, API_Attachment} from '@network';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../utils';
import Toast from 'react-native-simple-toast';
export default {
  CheckToday(callback) {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.CheckToday()
          .then(rs => {
            var data = JSON.parse(rs.data.data);
            FuncCommon.Data_Offline_Set('CheckToday', data);
            callback(data);
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var data = await FuncCommon.Data_Offline_Get('CheckToday');
        callback(data);
      }
    });
  },
  Announcements_ListAll(data, list, callback) {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Announcements.ListAll(data)
          .then(rs => {
            var data = JSON.parse(rs.data.data);
            for (let i = 0; i < data.length; i++) {
              list.push(data[i]);
            }
            FuncCommon.Data_Offline_Set('Announcements_ListAll', list);
            callback(list);
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var _list = await FuncCommon.Data_Offline_Get('Announcements_ListAll');
        callback(_list);
      }
    });
  },
  GetAlertsByUser(obj, list, callback) {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.GetAlertsByUser(obj)
          .then(rs => {
            console.log(rs);
            var data = JSON.parse(rs.data.data);
            for (let i = 0; i < data.length; i++) {
              list.push(data[i]);
            }
            FuncCommon.Data_Offline_Set('GetAlertsByUser', list);
            callback(list);
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var _list = await FuncCommon.Data_Offline_Get('GetAlertsByUser');
        callback(_list);
      }
    });
  },
  GetItems_Announcements(data, callback) {
    var obj = {AnnouncementGuid: data};
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Announcements.GetItems(obj)
          .then(rs => {
            var data = JSON.parse(rs.data.data);
            FuncCommon.Data_Offline_Set('GetItems_Announcements' + obj, data);
            if (data !== null) {
              callback(data);
            } else {
              Toast.showWithGravity(
                'Thông báo này đã bị xoá bỏ',
                Toast.SHORT,
                Toast.CENTER,
              );
              Actions.pop();
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var data = await FuncCommon.Data_Offline_Get(
          'GetItems_Announcements' + obj,
        );
        if (data !== null) {
          callback(data);
        } else {
          Toast.showWithGravity(
            'Thông báo này đã bị xoá bỏ',
            Toast.SHORT,
            Toast.CENTER,
          );
          Actions.pop();
        }
      }
    });
  },
  GetAttList(data, callBack) {
    var _obj = {
      ModuleId: data.ModuleId,
      ParentGuid: data.ParentGuid,
      RecordGuid: data.RecordGuid,
      Keyword: data.Keyword,
    };
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Attachment.GetAttList(_obj)
          .then(res => {
            FuncCommon.Data_Offline_Set(
              'API_Attachment_GetAttList' + _obj,
              res,
            );
            var data = res.data.data != 'null' ? JSON.parse(res.data.data) : [];
            callBack(data);
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var res = await FuncCommon.Data_Offline_Get(
          'API_Attachment_GetAttList' + _obj,
        );
        var data = res.data.data != 'null' ? JSON.parse(res.data.data) : [];
        callBack(data);
      }
    });
  },
  CountNotification(callback) {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.CountNotification()
          .then(rs => {
            var data = JSON.parse(rs.data.data);
            FuncCommon.Data_Offline_Set('CountNotification', data);
            callback(data);
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var data = await FuncCommon.Data_Offline_Get('CountNotification');
        callback(data);
      }
    });
  },
};
