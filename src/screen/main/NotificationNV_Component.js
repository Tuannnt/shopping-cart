import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, Badge, SearchBar} from 'react-native-elements';
import {
  Image,
  FlatList,
  Text,
  RefreshControl,
  StyleSheet,
  View,
  ToastAndroid,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {listCombobox, controller} from '@Home';
import {AppStyles, AppColors} from '@theme';
import {Thumbnail} from 'native-base';
import {API_HR, API} from '@network';
import {FuncCommon} from '../../utils';
import Toast from 'react-native-simple-toast';
import {GetCountNotification} from '../../redux/notification/actions';

class NotificationNV_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshingMy: false,
      refreshingAll: false,
      loading: false,
      Tab: 'ALL',
      ListView: [],
      ListMy: [],
      CountHR: 0,
      CountWF: 0,
      Datajtable: {
        CurrentPage: 0,
        Length: 30,
        QueryOrderBy: 'CreatedDate DESC',
        StartDate: null,
        EndDate: null,
        ModuleId: 'HR_Announcements',
      },
      staticParam: {
        CurrentPage: 0,
        Length: 30,
        QueryOrderBy: 'CreatedDate DESC',
        StartDate: null,
        EndDate: null,
        ModuleId: null,
      },
    };
    this.props.ReloadTab(this._reloadData);
  }
  componentDidMount = () => {
    this.CountNotification();
    this.search(this.state.Tab);
    this.setState({loading: true});
  };
  CountNotification = type => {
    controller.CountNotification(rs => {
      this.setState({CountHR: rs.countHR, CountWF: rs.countWF});
      if (type === 'reload') {
        this.props.CountNotificationMain(+rs.countHR + +rs.countWF);
      }
    });
  };
  _reloadData = () => {
    this.setState({
      refreshingMy: false,
      refreshingAll: false,
      loading: false,
      ListView: [],
      ListMy: [],
      CountHR: 0,
      CountWF: 0,
      Datajtable: {
        CurrentPage: 0,
        Length: 30,
        QueryOrderBy: 'CreatedDate DESC',
        StartDate: null,
        EndDate: null,
        ModuleId: 'HR_Announcements',
      },
      staticParam: {
        CurrentPage: 0,
        Length: 30,
        QueryOrderBy: 'CreatedDate DESC',
        StartDate: null,
        EndDate: null,
        ModuleId: null,
      },
    });
    this.componentDidMount();
  };

  search = val => {
    this.setState({Tab: val, loading: true});
    if (val === 'ALL') {
      this.setState({refreshingAll: true});
      this.nextPage(true);
    } else {
      this.setState({refreshingMy: true});
      this.GetAlertsByUser(true);
    }
  };
  nextPage = s => {
    var list = [];
    if (s === true) {
      this.state.Datajtable.CurrentPage = 0;
    } else {
      list = this.state.ListView;
    }
    this.state.Datajtable.CurrentPage++;
    this.setState({Datajtable: this.state.Datajtable});
    controller.GetAlertsByUser(this.state.Datajtable, list, rs => {
      this.setState({ListView: rs, loading: false, refreshingAll: false});
    });
  };
  GetAlertsByUser = s => {
    var list = [];
    if (s === true) {
      this.state.staticParam.CurrentPage = 0;
    } else {
      list = this.state.ListMy;
    }
    this.state.staticParam.CurrentPage++;
    this.setState({staticParam: this.state.staticParam});
    controller.GetAlertsByUser(this.state.staticParam, list, rs => {
      console.log(rs);
      this.setState({ListMy: rs, loading: false, refreshingMy: false});
    });
  };
  renderReadAll = () => {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Icon
          color="#888888"
          name={'eye-check-outline'}
          type="material-community"
          size={22}
          onPress={() => this.handleReadAll()}
        />
      </View>
    );
  };
  handleReadAll = () => {
    API.ReadAll({})
      .then(res => {
        console.log(res);
        if (res.data.errorCode === 200) {
          this.CountNotification('reload');
          this.search(this.state.Tab);
        } else {
          Toast.showWithGravity(
            'Có lỗi khi cập nhật',
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {});
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <Header
          statusBarProps={AppStyles.statusbarProps}
          {...AppStyles.headerProps}
          centerComponent={this.renderTitle()}
          rightComponent={this.renderReadAll()}
        />
        <View style={{alignItems: 'center', flexDirection: 'row'}}>
          <TouchableOpacity style={[{flex: 1, flexDirection: 'row'}]}>
            <TouchableOpacity
              style={[
                styles.stylesTitle,
                {
                  backgroundColor:
                    this.state.Tab == 'ALL'
                      ? AppColors.ColorButtonSubmit
                      : '#fff',
                },
              ]}
              onPress={() => this.search('ALL')}>
              <Text
                style={[{color: this.state.Tab !== 'ALL' ? 'black' : '#fff'}]}>
                Thông báo chung ({this.state.CountHR})
              </Text>
            </TouchableOpacity>
          </TouchableOpacity>
          <TouchableOpacity style={[{flex: 1, flexDirection: 'row'}]}>
            <TouchableOpacity
              style={[
                styles.stylesTitle,
                {
                  backgroundColor:
                    this.state.Tab == 'MY'
                      ? AppColors.ColorButtonSubmit
                      : '#fff',
                },
              ]}
              onPress={() => this.search('MY')}>
              <Text
                style={[{color: this.state.Tab === 'ALL' ? 'black' : '#fff'}]}>
                Thông báo riêng ({this.state.CountWF})
              </Text>
            </TouchableOpacity>
          </TouchableOpacity>
        </View>
        {this.state.Tab === 'ALL' ? (
          this.state.loading === true ? null : this.state.ListView.length >
            0 ? (
            this.ViewListAll(this.state.ListView)
          ) : (
            <View style={{alignItems: 'center'}}>
              <Text>Không có dữ liệu</Text>
            </View>
          )
        ) : this.state.loading === true ? null : this.state.ListMy.length >
          0 ? (
          this.ViewListMy(this.state.ListMy)
        ) : (
          <View style={{alignItems: 'center'}}>
            <Text>Không có dữ liệu</Text>
          </View>
        )}
      </View>
    );
  }
  renderTitle = () => {
    return (
      <View style={[AppStyles.TabbarTop_Center, {marginBottom: 30}]}>
        <View style={AppStyles.Home_Logo_Icon}>
          <Image
            resizeMode="contain"
            style={
              Platform.OS === 'ios'
                ? {width: 100, height: 50}
                : {width: 100, height: 60}
            }
            source={require('../../images/logo/logoesvn1.png')}
          />
        </View>
      </View>
    );
  };
  ViewListAll = item => {
    return (
      <FlatList
        data={item}
        renderItem={({item, index}) => (
          <TouchableOpacity
            style={[{flexDirection: 'row'}]}
            onPress={() => this.openurl(item)}>
            <View style={{justifyContent: 'center', paddingLeft: 10}}>
              <Image
                resizeMode="contain"
                source={require('../../images/logo/eastern-sun-logo.png'
                )}
                style={{width: 60, height: 20}}
              />
            </View>
            <View
              style={[
                {
                  flex: 1,
                  padding: 10,
                  borderBottomWidth: 0.5,
                  borderBottomColor: AppColors.gray,
                },
              ]}>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text style={(styles.subtitleStyle, {fontWeight: 'bold'})}>
                      {item.Title}
                    </Text>
                  </View>
                  {item.IsRead == false ? (
                    <Icon
                      iconStyle={{color: '#5882FA'}}
                      name={'controller-record'}
                      type="entypo"
                      size={15}
                    />
                  ) : null}
                </View>
              </View>
              <View style={{flexDirection: 'row', height: 40}}>
                <View style={{flex: 1}}>
                  <Text style={styles.subtitleStyle}>
                    {item.Description === null
                      ? ''
                      : item.Description.substring(0, 50)}
                  </Text>
                </View>
                <View style={{justifyContent: 'flex-end'}}>
                  <Text
                    style={[AppStyles.Textdefault, {color: AppColors.gray}]}>
                    {FuncCommon.ConDate(item.CreatedDate, 0)}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        )}
        onEndReached={() =>
          item.length >=
          this.state.Datajtable.CurrentPage * this.state.Datajtable.Length
            ? this.nextPage(false)
            : null
        }
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshingAll}
            onRefresh={() => this.nextPage('load')}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    );
  };
  ViewListMy = item => {
    return (
      <FlatList
        data={item}
        renderItem={({item, index}) => (
          <TouchableOpacity
            style={[{flexDirection: 'row'}]}
            onPress={() => this.openurl(item)}>
            <View style={{justifyContent: 'center', paddingLeft: 10}}>
              <Thumbnail
                style={{width: 40, height: 40}}
                source={API_HR.GetPicApplyLeaves(item.SenderGuid)}
              />
            </View>
            <View
              style={[
                {
                  flex: 1,
                  borderBottomWidth: 0.5,
                  borderBottomColor: AppColors.gray,
                  padding: 5,
                  margin: 5,
                },
              ]}>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text style={(styles.subtitleStyle, {fontWeight: 'bold'})}>
                      {item.Title}
                    </Text>
                  </View>
                  {item.IsRead == false ? (
                    <Icon
                      iconStyle={{color: '#5882FA'}}
                      name={'controller-record'}
                      type="entypo"
                      size={15}
                    />
                  ) : null}
                </View>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <Text style={styles.subtitleStyle}>
                    {item.Description === null ? '' : item.Description}
                  </Text>
                </View>
                <View style={{justifyContent: 'flex-end'}}>
                  <Text
                    style={[AppStyles.Textdefault, {color: AppColors.gray}]}>
                    {FuncCommon.ConDate(item.CreatedDate, 0, 'iso')}
                    {/* fix lỗi ngày  */}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        )}
        onEndReached={() =>
          item.length >=
          this.state.staticParam.CurrentPage * this.state.staticParam.Length
            ? this.GetAlertsByUser(false)
            : null
        }
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshingMy}
            onRefresh={() => this.GetAlertsByUser(true)}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    );
  };
  handleUpdateIsReadOffline = item => {
    let {ListMy} = this.state;
    let arr =
      ListMy.map(x => {
        if (x.AlertGuid === item.AlertGuid) {
          x.IsRead = true;
        }
        return x;
      }) || [];
    this.setState({ListMy: arr});
  };
  //Xem
  openurl = item => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        if (item.MobileUrl != null) {
          var module = item.MobileUrl.split('=')[1].split('&')[0];
          if (!module || module === 'null') {
            Toast.showWithGravity(
              'Chức năng này không hỗ trợ dưới mobile',
              Toast.SHORT,
              Toast.CENTER,
            );
            if (item.RecordGuid !== null && item.RecordGuid !== '') {
              API.UpdateIsRead({AlertGuid: item.AlertGuid})
                .then(res => {
                  this.handleUpdateIsReadOffline(item);
                })
                .catch(error => {});
            }
            return;
          }
        }
        if (item.RecordGuid !== null && item.RecordGuid !== '') {
          API.UpdateIsRead({AlertGuid: item.AlertGuid})
            .then(res => {
              Actions[module]({RecordGuid: item.RecordGuid});
            })
            .catch(error => {});
        }
      } else {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
      }
    });
  };
  removeLine = string => {
    if (string !== null && string !== '') {
      var check = string.split(/\n/g);
      return Str;
    } else {
      return '';
    }
  };
}
const styles = StyleSheet.create({
  stylesTitle: {
    flex: 1,
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 10,
    borderColor: AppColors.gray,
    margin: 10,
    padding: 10,
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => {
  return {
    CountNotificationMain: count => {
      dispatch(GetCountNotification(count));
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationNV_Component);
