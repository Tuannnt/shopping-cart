import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Animated, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from "react-native-router-flux";
import { Divider, ListItem, Icon } from 'react-native-elements';
import { API } from "@network";
import { WebView } from 'react-native-webview';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { FuncCommon } from '../../utils';
const { height } = Dimensions.get("window");
class CategoryDetailComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 15,
                DBName: "ES_HR",
                Scheme: "HR",
                TableName: "Announcements",
            }
        };
    }
    static defaultProps = {
        draggableRange: { top: height - 120, bottom: 180 }
    };

    GetAll = () => {
        this.setState({ refreshing: true });

        API.ListAnnouncements(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }
    componentDidMount() {
        this.GetAll();
    }
    render() {
        const { top, bottom } = this.props.draggableRange;
        return (
            <ScrollView>
                {
                    this.state.list.map((l, i) => (
                        <View key={i} style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                            <ListItem
                                style={{ flex: 5 }}
                                key={i}
                                title={l.Title}
                                titleStyle={AppStyles.Titledefault}
                                subtitle={() => {
                                    return (
                                        <View >
                                            <View>
                                                <WebView
                                                    style={{ height: 50, marginRight: 10 }}
                                                    originWhitelist={['*']}
                                                    source={{ html: '<Text style="font-size:250%; color:"#989898"">' + this.removeLine(l.Contents) + '</Text>' }}
                                                />
                                            </View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ width: '50%' }}>
                                                    <Text style={AppStyles.Textdefault}>Bắt đầu: {FuncCommon.ConDate(l.StartDate, 1)}</Text>
                                                </View>
                                                <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                    <Text
                                                        style={AppStyles.Textdefault}>Kết thúc: {FuncCommon.ConDate(l.EndDate, 1)}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    )
                                }}
                                onPress={() => this.openAnnouncements(l.RowGuid)}
                                bottomDivider
                            />
                        </View>
                    ))
                }
            </ScrollView>
        );
    }
    viewAnnouncements() {
        Actions.Announcements({});
    }
    openAnnouncements(id) {
        Actions.hrItemAnnouncements({ viewId: id });
    }
    //Xóa xuống dòng
    removeLine(string) {
        if (string !== null && string !== "" && string !== undefined) {
            var Str = string.replace(/\n/g, ' ');
            Str = Str.replace(/<\/?[^>]+(>|$)/g, "");
            return Str.substring(0, 120) + '...';
        }
    };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryDetailComponent);
