import React, {Component} from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {ActionConst, Actions} from 'react-native-router-flux';
import * as UserActions from '@redux/user/actions';
import Icon from 'react-native-vector-icons/AntDesign';
import {Thumbnail} from 'native-base';
import {API, API_HR} from '../../network';
import {AppStyles, AppSizes, AppColors} from '@theme';
class DrawerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      Search: '',
      Id_User: '',
      FullName_User: '',
      LoginName_User: '',
      CheckList: [
        {
          title: 'Thiết đặt',
          icon: 'setting',
          code: 'setting',
          color: 'black',
        },
        {
          title: 'Đăng xuất',
          icon: 'logout',
          code: 'checkout',
          color: 'black',
        },
      ],
    };
  }
  componentDidMount = () => {
    this.loadData();
  };
  loadData() {
    API.getProfile()
      .then(rs => {
        this.setState({
          LoginName_User: rs.data.LoginName,
          FullName_User: rs.data.FullName,
          Id_User: rs.data.EmployeeGuid,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  render() {
    return (
      <View style={[AppStyles.container, {marginTop: 30}]}>
        <View style={AppStyles.Drawer_HeaderContainer}>
          <View style={{flex: 2}}>
            <Thumbnail source={API_HR.GetPicApplyLeaves(this.state.Id_User)} />
          </View>
          <View style={{flex: 5, justifyContent: 'center'}}>
            <Text style={[AppStyles.h2, {color: AppColors.blueFacebook}]}>
              {this.state.FullName_User}
            </Text>
            <Text style={AppStyles.Textdefault}>
              {this.state.LoginName_User}
            </Text>
          </View>
        </View>
        <ScrollView>
          {this.state.CheckList.map((item, index) => (
            <TouchableOpacity onPress={() => this.itemPress(item, index)}>
              <View style={styles.ViewFlatList}>
                <Icon name={item.icon} color={item.color} style={styles.Icon} />
                <Text style={(AppStyles.Textdefault, {color: item.color})}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
  itemPress(item, index) {
    let checkList = [...this.state.CheckList];
    checkList.forEach(obj => {
      if (obj.code === item.code) {
        obj.color = AppColors.Maincolor;
      } else {
        obj.color = 'black';
      }
    });
    this.setState({CheckList: checkList});
    switch (item.code) {
      case 'checkout':
        this.props.logout();
        Actions.login(ActionConst.RESET);
        break;
      case 'setting':
        Actions.testSound();
        break;
    }
  }
  S;
}

const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    marginRight: 10,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
});

//Redux
const mapStateToProps = state => ({
  user: state.user.account,
});

const mapDispatchToProps = {
  getProfile: UserActions.getProfile,
  logout: UserActions.logout,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawerComponent);
