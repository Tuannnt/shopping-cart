import React, {Component} from 'react';
import {StyleSheet, View, Linking, PermissionsAndroid} from 'react-native';
import {connect} from 'react-redux';
import MenuBottomComponent from './MenuBottomComponent';
import {Actions} from 'react-native-router-flux';
import {AppStyles, AppSizes, AppColors} from '@theme';
//Begin created by habx@esvn.com.vn
import signalr from 'react-native-signalr';
import configApp from '../../configApp';
import {API} from '@network';
//End created by habx@esvn.com.vn
import {FuncCommon} from '../../utils';
import {getProfile} from '../../redux/user/actions';
import {listCombobox, controller} from '@Home';
import {GetCountNotification} from '../../redux/notification/actions';

class HomeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyselectTab: 'home',
      BackModuleByCode: 'MyProfile',
      DatachatGroupNew: null,
      LocationId: '',
      loading: false,
    };
    API.getProfile()
      .then(rs => {
        FuncCommon.Data_Offline_Set('API_getProfile', rs.data);
        this.setState({LocationId: rs.data.LocationId});
        global.__appSIGNALR.SIGNALR_object.USER = rs.data;
        global.__appSIGNALR.SIGNALR_object.IS_AUTHEN = true;
        global.__appSIGNALR.initName();
      })
      .catch(error => {
        console.logError('Lỗi lấy dữ liệu người đăng nhập', error);
      });
    FuncCommon.Data_Offline(async function(d) {
      if (!d) {
        var x = await FuncCommon.Data_Offline_Get('API_getProfile');
        this.setState({LocationId: x.LocationId});
        global.__appSIGNALR.SIGNALR_object.USER = x;
        global.__appSIGNALR.SIGNALR_object.IS_AUTHEN = true;
        global.__appSIGNALR.initName();
      }
    });
    switch (this.props.data) {
      case 'chat':
        this.state.keyselectTab = 'chat';
        this.setState({
          keyselectTab: 'chat',
        });
        break;
      case 'Viewchat':
        this.state.keyselectTab = 'chat';
        this.state.DatachatGroupNew = this.props.DatachatGroupNew;
        this.setState({
          keyselectTab: 'chat',
          DatachatGroupNew: this.state.DatachatGroupNew,
        });
        break;
      default:
        break;
    }
  }
  componentDidMount = () => {
    this.props.getProfile();
    //test redux
    this.state.keyselectTab = this.props.data;
    this.setState({
      keyselectTab: this.state.keyselectTab,
    });
    if (Platform.OS === 'android') {
      Linking.getInitialURL().then(url => {
        if (global.__Linking !== url) {
          FuncCommon.RouterLink(url, Actions);
          global.__Linking = url;
        }
      });
    } else {
      Linking.addEventListener('url', this.handleOpenURL);
    }
    controller.CountNotification(rs => {
      this.props.CountNotificationMain(+rs.countHR + +rs.countWF);
    });
  };

  handleOpenURL = url => {
    if (global.__Linking !== url) {
      FuncCommon.RouterLink(u, Actions);
      global.__Linking = url;
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.BackModuleByCode !== undefined) {
      this.setState({BackModuleByCode: nextProps.BackModuleByCode});
    }
    controller.CountNotification(rs => {
      this.props.CountNotificationMain(+rs.countHR + +rs.countWF);
      if (
        nextProps.moduleId === 'chat' ||
        nextProps.moduleId === 'setting' ||
        nextProps.moduleId === 'home'
      ) {
        this.setState({keyselectTab: nextProps.moduleId}, () => {
          this._CalbackView();
        });
      } else if (nextProps.moduleId === 'QRCodeV2') {
        Actions.push(nextProps.Data.Link, {Id: nextProps.Data.Value.data});
      } else if (nextProps.moduleId === 'ListAllMSS') {
        Actions.viewItemMessange({
          guidGroup: nextProps.Data.guidGroup,
          Name: nextProps.Data.Name,
          IsGroup: nextProps.Data.IsGroup,
          LoginName: null,
        });
      } else {
        this.setState({keyselectTab: 'notification'}, () => {
          this._CalbackView();
        });
      }
    });
  };
  _CalbackView() {}
  CalbackView = d => {
    this._CalbackView = d;
  };
  render() {
    return this.state.loading === true ? null : (
      <View style={AppStyles.container}>
        <MenuBottomComponent
          LocationId={this.state.LocationId}
          CallbackView={this.CalbackView}
          keyselectTab={this.state.keyselectTab}
          BackModuleByCode={this.state.BackModuleByCode}
          DatachatGroupNew={this.state.DatachatGroupNew}
        />
      </View>
    );
  }
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
      console.log(err);
    }
  }
}

//Redux
const mapStateToProps = state => ({
  key: state.MS.data,
});
const mapDispatchToProps = dispatch => {
  return {
    CountNotificationMain: count => {
      dispatch(GetCountNotification(count));
    },
    getProfile: () => {
      dispatch(getProfile());
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeComponent);
