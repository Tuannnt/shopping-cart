import React, {Component} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  View,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {ActionConst, Actions} from 'react-native-router-flux';
import * as UserActions from '@redux/user/actions';
import {Divider, Icon, SearchBar} from 'react-native-elements';
import {API, API_HR} from '../../network';
import {TabBar_Title, CustomView} from '@Component';
import {AppStyles, AppSizes, AppColors} from '@theme';
import {ToastAndroid} from 'react-native';
import {FuncCommon} from '../../utils';
import Toast from 'react-native-simple-toast';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

class Edit_PassWord_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DataUp: {
        PasswordOld: '',
        PasswordNew: '',
        PasswordReNew: '',
      },
    };
  }
  componentDidMount = () => {};
  testPass = () => {};
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={[AppStyles.container]}>
            <TabBar_Title
              title={'Thay đổi mật khẩu'}
              callBack={() => this.callBack()}
            />
            <Divider />
            <ScrollView style={{flex: 1}}>
              <View style={{justifyContent: 'center', padding: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Mật khẩu cũ</Text>
                  <Text style={{color: 'red'}}> *</Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  value={this.state.DataUp.PasswordOld}
                  placeholder={'Nhập mật khẩu cũ'}
                  autoFocus={true}
                  autoCapitalize="none"
                  secureTextEntry={true}
                  onChangeText={text => this.setPasswordOld(text)}
                />
              </View>
              <View style={{justifyContent: 'center', padding: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Mật khẩu mới</Text>
                  <Text style={{color: 'red'}}> *</Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  value={this.state.DataUp.PasswordNew}
                  placeholder={'Nhập mật khẩu mới'}
                  secureTextEntry={true}
                  autoCapitalize="none"
                  onChangeText={text => this.setPasswordNew(text)}
                />
              </View>
              <View style={{justifyContent: 'center', padding: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>
                    Xác nhận mật khẩu mới
                  </Text>
                  <Text style={{color: 'red'}}> *</Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  value={this.state.DataUp.PasswordReNew}
                  placeholder={'Xác nhận mật khẩu mới'}
                  secureTextEntry={true}
                  onChangeText={text => this.setPasswordReNew(text)}
                />
              </View>
              <TouchableOpacity
                style={{
                  backgroundColor: AppColors.ColorButtonSubmit,
                  alignItems: 'center',
                  justifyContent: 'center',
                  padding: 10,
                  borderRadius: 10,
                  margin: 10,
                }}
                onPress={() => this.onSubmit()}>
                <Text style={{color: '#fff'}}>Xác nhận</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  //#region view popup đổi password
  _openEditpass() {}
  openEditpass = d => {
    this._openEditpass = d;
  };
  //#endregion
  setPasswordOld = val => {
    this.state.DataUp.PasswordOld = val;
    this.setState({DataUp: this.state.DataUp});
  };
  setPasswordNew = val => {
    this.state.DataUp.PasswordNew = val;
    this.setState({DataUp: this.state.DataUp});
  };
  setPasswordReNew = val => {
    this.state.DataUp.PasswordReNew = val;
    this.setState({DataUp: this.state.DataUp});
  };
  onSubmit = () => {
    FuncCommon.Data_Offline(async d => {
      if (!d) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng.',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
    });
    if (this.state.DataUp.PasswordOld === '') {
      Toast.showWithGravity(
        'Yêu cầu nhập mật khẩu cũ.',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    // else {
    //     var parttern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,}/;
    //     if (parttern.test(this.state.DataUp.PasswordOld)) {
    //         null;
    //     } else {
    //         Toast.showWithGravity(
    //             'Yêu cầu mật khẩu cũ tối thiểu gồm 6 ký tự bao gồm cả chữ, số và có ký tự in hoa.',
    //             Toast.SHORT,
    //             Toast.CENTER,
    //         );
    //         return;
    //     }
    // }
    if (this.state.DataUp.PasswordNew === '') {
      Toast.showWithGravity(
        'Yêu cầu nhập mật khẩu mới.',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    } else {
      var parttern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,}/;
      if (parttern.test(this.state.DataUp.PasswordNew)) {
        null;
      } else {
        Toast.showWithGravity(
          'Yêu cầu mật khẩu mới tối thiểu gồm 6 ký tự bao gồm cả chữ, số và có ký tự in hoa.',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
    }
    if (this.state.DataUp.PasswordReNew === '') {
      Toast.showWithGravity(
        'Yêu cầu xác nhận lại mật khẩu mới.',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (this.state.DataUp.PasswordNew !== this.state.DataUp.PasswordReNew) {
      Toast.showWithGravity(
        'Xác nhận mật khẩu mới không trùng khớp với mật khẩu mới.',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    API.ChangePassword(this.state.DataUp)
      .then(rs => {
        Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
        if (rs.data.errorCode === 200) {
          this.callBack();
        }
      })
      .catch(error => {
        Toast.showWithGravity(
          'Đã xảy ra lỗi thay đổi mật khẩu' + error.response.data,
          Toast.SHORT,
          Toast.CENTER,
        );
      });
  };
  callBack() {
    this.setState({EventBoxIcon: true});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'setting', ActionTime: new Date().getTime()});
  }
}

//Redux
const mapStateToProps = state => ({
  user: state.user.account,
});

const mapDispatchToProps = {
  getProfile: UserActions.getProfile,
  logout: UserActions.logout,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Edit_PassWord_Component);
