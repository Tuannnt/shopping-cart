import React, { Component } from 'react';
import {
  Image,
  Alert,
  ScrollView,
  Text,
  TouchableOpacity,
  PermissionsAndroid,
  View,
  ImageBackground,
  Dimensions,
  Animated,
  KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';
import { Divider } from 'react-native-elements';
import LoadingComponent from '../component/LoadingComponent';
import MyDay_Component from './MyDay_Component';
import { FlatGrid } from 'react-native-super-grid';
import { Actions } from 'react-native-router-flux';
import { Icon, Badge } from 'react-native-elements';
import { API } from '@network';
import * as UserAction from '@redux/user/actions';
import { AppStyles, AppSizes, AppColors } from '@theme';
import { FuncCommon } from '@utils';
import { ToastAndroid } from 'react-native';
import Toast from 'react-native-simple-toast';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const TimeOut = 500;
class ViewHomeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      ListMenuAll: [],
      dataMenu: [],
      itemsTop: [],
      BackModuleByCode: this.props.BackModuleByCode,
      OffsetYNotification: new Animated.Value(SCREEN_HEIGHT),
      opacityTabView: new Animated.Value(0),
      animated_downOn: new Animated.Value(1),
      setoffNotification: false,
    };
    this.props.ReloadTab(this._reloadData);
  }
  _ReloadMyday() { }
  ReloadMyday = d => {
    this._ReloadMyday = d;
  };
  _reloadData = () => {
    this._ReloadMyday();
    this.setState({
      ListMenuAll: [],
      dataMenu: [],
      itemsTop: [],
      BackModuleByCode: this.props.BackModuleByCode,
      setoffNotification: false,
      loading: false,
      OffsetYNotification: new Animated.Value(SCREEN_HEIGHT),
      opacityTabView: new Animated.Value(0),
      animated_downOn: new Animated.Value(1),
      setoffNotification: false,
    });
    this.loadData();
    this.SkillAnimated();
  };
  componentDidMount() {
    this.setState({ loading: true }, () => {
      this.SkillAnimated();
      this.loadData();
    });
  }
  componentWillReceiveProps(nextProps) {
    this.setState({ loading: true }, () => {
      this.SkillAnimated();
      this.loadData();
    });
  }

  loadData = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.Applications()
          .then(res => {
            FuncCommon.Data_Offline_Set('Applications', res);
            this.setState({ ListMenuAll: res }, () => {
              this.LoadListMenu(res);
            });
          })
          .catch(error => {
            this.setState({ loading: false });
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('Applications');
        this.setState({ ListMenuAll: x }, () => {
          this.LoadListMenu(x);
        });
      }
    });
  };
  //thông báo bên HR
  CountNotificationHR = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.CountNoification()
          .then(res => {
            FuncCommon.Data_Offline_Set('CountNoification', res);
            this.NotificationHR(res);
          })
          .catch(error => {
            console.log(error);
            this.setState({ loading: false });
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('CountNoification');
        this.NotificationHR(x);
      }
    });
  };
  NotificationHR = res => {
    var listData = JSON.parse(res.data.data);
    for (var i = 0; i < this.state.dataMenu.length; i++) {
      var data = listData.find(x => x.Name == this.state.dataMenu[i].Code);
      if (data !== null && data !== undefined) {
        if (data.Value > 99) {
          this.state.dataMenu[i].Attributes[2].Value = '99+';
        } else {
          this.state.dataMenu[i].Attributes[2].Value = data.Value;
        }
      }
    }
    this.setState({ dataMenu: this.state.dataMenu, loading: false });
  };
  //Thông báo bên AM
  CountNotificationAM = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.CountMenu_AM()
          .then(res => {
            FuncCommon.Data_Offline_Set('CountMenu_AM', res);
            this.NotificationAM(res);
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('CountMenu_AM');
        this.NotificationAM(x);
      }
    });
  };
  NotificationAM = res => {
    var listData = JSON.parse(res.data.data);
    for (var i = 0; i < this.state.dataMenu.length; i++) {
      var data = listData.find(x => x.Name == this.state.dataMenu[i].Code);
      if (data !== null && data !== undefined) {
        if (data.Value > 99) {
          this.state.dataMenu[i].Attributes[2].Value = '99+';
        } else {
          this.state.dataMenu[i].Attributes[2].Value = data.Value;
        }
      }
    }
    this.setState({ dataMenu: this.state.dataMenu, loading: false });
  };
  NotificationPro = () => {
    API.CountMenu_Pro()
      .then(res => {
        var listData = JSON.parse(res.data.data);
        for (var i = 0; i < this.state.dataMenu.length; i++) {
          var data = listData.find(x => x.Name == this.state.dataMenu[i].Code);
          if (data !== null && data !== undefined) {
            this.state.dataMenu[i].Attributes[2].Value = data.Value;
          }
        }
        this.setState({ dataMenu: this.state.dataMenu, loading: false });
      })
      .catch(error => {
        console.log(error);
      });
    // this.LoadListMenu(this.state.dataMenu, );
  };
  LoadListMenu = res => {
    var listItemMenu = [];
    var listItemMenuDetail = [];
    for (var i = 0; i < res.length; i++) {
      var obj = res.find(x => x.ParentId == null && x.Id == res[i].Id);
      if (obj !== undefined) {
        obj.checkbox = false;
        if (this.props.BackModuleByCode != '') {
          if (obj.Code == this.props.BackModuleByCode) {
            obj.checkbox = true;
          }
        } else {
          obj.checkbox = obj.Code === 'MyProfile' ? true : false;
        }
        listItemMenu.push(obj);
      }
    }
    var _listTop = listItemMenu.find(x => x.checkbox == true);
    for (var i = 0; i < res.length; i++) {
      var obj = res.find(x => x.ParentId == _listTop.Id && x.Id == res[i].Id);
      if (obj !== undefined) {
        listItemMenuDetail.push(obj);
      }
    }
    this.setState(
      { itemsTop: listItemMenu, dataMenu: listItemMenuDetail },
      () => {
        if (
          this.props.BackModuleByCode === '' ||
          this.props.BackModuleByCode === 'MyProfile'
        ) {
          this.CountNotificationHR();
        } else if (this.props.BackModuleByCode === 'PRO') {
          this.NotificationPro();
        } else {
          this.CountNotificationAM();
        }
      },
    );
  };
  CustomView = para => (
    <ScrollView>
      <FlatGrid
        itemDimension={100}
        items={para}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            onPress={() => this.onclickitemDetail(item.Code, item.Privileges)}>
            <View style={AppStyles.Home_Container_Item_Detail}>
              <Icon
                name={item.Attributes[0].Value}
                type={item.Attributes[1].Value}
                size={AppSizes.Home_SizeIcon_Top}
                color={AppColors.Maincolor}
              />
              <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
                {item.Title}
              </Text>
            </View>
            {item.Attributes[2].Value != 0 &&
              item.Attributes[2].Value != undefined ? (
              <Badge
                status="error"
                value={item.Attributes[2].Value}
                containerStyle={AppStyles.Home_Badge}
              />
            ) : null}
          </TouchableOpacity>
        )}
      />
    </ScrollView>
  );
  CustomTabBarView = para => (
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
      {para.map((item, index) =>
        item.checkbox === true ? (
          <TouchableOpacity
            key={index}
            style={[
              {
                backgroundColor: AppColors.Maincolor,
                borderRadius: 15,
                margin: 5,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10,
                width: 100,
              },
            ]}
            onPress={() => this.onclickItem(item.Id)}>
            <View style={{}}>
              <Icon
                name={item.Attributes[0].Value}
                type={item.Attributes[1].Value}
                size={AppSizes.Home_SizeIcon_Top}
                color={'#fff'}
              />
            </View>
            <View>
              <Text
                style={[
                  AppStyles.containerCentered,
                  AppStyles.Textdefault,
                  { color: '#fff' },
                ]}>
                {item.Title}
              </Text>
            </View>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            key={index}
            style={AppStyles.Home_Container_Item_top}
            onPress={() => this.onclickItem(item.Id)}>
            <View style={{}}>
              <Icon
                name={item.Attributes[0].Value}
                type={item.Attributes[1].Value}
                size={30}
                color={AppColors.Maincolor}
              />
            </View>
            <View>
              <Text
                style={[
                  AppStyles.containerCentered,
                  AppStyles.Textdefault,
                  { color: 'black' },
                ]}>
                {item.Title}
              </Text>
            </View>
          </TouchableOpacity>
        ),
      )}
    </ScrollView>
  );
  render() {
    const opacityTabView = this.state.opacityTabView.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 0.6],
    });
    const rotateStart = this.state.animated_downOn.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return (
      <KeyboardAvoidingView>
        <ImageBackground
          source={require('../../images/logo/hinhnen.jpg')}
          style={AppStyles.Home_ImageBackground}>
          <View>
            <ScrollView scrollEventThrottle={16}>
              <View style={AppStyles.Home_container_Top}>
                <View style={AppStyles.Home_Logo_Icon}>
                  <Image
                    source={require('../../images/logo/logoesvn1.png')}
                    style={{ height: 45, width: '50%' }}
                    resizeMode="contain"
                  />
                </View>
                <Divider />
                <View style={{ alignItems: 'center' }}>
                  {this.state.itemsTop.length > 0
                    ? this.CustomTabBarView(this.state.itemsTop)
                    : null}
                </View>
              </View>
            </ScrollView>
          </View>
          <View style={{ flex: 1, justifyContent: 'space-between' }}>
            {this.state.loading !== true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  width: SCREEN_WIDTH,
                  flex: 1,
                }}>
                {this.CustomView(this.state.dataMenu)}
              </View>
            ) : (
              <LoadingComponent backgroundColor="#fff" />
            )}
            {/*notification*/}
            {this.state.OnOffopacity === true ? (
              <Animated.View
                style={{
                  backgroundColor: 'black',
                  position: 'absolute',
                  width: SCREEN_WIDTH,
                  opacity: opacityTabView,
                  height: SCREEN_HEIGHT,
                }}>
                <TouchableOpacity
                  style={{ flex: 1 }}
                  onPress={() => {
                    this.setState({
                      setoffNotification: !this.state.setoffNotification,
                    }),
                      this.openNotification();
                  }}
                />
              </Animated.View>
            ) : null}
            {/* <Animated.View
          style={{
            backgroundColor: '#fff',
            bottom: 0,
            position: 'absolute',
            zindex: 10,
            width: SCREEN_WIDTH,
            transform: [{translateY: this.state.OffsetYNotification}],
          }}> */}
            {/* {global.__appSIGNALR.SIGNALR_object.USER.LocationId === "CB" ?
            <View style={{ height: SCREEN_HEIGHT }}>
              <Divider />
              <View style={{ flexDirection: 'row', padding: 10 }}>
                <TouchableOpacity style={{ flex: 2, flexDirection: 'row' }} onPress={() => { this.setState({ setoffNotification: !this.state.setoffNotification, }), this.openNotification(); }}>
                  <Icon name={'bullhorn'} type="font-awesome" size={25} iconStyle={{ marginRight: 10 }} />
                  <Text style={AppStyles.Titledefault}>Thông báo</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end' }} onPress={() => { this.setState({ setoffNotification: !this.state.setoffNotification, }), this.openNotification(); }}>
                  <Animated.View style={{ transform: [{ rotate: rotateStart }, { perspective: 400 }], }}>
                    <Icon name={'chevron-down'} type={'feather'} size={25} />
                  </Animated.View>
                </TouchableOpacity>
              </View>
              <Divider />
              <CategoryDetailComponent />
            </View>
            :
            // nhân viên */}
          </View>
          <View style={{ backgroundColor: '#fff' }}>
            <Divider />
            <View style={{ flexDirection: 'row', padding: 10 }}>
              <Text style={[AppStyles.Titledefault, { flex: 1 }]}>Hôm nay</Text>
              <View
                style={{
                  flexDirection: 'row',
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.ConDate(this.state.todayDate, 0)}
                </Text>
              </View>
            </View>
            <Divider />
            <MyDay_Component
              ReloadTab={this.ReloadMyday}
              appState={this.state.appState}
            />
          </View>
          {/* } */}
          {/* </Animated.View> */}
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
  onclickItem = para => {
    var listItemMenuDetail = [];
    var _ActionsNow = this.state.ListMenuAll.find(x => x.Id == para);
    if (_ActionsNow !== undefined && _ActionsNow.Attributes.length > 2) {
      for (let y = 0; y < _ActionsNow.Attributes.length; y++) {
        if (_ActionsNow.Attributes[y].Key == 'Badge_value') {
          var obj = _ActionsNow.Privileges.find(x => x.ActionId == 'LISTVIEW');
          if (obj != undefined && obj != null) {
            Actions.push(_ActionsNow.Code);
          } else {
            Alert.alert(
              'Thông báo',
              'Xin lỗi. Bạn không có quyền truy cập.',
              [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
              { cancelable: false },
            );
          }
        }
      }
    }
    for (var i = 0; i < this.state.ListMenuAll.length; i++) {
      var obj = this.state.ListMenuAll.find(
        x => x.ParentId == para && x.Id == this.state.ListMenuAll[i].Id,
      );

      if (obj !== undefined) {
        listItemMenuDetail.push(obj);
      }
    }
    this.state.dataMenu = listItemMenuDetail;
    var _itemsTop = this.state.itemsTop;
    var code = '';
    for (let j = 0; j < _itemsTop.length; j++) {
      _itemsTop[j].checkbox = false;
      if (_itemsTop[j].Id == para) {
        _itemsTop[j].checkbox = true;
        code = _itemsTop[j].Code;
      }
    }
    this.setState(
      {
        dataMenu: this.state.dataMenu,
        itemsTop: _itemsTop,
      },
      () => {
        if (code === 'MyProfile') {
          this.CountNotificationHR();
        } else if (code === 'PRO') {
          this.NotificationPro();
        } else {
          this.CountNotificationAM();
        }
      },
    );
  };
  // click actions
  onclickitemDetail = (para, listView) => {
    var obj = listView.find(x => x.ActionId == 'LISTVIEW');
    if (obj != undefined && obj != null) {
      this.props.addItem(this.state.ListMenuAll);
      Actions.push(para);
    } else {
      Alert.alert(
        'Thông báo',
        'Xin lỗi. Bạn không có quyền truy cập.',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false },
      );
    }
  };
  openNotification = () => {
    if (this.state.setoffNotification == false) {
      this.setState({ OnOffopacity: true });
      Animated.sequence([this.OpenFull]).start();
      Animated.sequence([this.opacityTabView]).start();
      Animated.sequence([this.DownOn]).start();
    } else {
      Animated.sequence([this.OffFull]).start();
      Animated.sequence([this.OffopacityTabView]).start();
      Animated.sequence([this.UpOn]).start();
      setTimeout(() => {
        this.setState({ OnOffopacity: false });
      }, 100);
    }
  };
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
      console.log(err);
    }
  }
  SkillAnimated = () => {
    //#region bật tab
    this.OpenTabView = Animated.timing(this.state.OffsetYNotification, {
      toValue: SCREEN_HEIGHT - SCREEN_HEIGHT / 4,
      duration: TimeOut,
      useNativeDriver: true,
    }).start();
    this.OpenFull = Animated.timing(this.state.OffsetYNotification, {
      toValue: SCREEN_HEIGHT / 6,
      duration: TimeOut,
      useNativeDriver: true,
    });
    this.OffFull = Animated.timing(this.state.OffsetYNotification, {
      toValue: SCREEN_HEIGHT - SCREEN_HEIGHT / 4,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#region  mờ tab
    this.opacityTabView = Animated.timing(this.state.opacityTabView, {
      toValue: 1,
      duration: TimeOut,
      useNativeDriver: true,
    });
    this.OffopacityTabView = Animated.timing(this.state.opacityTabView, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    this.DownOn = Animated.timing(this.state.animated_downOn, {
      toValue: 0,
      duration: TimeOut,
    });
    this.UpOn = Animated.timing(this.state.animated_downOn, {
      toValue: 1,
      duration: TimeOut,
    });
  };
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {
  addItem: data => UserAction.applications(data),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewHomeComponent);
