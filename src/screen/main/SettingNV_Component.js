import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { ActionConst, Actions } from 'react-native-router-flux';
import * as UserActions from '@redux/user/actions';
import { Divider, Icon, SearchBar } from 'react-native-elements';
import { Thumbnail } from 'native-base';
import { API, API_HR } from '../../network';
import { LoadingComponent, CustomView } from '@Component';
import { AppStyles, AppSizes, AppColors } from '@theme';
import { FuncCommon } from '../../utils';
import OneSignal from 'react-native-onesignal';
var DEVICE = '';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const ItemCode = {
  PERSONNEL: 0,
  SELL: 1,
  CUSTOMER: 2,
  PURCHASE: 3,
  ACCOUNTANT: 4,
  WAREHOUSE: 5,
  OPERATIONAL: 6,
  DOCUMENT: 7,
  MARKETING: 8,
  PROJECT: 9,
  MANUFACTURING: 10,
  PROPERTY: 11,
  ESTABLISH: 12,
  SETTING: 13,
  LOGOUT: 14,
  LOGCHECKIN: 15,
};

class DrawerComponent extends Component {
  constructor(props) {
    super(props);
    OneSignal.addEventListener('ids', this.onIds);
    this.state = {
      refreshing: true,
      UserLogin: null,
      List: [
        {
          title: 'Đổi mật khẩu',
          iconname: 'lock',
          icontype: 'feather',
          code: ItemCode.SETTING,
        },
        {
          title: 'Lịch sử chấm công định vị',
          iconname: 'download-cloud',
          icontype: 'feather',
          code: ItemCode.LOGCHECKIN,
        },
        {
          title: 'Đăng xuất',
          iconname: 'log-out',
          icontype: 'feather',
          code: ItemCode.LOGOUT,
        },
      ],
      DataUp: {
        PasswordOld: '',
      },
    };
  }
  onIds(device) {
    DEVICE = device;
  }
  componentDidMount = () => {
    this.loadData();
  };
  componentWillReceiveProps(nextProps) {
    this.loadData();
  }
  loadData() {
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 5000);
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.getProfile()
          .then(rs => {
            this.setState({ UserLogin: rs.data, refreshing: false });
          })
          .catch(error => {
            console.log(error.data);
            this.setState({ refreshing: false });
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('getProfile');
        this.setState({ UserLogin: x.data || {}, refreshing: false });
      }
    });
  }
  render() {
    return !this.state.refreshing ? (
      <View style={[AppStyles.container]}>
        <View style={[AppStyles.Drawer_HeaderContainer, { marginTop: 20 }]}>
          <View style={{}}>
            <Thumbnail
              source={API_HR.GetPicApplyLeaves(
                this.state.UserLogin?.EmployeeGuid,
              )}
            />
          </View>
          <View style={{ justifyContent: 'center', marginLeft: 10 }}>
            <Text style={[AppStyles.h2, { color: AppColors.blueFacebook }]}>
              {this.state.UserLogin?.FullName}
            </Text>
            <Text style={AppStyles.Textdefault}>
              {this.state.UserLogin?.EmployeeId}
            </Text>
          </View>
        </View>
        <ScrollView>
          {this.state.List.map((item, index) => (
            <TouchableOpacity
              style={{ justifyContent: 'center' }}
              onPress={() => this.itemPress(item, index)}>
              <View style={[styles.ViewFlatList]}>
                <Icon name={item.iconname} type={item.icontype} />
                <Text
                  style={
                    (AppStyles.Textdefault, { color: item.color, marginLeft: 10 })
                  }>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </ScrollView>
        <View style={{ bottom: 15, alignItems: 'center' }}>
          <Text style={[AppStyles.h6, { color: AppColors.gray }]}>
            Phiên bản 1.1
          </Text>
        </View>
      </View>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  itemPress(item, index) {
    let List = this.state.List;
    this.setState({ List: List });
    switch (item.code) {
      case ItemCode.LOGOUT:
        this.RemoveDevice();
        this.props.logout();
        Actions.login(ActionConst.RESET);
        break;
      case ItemCode.SETTING:
        Actions.editpass();
        break;
      case ItemCode.LOGCHECKIN:
        Actions.checkinHistory();
        break;
    }
  }
  RemoveDevice() {
    try {
      var _item = {
        DeviceId: DEVICE.userId,
        LoginName: this.state.UserLogin.LoginName,
        OrganizationGuid: this.state.UserLogin.OrganizationGuid,
        ModuleId: 'MOBILE',
      };
      API.RemoveDevice(_item)
        .then(rs => { })
        .catch(error => {
          console.log('Lỗi Khi xoá thiết bị', error);
        });
    } catch (ex) {
      console.log('Module : LOGIN_FORM, ERROR: "Can not load user login"', ex);
    }
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    marginRight: 10,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
    marginLeft: 10,
  },
});

//Redux
const mapStateToProps = state => ({
  user: state.user.account,
});

const mapDispatchToProps = {
  getProfile: UserActions.getProfile,
  logout: UserActions.logout,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawerComponent);
