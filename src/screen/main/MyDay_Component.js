import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  PermissionsAndroid,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import { ActionConst, Actions } from 'react-native-router-flux';
import { Divider, ListItem, Icon } from 'react-native-elements';
import * as UserActions from '@redux/user/actions';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { FuncCommon } from '@utils';
import { listCombobox, controller } from '@Home';
import Toast from 'react-native-simple-toast';
import Geolocation from '@react-native-community/geolocation';
import { API, API_TM_TASKS } from '@network';
import AsyncStorage from '@react-native-async-storage/async-storage';
import _ from 'lodash';
import {
  check,
  PERMISSIONS,
  RESULTS,
  request,
  openSettings,
} from 'react-native-permissions';
import LoadingComponent from '../component/LoadingComponent';
const KeyLog = 'LogTimeKeeping';
const _styles = StyleSheet.create({
  Style_ViewAll: {
    flexDirection: 'row',
    // flexWrap: 'wrap',
    // justifyContent: 'flex-start',
  },
  image: {
    flex: 1,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
class MyDay_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ListMyDay: [],
      refreshing: false,
    };
    //châm công
    this.serverLoca = null;
    this.localLoca = null;
    this.props.ReloadTab(this._reloadData);
    this.currentDate = new Date();
    this.loading = false;
  }
  componentDidMount() {
    this.checkMyDay();
    this.getProfile();
  }
  componentDidUpdate = prevProps => {
    //check app come from background or inactive to reload data
    if (!_.isEqual(this.props.appState, prevProps.appState)) {
      if (
        prevProps.appState.match(/inactive|background/) &&
        this.props.appState === 'active'
      ) {
        this.checkMyDay();
      }
    }
  };
  getProfile = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.getProfile()
          .then(rs => {
            this.setState({ UserLogin: rs.data });
          })
          .catch(error => {
            console.log(error.data);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('getProfile');
        this.setState({ UserLogin: x.data || {} });
      }
    });
  };
  checkPermisstionIos = async () => {
    check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
      .then(async result => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            Toast.showWithGravity(
              'Thiết bị không hỗ trợ',
              Toast.SHORT,
              Toast.CENTER,
            );
            break;
          case RESULTS.DENIED:
            const res2 = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
            if (res2 === RESULTS.GRANTED) {
              this.iosPermisstionGranted();
            } else {
              Toast.showWithGravity(
                'Bạn cần cấp quyền truy cập vị trí!',
                Toast.SHORT,
                Toast.CENTER,
              );
            }
            break;
          case RESULTS.LIMITED:
            Toast.showWithGravity(
              'Quyền truy cập bị giới hạn!',
              Toast.SHORT,
              Toast.CENTER,
            );
            break;
          case RESULTS.GRANTED:
            this.iosPermisstionGranted();
            console.log('The permission is granted');
            break;
          case RESULTS.BLOCKED:
            setTimeout(() => {
              openSettings();
            }, 1500);
            Toast.showWithGravity(
              'Bạn cần cấp quyền truy cập vị trí!',
              Toast.SHORT,
              Toast.CENTER,
            );
            break;
        }
      })
      .catch(error => {
        Toast.showWithGravity('Có lỗi xảy ra', Toast.SHORT, Toast.CENTER);
        console.log(error);
      });
  };
  iosPermisstionGranted = async () => {
    await Geolocation.requestAuthorization();
    Geolocation.getCurrentPosition(
      d => this._currenLocation(d),
      error => {
        console.log(error);
        if (error.code === 2) {
          Toast.showWithGravity(
            'Yêu cầu bật định vị',
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      },
      {
        timeout: 10000, // thoi gian cho lay vi tri
        maximumAge: 1000, // thoi gian luu cache cua vi tri
      },
    );
  };
  _reloadData = () => {
    controller.CheckToday(rs => {
      this.setState({ ListMyDay: rs, refreshing: false });
    });
  };
  checkMyDay = () => {
    this.setState({ refreshing: true }, () => {
      controller.CheckToday(rs => {
        this.setState({ ListMyDay: rs, refreshing: false });
      });
    });
  };
  render() {
    return this.state.refreshing ? (
      <View style={{ height: 100 }}>
        <LoadingComponent backgroundColor={'transparent'} />
      </View>
    ) : (
      <ScrollView
        ref={ref => {
          this.myScroll = ref;
        }}>
        {this.state.ListMyDay && this.state.ListMyDay.length > 0 ? (
          <View style={_styles.Style_ViewAll}>
            {this.state.ListMyDay.map((item, i) => (
              <TouchableOpacity
                style={[_styles.image]}
                onPress={() => this.onclick(item)}>
                {item.CheckValue === 'N' ? (
                  <Icon
                    name={item.IconName}
                    type={item.IconType}
                    size={30}
                    color={AppColors.StatusTask_CG}
                  />
                ) : item.CheckValue === 'Y' ? (
                  <Icon
                    name={item.IconName}
                    type={item.IconType}
                    size={30}
                    color={AppColors.StatusTask_HT}
                  />
                ) : (
                  <Icon
                    name={'loader'}
                    type={'feather'}
                    size={30}
                    color={AppColors.StatusTask_CG}
                  />
                )}
                <Text style={{}}>{item.Title}</Text>
              </TouchableOpacity>
            ))}
          </View>
        ) : null}
      </ScrollView>
    );
  }

  onclick = item => {
    switch (item.Code) {
      case 'TimeKeeperLogsLocation':
        if (item.CheckValue === 'Y') {
          Toast.showWithGravity(
            'Bạn đã chấm công giờ vào lúc: ' + item.Value,
            Toast.SHORT,
            Toast.CENTER,
          );
        }
        this._setCheckin(item);
        break;
      case 'TimeKeeperLogsLocationOUT':
        this._setCheckin(item);
        break;

      default:
        break;
    }
  };
  //#region chấm công
  _setCheckin = async item => {
    FuncCommon.Data_Offline(async d => {
      if (!d) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
    });
    await API_TM_TASKS.GetLocationCheckin({})
      .then(async rs => {
        // set current Date get from server
        this.currentDate = rs.data.Time ? new Date(rs.data.Time) : new Date();
        //// lay gio server de tranh chinh gio tai dien thoai
        if (rs.data.Status) {
          if (
            rs.data.Data !== undefined &&
            rs.data.Data != null &&
            rs.data.Data !== '' &&
            rs.data.Data !== '[]'
          ) {
            var _d = JSON.parse(JSON.parse(rs.data.Data))[0];
            this.serverLoca = _d;
            this.AboutPostion = _d.AboutPostion;
            if (Platform.OS === 'ios') {
              await this.checkPermisstionIos();
            } else {
              await this.requestLocationPermission();
            }
          }
        } else {
          if (item.CheckValue === 'Y') {
            Toast.showWithGravity(
              'Không lấy được vị trí chấm công. Trước đó bạn đã chấm giờ ra là: ' +
              item.Value,
              Toast.SHORT,
              Toast.CENTER,
            );
            return;
          }
          Toast.showWithGravity(
            'Không lấy được vị trí chấm công.',
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  _actionCheckin() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 1000);
    let _EmployeeGuid =
      global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid ||
      this.state.UserLogin.EmployeeGuid;
    if (!_EmployeeGuid) {
      Toast.showWithGravity(
        'Phiên làm việc đã hết hạn, mời bạn đăng nhập lại',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.props.logout();
      Actions.login(ActionConst.RESET);
      return;
    }
    // debouce không chấm công liên tục 2 lần => sau 1 giây
    var edittime =
      this.currentDate.getHours() >= 12
        ? FuncCommon.ConDate(this.currentDate, 75) + ' ' + 'PM'
        : FuncCommon.ConDate(this.currentDate, 75) + ' ' + 'AM';
    var _o = [
      {
        MachineNumber: 1,
        IndRegID: '',
        DateTimeRecord: edittime,
        DateOnlyRecord: FuncCommon.ConDate(this.currentDate, 2),
        TimeOnlyRecord: FuncCommon.ConDate(this.currentDate, 76).toString(),
        CheckTime: this.currentDate.getHours() >= 12 ? 'PM' : 'AM',
        IPMachine: '',
        IPPort: '',
        EmployeeGuid: _EmployeeGuid,
        // Latitude: this.state.initialRegion.latitude,
        // Longitude: this.state.initialRegion.longitude,
        //DeviceId: this.serverLoca.DeviceID
      },
    ];
    ////// -------------->
    this.checkFileExist(_o[0]);
    ////// <-------------- save log timeKeeper
    API_TM_TASKS.TimeKeeperLogs_add(_o)
      .then(rs => {
        Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
        if (rs.data.errorCode === 200) {
          clearInterval(this.time);
          this.checkMyDay();
        }
      })
      .catch(error => {
        console.log('Lỗi chấm công :' + error);
      });
  }
  checkFileExist = async logData => {
    console.log('begin ghi file log');
    var _d = await AsyncStorage.getItem(KeyLog);
    var dataJSon = JSON.parse(_d);
    if (!dataJSon) {
      console.log('Không tìm thấy dữ liệu');
      this.writeFile([logData]);
      return;
    }
    this.writeFile([...dataJSon, logData]);
  };

  writeFile = async data => {
    try {
      await AsyncStorage.setItem(KeyLog, JSON.stringify(data));
    } catch (ex) {
      await AsyncStorage.setItem(KeyLog, JSON.stringify([]));
    }
  };

  // RemoveDevice() {
  //     try {
  //         var _item = {
  //             DeviceId: DEVICE.userId,
  //             LoginName: this.state.UserLogin.LoginName,
  //             OrganizationGuid: this.state.UserLogin.OrganizationGuid,
  //             ModuleId: 'MOBILE',
  //         };
  //         API.RemoveDevice(_item)
  //             .then(rs => { })
  //             .catch(error => {
  //                 console.log('Lỗi Khi xoá thiết bị', error);
  //             });
  //     } catch (ex) {
  //         console.log('Module : LOGIN_FORM, ERROR: "Can not load user login"', ex);
  //     }
  // }

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Quyền vị trí',
          message: 'Bạn cần cấp quyền cho vị trí',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Geolocation.getCurrentPosition(
          d => this._currenLocation(d),
          err => {
            this.errorCallback_highAccuracy(err);
          },
        );
      } else {
        Toast.showWithGravity(
          'Không có quyền truy cập vị trí.',
          Toast.SHORT,
          Toast.CENTER,
        );
      }
    } catch (err) {
      console.log(err);
    }
  }
  errorCallback_highAccuracy = error => {
    // callback khi 1 số thiết bị cũ bị chậm
    if (error.code === 2) {
      Toast.showWithGravity('Yêu cầu bật định vị', Toast.SHORT, Toast.CENTER);
    }
    if (error.code === 3) {
      Geolocation.getCurrentPosition(
        d => this._currenLocation(d),
        err => {
          this.errorCallback_lowAccuracy(err);
        },
        { timeout: 5000, enableHighAccuracy: true },
      );
    }
  };
  errorCallback_lowAccuracy = error => {
    if (error.code === 2) {
      Toast.showWithGravity('Yêu cầu bật định vị', Toast.SHORT, Toast.CENTER);
    }
    if (error.code === 3) {
      Toast.showWithGravity(
        'Có lỗi xảy ra khi lấy vị trí định vị',
        Toast.SHORT,
        Toast.CENTER,
      );
    }
  };
  _currenLocation = d => {
    console.log(d);
    this.localLoca = d.coords;
    this.__showButtonCheckin();
  };
  distance = (lat1, lon1, lat2, lon2) => {
    var p = 0.017453292519943295; // Math.PI / 180
    var c = Math.cos;
    var a =
      0.5 -
      c((lat2 - lat1) * p) / 2 +
      (c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))) / 2;
    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  };
  __showButtonCheckin() {
    if (
      !this.localLoca ||
      !this.serverLoca ||
      !this.serverLoca.latitude ||
      !this.serverLoca.longitude ||
      !this.localLoca.latitude ||
      !this.localLoca.longitude
    ) {
      Toast.showWithGravity(
        'Không tìm thấy vị trí chấm công.',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let distance =
      this.distance(
        this.serverLoca.latitude,
        this.serverLoca.longitude,
        this.localLoca.latitude,
        this.localLoca.longitude,
      ) * 1000;

    if (distance <= Number(this.AboutPostion)) {
      this._actionCheckin();
    } else {
      Toast.showWithGravity(
        'Không tìm thấy vị trí chấm công.',
        Toast.SHORT,
        Toast.CENTER,
      );
    }
  }
  //#endregion
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {
  getProfile: UserActions.getProfile,
  logout: UserActions.logout,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyDay_Component);
