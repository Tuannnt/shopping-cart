import { AppStyles, AppColors } from '@theme';
export default {
    ListMyDay: [
        {
            Title: 'Chấm công',
            color: 'red',
            Icon: {
                name: "checkcircleo",
                type: "antdesign"
            }
        },
        {
            Title: 'Có ăn',
            color: 'green',
            Icon: {
                name: "checkcircleo",
                type: "antdesign"
            }
        },
        {
            Title: 'Làm thêm',
            color: 'blue',
            Icon: {
                name: "checkcircleo",
                type: "antdesign"
            }
        },
    ]
}