import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  Animated,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import TabNavigator from 'react-native-tab-navigator';
import {Button, Icon} from 'react-native-elements';
import {AppStyles, AppColors} from '@theme';
import {API} from '@network';
import {LoadingComponent} from '@Component';
import Notification_Component from './Notification_Component';
import ViewHomeComponent from './ViewHomeComponent';
import NotificationNV_Component from './NotificationNV_Component';
import SettingNV_Component from './SettingNV_Component';
import ListAll_Messange_Component from '../MS/ListAll_Messange_Component';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const TimeOut = 200;
var setPopUp = false;
const DataExtension = [
  {
    icon: 'maximize',
    typeIcon: 'feather',
    title: 'Nhập xuất kho',
    key: 'voucherAMViewItem',
  },
  {
    icon: 'maximize',
    typeIcon: 'feather',
    title: 'Đề nghị nhập',
    key: 'addTicketRequests',
  },
  {
    icon: 'maximize',
    typeIcon: 'feather',
    title: 'Kiểm kê kho',
    key: 'amAddTicketChecking',
  },
];
class MenuBottomComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Hiệu ứng
      PopUp: false,
      TabViewOffsetY: new Animated.Value(1000),
      opacityTabView: new Animated.Value(0),
      selectedTab: this.props.keyselectTab ?? 'home',
      open: false,
      DatachatGroupNew: this.props.DatachatGroupNew,
      LocationId: this.props.LocationId,
    };
    //#region Hiệu ứng
    this.OpenTabView = Animated.timing(this.state.TabViewOffsetY, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    this.OffTabView = Animated.timing(this.state.TabViewOffsetY, {
      toValue: DRIVER.height,
      duration: TimeOut,
      useNativeDriver: true,
    });
    this.opacityTabView = Animated.timing(this.state.opacityTabView, {
      toValue: 1,
      duration: TimeOut,
      useNativeDriver: true,
    });
    this.OffopacityTabView = Animated.timing(this.state.opacityTabView, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion
    this.props.CallbackView(this.CallbackView);
  }
  setSelectedTab = tab => {
    this.setState({selectedTab: tab});
  };
  componentDidMount = () => {
    console.log('iiiiiiiiiiiiiiiiiiiiiii' + this.props.BackModuleByCode);
  };
  CallbackView = () => {
    this._ReloadMessage();
  };
  _ReloadMessage() {}
  ReloadMessage = d => {
    this._ReloadMessage = d;
  };
  ReloadTab = d => {
    this._ReloadMessage = d;
  };
  render() {
    return this.props.CountNotification === null ? (
      <LoadingComponent backgroundColor={'#ffffff'} />
    ) : (
      <View style={{flex: 1, backgroundColor: '#ffffff'}}>
        <TabNavigator tabBarStyle={AppStyles.TabNavigator_Container}>
          <TabNavigator.Item
            tabStyle={AppStyles.TabNavigator_Item}
            selected={this.state.selectedTab === 'home'}
            title={'Cá nhân'}
            renderIcon={() => (
              <Icon
                iconStyle={AppStyles.TabNavigator_Icon}
                name="user"
                type={'feather'}
              />
            )}
            renderSelectedIcon={() => (
              <Icon
                iconStyle={AppStyles.TabNavigator_IconClick}
                name="user"
                type={'feather'}
              />
            )}
            onPress={() => this.setState({selectedTab: 'home'})}>
            <ViewHomeComponent
              BackModuleByCode={this.props.BackModuleByCode}
              ReloadTab={this.ReloadTab}
            />
          </TabNavigator.Item>
          {/* <TabNavigator.Item
            tabStyle={AppStyles.TabNavigator_Item}
            selected={this.state.selectedTab === 'chat'}
            title={'Chat'}
            renderIcon={() => (
              <Icon
                iconStyle={AppStyles.TabNavigator_Icon}
                name="message-square"
                type={'feather'}
              />
            )}
            renderSelectedIcon={() => (
              <Icon
                iconStyle={AppStyles.TabNavigator_IconClick}
                name="message-square"
                type={'feather'}
              />
            )}
            onPress={() => this.setState({selectedTab: 'chat'})}>
            <ListAll_Messange_Component
              ReloadMessage={this.ReloadMessage}
              DatachatGroupNew={this.state.DatachatGroupNew}
            />
          </TabNavigator.Item> */}
          <TabNavigator.Item
            tabStyle={AppStyles.TabNavigator_Item}
            selected={this.state.selectedTab === 'notification'}
            title={'Thông báo'}
            renderBadge={() => this.CustomBadgeView()}
            renderIcon={() => (
              <Icon
                iconStyle={AppStyles.TabNavigator_Icon}
                name="bell"
                type={'feather'}
              />
            )}
            renderSelectedIcon={() => (
              <Icon
                iconStyle={AppStyles.TabNavigator_IconClick}
                name="bell"
                type={'feather'}
              />
            )}
            onPress={() => this.setState({selectedTab: 'notification'})}>
            <NotificationNV_Component ReloadTab={this.ReloadTab} />
          </TabNavigator.Item>
          <TabNavigator.Item
            tabStyle={AppStyles.TabNavigator_Item}
            selected={this.state.selectedTab === 'setting'}
            title={'Menu'}
            renderIcon={() => (
              <Icon
                iconStyle={AppStyles.TabNavigator_Icon}
                name="menu"
                type={'feather'}
              />
            )}
            renderSelectedIcon={() => (
              <Icon
                iconStyle={AppStyles.TabNavigator_IconClick}
                name="menu"
                type={'feather'}
              />
            )}
            onPress={() => this.setState({selectedTab: 'setting'})}>
            <SettingNV_Component />
          </TabNavigator.Item>
        </TabNavigator>
        {this.state.PopUp === true ? this.renderPopUp() : null}
      </View>
    );
  }
  CustomBadgeView = () => {
    if (!this.props.CountNotification) {
      return null;
    }
    return (
      <View style={{backgroundColor: 'red', padding: 1, borderRadius: 50}}>
        <Text style={{fontSize: 10, color: '#fff'}}>
          {this.props.CountNotification}
        </Text>
      </View>
    );
  };
  renderPopUp = () => {
    const opacityTabView = this.state.opacityTabView.interpolate({
      inputRange: [0, 1],
      outputRange: [-4, 1],
    });
    const opacityTabViewv2 = this.state.opacityTabView.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 0.5],
    });
    return ((
      // setPopUp === true ?
      <View
        style={{
          position: 'absolute',
          bottom: 60,
          height: DRIVER.height,
          width: DRIVER.width,
        }}>
        <Animated.View
          style={[
            {
              backgroundColor: 'black',
              opacity: opacityTabViewv2,
              height: DRIVER.height,
            },
          ]}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() => {
              this.setPopUp();
            }}
          />
        </Animated.View>
        <Animated.View
          style={{
            bottom: 0,
            position: 'absolute',
            width: DRIVER.width,
            transform: [
              {translateY: this.state.TabViewOffsetY},
              {scale: (opacityTabView, opacityTabView)},
            ],
          }}>
          <View
            style={{
              flexDirection: 'column',
              maxHeight: DRIVER.height / 4,
              margin: 10,
              backgroundColor: 'white',
              borderRadius: 10,
              padding: 5,
            }}>
            <ScrollView style={{width: DRIVER.width}}>
              <View style={[_styles.imageGrid, {width: DRIVER.width}]}>
                {DataExtension.map(item => {
                  return (
                    <TouchableOpacity
                      style={{padding: 10, width: 82}}
                      onPress={() => this.NextTabQRCode(item.key)}>
                      <Icon
                        iconStyle={{color: AppColors.Maincolor}}
                        name={item.icon}
                        type={item.typeIcon}
                        size={35}
                      />
                      <Text style={{textAlign: 'center'}}>{item.title}</Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </ScrollView>
          </View>
        </Animated.View>
      </View> /*: null*/ /*: null*/ /*: null*/ /*: null*/ /*: null*/ /*: null*/ /*: null*/
      // : null
    ) /*: null*/);
  };
  setPopUp = () => {
    if (setPopUp === false) {
      this.setState({PopUp: !this.state.PopUp});
      setPopUp = !setPopUp;
      Animated.sequence([this.opacityTabView]).start();
      Animated.sequence([this.OpenTabView]).start();
    } else {
      Animated.sequence([this.OffTabView]).start();
      Animated.sequence([this.OffopacityTabView]).start();
      setTimeout(() => {
        setPopUp = false;
        this.setState({PopUp: !this.state.PopUp});
      }, 500);
    }
  };
  NextTabQRCode = key => {
    Actions.qrCodeV2({link: key});
  };
}
const mapStateToProps = state => {
  const {notification} = state;
  return {CountNotification: notification.CountNotification};
};
const mapDispatchToProps = {};
//custom ảnh
const _styles = StyleSheet.create({
  imageGrid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  image: {
    width: DRIVER.width / 3.03,
    height: DRIVER.height / 3.03,
    margin: 0.5,
  },
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(MenuBottomComponent);
