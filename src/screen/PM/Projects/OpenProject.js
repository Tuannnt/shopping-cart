import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Icon, Header, Divider} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {API_HR} from '@network';
import {API_Projects} from '@network';
import {Left, Container, Item, Thumbnail, ListItem} from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import {WebView} from 'react-native-webview';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import _ from 'lodash';
const ItemCode = {
  PERSONNEL: 0,
  SELL: 1,
  CUSTOMER: 2,
  PURCHASE: 3,
  ACCOUNTANT: 4,
  WAREHOUSE: 5,
  OPERATIONAL: 6,
  DOCUMENT: 7,
  MARKETING: 8,
  PROJECT: 9,
  MANUFACTURING: 10,
  PROPERTY: 11,
  ESTABLISH: 12,
  LOGOUT: 13,
};
class OpenProject extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      list: [],
      Data: [],
      listDetail: [],
      listMember: [],
      ProjectGuid: '',
      stylex: 1,
      selectedTab: 'puzzle-outline',
    };
    this.listtabbarBotom = [
      {
        Title: 'Trang chủ',
        Icon: 'home-outline',
        Type: 'material-community',
        Value: 'home-outline',
        Checkbox: false,
      },
      {
        Title: 'Hạng mục',
        Icon: 'puzzle-outline',
        Type: 'material-community',
        Value: 'puzzle-outline',
        Checkbox: false,
      },
      {
        Title: 'Công việc',
        Icon: 'logo-buffer',
        Type: 'ionicon',
        Value: 'logo-buffer',
        Checkbox: false,
      },
      {
        Title: 'Lịch',
        Icon: 'calendar-clock',
        Type: 'material-community',
        Value: 'calendar-clock',
        Checkbox: false,
      },
    ];
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'home-outline':
        Actions.app();
        break;
      case 'puzzle-outline':
        this.onClick_HM();
        break;
      case 'logo-buffer':
        this.onClickAprover();
        break;
      case 'calendar-clock':
        this.onClickNotApprove();
        break;
      default:
        break;
    }
  }
  // sự kiện click hạng mục
  onClick_HM = () => {
    this.state.selectedTab = 'puzzle-outline';
    this.setState({
      list: [],
      selectedTab: this.state.selectedTab,
    });
    Actions.ProjectItems(this.props.ProjectGuid);
  };
  onClick(data) {
    if (data == 'GDDA') {
      this.setState({
        stylex: 2,
      });
      this.setState({list: []});
      //this.GetAll_HD();
    } else if (data == 'DA') {
      this.setState({
        stylex: 1,
      });
    } else if (data == 'DDA') {
      this.setState({
        stylex: 3,
      });
      this.setState({list_lg: []});
      //this.GetAll_LG();
    }
  }
  openAttachments() {
    var obj = {
      ModuleId: '7',
      RecordGuid: this.props.InsuranceGuid,
    };
    Actions.attachmentComponent(obj);
  }
  componentDidMount(): void {
    this.getItem();
  }
  getItem() {
    let id = {IdS: [this.props.ProjectGuid]};
    API_Projects.Project_GetProject(id)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data).model,
          listDetail: JSON.parse(res.data.data).detail,
          listMember: JSON.parse(res.data.data).member,
        });
        console.log('===========> error' + res.data.data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  keyExtractor = (item, index) => index.toString();
  render() {
    return (
      <Container>
        <TouchableWithoutFeedback
          style={{flex: 30}}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View
            style={{
              flex: 1,
              fontSize: 11,
              fontFamily: Fonts.base.family,
              backgroundColor: 'white',
            }}>
            <TabBar_Title
              title={'Thông tin dự án'}
              callBack={() => Actions.pop()}
              FormAttachment={true}
              CallbackFormAttachment={() => this.openAttachments()}
            />
            <View
              style={{
                height: 40,
                flexDirection: 'row',
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              {this.state.stylex == 1 ? (
                <TouchableOpacity
                  onPress={() => this.onClick('DA')}
                  style={styles.ClickPending}>
                  <Text style={styles.FormSize}>Thông tin chung</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.onClick('DA')}
                  style={styles.TabarPending}>
                  <Text style={styles.FormSize}>Thông tin chung</Text>
                </TouchableOpacity>
              )}
              {this.state.stylex == 2 ? (
                <TouchableOpacity
                  onPress={() => this.onClick('GDDA')}
                  style={styles.ClickApprove}>
                  <Text style={styles.FormSize}>Giai đoạn </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.onClick('GDDA')}
                  style={styles.TabarApprove}>
                  <Text style={styles.FormSize}>Giai đoạn</Text>
                </TouchableOpacity>
              )}
              {this.state.stylex == 3 ? (
                <TouchableOpacity
                  onPress={() => this.onClick('DDA')}
                  style={styles.ClickApprove_BH}>
                  <Text style={styles.FormSize}>Đội dự án</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.onClick('DDA')}
                  style={styles.TabarApprove_BH}>
                  <Text style={styles.FormSize}>Đội dự án</Text>
                </TouchableOpacity>
              )}
            </View>
            <View style={{flex: 10}}>
              <View style={{padding: 20, height: '100%'}}>
                {this.state.stylex == 1 ? (
                  <ScrollView style={{}}>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>Mã dự án :</Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ProjectId}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tiêu đề dự án :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ProjectName}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.DepartmentName}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Người tạo phiếu :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.EmployeeName}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Thuộc dự án (nếu có) :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ParentName}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Dự án phát sinh :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsIncurred == 'Y'
                            ? 'Đơn hàng phát sinh có phí'
                            : this.state.Data.IsIncurred == 'N'
                            ? 'Không phải đơn hàng phát sinh'
                            : this.state.Data.IsIncurred == 'F'
                            ? 'Đơn hàng phát sinh không phí'
                            : null}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Bắt đầu dự kiến :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={AppStyles.Labeldefault}>
                          Bắt đầu thực tế :
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.StartDate)}
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.ActualStartDate)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Kết thúc dự kiến :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={AppStyles.Labeldefault}>
                          Kết thúc thực tế :
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.EndDate)}
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.ActualEndDate)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày giao dự kiến :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày giao thực tế :
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.PlanDeliveryDate)}
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.ActualDeliveryDate)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <Divider style={{marginBottom: 10, marginTop: 10}} />
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Số hiệu máy hoặc sản phẩm (nếu có) :
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ProductNo}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tên máy hoặc sản phẩm dự án :
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ProductName}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 10}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tổng giá trị dự kiến ban đầu{' '}
                        </Text>
                      </View>
                      <View style={styles.FormALl}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.addPeriod(this.state.Data.AmountPlan)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 10}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tổng tiền thực tế ban đầu{' '}
                        </Text>
                      </View>
                      <View style={styles.FormALl}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.addPeriod(this.state.Data.AmountActual)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 10}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tổng mức chênh lệch của dự án{' '}
                        </Text>
                      </View>
                      <View style={styles.FormALl}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.addPeriod(this.state.Data.AmountDifference)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 10}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tỉ lệ % giữa thực tế và dự kiến{' '}
                        </Text>
                      </View>
                      <View style={styles.FormALl}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.DifferenceRate}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Địa chỉ khách hàng:
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.Address}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          % hoàn thành:
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>Phiên bản:</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.PercentComplete}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.Version}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tên đầy đủ người phê duyệt:
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ApprovedBy}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Người kiểm tra, đánh giá dự án:
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.CheckerBy}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Chú thích (nếu có):
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.Note}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <Divider style={{marginBottom: 10, marginTop: 10}} />
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tổng thời gian DK:
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tổng thời gian TT:
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.PlanDuration}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ActualDuration}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Mức độ ưu tiên:
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>Trạng thái:</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.Priority == 'L'
                            ? 'Thấp'
                            : this.state.Data.Priority == 'N'
                            ? 'Bình thường'
                            : this.state.Data.Priority == 'H'
                            ? 'Cao'
                            : this.state.Data == 'V'
                            ? 'Rất cao'
                            : null}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.Status == 'N'
                            ? 'Chưa bắt đầu'
                            : this.state.Data.Status == 'P'
                            ? 'Đang thực hiện'
                            : this.state.Data.Status == 'C'
                            ? 'Đã hoàn thành'
                            : this.state.Data.Status == 'W'
                            ? 'Đợi bộ phận khác'
                            : this.state.Data.Status == 'D'
                            ? 'Bị hoãn lại'
                            : this.state.Data.Status == 'K'
                            ? 'Khác'
                            : null}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>Kích hoạt:</Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Trạng thái duyệt:
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsActive == 1
                            ? 'Đã kích hoạt'
                            : this.state.Data.IsActive == 0
                            ? 'Chưa kích hoạt'
                            : null}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsApproved == 1
                            ? 'Đã duyệt'
                            : this.state.Data.IsApproved == 0
                            ? 'Chưa duyệt'
                            : null}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Phiên bản hiện tại:
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsCurrentVersion == 1
                            ? 'Phiên bản hiện tại'
                            : this.state.Data.IsCurrentVersion == 0
                            ? 'Không phải phiên bản hiện tại'
                            : null}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Hoàn thành vượt kế hoạch:
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsBeyondPlan == 'Y'
                            ? 'Hoàn thành vượt kế hoạch'
                            : this.state.Data.IsBeyondPlan == 'N'
                            ? 'Không vượt kế hoạch'
                            : null}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </ScrollView>
                ) : null}
                {this.state.stylex == 2 ? (
                  <FlatList
                    style={{marginStart: -20, height: '100%'}}
                    ref={ref => {
                      this.ListView_Ref = ref;
                    }}
                    ListEmptyComponent={this.ListEmpty}
                    ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                    keyExtractor={item => item.BankGuid}
                    data={this.state.listDetail}
                    renderItem={({item, index}) => (
                      <ScrollView>
                        <View>
                          <ListItem>
                            <View style={{flexDirection: 'row'}}>
                              <View style={{flex: 8}}>
                                <Text style={[AppStyles.Titledefault]}>
                                  {item.PhaseName}{' '}
                                </Text>
                                <Text style={[AppStyles.Textdefault]}>
                                  Trạng thái:{' '}
                                  {item.IsActive == 1
                                    ? 'Hoạt động'
                                    : item.IsActive == 0
                                    ? 'Chưa hoạt động'
                                    : null}
                                </Text>
                              </View>
                              <View style={{flex: 4, alignItems: 'flex-end'}}>
                                <Text style={[AppStyles.Textdefault]}>
                                  {this.customDate(item.StartDate)}
                                </Text>
                                <Text style={[AppStyles.Textdefault]}>
                                  {this.customDate(item.EndDate)}
                                </Text>
                              </View>
                            </View>
                          </ListItem>
                        </View>
                      </ScrollView>
                    )}
                    onEndReachedThreshold={0.5}
                  />
                ) : null}
                {this.state.stylex == 3 ? (
                  <FlatList
                    style={{marginStart: -25, height: '100%'}}
                    ref={ref => {
                      this.ListView_Ref = ref;
                    }}
                    ListEmptyComponent={this.ListEmpty}
                    ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                    keyExtractor={item => item.BankGuid}
                    data={this.state.listMember}
                    renderItem={({item, index}) => (
                      <ScrollView>
                        <View>
                          <ListItem>
                            <View style={{flexDirection: 'row'}}>
                              <View style={{flex: 1}}>
                                <Thumbnail
                                  source={API_HR.GetPicApplyLeaves(
                                    item.EmployeeGuid,
                                  )}
                                />
                              </View>
                              <View style={{flex: 3}}>
                                <Text style={[AppStyles.Titledefault]}>
                                  {item.EmployeeName}
                                </Text>
                                <Text style={[AppStyles.Textdefault]}>
                                  Thuộc phòng ban: {item.DepartmentName}
                                </Text>
                                <Text style={[AppStyles.Textdefault]}>
                                  Chức vụ: {item.CategoryRoleName}
                                </Text>
                              </View>
                            </View>
                          </ListItem>
                        </View>
                      </ScrollView>
                    )}
                    onEndReachedThreshold={0.5}
                  />
                ) : null}
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
        {/* hiển thị nút tuỳ chọn */}
        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback =>
              this.onCallbackValueBottom(callback)
            }
          />
        </View>
      </Container>
    );
  }
  onPressBack() {}
  ListEmpty = () => {
    if (
      this.state.listMember &&
      this.state.listMember != null &&
      this.state.listDetail.length > 0 &&
      this.state.listDetail != null
    )
      return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  closeComment() {
    this.setState({isModalVisible: !this.state.isModalVisible});
  }
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
  TabarPending: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  ClickPending: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
  },
  ClickApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
  },
  TabarApprove_BH: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  ClickApprove_BH: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  FormSize: {
    fontSize: 15,
  },
});
const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(OpenProject);
