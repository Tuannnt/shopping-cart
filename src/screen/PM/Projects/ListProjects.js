
import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, RefreshControl, TouchableWithoutFeedback, Keyboard, TouchableOpacity, ScrollView } from 'react-native';
import { Header, SearchBar, Icon, Badge } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { API_Projects } from '@network';
import API from "../../../network/API";
import { Container, ListItem } from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';

class ListProjects extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staticParam: {
                StartDate: null,
                EndDate: null,
                Status: "",
                SearchStatus: "",
                SearchPriority: "N",
                IsActive : true,
                OrderTypeId: "",
                CategoryPhases: "",
                NumberPage: 0,
                Length: 100,
                search: {
                    value: ""
                }
            },
            EvenFromSearch: false,
            name: '',
            refreshing: false,
            list: [],

        };
    }
    componentDidMount() {
        this.nextPage();
    }
    render() {
        let arrayholder = [];
        return (
            <Container>
                <View style={{ flex: 3 }}>
                    <TouchableWithoutFeedback style={{ flex: 30 }} onPress={() => {
                        Keyboard.dismiss()
                    }}>
                        <View style={{ flex: 1 }}>
                            <TabBar
                                title={'Danh sách dự án'}
                                FormSearch={true}
                                CallbackFormSearch={(callback) => this.setState({ EvenFromSearch: callback })}
                                BackModuleByCode={'PM'}
                            />
                            {/* <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, borderWidth: 0.5, flexDirection: 'row' }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{}}>
                                    <TouchableOpacity onPress={() => this.OnChangeAll()} style={styles.TabarPending}>
                                        <Text style={this.state.staticParam.SearchPriority == '' ? { color: AppColors.Maincolor } : ''}>Tất cả</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.OnChange_L()} style={[{ flex: 1, width: 60, alignItems: 'center', justifyContent: 'center', borderColor: '#C0C0C0', borderWidth: 0.5 }]}>
                                        <Text style={this.state.staticParam.SearchPriority == 'L' ? { color: AppColors.Maincolor } : ''}>Thấp</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.OnChange_N()} style={[{ flex: 1, width: 80, alignItems: 'center', justifyContent: 'center', borderColor: '#C0C0C0', borderWidth: 0.5 }]}>
                                        <Text style={this.state.staticParam.SearchPriority == 'N' ? { color: AppColors.Maincolor } : ''}>Bình thường</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.OnChange_H()} style={[{ flex: 1, width: 60, alignItems: 'center', justifyContent: 'center', borderColor: '#C0C0C0', borderWidth: 0.5 }]}>
                                        <Text style={this.state.staticParam.SearchPriority == 'H' ? { color: AppColors.Maincolor } : ''}>Cao</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.OnChange_V()} style={styles.TabarApprove}>
                                        <Text style={this.state.staticParam.SearchPriority == 'V' ? { color: AppColors.Maincolor } : ''}>Rất cao</Text>
                                    </TouchableOpacity>
                                </ScrollView>
                            </View> */}
                            {this.state.EvenFromSearch == true ?
                                <SearchBar
                                    placeholder="Tìm kiếm..."
                                    lightTheme
                                    round
                                    inputContainerStyle={{ backgroundColor: '#e1ecf4' }}
                                    containerStyle={AppStyles.FormSearchBar}
                                    onChangeText={text => this.updateSearch(text)}
                                    value={this.state.staticParam.search.value}
                                /> : null}
                            <ScrollView>
                                <View style={{ flex: 14 }}>
                                    <FlatList
                                        style={{ marginBottom: 60, height: '100%' }}
                                        ref={(ref) => {
                                            this.ListView_Ref = ref;
                                        }}
                                        // ListEmptyComponent={this.ListEmpty}
                                        ListHeaderComponent={this.renderHeader}
                                        ListFooterComponent={this.renderFooter}
                                        keyExtractor={item => item.BankGuid}
                                        data={this.state.list}
                                        renderItem={({ item, index }) => (
                                            <ScrollView>
                                                <View >
                                                    <ListItem onPress={() => this.openView(item.ProjectGuid)}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <View style={{ flex: 2 }}>
                                                                <Icon
                                                                    color={'#808080'}
                                                                    name={'folderopen'}
                                                                    type='antdesign' />
                                                            </View>
                                                            <View style={{ flex: 8 }}>
                                                                <Text style={[AppStyles.Titledefault]}>{item.ProjectName}</Text>
                                                                <Text style={[AppStyles.Textdefault]}>Mã dự án: {item.ProjectId}</Text>
                                                                <Text style={[AppStyles.Textdefault]}>Ưu tiên: {item.Priority == 'L' ? 'Thấp' : item.Priority == 'N' ? 'Bình thường' : item.Priority == 'H' ? 'Cao' : item.Priority == 'V' ? 'Rất cao' : null}</Text>
                                                                <Text style={[AppStyles.Textdefault]}>Số ngày: {item.PlanDuration}</Text>
                                                            </View>
                                                            <View style={{ flex: 4, alignItems: 'flex-end' }}>
                                                                <Text style={[AppStyles.Textdefault]}>{this.customDate(item.StartDate)}</Text>
                                                                <Text style={[AppStyles.Textdefault]}>{this.customDate(item.EndDate)}</Text>
                                                            </View>
                                                        </View>
                                                    </ListItem>
                                                </View>
                                            </ScrollView>
                                        )}
                                        onEndReachedThreshold={0.5}
                                        onEndReached={() =>{this.state.list.length >= (this.state.staticParam.NumberPage * this.state.staticParam.Length)?  this.nextPage(): null}}
                                    />
                                </View>
                            </ScrollView>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </Container>
        )
    }
    openView(id) {
        Actions.OpenProject({ ProjectGuid: id });
        console.log('===================chuyen Id:' + id);
    }
    EvenFromSearch() {
        if (this.state.EvenFromSearch === false) {
            this.setState({ EvenFromSearch: true })
        }
        else {
            this.setState({ EvenFromSearch: false })
        }
    }
    updateSearch = text => {
        this.state.staticParam.search.value = text;
        this.setState({
            list: [],
            staticParam: {
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                Status: this.state.staticParam.Status,
                SearchStatus: this.state.SearchStatus,
                IsActive: this.state.IsActive,
                SearchPriority: this.state.SearchPriority,
                OrderTypeId: this.state.OrderTypeId,
                CategoryPhases: this.state.CategoryPhases,
                NumberPage: 1,
                Length: 15,
                search: {
                    value: this.state.staticParam.search.value
                }
            }
        });
        this.GetAll();
    }
    nextPage() {
        this.setState({
            staticParam: {
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                Status: this.state.staticParam.Status,
                SearchStatus: this.state.SearchStatus,
                IsActive: this.state.IsActive,
                SearchPriority: this.state.SearchPriority,
                OrderTypeId: this.state.OrderTypeId,
                CategoryPhases: this.state.CategoryPhases,
                NumberPage: 1,
                Length: 15,
                search: {
                    value: this.state.staticParam.search.value
                }
            }
        });
        this.GetAll();
    }
    GetAll = () => {
        API_Projects.Project_GetProjects(this.state.staticParam).then(res => {
            for (let i = 0; i < JSON.parse(res.data.data).length; i++) {
                this.state.list.push(JSON.parse(res.data.data)[i]);
            }
            this.setState({ list:this.state.list})
        })
            .catch(error => {
                console.log('===========> error');
                console.log(error);
            });
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    //định dạng tiền tệ
    addPeriod(nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
}
const styles = StyleSheet.create({

    statusNew: {
        marginTop: 5,
        width: 8,
        height: 8,
        borderRadius: 4,
        borderWidth: 0.1,
        borderColor: 'white',
        backgroundColor: '#4baf57',
    },
    TabarPending: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        width: 70
    },
    TabarApprove: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        width: 70
    },
    tabNavigatorItem: {
        justifyContent: 'space-between',
        alignSelf: 'center',
    },
    itemIcon: {
        fontWeight: '600',
        fontSize: 23,
        color: 'black',
    },
    itemIconClick: {
        fontWeight: '600',
        fontSize: 23,
        color: '#f6b801',
    },
})
//Redux
const mapStateToProps = state => ({
})
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(ListProjects);