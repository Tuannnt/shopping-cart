import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Icon, Header, Divider} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {API_HR} from '@network';
import {API_Projects} from '@network';
import {Left, Container, Item, Thumbnail, ListItem} from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import {WebView} from 'react-native-webview';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import _ from 'lodash';
const ItemCode = {
  PERSONNEL: 0,
  SELL: 1,
  CUSTOMER: 2,
  PURCHASE: 3,
  ACCOUNTANT: 4,
  WAREHOUSE: 5,
  OPERATIONAL: 6,
  DOCUMENT: 7,
  MARKETING: 8,
  PROJECT: 9,
  MANUFACTURING: 10,
  PROPERTY: 11,
  ESTABLISH: 12,
  LOGOUT: 13,
};
class OpenProjectItems extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      Data: [],
      list_cost: {},
      list_ProjectItemGuid_Estimate: {},
      listMember: [],
      ProjectItemGuid: '',
      stylex: 1,
    };
  }
  onClick(data) {
    if (data == 'CP') {
      this.setState({
        stylex: 2,
      });
      this.setState({list: []});
      //this.GetAll_HD();
    } else if (data == 'HM') {
      this.setState({
        stylex: 1,
      });
    } else if (data == 'NV') {
      this.setState({
        stylex: 3,
      });
      this.setState({list_lg: []});
      //this.GetAll_LG();
    }
  }
  openAttachments() {
    var obj = {
      ModuleId: '7',
      RecordGuid: this.props.InsuranceGuid,
    };
    Actions.attachmentComponent(obj);
  }
  componentDidMount(): void {
    this.getItem();
  }
  getItem() {
    let id = {IdS: [this.props.ProjectItemGuid]};
    API_Projects.Project_GetProjectItem(id)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data).ProjectItem,
          list_cost: JSON.parse(res.data.data).ProjectItem_Cost,
          list_ProjectItemGuid_Estimate: JSON.parse(res.data.data)
            .ProjectItem_Estimate,
          listMember: JSON.parse(res.data.data).ProjectItem_Member,
        });
        console.log('===========> error' + res.data.data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  keyExtractor = (item, index) => index.toString();
  render() {
    return (
      <Container>
        <TouchableWithoutFeedback
          style={{flex: 30}}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View
            style={{
              flex: 1,
              fontSize: 11,
              fontFamily: Fonts.base.family,
              backgroundColor: 'white',
            }}>
            <TabBar_Title
              title={'Chi tiết hạng mục'}
              callBack={() => Actions.pop()}
              FormAttachment={true}
              CallbackFormAttachment={() => this.openAttachments()}
            />
            <View
              style={{
                height: 40,
                flexDirection: 'row',
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              {this.state.stylex == 1 ? (
                <TouchableOpacity
                  onPress={() => this.onClick('HM')}
                  style={styles.ClickPending}>
                  <Text style={styles.FormSize}>Hạng mục</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.onClick('HM')}
                  style={styles.TabarPending}>
                  <Text style={styles.FormSize}>Hạng mục</Text>
                </TouchableOpacity>
              )}
              {this.state.stylex == 2 ? (
                <TouchableOpacity
                  onPress={() => this.onClick('CP')}
                  style={styles.ClickApprove}>
                  <Text style={styles.FormSize}>Chi phí </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.onClick('CP')}
                  style={styles.TabarApprove}>
                  <Text style={styles.FormSize}>Chi phí</Text>
                </TouchableOpacity>
              )}
              {this.state.stylex == 3 ? (
                <TouchableOpacity
                  onPress={() => this.onClick('NV')}
                  style={styles.ClickApprove_BH}>
                  <Text style={styles.FormSize}>Nhân viên</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.onClick('NV')}
                  style={styles.TabarApprove_BH}>
                  <Text style={styles.FormSize}>Nhân viên</Text>
                </TouchableOpacity>
              )}
            </View>
            <View style={{flex: 10}}>
              <View style={{padding: 20, height: '100%'}}>
                {this.state.stylex == 1 ? (
                  <ScrollView>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>Dự án :</Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ProjectName}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tên hạng mục dự án :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.Title}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Thuộc hạng mục dự án :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.ParentName}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Giai đoạn dự án :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.PhaseName}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 2}}>
                        <Text style={AppStyles.Labeldefault}>
                          Người tham gia phụ trách :
                        </Text>
                      </View>
                      <View style={{flex: 3}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.OtherPerson}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày bắt đầu :
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày kết thúc :
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.PlanStartDate)}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.PlanEndDate)}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày bắt đầu thực tế :
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày kết thúc thực tế :
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.ActualStartDate)}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.ActualEndDate)}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Số ngày làm :
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Số ngày cảnh báo :
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.DifferenceDate}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.WarningDay}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          % tỉ lệ dự án :
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Vấn đề tồn tại :
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.PercentProject} %
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsIssueExist == 1
                            ? 'Vấn đề tồn tại'
                            : this.state.Data.IsIssueExist == 0
                            ? 'Mục dự đoán ban đầu'
                            : null}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Mức độ ưu tiên :
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Thứ tự sắp xếp :
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.Priority == 'L'
                            ? 'Thấp'
                            : this.state.Data.Priority == 'N'
                            ? 'Bình thường'
                            : this.state.Data.Priority == 'H'
                            ? 'Cao'
                            : this.state.Data == 'V'
                            ? 'Rất cao'
                            : null}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.OrderId}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Trạng thái phê duyệt :
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsApproved == 1
                            ? 'Hoàn thành'
                            : this.state.Data.IsApproved == 0
                            ? 'Không hoàn thành'
                            : null}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsLocked == 1
                            ? 'Khóa'
                            : this.state.Data.IsLocked == 0
                            ? 'Chưa khóa'
                            : null}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>Mô tả :</Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 8}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.Description}
                        </Text>
                      </View>
                    </View>
                  </ScrollView>
                ) : null}
                {this.state.stylex == 2 && this.state.list_cost.length > 0 ? (
                  <ScrollView>
                    <View>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Tên chi phí hạng mục :
                          </Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <View style={{flex: 1}}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.state.list_cost[0].CostName}
                          </Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Chỉ số của hạng mục :
                          </Text>
                        </View>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>Bí danh :</Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <View style={{flex: 1}}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.state.list_cost[0].Bulleted}
                          </Text>
                        </View>
                        <View style={{flex: 1}}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.state.list_cost[0].Alias}
                          </Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Chi phí dự kiến :
                          </Text>
                        </View>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            chi phí thực tế :
                          </Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <View style={{flex: 1}}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.list_cost[0].PlanCost)}
                          </Text>
                        </View>
                        <View style={{flex: 1}}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.list_cost[0].ActualCost)}
                          </Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>Mô tả :</Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <View style={{flex: 1}}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.state.list_cost[0].Description}
                          </Text>
                        </View>
                      </View>
                      <Divider style={{marginBottom: 10, marginTop: 10}} />
                      <FlatList
                        style={{marginStart: -15, height: '100%'}}
                        ref={ref => {
                          this.ListView_Ref = ref;
                        }}
                        ListEmptyComponent={this.ListEmpty}
                        ListHeaderComponent={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                        keyExtractor={item => item.BankGuid}
                        data={this.state.list_ProjectItemGuid_Estimate}
                        renderItem={({item, index}) => (
                          <ScrollView>
                            <View>
                              <ListItem>
                                <View style={{flexDirection: 'row'}}>
                                  <View style={{flex: 8}}>
                                    <Text
                                      style={{
                                        FormSize: 13,
                                        fontWeight: 'bold',
                                      }}>
                                      {item.EstimateName}
                                    </Text>
                                    <Text style={[AppStyles.Textdefault]}>
                                      Đơn vị tính: {item.UnitId}
                                    </Text>
                                  </View>
                                  <View
                                    style={{flex: 3, alignItems: 'flex-end'}}>
                                    <Text style={[AppStyles.Textdefault]}>
                                      {item.Quantity}
                                    </Text>
                                    <Text style={[AppStyles.Textdefault]}>
                                      {this.addPeriod(item.Price)}
                                    </Text>
                                  </View>
                                </View>
                              </ListItem>
                            </View>
                          </ScrollView>
                        )}
                        onEndReachedThreshold={0.5}
                      />
                    </View>
                  </ScrollView>
                ) : null}
                {this.state.stylex == 3 ? (
                  <View>
                    <FlatList
                      style={{marginStart: -25, height: '100%'}}
                      ref={ref => {
                        this.ListView_Ref = ref;
                      }}
                      ListEmptyComponent={this.ListEmpty}
                      ListHeaderComponent={this.renderHeader}
                      ListFooterComponent={this.renderFooter}
                      keyExtractor={item => item.BankGuid}
                      data={this.state.listMember}
                      renderItem={({item, index}) => (
                        <ScrollView>
                          <View>
                            <ListItem>
                              <View style={{flexDirection: 'row'}}>
                                <View style={{flex: 1}}>
                                  <Thumbnail
                                    source={API_HR.GetPicApplyLeaves(
                                      item.EmployeeGuid,
                                    )}
                                  />
                                </View>
                                <View style={{flex: 3}}>
                                  <Text style={[AppStyles.Titledefault]}>
                                    {item.EmployeeName}
                                  </Text>
                                  <Text style={[AppStyles.Textdefault]}>
                                    Thuộc phòng ban: {item.DepartmentName}
                                  </Text>
                                  <Text style={[AppStyles.Textdefault]}>
                                    Chức vụ: {item.CategoryRoleName}
                                  </Text>
                                </View>
                              </View>
                            </ListItem>
                          </View>
                        </ScrollView>
                      )}
                      onEndReachedThreshold={0.5}
                    />
                  </View>
                ) : null}
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
  ListEmpty = () => {
    if (
      this.state.listMember.length > 0 &&
      this.state.listMember.length > 0 &&
      this.state.list_ProjectItemGuid_Estimate.length > 0 &&
      this.state.list_ProjectItemGuid_Estimate != null
    )
      return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  onPressBack() {
    Actions.RegisterCars();
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  closeComment() {
    this.setState({isModalVisible: !this.state.isModalVisible});
  }
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    fontSize: 14,
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
  },
  ClickApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
  },
  TabarApprove_BH: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  ClickApprove_BH: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  FormSize: {
    fontSize: 15,
  },
});
const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(OpenProjectItems);
