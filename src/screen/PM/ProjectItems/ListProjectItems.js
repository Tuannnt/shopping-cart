
import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, RefreshControl, TouchableWithoutFeedback, Keyboard, TouchableOpacity, ScrollView } from 'react-native';
import { Header, SearchBar, Icon, Badge } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { API_Projects } from '@network';
import API from "../../../network/API";
import { Container, ListItem } from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import MultiSelect_Component from '../../component/MultiSelect_Component';
import Searchable_Dropdown from '../../component/Searchable_Dropdown';

class ListProjectItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staticParam: {
                ProjectGuid: "",
                StartDate: null,
                EndDate: null,
                LoginName: "",
                search: {
                    value: ""
                }
            },
            ProjectGuid: "",
            ListProject: [],
            EvenFromSearch: false,
            name: '',
            refreshing: false,
            list: [],
            ListCountNoti: [],
            Notapprove: '',
            selectedTab: 'profile',
            Notapprove_Advance: '',

        };
    }
    componentDidMount() {
        this.GetAll();
        this.GetProject();
        if (this.props.data && this.props.data != null && this.props.data != "") {
            this.state.staticParam.ProjectGuid = this.props.data;
            this.GetAll();
        }
    }
    loadMoreData() {
        this.nextPage();
    }
    render() {
        let arrayholder = [];
        return (
            <Container>
                <View style={{ flex: 1 }}>
                    <TouchableWithoutFeedback style={{ flex: 10 }} onPress={() => {
                        Keyboard.dismiss()
                    }}>
                        <View style={{ flex: 1 }}>
                            <TabBar
                                title={'Hạng mục dự án'}
                                FormSearch={true}
                                CallbackFormSearch={(callback) => this.setState({ EvenFromSearch: callback })}
                                BackModuleByCode={'PM'}
                            />
                            {this.state.ListProject.length > 0 ?
                                <Searchable_Dropdown
                                    ListCombobox={this.state.ListProject}
                                    placeholder='Chọn dự án'
                                    defaultItems={0}
                                    CallBackListCombobox={(id) => this.onValueChange2(id)}
                                />
                                : null
                            }
                            {this.state.EvenFromSearch == true ?
                                <SearchBar
                                    placeholder="Tìm kiếm..."
                                    lightTheme
                                    round
                                    inputContainerStyle={{ backgroundColor: '#e1ecf4' }}
                                    containerStyle={AppStyles.FormSearchBar}
                                    onChangeText={text => this.updateSearch(text)}
                                    value={this.state.staticParam.search.value}
                                /> : null}
                            <View style={{ flex: 1 }}>
                                <FlatList
                                    style={{ marginBottom: 60, height: '100%' }}
                                    ref={(ref) => {
                                        this.ListView_Ref = ref;
                                    }}
                                    ListEmptyComponent={this.ListEmpty}
                                    ListHeaderComponent={this.renderHeader}
                                    ListFooterComponent={this.renderFooter}
                                    keyExtractor={item => item.BankGuid}
                                    data={this.state.list}
                                    renderItem={({ item, index }) => (
                                        <ScrollView style={{}}>
                                            <View >
                                                <ListItem onPress={() => this.openView(item.ProjectItemGuid)}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View style={{ flex: 2 }}>
                                                            <Icon
                                                                color={'#808080'}
                                                                name={'folderopen'}
                                                                type='antdesign' />
                                                        </View>
                                                        <View style={{ flex: 8 }}>
                                                            <Text style={[AppStyles.Titledefault]}>{item.Title}</Text>
                                                            <Text style={[AppStyles.Textdefault]}>Tiến độ : {item.PercentCompleted} %</Text>
                                                            <Text style={[AppStyles.Textdefault]}>BĐ kế hoạch : {this.customDate(item.PlanStartDate)}</Text>
                                                            <Text style={[AppStyles.Textdefault]}>HT kế hoạch : {this.customDate(item.PlanEndDate)}</Text>
                                                        </View>
                                                        <View style={{ flex: 4, alignItems: 'flex-end' }}>
                                                            <Text style={[AppStyles.Textdefault]}>{this.customDate(item.ActualStartDate)}</Text>
                                                            <Text style={[AppStyles.Textdefault]}>{this.customDate(item.ActualEndDate)}</Text>
                                                        </View>
                                                    </View>
                                                </ListItem>
                                            </View>
                                        </ScrollView>
                                    )}
                                    onEndReachedThreshold={0.5}
                                />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </Container>
        )
    }
    openView(id) {
        Actions.OpenProjectItems({ ProjectItemGuid: id });
        console.log('===================chuyen Id:' + id);
    }
    EvenFromSearch() {
        if (this.state.EvenFromSearch === false) {
            this.setState({ EvenFromSearch: true })
        }
        else {
            this.setState({ EvenFromSearch: false })
        }
    }
    onValueChange2(value) {
        this.setState({
            id: value
        });
        this.state.staticParam.ProjectGuid = value;
        let rs = this.state.ListProject.find(x => x.ProjectGuid === value)
        if (rs != null) {
            this.setState({ ProjectName: rs.ProjectName })
        }
        this.GetAll();
    }
    updateSearch = text => {
        this.state.staticParam.search.value = text;
        this.setState({
            list: [],
            staticParam: {
                ProjectGuid: this.state.staticParam.ProjectGuid,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                LoginName: this.state.staticParam.LoginName,
                search: {
                    value: this.state.staticParam.search.value
                }
            }
        });
        this.GetAll();
    }
    nextPage() {
        this.setState({
            staticParam: {
                ProjectGuid: this.state.staticParam.ProjectGuid,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                LoginName: this.state.staticParam.LoginName,
                search: {
                    value: this.state.staticParam.search.value
                }
            }
        });
        this.GetAll();
    }
    GetProject() {
        let _obj = {};
        API_Projects.Project_GetProject_Select(_obj).then(rs => {
            var _listEmp = JSON.parse(rs.data.data);
            let _reListEmp = [];
            for (var i = 0; i < _listEmp.length; i++) {
                _reListEmp.push({
                    name: _listEmp[i].ProjectName,
                    id: _listEmp[i].ProjectGuid,
                });
            }
            this.setState(
                {
                    ListProject: _reListEmp,
                });
        }).catch(error => {
            console.log('Error when call API GetEmployee Mobile.')
            console.log(error)
        });
    }
    GetAll = () => {
        API_Projects.Project_GetProjectItems(this.state.staticParam).then(res => {
            this.setState({ list: JSON.parse(res.data.data) })
        })
            .catch(error => {
                console.log('===========> error');
                console.log(error);
            });
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    //định dạng tiền tệ
    addPeriod(nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    ListEmpty = () => {
        if (this.state.list && this.state.list != null && this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
}
const styles = StyleSheet.create({

    statusNew: {
        marginTop: 5,
        width: 8,
        height: 8,
        borderRadius: 4,
        borderWidth: 0.1,
        borderColor: 'white',
        backgroundColor: '#4baf57',
    },
    tabNavigatorItem: {
        justifyContent: 'space-between',
        alignSelf: 'center',
    },
    itemIcon: {
        fontWeight: '600',
        fontSize: 23,
        color: 'black',
    },
    itemIconClick: {
        fontWeight: '600',
        fontSize: 23,
        color: '#f6b801',
    },
})
//Redux
const mapStateToProps = state => ({
})
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(ListProjectItems);