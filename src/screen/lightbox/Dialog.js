import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Actions, Overlay} from 'react-native-router-flux';
import {
  TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,
  TouchableWithoutFeedback,
  TouchableNativeFeedback,
  Animated,
  Easing,
  StyleSheet,
  Image,
  View,
  Text,
  Button,
  ScrollView
} from 'react-native';
import {AppStyles, AppSizes, AppColors} from '@theme'
import _ from 'lodash';
import * as Animatable from 'react-native-animatable';

const {width, height} = Dimensions.get('window');

const DefaultConfig = {
  maxWidthPercentage: 0.85,
  maxHeightPercentage: 0.85,
}

const OS = {
  // iOS: Platform.OS === 'ios',
  iOS: true,
}

const configOS = {
  borderRadius: OS.iOS ? 5 : 2,
  hasDivider: OS.iOS,
  titleAlign: OS.iOS ? 'center' : 'left',
  buttonStyle: OS.iOS ? {flex: 1} : {}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dialog: {
    backgroundColor: '#fff',
    width: Math.min(width, height) * DefaultConfig.maxWidthPercentage,
    maxHeight: height * DefaultConfig.maxHeightPercentage,
    borderRadius: configOS.borderRadius,
    borderWidth: 0,
  },
  title: {
    ...AppStyles.h3,
    alignSelf: 'stretch',
    textAlign: configOS.titleAlign,
    margin: AppSizes.paddingSmall,
  },
  textContent: {
    ...AppStyles.h4,
    padding: AppSizes.paddingMedium,
  },
  divider: {
    backgroundColor: '#00000020',
    alignSelf: 'stretch',
  },
  button: {
    flex: 1,
  },
  buttonText: {
    ...AppStyles.h4,
    textAlign: 'center',
    padding: AppSizes.paddingSmall,
    backgroundColor: 'transparent'
  }
});

const dialogDefaultProps = {
  positiveText: 'Ok',
  negativeText: null,
  neutralText: null,
  positiveColor: AppColors.textPrimary,
  negativeColor: AppColors.secondary,
  neutralColor: AppColors.textSecondary,
  positiveAction: null,
  negativeAction: null,
  neutralAction: null,
  title: 'Title',
  content: 'Content',
  customContent: null,
  titleColor: '#333',
  contentColor: '#333',
  dismissListener: null,
}

class Dialog extends Component {

  static propTypes = {
    positiveText: PropTypes.string,
    negativeText: PropTypes.string,
    neutralText: PropTypes.string,
    positiveColor: PropTypes.string,
    negativeColor: PropTypes.string,
    neutralColor: PropTypes.string,
    positiveAction: PropTypes.func,
    negativeAction: PropTypes.func,
    neutralAction: PropTypes.func,
    title: PropTypes.string,
    content: PropTypes.string,
    customContent: PropTypes.any,
    titleColor: PropTypes.string,
    contentColor: PropTypes.string,
    dismissListener: PropTypes.func,
    height: PropTypes.any,
  }

  static defaultProps = {
    ...dialogDefaultProps
  }

  render() {
    return (
      /** Dim background and handle touch outside */
      <Animatable.View ref="overlay" duration={300} animation="fadeIn" style={styles.container}>
        <Animatable.View ref="body" animation="zoomIn" duration={300} style={this.dialogStyle()}>
          {this.renderTitle()}
          {this.renderHorizontalDivider()}
          <ScrollView>
            {this.renderContent()}
          </ScrollView>
          {this.renderHorizontalDivider()}
          {this.renderButtons()}
        </Animatable.View>
      </Animatable.View>
    );
  }

  dialogStyle() {
    if (this.props.height) {
      let dialogHeight = undefined;
      if (typeof this.props.height === 'number') {
        dialogHeight = this.props.height;
      } else if (typeof this.props.height === 'string' && this.props.height.endsWith('%')) {
        dialogHeight = height * (parseFloat(this.props.height) / 100);
      }
      return [styles.dialog, {height: dialogHeight}];
    }
    return [styles.dialog];
  }

  renderHorizontalDivider() {
    if (configOS.hasDivider) {
      return (<View style={[styles.divider, {height: 1,}]}/>)
    }
    return null;
  }

  renderVerticalDivider() {
    if (configOS.hasDivider) {
      return (<View style={[styles.divider, {width: 1,}]}/>);
    }
    return null;
  }

  renderTitle() {
    return (<Text style={[styles.title, {color: this.props.titleColor,}]}>{this.props.title}</Text>);
  }

  renderContent() {
    if (this.props.customContent) {
      return this.props.customContent;
    }
    // For testing

    // return (
    //   <View>
    //     {Array.apply(null, { length: 20 }).map(Number.call, Number).map(() => {
    //       return (<Text style={[styles.textContent, { color: this.props.contentColor }]}>{this.props.content}</Text>);
    //     })}
    //   </View>
    // );

    return (<Text style={[styles.textContent, {color: this.props.contentColor}]}>{this.props.content}</Text>);
  }

  renderButtons() {
    if (!this.props.positiveText && !this.props.negativeText && !this.props.neutralText) return null;
    const numberOfButtons =
      (this.props.positiveText ? 1 : 0) +
      (this.props.negativeText ? 1 : 0) +
      (this.props.neutralText ? 1 : 0);
    return (
      <View style={{flexDirection: 'row-reverse', alignItems: 'center',}}>
        {this.renderPositiveButton()}
        {numberOfButtons > 1 ? this.renderVerticalDivider() : null}
        {this.renderNegativeButton()}
        {numberOfButtons > 2 ? this.renderVerticalDivider() : null}
        {this.renderNeutralButton()}
      </View>
    );
  }

  renderPositiveButton() {
    if (!this.props.positiveText) return null;
    return (
      <View style={configOS.buttonStyle}>
        <TouchableOpacity onPress={() => this._onPositivePress()}>
          <Text style={[styles.buttonText, {color: this.props.positiveColor,}]}>{this.props.positiveText}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderNegativeButton() {
    if (!this.props.negativeText) return null;
    return (
      <View style={configOS.buttonStyle}>
        <TouchableOpacity onPress={() => this._onNegativePress()}>
          <Text style={[styles.buttonText, {color: this.props.negativeColor}]}>{this.props.negativeText}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderNeutralButton() {
    if (!this.props.neutralText) return null;
    return (
      <View style={configOS.buttonStyle}>
        <TouchableOpacity onPress={() => this._onNeutralPress()}>
          <Text style={[styles.buttonText, {color: this.props.neutralColor}]}>{this.props.neutralText}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  _onPositivePress() {
    if (this.props.positiveAction) {
      if (!this.props.positiveAction()) {
        this.dismiss();
      }
    } else {
      this.dismiss();
    }
  }

  _onNegativePress() {
    this.dismiss();
    if (this.props.negativeAction) {
      this.props.negativeAction();
    }
  }

  _onNeutralPress() {
    this.dismiss();
    if (this.props.neutralAction) {
      this.props.neutralAction();
    }
  }

  dismiss() {
    // Promise.all(this.refs.body.zoomOut(300), this.refs.overlay.fadeOut(300))
    //   .then(endState => {
    //     if (endState) {
    //       Actions.pop();
    //       if (this.props.dismissListener) {
    //         this.props.dismissListener();
    //       }
    //     }
    //   })
    //   .catch(() => {
    //     Actions.pop();
    //     if (this.props.dismissListener) {
    //       this.props.dismissListener();
    //     }
    //   })

    // For issue close Progress immediately so do not run Animation here
    // Fix it later
    Actions.pop();
    if (this.props.dismissListener) {
      this.props.dismissListener();
    }
  }

  static show(props) {
    Actions.dialog({...dialogDefaultProps, ...props});
  }
}

export default Dialog;
