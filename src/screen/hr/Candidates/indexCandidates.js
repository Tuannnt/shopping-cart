import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  FlatList,
  StyleSheet,
  RefreshControl,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
} from 'react-native';
import {Header, SearchBar, Icon, Badge} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_RegisterCars} from '@network';
import API from '../../../network/API';
import {Container, ListItem, Button} from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '@utils';

const SCREEN_WIDTH = Dimensions.get('window').width;
class indexCandidates extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ValueSearchDate: '4', // 30 ngày trước

      staticParam: {
        CurrentPage: 1,
        StartDate: new Date(),
        EndDate: new Date(),
        Status: '',
        NumberPage: 1,
        Length: 10,
        search: {
          value: '',
        },
      },
      name: '',
      refreshing: false,
      list: [],
      ListCountNoti: [],
      Pending: '',
      NotApprove_RegisterCars: '',
      selectedTab: '',
      isFetching: false,
      EvenFromSearch: false,
    };
  }
  setStartDate = date => {
    this.setState(
      prevState => ({
        staticParam: {...prevState.staticParam, StartDate: date},
      }),
      () => {
        this.nextPage();
      },
    );
  };
  setEndDate = date => {
    this.setState(
      prevState => ({
        staticParam: {...prevState.staticParam, EndDate: date},
      }),
      () => {
        this.nextPage();
      },
    );
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.setState(
        prevState => ({
          staticParam: {
            ...prevState.staticParam,
            EndDate: new Date(callback.end),
            StartDate: new Date(callback.start),
            ValueSearchDate: callback.value,
          },
        }),
        () => {
          this.nextPage();
        },
      );
    }
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  loadMoreData() {
    this.nextPage();
  }
  render() {
    let arrayholder = [];
    return (
      <Container>
        <View style={{flex: 3}}>
          <TouchableWithoutFeedback
            style={{flex: 30}}
            onPress={() => {
              Keyboard.dismiss();
            }}>
            <View style={{flex: 1}}>
              <TabBar
                title={'Hồ sơ ứng viên'}
                FormSearch={true}
                CallbackFormSearch={callback =>
                  this.setState({EvenFromSearch: callback})
                }
                BackModuleByCode={'HR'}
                //addForm={"addRegistercar"}
              />
              <View
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  borderRadius: 10,
                  flex: 1,
                  borderWidth: 0.5,
                  flexDirection: 'row',
                }}>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  style={{}}>
                  <TouchableOpacity
                    onPress={() => this.OnChangeAll()}
                    style={styles.TabarPending}>
                    <Text
                      style={
                        this.state.staticParam.Status == ''
                          ? {color: AppColors.Maincolor}
                          : {color: AppColors.black}
                      }>
                      Tất cả
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.OnChange_A()}
                    style={[
                      {
                        flex: 1,
                        width: 100,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#C0C0C0',
                        borderWidth: 0.5,
                      },
                    ]}>
                    <Text
                      style={
                        this.state.staticParam.Status == 'A'
                          ? {color: AppColors.Maincolor}
                          : {color: AppColors.black}
                      }>
                      Chờ lọc
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.OnChange_B()}
                    style={[
                      {
                        flex: 1,
                        width: 100,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#C0C0C0',
                        borderWidth: 0.5,
                      },
                    ]}>
                    <Text
                      style={
                        this.state.staticParam.Status == 'B'
                          ? {color: AppColors.Maincolor}
                          : {color: AppColors.black}
                      }>
                      Phỏng vấn
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.OnChange_C()}
                    style={[
                      {
                        flex: 1,
                        width: 100,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#C0C0C0',
                        borderWidth: 0.5,
                      },
                    ]}>
                    <Text
                      style={
                        this.state.staticParam.Status == 'C'
                          ? {color: AppColors.Maincolor}
                          : {color: AppColors.black}
                      }>
                      Chờ duyệt
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.OnChange_P()}
                    style={[
                      {
                        flex: 1,
                        width: 100,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#C0C0C0',
                        borderWidth: 0.5,
                      },
                    ]}>
                    <Text
                      style={
                        this.state.staticParam.Status == 'P'
                          ? {color: AppColors.Maincolor}
                          : {color: AppColors.black}
                      }>
                      Đã duyệt
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.OnChange_F()}
                    style={styles.TabarApprove}>
                    <Text
                      style={
                        this.state.staticParam.Status == 'F'
                          ? {color: AppColors.Maincolor}
                          : {color: AppColors.black}
                      }>
                      Không đạt
                    </Text>
                  </TouchableOpacity>
                </ScrollView>
              </View>
              {this.state.EvenFromSearch == true ? (
                <View>
                  <View
                    style={{
                      flexDirection: 'row',
                      paddingLeft: 10,
                      paddingRight: 10,
                    }}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'column',
                        padding: 5,
                      }}>
                      <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                        Thời gian bắt đầu
                      </Text>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() =>
                          this.setState({setEventStartDate: true})
                        }>
                        <Text>
                          {FuncCommon.ConDate(
                            this.state.staticParam.StartDate,
                            0,
                          )}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{flex: 1, flexDirection: 'column', padding: 5}}>
                      <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                        Thời gian kết thúc
                      </Text>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.setState({setEventEndDate: true})}>
                        <Text>
                          {FuncCommon.ConDate(
                            this.state.staticParam.EndDate,
                            0,
                          )}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'column', padding: 5}}>
                      <Text
                        style={[
                          AppStyles.Labeldefault,
                          {textAlign: 'center'},
                          styles.timeHeader,
                        ]}>
                        Lọc
                      </Text>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionSearchDate()}>
                        <Icon
                          name={'down'}
                          type={'antdesign'}
                          size={18}
                          color={AppColors.gray}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <SearchBar
                    placeholder="Tìm kiếm..."
                    lightTheme
                    round
                    inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                    containerStyle={AppStyles.FormSearchBar}
                    onChangeText={text => this.updateSearch(text)}
                    value={this.state.staticParam.search.value}
                  />
                </View>
              ) : null}

              {this.state.list ? (
                <View style={{flex: 14}}>
                  <FlatList
                    style={{marginBottom: 60, height: '100%'}}
                    refreshing={this.state.isFetching}
                    ref={ref => {
                      this.ListView_Ref = ref;
                    }}
                    ListEmptyComponent={this.ListEmpty}
                    //ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                    keyExtractor={item => item.BankGuid}
                    data={this.state.list}
                    renderItem={({item, index}) => (
                      <ScrollView style={{}}>
                        <View>
                          <ListItem
                            onPress={() => this.openView(item.CandidateGuid)}>
                            <View style={{flexDirection: 'row'}}>
                              <View style={{flex: 8}}>
                                <Text style={[AppStyles.Titledefault]}>
                                  {item.CandidateId}
                                </Text>
                                <Text style={[AppStyles.Textdefault]}>
                                  Tên ứng viên: {item.FullName}
                                </Text>
                                <Text style={[AppStyles.Textdefault]}>
                                  Ngày nộp hồ sơ:{' '}
                                  {this.customDate(item.ApplyDate)}
                                </Text>
                              </View>
                              <View style={{flex: 4, alignItems: 'flex-end'}}>
                                <Text style={[AppStyles.Textdefault]}>
                                  {this.customDate(item.BirthDate)}
                                </Text>
                                <Text style={[AppStyles.Textdefault]}>
                                  {item.Identification}
                                </Text>
                                {item.Status == 'Y' ? (
                                  <Text
                                    style={[
                                      {
                                        justifyContent: 'center',
                                        color: AppColors.AcceptColor,
                                      },
                                      AppStyles.Textdefault,
                                    ]}>
                                    {item.StatusWF}
                                  </Text>
                                ) : (
                                  <Text
                                    style={[
                                      {
                                        justifyContent: 'center',
                                        color: AppColors.PendingColor,
                                      },
                                      AppStyles.Textdefault,
                                    ]}>
                                    {item.StatusWF}
                                  </Text>
                                )}
                              </View>
                            </View>
                          </ListItem>
                        </View>
                      </ScrollView>
                    )}
                    onEndReachedThreshold={0.5}
                    onEndReached={() => this.loadMoreData()}
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                        tintColor="#f5821f"
                        titleColor="#fff"
                        colors={['black']}
                      />
                    }
                  />
                </View>
              ) : null}
            </View>
          </TouchableWithoutFeedback>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(), this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.staticParam.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(), this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.staticParam.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
        </View>
      </Container>
    );
  }
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({EvenFromSearch: true});
    } else {
      this.setState({EvenFromSearch: false});
    }
  }
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null && strDate != '') {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  updateSearch = text => {
    this.state.staticParam.search.value = text;
    this.setState({
      list: [],
      staticParam: {
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
        Status: this.state.staticParam.Status,
        NumberPage: 1,
        Length: 15,
        search: {
          value: this.state.staticParam.search.value,
        },
      },
    });
    console.log('keyword: ' + this.state.staticParam.Search);
    this.GetAll();
  };
  nextPage() {
    this.state.staticParam.CurrentPage++;
    this.setState({
      staticParam: {
        CurrentPage: this.state.staticParam.CurrentPage,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
        Status: this.state.staticParam.Status,
        NumberPage: 1,
        Length: 1000,
        search: {
          value: this.state.staticParam.search.value,
        },
      },
    });
    this.GetAll();
  }
  //Lọc trạng thái
  OnChangeAll() {
    this.state.list = [];
    this.state.staticParam.Status = '';
    this.setState({
      staticParam: {
        CurrentPage: this.state.staticParam.CurrentPage,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
        Status: this.state.staticParam.Status,
        NumberPage: 1,
        Length: 1000,
        search: {
          value: this.state.staticParam.search.value,
        },
      },
    });
    this.GetAll();
  }
  OnChange_A() {
    this.state.list = [];
    this.state.staticParam.Status = 'A';
    this.setState({
      staticParam: {
        CurrentPage: this.state.staticParam.CurrentPage,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
        Status: this.state.staticParam.Status,
        NumberPage: 1,
        Length: 1000,
        search: {
          value: this.state.staticParam.search.value,
        },
      },
    });
    this.GetAll();
  }
  OnChange_B() {
    this.state.list = [];
    this.state.staticParam.Status = 'B';
    this.setState({
      staticParam: {
        CurrentPage: this.state.staticParam.CurrentPage,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
        Status: this.state.staticParam.Status,
        NumberPage: 1,
        Length: 1000,
        search: {
          value: this.state.staticParam.search.value,
        },
      },
    });
    this.GetAll();
  }
  OnChange_C() {
    this.state.list = [];
    this.state.staticParam.Status = 'C';
    this.setState({
      staticParam: {
        CurrentPage: this.state.staticParam.CurrentPage,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
        Status: this.state.staticParam.Status,
        NumberPage: 1,
        Length: 1000,
        search: {
          value: this.state.staticParam.search.value,
        },
      },
    });
    this.GetAll();
  }
  OnChange_P() {
    this.state.list = [];
    this.state.staticParam.Status = 'P';
    this.setState({
      staticParam: {
        CurrentPage: this.state.staticParam.CurrentPage,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
        Status: this.state.staticParam.Status,
        NumberPage: 1,
        Length: 1000,
        search: {
          value: this.state.staticParam.search.value,
        },
      },
    });
    this.GetAll();
  }
  OnChange_F() {
    this.state.list = [];
    this.state.staticParam.Status = 'F';
    this.setState({
      staticParam: {
        CurrentPage: this.state.staticParam.CurrentPage,
        StartDate: this.state.staticParam.StartDate,
        EndDate: this.state.staticParam.EndDate,
        Status: this.state.staticParam.Status,
        NumberPage: 1,
        Length: 1000,
        search: {
          value: this.state.staticParam.search.value,
        },
      },
    });
    this.GetAll();
  }
  GetAll = () => {
    API_RegisterCars.Candidates_ListAll(this.state.staticParam)
      .then(res => {
        this.state.list = this.state.list.concat(
          JSON.parse(res.data.data).data,
        );
        this.setState({list: JSON.parse(res.data.data).data});
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
    API.CountStatus()
      .then(res => {
        var listCount = JSON.parse(res.data.data);
        this.setState({ListCountNoti: listCount});
        var _Pending = '';
        var _NotApprove_RegisterCars = '';
        var data = listCount.find(x => x.Name == 'Pending');
        if (data !== null && data !== undefined) {
          if (data.Value > 99) {
            _Pending = '99+';
          } else {
            _Pending = data.Value;
          }
          this.setState({Pending: _Pending});
        }
        var data1 = listCount.find(x => x.Name == 'NotApprove_RegisterCars');
        if (data1 !== null && data1 !== undefined) {
          if (data1.Value > 99) {
            _NotApprove_RegisterCars = '99+';
          } else {
            _NotApprove_RegisterCars = data1.Value;
          }
          this.setState({NotApprove_RegisterCars: _NotApprove_RegisterCars});
        }
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  openView(id) {
    Actions.openCandidates({RecordGuid: id});
    console.log('===================chuyen Id:' + id);
  }
  componentDidMount() {
    this.GetAll();
  }
  clickItem(item) {
    console.log('================> ' + JSON.stringify(item));
    switch (item) {
      case 'Back':
        Actions.app();
        break;
      case 'Open':
        Actions.openRegistercar();
        break;
      default:
        break;
    }
  }
}
const styles = StyleSheet.create({
  searchcontainer: {
    backgroundColor: 'white',
    height: 50,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0, //no effect
    shadowColor: 'white', //no effect
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  statusNew: {
    marginTop: 5,
    width: 8,
    height: 8,
    borderRadius: 4,
    borderWidth: 0.1,
    borderColor: 'white',
    backgroundColor: '#4baf57',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 100,
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 23,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 23,
    color: '#f6b801',
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(indexCandidates);
