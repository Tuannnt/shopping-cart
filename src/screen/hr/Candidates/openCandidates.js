import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import {Icon, Header} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {API_RegisterCars} from '@network';
import API from '../../../network/API';
import {DatePicker, Container, Item, Label, Picker} from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
const ItemCode = {
  PERSONNEL: 0,
  SELL: 1,
  CUSTOMER: 2,
  PURCHASE: 3,
  ACCOUNTANT: 4,
  WAREHOUSE: 5,
  OPERATIONAL: 6,
  DOCUMENT: 7,
  MARKETING: 8,
  PROJECT: 9,
  MANUFACTURING: 10,
  PROPERTY: 11,
  ESTABLISH: 12,
  LOGOUT: 13,
};
class openCandidates extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      Data: [],
      listWofflowData: [],
      loading: false,
      isModalVisible: false,
      txtComment: '',
      checkInLogin: '',
      LoginName: '',
      Date: null,
      StartTime: null,
      EndTime: null,
      selected2: [],
    };
  }
  componentDidMount(): void {
    this.getItem();
  }
  getItem() {
    let id = {Id: this.props.RecordGuid};
    API_RegisterCars.Candidatdes_Items(id)
      .then(res => {
        let date1 = JSON.parse(res.data.data).Date;
        let starttime1 = JSON.parse(res.data.data).StartTime;
        let endTime1 = JSON.parse(res.data.data).EndTime;
        let Date77 = new Date(date1);
        let StartTime11 = new Date(starttime1);
        let EndTime22 = new Date(endTime1);
        this.setState({
          Data: JSON.parse(res.data.data),
          Date: Date77,
          StartTime: StartTime11,
          EndTime: EndTime22,
        });
        this.state.Data.Tolls = this.state.Data.Tolls
          ? this.state.Data.Tolls.toString()
          : '';
        console.log('===========> error' + res.data.data);
        let checkin = {
          CandidateGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_RegisterCars.CheckLogin_Candidates(checkin)
          .then(res => {
            this.setState({checkInLogin: res.data.data});
            console.log('===checkin====' + res.data.data);
            console.log('--------' + this.state.Data.Permisstion);
          })
          .catch(error => {
            console.log(error.data.data);
          });
        API.getProfile().then(rs => {
          console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
          this.setState({LoginName: rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  keyExtractor = (item, index) => index.toString();
  render() {
    return (
      <Container>
        <TouchableWithoutFeedback
          style={{flex: 30}}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View
            style={{
              flex: 1,
              fontSize: 11,
              fontFamily: Fonts.base.family,
              backgroundColor: 'white',
            }}>
            <TabBar_Title
              title={'Chi tiết hồ sơ ứng viên'}
              callBack={() => this.onPressBack()}
              FormAttachment={true}
              CallbackFormAttachment={() => this.openAttachments()}
            />
            <View style={{flex: 10}}>
              <View style={{padding: 20}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Mã ứng viên :</Text>
                  <Text style={([AppStyles.Textdefault], {marginLeft: 10})}>
                    {this.state.Data.CandidateId}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Text style={AppStyles.Labeldefault}>Tên ứng viên :</Text>
                  <Text style={([AppStyles.Textdefault], {marginLeft: 10})}>
                    {this.state.Data.FullName}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Labeldefault}>Ngày sinh </Text>
                  </View>
                  <View style={styles.FormALl}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.customDate(this.state.Data.BirthDate)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Labeldefault}>Số CMT </Text>
                  </View>
                  <View style={styles.FormALl}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data.Identification}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Labeldefault}>Ngày cấp CMT </Text>
                  </View>
                  <View style={styles.FormALl}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.customDate(this.state.Data.IssueDate)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Text style={AppStyles.Labeldefault}>Giới tính :</Text>
                  <Text style={([AppStyles.Textdefault], {marginLeft: 10})}>
                    {this.state.Data.Gender == 'M'
                      ? 'Nam'
                      : this.state.Data.Gender == 'F'
                      ? 'Nữ'
                      : ''}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Labeldefault}>Sđt cá nhân </Text>
                  </View>
                  <View style={styles.FormALl}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data.HomeMobile}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Text style={AppStyles.Labeldefault}>Email cá nhân :</Text>
                  <Text style={([AppStyles.Textdefault], {marginLeft: 10})}>
                    {this.state.Data.HomeEmail}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Text style={AppStyles.Labeldefault}>Trình độ học vấn :</Text>
                  <Text style={([AppStyles.Textdefault], {marginLeft: 10})}>
                    {this.state.Data.EducationName}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Labeldefault}>
                      Địa chỉ thường trú:
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    height: '100%',
                    width: '100%',
                  }}>
                  <View style={{flex: 2}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data.PermanentAddress}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={{flex: 1}}>
              {this.state.checkInLogin !== '' ? (
                <TabBarBottom
                  //key để quay trở lại danh sách
                  backListByKey="Candidates"
                  keyCommentWF={{
                    dbName: 'HR',
                    scheme: 'HR',
                    tableName: 'CandidateProcess',
                    RecordGuid: this.props.RecordGuid,
                    Title: this.state.Data.FullName,
                  }}
                  // tiêu đề hiển thị trong popup xử lý phiếu
                  Title={this.state.Data.FullName}
                  //kiểm tra quyền xử lý
                  Permisstion={this.state.Data.Permisstion}
                  OffButonSave={true}
                  //kiểm tra bước đầu quy trình
                  checkInLogin={this.state.checkInLogin}
                  onSubmitWF={(callback, CommentWF) =>
                    this.submitWorkFlow(callback, CommentWF)
                  }
                />
              ) : null}
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
  onPressBack() {
    Actions.pop();
  }
  openAttachments() {
    var obj = {
      ModuleId: '11',
      RecordGuid: this.props.RecordGuid,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if (csStatus === 'W') {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  handleEmail(data) {
    this.setState({CommentWF: data});
  }
  setPickUpLocation(PickUpLocation) {
    this.state.Data.PickUpLocation = PickUpLocation;
    this.setState({Data: this.state.Data});
  }
  setGetOffLocation(GetOffLocation) {
    this.state.Data.GetOffLocation = GetOffLocation;
    this.setState({Data: this.state.Data});
  }
  setReplaceBy(ReplaceBy) {
    this.state.Data.ReplaceBy = ReplaceBy;
    this.setState({Data: this.state.Data});
  }
  setTolls(Tolls) {
    this.state.Data.Tolls = Tolls;
    this.setState({Data: this.state.Data});
  }
  setDescription(Description) {
    this.state.Data.Description = Description;
    this.setState({Data: this.state.Data});
  }
  setDate(Date) {
    this.state.Data.Date = Date;
    this.setState({Data: this.state.Data});
  }
  setStartDate(StartTime) {
    this.state.Data.StartTime = StartTime;
    this.setState({Data: this.state.Data});
  }
  setEndDate(EndTime) {
    this.state.Data.EndTime = EndTime;
    this.setState({Data: this.state.Data});
  }
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      CandidateGuid: this.state.Data.CandidateGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'B') {
      API_RegisterCars.Approve_Candidates(obj)
        .then(rs => {
          console.log('====Lỗi');
          console.log(rs);
          if (rs.data.errorCode === 200) {
            this.closeComment();
            Alert.alert(
              'Thông báo',
              'Trình lên thành công',
              [{text: 'OK', onPress: () => Keyboard.dismiss()}],
              {cancelable: false},
            );
            this.onPressBack();
          } else {
            Alert.alert(
              'Thông báo',
              'Lỗi khi trình phiếu',
              [{text: 'OK', onPress: () => Keyboard.dismiss()}],
              {cancelable: false},
            );
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data.data);
        });
    } else {
      API_RegisterCars.NotApprove_Candidates(obj)
        .then(rs => {
          console.log('====Lỗi');
          console.log(rs);
          if (rs.data.errorCode === 200) {
            Alert.alert(
              'Thông báo',
              'Trả lại thành công',
              [{text: 'OK', onPress: () => Keyboard.dismiss()}],
              {cancelable: false},
            );
            this.onPressBack();
          } else {
            Alert.alert(
              'Thông báo',
              'Lỗi khi xử lý phiếu',
              [{text: 'OK', onPress: () => Keyboard.dismiss()}],
              {cancelable: false},
            );
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data.data);
        });
    }
  }
  closeComment() {
    this.setState({isModalVisible: !this.state.isModalVisible});
  }

  renderFooter = () => {
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(openCandidates);
