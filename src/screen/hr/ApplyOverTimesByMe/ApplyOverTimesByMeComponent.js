import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, Icon, ListItem, SearchBar} from 'react-native-elements';
import {API_ApplyOverTimes} from '../../../network';
import Fonts from '../../../theme/fonts';
import {Picker} from 'native-base';
import * as UserAction from '@redux/user/actions';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 2,
    paddingBottom: 5,
    paddingLeft: 10,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
});

class ApplyOverTimesByMeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      Data: null,
      ListAll: [],
      loading: false,
      EvenFromSearch: false,
    };
    this._search = {
      StartDate: '2019-11-24',
      EndDate: '2020-12-24',
      Status: 'W',
      NumberPage: 0,
      txtSearch: '',
      Length: 14,
      Query: 'CreatedDate DESC',
    };
    this.listtabbarBotom = [
      {
        Title: 'Trang chủ',
        Icon: 'home',
        Type: 'antdesign',
        Value: 'home',
        Checkbox: false,
      },
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'CD',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'DD',
        Checkbox: false,
      },
      {
        Title: 'Trả lại',
        Icon: 'export2',
        Type: 'antdesign',
        Value: 'TL',
        Checkbox: false,
      },
    ];
  }

  componentDidMount(): void {
    this.nextPage();
  }
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({EvenFromSearch: true});
    } else {
      this.setState({EvenFromSearch: false});
    }
  }
  componentWillReceiveProps(nextProps) {
    this.nextPage();
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.txtSearch = search;
    this._search.NumberPage = 0;
    this.nextPage();
  };
  //List danh sách phân trang
  nextPage() {
    console.log(this.props);
    this.setState({loading: true});
    this._search.NumberPage++;
    API_ApplyOverTimes.ApplyOverTimesByMe_ListAll(this._search)
      .then(res => {
        this.state.ListAll = this.state.ListAll.concat(
          JSON.parse(res.data.data).data,
        );
        this.setState({
          ListAll: this.state.ListAll,
          refreshing: false,
        });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log(error);
      });
  }
  loadMoreData() {
    this.nextPage();
  }
  CustomeListAll = item => (
    <FlatList
      data={item}
      style={{flex: 1}}
      renderItem={({item, index}) => (
        <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
          <ListItem
            subtitle={() => {
              return (
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 3, flexDirection: 'row'}}>
                    <View style={{flex: 5}}>
                      <Text style={[AppStyles.Titledefault]}>
                        {item.ApplyOvertimeId}
                      </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        Tiêu đề: {item.Title}
                      </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        Người tạo: {item.EmployeeName}
                      </Text>
                    </View>
                  </View>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {' '}
                      {item.ApplyDateString}
                    </Text>
                    {item.Status == 'Y' ? (
                      <Text
                        style={[
                          {
                            justifyContent: 'center',
                            color: AppColors.AcceptColor,
                          },
                          AppStyles.Textdefault,
                        ]}>
                        {item.StatusWF}
                      </Text>
                    ) : (
                      <Text
                        style={[
                          {
                            justifyContent: 'center',
                            color: AppColors.PendingColor,
                          },
                          AppStyles.Textdefault,
                        ]}>
                        {item.StatusWF}
                      </Text>
                    )}
                  </View>
                </View>
              );
            }}
            bottomDivider
            //chevron
            onPress={() => this.onViewItem(item.ApplyOvertimeGuid)}
          />
        </View>
      )}
      onEndReached={() => this.loadMoreData()}
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['black']}
        />
      }
    />
  );
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Đăng ký làm thêm giờ'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            BackModuleByCode={'MyProfile'}
          />
          <Divider />
          {this.state.EvenFromSearch == true ? (
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.updateSearch(text)}
              value={this._search.txtSearch}
            />
          ) : null}
          <View style={{flex: 1}}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>
          {/* hiển thị nút tuỳ chọn */}
          {/* <View style={AppStyles.StyleTabvarBottom}>
                        <TabBarBottomCustom
                            ListData={this.listtabbarBotom}
                            onCallbackValueBottom={(callback) => this.onCallbackValueBottom(callback)}
                        />
                    </View> */}
        </View>
      </TouchableWithoutFeedback>
    );
  }
  //view item
  onViewItem(id) {
    debugger;
    Actions.applyOverTimesItem({viewId: id});
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'home':
        Actions.app();
        break;
      case 'CD':
        this._search.Search = '';
        (this._search.Status = 'W'), this.updateSearch('');
        break;
      case 'DD':
        this._search.Search = '';
        (this._search.Status = 'Y'), this.updateSearch('');
        break;
      case 'TL':
        this._search.Search = '';
        (this._search.Status = 'R'), this.updateSearch('');
        break;
      default:
        break;
    }
  }
  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }

  onclickreturnSearch() {
    this._search.Search = '';
    this.updateSearch('');
  }

  onValueChange(item) {
    console.log('============>>' + item);
    switch (item) {
      case 'Status': {
        break;
      }
      case 'StartDate': {
        break;
      }
      case 'EndDate': {
        break;
      }
      default: {
      }
    }
  }

  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  //Load lại
  _onRefresh = () => {
    this.nextPage();
    this.setState({refreshing: true});
  };
}

const mapStateToProps = state => ({
  applications: state.user.data,
});

const mapDispatchToProps = {
  addItem: data => UserAction.applications(data),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ApplyOverTimesByMeComponent);
