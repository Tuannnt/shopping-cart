import React, { Component } from 'react';
import {
  FlatList,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Alert,
  TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_ApplyOverTimes, API } from '../../../network';
import { Divider, ListItem } from 'react-native-elements';
import Fonts from '../../../theme/fonts';
import TabBarBottom from '../../component/TabBarBottom';
import { DatePicker, Container, Item, Label, Picker } from 'native-base';
import { Action } from 'rxjs/internal/scheduler/Action';
import { Actions } from 'react-native-router-flux';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
const styles = StyleSheet.create({
  styletextLable: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  styletext: {
    fontSize: 14,
    padding: 5,
  },
  textLableCol2: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  textCol2: {
    fontSize: 14,
    padding: 5,
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
  },
});

class ApplyOverTimesItemByMe_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      DataDetail: null,
      isModalVisible: false,
      Title: '',
      checkInLogin: '',
      LoginName: '',
    };
  }

  componentDidMount(): void {
    this.getItem();
  }
  getItem() {
    let id = { Id: this.props.viewId };
    API_ApplyOverTimes.ApplyOverTimes_Items(id)
      .then(res => {
        this.setState({ Data: JSON.parse(res.data.data).model });
        this.setState({ DataDetail: JSON.parse(res.data.data).detail });
        let checkin = {
          ApplyOvertimeGuid: this.props.viewId,
          WorkFlowGuid: JSON.parse(res.data.data).model.WorkFlowGuid,
        };
        API_ApplyOverTimes.ApplyOverTimes_checkin(checkin)
          .then(res => {
            this.setState({ checkInLogin: res.data.data });
            console.log('===checkin====' + res.data.data);
          })
          .catch(error => {
            console.log(error.data.data);
          });
        API.getProfile().then(rs => {
          console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
          this.setState({ LoginName: rs.data.LoginName });
        });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
      });
  }
  showActionSheet = () => this.actionSheet.show();
  getActionSheetRef = ref => (this.actionSheet = ref);
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'back', ActionTime: new Date().getTime() });
  }
  CustomView = item => (
    <TouchableWithoutFeedback
      style={{ flex: 1 }}
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <TabBar_Title
          title={item.ApplyOvertimeId}
          callBack={() => this.callBackList()}
          FormAttachment={true}
          CallbackFormAttachment={() => this.openAttachments()}
        />
        <Divider />
        {/*hiển thị nội dung chính*/}
        <View style={{ padding: 15 }}>
          {/*Mã phiếu*/}
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Mã:</Text>
            </View>
            <View style={{ flex: 2 }}>
              <Text style={[AppStyles.Textdefault]}>
                {item.ApplyOvertimeId}
              </Text>
            </View>
          </View>
          {/*Tiêu đề phiêu*/}
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Tiêu đề phiếu :</Text>
            </View>
            <View style={{ flex: 2 }}>
              <Text style={[AppStyles.Textdefault]}>{item.Title}</Text>
            </View>
          </View>
          {/*Người tạo phiếu*/}
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
            </View>
            <View style={{ flex: 2 }}>
              <Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text>
            </View>
          </View>
          {/*Phòng ban*/}
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
            </View>
            <View style={{ flex: 2 }}>
              <Text style={[AppStyles.Textdefault]}>{item.DepartmentName}</Text>
            </View>
          </View>
          {/*Chức vụ*/}
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Chức vụ :</Text>
            </View>
            <View style={{ flex: 2 }}>
              <Text style={[AppStyles.Textdefault]}>{item.JobTitleName}</Text>
            </View>
          </View>
          {/*Ngày tạo phiếu*/}
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Ngày tạo :</Text>
            </View>
            <View style={{ flex: 2 }}>
              <Text style={[AppStyles.Textdefault]}>
                {this.customDate(item.ApplyDate)}
              </Text>
            </View>
          </View>
          {/*nội dung phiếu*/}
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={[AppStyles.Textdefault]}>{item.Description}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
            {this.state.Data.Status == 'Y' ? (
              <Text
                style={[AppStyles.Textdefault, { color: AppColors.AcceptColor }]}>
                {this.customStatus(item.Status)}
              </Text>
            ) : (
              <Text
                style={[
                  AppStyles.Textdefault,
                  { color: AppColors.PendingColor },
                ]}>
                {this.customStatus(item.Status)}
              </Text>
            )}
          </View>
        </View>
        {/*hiển thị nội dung chi tiết*/}
        <Text style={{ fontSize: 16, fontWeight: 'bold', padding: 10 }}>
          Chi tiết phiếu
        </Text>
        <Divider />
        <View style={{ flex: 5 }}>
          <FlatList
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            keyExtractor={item => item.BankGuid}
            data={this.state.DataDetail}
            renderItem={({ item, index }) => (
              <View style={{ fontSize: 14, fontFamily: Fonts.base.family }}>
                <ListItem
                  title={item.EmployeeName}
                  subtitle={() => {
                    return (
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ width: '100%' }}>
                          <Text style={[AppStyles.Textdefault]}>
                            Chức vụ: {item.JobTitleName}
                          </Text>
                          <Text style={[AppStyles.Textdefault]}>
                            Mô tả: {item.Description}
                          </Text>
                        </View>
                      </View>
                    );
                  }}
                  bottomDivider
                  titleStyle={{ fontSize: 14, fontWeight: 'bold' }}
                  //chevron
                  onPress={() => this.onView(item.Id)}
                />
              </View>
            )}
          />
        </View>
        {/*nút xử lý*/}
        <View style={{ flex: 1 }}>
          {this.state.checkInLogin !== '' ? (
            <TabBarBottom
              onDelete={() => this.onDelete()}
              onUpdate={() => this.onUpdate()}
              backListByKey="ApplyOverTimes"
              Title={item.Title}
              Permisstion={item.Permisstion}
              checkInLogin={this.state.checkInLogin}
              onSubmitWF={(callback, CommentWF) =>
                this.submitWorkFlow(callback, CommentWF)
              }
              keyCommentWF={{
                dbName: 'HR',
                scheme: 'HR',
                tableName: 'ApplyOvertimeProcess',
                RecordGuid: this.props.viewId,
                Title: item.ApplyOvertimeId,
              }}
            />
          ) : null}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
  // formEdit = (item) => (
  //     <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => {
  //         Keyboard.dismiss()
  //     }}>
  //         <View style={{ flex: 1, backgroundColor: '#fff' }}>
  //             <TabBar_Title
  //                 title={item.ApplyOvertimeId}
  //                 callBack={() => this.callBackList()} />
  //             <Divider />
  //             {/*hiển thị nội dung chính*/}
  //             <View style={{ padding: 15 }}>
  //                 <View style={{ flexDirection: 'row' }}>
  //                     <View style={{ flex: 1 }}>
  //                         {/*Mã phiếu*/}
  //                         <View style={{ flexDirection: 'row' }}>
  //                             <View style={{ flex: 1 }}>
  //                                 <Text style={[styles.textLableCol2]}>Mã:</Text>
  //                             </View>
  //                             <View style={{ flex: 3 }}>
  //                                 <Text style={styles.textCol2}>{item.ApplyOvertimeId}</Text>
  //                             </View>
  //                         </View>
  //                     </View>
  //                     <View style={{ flex: 1 }}>
  //                         {/*trạng thái*/}
  //                         <View style={{ flexDirection: 'row' }}>
  //                             <View style={{ flex: 1 }}>
  //                                 <Text style={styles.textLableCol2}>Trạng thái :</Text>
  //                             </View>
  //                             <View style={{ flex: 1 }}>
  //                                 {item.Status == "Y" ?
  //                                     <Text style={{
  //                                         fontSize: 14,
  //                                         padding: 5,
  //                                         color: 'green'
  //                                     }}>{item.StatusWF}</Text>
  //                                     : <Text style={{
  //                                         fontSize: 14,
  //                                         padding: 5,
  //                                         color: 'red'
  //                                     }}>{item.StatusWF}</Text>}

  //                             </View>
  //                         </View>
  //                     </View>
  //                 </View>
  //                 {/*Tiêu đề phiêu*/}
  //                 <View style={{ flexDirection: 'row', alignItems: "center" }}>
  //                     <View style={{ flex: 1 }}>
  //                         <Text style={styles.styletextLable}>Tiêu đề phiếu :</Text>
  //                     </View>
  //                     <View style={{ flex: 2 }}>
  //                         <TextInput style={{ borderBottomWidth: 0.5, borderColor: "#C0C0C0" }}
  //                             placeholder="NHập tiêu đề phiếu"
  //                             value={item.Title}
  //                             onChangeText={Title => this.setState({ Title: Title })} />
  //                     </View>
  //                 </View>
  //                 {/*Người tạo phiếu*/}
  //                 <View style={{ flexDirection: 'row', alignItems: "center" }}>
  //                     <View style={{ flex: 1 }}>
  //                         <Text style={styles.styletextLable}>Người tạo :</Text>
  //                     </View>
  //                     <View style={{ flex: 2 }}>
  //                         <TextInput style={{ borderBottomWidth: 0.5, borderColor: "#C0C0C0" }}
  //                             underlineColorAndroid="transparent"
  //                             placeholder="NHập tiêu đề phiếu"
  //                             placeholderTextColor="#C0C0C0"
  //                             autoCapitalize="none"
  //                             value={item.EmployeeName}
  //                             onChangeText={EmployeeName => this.setState({ EmployeeName: EmployeeName })} />
  //                     </View>
  //                 </View>
  //                 {/*Phòng ban*/}
  //                 <View style={{ flexDirection: 'row', alignItems: "center" }}>
  //                     <View style={{ flex: 1 }}>
  //                         <Text style={styles.styletextLable}>Phòng ban :</Text>
  //                     </View>
  //                     <View style={{ flex: 2 }}>
  //                         <TextInput style={{ borderBottomWidth: 0.5, borderColor: "#C0C0C0" }}
  //                             placeholder="Nhập phòng ban"
  //                             value={item.DepartmentName}
  //                             onChangeText={DepartmentName => this.setState({ DepartmentName: DepartmentName })} />
  //                     </View>
  //                 </View>
  //                 {/*Chức vụ*/}
  //                 <View style={{ flexDirection: 'row', alignItems: "center" }}>
  //                     <View style={{ flex: 1 }}>
  //                         <Text style={styles.styletextLable}>Chức vụ :</Text>
  //                     </View>
  //                     <View style={{ flex: 2 }}>
  //                         <TextInput style={{ borderBottomWidth: 0.5, borderColor: "#C0C0C0" }}
  //                             placeholder="Nhập chức vụ"
  //                             value={item.JobTitleName}
  //                             onChangeText={JobTitleName => this.setState({ JobTitleName: JobTitleName })} />
  //                     </View>
  //                 </View>
  //                 {/*Ngày tạo phiếu*/}
  //                 <View style={{ flexDirection: 'row', alignItems: "center" }}>
  //                     <View style={{ flex: 1 }}>
  //                         <Text style={styles.styletextLable}>Ngày tạo :</Text>
  //                     </View>
  //                     <View style={{ flex: 2 }}>
  //                           <DatePicker
  // locale = "vie"
  //style = {{ borderBottomWidth: 0.5, borderColor: "#C0C0C0" }}
  //                             defaultDate={new Date()}
  //                             locale={"vi"}
  //                             timeZoneOffsetInMinutes={undefined}
  //                             modalTransparent={false}
  //                             animationType={"fade"}
  //                             androidMode={"default"}
  //                             placeHolderTextStyle={{ color: "#C0C0C0" }}
  //                             borderColor={"#C0C0C0"}
  //                             onDateChange={(ApplyDate) => this.setState({ ApplyDate: ApplyDate })}
  //                             disabled={false}
  //                         ></DatePicker>
  //                     </View>
  //                 </View>
  //                 {/*nội dung phiếu*/}
  //                 <View style={{ flexDirection: 'row', alignItems: "center" }}>
  //                     <View style={{ flex: 1 }}>
  //                         <Text style={styles.styletextLable}>Nội dung :</Text>
  //                     </View>
  //                     <View style={{ flex: 2 }}>
  //                         <TextInput style={{ borderBottomWidth: 0.5, borderColor: "#C0C0C0" }}
  //                             placeholder="Nhập nội dung"
  //                             value={item.Description}
  //                             onChangeText={Description => this.setState({ Description: Description })} />
  //                     </View>
  //                 </View>
  //             </View>
  //             {/*hiển thị nội dung chi tiết*/}
  //             <Text style={{ fontSize: 16, fontWeight: 'bold', padding: 10 }}>Chi tiết phiếu</Text>
  //             <Divider />
  //             <View style={{ flex: 5 }}>
  //                 <FlatList
  //                     refreshing={this.state.isFetching}
  //                     ref={(ref) => {
  //                         this.ListView_Ref = ref;
  //                     }}
  //                     ListHeaderComponent={this.renderHeader}
  //                     ListFooterComponent={this.renderFooter}
  //                     keyExtractor={item => item.BankGuid}
  //                     data={this.state.DataDetail}
  //                     renderItem={({ item, index }) => (
  //                         <View style={{ fontSize: 14, fontFamily: Fonts.base.family }}>
  //                             <ListItem
  //                                 title={item.EmployeeName}
  //                                 subtitle={() => {
  //                                     return (
  //                                         <View style={{ flexDirection: 'row' }}>
  //                                             <View style={{ width: '100%' }}>
  //                                                 <Text style={styles.subtitleStyle}>Chức
  //                                                     vụ: {item.JobTitleName}</Text>
  //                                                 <Text style={styles.subtitleStyle}>Mô tả: {item.Description}</Text>
  //                                             </View>
  //                                         </View>
  //                                     )
  //                                 }}
  //                                 bottomDivider
  //                                 titleStyle={{ fontSize: 14, fontWeight: 'bold' }}
  //                                 //chevron
  //                                 onPress={() => this.onView(item)}
  //                             />
  //                         </View>
  //                     )
  //                     }
  //                 />
  //             </View>
  //             {/*nút xử lý*/}
  //             <View style={{ flex: 1, bottom: 0 }}>
  //                 {this.state.checkInLogin !== "" ?
  //                     <TabBarBottom
  //                         onDelete={() => this.onDelete()}
  //                         onUpdate={() => this.onUpdate()}
  //                         onAttachment={() => this.onAttachment()}
  //                         Attachment={true}
  //                         backListByKey='ApplyOverTimes'
  //                         Title={item.Title}
  //                         Permisstion={item.Permisstion}
  //                         checkInLogin={this.state.checkInLogin}
  //                         onSubmitWF={(callback, CommentWF) => this.submitWorkFlow(callback, CommentWF)}
  //                         keyCommentWF={{
  //                             dbName: 'HR',
  //                             scheme: 'HR',
  //                             tableName: 'ApplyOvertimeProcess',
  //                             RecordGuid: this.props.viewId,
  //                             Title: item.ApplyOvertimeId
  //                         }}
  //                     /> : null}
  //             </View>
  //         </View>
  //     </TouchableWithoutFeedback>
  // )
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  onView(id) {
    // Actions.OpenApplyOvertimeDetails({ Id: id });
    // console.log('===================chuyen Id:' + id);
  }
  openAttachments() {
    var obj = {
      ModuleId: '16',
      RecordGuid: this.props.viewId,
    };
    Actions.attachmentComponent(obj);
  }

  closeComment() {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if (csStatus === 'W') {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Kết Thúc';
    }
    return stringstatus.toString();
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      ApplyOvertimeGuid: this.props.viewId,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'B') {
      API_ApplyOverTimes.ApplyOverTimes_approve(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            this.closeComment();
            Alert.alert(
              'Thông báo',
              'Trình lên thành công',
              [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
              { cancelable: false },
            );
            this.callBackList();
          } else {
            Alert.alert(
              'Thông báo',
              'Lỗi khi trình phiếu',
              [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
              { cancelable: false },
            );
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      API_ApplyOverTimes.ApplyOverTimes_noapprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            this.closeComment();
            Alert.alert(
              'Thông báo',
              'Trả lại thành công',
              [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
              { cancelable: false },
            );
            this.callBackList();
          } else {
            Alert.alert(
              'Thông báo',
              'Lỗi khi trả lại phiếu',
              [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
              { cancelable: false },
            );
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }
  //xóa
  onDelete = () => {
    let id = { Id: this.props.viewId };
    this.setState({ loading: true });
    API_ApplyOverTimes.ApplyOverTimes_Delete(id.Id)
      .then(response => {
        console.log(
          '==============ketquaXoa-errorCode: ' + response.data.Error,
        );
        if (response.data.Error == false) {
          this.setState({
            loading: false,
            ApplyOvertimeGuid: this.props.viewId,
          });
          Alert.alert(
            'Thông báo',
            'Xóa thành công',
            [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
            { cancelable: false },
          );
          this.callBackList();
        } else {
          Alert.alert(
            'Thông báo',
            'Có lỗi khi xóa',
            [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
            { cancelable: false },
          );
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  };
  //Lưu sửa
  onUpdate() { }
  callBackListView() {
    Actions.ApplyOverTimes();
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}
const mapStateToProps = state => ({
  applications: state.user.data,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ApplyOverTimesItemByMe_Component);
