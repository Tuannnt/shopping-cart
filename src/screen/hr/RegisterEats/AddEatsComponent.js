import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Keyboard,
  StyleSheet,
  Platform,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Container } from 'native-base';
import { FuncCommon } from '../../../utils';
import DatePicker from 'react-native-datepicker';
import _ from 'lodash';
import { AppStyles, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RequiredText from '../../component/RequiredText';
import RNPickerSelect from 'react-native-picker-select';
import LoadingComponent from '../../component/LoadingComponent';
import { API_HR } from '@network';
import controller from './controller';
import listCombobox from './listCombobox';
import Toast from 'react-native-simple-toast';
import { Combobox } from '@Component';

const headerTable = listCombobox.headerTable;
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
const ListType = [
  {
    value: 'A',
    label: '12 giờ',
  },
  {
    value: 'B',
    label: '18 giờ',
  },
  {
    value: 'C',
    label: '24 giờ',
  },
  {
    value: 'N',
    label: 'Ăn ngoài',
  },
];
class AddEatsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [],
      Type: 'A',
      Date: new Date(),
      Quantity: '1',
      loading: true,
      ListEmployee: [],
      ListDepartment: [],
      workShiftData: [],
      init: true,
      // MoneyRation: '30000',
    };
    this.setDate = this.setDate.bind(this);
    this.refreshing = false;
  }
  componentDidMount(): void {
    Promise.all([
      this.getNumberAuto(),
      this.getAllEmployee(),
      this.GetDepEatsGuid(),
      this.getAllWorkShift(),
    ]);
    const { itemData, rowData = [] } = this.props;
    if (itemData) {
      this.setState({
        RegisterEatsId: itemData.RegisterEatsId,
        Quantity: itemData.Quantity + '',
        // Amount: itemData.Amount ? itemData.Amount + '' : undefined,
        // MoneyRation: itemData.MoneyRation + '',
        Date: FuncCommon.ConDate(new Date(itemData.Date), 0),
        Type: itemData.Type,
        Note: itemData.Note,
        FullName: itemData.EmployeeName,
        DepartmentName: itemData.DepartmentName,
        JobTitleNameEmployee: itemData.JobTitleName,
        RegisterEatsId: itemData.RegisterEatsId,
        DepEatsGuid: itemData.DepEatsGuid,
        DepEatsName: itemData.DepEatsName,
        rows: [...rowData].map(row => {
          if (row.ShiftID) {
            row.ShiftId = row.ShiftID;
          }
          return row;
        }),
        EmployeeAskGuid: null,
      });
    } else {
      this.setState({
        Date: FuncCommon.ConDate(new Date(), 0),
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        JobTitleNameEmployee:
          global.__appSIGNALR.SIGNALR_object.USER.JobTitleName,
      });
    }
  }
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_RE',
      Prefixs: null,
      VoucherType: '',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      this.setState({ RegisterEatsId: data.Value });
    });
  };
  setDate(newDate) {
    this.setState({ Date: newDate });
  }
  onChangeType(value) {
    this.setState({
      Type: value,
    });
  }
  setNote(Note) {
    this.setState({ Note });
  }
  setQuantity(Quantity) {
    this.setState({ Quantity });
  }
  onClickBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  getAllEmployee = () => {
    controller.getAllEmployee(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.EmployeeId,
        label: `${item.FullName} [${item.EmployeeId}]`,
        DepartmentId: item.DepartmentId,
      }));
      this.setState({
        ListEmployee: [{ value: null, label: 'Chọn' }, ...data],
        loading: false,
      });
    });
  };
  GetDepEatsGuid = () => {
    controller.GetDepEatsGuid(rs => {
      let data = rs.map(item => ({
        value: item.DepartmentGuid,
        text: `[${item.DepartmentId}] ${item.DepartmentName}`,
        DepartmentId: item.DepartmentId,
      }));
      this.setState({
        ListDepartment: [...data],
        loading: false,
      });
    });
  };
  getAllWorkShift = () => {
    // controller.getAllWorkShift(rs => {
    //   let data = JSON.parse(rs);
    //   data.unshift({
    //     value: null,
    //     label: 'Chọn ca',
    //   });
    //   this.setState({workShiftData: data});
    // });
  };

  onChangeState = (key, value) => {
    this.setState({ [key]: value });
  };
  Submit() {
    if (this.refreshing) {
      return;
    }
    this.refreshing = true;
    setTimeout(() => {
      this.refreshing = false;
    }, 2000);
    let Quantity = '';
    // let Amount = '';
    // let MoneyRation = '';
    // if (!this.state.Quantity) {
    //   Toast.showWithGravity(
    //     'Bạn chưa nhập số lượng',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    // if (!this.state.EmployeeAskGuid) {
    //   Toast.showWithGravity(
    //     'Bạn chưa chọn nhân viên',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    // if (Number(this.state.Quantity.replace(/\./g, '')) <= 0) {
    //   Toast.showWithGravity(
    //     'Vui lòng nhập số lượng lớn hơn 0',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    Quantity = this.state.Quantity.replace(/\./g, '');
    // MoneyRation = this.state.MoneyRation.replace(/\./g, '');
    // if (this.state.Amount) {
    //   Amount = this.state.Amount.replace(/\./g, '');
    // }
    let lstDetails = [...this.state.rows].map(x => {
      let obj = { ...x };
      obj.Lunch = x.Lunch ? x.Lunch : '0';
      obj.Dinner = x.Dinner ? x.Dinner : '0';
      obj.Snacks = x.Snacks ? x.Snacks : '0';
      obj.Nocturnal = x.Nocturnal ? x.Nocturnal : '0';
      obj.Overtime = x.Overtime ? x.Overtime : '0';
      return obj;
    });

    // if (!this.state.Type) {
    //   Toast.showWithGravity('Bạn chưa chọn xuất ăn', Toast.SHORT, Toast.CENTER);
    //   return;
    // }

    // if (Number(Quantity) >= 1000) {
    //   Toast.showWithGravity(
    //     'Vui lòng không nhập quá số lượng',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }

    let isNotValidated = false;
    if (lstDetails.length === 0) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập chi tiết',
        [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
        { cancelable: false },
      );
      return;
    }
    for (let i = 0; i < lstDetails.length; i++) {
      let lst = lstDetails[i];
      if (!lst.EmployeeId) {
        Alert.alert(
          'Thông báo',
          `Bạn chưa chọn nhân viên ở dòng ${i + 1}`,
          [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
          { cancelable: false },
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }
    if (this.props.itemData) {
      this.Update(lstDetails);
      return;
    }
    let auto = { Value: this.state.RegisterEatsId, IsEdit: false };
    let _obj = {
      Quantity,
      // Amount,
      // MoneyRation,
      ConfirmBy: null,
      Date: FuncCommon.ConDate(FuncCommon.ConDate(this.state.Date, 99), 2),
      EndDate: FuncCommon.ConDate(FuncCommon.ConDate(this.state.Date, 99), 2),
      Note: this.state.Note,
      Type: this.state.Type,
      Status: null,
      FileAttachments: [],
      EmployeeAskGuid: null,
      RegisterEatsId: this.state.RegisterEatsId,
      DepEatsGuid: this.state.DepEatsGuid,
      DepEatsName: this.state.DepEatsName,
    };
    let _data = new FormData();
    _data.append('submit', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    // _data.append('lstTitlefile', JSON.stringify(undefined));
    API_HR.InsertRegisterEats(_data)
      .then(rs => {
        console.log(rs);
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onClickBack();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }

  Update = lstDetails => {
    let _obj = {
      ...this.props.itemData,
      // Quantity: this.state.Quantity.replace(/\./g, ''),
      // Amount: this.state.Amount ? this.state.Amount.replace(/\./g, '') : '0',
      // MoneyRation: this.state.MoneyRation.replace(/\./g, ''),
      Quantity: 0,
      Date: FuncCommon.ConDate(FuncCommon.ConDate(this.state.Date, 99), 2),
      Type: this.state.Type,
      Note: this.state.Note ? this.state.Note : '',
      RegisterEatsId: this.state.RegisterEatsId,
      DepEatsGuid: this.state.DepEatsGuid,
      DepEatsName: this.state.DepEatsName,
    };
    let _data = new FormData();
    let auto = { Value: this.state.RegisterEatsId, IsEdit: false };
    _data.append('submit', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('lstDetails', JSON.stringify(lstDetails));

    API_HR.RegisterEats_Update(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          console.log(_rs);
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Chỉnh sửa thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onClickBack();
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };
  addRows = () => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({ rows: data });
  };
  onDelete = index => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({ rows: res });
  };
  handleChangeRows = (val, index, name) => {
    const { rows } = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    if (name === 'EmployeeId') {
      const emp = this.state.ListEmployee.find(x => x.value === val);
      if (emp) {
        res[index].DepartmentId = emp.DepartmentId;
        res[index].EmployeeGuid = emp.EmployeeGuid;
      }
    }
    this.setState({ rows: res });
  };
  _openComboboxEmployees() { }
  openComboboxEmployees = d => {
    this._openComboboxEmployees = d;
  };
  onActionEmployees() {
    this._openComboboxEmployees();
  }
  ChangeEmployees = rs => {
    if (rs.value) {
      this.onChangeState('EmployeeAskGuid', rs.value);
      this.onChangeState('EmployeeAskName', rs.text);
    }
  };
  _openComboboxDepartments() { }
  openComboboxDepartments = d => {
    this._openComboboxDepartments = d;
  };
  onActionDepartments() {
    this._openComboboxDepartments();
  }
  ChangeDepartments = rs => {
    if (rs.value) {
      if (this.props.itemData && this.state.init === true) {
        this.setState({ init: false });
        return;
      }

      controller.GetItemByDepEatsGuid(
        {
          DepEatsGuid: rs.value,
          StartTime: FuncCommon.ConDate(
            FuncCommon.ConDate(this.state.Date, 99),
            2,
          ),
        },
        rs => {
          this.setState({ rows: JSON.parse(rs) });
        },
      );
      this.onChangeState('DepEatsGuid', rs.value);
      this.onChangeState('DepEatsName', rs.text);
    }
  };
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    return (
      <View style={{ flexDirection: 'row' }} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 40 }]}>
          <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, { width: 250 }]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'EmployeeId');
            }}
            items={this.state.ListEmployee}
            style={[stylePicker]}
            value={row.EmployeeId}
            placeholder={{}}
          />
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Lunch}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={Lunch => {
              this.handleChangeRows(Lunch + '', index, 'Lunch');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Dinner}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={Dinner => {
              this.handleChangeRows(Dinner + '', index, 'Dinner');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Snacks}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={Snacks => {
              this.handleChangeRows(Snacks + '', index, 'Snacks');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Nocturnal}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={Nocturnal => {
              this.handleChangeRows(Nocturnal + '', index, 'Nocturnal');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Overtime}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={Overtime => {
              this.handleChangeRows(Overtime + '', index, 'Overtime');
            }}
          />
        </TouchableOpacity>
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'ShiftId');
            }}
            items={this.state.workShiftData}
            style={stylePicker}
            value={row.ShiftId}
            placeholder={{}}
          />
        </TouchableOpacity> */}
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}> */}
        {/* <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.MainMeal}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={MainMeal => {
              this.handleChangeRows(MainMeal + '', index, 'MainMeal');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.SideDishesOne}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={SideDishesOne => {
              this.handleChangeRows(SideDishesOne + '', index, 'SideDishesOne');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.SideDishesTow}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={SideDishesTow => {
              this.handleChangeRows(SideDishesTow + '', index, 'SideDishesTow');
            }}
          />
        </TouchableOpacity> */}
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={Note => {
              this.handleChangeRows(Note + '', index, 'Note');
            }}
          />
        </TouchableOpacity> */}

        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td, { width: 80 }]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  onChangeText = (key, value) => {
    this.setState({ [key]: value });
  };
  render() {
    const { RegisterEatsId } = this.state;

    let Total = '';
    // if (this.state.MoneyRation && this.state.Quantity) {
    //   Total = FuncCommon.addPeriod(
    //     +this.state.MoneyRation.replace(/\./g, '') *
    //       +this.state.Quantity.replace(/\./g, ''),
    //   );
    // }
    if (this.state.loading) {
      return <LoadingComponent />;
    }

    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{ flex: 1 }}>
        <View style={[AppStyles.container]}>
          <Container>
            {this.props.itemData ? (
              <TabBar_Title
                title={'Sửa đăng ký ăn'}
                callBack={() => Actions.pop()}
              />
            ) : (
              <TabBar_Title
                title={'Thêm đăng ký ăn'}
                callBack={() => Actions.pop()}
              />
            )}
            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>
                      Mã phiếu <RequiredText />
                    </Text>
                  </View>
                  <View style={[AppStyles.FormInput]}>
                    <Text
                      style={[AppStyles.TextInput, { color: AppColors.gray }]}>
                      {RegisterEatsId}
                    </Text>
                  </View>
                </View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Người tạo phiếu</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, { color: 'black' }]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, { color: 'black' }]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Chức danh</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, { color: 'black' }]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.JobTitleNameEmployee}
                    editable={false}
                  />
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày đăng ký <RequiredText />
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '100%' }}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{ width: '100%' }}
                        date={this.state.Date}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setDate(date)}
                      />
                    </View>
                  </View>
                </View>
                <View style={{ padding: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Phòng ban đăng ký</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionDepartments()}>
                    <Text
                      style={[
                        AppStyles.TextInput,
                        !this.state.DepEatsGuid
                          ? { color: AppColors.gray }
                          : { color: 'black' },
                      ]}>
                      {this.state.DepEatsGuid
                        ? this.state.DepEatsName
                        : 'Chọn phòng ban đăng ký'}
                    </Text>
                  </TouchableOpacity>
                </View>
                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>
                    Giờ ăn <RequiredText />
                  </Text>
                  <View
                    style={{
                      ...AppStyles.FormInputPicker,
                      justifyContent: 'center',
                    }}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value => this.onChangeType(value)}
                      items={ListType || []}
                      placeholder={{}}
                      value={this.state.Type}
                      style={{
                        inputIOS: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                        inputAndroid: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                      }}
                    />
                  </View>
                </View> */}
                {/* <View style={{ padding: 10, paddingBottom: 0 }}>
                    <Label style={AppStyles.Labeldefault}>
                      Xuất ăn <RequiredText />{' '}
                    </Label>
                    <View style={[{...AppStyles.FormInputPicker, justifyContent: 'center'}]}>
                      <RNPickerSelect
                        doneText="Xong"
                        onValueChange={value => this.onChangeType(value)}
                        items={Ration}
                        placeholder={{}}
                        value={this.state.Type}
                        style={{
                          inputIOS: {
                            padding: 10,
                            color: 'black',
                            fontSize: 14,
                            paddingRight: 15, // to ensure the text is never behind the icon
                          },
                          inputAndroid: {
                          padding: 10,
                            color: 'black',
                            fontSize: 14,
                            paddingRight: 15, // to ensure the text is never behind the icon
                          },
                        }}
                        useNativeAndroidPickerStyle={false}
                      />
                    </View>
                </View> */}
                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Số lượng <RequiredText />
                    </Text>
                  </View>

                  <TextInput
                    style={[AppStyles.FormInput, {textAlign: 'right'}]}
                    underlineColorAndroid="transparent"
                    keyboardType="numeric"
                    autoCapitalize="none"
                    value={
                      !this.state.Quantity
                        ? this.state.Quantity
                        : FuncCommon.addPeriodTextInput(this.state.Quantity)
                    }
                    onChangeText={Quantity =>
                      this.onChangeText('Quantity', Quantity)
                    }
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>
                    Nhân viên ăn <RequiredText />
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionEmployees()}>
                    <Text
                      style={[
                        AppStyles.TextInput,
                        !this.state.EmployeeAskGuid
                          ? {color: AppColors.gray}
                          : {color: 'black'},
                      ]}>
                      {this.state.EmployeeAskGuid
                        ? this.state.EmployeeAskName
                        : 'Chọn nhân viên'}
                    </Text>
                  </TouchableOpacity>
                </View> */}
                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Số tiền/Suất <RequiredText />
                    </Text>
                  </View>

                  <TextInput
                    style={[AppStyles.FormInput, {textAlign: 'right'}]}
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    value={
                      !this.state.MoneyRation
                        ? this.state.MoneyRation
                        : FuncCommon.addPeriodTextInput(this.state.MoneyRation)
                    }
                    onChangeText={MoneyRation =>
                      this.onChangeText('MoneyRation', MoneyRation)
                    }
                  />
                </View> */}
                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Tổng tiền</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {textAlign: 'right'}]}
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    value={
                      !this.state.Amount
                        ? this.state.Amount
                        : FuncCommon.addPeriodTextInput(this.state.Amount)
                    }
                    onChangeText={Amount => this.onChangeText('Amount', Amount)}
                  />
                </View> */}
                {/* <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Nội dung</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    multiline
                    value={this.state.Note}
                    onChangeText={Note => this.setNote(Note)}
                    maxLength={255}
                  />
                </View> */}
                {/* Table */}
                <View style={{ marginTop: 5 }}>
                  <View style={{ width: 150, marginLeft: 10 }}>
                    <Button
                      title="Thêm dòng"
                      type="outline"
                      size={15}
                      onPress={() => this.addRows()}
                      buttonStyle={{ padding: 5, marginBottom: 5 }}
                      titleStyle={{ marginLeft: 5 }}
                      icon={
                        <Icon
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>
                  <ScrollView
                    horizontal={true}
                    style={{ paddingBottom: 25, marginBottom: 20 }}>
                    <View style={{ flexDirection: 'column' }}>
                      <View style={{ flexDirection: 'row' }}>
                        {headerTable.map(item => (
                          <TouchableOpacity
                            key={item.title}
                            style={[AppStyles.table_th, { width: item.width }]}>
                            <Text style={AppStyles.Labeldefault}>
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        ))}
                      </View>
                      <View>
                        {this.state.rows.length > 0 ? (
                          <FlatList
                            data={this.state.rows}
                            renderItem={({ item, index }) => {
                              return this.itemFlatList(item, index);
                            }}
                            keyExtractor={(rs, index) => index.toString()}
                          />
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                { textAlign: 'left' },
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View>
              </View>
            </ScrollView>

            <View>
              <Button
                buttonStyle={{ backgroundColor: AppColors.ColorButtonSubmit }}
                title="Lưu"
                onPress={() => this.Submit()}
              />
            </View>
            {/* {this.state.ListEmployee.length === 0 ? null : (
              <Combobox
                value={this.state.EmployeeAskGuid || null}
                TypeSelect={'single'}
                callback={this.ChangeEmployees}
                data={this.state.ListEmployee}
                nameMenu={'Chọn nhân viên'}
                eOpen={this.openComboboxEmployees}
                position={'bottom'}
              />
            )} */}
            {this.state.ListDepartment.length === 0 ? null : (
              <Combobox
                value={this.state.DepEatsGuid || null}
                TypeSelect={'single'}
                callback={this.ChangeDepartments}
                data={this.state.ListDepartment}
                nameMenu={'Chọn phòng ban'}
                eOpen={this.openComboboxDepartments}
                position={'bottom'}
              />
            )}
          </Container>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddEatsComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
const picker = {
  inputIOS: {
    padding: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    padding: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
    borderRadius: 2,
  },
};
