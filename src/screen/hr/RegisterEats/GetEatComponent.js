import React, { Component } from 'react';
import {
  Keyboard,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
  FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import { API_HR, API } from '@network';
import { Actions } from 'react-native-router-flux';
import { Divider } from 'react-native-elements';
import TabBarBottom from '../../component/TabBarBottom';
import { AppStyles, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import moment from 'moment';
import listCombobox from './listCombobox';
import controller from './controller';
import { FuncCommon } from '../../../utils';
import Toast from 'react-native-simple-toast';

const headerTable = [
  { title: 'STT', width: 40 },
  { title: 'Tên Nhân viên', width: 250 },
  { title: 'Trưa', width: 100 },
  { title: 'Tối', width: 100 },
  { title: 'Phụ', width: 100 },
  { title: 'Đêm', width: 100 },
  { title: 'Làm thêm', width: 100 },
  // {title: 'Ca đăng ký', width: 200},
  // {title: 'Xuất ăn chính', width: 150},
  // {title: 'Xuất ăn phụ 1', width: 150},
  // {title: 'Xuất ăn phụ 2', width: 150},
  // { title: 'Nội dung', width: 200 },
  { title: 'Hành động', width: 80, hideInDetail: true },
];
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});

class GetEatComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [],
      Data: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      Quantity: '',
      viewId: '',
      Type: '',
      ListEmployee: [],
      workShiftData: [],
    };
  }

  componentDidMount = () => {
    if (this.props.RecordGuid !== undefined) {
      this.state.viewId = this.props.RecordGuid;
    } else {
      this.state.viewId = this.props.viewId;
    }
    Promise.all([
      this.getItem(),
      this.getAllEmployeeByDepart(),
      this.getAllWorkShift(),
    ]);
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }
  getAllEmployeeByDepart = () => {
    controller.getAllEmployeeByDepart(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.value,
        label: item.text,
      }));
      data.unshift({
        value: null,
        label: '',
      });
      this.setState({ ListEmployee: data });
    });
  };
  getAllWorkShift = () => {
    controller.getAllWorkShift(rs => {
      let data = JSON.parse(rs).map(item => ({
        ...item,
        value: item.value,
        label: item.text,
      }));
      data.unshift({
        value: null,
        label: '',
      });
      this.setState({ workShiftData: data });
    });
  };
  onPressBack() {
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  Delete = () => {
    let id = { Id: this.state.viewId };
    this.setState({ loading: true });
    API_HR.deleteRegisterEat(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            RowGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  };
  getItem() {
    var obj = {
      RowGuid: this.state.viewId,
      WorkFlowGuid: this.props.WorkFlowGuid,
    };
    API_HR.getItemRegisterEat(obj)
      .then(res => {
        let model = JSON.parse(res.data.data).model;
        let detail = JSON.parse(res.data.data).detail;
        let ActualRefundDate1 = model.Date;
        let ActualRefundDate11 = new Date(ActualRefundDate1);
        let _data = { ...model };
        // let rows = model.rsDetail.map(data => {
        //   if (data.MainMeal) {
        //     data.MainMeal = data.MainMeal + '';
        //   }
        //   if (data.SideDishesOne) {
        //     data.SideDishesOne = data.SideDishesOne + '';
        //   }
        //   if (data.SideDishesTow) {
        //     data.SideDishesTow = data.SideDishesTow + '';
        //   }
        //   return data;
        // });
        _data.Quantity = String(_data.Quantity);
        _data.Date = new Date(_data.Date);
        this.setState({
          Data: _data,
          Date: ActualRefundDate11,
          rows: [...detail],
        });
        let checkin = {
          RowGuid: this.state.viewId,
          WorkFlowGuid: model.WorkFlowGuid,
        };
        API_HR.CheckLoginEat(checkin)
          .then(res => {
            this.setState({ checkInLogin: res.data.data });
          })
          .catch(error => {
            console.log(error.data.data);
          });
        API.getProfile().then(rs => {
          this.setState({ LoginName: rs.data.LoginName });
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
  itemFlatList = (row, index) => {
    return (
      <View style={{ flexDirection: 'row' }} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 40 }]}>
          <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 250 }]}>
          <Text style={[AppStyles.TextInput, { marginLeft: 10 }]}>
            {row.EmployeeName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={[AppStyles.FormInput_custom, { color: 'black' }]}
            underlineColorAndroid="transparent"
            value={row.Lunch + ''}
            autoCapitalize="none"
            keyboardType="numeric"
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={[AppStyles.FormInput_custom, { color: 'black' }]}
            underlineColorAndroid="transparent"
            value={row.Dinner + ''}
            autoCapitalize="none"
            keyboardType="numeric"
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={[AppStyles.FormInput_custom, { color: 'black' }]}
            underlineColorAndroid="transparent"
            value={row.Snacks + ''}
            autoCapitalize="none"
            keyboardType="numeric"
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={[AppStyles.FormInput_custom, { color: 'black' }]}
            underlineColorAndroid="transparent"
            value={row.Nocturnal + ''}
            autoCapitalize="none"
            keyboardType="numeric"
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={[AppStyles.FormInput_custom, { color: 'black' }]}
            underlineColorAndroid="transparent"
            value={row.Overtime + ''}
            autoCapitalize="none"
            keyboardType="numeric"
            editable={false}
          />
        </TouchableOpacity>
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.ShiftName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.MainMeal}
            autoCapitalize="none"
            keyboardType="numeric"
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.SideDishesOne}
            autoCapitalize="none"
            keyboardType="numeric"
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.SideDishesTow}
            autoCapitalize="none"
            keyboardType="numeric"
            editable={false}
          />
        </TouchableOpacity> */}
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, { width: 200 }]}>
          <TextInput
            style={[AppStyles.FormInput_custom, { color: 'black' }]}
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            editable={false}
          />
        </TouchableOpacity> */}
      </View>
    );
  };

  CustomViewDetailRoutings = () => {
    return (
      <View style={{ flex: 1, paddingBottom: 10 }}>
        <View style={{ padding: 15 }}>
          <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1.5 }}>
              <Text style={AppStyles.Labeldefault}>Người đăng ký :</Text>
            </View>
            <View style={{ flex: 3 }}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  { textAlign: 'left', paddingLeft: 10 },
                ]}>
                {this.state.Data.EmployeeName}
              </Text>
            </View>
          </View>
          {/*Phòng ban*/}
          <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1.5 }}>
              <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
            </View>
            <View style={{ flex: 3 }}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  { textAlign: 'left', paddingLeft: 10 },
                ]}>
                {this.state.Data.DepartmentName}
              </Text>
            </View>
          </View>
          {/*Chức vụ*/}
          <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1.5 }}>
              <Text style={AppStyles.Labeldefault}>Chức vụ :</Text>
            </View>
            <View style={{ flex: 3 }}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  { textAlign: 'left', paddingLeft: 10 },
                ]}>
                {this.state.Data.JobTitleName}
              </Text>
            </View>
          </View>

          {/*Ngày tạo phiếu*/}
          <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1.5 }}>
              <Text style={AppStyles.Labeldefault}>Ngày đăng ký :</Text>
            </View>
            <View style={{ flex: 3 }}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  { textAlign: 'left', paddingLeft: 10 },
                ]}>
                {moment(this.state.Data.Date).format('DD/MM/YYYY')}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1.5 }}>
              <Text style={AppStyles.Labeldefault}>Phòng ban đăng ký :</Text>
            </View>
            <View style={{ flex: 3 }}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  { textAlign: 'left', paddingLeft: 10 },
                ]}>
                {this.state.Data.DepEatsName}
              </Text>
            </View>
          </View>
          {/* <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Số lượng:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {FuncCommon.addPeriod(this.state.Data.Quantity)}
              </Text>
            </View>
          </View> */}
          {/* <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Số tiền/Suất:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {FuncCommon.addPeriod(this.state.Data.MoneyRation)}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Tổng tiền:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {FuncCommon.addPeriod(this.state.Data.Amount)}
              </Text>
            </View>
          </View> */}
          {/* <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Giờ ăn :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {this.state.Data.Type === 'A'
                  ? '12 giờ'
                  : this.state.Data.Type === 'B'
                  ? '18 giờ'
                  : this.state.Data.Type === 'C'
                  ? '24 giờ'
                  : 'Ăn ngoài'}
              </Text>
            </View>
          </View> */}
          {/* Nhân viên ăn */}
          {/* <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1.5 }}>
              <Text style={AppStyles.Labeldefault}>Người đăng ký :</Text>
            </View>
            <View style={{ flex: 3 }}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  { textAlign: 'left', paddingLeft: 10 },
                ]}>
                {this.state.Data.EmployeeAskName}
              </Text>
            </View>
          </View> */}
          {/* <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1.5 }}>
              <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
            </View>
            <View style={{ flex: 3 }}>
              {this.state.Data.Status == 'Y' ? (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    { color: AppColors.AcceptColor },
                    { textAlign: 'left', paddingLeft: 10 },
                  ]}>
                  {this.state.Data.StatusWF}
                </Text>
              ) : (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    { color: AppColors.PendingColor },
                    { textAlign: 'left', paddingLeft: 10 },
                  ]}>
                  {this.state.Data.StatusWF}
                </Text>
              )}
            </View>
          </View> */}
          {/*nội dung phiếu*/}
          {/* <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1.5 }}>
              <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
            </View>
            <View style={{ flex: 3 }}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  { textAlign: 'left', paddingLeft: 10 },
                ]}>
                {this.state.Data.Note}
              </Text>
            </View>
          </View> */}
        </View>
        <Text style={{ fontSize: 16, fontWeight: 'bold', padding: 10 }}>
          Chi tiết phiếu
        </Text>
        <ScrollView style={{ paddingBottom: 10 }} horizontal={true}>
          <View style={{ flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row' }}>
              {headerTable.map(item => {
                if (item.hideInDetail) {
                  return null;
                }
                return (
                  <TouchableOpacity
                    key={item.title}
                    style={[AppStyles.table_th, { width: item.width }]}>
                    <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <FlatList
                  data={this.state.rows}
                  renderItem={({ item, index }) => {
                    return this.itemFlatList(item, index);
                  }}
                  keyExtractor={(rs, index) => index.toString()}
                />
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'left' }]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };

  showActionSheet = () => this.actionSheet.show();
  getActionSheetRef = ref => (this.actionSheet = ref);

  CustomView = item => (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <TabBar_Title
        title={'Chi tiết phiếu ăn'}
        callBack={() => this.callBackList()}
      />
      <Divider />
      {/*hiển thị nội dung chính*/}
      <ScrollView style={{ paddingBottom: 30 }}>
        {this.CustomViewDetailRoutings()}
      </ScrollView>
      {/*nút xử lý*/}
      <View style={{ maxHeight: 50 }}>
        {this.state.Data.LoginName ===
          global.__appSIGNALR.SIGNALR_object.USER.LoginName ? (
          <TabBarBottom
            onDelete={() => this.Delete()}
            //key để quay trở lại danh sách
            callbackOpenUpdate={this.callbackOpenUpdate}
            backListByKey="Registereats"
            // keyCommentWF={{
            //   ModuleId: 33,
            //   RecordGuid: this.state.viewId,
            //   Title: 'Phiếu ăn',
            //   //LoginName:this.state.Data.LoginName
            // }}
            keyCommentWF={null}
            // tiêu đề hiển thị trong popup xử lý phiếu
            Title="Phiếu ăn"
            //kiểm tra quyền xử lý
            Permisstion={
              this.state.Data.LoginName ===
                global.__appSIGNALR.SIGNALR_object.USER.LoginName
                ? 1
                : 0
            }
            //kiểm tra bước đầu quy trình
            checkInLogin={
              this.state.Data.LoginName ===
                global.__appSIGNALR.SIGNALR_object.USER.LoginName
                ? 1
                : 0
            }
            onSubmitWF={null}
          />
        ) : null}
      </View>
    </View>
  );
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  callbackOpenUpdate = () => {
    Actions.addregistereats({
      itemData: this.state.Data,
      rowData: this.state.rows,
    });
  };
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'Back', ActionTime: new Date().getTime() });
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    if (callback == 'D') {
      let obj = {
        RowGuid: this.state.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.state.LoginName,
        Comment: CommentWF,
      };
      API_HR.approveRegisterEats(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      let obj = {
        RowGuid: this.state.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        Comment: CommentWF,
      };
      API_HR.notapproveRegisterEats(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetEatComponent);
