export default {
  headerTable: [
    { title: 'STT', width: 40 },
    { title: 'Tên Nhân viên', width: 250 },
    { title: 'Trưa', width: 100 },
    { title: 'Tối', width: 100 },
    { title: 'Phụ', width: 100 },
    { title: 'Đêm', width: 100 },
    { title: 'Làm thêm', width: 100 },
    // {title: 'Ca đăng ký', width: 200},
    // {title: 'Xuất ăn chính', width: 150},
    // {title: 'Xuất ăn phụ 1', width: 150},
    // {title: 'Xuất ăn phụ 2', width: 150},
    { title: 'Hành động', width: 80, hideInDetail: true },
  ],
  Ration: [
    {
      value: 'A',
      label: 'Xuất ăn 12k',
    },
    {
      value: 'B',
      label: 'Xuất ăn 15k',
    },
    {
      value: 'C',
      label: 'Xuất ăn 28k',
    },
  ],
  RationList: [
    {
      value: 'D',
      text: 'Tất cả',
    },
    {
      value: 'A',
      text: '12 giờ',
    },
    {
      value: 'B',
      text: '18 giờ',
    },
    {
      value: 'C',
      text: '24 giờ',
    },

    {
      value: 'N',
      text: 'Ăn ngoài',
    },
  ],
};
