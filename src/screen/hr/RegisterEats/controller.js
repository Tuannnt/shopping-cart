import { API_ApplyOverTimes, API_HR } from '@network';
import { ErrorHandler } from '@error';

export default {
  getNumberAuto(value, callback) {
    API_ApplyOverTimes.GetNumberAuto(value)
      .then(res => {
        console.log(res + '=======> get Number auto');
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  },
  GetItemByDepEatsGuid(value, callback) {
    API_HR.GetItemByDepEatsGuid(value)
      .then(res => {
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  },
  // List nhan vien
  getAllEmployeeByDepart(callback) {
    API_ApplyOverTimes.GetEmployeeByDepartment()
      .then(res => {
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  },
  getAllEmployee(callback) {
    API_HR.GetEmp()
      .then(res => {
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  },
  GetDepEatsGuid(callback) {
    API_HR.GetDepEatsGuid()
      .then(res => {
        const { data } = res;
        callback(data);
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  },
  getAllWorkShift(callback) {
    API_HR.GetAllShiftEat()
      .then(res => {
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  },
};
