import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import {
    Keyboard,
    Platform,
    Text,
    TouchableOpacity,
    View,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Image,
    RefreshControl,
    FlatList
} from 'react-native';
import { Header, Icon, Divider } from 'react-native-elements';
import { AppStyles, getStatusBarHeight } from '@theme';
import colorsToDo from '../../TM/To_Do/css/style_ToDo';
import stylesToDo from '../../TM/To_Do/css/style_ToDo';
import { API, API_TODO } from '@network';
import LoadingComponent from '../../component/LoadingComponent';
// import { FuncCommon } from '../../../utils';
class index_ToDo_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            DataCount: [],
            UserInformation: null,
        };
        this.ListCategories = [
            {
                key: 'Myday',
                icon: 'light-down',
                typeIcon: 'entypo',
                colorIcon: '#8B8378',
                Title: 'Ngày của tôi',
                CountContent: 0,
                count: 1
            },
            {
                key: 'IsFollow',
                icon: 'star-o',
                typeIcon: 'font-awesome',
                colorIcon: '#FFD700',
                Title: 'Quan trọng',
                CountContent: 3,
                count: 2
            },
            {
                key: 'Myday',
                icon: 'calendar',
                typeIcon: 'feather',
                colorIcon: '#008B00',
                Title: 'Đã lập kế hoạch',
                CountContent: 0,
                count: 3
            },
            {
                key: 'Myday',
                icon: 'user',
                typeIcon: 'feather',
                colorIcon: '#B23AEE',
                Title: 'Đã giao cho bạn',
                CountContent: 0,
                count: 4
            },
            {
                key: 'Myday',
                icon: 'shield',
                typeIcon: 'feather',
                colorIcon: '#4876FF',
                Title: 'Tác vụ',
                CountContent: 0,
                count: 5
            }
        ];
    }
    componentDidMount() {
        this.Information();
    };
    componentWillReceiveProps = (nextProps) => {
        if (nextProps.moduleId === "Back") {
            this.ListCategories = [
                {
                    key: 'Myday',
                    icon: 'light-down',
                    typeIcon: 'entypo',
                    colorIcon: '#8B8378',
                    Title: 'Ngày của tôi',
                    CountContent: 0,
                    count: 1
                },
                {
                    key: 'IsFollow',
                    icon: 'star-o',
                    typeIcon: 'font-awesome',
                    colorIcon: '#FFD700',
                    Title: 'Quan trọng',
                    CountContent: 3,
                    count: 2
                },
                {
                    key: 'Myday',
                    icon: 'calendar',
                    typeIcon: 'feather',
                    colorIcon: '#008B00',
                    Title: 'Đã lập kế hoạch',
                    CountContent: 0,
                    count: 3
                },
                {
                    key: 'Myday',
                    icon: 'user',
                    typeIcon: 'feather',
                    colorIcon: '#B23AEE',
                    Title: 'Đã giao cho bạn',
                    CountContent: 0,
                    count: 4
                },
                {
                    key: 'Myday',
                    icon: 'shield',
                    typeIcon: 'feather',
                    colorIcon: '#4876FF',
                    Title: 'Tác vụ',
                    CountContent: 0,
                    count: 5
                }
            ];0
            this.Information();
        }
    }
    Information() {
        API.getProfile().then(rs => {
            this.state.UserInformation = rs.data;
        }).catch(error => {
            console.log("error:", error);
            Alert.alert("Thông báo", "Có lỗi khi lấy thông tin người đăng nhập")
        });
        this.GetAllCategory();
    }
    GetAllCategory() {
        this.setState({ loading: true });
        try {
            var obj = {
                CategoryOfTaskGuid: null,
                Keyword: ""
            };
            API_TODO.ListCategoryOfTasks(obj).then(rs => {
                if (rs.data.errorCode === 200) {
                    var ListView = this.ListCategories;
                    var data = JSON.parse(rs.data.data);
                    this.state.DataCount = JSON.parse(rs.data.data_v2);
                    this.ListCategories[0].CountContent = this.state.DataCount[0].CountToday;
                    this.ListCategories[1].CountContent = this.state.DataCount[0].CountFollow;
                    var count = 5;
                    for (let i = 0; i < data.length; i++) {
                        ListView.push({
                            key: data[i].CategoryOfTaskGuid,
                            icon: 'list',
                            typeIcon: 'feather',
                            colorIcon: '#9fa1a3',
                            Title: data[i].Title,
                            CountContent: 0,
                            count: count + 1
                        })
                        count = count + 1
                    };
                    this.setState({ loading: false })
                }
            });
        } catch (error) {
            console.log('Có lỗi khi lấy dữ liệu');
        }
    }
    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1 }}
            >
                <TouchableWithoutFeedback
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    {this.state.loading !== true ?
                        <View style={[stylesToDo.container_Full, { backgroundColor: '#fff' }]}>
                            {this.state.UserInformation !== null ? this.Header() : null}
                            {this.ViewListCategory(this.ListCategories)}
                            {this.ViewFooter()}
                        </View>
                        :
                        <LoadingComponent backgroundColor={'#fff'} />
                    }
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }
    //#region Header
    Header = () => {
        if (Platform.OS === "ios") {
            return (
                <Header
                    containerStyle={[stylesToDo.squareContainer, stylesToDo.background]}
                    placement="left"
                    leftComponent={this.renderLeftComponent()}
                    rightComponent={this.renderRightComponent()}
                />
            )
        } else {
            return (
                <View style={[stylesToDo.squareContainer, stylesToDo.background, { flexDirection: 'row' }]}>
                    {this.renderLeftComponent()}
                    {this.renderRightComponent()}
                </View>
            )
        }
    }
    renderLeftComponent = () => {
        return (
            <TouchableOpacity style={[{ flex: 1, flexDirection: 'row', alignItems: 'center' }]}>
                <View style={[stylesToDo.contentCenter, { backgroundColor: '#9A32CD', height: 30, width: 30, borderRadius: 20, marginLeft: 10, marginRight: 10 }]}>
                    <Text style={{ color: '#fff' }}>{this.state.UserInformation.FullName.substring(0, 1)}</Text>
                </View>
                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{this.state.UserInformation.FullName}</Text>
            </TouchableOpacity>
        );
    }
    renderRightComponent = () => {
        return (
            <TouchableOpacity style={[stylesToDo.contentCenter, { marginRight: 10 }]}>
                <Icon name={'ios-search'} type={'ionicon'} color={colorsToDo.primary} size={25} />
            </TouchableOpacity>
        );
    }

    //#endregion

    //#region List
    ViewListCategory = (rs) => {
        return (
            <FlatList
                containerStyle={{ flex: 1 }}
                data={rs}
                renderItem={({ item, i }) => (
                    <View key={i} style={{ padding: 10, flexDirection: 'column' }}>
                        <TouchableOpacity style={{ flexDirection: 'row', paddingRight: 5, paddingLeft: 5 }} onPress={() => this.NextPage(item.key, item.Title)}>
                            <View style={{ flex: 1, alignItems: 'flex-start' }}>
                                <Icon name={item.icon} type={item.typeIcon} color={item.colorIcon} size={22} />
                            </View>
                            <View style={[stylesToDo.justifyContent, { flex: 7 }]}>
                                <Text style={[stylesToDo.styleText]}>{item.Title}</Text>
                            </View>
                            <View style={[stylesToDo.justifyContent, { flex: 1, alignItems: 'flex-end' }]}>
                                <Text style={[stylesToDo.styleText, { color: colorsToDo.gray }]}>{item.CountContent === 0 ? '' : item.CountContent}</Text>
                            </View>
                        </TouchableOpacity>
                        {item.count === 5 ? <View style={{ borderWidth: 0.5, borderColor: '#E8E8E8', marginTop: 20 }} /> : null}
                    </View>
                )
                }
                keyExtractor={(item, index) => index.toString()}
                //onEndReached={() => this.loadMoreData()}
                refreshControl={
                    < RefreshControl
                        refreshing={false}
                        onRefresh={() => this._onRefresh}
                        tintColor="#f5821f"
                        titleColor="#fff"
                        colors={['red']}
                    />
                }
            />
        )
    }
    //#endregion

    //#region Footer
    ViewFooter = () => {
        return (
            <View style={[stylesToDo.alignItems, { flexDirection: 'row', padding: 10, bottom: 0, backgroundColor: '#fff' }]}>
                <TouchableOpacity style={{ flexDirection: 'row', flex: 5 }} onPress={() => this.AddCategory()}>
                    <Icon name={'plus'} type={'feather'} color={colorsToDo.primary} size={25} iconStyle={{ paddingLeft: 5, paddingRight: 10 }} />
                    <Text style={[stylesToDo.styleText, { color: colorsToDo.primary }]} >Danh sách mới</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-end', paddingRight: 10 }}>
                    <Icon name={'folder-plus'} type={'feather'} color={colorsToDo.primary} size={20} />
                </TouchableOpacity> */}
            </View>
        )
    }
    //#endregion

    //#region NextPage
    NextPage = (key, title) => {
        //dữ liệu để chuyển tab chi tiết
        var _CategoryOfTaskGuid = null;
        var _DateTime = null;
        var _IsFollow = null;
        switch (key) {
            case "Myday":
                _DateTime = new Date();
                break;
            case "IsFollow":
                _IsFollow = 1;
                break;
            default:
                _CategoryOfTaskGuid = key;
                break;
        }
        Actions.listDataToDo({
            Data: {
                CategoryOfTaskGuid: _CategoryOfTaskGuid,
                DateTime: _DateTime,
                IsFollow: _IsFollow,
                Title: title
            }
        }
        );
    }
    AddCategory = () => {
        Actions.listDataToDo({
            Data: {
                CategoryOfTaskGuid: null,
                DateTime: null,
                IsFollow: null,
                Title: ""
            },
            AddCategory: true
        })
    }
    //#endregion
}
//Redux
const mapStateToProps = state => ({
})
//Using call para public all page
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(index_ToDo_Component);
