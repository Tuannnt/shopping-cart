import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  FlatList,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {Icon, Header, Divider, Button} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../../utils';
import Fonts from '../../../theme/fonts';
import {API_RegisterCars} from '@network';
import API from '../../../network/API';
import {Container, Item, Label, ListItem} from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import DatePicker from 'react-native-datepicker';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import controller from './controller';
import moment from 'moment';
import RNPickerSelect from 'react-native-picker-select';
import Toast from 'react-native-simple-toast';
import listCombobox from './listCombobox';
var width = Dimensions.get('window').width; //full width
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
const ItemCode = {
  PERSONNEL: 0,
  SELL: 1,
  CUSTOMER: 2,
  PURCHASE: 3,
  ACCOUNTANT: 4,
  WAREHOUSE: 5,
  OPERATIONAL: 6,
  DOCUMENT: 7,
  MARKETING: 8,
  PROJECT: 9,
  MANUFACTURING: 10,
  PROPERTY: 11,
  ESTABLISH: 12,
  LOGOUT: 13,
};
class openAdvances extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      rows: [],
      staticParam: {
        AdvanceGuid: '',
      },
      AdvanceGuid: null,
      Data: [],
      listWofflowData: [],
      loading: false,
      isModalVisible: false,
      txtComment: '',
      checkInLogin: '',
      LoginName: '',
      LiEmp: [],
      listBank: [],
      ActualRefundDate: FuncCommon.ConDate(new Date(), 0),
      RefundDate: FuncCommon.ConDate(new Date(), 0),
      FullName: '',
      selected2: [],
      Reciever: [],
      headerTable: listCombobox.headerTable,
    };
  }
  componentDidMount() {
    this.state.AdvanceGuid = this.props.RecordGuid || this.props.RowGuid;
    Promise.all([this.getItem(), this.GetEmployee(), this.getBank()]);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }

  getItem() {
    let headerTable = listCombobox.headerTable;
    let id = {Id: this.props.RecordGuid || this.props.RowGuid};
    API_RegisterCars.Advances_Items(id.Id)
      .then(res => {
        let rows = JSON.parse(res.data.data).detail;
        let _data = _.cloneDeep(JSON.parse(res.data.data).model);
        // trang thai tien mat chuyen khoan
        const typeCash = 'T';
        if (_data.AdvanceForm === typeCash) {
          headerTable = listCombobox.headerTableCash;
        }
        //
        this.setState({
          rows: rows.map(row => ({
            ...row,
            RefundAmount: row.RefundAmount ? row.RefundAmount + '' : '',
          })),
          Data: _data,
          headerTable,
        });
        this.state.Data.TotalAdvance = this.state.Data.TotalAdvance
          ? this.state.Data.TotalAdvance.toString()
          : '';
        let checkin = {
          AdvanceGuid: JSON.parse(res.data.data).model.AdvanceGuid,
          WorkFlowGuid: JSON.parse(res.data.data).model.WorkFlowGuid,
        };
        API_RegisterCars.CheckLogin_Advances(checkin)
          .then(rs => {
            this.setState({checkInLogin: rs.data.data});
          })
          .catch(error => {
            console.log(error.data);
          });
        API.getProfile().then(_rs => {
          this.setState({LoginName: _rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log(error.data);
      });
  }
  getBank = () => {
    controller.GetBanksDetail(rs => {
      this.setState({listBank: rs});
    });
  };
  keyExtractor = (item, index) => index.toString();
  GetEmployee() {
    let _obj = {};
    API_RegisterCars.GetEmpAdvances(_obj)
      .then(rs => {
        var _listEmp = rs.data;
        let _reListEmp = [
          {
            label: 'Danh sách nhân viên',
            value: '',
          },
        ];
        for (var i = 0; i < _listEmp.length; i++) {
          _reListEmp.push({
            label: _listEmp[i].FullName,
            value: _listEmp[i].EmployeeGuid,
          });
        }
        this.setState({
          LiEmp: _reListEmp,
        });
      })
      .catch(error => {
        console.log('Error when call API GetEmployee Mobile.');
        console.log(error);
      });
  }

  Delete = () => {
    let id = {Id: this.props.RecordGuid || this.props.RowGuid};
    this.setState({loading: true});
    API_RegisterCars.Advances_Delete(id.Id)
      .then(response => {
        console.log(
          '==============ketquaXoa-errorCode: ' + response.data.Error,
        );
        if (response.data.Error === false) {
          this.setState({
            loading: false,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity(
            response.data.message,
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  };
  //#region chỉnh sửa phiếu tạm ứng
  setEventEditRoutings = () => {
    if (this.state.ViewEditRoutings === false) {
    }
    this.setViewOpen('ViewEditRoutings');
  };
  setEventDetailRoutings = () => {
    if (this.state.ViewDetailRoutings === false) {
    }
    this.setViewOpen('ViewDetailRoutings');
  };

  handleChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  helperBank = guid => {
    let index = this.state.listBank.findIndex(bank => bank.guid == guid);
    if (index === -1) {
      return '';
    }
    return this.state.listBank[index].name;
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Title}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 300}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.ObjectName}
          </Text>
        </TouchableOpacity>
        {this.state.Data.AdvanceForm !== 'T' && (
          <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
            <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
              {' '}
              {row.TKBanks}
            </Text>
          </TouchableOpacity>
        )}
        {this.state.Data.AdvanceForm !== 'T' && (
          <TouchableOpacity style={[AppStyles.table_td_custom, {width: 250}]}>
            <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
              {' '}
              {this.helperBank(row.BankGuid)}
            </Text>
          </TouchableOpacity>
        )}

        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'right'}]}>
            {' '}
            {FuncCommon.addPeriod(row.RefundAmount)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Description}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  CustomViewDetailRoutings = () => {
    return (
      <View>
        <View>
          <View style={{padding: 20}}>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Số phiếu :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {marginLeft: 10, textAlign: 'left'},
                  ]}>
                  {this.state.Data.AdvanceId}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Người tạm ứng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {marginLeft: 10, textAlign: 'left'},
                  ]}>
                  {this.state.Data.EmpAdvanceName}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Lý do tạm ứng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {marginLeft: 10, textAlign: 'left'},
                  ]}>
                  {this.state.Data.AdvanceReason}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày tạm ứng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {marginLeft: 10, textAlign: 'left'},
                  ]}>
                  {this.customDate(this.state.Data.ActualRefundDate)}
                </Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày hoàn ứng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {marginLeft: 10, textAlign: 'left'},
                  ]}>
                  {this.customDate(this.state.Data.RefundDate)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Tiền tạm ứng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {marginLeft: 10, textAlign: 'left'},
                  ]}>
                  {this.addPeriod(this.state.Data.TotalPayment)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Loại tạm ứng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {marginLeft: 10, textAlign: 'left'},
                  ]}>
                  {this.handleType(this.state.Data.Type)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Hình thức tạm ứng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {marginLeft: 10, textAlign: 'left'},
                  ]}>
                  {this.state.Data.AdvanceForm === 'T'
                    ? 'Tiền mặt'
                    : 'Chuyển khoản'}
                </Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
              </View>
              <View style={{flex: 2}}>
                {this.state.Data.Status == 'Y' ? (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        marginLeft: 10,
                        color: AppColors.AcceptColor,
                        textAlign: 'left',
                      },
                    ]}>
                    {this.state.Data.StatusWF}
                  </Text>
                ) : (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        marginLeft: 10,
                        color: AppColors.PendingColor,
                        textAlign: 'left',
                      },
                    ]}>
                    {this.state.Data.StatusWF}
                  </Text>
                )}
              </View>
            </View>
          </View>
          <Divider style={{marginTop: 10}} />
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                marginLeft: 10,
                flex: 1,
              }}>
              Chi tiết
            </Text>
          </View>
          <View style={{flex: 1}}>
            <View style={{marginTop: 5, marginBottom: 10, paddingBottom: 10}}>
              {/* Table */}
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'column', marginHorizontal: 10}}>
                  <View style={{flexDirection: 'row'}}>
                    {this.state.headerTable.map(item => {
                      if (item.hideInDetail) {
                        return null;
                      }
                      return (
                        <TouchableOpacity
                          key={item.title}
                          style={[AppStyles.table_th, {width: item.width}]}>
                          <Text style={AppStyles.Labeldefault}>
                            {item.title}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                  <View>
                    {this.state.rows.length > 0 ? (
                      <FlatList
                        data={this.state.rows}
                        renderItem={({item, index}) => {
                          return this.itemFlatList(item, index);
                        }}
                        keyExtractor={(rs, index) => index.toString()}
                      />
                    ) : (
                      <TouchableOpacity style={[AppStyles.table_foot]}>
                        <Text
                          style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                          Không có dữ liệu!
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      </View>
    );
  };
  CustomViewDetailRoutingsPayment = () => {
    return (
      <View>
        <View>
          <View style={{padding: 20}}>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Số phiếu :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault, {marginLeft: 10}]}>
                  {this.state.Data.AdvanceId}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault, {marginLeft: 10}]}>
                  {this.state.Data.EmpAdvanceName}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Lý do thanh toán :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault, {marginLeft: 10}]}>
                  {this.state.Data.AdvanceReason}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Hạn thanh toán :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault, {marginLeft: 10}]}>
                  {this.customDate(this.state.Data.ActualRefundDate)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>
                  Tổng tiền thanh toán :
                </Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault, {marginLeft: 10}]}>
                  {this.addPeriod(this.state.Data.TotalPayment)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Mô tả :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault, {marginLeft: 10}]}>
                  {this.state.Data.Description}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
              </View>
              <View style={{flex: 2}}>
                {this.state.Data.Status == 'Y' ? (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {marginLeft: 10, color: AppColors.AcceptColor},
                    ]}>
                    {this.state.Data.StatusWF}
                  </Text>
                ) : (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {marginLeft: 10, color: AppColors.PendingColor},
                    ]}>
                    {this.state.Data.StatusWF}
                  </Text>
                )}
              </View>
            </View>
          </View>
          <Divider style={{marginTop: 10}} />
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                marginLeft: 10,
                flex: 1,
              }}>
              Chi tiết
            </Text>
          </View>
          <View style={{flex: 1}}>
            <View style={{marginTop: 5, marginBottom: 10, paddingBottom: 10}}>
              {/* Table */}
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'column', marginHorizontal: 10}}>
                  <View style={{flexDirection: 'row'}}>
                    {this.state.headerTable.map(item => {
                      if (item.hideInDetail) {
                        return null;
                      }
                      return (
                        <TouchableOpacity
                          key={item.title}
                          style={[AppStyles.table_th, {width: item.width}]}>
                          <Text style={AppStyles.Labeldefault}>
                            {item.title}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                  <View>
                    {this.state.rows.length > 0 ? (
                      <FlatList
                        data={this.state.rows}
                        renderItem={({item, index}) => {
                          return this.itemFlatList(item, index);
                        }}
                        keyExtractor={(rs, index) => index.toString()}
                      />
                    ) : (
                      <TouchableOpacity style={[AppStyles.table_foot]}>
                        <Text
                          style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                          Không có dữ liệu!
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      </View>
    );
  };
  callbackOpenUpdate = () => {
    Actions.addAdvances({itemData: this.state.Data, rows: this.state.rows});
  };
  render() {
    const title =
      this.state.Data.TypeOfTicket === 'A'
        ? 'Chi tiết phiếu tạm ứng'
        : 'Chi tiết phiếu thanh toán';
    return (
      <Container>
        <View style={{flex: 30}}>
          <View
            style={{
              flex: 1,
              fontSize: 11,
              fontFamily: Fonts.base.family,
              backgroundColor: 'white',
            }}>
            <TabBar_Title title={title} callBack={() => this.onPressBack()} />
            <View
              style={{
                flex: 10,
              }}>
              <ScrollView>
                {this.state.Data.TypeOfTicket === 'A'
                  ? this.CustomViewDetailRoutings()
                  : this.CustomViewDetailRoutingsPayment()}
              </ScrollView>
            </View>
            <View style={{flex: 1}}>
              {this.state.checkInLogin !== '' ? (
                <TabBarBottom
                  //key để quay trở lại danh sách
                  onDelete={() => this.Delete()}
                  backListByKey="Advances"
                  keyCommentWF={{
                    ModuleId: 36,
                    RecordGuid: this.props.RecordGuid || this.props.RowGuid,
                    Title: this.state.Data.AdvanceId,
                  }}
                  onAttachment={() => this.openAttachments()}
                  callbackOpenUpdate={this.callbackOpenUpdate}
                  // tiêu đề hiển thị trong popup xử lý phiếu
                  Title={this.state.Data.AdvanceId}
                  //kiểm tra quyền xử lý
                  Permisstion={this.state.Data.Permisstion}
                  //kiểm tra bước đầu quy trình
                  checkInLogin={this.state.checkInLogin}
                  onSubmitWF={(callback, CommentWF) =>
                    this.submitWorkFlow(callback, CommentWF)
                  }
                />
              ) : null}
            </View>
          </View>
        </View>
      </Container>
    );
  }
  openAttachments() {
    var obj = {
      ModuleId: '17',
      RecordGuid: this.props.RecordGuid || this.props.RowGuid,
      notEdit: this.state.checkInLogin === '1' ? false : true,
    };
    Actions.attachmentComponent(obj);
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  handleType = type => {
    if (!type) {
      return '';
    }
    let res = listCombobox.ListType.find(x => type === x.value);
    if (!res) {
      return '';
    }
    return res.text;
  };
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    console.log('===customStatus====' + csStatus);

    if (csStatus === 'W') {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Kết Thúc';
    }
    return stringstatus.toString();
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
    let rs = this.state.LiEmp.find(x => x.EmployeeGuid === value);
    if (rs != null) {
      this.setState({FullName: rs.FullName});
    }
  }
  handleEmail(data) {
    this.setState({CommentWF: data});
  }
  setAdvanceReason(AdvanceReason) {
    this.state.Data.AdvanceReason = AdvanceReason;
    this.setState({Data: this.state.Data});
  }
  setTotalAdvance(TotalAdvance) {
    this.state.Data.TotalAdvance = TotalAdvance;
    this.setState({Data: this.state.Data});
  }
  setDescription(Description) {
    this.state.Data.Description = Description;
    this.setState({Data: this.state.Data});
  }
  setActualRefundDate(ActualRefundDate) {
    this.state.Data.ActualRefundDate = ActualRefundDate;
    this.setState({Data: this.state.Data});
  }
  setRefundDate(RefundDate) {
    this.state.Data.RefundDate = RefundDate;
    this.setState({Data: this.state.Data});
  }
  onValueChangeType(value) {
    this.setState({
      Type: value,
    });
  }
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      AdvanceGuid: this.state.Data.AdvanceGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'D') {
      API_RegisterCars.Approve_Advances(obj)
        .then(rs => {
          if (rs.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data);
        });
    } else {
      API_RegisterCars.NotApprove_Advances(obj)
        .then(rs => {
          console.log('====Lỗi');
          console.log(rs);
          if (rs.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);

            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data);
        });
    }
  }
  sendNoti(obj) {
    API.sendNotiByUser(obj)
      .then(res => {})
      .catch(error => {
        alert(res.data.message);
      });
  }
  renderFooter = () => {
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
  getParsedDate(strDate, data) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!
      var hours = date.getHours();
      var min = date.getMinutes();
      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (data == '1') {
        // "1":  dd-mm-yyy HH:mm
        if (hours < 10) {
          hours = '0' + hours;
        }
        if (min < 10) {
          min = '0' + min;
        }
        date = dd + '-' + mm + '-' + yyyy + ' ' + hours + ':' + min;
        return date.toString();
      } else {
        date = dd + '-' + mm + '-' + yyyy;
        return date.toString();
      }
    } else {
      return '';
    }
  }
  ConDate = function(data, number) {
    try {
      if (data == null || data == '') {
        return '';
      }
      if (data !== null && data != '' && data != undefined) {
        try {
          if (data.indexOf('SA') != -1 || data.indexOf('CH') != -1) {
            if (data.indexOf('SA') != -1) {
            }
            if (data.indexOf('CH') != -1) {
            }
          }

          if (data.indexOf('Date') != -1) {
            data = data.substring(data.indexOf('Date') + 5);
            data = parseInt(data);
          }
        } catch (ex) {}
        var newdate = new Date(data);
        if (number == 3) {
          return newdate;
        }
        if (number == 2) {
          return '/Date(' + newdate.getTime() + ')/';
        }
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();
        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;
        if (mm < 10) mm = '0' + mm;
        if (number == 0) {
          return (todayDate = day + '/' + month + '/' + year);
        }
        if (number == 1) {
          return (todayDate =
            day + '/' + month + '/' + year + ' ' + hh + ':' + mm);
        }
        if (number == 4) {
          return new Date(year, month - 1, day);
        }
      } else {
        return '';
      }
    } catch (ex) {
      return '';
    }
  };
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(openAdvances);
