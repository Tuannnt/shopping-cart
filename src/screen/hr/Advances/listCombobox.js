import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  dropDownType: [
    {
      value: 'A',
      text: 'Tạm ứng',
    },
    {
      value: 'B',
      text: 'Thanh toán',
    },
  ],
  //#endregion
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Nội dung', width: 200},
    {title: 'Đối tượng nhận', width: 300},
    {title: 'Số tài khoản', width: 200},
    {title: 'Ngân hàng', width: 250},
    {title: 'Số tiền', width: 200},
    {title: 'Ghi chú', width: 200},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  headerTableCash: [
    {title: 'STT', width: 40},
    {title: 'Nội dung', width: 200},
    {title: 'Đối tượng nhận', width: 300},
    {title: 'Số tiền', width: 200},
    {title: 'Ghi chú', width: 200},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  ListType: [
    {
      value: 'A',
      text: 'Tạm ứng công trình',
    },
    {
      value: 'B',
      text: 'Tạm ứng đi công tác',
    },
    {
      value: 'C',
      text: 'Tạm ứng lương',
    },
    {
      value: 'D',
      text: 'Tạm ứng khác',
    },
  ],
};
