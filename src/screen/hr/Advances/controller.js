import {API_RegisterCars, API_ApplyOverTimes} from '@network';
import {ErrorHandler} from '@error';

export default {
  getNumberAuto(value, callback) {
    API_ApplyOverTimes.GetNumberAuto(value)
      .then(res => {
        console.log(res + '=======> get Number auto');
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  },
  GetBanksDetail(callback) {
    API_RegisterCars.GetBanksDetail()
      .then(res => {
        callback(JSON.parse(res.data.data));
      })
      .catch(error => {
        console.log(error.data);
      });
  },
  removeAttactment(data, list, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },
};
