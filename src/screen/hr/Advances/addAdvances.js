import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  FlatList,
  Keyboard,
  StyleSheet,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { Button, Icon, Divider } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { API_RegisterCars } from '@network';
import { Container } from 'native-base';
import { FuncCommon } from '../../../utils';
import DatePicker from 'react-native-datepicker';
import _ from 'lodash';
import { AppStyles, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RNPickerSelect from 'react-native-picker-select';
import Combobox from '../../component/Combobox';
import RequiredText from '../../component/RequiredText';
import controller from './controller';
import listCombobox from './listCombobox';
import Toast from 'react-native-simple-toast';
import ComboboxV2 from '../../component/ComboboxV2';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';

const picker = {
  inputIOS: {
    padding: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    padding: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
    borderRadius: 2,
  },
};
const LiAdvanceForm = [
  { value: 'C', label: 'Chuyển khoản' },
  { value: 'T', label: 'Tiền mặt' },
];
const ListType = [
  { value: 'A', label: 'Tạm ứng' },
  { value: 'B', label: 'Thanh toán' },
];
const styleCustomIos =
  Platform.OS === 'ios'
    ? {
      inputIOS: {
        fontSize: 16,
        paddingVertical: 10,
        paddingHorizontal: 10,
        color: 'black',
        paddingRight: 10, // to ensure the text is never behind the icon
      },
    }
    : {};
class AddAdvances extends Component {
  constructor(props) {
    super(props);
    this.DataProcess = FuncCommon.DataProcess();
    this.state = {
      Attachment: [],
      rows: [{}],
      refreshing: false,
      listBank: [],
      search: '',
      LiEmp: [],
      _listEmpDetail: [],
      selected: undefined,
      Dropdown: [
        {
          value: null,
          label: 'Chọn trạng thái',
        },
        {
          value: 'A',
          label: 'Tạm ứng công trình',
        },
        {
          value: 'B',
          label: 'Tạm ứng công tác',
        },
        {
          value: 'C',
          label: 'Tạm ứng lương',
        },
        {
          value: 'D',
          label: 'Tạm ứng khác',
        },
      ],
      TypeOfTicket: 'A',
      Type: 'A',
      TypeName: '',
      RefundDate: new Date(),
      ActualRefundDate: new Date(),
      TotalPayment: '0',
      AdvanceForm: 'C',
      headerTable: listCombobox.headerTable,
    };
    this.loading = false;
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  Submit() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 2000);
    const { Attachment } = this.state;
    var _lstTitlefile = [];
    let str = this.state.TypeOfTicket === 'A' ? 'Tạm ứng' : 'Thanh toán';
    var data1 = this.ConDate(this.state.ActualRefundDate, 4);
    var data2 = this.ConDate(this.state.RefundDate, 4);
    if (data1 > data2) {
      Toast.showWithGravity(
        'Ngày hoàn ứng phải lớn hơn hoặc bằng ngày tạm ứng',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.AdvanceReason) {
      Toast.showWithGravity(
        'Bạn cần nhập lý do ' + str,
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (this.state.AdvanceReason.length > 255) {
      Toast.showWithGravity(
        `Lý do ${str} không vượt quá 255 ký tự.`,
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (this.state.Description && this.state.Description.length > 500) {
      Toast.showWithGravity(
        `Lý do không vượt quá 500 ký tự.`,
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.TotalPayment) {
      Toast.showWithGravity(
        'Bạn cần nhập số tiền ' + str,
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (this.props.itemData) {
      this.Update();
      return;
    }
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    let _emp = this.state.LiEmp.find(x => x.value === this.state.selected2);
    let EmpAdvanceName = _emp.label;
    let EmpAdvanceId = _emp.id;
    let _obj = {
      FileAttachments: [],
      EmpAdvanceGuid: this.state.selected2,
      EmpAdvanceId,
      EmpAdvanceName,
      // EmpAdvanceName: this.state.FullName,
      TypeOfTicket: this.state.TypeOfTicket,
      AdvanceForm: this.state.AdvanceForm,
      Type: this.state.Type,
      AdvanceReason: this.state.AdvanceReason,
      RefundDate: FuncCommon.ConDate(this.state.RefundDate, 2),
      ActualRefundDate: FuncCommon.ConDate(this.state.ActualRefundDate, 2),
      TotalPayment: +this.state.TotalPayment,
      Description: this.state.Description,
      AdvanceId: this.state.AdvanceId,
      AdvanceType: 0,
      CurrencyId: 'VND',
      CurrencyRate: 1,
    };
    let lstDetails = _.cloneDeep(this.state.rows).map(x => {
      if (x.RefundAmount) {
        x.RefundAmount = +x.RefundAmount.replace(/\./g, '');
      }
      return x;
    });
    let auto = { Value: this.state.AdvanceId, IsEdit: this.state.isEdit };
    let _data = new FormData();
    _data.append('submit', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteAttach', JSON.stringify(undefined));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_RegisterCars.Advances_InsertAPI(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.callBackList();
        }
      })
      .catch(error => {
        console.log('Error when call API insert Mobile.');
        console.log(error);
      });
  }
  Update = () => {
    let edit = {
      ...this.props.itemData,
      EmpAdvanceGuid: this.state.selected2,
      TypeOfTicket: this.state.TypeOfTicket,
      AdvanceReason: this.state.AdvanceReason,
      RefundDate: FuncCommon.ConDate(this.state.RefundDate, 2),
      ActualRefundDate: this.state.ActualRefundDate
        ? FuncCommon.ConDate(this.state.ActualRefundDate, 2)
        : null,
      TotalPayment: +this.state.TotalPayment.replace(/\./g, ''),
      Description: this.state.Description,
      Type: this.state.Type,
    };
    let lstDetails = _.cloneDeep(this.state.rows).map(x => {
      if (x.RefundAmount) {
        x.RefundAmount = +x.RefundAmount.replace(/\./g, '');
      }
      return x;
    });
    let auto = { IsEdit: this.state.IsEdit };
    let _data = new FormData();
    _data.append('edit', JSON.stringify(edit));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteDetail', JSON.stringify([]));
    _data.append('DeleteAttach', JSON.stringify([]));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    _data.append('lstTitlefile', JSON.stringify([]));
    API_RegisterCars.Advances_Update(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode === 200) {
          Toast.showWithGravity(
            'Cập nhật thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        } else {
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };
  //#region hàm Quay lại
  callBackList(rs) {
    this.setState({ loading: false });
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  //#endregion
  componentDidMount() {
    const { itemData, rows } = this.props;
    if (itemData) {
      this.setState({
        rows: rows.map(row => ({
          ...row,
          RefundAmount: row.RefundAmount ? row.RefundAmount + '' : '',
        })),
        isEdit: false,
        AdvanceId: itemData.AdvanceId,
        FullName: itemData.FullName,
        DepartmentName: itemData.DepartmentName,
        selected2: itemData.EmpAdvanceGuid,
        TypeOfTicket: itemData.TypeOfTicket,
        AdvanceReason: itemData.AdvanceReason,
        ActualRefundDate: new Date(itemData.ActualRefundDate),
        RefundDate: new Date(itemData.RefundDate),
        TotalPayment: itemData.TotalPayment + '',
        Description: itemData.Description,
        Type: itemData.Type,
      });
    } else {
      this.setState({
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        selected2: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
      });
      this.getNumberAuto();
    }
    Promise.all([this.GetEmPloyeesDetail(), this.getEmp(), this.getBank()]);
  }
  getBank = () => {
    controller.GetBanksDetail(rs => {
      let result =
        rs.map(x => ({
          value: x.id,
          text: x.name,
          guid: x.guid,
        })) || [];
      this.setState({ listBank: result });
    });
  };
  getEmp = () => {
    API_RegisterCars.GetEmpAdvances({})
      .then(rs => {
        var _listEmp = rs.data.map(data => ({
          label: data.FullName,
          value: data.EmployeeGuid,
          id: data.ObjectId,
        }));
        this.setState({
          LiEmp: _listEmp,
        });
      })
      .catch(error => {
        console.log('Error when call API GetEmployee Mobile.');
        console.log(error);
      });
  };
  GetEmPloyeesDetail = (callback, search = '') => {
    API_RegisterCars.GetEmPloyeesDetail({ search })
      .then(rs => {
        let _listEmpDetail = [];
        let _data = JSON.parse(rs.data.data);
        if (_data) {
          _listEmpDetail = JSON.parse(rs.data.data).map(item => ({
            text: `${item.name}`,
            value: item.id,
            // EmployeeGuid: item.EmployeeGuid,
            // ObjectName: item.name,
            // BankAccount: item.BankAccount,
            BankCode: item.BankCode,
            BankName: item.BankName,
          }));
        }
        if (callback) {
          callback(_listEmpDetail);
        } else {
          this.setState({
            _listEmpDetail,
          });
        }
      })
      .catch(error => {
        console.log('Error when call API GetEmployee Mobile.');
        console.log(error);
      });
  };
  getNumberAuto = () => {
    const isPayment = this.state.TypeOfTicket === 'B';
    let str = 'QTDSCT_TU';
    if (isPayment) {
      str = 'QTDSCT_TT';
    }
    let val = {
      AliasId: str,
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({ isEdit: data.IsEdit, AdvanceId: data.Value });
    });
  };
  addRows = () => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({ rows: data });
  };
  onDelete = index => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({ rows: res });
  };
  handleChangeRows = (val, index, name) => {
    const { rows } = this.state;
    let res = _.cloneDeep(rows);
    let _total = '0';
    res[index][name] = val;
    if (name === 'ObjectId' && this.state.TypeOfTicket === 'A') {
      let _user = this.state._listEmpDetail.find(x => x.value === val);
      if (_user) {
        res[index].TKBanks = _user.BankAccount;
        res[index].EmployeeGuid = _user.EmployeeGuid;
        res[index].ObjectName = _user.ObjectName;
      }
      // bind du lieu so tai khoan




    }
    if (name === 'RefundAmount') {
      _total = res.reduce(
        (total, row) =>
          total + (+row.RefundAmount?.replace(/[.,\s]/g, '') || 0),
        0,
      );
      this.setState({ TotalPayment: _total + '' });
    }
    this.setState({ rows: res });
  };
  handleChangeAdvanceForm = val => {
    let headerTable = listCombobox.headerTable;
    const typeCash = 'T';
    if (val === typeCash) {
      headerTable = listCombobox.headerTableCash;
    }
    this.setState({
      AdvanceForm: val,
      headerTable,
    });
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{ flexDirection: 'row' }} key={index}>
        <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}>
          <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Title}
            autoCapitalize="none"
            onChangeText={Result => {
              this.handleChangeRows(Result, index, 'Title');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: 300 }]}
          onPress={() => {
            this.openEmps(index);
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.ObjectId,
                { color: 'black', fontSize: 14 },
              ]}>
              {row.ObjectId ? row.ObjectName : ''}
            </Text>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Icon
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        {this.state.AdvanceForm !== 'T' && (
          <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}>
            <TextInput
              style={AppStyles.FormInput_custom}
              underlineColorAndroid="transparent"
              value={row.TKBanks}
              autoCapitalize="none"
              onChangeText={TKBanks => {
                this.handleChangeRows(TKBanks, index, 'TKBanks');
              }}
            />
          </TouchableOpacity>
        )}
        {this.state.AdvanceForm !== 'T' && (
          <TouchableOpacity
            style={[AppStyles.table_td_custom, { width: 250 }]}
            onPress={() => {
              this.openOrders(index);
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 5,
              }}>
              <Text
                style={[
                  AppStyles.TextInput,
                  row.BankName,
                  { color: 'black', fontSize: 14 },
                ]}>
                {row.BankName ? row.BankName : ''}
              </Text>
              <View style={{ flex: 1, alignItems: 'flex-end' }}>
                <Icon
                  color={'black'}
                  name={'caretdown'}
                  type="antdesign"
                  size={10}
                />
              </View>
            </View>
          </TouchableOpacity>
        )}
        <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}>
          <TextInput
            style={[AppStyles.FormInput_custom, { textAlign: 'right' }]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={
              !row.RefundAmount
                ? row.RefundAmount
                : FuncCommon.addPeriodTextInput(row.RefundAmount)
            }
            onChangeText={RefundAmount => {
              this.handleChangeRows(RefundAmount, index, 'RefundAmount');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Description}
            autoCapitalize="none"
            onChangeText={Description => {
              this.handleChangeRows(Description, index, 'Description');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td, { width: 80 }]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  onChangeState = (key, value) => {
    this.setState({ [key]: value });
  };
  handleChangeTypeOfTicket = val => {
    this.setState(
      {
        TypeOfTicket: val,
      },
      () => {
        this.getNumberAuto();
      },
    );
  };
  render() {
    const { isEdit, AdvanceId, rows } = this.state;

    let total = FuncCommon.addPeriodTextInput(
      rows.reduce(
        (total, row) =>
          total + (+FuncCommon.formatCommaToDot(row.RefundAmount) || 0),
        0,
      ),
    );
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{ flex: 1 }}>
        <View style={[AppStyles.container]}>
          {!this.props.itemData ? (
            <TabBar_Title
              title={`Đăng ký ${this.state.TypeOfTicket === 'A' ? 'tạm ứng' : 'thanh toán'
                }`}
              callBack={() => {
                this.callBackList();
              }}
            />
          ) : (
            <TabBar_Title
              title={`Chỉnh sửa ${this.state.TypeOfTicket === 'A' ? 'tạm ứng' : 'thanh toán'
                }`}
              callBack={() => {
                this.callBackList();
              }}
            />
          )}
          <ScrollView ref={scrollView => (this.scrollView = scrollView)}>
            <View style={styles.containerContent}>
              <View style={{ padding: 10, paddingBottom: 0 }}>
                <Text style={AppStyles.Labeldefault}>Loại phiếu</Text>
                <View
                  style={{
                    ...AppStyles.FormInputPicker,
                    justifyContent: 'center',
                  }}>
                  <RNPickerSelect
                    doneText="Xong"
                    onValueChange={TypeOfTicket => {
                      this.handleChangeTypeOfTicket(TypeOfTicket);
                    }}
                    items={ListType || []}
                    placeholder={{}}
                    value={this.state.TypeOfTicket}
                    style={styleCustomIos}
                    disabled={this.props.itemData}
                  />
                </View>
              </View>
              <View style={{ padding: 10, paddingBottom: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={AppStyles.Labeldefault}>Mã phiếu</Text>
                </View>
                {isEdit && (
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    value={AdvanceId}
                    autoCapitalize="none"
                    onChangeText={Title => this.setState({ AdvanceId: Title })}
                  />
                )}
                {!isEdit && (
                  <View style={[AppStyles.FormInput]}>
                    <Text
                      style={[AppStyles.TextInput, { color: AppColors.gray }]}>
                      {AdvanceId}
                    </Text>
                  </View>
                )}
              </View>
              {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người tạo</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View> */}
              {this.state.TypeOfTicket === 'A' && (
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <Text style={AppStyles.Labeldefault}>Người tạm ứng</Text>
                  <View
                    style={{
                      ...AppStyles.FormInputPicker,
                      justifyContent: 'center',
                    }}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value => this.onValueChange2(value)}
                      items={this.state.LiEmp}
                      placeholder={{}}
                      value={this.state.selected2}
                      style={picker}
                      useNativeAndroidPickerStyle={false}
                    />
                  </View>
                </View>
              )}
              {this.state.TypeOfTicket === 'A' && (
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <Text style={AppStyles.Labeldefault}>Loại tạm ứng</Text>
                  <View
                    style={{
                      ...AppStyles.FormInputPicker,
                      justifyContent: 'center',
                    }}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value => this.onValueChangeType(value)}
                      items={this.state.Dropdown}
                      placeholder={{}}
                      value={this.state.Type}
                      style={styleCustomIos}
                    />
                  </View>
                </View>
              )}
              <View style={{ padding: 10, paddingBottom: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={AppStyles.Labeldefault}>
                    Lý do{' '}
                    {this.state.TypeOfTicket === 'A' ? 'Tạm ứng' : 'Thanh toán'}{' '}
                    <RequiredText />
                  </Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={this.state.AdvanceReason}
                  onChangeText={AdvanceReason =>
                    this.setAdvanceReason(AdvanceReason)
                  }
                  placeholder="Tối đa 255 ký tự"
                />
              </View>
              <View
                style={{
                  padding: 10,
                  paddingBottom: 0,
                  flexDirection: 'column',
                }}>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                  <Text style={AppStyles.Labeldefault}>
                    {this.state.TypeOfTicket === 'A'
                      ? 'Ngày tạm ứng'
                      : 'Hạn thanh toán'}
                  </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ width: '100%' }}>
                    <DatePicker
                      locale="vie"
                      locale={'vi'}
                      style={{ width: '100%' }}
                      date={this.state.ActualRefundDate}
                      mode="date"
                      androidMode="spinner"
                      placeholder="Chọn ngày"
                      format="DD/MM/YYYY"
                      confirmBtnText="Xong"
                      cancelBtnText="Huỷ"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: 4,
                          marginLeft: 0,
                        },
                        dateInput: {
                          marginRight: 36,
                        },
                      }}
                      onDateChange={date => this.setActualRefundDate(date)}
                    />
                  </View>
                </View>
              </View>
              {this.state.TypeOfTicket === 'A' && (
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày hoàn ứng dự kiến
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '100%' }}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{ width: '100%' }}
                        date={this.state.RefundDate}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setRefundDate(date)}
                      />
                    </View>
                  </View>
                </View>
              )}
              <View style={{ padding: 10, paddingBottom: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={AppStyles.Labeldefault}>
                    {this.state.TypeOfTicket === 'A'
                      ? 'Số tiền tạm ứng'
                      : 'Tổng tiền thanh toán'}
                    <RequiredText />
                  </Text>
                </View>
                {this.state.TypeOfTicket === 'A' ? (
                  <TextInput
                    style={[AppStyles.FormInput, { textAlign: 'right' }]}
                    value={
                      !this.state.TotalPayment
                        ? this.state.TotalPayment
                        : FuncCommon.addPeriodTextInput(this.state.TotalPayment)
                    }
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    onChangeText={TotalPayment =>
                      this.setTotalPayment(TotalPayment)
                    }
                  />
                ) : (
                  <Text style={[AppStyles.FormInput, { textAlign: 'right' }]}>
                    {`${total}đ`}
                  </Text>
                )}
              </View>
              {this.state.TypeOfTicket === 'A' && (
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <Text style={AppStyles.Labeldefault}>Hình thức tạm ứng</Text>
                  <View
                    style={{
                      ...AppStyles.FormInputPicker,
                      justifyContent: 'center',
                    }}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={AdvanceForm =>
                        this.handleChangeAdvanceForm(AdvanceForm)
                      }
                      items={LiAdvanceForm}
                      placeholder={{}}
                      value={this.state.AdvanceForm}
                      style={styleCustomIos}
                    />
                  </View>
                </View>
              )}

              <View style={{ padding: 10, paddingBottom: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={AppStyles.Labeldefault}>Mô tả</Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  numberOfLines={4}
                  value={this.state.Description}
                  onChangeText={Description => this.setDescription(Description)}
                  placeholder="Tối đa 500 ký tự"
                />
              </View>
              <View
                style={{
                  marginTop: 5,
                  marginBottom: 10,
                  paddingBottom: 10,
                }}>
                <View style={{ width: 150, marginLeft: 10 }}>
                  <Button
                    title="Thêm dòng"
                    type="outline"
                    size={15}
                    onPress={() => {
                      this.addRows();
                      this.scrollView.scrollToEnd();
                    }}
                    buttonStyle={{ padding: 5, marginBottom: 5 }}
                    titleStyle={{ marginLeft: 5 }}
                    icon={
                      <Icon
                        name="pluscircleo"
                        type="antdesign"
                        color={AppColors.blue}
                        size={14}
                      />
                    }
                  />
                </View>

                {/* Table */}
                <ScrollView horizontal={true}>
                  <View style={{ flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row' }}>
                      {this.state.headerTable.map(item => (
                        <TouchableOpacity
                          key={item.title}
                          style={[AppStyles.table_th, { width: item.width }]}>
                          <Text style={AppStyles.Labeldefault}>
                            {item.title}
                          </Text>
                        </TouchableOpacity>
                      ))}
                    </View>
                    <View>
                      {this.state.rows.length > 0 ? (
                        <FlatList
                          data={this.state.rows}
                          renderItem={({ item, index }) => {
                            return this.itemFlatList(item, index);
                          }}
                          keyExtractor={(rs, index) => index.toString()}
                        />
                      ) : (
                        <TouchableOpacity style={[AppStyles.table_foot]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              { textAlign: 'left' },
                            ]}>
                            Không có dữ liệu!
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  </View>
                </ScrollView>
                {!this.props.itemData && (
                  <View
                    style={{
                      flexDirection: 'column',
                      padding: 10,
                      marginBottom: 20,
                    }}>
                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                        <Text style={{ color: 'red' }}> </Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          flexDirection: 'row',
                        }}
                        onPress={() => this.openAttachment()}>
                        <Icon
                          name="attachment"
                          type="entypo"
                          size={15}
                          color={AppColors.ColorAdd}
                        />
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            { color: AppColors.ColorAdd },
                          ]}>
                          {' '}
                          Chọn file
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <Divider style={{ marginBottom: 10 }} />
                    {this.state.Attachment.length > 0
                      ? this.state.Attachment.map((para, i) => (
                        <View
                          key={i}
                          style={[
                            styles.StyleViewInput,
                            { flexDirection: 'row', marginBottom: 3 },
                          ]}>
                          <View
                            style={[
                              AppStyles.containerCentered,
                              { padding: 10 },
                            ]}>
                            <Image
                              style={{
                                width: 30,
                                height: 30,
                                borderRadius: 10,
                              }}
                              source={{
                                uri: this.FileAttackments.renderImage(
                                  para.FileName,
                                ),
                              }}
                            />
                          </View>
                          <TouchableOpacity
                            key={i}
                            style={{ flex: 1, justifyContent: 'center' }}>
                            <Text style={AppStyles.Textdefault}>
                              {para.FileName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[
                              AppStyles.containerCentered,
                              { padding: 10 },
                            ]}
                            onPress={() => this.removeAttactment(para)}>
                            <Icon
                              name="close"
                              type="antdesign"
                              size={20}
                              color={AppColors.ColorDelete}
                            />
                          </TouchableOpacity>
                        </View>
                      ))
                      : null}
                  </View>
                )}
              </View>
            </View>
          </ScrollView>

          <View>
            <Button
              buttonStyle={{ backgroundColor: AppColors.ColorButtonSubmit }}
              title="Lưu"
              onPress={() => this.Submit()}
            />
          </View>
          <OpenPhotoLibrary
            callback={this.callbackLibarary}
            openLibrary={this.openLibrary}
          />
          <ComboboxV2
            callback={this.ChoiceAtt}
            data={listCombobox.ListComboboxAtt}
            eOpen={this.openCombobox_Att}
          />
          {this.state.listBank.length > 0 && (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeOrder}
              data={this.state.listBank}
              nameMenu={'Chọn'}
              eOpen={this.openComboboxOrder}
              position={'bottom'}
              value={null}
            />
          )}
          {this.state._listEmpDetail.length > 0 && (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeEmp}
              data={this.state._listEmpDetail}
              nameMenu={'Chọn'}
              eOpen={this.openComboboxEmp}
              position={'bottom'}
              value={null}
              callback_SearchPaging={(callback, textsearch) => {
                this.GetEmPloyeesDetail(callback, textsearch);
              }}
            />
          )}
        </View>
      </KeyboardAvoidingView>
    );
  }
  _openComboboxEmp() { }
  openComboboxEmp = d => {
    this._openComboboxEmp = d;
  };

  openEmps(index) {
    this.setState({ currentIndexItem: index }, () => {
      this._openComboboxEmp();
    });
  }
  ChangeEmp = rs => {
    let rsdetal = {};
    const { currentIndexItem, rows } = this.state;
    let { value, text } = rs;
    if (!rs) {
      return;
    }

    if (this.state.TypeOfTicket === 'A') {
      API_RegisterCars.GetInfoEmp(value)
        .then(_rs => {
          if (_rs.data) {
            rsdetal = _rs.data;
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.ObjectName = text;
        row.ObjectId = value;
        if (rsdetal.EmployeeGuid) {
          row.EmployeeGuid = rsdetal.EmployeeGuid;
        }
        if (rsdetal.TKBanks) {
          row.TKBanks = rsdetal.TKBanks;
        }
        // bind du lieu so tai khoan
      }
      return row;
    });
    this.setState({ rows: data, currentIndexItem: null });
  };
  //#region openAttachment
  _openCombobox_Att() { }
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#region open img
  _open() { }
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({ Attachment: this.state.Attachment });
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({ Attachment: this.state.Attachment });
    } catch (err) {
      this.setState({ loading: false });
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion
  _openComboboxOrder() { }
  openComboboxOrder = d => {
    this._openComboboxOrder = d;
  };

  openOrders(index) {
    this.setState({ currentIndexItem: index }, () => {
      this._openComboboxOrder();
    });
  }
  ChangeOrder = rs => {
    const { currentIndexItem, rows, listBank } = this.state;
    let { value, text } = rs;
    if (!rs || !value) {
      return;
    }
    let bank = listBank.find(x => x.value === value);
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.BankCode = value;
        row.BankName = text;
        row.BankGuid = bank.guid;
      }
      return row;
    });
    this.setState({ rows: data, currentIndexItem: null });
  };
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{ color: 'black' }}
          name={'arrowleft'}
          type="antdesign"
          size={30}
          onPress={() => this.indexRegistercar()}
        />
      </View>
    );
  }
  _openCombobox() { }
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCombobox() {
    this._openCombobox();
  }
  setAdvanceReason(AdvanceReason) {
    this.setState({ AdvanceReason });
  }
  setTotalPayment(TotalPayment) {
    this.setState({ TotalPayment });
  }
  setDescription(Description) {
    this.setState({ Description });
  }
  setActualRefundDate(ActualRefundDate) {
    this.setState({ ActualRefundDate: FuncCommon.ConDate(ActualRefundDate, 99) });
  }
  setRefundDate(RefundDate) {
    this.setState({ RefundDate: FuncCommon.ConDate(RefundDate, 99) });
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
    let rs = this.state.LiEmp.find(x => x.EmployeeGuid === value);
    if (rs != null) {
      this.setState({ FullName: rs.FullName });
    }
  }
  onValueChangeType(value) {
    this.setState({
      Type: value,
    });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  ConDate = function (data, number) {
    try {
      if (data == null || data == '') {
        return '';
      }
      if (data !== null && data != '' && data != undefined) {
        try {
          if (data.indexOf('SA') != -1 || data.indexOf('CH') != -1) {
            if (data.indexOf('SA') != -1) {
            }
            if (data.indexOf('CH') != -1) {
            }
          }

          if (data.indexOf('Date') != -1) {
            data = data.substring(data.indexOf('Date') + 5);
            data = parseInt(data);
          }
        } catch (ex) { }
        var newdate = new Date(data);
        if (number == 3) {
          return newdate;
        }
        if (number == 2) {
          return '/Date(' + newdate.getTime() + ')/';
        }
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();
        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;
        if (mm < 10) mm = '0' + mm;
        if (number == 0) {
          return (todayDate = day + '/' + month + '/' + year);
        }
        if (number == 1) {
          return (todayDate =
            day + '/' + month + '/' + year + ' ' + hh + ':' + mm);
        }
        if (number == 4) {
          return new Date(year, month - 1, day);
        }
      } else {
        return '';
      }
    } catch (ex) {
      return '';
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddAdvances);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 10,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
