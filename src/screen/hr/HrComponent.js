import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {FlatGrid} from 'react-native-super-grid';
import Icon from 'react-native-vector-icons/AntDesign';
import {Actions} from 'react-native-router-flux';
import {Divider, ListItem} from 'react-native-elements';
import {Switch} from 'native-base';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  gridView: {
    marginTop: 5,
    flex: 1,
    height: '50%',
  },
  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#f6b801',
    backgroundColor: '#fff',
    // '#9702FF'
    padding: 10,
    height: 100,
  },
  itemName: {
    fontSize: 16,
    color: 'black',
    fontWeight: '600',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 50,
    color: '#f6b801',
  },
});
const items = [
  {name: 'Nhân sự', icon: 'user', code: 'user'},
  {name: 'Xin nghỉ phép', icon: 'barschart'},
  {name: 'Đăng ký xe', icon: 'shoppingcart', code: 'Cars'},
  {name: 'Hợp đồng', icon: 'edit'},
  {name: 'Bảo hiểm', icon: 'folderopen'},
  {name: 'Lương', icon: 'message1'},
  {name: 'Đăng ký ăn', icon: 'notification'},
  {name: 'Làm thêm giờ', icon: 'wallet'},
  {name: 'Đi công tác', icon: 'setting', code: 'applyOutsides'},
];

class HrComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView style={{height: '75%'}}>
          <FlatGrid
            itemDimension={100}
            items={items}
            style={[styles.gridView]}
            renderItem={({item, index}) => (
              <View style={[styles.itemContainer]}>
                <Icon
                  style={styles.itemIcon}
                  name={item.icon}
                  onPress={() => this.onclickItem(item)}
                />
                <Text style={styles.itemName}>{item.name}</Text>
              </View>
            )}
          />
        </ScrollView>
      </View>
    );
  }
  onclickItem = item => {
    console.log('================> ' + JSON.stringify(item));
    if (item.code == 'user') {
      Actions.employees();
    } else if (item.code == 'applyOutsides') {
      Actions.applyOutsidesListAll();
    } else if (item.code == 'pencil') {
      Actions.applyLeaves();
    } else {
      Actions.shoping({item: item});
    }
  };
  onclickItem = item => {
    console.log('================> ' + JSON.stringify(item));
    switch (item.code) {
      case 'user':
        Actions.employees();
        break;
      case 'Cars':
        Actions.indexRegistercar();
        break;
      default:
        break;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HrComponent);
