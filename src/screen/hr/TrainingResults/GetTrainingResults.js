import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  ToastAndroid,
  Alert,
} from 'react-native';
import {
  DatePicker,
  Label,
  Item,
  Input,
  Container,
  Content,
  Picker,
  Button,
} from 'native-base';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_HR, API} from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, ListItem, Header, Icon} from 'react-native-elements';
import Fonts from '../../../theme/fonts';
import {Actions} from 'react-native-router-flux';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import Toast from 'react-native-simple-toast';

const styles = StyleSheet.create({
  styletextLable: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  styletext: {
    fontSize: 14,
    padding: 5,
  },
  textLableCol2: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  textCol2: {
    fontSize: 14,
    padding: 5,
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
  },
  containerContent: {
    flex: 10,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
});
const options = [
  {
    component: <Text style={{color: 'crimson', fontSize: 20}}>Đóng</Text>,
  },
  {
    component: <Text style={{color: 'blue', fontSize: 20}}>Duyệt phiếu</Text>,
    height: 80,
  },
  {
    component: <Text style={{color: 'blue', fontSize: 20}}>Trả lại</Text>,
    height: 80,
  },
];

class GetTrainingResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      ListReason: [
        {
          ReasonName: 'Lý do',
          ReasonLeaveWorkId: '',
        },
      ],
      selected2: [],
      //StartTime: null,
      viewId: '',
    };
  }
  setPickUpLocation(PickUpLocation) {
    this.state.Data.PickUpLocation = PickUpLocation;
    this.setState({Data: this.state.Data});
  }

  componentDidMount(): void {
    if (this.props.RecordGuid !== undefined) {
      this.state.viewId = this.props.RecordGuid;
    } else {
      this.state.viewId = this.props.viewId;
    }
    this.getItem();
    this.GetReason();
  }
  onPressBack = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'back', ActionTime: new Date().getTime()});
  };
  setDescription(Description) {
    this.state.Data.Description = Description;
    this.setState({Data: this.state.Data});
  }
  setTitle(Title) {
    this.state.Data.Title = Title;
    this.setState({Data: this.state.Data});
  }
  setStartDate(StartDate) {
    this.state.Data.StartDate = StartDate;
    this.setState({Data: this.state.Data});
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  setOtherReason(OtherReason) {
    this.state.Data.OtherReason = OtherReason;
    this.setState({Data: this.state.Data});
  }
  setDescription(Description) {
    this.state.Data.Description = Description;
    this.setState({Data: this.state.Data});
  }

  rederType = () => (
    <Picker
      mode="dropdown"
      iosIcon={<Icon name="arrow-down" />}
      stylerederType={{width: undefined}}
      placeholder="Chọn giờ ăn"
      placeholderStyle={{color: '#bfc6ea'}}
      placeholderIconColor="#007aff"
      selectedValue={this.state.Type}
      onValueChange={this.onValueChangeType.bind(this)}>
      <Picker.Item label="12 giờ" value="A" />
      <Picker.Item label="18 giờ" value="B" />
      <Picker.Item label="24 giờ" value="C" />
      <Picker.Item label="Ăn ngoài" value="D" />
    </Picker>
  );
  Update() {
    let _obj = {
      ResignationFormGuid: this.state.viewId,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      EmployeeGuid: this.state.Data.EmployeeGuid,
      EmployeeId: this.state.Data.EmployeeId,
      EmployeeName: this.state.Data.EmployeeName,
      LoginName: this.state.Data.LoginName,
      DepartmentGuid: this.state.Data.DepartmentGuid,
      DepartmentId: this.state.Data.DepartmentId,
      DepartmentName: this.state.Data.DepartmentName,
      Title: this.state.Data.Title,
      OtherReason: this.state.Data.OtherReason,
      StartDate: this.state.Data.StartDate,
      Description: this.state.Data.Description,
      ReasonLeaveWorkId: this.state.selected2,
    };
    if (_obj.Title === null || _obj.Title === undefined || _obj.Title === '') {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập tiêu đề',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (
      _obj.ReasonLeaveWorkId === null ||
      _obj.ReasonLeaveWorkId === undefined ||
      _obj.ReasonLeaveWorkId === '' ||
      _obj.ReasonLeaveWorkId === 0
    ) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa chọn lý do',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    API_HR.ResignationForms_Update(_obj)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          console.log(_rs);
        } else {
          Toast.showWithGravity(
            'Cập nhật thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  }
  onAttachment() {
    Actions.attachmentComponent({
      ModuleId: '5',
      RecordGuid: this.state.viewId,
    });
  }
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_HR.deleteTrainingResults(id.Id)
      .then(response => {
        console.log(
          '==============ketquaXoa-errorCode: ' + response.data.Error,
        );
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            TrainingResultGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };

  getItem() {
    let id = this.state.viewId;
    var obj = {
      TrainingResultGuid: this.state.viewId,
    };
    API_HR.getTrainingResults(obj)
      .then(res => {
        // let StartDate1 = JSON.parse (res.data.data).StartDate;
        // let StartDate11 = new Date (StartDate1);
        this.setState({
          Data: JSON.parse(res.data.data),
          //StartDate: StartDate11,
          //selected2: JSON.parse (res.data.data).ReasonLeaveWorkId,
          //Quantity : JSON.parse (res.data.data).Quantity.toString()
        });
        let checkin = {
          TrainingResultGuid: this.state.viewId,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_HR.CheckStart(checkin)
          .then(res => {
            this.setState({checkInLogin: res.data.data});
            console.log('===checkin====' + res.data.data);
            console.log('callback==' + this.props.LoginName);
          })
          .catch(error => {
            console.log(error.data.data);
          });
        API.getProfile().then(rs => {
          console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
          this.setState({LoginName: rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
      });
  }
  showActionSheet = () => this.actionSheet.show();
  getActionSheetRef = ref => (this.actionSheet = ref);
  GetReason() {
    let _obj = {};
    API_HR.GetReason(_obj)
      .then(rs => {
        var _listReason = rs.data;
        let _reListReason = [
          {
            ReasonName: 'Mời bạn chọn lý do',
            ReasonLeaveWorkId: '',
          },
        ];
        for (var i = 0; i < _listReason.length; i++) {
          _reListReason.push({
            ReasonName: _listReason[i].ReasonName,
            ReasonLeaveWorkId: _listReason[i].ReasonLeaveWorkId,
          });
        }
        this.setState({
          ListReason: _reListReason,
        });
      })
      .catch(error => {
        console.log('Error when call API GetCar Mobile.');
        console.log(error);
      });
  }
  renderReason = () => (
    <Picker
      mode="dropdown"
      iosIcon={<Icon name="arrow-down" />}
      style={{width: undefined}}
      placeholder="Chọn lý do"
      placeholderStyle={{color: '#bfc6ea'}}
      placeholderIconColor="#007aff"
      selectedValue={this.state.selected2}
      onValueChange={this.onValueChange2.bind(this)}>
      {this.state.ListReason && this.state.ListReason.length > 0 ? (
        this.state.ListReason.map((_item, _index) => (
          <Picker.Item
            key={_index}
            label={_item.ReasonName}
            value={_item.ReasonLeaveWorkId}
          />
        ))
      ) : (
        <Picker.Item key={_index} label="Chọn lý do" value="" />
      )}
    </Picker>
  );
  CustomView = item => (
    <TouchableWithoutFeedback
      style={{flex: 1}}
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <TabBar_Title
          title={'Chi tiết phiếu'}
          callBack={() => this.onPressBack()}
          //FormAttachment={true}
          //CallbackFormAttachment={() => this.openAttachments()}
        />
        <Divider />
        {/*hiển thị nội dung chính*/}
        {this.state.checkInLogin == 1 ? (
          <View style={{flex: 10, padding: 15}}>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Mã phiếu:</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={[AppStyles.Textdefault]}>
                  {item.TrainingRequestId}
                </Text>
              </View>
            </View>
            {/*Người tạo phiếu*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text>
              </View>
            </View>
            {/*Phòng ban*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {item.DepartmentName}
                </Text>
              </View>
            </View>
            {/*Tiêu đề*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Tiêu đề :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.Title}</Text>
              </View>
            </View>
            {/*Chi phí đào tạo*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Chi phí :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.Cost}</Text>
              </View>
            </View>
            {/*Địa diểm đào tạo*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Địa điểm :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.Location}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày tạo :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.customDate(item.CreatedDate)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
              </View>
              <View style={{flex: 3}}>
                {item.Status == 'Y' ? (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.PendingColor},
                    ]}>
                    {item.StatusWF}
                  </Text>
                ) : (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.AcceptColor},
                    ]}>
                    {item.StatusWF}
                  </Text>
                )}
              </View>
            </View>
            {/*nội dung phiếu*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={[AppStyles.Textdefault]}>{item.Note}</Text>
              </View>
            </View>
          </View>
        ) : (
          <View style={{flex: 10, padding: 15}}>
            {/*Người tạo phiếu*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text>
              </View>
            </View>
            {/*Phòng ban*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {item.DepartmentName}
                </Text>
              </View>
            </View>
            {/*Tiêu đề*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Tiêu đề :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.Title}</Text>
              </View>
            </View>
            {/*Chi phí đào tạo*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Chi phí :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.Cost}</Text>
              </View>
            </View>
            {/*Địa diểm đào tạo*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Địa điểm :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.Location}</Text>
              </View>
            </View>
            {/*Chức vụ*/}
            {/* <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <Text style={styles.styletextLable}>Chức vụ :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={styles.styletext}>{item.JobTitleName}</Text>
                </View>
              </View> */}
            {/*Ngày tạo phiếu*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày tạo :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.customDate(item.CreatedDate)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
              </View>
              <View style={{flex: 2}}>
                {item.Status == 'Y' ? (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.PendingColor},
                    ]}>
                    {item.StatusWF}
                  </Text>
                ) : (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.AcceptColor},
                    ]}>
                    {item.StatusWF}
                  </Text>
                )}
              </View>
            </View>
            {/*nội dung phiếu*/}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={[AppStyles.Textdefault]}>{item.Note}</Text>
              </View>
            </View>
          </View>
        )}

        {/*nút xử lý*/}
        <View style={{flex: 1}}>
          {this.state.checkInLogin !== '' ? (
            <TabBarBottom
              //key để quay trở lại danh sách
              onDelete={() => this.Delete()}
              // onUpdate={() => this.Update ()}
              isComplete={this.state.Data.StatusWF == 'Kết thúc'}
              onAttachment={() => this.onAttachment()}
              Attachment={true}
              backListByKey="TrainingResultProcess"
              keyCommentWF={{
                dbName: 'HR',
                scheme: 'HR',
                tableName: 'TrainingResultProcess',
                RecordGuid: this.state.viewId,
                Title: this.state.Data.Title,
                //LoginName:this.state.Data.LoginName
              }}
              // tiêu đề hiển thị trong popup xử lý phiếu
              Title={this.state.Data.Title}
              //kiểm tra quyền xử lý
              Permisstion={this.state.Data.Permisstion}
              //kiểm tra bước đầu quy trình
              checkInLogin={this.state.checkInLogin}
              onSubmitWF={(callback, CommentWF) =>
                this.submitWorkFlow(callback, CommentWF)
              }
            />
          ) : null}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    console.log('callback==' + callback + CommentWF);
    if (callback == 'D') {
      let obj = {
        TrainingResultGuid: this.state.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.state.LoginName,
        Comment: CommentWF,
      };
      API_HR.approveTrainingResults(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity(
              'Có lỗi khi xử lý phiếu',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data.data);
        });
    } else {
      let obj = {
        ResignationFormGuid: this.state.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.state.Loginname,
        Comment: CommentWF,
      };
      API_HR.notapproveResignationForms(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng ngày và giờ
  customDatetime(strDateTime) {
    if (strDateTime != null) {
      var strSplitDateTime = String(strDateTime).split(' ');
      var datetime = new Date(strSplitDateTime[0]);
      // alert(date);
      var HH = datetime.getHours();
      var MM = datetime.getMinutes();
      var dd = datetime.getDate();
      var mm = datetime.getMonth() + 1; //January is 0!
      var yyyy = datetime.getFullYear();
      if (HH < 10) {
        HH = '0' + HH;
      }
      if (MM < 10) {
        MM = '0' + MM;
      }
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      datetime = dd + '-' + mm + '-' + yyyy + ' ' + HH + ':' + MM;
      return datetime.toString();
    } else {
      return '';
    }
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetTrainingResults);
