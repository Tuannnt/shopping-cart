import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {
  Header,
  Divider,
  Button,
  Input,
  SearchBar,
  Icon,
} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions, ActionConst} from 'react-native-router-flux';
import {Back} from '@component';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import {
  Container,
  Content,
  List,
  Left,
  Right,
  Form,
  ListItem,
  Item,
  Thumbnail,
  Body,
  Label,
  Picker,
} from 'native-base';
// import Icon from 'react-native-vector-icons/AntDesign';
import {AppStyles, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RNPickerSelect from 'react-native-picker-select';
import {FlatGrid} from 'react-native-super-grid';
class TimeKeepingbyMonthComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 15;
    this.TotalRow = 0;
    var d = new Date();
    var m = d.getMonth() + 1;
    var y = d.getFullYear();
    this.state = {
      name: '',
      loading: false,
      refreshing: false,
      employeeData: [],
      search: '',
      EmployeeItem: [],
      Month: m,
      Year: y,
      TotalCong: 0,
      TotalTre: 0,
      TotalSom: 0,
      TongNgayNghi: 0,
      TongDaNghiNV: 0,
      PhepConLai: 0,
      listMonth: [
        {
          value: 1,
          label: 'Tháng 1',
        },
        {
          value: 2,
          label: 'Tháng 2',
        },
        {
          value: 3,
          label: 'Tháng 3',
        },
        {
          value: 4,
          label: 'Tháng 4',
        },
        {
          value: 5,
          label: 'Tháng 5',
        },
        {
          value: 6,
          label: 'Tháng 6',
        },
        {
          value: 7,
          label: 'Tháng 7',
        },
        {
          value: 8,
          label: 'Tháng 8',
        },
        {
          value: 9,
          label: 'Tháng 9',
        },
        {
          value: 10,
          label: 'Tháng 10',
        },
        {
          value: 11,
          label: 'Tháng 11',
        },
        {
          value: 12,
          label: 'Tháng 12',
        },
      ],
      listYear: [
        {
          value: 2018,
          label: 'Năm 2018',
        },
        {
          value: 2019,
          label: 'Năm 2019',
        },
        {
          value: 2020,
          label: 'Năm 2020',
        },
        {
          value: 2021,
          label: 'Năm 2021',
        },
        {
          value: 2022,
          label: 'Năm 2022',
        },
        {
          value: 2023,
          label: 'Năm 2023',
        },
        {
          value: 2024,
          label: 'Năm 2024',
        },
        {
          value: 2025,
          label: 'Năm 2025',
        },
        {
          value: 2026,
          label: 'Năm 2026',
        },
        {
          value: 2027,
          label: 'Năm 2027',
        },
        {
          value: 2028,
          label: 'Năm 2028',
        },
        {
          value: 2029,
          label: 'Năm 2029',
        },
        {
          value: 2030,
          label: 'Năm 2030',
        },
        {
          value: 2031,
          label: 'Năm 2031',
        },
        {
          value: 2032,
          label: 'Năm 2032',
        },
        {
          value: 2033,
          label: 'Năm 2033',
        },
      ],

      list: [
        {
          Title: 'Tổng công',
          Icon: 'barschart',
          type: 'antdesign',
          value: 0,
        },
        {
          Title: 'Trễ ',
          Icon: 'clockcircle',
          type: 'antdesign',
          value: 0,
        },
        {
          Title: 'Sớm',
          Icon: 'clockcircleo',
          type: 'antdesign',
          value: 0,
        },
        {
          Title: 'Số phép ',
          Icon: 'book',
          type: 'font-awesome-5',
          value: 0,
        },
        {
          Title: 'Đã nghỉ',
          Icon: 'exclamationcircle',
          type: 'antdesign',
          value: 0,
        },
        {
          Title: 'Còn lại',
          Icon: 'exclamationcircleo',
          type: 'antdesign',
          value: 0,
        },
      ],
    };
  }
  componentDidMount(): void {
    if (this.props.EmployeeItem != '' && this.props.EmployeeItem != null) {
      this.setState({
        loading: false,
        EmployeeItem: this.props.EmployeeItem,
        employeeData: [],
      });
    } else {
      this.setState({loading: false, employeeData: []});
    }

    this.getAllTimeKeepingsPage(this.page);
  }

  keyExtractor = (item, index) => index.toString();

  renderItem = ({item}) => {
    return (
      <ListItem>
        <Body>
          <Text note numberOfLines={1}>
            {'Ngày: ' + this.ConvertDateview(item.DateCheck)}
          </Text>
          <Text note numberOfLines={1}>
            {'Giờ vào: ' + item.CheckIn}
          </Text>
          {this.state.Gender == 'M'
            ? 'Nam'
            : this.state.Gender == 'F'
            ? 'Nữ'
            : null}
          <Text note numberOfLines={1}>
            Trễ: {item.MinutesLate}
          </Text>
        </Body>
        <Body>
          <Text note numberOfLines={1}>
            {'Tổng giờ: ' + this.addPeriod(item.HoursWork)}
          </Text>
          <Text note numberOfLines={1}>
            {'Giờ ra: ' + item.CheckOut}
          </Text>
          <Text note numberOfLines={1}>
            Sớm: {item.MinutesEarly}
          </Text>
        </Body>
      </ListItem>
    );
  };
  _onRefresh = () => {
    this.getAllTimeKeepingsPage(this.page);
  };

  searchFilterFunction = text => {
    this.setState({employeeData: []});
    this.state.search = text;
    this.getAllTimeKeepingsPage(1);
  };

  onPressback() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  MonthList = () => {
    return this.state.listMonth.map((x, i) => {
      return <Picker.Item label={x.text} key={i} value={x.value} />;
    });
  };
  YearList = () => {
    return this.state.listYear.map((x, i) => {
      return <Picker.Item label={x.text} key={i} value={x.value} />;
    });
  };
  render() {
    let arrayholder = [];
    return (
      <Container>
        <TabBar_Title
          title={'Nhật ký chấm công'}
          callBack={() => this.onPressback()}
        />
        <Content style={{marginStart: 5, marginEnd: 5}}>
          <View thumbnail style={{marginTop: 10}}>
            <Body style={{flex: 1, alignItems: 'center'}}>
              <Thumbnail
                source={API_HR.GetPicApplyLeaves(
                  this.state.EmployeeItem.EmployeeGuid,
                )}
              />
              <Text>{this.state.EmployeeItem.FullName}</Text>
              <Text>Mã: {this.state.EmployeeItem.EmployeeID}</Text>
            </Body>
          </View>
          <Divider style={{marginBottom: 0, marginTop: 10}} />
          <View style={styles.containerContent}>
            <Content>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <RNPickerSelect
                    doneText="Xong"
                    onValueChange={value => this.onValueChangeMonth(value)}
                    items={this.state.listMonth}
                    value={this.state.Month}
                    placeholder={{}}
                  />
                </View>
                <View style={{flex: 1}}>
                  <RNPickerSelect
                    doneText="Xong"
                    onValueChange={value => this.onValueChangeYear(value)}
                    items={this.state.listYear}
                    value={this.state.Year}
                    placeholder={{}}
                  />
                </View>
              </View>
            </Content>
          </View>

          {/*<View style={styles.containerContent}>*/}
          {/*    <Content style={{marginStart:10}}>*/}
          {/*        <View  style={{flexDirection:'row'}}>*/}
          {/*            <View style={{flex:1}}>*/}
          {/*                <Text style={{fontWeight:'bold'}}>Tổng công: {this.state.TotalCong}</Text>*/}
          {/*            </View>*/}
          {/*            <View style={{flex:1}}>*/}
          {/*                <Text style={{fontWeight:'bold'}}>Trễ: {this.state.TotalTre}</Text>*/}
          {/*            </View>*/}
          {/*            <View style={{flex:1}}>*/}
          {/*                <Text style={{fontWeight:'bold'}}>Sớm: {this.state.TotalSom}</Text>*/}
          {/*            </View>*/}
          {/*        </View>*/}
          {/*    </Content>*/}
          {/*    </View>*/}
          {/*<View style={styles.containerContent}>*/}
          {/*    <Content style={{marginStart:10}}>*/}
          {/*        <View  style={{flexDirection:'row'}}>*/}
          {/*            <View style={{flex:1}}>*/}
          {/*                <Text style={{fontWeight:'bold'}}>Số phép: {this.state.TongNgayNghi}</Text>*/}
          {/*            </View>*/}
          {/*            <View style={{flex:1}}>*/}
          {/*                <Text style={{fontWeight:'bold'}}>Đã nghỉ: {this.state.TongDaNghiNV}</Text>*/}
          {/*            </View>*/}
          {/*            <View style={{flex:1}}>*/}
          {/*                <Text style={{fontWeight:'bold'}}>Còn lại: {this.state.PhepConLai}</Text>*/}
          {/*            </View>*/}
          {/*        </View>*/}
          {/*    </Content>*/}
          {/*</View>*/}
          <View style={{}}>
            <FlatGrid
              itemDimension={100}
              items={this.state.list}
              style={styles.gridView}
              renderItem={({item, index}) => (
                <View
                  style={{
                    borderRadius: 10,
                    backgroundColor: '#FFFFFF',
                    height: 70,
                    borderColor: AppColors.Maincolor,
                    borderWidth: 1,
                  }}>
                  <View style={[styles.itemContainerdetail]}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <Icon
                          name={item.Icon}
                          type={item.type}
                          size={20}
                          color={AppColors.Maincolor}
                        />
                      </View>
                      <View style={{flex: 1}}>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            fontSize: 17,
                            textAlign: 'right',
                            paddingRight: 5,
                          }}>
                          {item.value}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <Text style={AppStyles.Textdefault}>{item.Title}</Text>
                    </View>
                  </View>
                </View>
              )}
            />
          </View>

          {this.state.employeeData ? (
            <View>
              <FlatList
                keyExtractor={this.keyExtractor}
                data={this.state.employeeData}
                refreshing={this.state.loading}
                renderItem={this.renderItem}
                onEndReached={this.handleLoadMore}
                onRefresh={this._onRefresh}
                ListFooterComponent={this.renderFooter.bind(this)}
                onEndReachedThreshold={0.4}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.loading}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['red', 'green', 'blue']}
                  />
                }
              />
            </View>
          ) : null}
        </Content>
      </Container>
    );
  }
  clickBack() {
    Actions.app();
  }

  onValueChangeMonth = Month => {
    this.setState({employeeData: []});
    this.state.Month = Month;
    this.state.Year = this.state.Year;
    this.page = 1;
    this.getAllTimeKeepingsPage(this.page);
  };
  onValueChangeYear = Year => {
    this.setState({employeeData: []});
    this.state.Month = this.state.Month;
    this.state.Year = Year;
    this.page = 1;
    this.getAllTimeKeepingsPage(this.page);
  };
  editItem(item) {
    // Actions.empviewitem({EmployeeGuid: item.EmployeeGuid});
  }
  addItem() {
    Actions.additem({EmployeeGuid: ''});
  }
  getAllTimeKeepingsPage = NumberPage => {
    let obj = {
      NumberPage: NumberPage,
      Length: this.Length,
      txtSearch: this.state.search,
      LoginName: this.props.EmployeeItem.EmployeeID,
      Month: this.state.Month,
      Year: this.state.Year,
    };

    this.setState({loading: true});

    API_HR.getAllTimeKeepingsPage(obj)
      .then(response => {
        console.log(
          '==============ketqua6666: ' +
            JSON.stringify(
              JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
            ),
        );
        let listData = this.state.employeeData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
          ),
        );
        let data = listData.concat(data1);
        this.arrayholder = data;
        let TotalRow = JSON.parse(
          JSON.parse(JSON.stringify(response.data)).data,
        );
        this.TotalRow = TotalRow.Count;
        let TotalCong = 0;
        let TotalTre = 0;
        let TotalSom = 0;
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            if (data[i].WorkedDay == 1) {
              TotalCong += 1;
            }
            TotalTre +=
              data[i].MinutesLate > 0 && data[i].MinutesLate != null
                ? parseFloat(data[i].MinutesLate)
                : 0;
            TotalSom +=
              data[i].MinutesEarly > 0 && data[i].MinutesEarly != null
                ? parseFloat(data[i].MinutesEarly)
                : 0;
          }
        }
        this.setState({
          employeeData: data,
          loading: false,
          TotalCong: TotalCong,
          TotalTre: TotalTre,
          TotalSom: TotalSom,
        });
        var objLeave = {
          EmployeeGuid: this.state.EmployeeItem.EmployeeGuid,
        };
        API_HR.GetTotalLeaveInYear(objLeave)
          .then(response => {
            let rs = JSON.parse(JSON.parse(JSON.stringify(response.data)).data);
            this.setState({
              TongNgayNghi: rs.TongNgayNghi,
              TongDaNghiNV: rs.TongDaNghiNV,
              PhepConLai: rs.PhepConLai,
              list: [
                {
                  Title: 'Tổng công',
                  Icon: 'barschart',
                  type: 'antdesign',
                  value: this.state.TotalCong,
                },
                {
                  Title: 'Trễ ',
                  Icon: 'clockcircle',
                  type: 'antdesign',
                  value: this.state.TotalTre,
                },
                {
                  Title: 'Sớm',
                  Icon: 'clockcircleo',
                  type: 'antdesign',
                  value: this.state.TotalSom,
                },
                {
                  Title: 'Số phép ',
                  Icon: 'book',
                  type: 'font-awesome-5',
                  value: rs.TongNgayNghi,
                },
                {
                  Title: 'Đã nghỉ',
                  Icon: 'exclamationcircle',
                  type: 'antdesign',
                  value: rs.TongDaNghiNV,
                },
                {
                  Title: 'Còn lại',
                  Icon: 'exclamationcircleo',
                  type: 'antdesign',
                  value: rs.PhepConLai,
                },
              ],
            });
          })
          .catch(error => {
            console.log(error.data.data);
          });
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  handleLoadMore = () => {
    if (!this.state.loading && this.TotalRow > this.state.employeeData.length) {
      this.page = this.page + 1; // increase page by 1
      this.getAllTimeKeepingsPage(this.page); // method for API call
    }
  };
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
  ConvertDateview = data => {
    if (data !== null && data !== undefined && data !== '') {
      return (
        data.toString().substring(6, 8) +
        '/' +
        data.toString().substring(4, 6) +
        '/' +
        data.toString().substring(0, 4)
      );
    } else {
      return "';";
    }
  };
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;

      nStr += '';
      if (nStr.indexOf(',') >= 0) {
        var x = nStr.split(',');
      } else {
        var x = nStr.split('.');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}
const colors = {
  loginGreen: '#347F16',
};
const styles = StyleSheet.create({
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 10,
    backgroundColor: 'white',
  },
  loginContainer: {
    marginTop: 10,
    marginEnd: 10,
    alignItems: 'flex-end',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
  itemContainerdetail: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 17,
    flexDirection: 'column',
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TimeKeepingbyMonthComponent);
