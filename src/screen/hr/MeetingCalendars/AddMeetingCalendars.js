import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Keyboard,
  StyleSheet,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {Button, Divider, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../../utils';
import _ from 'lodash';
import controller from './controller';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {Container} from 'native-base';
import {AppStyles, AppColors} from '@theme';
import {API_MeetingCalendars} from '@network';
import DataCombobox from './DataCombobox';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';
import Toast from 'react-native-simple-toast';
import {
  Combobox,
  TabBar_Title,
  RequiredText,
  OpenPhotoLibrary,
  ComboboxV2,
} from '@Component';
const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
class AddResignationForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Title: '',
      ReasonLeaveWorkId: null,
      StartDate: FuncCommon.ConDate(new Date(), 0),
      OtherReason: '',
      Description: '',
      name: '',
      Attachment: [],
      ListEmp: [],
      ListAsset: [],
      refreshing: false,
      ListStatus: [],
      bankDataItem: null,
      search: '',
      selected2: undefined,
      RoomName: '',
      StartTime: new Date(),
      EndTime: new Date(),
      modalPickerTimeVisible: false,
    };
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }

  onChangeTitle(value) {
    this.setState({
      Title: value,
    });
    if (this.props.callBack) {
      this.props.callBack('Title', value);
    }
  }
  onClickBack() {
    Actions.pop();
  }
  Add = () => {
    const {Attachment} = this.state;
    if (!this.state.RoomId) {
      Toast.showWithGravity(
        'Bạn chưa chọn phòng họp',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.Title) {
      Toast.showWithGravity('Bạn chưa nhập tiêu đề', Toast.SHORT, Toast.CENTER);
      return;
    }
    if (this.state.EndTime.getTime() <= this.state.StartTime.getTime()) {
      Toast.showWithGravity(
        'Thời gian kết thúc cần lớn hơn thời gian bắt đầu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let RoomGuid = this.state.ListStatus.find(
      room => room.value === this.state.RoomId,
    ).guid;
    let _obj = {
      Title: this.state.Title,
      RoomGuid,
      StartTime: FuncCommon.ConDate(this.state.StartTime, 11),
      EndTime: FuncCommon.ConDate(this.state.EndTime, 11),
      Content: this.state.Content,
      FileAttachments: [],
      IsLocked: false,
      Status: null,
      CalendarContent: '',
      IsActive: true,
      IsDeleted: false,
    };
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    let _data = new FormData();
    _data.append('insert', JSON.stringify(_obj));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    _data.append('DeleteAttach', undefined);
    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    if (this.props.itemData) {
      this.Update(_obj);
      return;
    }
    API_MeetingCalendars.Insert(_data)
      .then(res => {
        let _res = res.data;
        if (_res.errorCode !== 200) {
          Toast.showWithGravity(_res.message, Toast.SHORT, Toast.CENTER);
          return;
        } else {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  };
  Submit() {
    FuncCommon.Data_Offline(async d => {
      if (!d) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      } else {
        this.Add();
      }
    });
  }
  Update = obj => {
    let _obj = {
      ...this.props.itemData,
      ...obj,
    };
    let _data = new FormData();
    _data.append('update', JSON.stringify(_obj));
    _data.append('lstTitlefile', JSON.stringify([]));
    _data.append('DeleteAttach_orders', JSON.stringify([]));
    API_MeetingCalendars.Update(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Cập nhật thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };

  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  componentDidMount() {
    const {itemData} = this.props;
    if (itemData) {
      this.setState({
        Title: itemData.Title,
        StartTime: new Date(FuncCommon.ConDate(itemData.StartTime, 14)),
        EndTime: new Date(FuncCommon.ConDate(itemData.EndTime, 14)),
        RoomId: itemData.RoomId,
        RoomName: itemData.RoomName,
        Description: itemData.Description,
        Content: itemData.Content,
        FullName: itemData.EmployeeName,
        DepartmentName: itemData.DepartmentName,
        JobTitleNameEmployee: itemData.JobTitleName,
      });
    } else {
      this.setState({
        StartDate: FuncCommon.ConDate(new Date(), 0),
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        JobTitleNameEmployee:
          global.__appSIGNALR.SIGNALR_object.USER.JobTitleName,
      });
    }
    this.getRoomId();
  }
  getRoomId = () => {
    controller.getRoomId(rs => {
      this.setState({ListStatus: rs});
    });
  };
  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(DataCombobox.options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion
  render() {
    const {isEdit, ResignationFormId} = this.state;
    const IS_CB = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'CB';
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <View style={[AppStyles.container]}>
          <Container>
            {this.props.itemData ? (
              <TabBar_Title
                title={'Chỉnh sửa lịch họp'}
                callBack={() => Actions.pop()}
              />
            ) : (
              <TabBar_Title
                title={'Thêm mới lịch họp'}
                callBack={() => Actions.pop()}
              />
            )}
            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người đăng ký</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault} required>
                      Tiêu đề <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    placeholder="Nhập tiêu đề..."
                    autoCapitalize="none"
                    value={this.state.Title}
                    onChangeText={value => this.onChangeState('Title', value)}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Phòng họp <RequiredText />
                    </Text>
                  </View>
                  <TouchableOpacity onPress={() => this.onActionComboboxType()}>
                    <Text style={[AppStyles.FormInput, {color: 'black'}]}>
                      {this.state.RoomName
                        ? this.state.RoomName
                        : 'Chọn phòng họp...'}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Thời gian bắt đầu <RequiredText />
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <TouchableOpacity
                        onPress={() => {
                          this.handlePickTimeOpen('StartTime');
                        }}
                        style={[
                          AppStyles.table_td_custom,
                          {padding: 12, borderWidth: 1},
                        ]}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'center'},
                          ]}>
                          {(this.state.StartTime &&
                            FuncCommon.ConDate(this.state.StartTime, 13)) ||
                            ''}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Thời gian kết thúc <RequiredText />
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <TouchableOpacity
                        onPress={() => {
                          this.handlePickTimeOpen('EndTime');
                        }}
                        style={[
                          AppStyles.table_td_custom,
                          {padding: 12, borderWidth: 1},
                        ]}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'center'},
                          ]}>
                          {(this.state.EndTime &&
                            FuncCommon.ConDate(this.state.EndTime, 13)) ||
                            ''}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>

                <View style={{padding: 10, paddingBottom: 10}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Nội dung</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    multiline
                    numberOfLines={4}
                    value={this.state.Content}
                    onChangeText={Content =>
                      this.onChangeState('Content', Content)
                    }
                  />
                </View>
                {/* attachment */}
                {!this.props.itemData && (
                  <View
                    style={{
                      flexDirection: 'column',
                      padding: 10,
                      marginBottom: 15,
                    }}>
                    <View style={{flexDirection: 'row', marginBottom: 5}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                        <Text style={{color: 'red'}}> </Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          flexDirection: 'row',
                        }}
                        onPress={() => this.openAttachment()}>
                        <Icon
                          name="attachment"
                          type="entypo"
                          size={15}
                          color={AppColors.ColorAdd}
                        />
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {color: AppColors.ColorAdd},
                          ]}>
                          {' '}
                          Chọn file
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <Divider />
                    {this.state.Attachment.length > 0
                      ? this.state.Attachment.map((para, i) => (
                          <View
                            key={i}
                            style={[{flexDirection: 'row', marginBottom: 3}]}>
                            <View
                              style={[
                                AppStyles.containerCentered,
                                {padding: 10},
                              ]}>
                              <Image
                                style={{
                                  width: 30,
                                  height: 30,
                                  borderRadius: 10,
                                }}
                                source={{
                                  uri: this.FileAttackments.renderImage(
                                    para.FileName,
                                  ),
                                }}
                              />
                            </View>
                            <TouchableOpacity
                              key={i}
                              style={{flex: 1, justifyContent: 'center'}}>
                              <Text style={AppStyles.Textdefault}>
                                {para.FileName}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[
                                AppStyles.containerCentered,
                                {padding: 10},
                              ]}
                              onPress={() => this.removeAttactment(para)}>
                              <Icon
                                name="close"
                                type="antdesign"
                                size={20}
                                color={AppColors.ColorDelete}
                              />
                            </TouchableOpacity>
                          </View>
                        ))
                      : null}
                  </View>
                )}
              </View>
            </ScrollView>

            <View>
              <Button
                buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
                title="Lưu"
                onPress={() => this.Submit()}
              />
            </View>

            <View>
              <OpenPhotoLibrary
                callback={this.callbackLibarary}
                openLibrary={this.openLibrary}
              />
              <ComboboxV2
                callback={this.ChoiceAtt}
                data={DataCombobox.ListComboboxAtt}
                eOpen={this.openCombobox_Att}
              />
            </View>

            {this.state.ListStatus.length > 0 ? (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeType}
                data={this.state.ListStatus}
                nameMenu={'Chọn trạng thái'}
                eOpen={this.openComboboxType}
                position={'bottom'}
                value={undefined}
              />
            ) : null}
            {this.state.modalPickerTimeVisible && (
              <DateTimePickerModal
                locale="vi-VN"
                cancelTextIOS="Huỷ"
                confirmTextIOS="Chọn"
                headerTextIOS="Chọn thời gian"
                isVisible={this.state.modalPickerTimeVisible}
                mode="datetime"
                onConfirm={this.handleConfirmTime}
                onCancel={this.hideDatePicker}
              />
            )}
          </Container>
        </View>
      </KeyboardAvoidingView>
    );
  }
  handlePickTimeOpen = type => {
    this.setState({
      typeTime: type,
      modalPickerTimeVisible: true,
    });
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      typeTime: null,
    });
  };
  handleConfirmTime = date => {
    const {typeTime} = this.state;
    this.setState({[typeTime]: date, typeTime: null});
    this.hideDatePicker();
  };
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.setState({
      RoomId: rs.value,
      RoomName: rs.text,
    });
  };
  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }

  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddResignationForms);
