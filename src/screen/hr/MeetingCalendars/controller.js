import { API_ApplyOverTimes, API_MeetingCalendars } from '@network';
import { ErrorHandler } from '@error';

export default {
  removeAttactment(data, list, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },
  getRoomId(callback) {
    API_MeetingCalendars.GetCarlenderCar().then((res) => {
      let data = []
      data = JSON.parse(res.data.data).map(x => ({
        value: x.RoomId,
        text: x.RoomName,
        guid: x.RoomGuid
      }))
      callback(data)
    }).catch(err => {
      console.log(err)
    })
  }
}
