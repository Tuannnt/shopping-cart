import React, {Component} from 'react';
import {
  Keyboard,
  TouchableHighlight,
  Text,
  View,
  ScrollView,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_MeetingCalendars, API} from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import {Actions} from 'react-native-router-flux';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, AppColors} from '@theme';
import Toast from 'react-native-simple-toast';
import {WebView} from 'react-native-webview';
import {FuncCommon} from '../../../utils';
import HTML from 'react-native-render-html';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class GetResignationForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      selected2: [],
      //StartTime: null,
      viewId: '',
    };
  }
  setPickUpLocation(PickUpLocation) {
    this.state.Data.PickUpLocation = PickUpLocation;
    this.setState({Data: this.state.Data});
  }

  componentDidMount = () => {
    if (this.props.RecordGuid !== undefined) {
      this.state.viewId = this.props.RecordGuid;
    } else {
      this.state.viewId = this.props.viewId;
    }
    this.getItem();
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      if (this.props.RecordGuid !== undefined) {
        this.state.viewId = this.props.RecordGuid;
      } else {
        this.state.viewId = this.props.viewId;
      }
      this.getItem();
    }
  }
  setDescription(Description) {
    this.state.Data.Description = Description;
    this.setState({Data: this.state.Data});
  }
  setTitle(Title) {
    this.state.Data.Title = Title;
    this.setState({Data: this.state.Data});
  }
  setStartDate(StartDate) {
    this.state.Data.StartDate = StartDate;
    this.setState({Data: this.state.Data});
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  setOtherReason(OtherReason) {
    this.state.Data.OtherReason = OtherReason;
    this.setState({Data: this.state.Data});
  }
  setDescription(Description) {
    this.state.Data.Description = Description;
    this.setState({Data: this.state.Data});
  }
  // show hide tab edit, detail
  setEventEditRoutings = () => {
    if (this.state.ViewEditRoutings === false) {
    }
    this.setViewOpen('ViewEditRoutings');
  };
  setEventDetailRoutings = () => {
    if (this.state.ViewDetailRoutings === false) {
    }
    this.setViewOpen('ViewDetailRoutings');
  };

  onAttachment() {
    Actions.attachmentComponent({
      ModuleId: '5',
      RecordGuid: this.state.viewId,
    });
  }
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_MeetingCalendars.Delete(id.Id)
      .then(response => {
        console.log(
          '==============ketquaXoa-errorCode: ' + response.data.Error,
        );
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            ResignationFormGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity(
            response.data.message,
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  };
  handleChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  getItem() {
    API_MeetingCalendars.GetItem(this.props.RecordGuid)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
        });
        API.getProfile().then(rs => {
          console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
          this.setState({LoginName: rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log(error.data);
        console.log('=======' + error.data);
      });
  }
  CustomViewDetailRoutings = () => {
    const item = this.state.Data;
    return (
      <View>
        <View style={{padding: 20}}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Tiêu đề:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>{item.Title}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Người đăng ký:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text>
            </View>
          </View>
          {/*Phòng ban*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>{item.DepartmentName}</Text>
            </View>
          </View>
          {/*Ngày tạo phiếu*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {FuncCommon.ConDate(item.StartTime, 1, 'iso')}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Thời gian kết thúc :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {FuncCommon.ConDate(item.EndTime, 1, 'iso')}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Phòng họp :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>{item.RoomName}</Text>
            </View>
          </View>
          {/*nội dung phiếu*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Nội dung công việc :</Text>
            </View>
            <View style={{flex: 3}}>
              <ScrollView>
                <TouchableHighlight>
                  <HTML
                    source={{html: item.Content}}
                    contentWidth={DRIVER.width}
                  />
                </TouchableHighlight>
              </ScrollView>
              {/* <WebView
                style={{height: 300}}
                originWhitelist={['*']}
                source={{
                  html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>${item.Content ||
                    '<div></div>'}</body></html>`,
                }}
              /> */}
            </View>
          </View>
        </View>
      </View>
    );
  };
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  showActionSheet = () => this.actionSheet.show();
  getActionSheetRef = ref => (this.actionSheet = ref);
  CustomView = item => (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TabBar_Title
        title={'Chi tiết lịch họp'}
        callBack={() => this.callBackList()}
      />
      {/*hiển thị nội dung chính*/}
      <ScrollView>
        {this.CustomViewDetailRoutings()}
        {/*Người tạo phiếu*/}
      </ScrollView>
      {/*nút xử lý*/}
      <View style={{maxHeight: 50}}>
        <TabBarBottom
          //key để quay trở lại danh sách
          onDelete={() => this.Delete()}
          isDraff={true}
          callbackOpenUpdate={this.callbackOpenUpdate}
          backListByKey="ResignationForms"
          onAttachment={() => this.onAttachment()}
          // tiêu đề hiển thị trong popup xử lý phiếu
          Title={this.state.Data.Title}
          //kiểm tra quyền xử lý
          Permisstion={1}
          //kiểm tra bước đầu quy trình
          checkInLogin={1}
        />
      </View>
    </View>
  );
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    console.log('callback==' + callback + CommentWF);
    if (callback == 'D') {
      let obj = {
        RowGuid: this.state.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.state.LoginName,
        Comment: CommentWF,
      };
      API_MeetingCalendars.approveResignationForms(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            Actions.ResignationForms();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data);
        });
    } else {
      let obj = {
        RowGuid: this.state.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.state.Loginname,
        Comment: CommentWF,
      };
      API_MeetingCalendars.notapproveResignationForms(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            Actions.ResignationForms();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data);
        });
    }
  }
  callbackOpenUpdate = () => {
    Actions.AddMeetingCalendars({itemData: this.state.Data});
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng ngày và giờ
  customDatetime(strDateTime) {
    if (strDateTime != null) {
      var strSplitDateTime = String(strDateTime).split(' ');
      var datetime = new Date(strSplitDateTime[0]);
      // alert(date);
      var HH = datetime.getHours();
      var MM = datetime.getMinutes();
      var dd = datetime.getDate();
      var mm = datetime.getMonth() + 1; //January is 0!
      var yyyy = datetime.getFullYear();
      if (HH < 10) {
        HH = '0' + HH;
      }
      if (MM < 10) {
        MM = '0' + MM;
      }
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      datetime = dd + '-' + mm + '-' + yyyy + ' ' + HH + ':' + MM;
      return datetime.toString();
    } else {
      return '';
    }
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetResignationForms);
