import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Keyboard,
  StyleSheet,
  Alert,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Button, Divider, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import TabBar_Title from '../../component/TabBar_Title';
import {FuncCommon} from '../../../utils';
import DatePicker from 'react-native-datepicker';
import _ from 'lodash';
import controller from './controller';
import RNPickerSelect from 'react-native-picker-select';
import {Container} from 'native-base';
import {AppStyles, AppColors} from '@theme';
import {API_HR} from '@network';
import DataCombobox from './DataCombobox';
import ComboboxV2 from '../../component/ComboboxV2';
import RequiredText from '../../component/RequiredText';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
class AddResignationForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Title: '',
      ReasonLeaveWorkId: null,
      StartDate: FuncCommon.ConDate(new Date(), 0),
      OtherReason: '',
      Description: '',
      name: '',
      Attachment: [],
      refreshing: false,
      ListReason: [
        {
          label: 'Chọn lý do',
          value: null,
        },
      ],
      bankDataItem: null,
      search: '',
      selected2: undefined,
    };
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }

  onChangeTitle(value) {
    this.setState({
      Title: value,
    });
    if (this.props.callBack) {
      this.props.callBack('Title', value);
    }
  }
  onClickBack() {
    Actions.pop();
  }
  Submit() {
    const {Attachment} = this.state;
    let _obj = {
      Title: this.state.Title,
      OtherReason: this.state.OtherReason,
      StartDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.StartDate, 99),
        2,
      ),
      Description: this.state.Description,
      ReasonLeaveWorkId: this.state.ReasonLeaveWorkId,
      ResignationFormId: this.state.ResignationFormId,
      FileAttachments: [],
    };
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    let auto = {Value: this.state.ResignationFormId, IsEdit: this.state.isEdit};
    let _data = new FormData();
    _data.append('ResignationForms', JSON.stringify(_obj));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteAttach', JSON.stringify(undefined));
    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    if (!_obj.ResignationFormId) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập mã phiếu',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (!_obj.Title) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập tiêu đề',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (!_obj.ReasonLeaveWorkId) {
      Alert.alert(
        'Thông báo ',
        'Bạn chưa chọn lý do',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (!_obj.ResignationFormId) {
      Alert.alert(
        'Thông báo ',
        'Bạn chưa điền mã phiếu',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (!_obj.StartDate) {
      Alert.alert(
        'Thông báo ',
        'Bạn chưa chọn ngày nghỉ',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (this.props.itemData) {
      this.Update();
      return;
    }
    API_HR.InsertResignationForms(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          console.log(_rs);
          Alert.alert(
            'Thông báo',
            'Thêm mới không thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Thông báo',
            'Thêm mới thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }
  Update = () => {
    let _obj = {
      ...this.props.itemData,
      Title: this.state.Title,
      OtherReason: this.state.OtherReason,
      StartDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.StartDate, 99),
        2,
      ),
      Description: this.state.Description,
      ReasonLeaveWorkId: this.state.ReasonLeaveWorkId,
      ResignationFormId: this.state.ResignationFormId,
      Permisstion: undefined,
    };
    let auto = {IsEdit: true};
    let _data = new FormData();
    _data.append('ResignationForms', JSON.stringify(_obj));
    _data.append('lstTitlefile', JSON.stringify([]));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteAttach_orders', JSON.stringify(undefined));
    API_HR.ResignationForms_Update(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Alert.alert(
            'Thông báo',
            'Cập nhật không thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Thông báo',
            'Cập nhật thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_RF',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      this.setState({isEdit: data.IsEdit, ResignationFormId: data.Value});
    });
  };
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  componentDidMount() {
    const {itemData} = this.props;
    this.getReason();
    if (itemData) {
      this.setState({
        Title: itemData.Title,
        OtherReason: itemData.OtherReason,
        StartDate: FuncCommon.ConDate(new Date(itemData.StartDate), 0),
        Description: itemData.Description,
        ReasonLeaveWorkId: itemData.ReasonLeaveWorkId,
        ResignationFormId: itemData.ResignationFormId,
        isEdit: false,
        FullName: itemData.EmployeeName,
        DepartmentName: itemData.DepartmentName,
        JobTitleNameEmployee:
          global.__appSIGNALR.SIGNALR_object.USER.JobTitleName,
      });
    } else {
      this.getNumberAuto();
      this.setState({
        StartDate: FuncCommon.ConDate(new Date(), 0),
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        JobTitleNameEmployee:
          global.__appSIGNALR.SIGNALR_object.USER.JobTitleName,
      });
    }
  }

  getReason = () => {
    let _obj = {};
    API_HR.GetReason(_obj)
      .then(rs => {
        var _listReason = rs.data;
        let _reListReason = [
          {
            label: 'Chọn lý do',
            value: null,
          },
        ];
        for (var i = 0; i < _listReason.length; i++) {
          _reListReason.push({
            label: _listReason[i].ReasonName,
            value: _listReason[i].ReasonLeaveWorkId,
          });
        }
        this.setState({
          ListReason: _reListReason,
        });
      })
      .catch(error => {
        console.log('Error when call API GetCar Mobile.');
        console.log(error);
      });
  };
  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(DataCombobox.options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion
  render() {
    const {isEdit, ResignationFormId} = this.state;
    const IS_CB = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'CB';
    return (
      <Container>
        <View style={{flex: 1}}>
          {this.props.itemData ? (
            <TabBar_Title
              title={'Sửa phiếu nghỉ việc'}
              callBack={() => {
                this.onPressBack();
              }}
            />
          ) : (
            <TabBar_Title
              title={'Thêm phiếu nghỉ việc'}
              callBack={() => {
                this.onPressBack();
              }}
            />
          )}
          <ScrollView>
            <View style={styles.containerContent}>
              {IS_CB && (
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>
              )}

              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>
                    Mã nghỉ việc <RequiredText />{' '}
                  </Text>
                </View>
                {isEdit && (
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    value={ResignationFormId}
                    autoCapitalize="none"
                    onChangeText={Title =>
                      this.onChangeState('ResignationFormId', Title)
                    }
                  />
                )}
                {!isEdit && (
                  <View style={[AppStyles.FormInput]}>
                    <Text
                      style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                      {ResignationFormId}
                    </Text>
                  </View>
                )}
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Người đăng ký</Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={this.state.FullName}
                  editable={false}
                />
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Chức vụ</Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={this.state.JobTitleNameEmployee}
                  editable={false}
                />
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault} required>
                    Tiêu đề <RequiredText />
                  </Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={this.state.Title}
                  onChangeText={value => this.onChangeState('Title', value)}
                />
              </View>

              <View
                style={{
                  padding: 10,
                  paddingBottom: 0,
                  flexDirection: 'column',
                }}>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={AppStyles.Labeldefault}>
                    Ngày nghỉ <RequiredText />
                  </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{width: '100%'}}>
                    <DatePicker
                      locale="vie"
                      locale={'vi'}
                      style={{width: '100%'}}
                      date={this.state.StartDate}
                      mode="date"
                      androidMode="spinner"
                      placeholder="Chọn ngày"
                      format="DD/MM/YYYY"
                      confirmBtnText="Xong"
                      cancelBtnText="Huỷ"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: 4,
                          marginLeft: 0,
                        },
                        dateInput: {
                          marginRight: 36,
                        },
                      }}
                      onDateChange={date =>
                        this.onChangeState('StartDate', date)
                      }
                    />
                  </View>
                </View>
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <Text style={AppStyles.Labeldefault}>
                  Lý do <RequiredText />
                </Text>
                <View
                  style={[
                    {...AppStyles.FormInputPicker, justifyContent: 'center'},
                  ]}>
                  <RNPickerSelect
                    doneText="Xong"
                    onValueChange={value =>
                      this.onChangeState('ReasonLeaveWorkId', value)
                    }
                    items={this.state.ListReason}
                    placeholder={{}}
                    value={this.state.ReasonLeaveWorkId}
                    style={{
                      inputIOS: {
                        padding: 10,
                        color: 'black',
                        fontSize: 14,
                        paddingRight: 15, // to ensure the text is never behind the icon
                      },
                      inputAndroid: {
                        padding: 10,
                        color: 'black',
                        fontSize: 14,
                        paddingRight: 15, // to ensure the text is never behind the icon
                      },
                    }}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Lý do khác</Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={this.state.OtherReason}
                  onChangeText={OtherReason =>
                    this.onChangeState('OtherReason', OtherReason)
                  }
                />
              </View>
              <View style={{padding: 10, paddingBottom: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Nội dung</Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  multiline
                  numberOfLines={4}
                  value={this.state.Description}
                  onChangeText={Description =>
                    this.onChangeState('Description', Description)
                  }
                />
              </View>
              {/* attachment */}
              {!this.props.itemData && (
                <View
                  style={{
                    flexDirection: 'column',
                    padding: 10,
                    marginBottom: 15,
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                      <Text style={{color: 'red'}}> </Text>
                    </View>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        flexDirection: 'row',
                      }}
                      onPress={() => this.openAttachment()}>
                      <Icon
                        name="attachment"
                        type="entypo"
                        size={15}
                        color={AppColors.ColorAdd}
                      />
                      <Text
                        style={[
                          AppStyles.Labeldefault,
                          {color: AppColors.ColorAdd},
                        ]}>
                        {' '}
                        Chọn file
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <Divider style={{marginBottom: 10}} />
                  {this.state.Attachment.length > 0
                    ? this.state.Attachment.map((para, i) => (
                        <View
                          key={i}
                          style={[
                            styles.StyleViewInput,
                            {flexDirection: 'row', marginBottom: 3},
                          ]}>
                          <View
                            style={[
                              AppStyles.containerCentered,
                              {padding: 10},
                            ]}>
                            <Image
                              style={{
                                width: 30,
                                height: 30,
                                borderRadius: 10,
                              }}
                              source={{
                                uri: this.FileAttackments.renderImage(
                                  para.FileName,
                                ),
                              }}
                            />
                          </View>
                          <TouchableOpacity
                            key={i}
                            style={{flex: 1, justifyContent: 'center'}}>
                            <Text style={AppStyles.Textdefault}>
                              {para.FileName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.containerCentered, {padding: 10}]}
                            onPress={() => this.removeAttactment(para)}>
                            <Icon
                              name="close"
                              type="antdesign"
                              size={20}
                              color={AppColors.ColorDelete}
                            />
                          </TouchableOpacity>
                        </View>
                      ))
                    : null}
                </View>
              )}
            </View>
          </ScrollView>
        </View>
        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
        <View>
          <OpenPhotoLibrary
            callback={this.callbackLibarary}
            openLibrary={this.openLibrary}
          />
          <ComboboxV2
            callback={this.ChoiceAtt}
            data={DataCombobox.ListComboboxAtt}
            eOpen={this.openCombobox_Att}
          />
        </View>
      </Container>
    );
  }

  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddResignationForms);
