import React, {Component} from 'react';
import {Text, View, ScrollView, ToastAndroid} from 'react-native';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_HR, API} from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import {Actions} from 'react-native-router-flux';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, AppColors} from '@theme';

class GetResignationForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      selected2: [],
      viewId: '',
    };
  }
  setPickUpLocation(PickUpLocation) {
    this.state.Data.PickUpLocation = PickUpLocation;
    this.setState({Data: this.state.Data});
  }

  componentDidMount = () => {
    if (this.props.RecordGuid !== undefined) {
      this.state.viewId = this.props.RecordGuid;
    } else {
      this.state.viewId = this.props.viewId;
    }
    this.getItem();
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      if (this.props.RecordGuid !== undefined) {
        this.state.viewId = this.props.RecordGuid;
      } else {
        this.state.viewId = this.props.viewId;
      }
      this.getItem();
    }
  }
  onAttachment = () => {
    Actions.attachmentComponent({
      ModuleId: '5',
      RecordGuid: this.state.viewId,
      notEdit: this.state.Data.StatusWF !== 'Nháp' ? true : false,
    });
  };
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_HR.deleteResignationForms(id.Id)
      .then(response => {
        console.log(
          '==============ketquaXoa-errorCode: ' + response.data.Error,
        );
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            ResignationFormGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  handleChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  getItem() {
    var obj = {
      ResignationFormGuid: this.state.viewId,
    };
    API_HR.ResignationForms_Items(obj)
      .then(res => {
        let StartDate1 = JSON.parse(res.data.data).StartDate;
        let StartDate11 = new Date(StartDate1);
        this.setState({
          Data: JSON.parse(res.data.data),
          StartDate: StartDate11,
          selected2: JSON.parse(res.data.data).ReasonLeaveWorkId,
          //Quantity : JSON.parse (res.data.data).Quantity.toString()
        });
        let checkin = {
          ResignationFormGuid: this.state.viewId,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_HR.CheckLoginResignationForms(checkin)
          .then(res => {
            this.setState({checkInLogin: res.data.data});
            console.log('===checkin====' + res.data.data);
            console.log('callback==' + this.props.LoginName);
          })
          .catch(error => {
            console.log(error.data.data);
          });
        API.getProfile().then(rs => {
          console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
          this.setState({LoginName: rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log('=======' + error.data);
      });
  }
  CustomViewDetailRoutings = () => {
    const item = this.state.Data;
    return (
      <View>
        <View style={{padding: 20}}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Mã phiếu:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault, {fontWeight: 'bold'}]}>
                {item.ResignationFormId}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Tiêu đề:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>{item.Title}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text>
            </View>
          </View>
          {/*Phòng ban*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>{item.DepartmentName}</Text>
            </View>
          </View>
          {/*Ngày tạo phiếu*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Ngày nghỉ :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.customDate(item.StartDate)}
              </Text>
            </View>
          </View>

          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
            </View>
            <View style={{flex: 3}}>
              {item.Status == 'Y' ? (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {color: AppColors.AcceptColor},
                  ]}>
                  {item.StatusWF}
                </Text>
              ) : (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {color: AppColors.PendingColor},
                  ]}>
                  {item.StatusWF}
                </Text>
              )}
            </View>
          </View>
          {/*nội dung phiếu*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>{item.Description}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  callBackList() {
    Actions.pop();

    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  CustomView = item => (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TabBar_Title
        title={'Chi tiết phiếu nghỉ việc'}
        callBack={() => this.callBackList()}
      />
      {/*hiển thị nội dung chính*/}
      <ScrollView>
        {this.CustomViewDetailRoutings()}
        {/*Người tạo phiếu*/}
      </ScrollView>
      {/*nút xử lý*/}
      <View style={{maxHeight: 50}}>
        {this.state.checkInLogin !== '' ? (
          <TabBarBottom
            //key để quay trở lại danh sách
            onDelete={() => this.Delete()}
            isDraff={this.state.Data.StatusWF === 'Nháp' ? true : false}
            isComplete={this.state.Data.Status === 'Y' ? true : false}
            haveEditButton={this.state.Data.PermissEdit === 1 ? true : false}
            callbackOpenUpdate={this.callbackOpenUpdate}
            backListByKey="ResignationForms"
            keyCommentWF={{
              ModuleId: 34,
              RecordGuid: this.state.viewId,
              Title: this.state.Data.Title,
              //LoginName:this.state.Data.LoginName
            }}
            onAttachment={() => this.onAttachment()}
            // tiêu đề hiển thị trong popup xử lý phiếu
            Title={this.state.Data.Title}
            //kiểm tra quyền xử lý
            Permisstion={this.state.Data.Permisstion}
            //kiểm tra bước đầu quy trình
            checkInLogin={this.state.checkInLogin}
            onSubmitWF={(callback, CommentWF) =>
              this.submitWorkFlow(callback, CommentWF)
            }
          />
        ) : null}
      </View>
    </View>
  );
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    console.log('callback==' + callback + CommentWF);
    if (callback == 'D') {
      let obj = {
        ResignationFormGuid: this.state.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.state.LoginName,
        Comment: CommentWF,
      };
      API_HR.approveResignationForms(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data.data);
        });
    } else {
      let obj = {
        ResignationFormGuid: this.state.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.state.Loginname,
        Comment: CommentWF,
      };
      API_HR.notapproveResignationForms(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity(
              'Có lỗi khi trả lại',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }
  callbackOpenUpdate = () => {
    Actions.AddResignationForms({itemData: this.state.Data});
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetResignationForms);
