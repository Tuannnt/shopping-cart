import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {
  Header,
  Divider,
  Icon,
  ListItem,
  Badge,
  SearchBar,
} from 'react-native-elements';
import {API_HR, API} from '@network';
import TabBar from '../../component/TabBar';
import Fonts from '../../../theme/fonts';
import {Picker} from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import {FuncCommon} from '../../../utils';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';

const SCREEN_WIDTH = Dimensions.get('window').width;
//custom giao diện
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '100',
    fontSize: 30,
    color: '#f6b801',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 0,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 14,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
});

class ResignationFormsComponent extends Component {
  constructor(props) {
    super(props);
    //khai báo biến
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      refreshing: false,
      Data: null,
      ListAll: [],
      ListCountNoti: [],
      ChoDuyet: '',
      TraLai: '',
      EvenFromSearch: false,
    };
    // dữ liệu trueyenf vao APi
    this._search = {
      //OrganizationGuid : '',
      StartDate: new Date(),
      EndDate: new Date(),
      Status: 'W',
      NumberPage: 0,
      search: {value: ''},
      Length: 10,
      Query: 'EmployeeName DESC',
    };
    this.onEndReachedCalledDuringMomentum = true;
    this.TotalRow = 0;
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
  }
  // khi trả lại trang,thì load vào danh sách
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.updateSearch('');
    }
  }
  //Tìm kiếm
  updateSearch = search => {
    this._search.search.value = search;
    this.nextPage(1);
  };
  //List danh sách phân trang
  nextPage(page) {
    this.setState({refreshing: true}, () => {
      if (page) {
        this._search.NumberPage = page;
      } else {
        this._search.NumberPage++;
      }
      API_HR.getAllResignationForms(this._search)
        .then(res => {
          let _data = JSON.parse(res.data.data);
          if (page) {
            this.state.ListAll = _data.list.data;
          } else {
            this.state.ListAll = this.state.ListAll.concat(_data.list.data);
          }
          this.listtabbarBotom[0].Badge = _data.countStt_W;
          this.TotalRow = _data.list.recordsTotal;
          this.setState({
            ListAll: this.state.ListAll,
            refreshing: false,
          });
        })
        .catch(error => {
          console.log(error.data.data);
          console.log(error);
          this.setState({
            refreshing: false,
          });
        });
    });
  }
  // khi kéo danh sách,thì phân trang
  loadMoreData() {
    if (this.TotalRow > this.state.ListAll.length) {
      this.nextPage();
    }
  }
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.nextPage(1);
    }
  };
  setStartDate = date => {
    this._search.StartDate = date;
  };
  setEndDate = date => {
    this._search.EndDate = date;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  onCallbackValueBottom(value) {
    switch (value) {
      case 'home':
        this.onPressHome();
        break;
      case 'profile':
        this.onChoDuyet();
        break;
      case 'checkcircleo':
        this.onDaDuyet();
        break;
      case 'export2':
        this.onKhongDuyet();
        break;
      default:
        break;
    }
  }
  // view hiển thị
  CustomeListAll = item => (
    <View style={{flex: 30}}>
      <FlatList
        data={item}
        style={{height: '100%', marginBottom: 20}}
        renderItem={({item, index}) => (
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              subtitle={() => {
                return (
                  <View style={{flexDirection: 'row', marginTop: -25}}>
                    <View style={{flex: 3, flexDirection: 'row'}}>
                      <View>
                        <Text
                          style={[
                            AppStyles.Titledefault,
                            {
                              //color: '#2E77FF',
                            },
                          ]}>
                          {item.EmployeeName}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          Tiêu đề: {item.Title}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          Lý do: {item.ReasonName}
                        </Text>
                      </View>
                    </View>
                    <View style={{flex: 1.5}}>
                      <Text
                        style={[
                          styles.subtitleStyle,
                          {textAlign: 'right', justifyContent: 'center'},
                        ]}>
                        {item.StartDateString}
                      </Text>
                      {item.Status == 'Y' ? (
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {
                              textAlign: 'right',
                              justifyContent: 'center',
                              color: AppColors.AcceptColor,
                            },
                          ]}>
                          {item.StatusWF}
                        </Text>
                      ) : (
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {
                              textAlign: 'right',
                              justifyContent: 'center',
                              color: AppColors.PendingColor,
                            },
                          ]}>
                          {item.StatusWF}
                        </Text>
                      )}
                    </View>
                  </View>
                );
              }}
              bottomDivider
              //chevron
              onPress={() => this.onViewItem(item.ResignationFormGuid)}
            />
          </View>
        )}
        onEndReachedThreshold={0.5}
        onMomentumScrollBegin={() => {
          this.onEndReachedCalledDuringMomentum = false;
        }}
        onEndReached={() => this.loadMoreData()}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={this.ListEmpty}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
      {/* hiển thị nút tuỳ chọn */}
    </View>
  );
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Phiếu xin nghỉ việc'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            addForm={true}
            CallbackFormAdd={() => Actions.AddResignationForms()}
            backToHome={true}
            BackModuleByCode={'HR'}
            //addForm={'AddResignationForms'}
          />
          <Divider />
          <View>
            {this.state.EvenFromSearch == true ? (
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventStartDate: true})}>
                      <Text>
                        {FuncCommon.ConDate(this._search.StartDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventEndDate: true})}>
                      <Text>{FuncCommon.ConDate(this._search.EndDate, 0)}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'column', padding: 5}}>
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        {textAlign: 'center'},
                        styles.timeHeader,
                      ]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={search => this.updateSearch(search)}
                  value={this._search.search.value}
                />
              </View>
            ) : null}
          </View>
          <View style={{flex: 10}}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>
          <View style={[AppStyles.StyleTabvarBottom, {paddingLeft: 0}]}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.onCallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(1), this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(1), this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  ListEmpty = () => {
    if (this.state.ListAll && this.state.ListAll.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={[AppStyles.centerAligned, {marginTop: 10}]}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  onPressHome() {
    Actions.home();
  }
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.search.value = '';
    this._search.Status = 'W';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      Status: this._search.Status,
      selectedTab: 'profile',
    });
    this.nextPage(1);
  };
  // sự kiện chờ duyệt
  onDaDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.search.value = '';
    this._search.Status = 'Y';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      Status: this._search.Status,
      selectedTab: 'checkcircleo',
    });
    this.nextPage(1);
  };
  // sự kiện ko duyệt
  onKhongDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.search.value = '';
    this._search.Status = 'R';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      Status: this._search.Status,
      selectedTab: 'export2',
    });
    this.nextPage(1);
  };
  //view item,click sang view khác
  onViewItem(id) {
    Actions.GetResignationForms({RecordGuid: id, prevStack: 'listAll'});
  }

  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }
  renderLeftMenu = () => {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'chevron-thin-left'}
          type="entypo"
          size={25}
          onPress={() => Actions.home()}
        />
      </View>
    );
  };
  renderRightMenu = () => {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'plus'}
          type="antdesign"
          size={30}
          onPress={() => Actions.AddResignationForms()}
        />
      </View>
    );
  };
  // xóa tìm kiếm
  onclickreturnSearch() {
    this._search.Search = '';
    this.updateSearch('');
  }
  //
  onValueChange(item) {
    console.log('============>>' + item);
    switch (item) {
      case 'Status': {
        break;
      }
      case 'StartDate': {
        break;
      }
      case 'EndDate': {
        break;
      }
      default: {
      }
    }
  }

  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  //Load lại
  _onRefresh = () => {
    this.updateSearch('');
    this.setState({refreshing: true});
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(ResignationFormsComponent);
