import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Text,
  Dimensions,
  Button,
} from 'react-native';
import {Header, SearchBar, Icon, Badge} from 'react-native-elements';
import {FlatGrid} from 'react-native-super-grid';
import {Actions} from 'react-native-router-flux';
import {API_RegisterCars} from '@network';
import {ECharts} from 'react-native-echarts-wrapper';
import TabBar from '../../component/TabBar';
var width = Dimensions.get('window').width; //full width
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
class indexChartDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      staticParam: {
        lstDepartmentID: '',
        Day: new Date().getDate(),
        Month: new Date().getMonth(),
        year: new Date().getFullYear(),
        GenitiveId: '',
        Status: '',
      },
      listDepartment: [],
      listDataActualSalary: [],
      list: [],
      ListCountNoti: [],
      listMenu: [
        {
          Title: 'Tổng nhân viên',
          Icon: 'users',
          type: 'entypo',
          count: 0,
          code: 'A1',
        },
        {
          Title: 'Số người nghỉ ',
          Icon: 'remove-user',
          type: 'entypo',
          count: 0,
          code: 'A2',
        },
        {
          Title: 'TL đóng BHXH',
          Icon: 'v-card',
          type: 'entypo',
          count: 0,
          code: 'A3',
        },
        {
          Title: 'GH hợp đồng',
          Icon: 'areachart',
          type: 'antdesign',
          count: 0,
          code: 'A4',
        },
        {
          Title: 'Nhân viên mới',
          Icon: 'add-user',
          type: 'entypo',
          count: 0,
          code: 'A5',
        },
        {
          Title: 'Nghỉ phép',
          Icon: 'account-alert',
          type: 'material-community',
          count: 0,
          code: 'A6',
        },
        {
          Title: 'HS tuyển dụng',
          Icon: 'clipboard-text-outline',
          type: 'material-community',
          count: 0,
          code: 'A7',
        },
        {
          Title: 'YC đào tạo ',
          Icon: 'school',
          type: 'material-community',
          count: 0,
          code: 'A8',
        },
        {
          Title: 'YC tuyển dụng',
          Icon: 'account-multiple-plus',
          type: 'material-community',
          count: 0,
          code: 'A9',
        },
        {
          Title: 'Đăng ký xe',
          Icon: 'car-multiple',
          type: 'material-community',
          count: 0,
          code: 'A10',
        },
        {
          Title: 'ĐK làm thêm',
          Icon: 'timetable',
          type: 'material-community',
          count: 0,
          code: 'A11',
        },
        {
          Title: 'Đăng ký ăn',
          Icon: 'ticket',
          type: 'entypo',
          count: 0,
          code: 'A12',
        },
      ],
      option: {
        grid: {
          containLabel: true,
          right: 20,
          left: 30,
          height: 200,
        },
        xAxis: {
          data: [],
          axisLabel: {
            interval: 0,
            rotate: 90,
            fontSize: 8,
          },
        },
        yAxis: {
          type: 'value',
        },
        series: [
          {
            label: {
              show: true,
              position: 'top',
              color: 'black',
              fontSize: 8,
              marginTop: 0,
            },
            data: [],
            type: 'bar',
            color: 'rgb(255, 117, 63)',
          },
        ],
      },
    };
  }

  loadMoreData() {
    this.nextPage();
  }
  componentDidMount() {
    this.GetAcount();
    this.GetItem_Dashboad();
  }
  GetAcount() {
    API_RegisterCars.CountDashboard()
      .then(res => {
        var listCount = JSON.parse(res.data.data);
        this.setState({ListCountNoti: listCount});
        var _listMenu = this.state.listMenu;
        for (var i = 0; i < _listMenu.length; i++) {
          if (_listMenu[i].code == 'A1') {
            _listMenu[i].count = listCount.TongNhanVien;
          } else if (_listMenu[i].code == 'A2') {
            _listMenu[i].count = listCount.NhanVienNghi;
          } else if (_listMenu[i].code == 'A3') {
            _listMenu[i].count = listCount.TiLeDongBH;
          } else if (_listMenu[i].code == 'A4') {
            _listMenu[i].count = listCount.GiaHanHD;
          } else if (_listMenu[i].code == 'A5') {
            _listMenu[i].count = listCount.NhanVienMoi;
          } else if (_listMenu[i].code == 'A6') {
            _listMenu[i].count =
              listCount.SoPhieuNghi + '/' + listCount.TongSoPhieuNghi;
          } else if (_listMenu[i].code == 'A7') {
            _listMenu[i].count = listCount.HoSoTuyenDung;
          } else if (_listMenu[i].code == 'A8') {
            _listMenu[i].count =
              listCount.PhieuYCDTChuaDuyet + '/' + listCount.TongPhieuYCDT;
          } else if (_listMenu[i].code == 'A9') {
            _listMenu[i].count =
              listCount.YeuCauTuyenDungChuaDuyet +
              '/' +
              listCount.TongYeuCauTuyenDung;
          } else if (_listMenu[i].code == 'A10') {
            _listMenu[i].count =
              listCount.DangKyXeChuaDuyet + '/' + listCount.TongDangKyXe;
          } else if (_listMenu[i].code == 'A11') {
            _listMenu[i].count =
              listCount.DangKyLamThemChuaDuyet +
              '/' +
              listCount.TongDangKyLamThem;
          } else if (_listMenu[i].code == 'A12') {
            _listMenu[i].count =
              listCount.DangKyAnChuaDuyet + '/' + listCount.TongDangKyAn;
          }
        }
        this.setState({
          listMenu: _listMenu,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  nextPage() {
    this.setState({
      staticParam: {
        lstDepartmentID: this.state.staticParam.lstDepartmentID,
        Day: this.state.staticParam.Day,
        Month: this.state.staticParam.Month,
        year: this.state.staticParam.year,
        GenitiveId: this.state.staticParam.GenitiveId,
        Status: this.state.staticParam.Status,
      },
    });
    this.GetItem_Dashboad();
  }
  GetItem_Dashboad = () => {
    API_RegisterCars.GetItemDashboad(this.state.staticParam)
      .then(res => {
        console.log(res);
        var _listDepartment = JSON.parse(res.data.data);
        var _listMenu = [];
        var _listDataActualSalary = [];
        for (var i = 0; i < _listDepartment.length; i++) {
          _listMenu.push(_listDepartment[i].DepartmentId);
          _listDataActualSalary.push(
            parseFloat(_listDepartment[i].ActualSalary),
          );
        }
        var _option = this.state.option;
        _option.xAxis.data = _listMenu;
        _option.series[0].data = _listDataActualSalary;
        this.setState({
          option: _option,
        });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <TabBar title={'Dashboard'} BackModuleByCode={'MyProfile'} />
        <ScrollView>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={{flex: 3}}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <FlatGrid
                  itemDimension={100}
                  items={this.state.listMenu}
                  style={styles.gridView}
                  renderItem={({item, index}) => (
                    <View
                      style={{
                        borderRadius: 10,
                        backgroundColor: '#FFFFFF',
                        height: 80,
                      }}>
                      <View style={[styles.itemContainerdetail]}>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{flex: 1}}>
                            <Icon
                              name={item.Icon}
                              type={item.type}
                              size={20}
                              color={AppColors.Maincolor}
                            />
                          </View>
                          <View style={{flex: 1}}>
                            <Text style={{fontWeight: 'bold', fontSize: 17}}>
                              {item.count}
                            </Text>
                          </View>
                        </View>
                        <View>
                          <Text>{item.Title}</Text>
                        </View>
                      </View>
                    </View>
                  )}
                />
              </ScrollView>
            </View>
            {/* <View style={{ flexDirection: 'row', paddingRight: 10 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', marginLeft: 10, flex: 1 }}>Nhân sự hàng ngày</Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  >
                                <View style={styles.chartContainer}>
                                    {
                                        (this.state.option && this.state.option.xAxis.data.length > 0 && this.state.option.series[0].data.length > 0) ?
                                            <ECharts option={this.state.option}></ECharts>
                                            : null
                                    }
                                </View>
                            </ScrollView>
                        </View> */}
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  itemContainerdetail: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 17,
    flexDirection: 'column',
  },
  chartContainer: {
    flexDirection: 'column',
    width: width,
    flex: 1,
    height: 300,
    margin: 0,
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(indexChartDashboard);
