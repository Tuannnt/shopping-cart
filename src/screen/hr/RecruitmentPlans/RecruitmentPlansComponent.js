import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Header, Divider, Icon, ListItem, Badge} from 'react-native-elements';
import {API_HR, API} from '@network';
import TabBar from '../../component/TabBar';
import Fonts from '../../../theme/fonts';
import {Picker} from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
//custom giao diện
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '100',
    fontSize: 30,
    color: '#f6b801',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 2,
    paddingBottom: 5,
    paddingLeft: 10,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
});

class RecruitmentPlansComponent extends Component {
  constructor(props) {
    super(props);
    //khai báo biến
    this.state = {
      refreshing: false,
      Data: null,
      ListAll: [],
      ListCountNoti: [],
      ChoDuyet: '',
      TraLai: '',
      loading: false,
    };
    // dữ liệu trueyenf vao APi
    this._search = {
      //OrganizationGuid : '',
      StartTime: '2019-11-24',
      EndTime: '2020-12-24',
      Status: '',
      NumberPage: 0,
      txtSearch: '',
      Length: 14,
      Query: 'EmployeeName DESC',
    };
  }
  //khi vào trang chạy đầu tiên
  componentDidMount(): void {
    this.nextPage();
  }
  // khi trả lại trang,thì load vào danh sách
  componentWillReceiveProps(nextProps) {
    this.nextPage();
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.txtSearch = search;
    this._search.NumberPage = 0;
    this.nextPage();
  };
  //List danh sách phân trang
  nextPage() {
    this.setState({loading: true});
    this._search.NumberPage++;
    API.CountStatus()
      .then(res => {
        var listCount = JSON.parse(res.data.data);
        this.setState({ListCountNoti: listCount});
        var _choduyet = '';
        var _tralai = '';
        var data = listCount.find(x => x.Name == 'NghiViec');
        if (data !== null && data !== undefined) {
          if (data.Value > 99) {
            _choduyet = '99+';
          } else {
            _choduyet = data.Value;
          }
          this.setState({ChoDuyet: _choduyet});
        }
        //tra lai
        var dataTraLai = listCount.find(x => x.Name == 'ResignationFormNot');
        if (dataTraLai !== null && dataTraLai !== undefined) {
          if (dataTraLai.Value > 99) {
            _tralai = '99+';
          } else {
            _tralai = dataTraLai.Value;
          }
          this.setState({TraLai: _tralai});
        }
      })
      .catch(error => {
        console.log(error.data);
      });
    API_HR.getAllRecruitmentPlans(this._search)
      .then(res => {
        this.state.ListAll = this.state.ListAll.concat(
          JSON.parse(res.data.data).data,
        );
        this.setState({
          ListAll: this.state.ListAll,
          refreshing: false,
        });
      })
      .catch(error => {
        console.log(error.data);
        console.log(error);
      });
  }
  // khi kéo danh sách,thì phân trang
  loadMoreData() {
    this.nextPage();
  }
  // view hiển thị
  CustomeListAll = item => (
    <View style={{flex: 30}}>
      <FlatList
        data={item}
        ListEmptyComponent={this.ListEmpty}
        style={{height: '100%', marginBottom: 60}}
        renderItem={({item, index}) => (
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              subtitle={() => {
                return (
                  <View style={{flexDirection: 'row', marginTop: -25}}>
                    <View style={{flex: 3, flexDirection: 'row'}}>
                      <View>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            fontSize: 16,
                            color: '#2E77FF',
                          }}>
                          {item.EmployeeName}
                        </Text>
                        <Text style={styles.subtitleStyle}>
                          Tiêu đề: {item.Title}
                        </Text>
                        <Text style={styles.subtitleStyle}>
                          Ngày tạo KH: {item.CreatedDateString}
                        </Text>
                      </View>
                    </View>
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          styles.subtitleStyle,
                          {textAlign: 'right', justifyContent: 'center'},
                        ]}>
                        {item.DateString}
                      </Text>
                      {item.Status == 'Y' ? (
                        <Text
                          style={{
                            textAlign: 'right',
                            justifyContent: 'center',
                            color: 'green',
                          }}>
                          {item.StatusWF}
                        </Text>
                      ) : (
                        <Text
                          style={{
                            textAlign: 'right',
                            justifyContent: 'center',
                            color: 'red',
                          }}>
                          {item.StatusWF}
                        </Text>
                      )}
                    </View>
                  </View>
                );
              }}
              bottomDivider
              //chevron
              onPress={() => this.onViewItem(item.RecruitmentPlanGuid)}
            />
          </View>
        )}
        onEndReached={() => this.loadMoreData()}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['black']}
          />
        }
      />
      <TabNavigator tabBarStyle={{height: 60}}>
        <TabNavigator
          ListEmptyComponent={this.ListEmpty}
          tabStyle={styles.tabNavigatorItem}
          selected={this.state.selectedTab === 'home'}
          title={'Trang chủ'}
          renderIcon={() => (
            <Icon style={styles.itemIcon} type="antdesign" name="home" />
          )}
          renderSelectedIcon={() => (
            <Icon
              iconStyle={styles.itemIconClick}
              type="antdesign"
              name="home"
            />
          )}
          onPress={() => this.onPressHome()}
        />
        <TabNavigator
          tabStyle={styles.tabNavigatorItem}
          selected={this.state.selectedTab === 'profile'}
          title={'Chờ duyệt'}
          renderIcon={() => (
            <View>
              <Icon style={styles.itemIcon} type="antdesign" name="profile" />
              <Badge
                status="error"
                value={this.state.ChoDuyet}
                containerStyle={{
                  position: 'absolute',
                  right: -10,
                  top: -10,
                }}
              />
            </View>
          )}
          renderSelectedIcon={() => (
            <View>
              <Icon
                iconStyle={styles.itemIconClick}
                type="antdesign"
                name="profile"
              />
              <Badge
                status="error"
                value={this.state.ChoDuyet}
                containerStyle={{
                  position: 'absolute',
                  right: -10,
                  top: -10,
                }}
              />
            </View>
          )}
          onPress={() => this.onChoDuyet()}
        />
        <TabNavigator
          ListEmptyComponent={this.ListEmpty}
          tabStyle={styles.tabNavigatorItem}
          selected={this.state.selectedTab === 'checkcircleo'}
          title={'Đã duyệt'}
          renderIcon={() => (
            <View>
              <Icon
                style={styles.itemIcon}
                type="antdesign"
                name="checkcircleo"
              />
              {/* <Badge
                                status="error"
                                value={this.state.DaDuyet}
                                containerStyle={{ position: 'absolute', right: -10, top: -10 }}
                            /> */}
            </View>
          )}
          renderSelectedIcon={() => (
            <Icon
              iconStyle={styles.itemIconClick}
              type="antdesign"
              name="checkcircleo"
            />
          )}
          onPress={() => this.onDaDuyet()}
        />
        <TabNavigator
          ListEmptyComponent={this.ListEmpty}
          tabStyle={styles.tabNavigatorItem}
          selected={this.state.selectedTab === 'export2'}
          title={'Trả lại'}
          renderIcon={() => (
            <View>
              <Icon style={styles.itemIcon} type="antdesign" name="export2" />
              <Badge
                status="error"
                value={this.state.TraLai}
                containerStyle={{
                  position: 'absolute',
                  right: -10,
                  top: -10,
                }}
              />
            </View>
          )}
          renderSelectedIcon={() => (
            <View>
              <Icon
                iconStyle={styles.itemIconClick}
                type="antdesign"
                name="export2"
              />
              <Badge
                status="error"
                value={this.state.TraLai}
                containerStyle={{
                  position: 'absolute',
                  right: -10,
                  top: -10,
                }}
              />
            </View>
          )}
          onPress={() => this.onKhongDuyet()}
        />
      </TabNavigator>
    </View>
  );
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <Header
            leftComponent={this.renderLeftMenu()}
            centerComponent={{
              text: 'Kế hoạch tuyển dụng',
              style: {color: 'black', fontSize: 25, marginLeft: -30},
            }}
            rightComponent={this.renderRightMenu()}
            containerStyle={{backgroundColor: '#fff', height: 60}}
          />
          <Divider />
          <View>
            <View style={{flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  width: 45,
                  height: 50,
                  textAlign: 'center',
                  justifyContent: 'center',
                }}>
                {this._search.txtSearch.length > 0 ? (
                  <TouchableWithoutFeedback
                    onPress={() => this.onclickreturnSearch()}
                    underlayColor="white">
                    <Icon name="close" type="antdesign" />
                  </TouchableWithoutFeedback>
                ) : (
                  <TouchableWithoutFeedback
                    onPress={this.onClickBack}
                    underlayColor="white">
                    <Icon name="search1" type="antdesign" />
                  </TouchableWithoutFeedback>
                )}
              </View>
              <View style={{flex: 8}}>
                <TextInput
                  style={{height: 50}}
                  onChangeText={search => this.updateSearch(search)}
                  value={this._search.txtSearch}
                  placeholder={'Nhập từ khóa tìm kiếm'}
                />
              </View>
            </View>
          </View>
          <View style={{flex: 10}}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  ListEmpty = () => {
    if (this.state.ListAll.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  onPressHome() {
    Actions.home();
  }
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'W';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'profile',
    });
    this.nextPage();
  };
  // sự kiện chờ duyệt
  onDaDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'Y';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'checkcircleo',
    });
    this.nextPage();
  };
  // sự kiện ko duyệt
  onKhongDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'R';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'export2',
    });
    this.nextPage();
  };
  //view item,click sang view khác
  onViewItem(id) {
    Actions.GetRecruitmentPlans({viewId: id});
  }

  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }
  renderLeftMenu = () => {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'chevron-thin-left'}
          type="entypo"
          size={25}
          onPress={() => Actions.home()}
        />
      </View>
    );
  };
  renderRightMenu = () => {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'plus'}
          type="antdesign"
          size={30}
          onPress={() => Actions.AddResignationForms()}
        />
      </View>
    );
  };
  // xóa tìm kiếm
  onclickreturnSearch() {
    this._search.Search = '';
    this.updateSearch('');
  }
  //
  onValueChange(item) {
    console.log('============>>' + item);
    switch (item) {
      case 'Status': {
        break;
      }
      case 'StartDate': {
        break;
      }
      case 'EndDate': {
        break;
      }
      default: {
      }
    }
  }

  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  //Load lại
  _onRefresh = () => {
    this.nextPage();
    this.setState({refreshing: true});
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(RecruitmentPlansComponent);
