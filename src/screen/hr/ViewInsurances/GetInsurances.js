import React, {Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  Linking,
  Keyboard,
} from 'react-native';
// import Icon from "react-native-vector-icons/FontAwesome";
import {Actions, ActionConst} from 'react-native-router-flux';
import {Header, Divider, Icon, SearchBar} from 'react-native-elements';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Left,
  Body,
} from 'native-base';
import {connect} from 'react-redux';
import {Back} from '@component';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import DropDownItem from 'react-native-drop-down-item';
import {WebView} from 'react-native-webview';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import TabBar_Title from '../../component/TabBar_Title';
const IC_ARR_DOWN = require('@images/logo/ic_arr_down.png');
const IC_ARR_UP = require('@images/logo/ic_arr_up.png');

type Props = {};
class GetInsurances extends Component {
  constructor(props) {
    super(props);
    this.state = {
      staticParam: {
        OrganizationGuid: '',
        EmployeeGuid: '',
        NumberPage: 1,
        Length: 1000,
        search: {
          value: '',
        },
      },
      Data: [],
      EmployeeId: null,
      status: null,
      EmployeeGuid: null,
      FullName: null,
      WorkPhone: null,
      bankDataItem: null,
      loading: false,
      contents: [],
      list: [],
      listHistory: [],
      list_lg: [],
      stylex: 1,
    };
    this.listtabbarBotom = [
      {
        Title: 'Hiện hành',
        Icon: 'user',
        Type: 'antdesign',
        Value: 'HDActive',
        Checkbox: true,
      },

      {
        Title: 'Lịch sử',
        Icon: 'filetext1',
        Type: 'antdesign',
        Value: 'HDHistory',
        Checkbox: false,
      },
    ];
  }
  componentDidMount(): void {
    console.log(this.props.EmployeeGuid);
    if (this.props.EmployeeGuid != '' && this.props.EmployeeGuid != null) {
      this.GetEmployeeById(this.props.EmployeeGuid);
    } else {
      this.setState({loading: false});
    }
    this.getAllHistory();
    this.getAllInsurancesIsActive();
  }
  onClick = data => {
    if (data == 'HDActive') {
      //   this.getAllInsurancesIsActive ();
      this.setState({
        stylex: 1,
      });
    } else if (data == 'HDHistory') {
      //   this.setState ({listHistory: []});
      //   this.getAllHistory ();
      this.setState({
        stylex: 2,
      });
    }
  };

  onPressback() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  render() {
    return (
      <Container style={{flex: 1}}>
        <TabBar_Title
          title={'Thông tin bảo hiểm'}
          callBack={() => this.onPressback()}
        />
        {this.state.stylex == 1 && this.state.Data.length > 0
          ? this.HDActive()
          : null}
        {this.state.stylex == 2 ? this.HDHistory() : null}
        <View style={[AppStyles.StyleTabvarBottom]}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onClick(callback)}
          />
        </View>
      </Container>
    );
  }
  HDActive = () => {
    return (
      <Content style={{marginStart: 0, marginEnd: 0}}>
        <Content style={{marginStart: 15, marginEnd: 15}}>
          <View thumbnail style={{marginTop: 10}}>
            <Body style={{flex: 1, alignItems: 'center'}}>
              <Thumbnail
                source={API_HR.GetPicApplyLeaves(this.state.EmployeeGuid)}
              />
              <Text>{this.state.FullName}</Text>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.EmployeeId}
              </Text>
              <Text
                style={([AppStyles.Textdefault], {color: 'blue'})}
                onPress={() => {
                  Linking.openURL('tel:' + this.state.HomeMobile);
                }}>
                {this.state.HomeMobile}
              </Text>
              <Text>{this.state.HomeEmail}</Text>
            </Body>
          </View>
          <Divider style={{marginBottom: 10, marginTop: 10}} />
          <View thumbnail>
            <Text style={{fontWeight: 'bold', paddingBottom: 12}}>
              Thông tin chung
            </Text>
            {/* <View style={{ flexDirection: 'row',padding :5 }}>
                                <View style={{ flex: 4 }}>
                                    <Text style={AppStyles.Labeldefault}>Mã nhân viên :</Text>
                                </View>
                                <View style={{ flex: 2 }}>
                                    <Text style={[AppStyles.Textdefault]}>{this.state.EmployeeId}</Text>
                                </View>
                            </View> */}
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Ngày sinh :</Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.BirthDate}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Chức vụ : </Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.JobTitleName}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Tài khoản : </Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.LoginName}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Trạng thái : </Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.StatusOfWork}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Địa chỉ :</Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.CurrentAddress}
                </Text>
              </View>
            </View>
          </View>

          <View thumbnail>
            <Text
              style={{
                fontWeight: 'bold',
                paddingBottom: 12,
                padding: 5,
              }}>
              Thông tin bảo hiểm
            </Text>
            <Divider style={{marginBottom: 10, marginTop: 10}} />
            {this.state.Data.length > 0 ? (
              this.state.Data.map((item, i) => (
                <View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 2}}>
                      <Text style={AppStyles.Labeldefault}>
                        Số sổ bảo hiểm :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {item.InsuranceNumber}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>
                        Mức đóng bảo hiểm :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.addPeriod(item.InsuranceSalary)}
                      </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>
                        Tỷ lệ bảo hiểm :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.addPeriod(item.EmployeeRate)}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Ngày bắt đầu :</Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.customDate(item.StartDate)}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>
                        Ngày kết thúc :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.customDate(item.EndDate)}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 2}}>
                      <Text style={AppStyles.Labeldefault}>
                        Tiền người lao động đóng :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.addPeriod(item.PaytByEmployee)}
                      </Text>
                    </View>
                  </View>
                  <Divider style={{marginBottom: 10, marginTop: 10}} />
                </View>
              ))
            ) : (
              <View style={styles.MainContainer}>
                <Text style={{textAlign: 'center'}}>Chưa đóng bảo hiểm.</Text>
              </View>
            )}
          </View>
          <View>
            {/* <ScrollView style={{alignSelf: 'stretch'}}>
              {this.state.contents
                ? this.state.contents.map ((param, i) => {
                    return (
                      <DropDownItem
                        key={i}
                        style={{marginBottom: 10}}
                        contentVisible={false}
                        invisibleImage={IC_ARR_DOWN}
                        visibleImage={IC_ARR_UP}
                        header={
                          <View>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                marginBottom: 15,
                              }}
                            >
                              {param.title}
                            </Text>
                            <Divider />
                          </View>
                        }
                      >
                        <View>
                          <Text>{param.body}</Text>
                        </View>
                      </DropDownItem>
                    );
                  })
                : null}
              <View style={{height: 96}} />
            </ScrollView> */}
          </View>
        </Content>
      </Content>
    );
  };
  HDHistory = () => {
    return (
      <View style={{padding: 5, flex: 1}}>
        <Text style={{fontWeight: 'bold'}}>Thông tin lịch sử :</Text>
        <ScrollView style={{flex: 1}}>
          {this.state.listHistory.length > 0
            ? this.state.listHistory.map((item, i) => (
                <View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>
                        Số sổ bảo hiểm :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {item.InsuranceNumber}
                      </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>
                        Mức đóng bảo hiểm :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.addPeriod(item.InsuranceSalary)}
                      </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>
                        Tỷ lệ bảo hiểm :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.addPeriod(item.EmployeeRate)}
                      </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Ngày bắt đầu :</Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.customDate(item.StartDate)}
                      </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>
                        Ngày kết thúc :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.customDate(item.EndDate)}
                      </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 2}}>
                      <Text style={AppStyles.Labeldefault}>
                        Tiền người lao động đóng :
                      </Text>
                    </View>
                    <View style={styles.FormALl}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.addPeriod(item.PaytByEmployee)}
                      </Text>
                    </View>
                  </View>
                  <Divider />
                </View>
              ))
            : null}
        </ScrollView>
      </View>
    );
  };
  ListEmpty = () => {
    if ((this.state.listHistory.length = 0)) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null && strDate != '' && strDate != undefined) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //kiểm tra độ dài tin nhắn
  checkMessage(str, i) {
    if (str !== null && str !== '') {
      var splitted = str.split(' ');
      var strreturn = '';
      var j = 0;
      for (var a = 0; a < splitted.length; a++) {
        j = j + 1;
        if (j == i) {
          strreturn = strreturn + ' ' + splitted[a] + '....';
          break;
        } else {
          strreturn = strreturn + ' ' + splitted[a];
        }
      }
      return strreturn.trim();
    } else {
      return 'Nhóm vừa khởi tạo';
    }
  }
  GetEmployeeById = Id => {
    let obj = {
      EmployeeGuid: Id,
    };
    this.setState({loading: true});
    API_HR.GetEmployeeById(obj)
      .then(response => {
        let data = JSON.parse(JSON.parse(JSON.stringify(response.data)).data);
        console.log('===================: ' + data);
        if (data.StatusOfWork == 'OM') {
          data.StatusOfWork = 'Nhân viên chính';
        } else if (data.StatusOfWork == 'PE') {
          data.StatusOfWork = 'Thử việc';
        } else if (data.StatusOfWork == 'PT') {
          data.StatusOfWork = 'Part-time';
        } else if (data.StatusOfWork == 'ML') {
          data.StatusOfWork = 'Nghỉ thai sản';
        } else if (data.StatusOfWork == 'OL') {
          data.StatusOfWork = 'Nghỉ khác';
        } else if (data.StatusOfWork == 'LJ') {
          data.StatusOfWork = 'Nghỉ việc';
        }
        data.Gender =
          data.Gender == 'M' ? 'Nam' : data.Gender == 'F' ? 'Nữ' : null;
        data.Identification =
          data.Identification != null ? data.Identification : '';
        data.HomeMobile = data.HomeMobile != null ? data.HomeMobile : '';
        data.WorkEmail = data.WorkEmail != null ? data.WorkEmail : '';
        data.BankAccount = data.BankAccount != null ? data.BankAccount : '';
        data.BankName = data.BankName != null ? data.BankName : '';
        this.setState({
          EmployeeGuid: data.EmployeeGuid,
          EmployeeId: data.EmployeeId,
          FullName: data.FullName,
          CandidateName: data.CandidateName,
          DepartmentName: data.DepartmentName,
          Identification: data.Identification,
          IssueIddate: data.IssueIddate,
          LoginName: data.LoginName,
          JobTitleName: data.JobTitleName,
          ProfestionaName: data.ProfestionaName,
          CurrentAddress: data.CurrentAddress,
          PermanentAddress: data.PermanentAddress,
          HireDate: data.HireDate,
          Gender: data.Gender == 'M' ? 'Nam' : data.Gender == 'F' ? 'Nữ' : null,
          MaritalStatus: data.MaritalStatus,
          BirthDate: this.getParsedDate(data.BirthDate),
          SalaryMethod: data.SalaryMethod,
          EthnicName: data.EthnicName,
          HomePhone: data.HomePhone,
          HomeMobile: data.HomeMobile,
          HomeEmail: data.HomeEmail,
          WorkPhone: data.WorkPhone,
          WorkEmail: data.WorkEmail,
          EducationName: data.EducationName,
          TaxCode: data.TaxCode,
          BankName: data.BankName,
          BankAccount: data.BankAccount,
          StatusOfWork: data.StatusOfWork,
          EndDateWork: data.EndDateWork,
          StartDate: data.StartDate,
          ImagePath: data.ImagePath,
          PhotoTitle: data.ImagePath,
          // contents: [
          //     {
          //         title: 'Thông tin thêm',
          //         body: 'Ngày vào làm: ' + this.getParsedDate(data.StartDate) + '\n' + 'Số CMT: ' + data.Identification + '\n' + 'Giới tính: ' + data.Gender + '\n' + 'Số điện thoại: ' + data.HomeMobile + '\n' + 'Email: ' + data.WorkEmail + '\n' + 'Số tài khoản: ' + data.BankAccount + '\n' + 'Ngân hàng: ' + data.BankName
          //         ,
          //     }],
          loading: false,
        });
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  getAllInsurancesIsActive() {
    let id = {EmployeeGuid: this.props.EmployeeGuid};
    API_HR.getInsurancesIsActive(id)
      .then(res => {
        var _data = JSON.parse(res.data.data);
        this.setState({
          Data: _data,
          //listHistory : JSON.parse(res.data.data),
        });
        console.log('===========> error' + res.data.data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  getAllHistory() {
    let id = {EmployeeGuid: this.props.EmployeeGuid};
    API_HR.getViewInsuaranHistory(id)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
          listHistory: JSON.parse(res.data.data),
        });
        console.log('===========> error' + res.data.data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
  },
  ClickApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
  },
  TabarApprove_BH: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  ClickApprove_BH: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  FormSize: {
    fontSize: 15,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GetInsurances);
