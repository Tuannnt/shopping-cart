import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  Dimensions,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {Header, Divider, Button, Input, SearchBar} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions, ActionConst} from 'react-native-router-flux';
import {Back} from '@component';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import {
  Container,
  Content,
  List,
  Left,
  Right,
  Form,
  ListItem,
  Thumbnail,
  Body,
  Item,
  Label,
  Picker,
} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import {AppStyles, AppSizes, AppColors} from '@theme';
import MultiSelect_Component from '../../component/MultiSelect_Component';
import TabBar from '../../component/TabBar';
import {FuncCommon} from '@utils';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';

const SCREEN_WIDTH = Dimensions.get('window').width;
class ViewInsurancesComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 15;
    this.TotalRow = 0;
    this._search = {
      StartDate: new Date(),
      EndDate: new Date(),
    };
    this.state = {
      ValueSearchDate: '1', // năm nay
      name: '',
      loading: false,
      refreshing: false,
      employeeData: [],
      bankDataItem: null,
      search: '',
      StatusOfWork: ['OM'],
      EvenFromSearch: false,
      StatusOfWorkData: [
        {
          id: 'OM',
          name: 'Nhân viên chính thức',
        },
        {
          id: 'PE',
          name: 'Nhân viên thử việc',
        },
        {
          id: 'PT',
          name: 'Nhân viên thời vụ',
        },
        {
          id: 'ML',
          name: 'Nghỉ thai sản',
        },
        {
          id: 'OL',
          name: 'Nghỉ khác',
        },
        {
          id: 'LJ',
          name: 'Nhân viên nghỉ việc',
        },
        {
          id: 'EC',
          name: 'Hết hạn hợp đồng',
        },
      ],
    };
  }
  componentDidMount() {
    this.getAllEmployeePage(1);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getAllEmployeePage(1);
    }
  }
  setStartDate = date => {
    this._search.StartDate = date;
    this.getAllEmployeePage(1);
  };
  setEndDate = date => {
    this._search.EndDate = date;
    this.getAllEmployeePage(1);
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.getAllEmployeePage(1);
    }
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  keyExtractor = (item, index) => index.toString();
  renderItem = ({item}) => {
    return (
      <ListItem avatar onPress={() => this.editItem(item)}>
        <Left>
          <Thumbnail source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
        </Left>
        <Body>
          <Text style={[AppStyles.Titledefault]}>{item.FullName}</Text>
          <Text style={[AppStyles.Textdefault]}>{'Mã:' + item.EmployeeID}</Text>
          <Text style={[AppStyles.Textdefault]}>
            Chức vụ: {item.JobTitlesName != null ? item.JobTitlesName : null}
          </Text>
          <Text
            style={[AppStyles.Textdefault]}
            onPress={() => {
              Linking.openURL('tel:' + item.Phone);
            }}>
            Sđt : {item.Phone != undefined ? item.Phone : null}
          </Text>
        </Body>
        <Right>
          <Icon name="right" style={{fontSize: 20}} />
        </Right>
      </ListItem>
    );
  };
  _onRefresh = () => {
    this.getAllEmployeePage(1);
  };
  searchFilterFunction = text => {
    this.setState({employeeData: []});
    this.state.search = text;
    this.getAllEmployeePage(1);
  };
  onPressback() {
    Actions.app();
  }
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({EvenFromSearch: true});
    } else {
      this.setState({EvenFromSearch: false});
    }
  }
  render() {
    let arrayholder = [];
    return (
      <Container>
        <TabBar
          title={'Danh sách nhân sự'}
          FormSearch={true}
          CallbackFormSearch={callback =>
            this.setState({EvenFromSearch: callback})
          }
          BackModuleByCode={'HR'}
        />
        <View>
          {this.state.EvenFromSearch == true ? (
            <View style={{flexDirection: 'column'}}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    padding: 5,
                  }}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian bắt đầu
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventStartDate: true})}>
                    <Text>{FuncCommon.ConDate(this._search.StartDate, 0)}</Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian kết thúc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventEndDate: true})}>
                    <Text>{FuncCommon.ConDate(this._search.EndDate, 0)}</Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column', padding: 5}}>
                  <Text
                    style={[
                      AppStyles.Labeldefault,
                      {textAlign: 'center'},
                      styles.timeHeader,
                    ]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <SearchBar
                placeholder="Tìm kiếm..."
                lightTheme
                round
                inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                containerStyle={AppStyles.FormSearchBar}
                onChangeText={text => this.searchFilterFunction(text)}
                value={this.state.search}
              />
            </View>
          ) : null}
        </View>
        <View style={{marginStart: 10, marginEnd: 10}}>
          {this.state.EvenFromSearch == true ? (
            <MultiSelect_Component
              selectText={'jkmiuj7yib'}
              ListCombobox={this.state.StatusOfWorkData}
              CallBackListCombobox={callback =>
                this.onValueChangeGroup(callback)
              }
            />
          ) : null}
        </View>
        {this.state.employeeData ? (
          <FlatList
            keyExtractor={this.keyExtractor}
            data={this.state.employeeData}
            refreshing={this.state.loading}
            renderItem={this.renderItem}
            onEndReached={this.handleLoadMore}
            onRefresh={this._onRefresh}
            ListFooterComponent={this.renderFooter.bind(this)}
            onEndReachedThreshold={0.4}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
        ) : null}
        {this.state.setEventStartDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.getAllEmployeePage(1),
                  this.setState({setEventStartDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this._search.StartDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setStartDate(setDate)}
            />
          </View>
        ) : null}
        {this.state.setEventEndDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.getAllEmployeePage(1),
                  this.setState({setEventEndDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this._search.EndDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setEndDate(setDate)}
            />
          </View>
        ) : null}
        <MenuSearchDate
          value={this.state.ValueSearchDate}
          callback={this.CallbackSearchDate}
          eOpen={this.openMenuSearchDate}
        />
      </Container>
    );
  }
  clickBack() {
    Actions.app();
  }
  onValueChangeGroup(value) {
    if (value.includes(',')) {
      let stamp = value.split(',');
      this.setState({
        StatusOfWork: stamp,
        employeeData: [],
      });
      this.state.StatusOfWork = stamp;
    } else {
      this.setState({
        StatusOfWork: value,
        employeeData: [],
      });
      this.state.StatusOfWork = value;
    }
    this.getAllEmployeePage(1);
  }
  StatusOfWorkData = () => {
    return this.state.StatusOfWorkData.map((x, i) => {
      return <Picker.Item label={x.text} key={i} value={x.value} />;
    });
  };
  editItem(item) {
    Actions.GetInsurances({EmployeeGuid: item.EmployeeGuid});
  }
  addItem() {
    Actions.additem({EmployeeGuid: ''});
  }
  getAllEmployeePage = NumberPage => {
    let obj = {
      NumberPage: NumberPage,
      Length: this.Length,
      Keyword: this.state.search,
      Status: this.state.StatusOfWork,
      lstDepartmentID: [],
      EndDate: this._search.EndDate,
      StartDate: this._search.StartDate,
    };
    console.log('555555 ' + this.state.StatusOfWork);
    this.setState({loading: true});
    API_HR.getAllEmployeePage(obj)
      .then(response => {
        console.log(
          '==============ketqua: ' +
            JSON.stringify(
              JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
            ),
        );
        console.log(
          'Lấy danh sách nhân viên đang làm việc :' + this.state.this,
        );
        let listData = this.state.employeeData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
          ),
        );

        let data = [];
        if (NumberPage === 1) {
          data = data1;
        } else {
          data = listData.concat(data1);
        }
        this.setState({employeeData: data, loading: false});
        this.arrayholder = data;
        let TotalRow = JSON.parse(
          JSON.parse(JSON.stringify(response.data)).data,
        );
        this.TotalRow = TotalRow.Count;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  handleLoadMore = () => {
    if (!this.state.loading && this.TotalRow > this.state.employeeData.length) {
      this.page = this.page + 1; // increase page by 1
      this.getAllEmployeePage(this.page); // method for API call
    }
  };
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
}
const colors = {
  loginGreen: '#347F16',
};
const styles = StyleSheet.create({
  loginContainer: {
    marginTop: 10,
    marginEnd: 10,
    alignItems: 'flex-end',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewInsurancesComponent);
