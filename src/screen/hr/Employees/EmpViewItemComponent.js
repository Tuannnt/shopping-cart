import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  Linking,
} from 'react-native';
// import Icon from "react-native-vector-icons/FontAwesome";
import { Actions, ActionConst } from 'react-native-router-flux';
import { Header, Divider, Icon, SearchBar } from 'react-native-elements';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Left,
  Body,
} from 'native-base';
import { connect } from 'react-redux';
import { Back } from '@component';
import { API_HR } from '@network';
import { ErrorHandler } from '@error';
import DropDownItem from 'react-native-drop-down-item';
import { WebView } from 'react-native-webview';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Combobox from '../../component/Combobox';
const IC_ARR_DOWN = require('@images/logo/ic_arr_down.png');
const IC_ARR_UP = require('@images/logo/ic_arr_up.png');

type Props = {};
class EmpViewItemComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      staticParam: {
        OrganizationGuid: '',
        EmployeeGuid: '',
        ContractType: '',
        NumberPage: 1,
        Length: 1000,
        search: {
          value: '',
        },
      },
      EmployeeId: null,
      status: null,
      EmployeeGuid: null,
      FullName: null,
      WorkPhone: null,
      bankDataItem: null,
      loading: false,
      contents: [],
      list: [],
      list_bh: [],
      list_lg: [],
      list_ct: [],
      list_ktkl: [],
      list_cccn: [],
      list_dt: [],
      list_gtgc: [],
      stylex: 1,
      dTypeOfRequest: [
        {
          value: 'CT',
          text: 'Công tác',
        },
        {
          value: 'KTKL',
          text: 'Khen thưởng - Kỉ luật',
        },
        {
          value: 'CCCN',
          text: 'Chứng chỉ',
        },
        {
          value: 'GTGC',
          text: 'Giảm trừ gia cảnh',
        },
      ],
    };
    this.listtabbarBotom = [
      {
        Title: 'Nhân sự',
        Icon: 'user',
        Type: 'antdesign',
        Value: 'NS',
        Checkbox: true,
      },
      {
        Title: 'Hợp đồng',
        Icon: 'filetext1',
        Type: 'antdesign',
        Value: 'HD',
        Checkbox: false,
      },
      {
        Title: 'Bảo hiểm',
        Icon: 'security',
        Type: 'materialicons',
        Value: 'BH',
        Checkbox: false,
      },
      {
        Title: 'Lương',
        Icon: 'logo-usd',
        Type: 'ionicon',
        Value: 'LG',
        Checkbox: false,
      },
      {
        Title: '',
        Icon: 'dots-three-horizontal',
        Type: 'entypo',
        Value: 'combobox',
        Checkbox: false,
      },
    ];
  }
  componentDidMount = () => {
    if (this.props.EmployeeGuid != '' && this.props.EmployeeGuid != null) {
      this.GetEmployeeById(this.props.EmployeeGuid);
    } else {
      this.setState({ loading: false });
    }
  }
  _openCombobox() { }
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCB(item) {
    this._openCombobox();
  }
  onPressback() {
    Actions.pop();
  }
  render() {
    return (
      <Container>
        <TabBar_Title
          title={'Thông tin nhân sự'}
          callBack={() => this.onPressback()}
        />
        <View style={{ flex: 1 }}>

          <Container>
            <View style={{}}>
              <View style={{ marginStart: 15, marginEnd: 15 }}>
                <View thumbnail style={{ marginTop: 10 }}>
                  <Body style={{ flex: 1, alignItems: 'center' }}>
                    <Thumbnail
                      source={API_HR.GetPicApplyLeaves(
                        this.state.EmployeeGuid,
                      )}
                    />
                    <Text>{this.state.FullName}</Text>
                    <Text
                      style={([AppStyles.Textdefault], { color: 'blue' })}
                      onPress={() => {
                        Linking.openURL('tel:' + this.state.HomeMobile);
                      }}>
                      {this.state.HomeMobile}
                    </Text>
                  </Body>
                </View>
                <Divider style={{ marginBottom: 10, marginTop: 100 }} />
                <View thumbnail>
                  <Text style={{ fontWeight: 'bold', paddingBottom: 12 }}>
                    Thông tin chung
                  </Text>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <Text style={AppStyles.Labeldefault}>Mã :</Text>
                    <View style={{ flex: 3, alignItems: 'flex-end' }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.EmployeeId}
                      </Text>
                    </View>
                  </View>

                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <Text style={AppStyles.Labeldefault}>Ngày sinh:</Text>
                    <View style={{ flex: 3, alignItems: 'flex-end' }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.BirthDate}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <Text style={AppStyles.Labeldefault}>Chức vụ:</Text>
                    <View style={{ flex: 3, alignItems: 'flex-end' }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.JobTitleName}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <Text style={AppStyles.Labeldefault}>Giới tính:</Text>
                    <View style={{ flex: 3, alignItems: 'flex-end' }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Gender == 'M'
                          ? 'Nam'
                          : this.state.Gender == 'F'
                            ? 'Nữ'
                            : null}
                      </Text>
                    </View>
                  </View>
                  {/* <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Số điện thoại:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.HomeMobile}
                        </Text>
                      </View>
                    </View> */}
                  {/* <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Tài khoản:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.LoginName}
                        </Text>
                      </View>
                    </View> */}
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <Text style={AppStyles.Labeldefault}>Trạng thái:</Text>
                    <View style={{ flex: 3, alignItems: 'flex-end' }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.StatusOfWork}
                      </Text>
                    </View>
                  </View>
                </View>
                {/* <Divider style={{ marginBottom: 10, marginTop: 10 }} /> */}
                {/* <View thumbnail>
                    <Text style={{ fontWeight: 'bold', paddingBottom: 12 }}>
                      Thông tin thêm
                    </Text>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Ngày vào làm:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.getParsedDate(this.state.StartDate)}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Số CM:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Identification}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Giới tính:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Gender == 'M'
                            ? 'Nam'
                            : this.state.Gender == 'F'
                              ? 'Nữ'
                              : null}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Số điện thoại:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.HomeMobile}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Email:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.WorkEmail}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Số tài khoản:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.BankAccount}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Ngân hàng:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.BankName}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                      <Text style={AppStyles.Labeldefault}>Địa chỉ:</Text>
                      <View style={{ flex: 3, alignItems: 'flex-end' }}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.CurrentAddress}
                        </Text>
                      </View>
                    </View>
                  </View> */}
                {/* <Divider style={{ marginBottom: 10, marginTop: 10 }} /> */}
                {/* <View>
                    <ScrollView style={{ alignSelf: 'stretch' }}>
                      {this.state.contents
                        ? this.state.contents.map((param, i) => {
                          return (
                            <DropDownItem
                              key={i}
                              style={{ marginBottom: 10 }}
                              contentVisible={false}
                              invisibleImage={IC_ARR_DOWN}
                              visibleImage={IC_ARR_UP}
                              header={
                                <View>
                                  <Text
                                    style={{
                                      fontWeight: 'bold',
                                      marginBottom: 15,
                                    }}>
                                    {param.title}
                                  </Text>
                                  <Divider />
                                </View>
                              }>
                              <View>
                                <Text>{param.body}</Text>
                              </View>
                            </DropDownItem>
                          );
                        })
                        : null}
                      <View style={{ height: 96 }} />
                    </ScrollView>
                  </View> */}
              </View>
            </View>
          </Container>
        </View>
        {/* <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onClick(callback)}
          />
        </View> */}
        {/* <Combobox
          TypeSelect={'single'} // single or multiple
          callback={this.onValueChange}
          data={this.state.dTypeOfRequest}
          eOpen={this.openCombobox}
          position={'bottom'}
          value={'CT'}
        /> */}
      </Container>
    );
  }
  openView(id) {
    Actions.openContracts({ ContractGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openInsurances(id) {
    Actions.openInsurances({ InsuranceGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openSalaries(id) {
    Actions.openSalaries({ SalaryGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openHistories(id) {
    Actions.openHistories({ HistoryGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openRewards(id) {
    Actions.openRewards({ RewardGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openCertificates(id) {
    Actions.openCertificates({ CertificateGuid: id });
    console.log('===================chuyen Id:' + id);
  }

  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //kiểm tra độ dài tin nhắn
  checkMessage(str, i) {
    if (str !== null && str !== '') {
      var splitted = str.split(' ');
      var strreturn = '';
      var j = 0;
      for (var a = 0; a < splitted.length; a++) {
        j = j + 1;
        if (j == i) {
          strreturn = strreturn + ' ' + splitted[a] + '....';
          break;
        } else {
          strreturn = strreturn + ' ' + splitted[a];
        }
      }
      return strreturn.trim();
    } else {
      return 'Nhóm vừa khởi tạo';
    }
  }
  GetEmployeeById = Id => {
    let obj = {
      EmployeeGuid: Id,
    };
    this.setState({ loading: true });
    API_HR.GetEmployeeById(obj)
      .then(response => {
        let data = JSON.parse(JSON.parse(JSON.stringify(response.data)).data);
        console.log('===================: ' + data);
        if (data.StatusOfWork == 'OM') {
          data.StatusOfWork = 'Nhân viên chính';
        } else if (data.StatusOfWork == 'PE') {
          data.StatusOfWork = 'Thử việc';
        } else if (data.StatusOfWork == 'PT') {
          data.StatusOfWork = 'Part-time';
        } else if (data.StatusOfWork == 'ML') {
          data.StatusOfWork = 'Nghỉ thai sản';
        } else if (data.StatusOfWork == 'OL') {
          data.StatusOfWork = 'Nghỉ khác';
        } else if (data.StatusOfWork == 'LJ') {
          data.StatusOfWork = 'Nghỉ việc';
        }
        data.Gender = data.Gender != null ? data.Gender : '';
        data.Identification =
          data.Identification != null ? data.Identification : '';
        data.HomeMobile = data.HomeMobile != null ? data.HomeMobile : '';
        data.WorkEmail = data.WorkEmail != null ? data.WorkEmail : '';
        data.BankAccount = data.BankAccount != null ? data.BankAccount : '';
        data.BankName = data.BankName != null ? data.BankName : '';
        this.setState({
          EmployeeGuid: data.EmployeeGuid,
          EmployeeId: data.EmployeeId,
          FullName: data.FullName,
          CandidateName: data.CandidateName,
          DepartmentName: data.DepartmentName,
          Identification: data.Identification,
          IssueIddate: data.IssueIddate,
          LoginName: data.LoginName,
          JobTitleName: data.JobTitleName,
          ProfestionaName: data.ProfestionaName,
          CurrentAddress: data.CurrentAddress,
          PermanentAddress: data.PermanentAddress,
          HireDate: data.HireDate,
          Gender: data.Gender,
          MaritalStatus: data.MaritalStatus,
          BirthDate: this.getParsedDate(data.BirthDate),
          SalaryMethod: data.SalaryMethod,
          EthnicName: data.EthnicName,
          HomePhone: data.HomePhone,
          HomeMobile: data.HomeMobile,
          HomeEmail: data.HomeEmail,
          WorkPhone: data.WorkPhone,
          WorkEmail: data.WorkEmail,
          EducationName: data.EducationName,
          TaxCode: data.TaxCode,
          BankName: data.BankName,
          BankAccount: data.BankAccount,
          StatusOfWork: data.StatusOfWork,
          EndDateWork: data.EndDateWork,
          StartDate: data.StartDate,
          ImagePath: data.ImagePath,
          PhotoTitle: data.ImagePath,
          // contents: [
          //     {
          //         title: 'Thông tin thêm',
          //         body: 'Ngày vào làm: ' + this.getParsedDate(data.StartDate) + '\n' + 'Số CMT: ' + data.Identification + '\n' + 'Giới tính: ' + data.Gender + '\n' + 'Số điện thoại: ' + data.HomeMobile + '\n' + 'Email: ' + data.WorkEmail + '\n' + 'Số tài khoản: ' + data.BankAccount + '\n' + 'Ngân hàng: ' + data.BankName
          //         ,
          //     }],
          loading: false,
        });
      })
      .catch(error => {
        this.setState({ loading: false });
        ErrorHandler.handle(error.data);
      });
  };
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  GetAll_HD = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    API_HR.Contracts_ListAll(this.state.staticParam)
      .then(res => {
        this.setState({ list: JSON.parse(res.data.data).data });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  GetAll_BH = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    API_HR.Insurances_ListAll(this.state.staticParam)
      .then(res => {
        this.setState({ list: JSON.parse(res.data.data).data });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  GetAll_LG = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    API_HR.Salaries_ListAll(this.state.staticParam)
      .then(res => {
        this.setState({ list: JSON.parse(res.data.data).data });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  GetAll_CT = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    API_HR.Histories_ListAll(this.state.staticParam)
      .then(res => {
        this.setState({ list: JSON.parse(res.data.data).data });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  GetAll_KTKL = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    API_HR.Rewards_ListAll(this.state.staticParam)
      .then(res => {
        this.setState({ list: JSON.parse(res.data.data).data });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  GetAll_CCCN = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    API_HR.Certificates_ListAll(this.state.staticParam)
      .then(res => {
        this.setState({ list: JSON.parse(res.data.data).data });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  GetAll_GTGC = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    API_HR.FamilyDeductions_ListAll(this.state.staticParam)
      .then(res => {
        this.setState({ list: JSON.parse(res.data.data).data });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  sumPCSalaries(data) {
    if (data !== null && data !== '' && data !== undefined) {
      data.AllowPro =
        data.AllowPro != null && data.AllowPro > 0
          ? parseFloat(data.AllowPro)
          : 0;
      data.AllowRes =
        data.AllowRes != null && data.AllowRes > 0
          ? parseFloat(data.AllowRes)
          : 0;
      data.AllowEffective =
        data.AllowEffective != null && data.AllowEffective > 0
          ? parseFloat(data.AllowEffective)
          : 0;
      data.AllowSen =
        data.AllowSen != null && data.AllowSen > 0
          ? parseFloat(data.AllowSen)
          : 0;
      data.AllowEat =
        data.AllowEat != null && data.AllowEat > 0
          ? parseFloat(data.AllowEat)
          : 0;
      data.AllowTravel =
        data.AllowTravel != null && data.AllowTravel > 0
          ? parseFloat(data.AllowTravel)
          : 0;
      return (
        data.AllowPro +
        data.AllowRes +
        data.AllowEffective +
        data.AllowSen +
        data.AllowEat +
        data.AllowTravel
      );
    } else {
      return 0;
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
  },
  TabarApprove: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    width: 80,
  },
  ClickApprove: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    width: 80,
  },
  TabarApprove1: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    width: 100,
  },
  ClickApprove1: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    width: 100,
  },
  TabarApprove_BH: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 100,
  },
  ClickApprove_BH: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 100,
  },
  FormSize: {
    fontSize: 15,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EmpViewItemComponent);
