import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  TouchableOpacity,
  Linking,
  Dimensions,
} from 'react-native';
import { Header, Divider, Button, Input, SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';
import DatePicker from 'react-native-date-picker';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Back } from '@component';
import { API, } from '@network';
import { API_HR } from '@network';
import { FuncCommon } from '../../../utils';
import {
  Container,
  Content,
  List,
  Left,
  Right,
  Form,
  ListItem,
  Thumbnail,
  Body,
  Item,
  Label,
  Picker,
} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import { AppStyles, AppSizes, AppColors } from '@theme';
import MultiSelect_Component from '../../component/MultiSelect_Component';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Combobox from '../../component/Combobox';
import MenuSearchDate from '../../component/MenuSearchDate';
const SCREEN_WIDTH = Dimensions.get('window').width;
const dTypeOfRequest = [
  {
    value: 'OL',
    text: 'Nghỉ khác',
  },
  {
    value: 'EC',
    text: 'Hết hạn hợp đồng',
  },
  {
    value: 'PT',
    text: 'Thời vụ',
  },
  {
    text: 'Thai sản',
    value: 'ML',

  },
  {
    text: 'Thực tập sinh',
    value: 'TT',

  },
  {
    text: 'Cộng tác viên',
    value: 'CT',

  },
];
class EmployeesComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      ValueSearchDate: '1', // năm nay
      name: '',
      ChinhThuc: '',
      ThuViec: '',
      ThuViec: '',
      NghiViec: '',
      loading: false,
      refreshing: false,
      employeeData: [],
      bankDataItem: null,
      search: '',
      StatusOfWork: ["null", "OM", "PE", "TT", "PT", "CT", "ML", "OL", "LJ", "EC"],
      EvenFromSearch: false,
      StatusOfWorkData: [
        {
          id: 'OM',
          name: 'Nhân viên chính thức',
        },
        {
          id: 'PE',
          name: 'Nhân viên thử việc',
        },
        {
          id: 'PT',
          name: 'Nhân viên thời vụ',
        },
        {
          id: 'ML',
          name: 'Nghỉ thai sản',
        },
        {
          id: 'OL',
          name: 'Nghỉ khác',
        },
        {
          id: 'LJ',
          name: 'Nhân viên nghỉ việc',
        },
        {
          id: 'EC',
          name: 'Hết hạn hợp đồng',
        },
      ],
    };
    this._search = {
      StartDate: null,
      EndDate: null,
    };
    this.listtabbarBotom = [
      {
        Title: 'Tất cả',
        Icon: 'team',
        Type: 'antdesign',
        Value: ["null", "OM", "PE", "TT", "PT", "CT", "ML", "OL", "LJ", "EC"],
        Checkbox: true,
        Badge: 0,
      },
      {
        Title: 'Chính thức',
        Icon: 'user',
        Type: 'antdesign',
        Value: 'OM',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Thử việc',
        Icon: 'filetext1',
        Type: 'antdesign',
        Value: 'PE',
        Checkbox: false,
        Badge: 0,
      },

      {
        Title: 'Nghỉ việc',
        Icon: 'deleteuser',
        Type: 'antdesign',
        Value: 'LJ',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: '',
        Icon: 'dots-three-horizontal',
        Type: 'entypo',
        Value: 'combobox',
        Checkbox: false,
      },
    ];
  }
  componentDidMount = () => {
    this.getAllEmployeePage(1)
  }
  keyExtractor = (item, index) => index.toString();

  renderItem = ({ item }) => {
    return (
      <ListItem avatar
      // onPress={() => this.editItem(item)}
      >
        <Left>
          <Thumbnail style={{ width: 40, height: 40 }} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
        </Left>
        <Body>
          <View style={{ flexDirection: 'row', alignItems: "center" }}>
            <Text style={[AppStyles.Titledefault, { flex: 1 }]}>{item.FullName}</Text>
            <Text style={[AppStyles.Labeldefault, { paddingRight: 10 }]}>{item.Phone}</Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: "center" }}>
            <View style={{ flex: 1 }}>
              <Text style={[AppStyles.Textdefault]}>{item.EmployeeID}</Text>
              <View style={{ flexDirection: 'row', alignItems: "center" }}>
                <Text style={[AppStyles.Textdefault]}>{item.JobTitlesName + " - " || null} {item.DepartmentName || null}</Text>
              </View>
              {!item.WorkEmail ? null :
                <View style={{ flexDirection: 'row', alignItems: "center" }}>
                  <Text style={[AppStyles.Textdefault]}>{item.WorkEmail || null}</Text>
                </View>
              }
            </View>
            <View style={{ padding: 10 }}>
              {!item.Phone ? null :
                <TouchableOpacity onPress={() => {
                  Linking.openURL('tel:' + item.Phone);
                }} >
                  <Icon name="phone" style={{ fontSize: 25 }} color={AppColors.green} />
                </TouchableOpacity>
              }
            </View>
          </View>


        </Body>

      </ListItem >
    );
  };

  render() {
    let arrayholder = [];
    return (
      <Container>
        <TabBar
          title={'Danh bạ nhân sự'}
          FormSearch={true}
          CallbackFormSearch={callback =>
            this.setState({ EvenFromSearch: callback })
          }
          BackModuleByCode={'MyProfile'}
        />
        <View>
          {this.state.EvenFromSearch == true ? (
            <View style={{ flexDirection: 'column' }}>
              <SearchBar
                placeholder="Tìm kiếm..."
                lightTheme
                round
                inputContainerStyle={{ backgroundColor: '#e1ecf4' }}
                containerStyle={AppStyles.FormSearchBar}
                onChangeText={text => this.searchFilterFunction(text)}
                value={this.state.search}
              />
            </View>
          ) : null}
        </View>
        <FlatList
          ListEmptyComponent={this.ListEmpty}
          keyExtractor={this.keyExtractor}
          data={this.state.employeeData}
          refreshing={this.state.loading}
          renderItem={this.renderItem}
          onEndReached={this.handleLoadMore}
          onRefresh={this._onRefresh}
          ListFooterComponent={this.renderFooter.bind(this)}
          onEndReachedThreshold={0.4}
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              tintColor="#f5821f"
              titleColor="#fff"
              colors={['red', 'green', 'blue']}
            />
          }
        />

        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onClick(callback)}
          />
        </View>
        <Combobox
          TypeSelect={'single'} // single or multiple
          callback={this.onValueChange}
          data={dTypeOfRequest || []}
          eOpen={this.openCombobox}
          position={'bottom'}
          value={undefined}

        />
      </Container>
    );
  }
  ListEmpty = () => {
    if (this.state.employeeData && this.state.employeeData.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={[AppStyles.centerAligned, { marginTop: 10 }]}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  _openMenuSearchDate() { }
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  setStartDate = date => {
    this._search.StartDate = date;
    this.getAllEmployeePage(1);
  };
  setEndDate = date => {
    this._search.EndDate = date;
    this.getAllEmployeePage(1);
  };
  //#region
  onClick = data => {
    if (data == 'combobox') {
      this.onActionCombobox();
    } else {
      if (typeof data === 'string') {
        this.state.StatusOfWork = [data];
      } else {
        this.state.StatusOfWork = data;
      }
      this.setState({
        employeeData: [],
        StatusOfWork: this.state.StatusOfWork,
      });
      this.getAllEmployeePage(1);
    }
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.getAllEmployeePage(1);
    }
  };
  //#region  combobox Category
  _openCombobox() { }
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCombobox() {
    this._openCombobox();
  }
  onValueChange = data => {
    if (data.value !== null) {
      this.state.StatusOfWork = [data.value];
      this.setState({
        employeeData: [],
        StatusOfWork: this.state.StatusOfWork,
      });
      this.getAllEmployeePage(1, data.value);
    }

  };
  //#endregion
  _onRefresh = () => {
    this.getAllEmployeePage(1);
  };

  searchFilterFunction = text => {
    this.setState({ employeeData: [] });
    this.state.search = text;
    this.getAllEmployeePage(1);
  };
  onPressback() {
    Actions.app();
  }
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({ EvenFromSearch: true });
    } else {
      this.setState({ EvenFromSearch: false });
    }
  }
  //#endregion
  clickBack() {
    Actions.app();
  }
  onValueChangeGroup(value) {
    if (value.includes(',')) {
      let stamp = value.split(',');
      this.setState({
        StatusOfWork: stamp,
        employeeData: [],
      });
      this.state.StatusOfWork = stamp;
    } else {
      this.setState({
        StatusOfWork: value,
        employeeData: [],
      });
      this.state.StatusOfWork = value;
    }
    this.getAllEmployeePage(1);
  }
  StatusOfWorkData = () => {
    return this.state.StatusOfWorkData.map((x, i) => {
      return <Picker.Item label={x.text} key={i} value={x.value} />;
    });
  };
  editItem(item) {
    Actions.empviewitem({ EmployeeGuid: item.EmployeeGuid });
  }
  addItem() {
    Actions.additem({ EmployeeGuid: '' });
  }
  getAllEmployeePage = (NumberPage) => {
    let obj = {
      CurrentPage: NumberPage,
      Length: this.Length,
      Keyword: this.state.search,
      Status: this.state.StatusOfWork,
      lstDepartmentID: [],
      StartDate: this._search.StartDate,
      EndDate: this._search.EndDate,
    };
    this.setState({ loading: true });
    API_HR.getAllEmployeePage(obj)
      .then(response => {
        let listData = this.state.employeeData;
        let data1 = JSON.parse(response.data.data).data
        let data = []
        if (NumberPage === 1) {
          data = data1;
        } else {
          data = listData.concat(data1);
        }
        this.setState({ employeeData: data, loading: false });
        let TotalRow = JSON.parse(response.data.data).recordsFiltered;
        this.TotalRow = TotalRow;
      })
      .catch(error => {
        console.log(error)
        this.setState({ loading: false });
      });
  };

  handleLoadMore = () => {
    if (!this.state.loading && this.TotalRow > this.state.employeeData.length) {
      this.page = this.page + 1; // increase page by 1
      this.getAllEmployeePage(this.page); // method for API call
    }
  };
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return <Text style={{ color: '#000' }} />;
  };
}
const colors = {
  loginGreen: '#347F16',
};
const styles = StyleSheet.create({
  loginContainer: {
    marginTop: 10,
    marginEnd: 10,
    alignItems: 'flex-end',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EmployeesComponent);
