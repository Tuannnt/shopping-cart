import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TouchableHighlight,
  Text,
  Keyboard,
  ScrollView,
  Dimensions,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import {Icon, Header} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {API_HR} from '@network';
import API from '../../../network/API';
import {DatePicker, Container, Item, Label, Picker} from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import {WebView} from 'react-native-webview';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import HTML from 'react-native-render-html';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class openContracts extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      Data: [],
      ContractGuid: '',
    };
  }
  componentDidMount(): void {
    this.getItem();
  }
  getItem() {
    let id = {Id: this.props.ContractGuid};
    API_HR.Contracts_GetItem(id)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data).model,
        });
      })
      .catch(error => {
        ErrorHandler.handle(error.data);
      });
  }
  keyExtractor = (item, index) => index.toString();
  handleTypeContract = data => {
    if (data === 'N') return 'Không xác định thời hạn';
    if (data === 'Y') return 'Xác định thời hạn';
    if (data === 'T') return 'Hợp đồng thời vụ';
    if (data === 'K') return 'Khác';
    return '';
  };
  render() {
    return (
      <View style={{flex: 1}}>
        <TouchableWithoutFeedback
          style={{flex: 30}}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={{flex: 1, backgroundColor: 'white'}}>
            <TabBar_Title
              title={'Chi tiết hợp đồng'}
              callBack={() => Actions.pop()}
              FormAttachment={true}
              CallbackFormAttachment={() => this.openAttachments()}
            />
            <View style={{flex: 1, padding: 20}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>Số hợp đồng :</Text>
                <View style={{flex: 3, alignItems: 'flex-end'}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.Data.ContractNo}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Mức lương :</Text>
                </View>
                <View style={styles.FormALl}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.addPeriod(this.state.Data.Salary)}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Ngày ký hợp đồng :</Text>
                </View>
                <View style={styles.FormALl}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.customDate(this.state.Data.ContractDate)}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Hạn hợp đồng :</Text>
                </View>
                <View style={styles.FormALl}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.customDate(this.state.Data.ContractExpired)}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <Text style={AppStyles.Labeldefault}>Loại hợp đồng :</Text>
                <View style={{flex: 3, alignItems: 'flex-end'}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.handleTypeContract(this.state.Data.ContractType)}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Nội dung:</Text>
                </View>
              </View>
              <View style={{flex: 1}}>
                <ScrollView style={{flex: 1}}>
                  <TouchableHighlight>
                    <HTML
                      source={{html: this.state.Data.Body}}
                      contentWidth={DRIVER.width}
                    />
                  </TouchableHighlight>
                </ScrollView>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
  openAttachments() {
    var obj = {
      ModuleId: '6',
      RecordGuid: this.props.ContractGuid,
      notEdit: true,
    };
    Actions.attachmentComponent(obj);
  }
  onPressBack() {
    Actions.RegisterCars();
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  closeComment() {
    this.setState({isModalVisible: !this.state.isModalVisible});
  }
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    fontSize: 14,
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(openContracts);
