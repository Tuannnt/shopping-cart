import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    StyleSheet,
    TouchableWithoutFeedback,
    View,
    TextInput,
    Text,
    Keyboard,
    ScrollView,
    Alert,
    ToastAndroid,
    TouchableOpacity
} from 'react-native';
import { Icon, Header } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import { API_HR } from '@network';
import API from "../../../network/API";
import { DatePicker, Container, Item, Label, Picker } from 'native-base';
import TabBarBottom from "../../component/TabBarBottom";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from "../../component/TabBar_Title";
import _ from "lodash";
const ItemCode = {
    PERSONNEL: 0,
    SELL: 1,
    CUSTOMER: 2,
    PURCHASE: 3,
    ACCOUNTANT: 4,
    WAREHOUSE: 5,
    OPERATIONAL: 6,
    DOCUMENT: 7,
    MARKETING: 8,
    PROJECT: 9,
    MANUFACTURING: 10,
    PROPERTY: 11,
    ESTABLISH: 12,
    LOGOUT: 13,
};
class openInsurances extends Component {
    constructor(props) {
        super(props);
        this.page = 1;
        this.Length = 10;
        this.TotalRow = 0;
        this.state = {
            Data: [],
            InsuranceGuid: ''
        };
    }
    componentDidMount(): void {
        this.getItem();
    }
    getItem() {
        let id = { Id: this.props.InsuranceGuid };
        API_HR.Insurances_GetItem(id).then(res => {
            this.setState({
                Data: JSON.parse(res.data.data).model,
            });
            console.log('===========> error' + res.data.data);
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }
    keyExtractor = (item, index) => index.toString()
    render() {
        return (
            <Container>
                <TouchableWithoutFeedback style={{ flex: 30 }} onPress={() => {
                    Keyboard.dismiss()
                }}>
                    <View style={{ flex: 1, fontSize: 11, fontFamily: Fonts.base.family, backgroundColor: 'white' }}>
                        <TabBar_Title
                            title={'Chi tiết bảo hiểm'}
                            callBack={() => Actions.pop()}
                            FormAttachment={true}
                            CallbackFormAttachment={() => this.openAttachments()}
                        />
                        <View style={{ flex: 10 }}>
                            <View style={{ padding: 20 }}>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={AppStyles.Labeldefault}>Số sổ bảo hiểm :</Text>
                                    <View style={{ flex: 3, alignItems: 'flex-end' }}>
                                        <Text style={[AppStyles.Textdefault]}>{this.state.Data.InsuranceNumber}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={AppStyles.Labeldefault}>Số thẻ bảo hiểm :</Text>
                                    <View style={{ flex: 3, alignItems: 'flex-end' }}>
                                        <Text style={[AppStyles.Textdefault]}>{this.state.Data.NumberCardYear}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={AppStyles.Labeldefault}>Mức đóng bảo hiểm : </Text>
                                    </View>
                                    <View style={styles.FormALl}>
                                        <Text style={[AppStyles.Textdefault]}>{this.addPeriod(this.state.Data.InsuranceSalary)}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={AppStyles.Labeldefault}>Tỷ lệ bảo hiểm : </Text>
                                    </View>
                                    <View style={styles.FormALl}>
                                        <Text style={[AppStyles.Textdefault]}>{this.addPeriod(this.state.Data.EmployeeRate)}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={AppStyles.Labeldefault}>Ngày bắt đầu : </Text>
                                    </View>
                                    <View style={styles.FormALl}>
                                        <Text style={[AppStyles.Textdefault]}>{this.customDate(this.state.Data.StartDate)}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={AppStyles.Labeldefault}>Ngày kết thúc : </Text>
                                    </View>
                                    <View style={styles.FormALl}>
                                        <Text style={[AppStyles.Textdefault]}>{this.customDate(this.state.Data.EndDate)}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ flex: 2 }}>
                                        <Text style={AppStyles.Labeldefault}>Số tiền người lao động đóng : </Text>
                                    </View>
                                    <View style={styles.FormALl}>
                                        <Text style={[AppStyles.Textdefault]}>{this.addPeriod(this.state.Data.PaytByEmployee)}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                                    <View style={{ flex: 3, alignItems: 'flex-end' }}>
                                        <Text style={[AppStyles.Textdefault]}>{this.state.Data.IsActive == 1 ? 'Tham gia ' : this.state.Data.IsActive == 0 ? 'Không tham gia ' : ''}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10, height: '100%', width: '100%' }}>
                                    <Text style={AppStyles.Labeldefault}>Mô tả :</Text>
                                    <Text style={[AppStyles.Textdefault], { flex: 2 }}>{this.state.Data.Description}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Container >
        );
    }
    openAttachments() {
        var obj = {
            ModuleId: '7',
            RecordGuid: this.props.InsuranceGuid,
            notEdit: true
        }
        Actions.attachmentComponent(obj);
    }
    onPressBack() {
        Actions.RegisterCars();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    //định dạng tiền tệ
    addPeriod(nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    formatValue(value) {
        return Tolls.formatMoney(parseFloat(value) / 100, "$ ");
    }
    closeComment() {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }

}
const styles = StyleSheet.create({
    headerContainer: {
        width: '100%',
        padding: 20,
        alignSelf: 'baseline',
        borderBottomWidth: 1,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
    },
    avatarMargin: {
        marginLeft: 36,
    },
    Icon: {
        width: '10%',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 26,
        marginRight: 20
    },
    ViewFlatList: {
        flex: 1,
        flexDirection: 'row',
        padding: 15,
    },
    input: {
        margin: 10,
        height: 40,
        borderColor: '#C0C0C0',
        borderWidth: 1
    },
    textArea: {
        height: 80,
        justifyContent: "flex-start",
        borderColor: '#C0C0C0',
        borderWidth: 1,
        margin: 10,
    },
    containerContent: {
        flex: 20,
        flexDirection: "row",
        marginBottom: 50,
        backgroundColor: 'white',
        marginLeft: 0
    },
    FormALl: {
        marginLeft: 10,
        alignItems: 'flex-end',
        flex: 1
    }
});
const mapStateToProps = state => ({ Loginname: state.user.account });

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(openInsurances);

