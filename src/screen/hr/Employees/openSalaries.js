import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  ToastAndroid,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Icon, Header, Divider} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {API_HR} from '@network';
import API from '../../../network/API';
import {
  DatePicker,
  Container,
  Item,
  Label,
  Picker,
  ListItem,
  Body,
} from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';

class openSalaries extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      staticParam: {
        EmployeeGuid: '',
        NumberPage: 1,
        Length: 10000,
        search: {
          value: '',
        },
      },
      Data: [],
      list: [],
      SalaryGuid: '',
      EmployeeGuid: null,
      stylex: 1,
    };
  }
  componentDidMount(): void {
    this.getItem();
  }
  getItem() {
    let id = {Id: this.props.SalaryGuid};
    API_HR.Salaries_GetItem(id)
      .then(res => {
        // console.log("8888888 "+ JSON.parse(JSON.stringify(JSON.parse(res.data.data).model)).EmployeeGuid);
        this.setState({
          //staticParam: {EmployeeGuid : JSON.parse(res.data.data).model.EmployeeGuid},
          Data: JSON.parse(res.data.data).model,
        });
        console.log('===========> error' + res.data.data);
      })
      .catch(error => {
        ErrorHandler.handle(error.data);
      });
  }
  keyExtractor = (item, index) => index.toString();
  render() {
    return (
      <Container>
        <TouchableWithoutFeedback
          style={{flex: 30}}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View
            style={{
              flex: 1,
              fontSize: 11,
              fontFamily: Fonts.base.family,
              backgroundColor: 'white',
            }}>
            <TabBar_Title
              title={'Chi tiết lương'}
              callBack={() => Actions.pop()}
              FormAttachment={true}
              CallbackFormAttachment={() => this.openAttachments()}
            />
            <ScrollView>
              {/* <View
              style={{
                height: 40,
                flexDirection: 'row',
                paddingRight: 10,
                paddingLeft: 10,
              }}> */}
              {/* {this.state.stylex == 1 ? (
                <TouchableOpacity
                  onPress={() => this.onClick('LG')}
                  style={styles.ClickPending}>
                  <Text style={styles.FormSize}>Lương và phụ cấp</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.onClick('LG')}
                  style={styles.TabarPending}>
                  <Text style={styles.FormSize}>Lương và phụ cấp</Text>
                </TouchableOpacity>
              )} */}
              {/* {this.state.stylex == 2 ? (
                <TouchableOpacity
                  onPress={() => this.onClick('PC')}
                  style={styles.ClickApprove_BH}>
                  <Text style={styles.FormSize}>Phụ cấp khác</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.onClick('PC')}
                  style={styles.TabarApprove_BH}>
                  <Text style={styles.FormSize}>Phụ cấp khác</Text>
                </TouchableOpacity>
              )} */}
              {/* </View> */}
              {this.state.stylex == 1 ? (
                <View style={{flex: 10}}>
                  <View style={{padding: 15}}>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                      <Text style={AppStyles.Labeldefault}>Nhân viên : </Text>
                      <View style={{flex: 3, alignItems: 'flex-end'}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.EmployeeName}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Labeldefault]}>
                          Lương cơ bản :
                        </Text>
                      </View>
                      <View style={styles.FormALl}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.addPeriod(this.state.Data.BasicSalary)}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày áp dụng :
                        </Text>
                      </View>
                      <View style={styles.FormALl}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.customDate(this.state.Data.AppliedDate)}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                      <Text style={AppStyles.Labeldefault}>Trạng thái : </Text>
                      <View style={{flex: 3, alignItems: 'flex-end'}}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.Data.IsActive == 1
                            ? 'Áp dụng '
                            : this.state.Data.IsActive == 0
                            ? 'Không áp dụng'
                            : ''}
                        </Text>
                      </View>
                    </View>
                    {this.state.Data.AllowRes && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp trách nhiệm :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowRes)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowCar && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={[AppStyles.Labeldefault]}>
                            Phụ cấp xăng xe :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowCar)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowExperience && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp nặng nhọc :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowExperience)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowRegion && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp chuyên cần :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowRegion)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowPhone && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp điện thoại :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowPhone)}
                          </Text>
                        </View>
                      </View>
                    )}

                    {this.state.Data.AllowSen && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp thâm niên :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowSen)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowFamily && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp không phạm lỗi :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowFamily)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowIso && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp đo máy RVF :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowIso)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowTravel && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp lái xe ngoài :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowTravel)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowEat && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>Tiền ăn :</Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowEat)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.Allow5S && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp rửa hàng Inox :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.Allow5S)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowKaizen && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp trông xưởng :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowKaizen)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowSafty && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp căng thẳng :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowSafty)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowEnvironment && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp môi trường làm việc :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowEnvironment)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowPro && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp vị trí làm việc :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowPro)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowEffective && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp nhà trọ :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowEffective)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowCoefficient2 && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp ca đêm (%) :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowCoefficient2)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowDisadvantaged && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp độc hại (Hàn) :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowDisadvantaged)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowProject && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Lương hiệu suất :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowProject)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowWorking && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp bằng cấp :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowWorking)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowBirthDay && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp căng thẳng (CNC) :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowBirthDay)}
                          </Text>
                        </View>
                      </View>
                    )}
                    {this.state.Data.AllowCoefficient1 && (
                      <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp chạy thử máy :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(this.state.Data.AllowCoefficient1)}
                          </Text>
                        </View>
                      </View>
                    )}

                    <View style={{flexDirection: 'row', marginTop: 10}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Lương cơ bản (Thử việc) :
                        </Text>
                      </View>
                      <View style={styles.FormALl}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.addPeriod(this.state.Data.AllowAccident)}
                        </Text>
                      </View>
                    </View>
                    <Divider style={{marginBottom: 10, marginTop: 10}} />
                    <View style={{flexDirection: 'row', marginTop: 0}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Labeldefault}>
                          Tổng phụ cấp :
                        </Text>
                      </View>
                      <View style={styles.FormALl}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.addPeriod(this.sumPCSalaries(this.state.Data))}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              ) : null}
              {/* {this.state.stylex == 2 ?
                            <View style={{ flex: 1 }}>
                                <FlatList
                                    style={{ marginBottom: 20, height: '100%' }}
                                    refreshing={this.state.isFetching}
                                    ref={(ref) => {
                                        this.ListView_Ref = ref;
                                    }}
                                    ListEmptyComponent={this.ListEmpty}
                                    //ListHeaderComponent={this.renderHeader}
                                    ListFooterComponent={this.renderFooter}
                                    keyExtractor={item => item.AllowanceGuid}
                                    data={this.state.list}
                                    renderItem={({ item, index }) => (
                                        <ScrollView>
                                            <ListItem >
                                                <Body>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Text style={AppStyles.Labeldefault}>Tên phụ cấp :</Text>
                                                        <Text style={[AppStyles.Textdefault]}>{item.AllowanceName}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                        <View style={{ flex: 1 }}>
                                                            <Text style={AppStyles.Labeldefault}>Tiền phụ cấp </Text>
                                                        </View>
                                                        <View style={styles.FormALl}>
                                                            <Text style={[AppStyles.Textdefault]}>{this.addPeriod(item.Value)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                        <Text style={AppStyles.Labeldefault}>Loại phụ cấp :</Text>
                                                        <Text style={[AppStyles.Textdefault]}>{item.Type == 'True' ? 'Cố định ' : item.Type == 'False' ? 'Không cố định' : ''}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                        <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                                                        <Text style={[AppStyles.Textdefault]}>{item.IsActive == 'True' ? 'Sử dụng ' : item.IsActive == 'False' ? 'Không sử dụng' : ''}</Text>
                                                    </View>
                                                </Body>
                                            </ListItem>
                                        </ScrollView>
                                    )}
                                />
                            </View>
                            : null} */}
            </ScrollView>
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
  openAttachments() {
    var obj = {
      ModuleId: '8',
      RecordGuid: this.props.SalaryGuid,
      notEdit: true,
    };
    Actions.attachmentComponent(obj);
  }
  onClick(data) {
    if (data == 'PC') {
      this.setState({
        stylex: 2,
      });
      this.setState({list: []});
      this.GetAll_PC();
    } else if (data == 'LG') {
      this.setState({
        stylex: 1,
      });
    }
  }
  GetAll_PC = () => {
    this.state.staticParam.EmployeeGuid = this.state.Data.EmployeeGuid;
    API_HR.EmployeeAllowances_ListAll(this.state.staticParam)
      .then(res => {
        this.setState({list: JSON.parse(res.data.data).data});
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  ListEmpty = () => {
    if (this.state.list.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  onPressBack() {
    Actions.RegisterCars();
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  sumPCSalaries(data) {
    if (data !== null && data !== '' && data !== undefined) {
      data.AllowPro =
        data.AllowPro != null && data.AllowPro > 0
          ? parseFloat(data.AllowPro)
          : 0;
      data.AllowRes =
        data.AllowRes != null && data.AllowRes > 0
          ? parseFloat(data.AllowRes)
          : 0;
      data.AllowEffective =
        data.AllowEffective != null && data.AllowEffective > 0
          ? parseFloat(data.AllowEffective)
          : 0;
      data.AllowSen =
        data.AllowSen != null && data.AllowSen > 0
          ? parseFloat(data.AllowSen)
          : 0;
      data.AllowEat =
        data.AllowEat != null && data.AllowEat > 0
          ? parseFloat(data.AllowEat)
          : 0;
      data.AllowTravel =
        data.AllowTravel != null && data.AllowTravel > 0
          ? parseFloat(data.AllowTravel)
          : 0;
      data.AllowCar =
        data.AllowCar != null && data.AllowCar > 0
          ? parseFloat(data.AllowCar)
          : 0;
      data.AllowPhone =
        data.AllowPhone != null && data.AllowPhone > 0
          ? parseFloat(data.AllowPhone)
          : 0;
      data.AllowFamily =
        data.AllowFamily != null && data.AllowFamily > 0
          ? parseFloat(data.AllowFamily)
          : 0;
      data.AllowChild =
        data.AllowChild != null && data.AllowChild > 0
          ? parseFloat(data.AllowChild)
          : 0;
      data.AllowHouse =
        data.AllowHouse != null && data.AllowHouse > 0
          ? parseFloat(data.AllowHouse)
          : 0;
      data.AllowChildCare =
        data.AllowChildCare != null && data.AllowChildCare > 0
          ? parseFloat(data.AllowChildCare)
          : 0;
      data.AllowMarriage =
        data.AllowMarriage != null && data.AllowMarriage > 0
          ? parseFloat(data.AllowMarriage)
          : 0;
      data.AllowBirthDay =
        data.AllowBirthDay != null && data.AllowBirthDay > 0
          ? parseFloat(data.AllowBirthDay)
          : 0;
      data.Allow5S =
        data.Allow5S != null && data.Allow5S > 0 ? parseFloat(data.Allow5S) : 0;
      data.AllowIso =
        data.AllowIso != null && data.AllowIso > 0
          ? parseFloat(data.AllowIso)
          : 0;
      data.AllowKaizen =
        data.AllowKaizen != null && data.AllowKaizen > 0
          ? parseFloat(data.AllowKaizen)
          : 0;
      data.AllowSafty =
        data.AllowSafty != null && data.AllowSafty > 0
          ? parseFloat(data.AllowSafty)
          : 0;
      //   data.AllowAccident =
      //     data.AllowAccident != null && data.AllowAccident > 0
      //       ? parseFloat(data.AllowAccident)
      //       : 0;
      data.AllowDisadvantaged =
        data.AllowDisadvantaged != null && data.AllowDisadvantaged > 0
          ? parseFloat(data.AllowDisadvantaged)
          : 0;
      data.AllowWorking =
        data.AllowWorking != null && data.AllowWorking > 0
          ? parseFloat(data.AllowWorking)
          : 0;
      data.AllowProject =
        data.AllowProject != null && data.AllowProject > 0
          ? parseFloat(data.AllowProject)
          : 0;
      data.AllowLunch =
        data.AllowLunch != null && data.AllowLunch > 0
          ? parseFloat(data.AllowLunch)
          : 0;
      data.AllowCoefficient1 =
        data.AllowCoefficient1 != null && data.AllowCoefficient1 > 0
          ? parseFloat(data.AllowCoefficient1)
          : 0;
      //   data.AllowCoefficient2 =
      //     data.AllowCoefficient2 != null && data.AllowCoefficient2 > 0
      //       ? parseFloat(data.AllowCoefficient2)
      //       : 0;
      data.AllowCoefficient3 =
        data.AllowCoefficient3 != null && data.AllowCoefficient3 > 0
          ? parseFloat(data.AllowCoefficient3)
          : 0;
      data.AllowOther =
        data.AllowOther != null && data.AllowOther > 0
          ? parseFloat(data.AllowOther)
          : 0;
      data.AllowRegion =
        data.AllowRegion != null && data.AllowRegion > 0
          ? parseFloat(data.AllowRegion)
          : 0;
      data.AllowEnvironment =
        data.AllowEnvironment != null && data.AllowEnvironment > 0
          ? parseFloat(data.AllowEnvironment)
          : 0;
      data.AllowExperience =
        data.AllowExperience != null && data.AllowExperience > 0
          ? parseFloat(data.AllowExperience)
          : 0;
      return (
        data.AllowPro +
        data.AllowRes +
        data.AllowEffective +
        data.AllowSen +
        data.AllowEat +
        data.AllowTravel +
        data.AllowCar +
        data.AllowPhone +
        data.AllowFamily +
        data.AllowChild +
        data.AllowHouse +
        data.AllowChildCare +
        data.AllowMarriage +
        data.AllowBirthDay +
        data.Allow5S +
        data.AllowIso +
        data.AllowKaizen +
        data.AllowSafty +
        // data.AllowAccident +
        data.AllowDisadvantaged +
        data.AllowWorking +
        data.AllowProject +
        data.AllowLunch +
        data.AllowCoefficient1 +
        // data.AllowCoefficient2 +
        data.AllowCoefficient3 +
        data.AllowOther +
        data.AllowRegion +
        data.AllowEnvironment +
        data.AllowExperience
      );
    } else {
      return 0;
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  closeComment() {
    this.setState({isModalVisible: !this.state.isModalVisible});
  }
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove_BH: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  ClickApprove_BH: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(openSalaries);
