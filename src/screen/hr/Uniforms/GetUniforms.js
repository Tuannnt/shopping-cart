import React, {Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  Image,
  FlatList,
  Keyboard,
} from 'react-native';
import Fonts from '../../../theme/fonts';
// import Icon from "react-native-vector-icons/FontAwesome";
import {Actions, ActionConst} from 'react-native-router-flux';
import {Header, Divider, Input, SearchBar} from 'react-native-elements';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Icon,
  Left,
  Body,
  Right,
} from 'native-base';
import {connect} from 'react-redux';
import {Back} from '@component';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import DropDownItem from 'react-native-drop-down-item';
import TabBar_Title from '../../component/TabBar_Title';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
const IC_ARR_DOWN = require('@images/logo/ic_arr_down.png');
const IC_ARR_UP = require('@images/logo/ic_arr_up.png');

type Props = {};
class GetUniforms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
      status: null,
      EmployeeGuid: null,
      FullName: null,
      bankDataItem: null,
      loading: false,
      contents: [],
    };
  }
  componentDidMount(): void {
    console.log('222222 : ' + this.props.EmployeeGuid);
    if (this.props.EmployeeGuid != '' && this.props.EmployeeGuid != null) {
      this.GetEmployeeById(this.props.EmployeeGuid);
    } else {
      this.setState({loading: false});
    }
    this.getAllUniForm();
  }
  renderLeftComponent = () => {
    let leftIcon = 'ios-arrow-round-back';
    let leftIconSet = 'ionicon';
    return (
      <TouchableOpacity
        style={[
          styles.squareContainer,
          {alignItems: 'center', justifyContent: 'center'},
        ]}
        onPress={() => this.onPressback()}>
        <Icon name={leftIcon} type={leftIconSet} color="black" size={30} />
      </TouchableOpacity>
    );
  };
  renderRightComponent = () => {};
  renderTitle = () => {
    let title = 'Cấp phát,thu hồi đồ dùng';
    return (
      <View
        style={{
          alignItems: 'center',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text
          style={[
            AppStyles.headerStyle,
            {fontWeight: 'bold', marginLeft: 0, fontSize: 18, color: 'black'},
          ]}>
          {title}
        </Text>
      </View>
    );
  };
  onPressback() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  render() {
    return (
      <Container>
        <View style={{backgroundColor: '#fff'}}>
          <TabBar_Title
            title={'Cấp phát,thu hồi đồ dùng'}
            callBack={() => this.onPressback()}
          />
        </View>
        <Content style={{marginStart: 0, marginEnd: 0}}>
          <Content style={{marginStart: 15, marginEnd: 15}}>
            <View thumbnail style={{marginTop: 10}}>
              <Body style={{flex: 1, alignItems: 'center'}}>
                <Thumbnail
                  source={API_HR.GetPicApplyLeaves(this.state.EmployeeGuid)}
                />
                <Text>{this.state.FullName}</Text>
                <Text
                  style={([AppStyles.Textdefault], {color: 'blue'})}
                  onPress={() => {
                    Linking.openURL('tel:' + this.state.HomeMobile);
                  }}>
                  {this.state.HomeMobile}
                </Text>
              </Body>
            </View>
            <Divider style={{marginBottom: 10, marginTop: 10}} />
            <View style={{flex: 10}}>
              <View style={{padding: 5}}>
                <View style={{flexDirection: 'row', padding: 1}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Mã nhân viên :</Text>
                  </View>
                  <View style={{flex: 7, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.EmployeeId}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Chức vụ :</Text>
                  </View>
                  <View style={{flex: 7, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.JobTitleName}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                  </View>
                  <View style={{flex: 7, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.StatusOfWork}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={{fontSize: 16, fontWeight: 'bold', marginTop: 10}}>
                    Lịch sử bàn giao :{' '}
                  </Text>
                </View>
                <View style={{flex: 5}}>
                  {this.state.ListData && this.state.ListData.length > 0 ? (
                    <FlatList
                      refreshing={this.state.isFetching}
                      ref={ref => {
                        this.ListView_Ref = ref;
                      }}
                      data={this.state.ListData}
                      renderItem={({item, index}) => (
                        <View
                          style={{
                            fontSize: 14,
                            fontFamily: Fonts.base.family,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              padding: 1,
                              marginTop: 8,
                            }}>
                            <View style={{flex: 2}}>
                              <Text style={AppStyles.Labeldefault}>
                                Ngày bàn giao:
                              </Text>
                            </View>
                            <View style={{flex: 3, alignItems: 'flex-end'}}>
                              <Text style={[AppStyles.Textdefault]}>
                                {this.customDate(item.UniformDate)}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              padding: 1,
                              marginTop: 8,
                            }}>
                            <View style={{flex: 2}}>
                              <Text style={AppStyles.Labeldefault}>
                                Tên đồ dùng:
                              </Text>
                            </View>
                            <View style={{flex: 3, alignItems: 'flex-end'}}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.UniformTypeName}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              padding: 1,
                              marginTop: 8,
                            }}>
                            <View style={{flex: 2}}>
                              <Text style={AppStyles.Labeldefault}>
                                Số lượng:
                              </Text>
                            </View>
                            <View style={{flex: 3, alignItems: 'flex-end'}}>
                              <Text style={[AppStyles.Textdefault]}>
                                {this.addPeriod(item.UniformQuantity)}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              padding: 1,
                              marginTop: 8,
                            }}>
                            <View style={{flex: 2}}>
                              <Text style={AppStyles.Labeldefault}>
                                Trạng thái đồ dùng:
                              </Text>
                            </View>
                            <View style={{flex: 3, alignItems: 'flex-end'}}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.Status}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              padding: 1,
                              marginTop: 8,
                            }}>
                            <View style={{flex: 2}}>
                              <Text style={AppStyles.Labeldefault}>
                                Người cấp phát:
                              </Text>
                            </View>
                            <View style={{flex: 3, alignItems: 'flex-end'}}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.IssuedBy}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              padding: 1,
                              marginTop: 8,
                            }}>
                            <View style={{flex: 2}}>
                              <Text style={AppStyles.Labeldefault}>
                                Bên giao:
                              </Text>
                            </View>
                            <View style={{flex: 3, alignItems: 'flex-end'}}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.Receiver}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              padding: 1,
                              marginTop: 8,
                            }}>
                            <View style={{flex: 2}}>
                              <Text style={AppStyles.Labeldefault}>
                                Bên nhận:
                              </Text>
                            </View>
                            <View style={{flex: 3, alignItems: 'flex-end'}}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.EmployeeName}
                              </Text>
                            </View>
                          </View>
                          <Divider style={{marginBottom: 10, marginTop: 10}} />
                        </View>
                      )}
                    />
                  ) : (
                    <View style={styles.MainContainer}>
                      <Text style={{textAlign: 'center', marginTop: 10}}>
                        Không có dữ liệu.
                      </Text>
                    </View>
                  )}
                </View>
              </View>
            </View>
          </Content>
        </Content>
      </Container>
    );
  }
  GetEmployeeById = Id => {
    let obj = {
      EmployeeGuid: Id,
    };
    this.setState({loading: true});
    API_HR.GetEmployeeById(obj)
      .then(response => {
        let data = JSON.parse(JSON.parse(JSON.stringify(response.data)).data);
        console.log('===================: ' + data);
        if (data.StatusOfWork == 'OM') {
          data.StatusOfWork = 'Nhân viên chính';
        } else if (data.StatusOfWork == 'PE') {
          data.StatusOfWork = 'Thử việc';
        } else if (data.StatusOfWork == 'PT') {
          data.StatusOfWork = 'Part-time';
        } else if (data.StatusOfWork == 'ML') {
          data.StatusOfWork = 'Nghỉ thai sản';
        } else if (data.StatusOfWork == 'OL') {
          data.StatusOfWork = 'Nghỉ khác';
        } else if (data.StatusOfWork == 'LJ') {
          data.StatusOfWork = 'Nghỉ việc';
        }
        data.Gender =
          data.Gender == 'M' ? 'Nam' : data.Gender == 'F' ? 'Nữ' : null;
        data.Identification =
          data.Identification != null ? data.Identification : '';
        data.HomeMobile = data.HomeMobile != null ? data.HomeMobile : '';
        data.WorkEmail = data.WorkEmail != null ? data.WorkEmail : '';
        data.BankAccount = data.BankAccount != null ? data.BankAccount : '';
        data.BankName = data.BankName != null ? data.BankName : '';
        this.setState({
          EmployeeGuid: data.EmployeeGuid,
          EmployeeId: data.EmployeeId,
          FullName: data.FullName,
          CandidateName: data.CandidateName,
          DepartmentName: data.DepartmentName,
          Identification: data.Identification,
          IssueIddate: data.IssueIddate,
          LoginName: data.LoginName,
          JobTitleName: data.JobTitleName,
          ProfestionaName: data.ProfestionaName,
          CurrentAddress: data.CurrentAddress,
          PermanentAddress: data.PermanentAddress,
          HireDate: data.HireDate,
          Gender: data.Gender == 'M' ? 'Nam' : data.Gender == 'F' ? 'Nữ' : null,
          MaritalStatus: data.MaritalStatus,
          BirthDate: this.getParsedDate(data.BirthDate),
          SalaryMethod: data.SalaryMethod,
          EthnicName: data.EthnicName,
          HomePhone: data.HomePhone,
          HomeMobile: data.HomeMobile,
          HomeEmail: data.HomeEmail,
          WorkPhone: data.WorkPhone,
          WorkEmail: data.WorkEmail,
          EducationName: data.EducationName,
          TaxCode: data.TaxCode,
          BankName: data.BankName,
          BankAccount: data.BankAccount,
          StatusOfWork: data.StatusOfWork,
          EndDateWork: data.EndDateWork,
          StartDate: data.StartDate,
          ImagePath: data.ImagePath,
          PhotoTitle: data.ImagePath,
          // contents: [
          //     {
          //         title: 'Thông tin thêm',
          //         body: 'Ngày vào làm: ' + this.getParsedDate(data.StartDate) + '\n' + 'Số CMT: ' + data.Identification + '\n' + 'Giới tính: ' + data.Gender + '\n' + 'Số điện thoại: ' + data.HomeMobile + '\n' + 'Email: ' + data.WorkEmail + '\n' + 'Số tài khoản: ' + data.BankAccount + '\n' + 'Ngân hàng: ' + data.BankName
          //         ,
          //     }],
          loading: false,
        });
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  getAllUniForm() {
    let id = {EmployeeGuid: this.props.EmployeeGuid};
    API_HR.getViewUniforms(id)
      .then(res => {
        this.setState({
          ListData: JSON.parse(res.data.data),
          //listHistory : JSON.parse(res.data.data),
        });
        console.log('===========> error' + res.data.data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!
      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  bigBlue: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 15,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GetUniforms);
