import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  Linking,
} from 'react-native';
// import Icon from "react-native-vector-icons/FontAwesome";
import { Actions, ActionConst } from 'react-native-router-flux';
import { Header, Divider, Icon, SearchBar } from 'react-native-elements';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Left,
  Body,
} from 'native-base';
import { connect } from 'react-redux';
import { Back } from '@component';
import { API_HR, API } from '@network';
import { ErrorHandler } from '@error';
import DropDownItem from 'react-native-drop-down-item';
import { WebView } from 'react-native-webview';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Combobox from '../../component/Combobox';
import { FuncCommon } from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import { addPeriod } from '../../../utils/FuncCommon';
const IC_ARR_DOWN = require('@images/logo/ic_arr_down.png');
const IC_ARR_UP = require('@images/logo/ic_arr_up.png');
class ProfileComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      staticParam: {
        OrganizationGuid: '',
        EmployeeGuid: '',
        ContractType: '',
        NumberPage: 1,
        Length: 10000,
        search: {
          value: '',
        },
      },
      EmployeeId: null,
      status: null,
      EmployeeGuid: null,
      FullName: null,
      WorkPhone: null,
      bankDataItem: null,
      loading: true,
      contents: [],
      list: [],
      list_bh: [],
      list_lg: [],
      list_ct: [],
      list_ktkl: [],
      list_cccn: [],
      list_dt: [],
      list_gtgc: [],
      stylex: 1,
      dTypeOfRequest: [
        {
          value: 'CT',
          text: 'Công tác',
        },
        {
          value: 'KTKL',
          text: 'Khen thưởng - Kỷ luật',
        },
        {
          value: 'CCCN',
          text: 'Chứng chỉ',
        },
        {
          value: 'GTGC',
          text: 'Giảm trừ gia cảnh',
        },
      ],
    };
    this.listtabbarBotom = [
      {
        Title: 'Nhân sự',
        Icon: 'user',
        Type: 'antdesign',
        Value: 'NS',
        Checkbox: true,
      },
      {
        Title: 'Hợp đồng',
        Icon: 'filetext1',
        Type: 'antdesign',
        Value: 'HD',
        Checkbox: false,
      },
      {
        Title: 'Bảo hiểm',
        Icon: 'security',
        Type: 'materialicons',
        Value: 'BH',
        Checkbox: false,
      },
      {
        Title: 'Lương',
        Icon: 'logo-usd',
        Type: 'ionicon',
        Value: 'LG',
        Checkbox: false,
      },
      {
        Title: '',
        Icon: 'dots-three-horizontal',
        Type: 'entypo',
        Value: 'combobox',
        Checkbox: false,
      },
    ];
  }
  componentDidMount = () => {
    this.Information();
  };
  Information() {
    API.getProfile()
      .then(rs => {
        this.state.UserInformation = rs.data;
        this.GetEmployeeById(rs.data.EmployeeGuid);
      })
      .catch(error => {
        console.log('error:', error);
        Alert.alert('Thông báo', 'Có lỗi khi lấý thông tin người đăng nhập');
      });
  }
  onClick(data) {
    if (data == 'HD') {
      this.setState({
        stylex: 2,
      });
      this.setState({ list: [] });
      this.GetAll_HD();
    } else if (data == 'NS') {
      this.setState({
        stylex: 1,
      });
    } else if (data == 'BH') {
      this.setState({
        stylex: 3,
      });
      this.setState({ list_bh: [] });
      this.GetAll_BH();
    } else if (data == 'LG') {
      this.setState({
        stylex: 4,
      });
      this.setState({ list_lg: [] });
      this.GetAll_LG();
    } else if (data == 'combobox') {
      this.onActionCB();
    }
  }
  onValueChange = data => {
    if (data.value == 'CT') {
      this.setState({
        stylex: 5,
      });
      this.setState({ list_ct: [] });
      this.GetAll_CT();
    } else if (data.value == 'KTKL') {
      this.setState({
        stylex: 6,
      });
      this.setState({ list_ktkl: [] });
      this.GetAll_KTKL();
    } else if (data.value == 'CCCN') {
      this.setState({
        stylex: 7,
      });
      this.setState({ list_cccn: [] });
      this.GetAll_CCCN();
    } else if (data.value == 'DT') {
      this.setState({
        stylex: 8,
      });
      this.setState({ list_dt: [] });
      this.GetAll_DT();
    } else if (data.value == 'GTGC') {
      this.setState({
        stylex: 9,
      });
      this.setState({ list_gtgc: [] });
      this.GetAll_GTGC();
    }
  };
  _openCombobox() { }
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCB(item) {
    this._openCombobox();
  }
  onPressback() {
    Actions.pop();
  }

  handleStatusOfWork = status => {
    if (status == 'OM') {
      return 'Nhân viên chính thức';
    } else if (status == 'PE') {
      return 'Thử việc';
    } else if (status == 'PT') {
      return 'Part-time';
    } else if (status == 'ML') {
      return 'Nghỉ thai sản';
    } else if (status == 'OL') {
      return 'Nghỉ khác';
    } else if (status == 'LJ') {
      return 'Nghỉ việc';
    }
    return '';
  };

  helpReason = status => {
    if (status == '3637f306-49c0-481e-ac98-0825c13e501d') {
      return 'Hoàn thành tốt công việc được giao';
    } else if (status == 'ec892e51-70cc-4be9-b56c-2b5afdd5d98d') {
      return 'Phụ cấp ăn ngoài';
    } else if (status == '4a8ac1c6-8f09-4cb6-81a2-b815ed75ab71') {
      return 'Chi phí vận chuyển';
    } else if (status == 'ad723b48-d0a7-4937-9fa9-c8f3e31317ef') {
      return 'Có cải tiến trong 5S';
    }
    return '';
  };
  render() {
    console.log(this.state.stylex);
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Thông tin cá nhân'}
          callBack={() => this.onPressback()}
        />
        <View style={{ flex: 1 }}>
          {this.state.stylex == 1 ? (
            <ScrollView style={{ padding: 10, flex: 1 }}>
              <TouchableOpacity style={{ alignItems: 'center' }}>
                <Thumbnail
                  source={API_HR.GetPicApplyLeaves(this.state.EmployeeGuid)}
                />
                <Text>{this.state.FullName}</Text>
                <Text
                  style={([AppStyles.Textdefault], { color: 'blue' })}
                  onPress={() => {
                    Linking.openURL('tel:' + this.state.WorkPhone);
                  }}>
                  {this.state.WorkPhone}
                </Text>
              </TouchableOpacity>
              <Divider style={{ marginBottom: 10, marginTop: 10 }} />
              <TouchableOpacity thumbnail>
                <Text style={{ fontWeight: 'bold', paddingBottom: 12 }}>
                  Thông tin chung
                </Text>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Mã :</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.EmployeeId}
                    </Text>
                  </View>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày sinh:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.BirthDate}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Chức vụ:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.JobTitleName}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Tài khoản:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.LoginName}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Trạng thái:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.StatusOfWork}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <Divider style={{ marginBottom: 10, marginTop: 10 }} />
              <TouchableOpacity thumbnail>
                <Text style={{ fontWeight: 'bold', paddingBottom: 12 }}>
                  Thông tin thêm
                </Text>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày vào làm:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.getParsedDate(this.state.StartDate)}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Số CM:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Identification}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Giới tính:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Gender == 'M'
                        ? 'Nam'
                        : this.state.Gender == 'F'
                          ? 'Nữ'
                          : null}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Số điện thoại:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.WorkPhone}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Email:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.WorkEmail}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Số tài khoản:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.BankAccount}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <Text style={AppStyles.Labeldefault}>Ngân hàng:</Text>
                  <View style={{ flex: 3, alignItems: 'flex-end' }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.BankName}
                    </Text>
                  </View>
                </View>
                {/* <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Text style={AppStyles.Labeldefault}>Địa chỉ:</Text>
                  <View style={{flex: 3, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.CurrentAddress}
                    </Text>
                  </View>
                </View> */}
              </TouchableOpacity>
              <Divider style={{ marginBottom: 10, marginTop: 10 }} />
              <TouchableOpacity>
                <ScrollView style={{ alignSelf: 'stretch' }}>
                  {this.state.contents
                    ? this.state.contents.map((param, i) => {
                      return (
                        <DropDownItem
                          key={i}
                          style={{ marginBottom: 10 }}
                          contentVisible={false}
                          invisibleImage={IC_ARR_DOWN}
                          visibleImage={IC_ARR_UP}
                          header={
                            <View>
                              <Text
                                style={{
                                  fontWeight: 'bold',
                                  marginBottom: 15,
                                }}>
                                {param.title}
                              </Text>
                              <Divider />
                            </View>
                          }>
                          <View>
                            <Text>{param.body}</Text>
                          </View>
                        </DropDownItem>
                      );
                    })
                    : null}
                  <View style={{ height: 96 }} />
                </ScrollView>
              </TouchableOpacity>
            </ScrollView>
          ) : null}
          {this.state.stylex == 2 ? (
            <FlatList
              style={{ marginBottom: 20, height: '100%' }}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={item => item.ContractGuid}
              data={this.state.list}
              renderItem={({ item, index }) => (
                <ScrollView>
                  <ListItem onPress={() => this.openView(item.ContractGuid)}>
                    <Body style={{ width: '100%', height: '100%', flex: 1 }}>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Số hợp đồng :
                          </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.ContractNo}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Loại hợp đồng :
                          </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.handleTypeContract(item.ContractType)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>Ngày ký : </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.customDate(item.ContractDate)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Hạn hợp đồng :
                          </Text>
                        </View>
                        {item.ContractExpired ? (
                          <View style={{ flex: 2 }}>
                            <Text style={[AppStyles.Textdefault]}>
                              {this.customDate(item.ContractExpired)}
                            </Text>
                          </View>
                        ) : null}
                      </View>
                    </Body>
                  </ListItem>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          ) : null}
          {this.state.stylex == 3 ? (
            <FlatList
              style={{ marginBottom: 20, height: '100%' }}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={item => item.InsuranceGuid}
              data={this.state.list}
              renderItem={({ item, index }) => (
                <ScrollView>
                  <ListItem
                    onPress={() => this.openInsurances(item.InsuranceGuid)}>
                    <Body>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <Text style={AppStyles.Labeldefault}>
                          Số sổ bảo hiểm :
                        </Text>
                        <View style={{ flex: 3, alignItems: 'flex-end' }}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.InsuranceNumber}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Mức đóng bảo hiểm :{' '}
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.InsuranceSalary)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Tỷ lệ bảo hiểm :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.EmployeeRate)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Ngày bắt đầu :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.customDate(item.StartDate)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Ngày kết thúc :
                          </Text>
                        </View>
                        {item.EndDate ? (
                          <View style={styles.FormALl}>
                            <Text style={[AppStyles.Textdefault]}>
                              {this.customDate(item.EndDate)}
                            </Text>
                          </View>
                        ) : null}
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 2 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Tiền người lao động đóng :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.PaytByEmployee)}
                          </Text>
                        </View>
                      </View>
                    </Body>
                  </ListItem>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          ) : null}
          {this.state.stylex == 4 ? (
            <FlatList
              style={{ marginBottom: 20, height: '100%' }}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={item => item.SalaryGuid}
              data={this.state.list}
              renderItem={({ item, index }) => (
                <ScrollView>
                  <ListItem>
                    <Body>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Lương cơ bản :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.BasicSalary)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Ngày áp dụng :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.customDate(item.AppliedDate)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                        <View style={{ flex: 3, alignItems: 'flex-end' }}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.IsActive == 'True'
                              ? 'Áp dụng '
                              : item.IsActive == 'False'
                                ? 'Không áp dụng'
                                : ''}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp trách nhiệm :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowRes)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp điện thoại :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowPhone)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp tiền ăn trưa :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowLunch)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>Gủi xe :</Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowCoefficient1)}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>Xăng xe :</Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowCar)}
                          </Text>
                        </View>
                      </View>
                      {/* <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp ca đêm (%) :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowCoefficient2)}
                          </Text>
                        </View>
                      </View> */}
                      <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp làm việc :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowWorking)}
                          </Text>
                        </View>
                      </View>
                      {/* <View style={{flexDirection: 'row', padding: 5}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp công tác:
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowWorking)}
                          </Text>
                        </View>
                      </View> */}
                      {/* <View style={{flexDirection: 'row', padding: 5}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp môi trường:
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowEnvironment)}
                          </Text>
                        </View>
                      </View> */}
                      {/* <View style={{flexDirection: 'row', padding: 5}}>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Labeldefault}>
                            Phụ cấp kiêm nhiệm:
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(item.AllowExperience)}
                          </Text>
                        </View>
                      </View> */}
                      <Divider style={{ marginBottom: 3, marginTop: 3 }} />
                      <View
                        style={{
                          flexDirection: 'row',
                          marginTop: 3,
                          padding: 5,
                        }}>
                        <View style={{ flex: 1 }}>
                          <Text style={AppStyles.Labeldefault}>
                            Tổng phụ cấp :
                          </Text>
                        </View>
                        <View style={styles.FormALl}>
                          <Text style={[AppStyles.Textdefault]}>
                            {FuncCommon.addPeriod(this.sumPCSalaries(item))}
                          </Text>
                        </View>
                      </View>
                    </Body>
                  </ListItem>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          ) : null}
          {this.state.stylex == 5 ? (
            <FlatList
              style={{ marginBottom: 20, height: '100%' }}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={item => item.BankGuid}
              data={this.state.list}
              renderItem={({ item, index }) => (
                <ScrollView style={{}}>
                  <View>
                    <ListItem
                      onPress={() => {
                        return;
                        this.openHistories(item.RowGuid);
                      }}>
                      <Body>
                        <View style={{ flex: 1 }}>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Chức vụ:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {item.JobTitleName}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Phòng ban:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {item.DepartmentName}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Ngày bắt đầu:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {item.StartDateApplyString}
                            </Text>
                          </View>
                          {item.DateApplyString ? (
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                                Thời gian kết thúc:
                              </Text>
                              <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                                {item.DateApplyString}
                              </Text>
                            </View>
                          ) : null}
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Trạng thái:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {this.handleStatusOfWork(item.StatusOfWork)}
                            </Text>
                          </View>
                        </View>
                      </Body>
                    </ListItem>
                  </View>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          ) : null}
          {this.state.stylex == 6 ? (
            <FlatList
              style={{ marginBottom: 20, height: '100%' }}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={item => item.BankGuid}
              data={this.state.list}
              renderItem={({ item, index }) => (
                <ScrollView style={{}}>
                  <View>
                    <ListItem onPress={() => this.openRewards(item.RewardGuid)}>
                      <Body>
                        <View style={{ flex: 1 }}>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Titledefault, { flex: 1 }]}>
                              {item.Title}
                            </Text>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.Date}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Hình thức:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {item.Type == 'B'
                                ? 'Khen thưởng'
                                : item.Type == 'D'
                                  ? 'Kỉ luật'
                                  : null}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Mức độ:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {item.RewardLevel}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Lý do:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {this.helpReason(item.Reason)}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Số tiền:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {FuncCommon.addPeriod(item.AmountOfMoney)}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Mô tả:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {item.Description}
                            </Text>
                          </View>
                        </View>
                      </Body>
                    </ListItem>
                  </View>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          ) : null}
          {this.state.stylex == 7 ? (
            <FlatList
              style={{ marginBottom: 20, height: '100%' }}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={item => item.BankGuid}
              data={this.state.list}
              renderItem={({ item, index }) => (
                <ScrollView style={{}}>
                  <View>
                    <ListItem
                      onPress={() =>
                        this.openCertificates(item.CertificateGuid)
                      }>
                      <Body>
                        <View style={{ flex: 1 }}>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Titledefault, { flex: 1 }]}>
                              {item.Title}
                            </Text>
                            <Text style={[AppStyles.Textdefault]}>
                              {this.customDate(item.IssueDate)}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Số cấp:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {item.CertificateId}
                            </Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
                              Đơn vị cấp:
                            </Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>
                              {item.IssueBy}
                            </Text>
                          </View>
                        </View>
                      </Body>
                    </ListItem>
                  </View>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          ) : null}
          {this.state.stylex == 9 ? (
            <FlatList
              style={{ marginBottom: 20, height: '100%' }}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={item => item.BankGuid}
              data={this.state.list}
              renderItem={({ item, index }) => (
                <ScrollView style={{}}>
                  <View>
                    <ListItem>
                      <Body>
                        <View style={{ flexDirection: 'row', padding: 5 }}>
                          <View style={{ flex: 1 }}>
                            <Text style={AppStyles.Labeldefault}>
                              Ngày áp dụng :{' '}
                            </Text>
                          </View>
                          <View style={styles.FormALl}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.Date && this.customDate(item.Date)}
                            </Text>
                          </View>
                        </View>
                        <View style={{ flexDirection: 'row', padding: 5 }}>
                          <View style={{ flex: 1 }}>
                            <Text style={AppStyles.Labeldefault}>
                              Số người phụ thuộc :{' '}
                            </Text>
                          </View>
                          <View style={styles.FormALl}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.Number}
                            </Text>
                          </View>
                        </View>
                        {/* <View style={{ flexDirection: 'row', padding: 5 }}>
                          <View style={{ flex: 1 }}>
                            <Text style={AppStyles.Labeldefault}>
                              QH với người nộp thuế :{' '}
                            </Text>
                          </View>
                          <View style={styles.FormALl}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.Relationship}
                            </Text>
                          </View>
                        </View> */}
                        <View style={{ flexDirection: 'row', padding: 5 }}>
                          <View style={{ flex: 1 }}>
                            <Text style={AppStyles.Labeldefault}>
                              Số tiền được giảm trừ :{' '}
                            </Text>
                          </View>
                          <View style={styles.FormALl}>
                            <Text style={[AppStyles.Textdefault]}>
                              {FuncCommon.addPeriod(item.Amount)}
                            </Text>
                          </View>
                        </View>
                      </Body>
                    </ListItem>
                  </View>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          ) : null}
        </View>
        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onClick(callback)}
          />
        </View>
        <Combobox
          TypeSelect={'single'} // single or multiple
          callback={this.onValueChange}
          data={this.state.dTypeOfRequest}
          eOpen={this.openCombobox}
          position={'bottom'}
        // value={'NS'}
        />
      </View>
    );
  }
  openView(id) {
    Actions.openContracts({ ContractGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openInsurances(id) {
    Actions.openInsurances({ InsuranceGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openSalaries(id) {
    Actions.openSalaries({ SalaryGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openHistories(id) {
    Actions.openHistories({ HistoryGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openRewards(id) {
    // Actions.openRewards({ RewardGuid: id });
    console.log('===================chuyen Id:' + id);
  }
  openCertificates(id) {
    Actions.openCertificates({ CertificateGuid: id });
    console.log('===================chuyen Id:' + id);
  }

  ListEmpty = () => {
    if (this.state.list.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  handleTypeContract = data => {
    if (data === 'N') {
      return 'Không xác định thời hạn';
    }
    if (data === 'Y') {
      return 'Xác định thời hạn 12 tháng';
    }
    if (data === 'M') {
      return 'Xác định thời hạn 36 tháng';
    }
    if (data === 'D') {
      return 'Đào tạo nghề';
    }
    if (data === 'V') {
      return 'Vụ việc';
    }
    if (data === 'T') {
      return 'Thử việc';
    }
    if (data === 'K') {
      return 'Người lao động cao tuổi';
    }
    return '';
  };
  //kiểm tra độ dài tin nhắn
  checkMessage(str, i) {
    if (str !== null && str !== '') {
      var splitted = str.split(' ');
      var strreturn = '';
      var j = 0;
      for (var a = 0; a < splitted.length; a++) {
        j = j + 1;
        if (j == i) {
          strreturn = strreturn + ' ' + splitted[a] + '....';
          break;
        } else {
          strreturn = strreturn + ' ' + splitted[a];
        }
      }
      return strreturn.trim();
    } else {
      return 'Nhóm vừa khởi tạo';
    }
  }
  GetEmployeeById = Id => {
    let obj = {
      EmployeeGuid: Id,
    };
    API_HR.GetEmployeeById(obj)
      .then(response => {
        let data = JSON.parse(JSON.parse(JSON.stringify(response.data)).data);
        console.log('===================: ' + data);
        if (data.StatusOfWork == 'OM') {
          data.StatusOfWork = 'Nhân viên chính thức';
        } else if (data.StatusOfWork == 'PE') {
          data.StatusOfWork = 'Thử việc';
        } else if (data.StatusOfWork == 'PT') {
          data.StatusOfWork = 'Part-time';
        } else if (data.StatusOfWork == 'ML') {
          data.StatusOfWork = 'Nghỉ thai sản';
        } else if (data.StatusOfWork == 'OL') {
          data.StatusOfWork = 'Nghỉ khác';
        } else if (data.StatusOfWork == 'LJ') {
          data.StatusOfWork = 'Nghỉ việc';
        }
        data.Gender = data.Gender != null ? data.Gender : '';
        data.Identification =
          data.Identification != null ? data.Identification : '';
        data.WorkPhone = data.WorkPhone != null ? data.WorkPhone : '';
        data.WorkEmail = data.WorkEmail != null ? data.WorkEmail : '';
        data.BankAccount = data.BankAccount != null ? data.BankAccount : '';
        data.BankName = data.BankName != null ? data.BankName : '';
        this.setState({
          EmployeeGuid: data.EmployeeGuid,
          EmployeeId: data.EmployeeId,
          FullName: data.FullName,
          CandidateName: data.CandidateName,
          DepartmentName: data.DepartmentName,
          Identification: data.Identification,
          IssueIddate: data.IssueIddate,
          LoginName: data.LoginName,
          JobTitleName: data.JobTitleName,
          ProfestionaName: data.ProfestionaName,
          CurrentAddress: data.CurrentAddress,
          PermanentAddress: data.PermanentAddress,
          HireDate: data.HireDate,
          Gender: data.Gender,
          MaritalStatus: data.MaritalStatus,
          BirthDate: this.getParsedDate(data.BirthDate),
          SalaryMethod: data.SalaryMethod,
          EthnicName: data.EthnicName,
          HomePhone: data.HomePhone,
          WorkPhone: data.WorkPhone,
          HomeEmail: data.HomeEmail,
          WorkPhone: data.WorkPhone,
          WorkEmail: data.WorkEmail,
          EducationName: data.EducationName,
          TaxCode: data.TaxCode,
          BankName: data.BankName,
          BankAccount: data.BankAccount,
          StatusOfWork: data.StatusOfWork,
          EndDateWork: data.EndDateWork,
          StartDate: data.StartDate,
          ImagePath: data.ImagePath,
          PhotoTitle: data.ImagePath,
          // contents: [
          //     {
          //         title: 'Thông tin thêm',
          //         body: 'Ngày vào làm: ' + this.getParsedDate(data.StartDate) + '\n' + 'Số CMT: ' + data.Identification + '\n' + 'Giới tính: ' + data.Gender + '\n' + 'Số điện thoại: ' + data.WorkPhone + '\n' + 'Email: ' + data.WorkEmail + '\n' + 'Số tài khoản: ' + data.BankAccount + '\n' + 'Ngân hàng: ' + data.BankName
          //         ,
          //     }],
          loading: false,
        });
      })
      .catch(error => {
        this.setState({ loading: false });
        ErrorHandler.handle(error.data);
      });
  };
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  GetAll_HD = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    this.setState({ loading: true }, () => {
      API_HR.Contracts_ListAll(this.state.staticParam)
        .then(res => {
          this.setState({ list: JSON.parse(res.data.data).data, loading: false });
        })
        .catch(error => {
          console.log('===========> error');
          console.log(error);
          this.setState({ loading: false });
        });
    });
  };
  GetAll_BH = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    this.setState({ loading: true }, () => {
      API_HR.Insurances_ListAll(this.state.staticParam)
        .then(res => {
          this.setState({ list: JSON.parse(res.data.data).data, loading: false });
        })
        .catch(error => {
          console.log('===========> error');
          console.log(error);
          this.setState({ loading: false });
        });
    });
  };
  GetAll_LG = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    this.setState({ loading: true }, () => {
      API_HR.Salaries_ListAll(this.state.staticParam)
        .then(res => {
          this.setState({ list: JSON.parse(res.data.data).data, loading: false });
        })
        .catch(error => {
          console.log('===========> error');
          console.log(error);
          this.setState({ loading: false });
        });
    });
  };
  GetAll_CT = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    this.setState({ loading: true }, () => {
      API_HR.Histories_ListAll(this.state.staticParam)
        .then(res => {
          this.setState({
            list: JSON.parse(res.data.data),
            loading: false,
          });
        })
        .catch(error => {
          console.log('===========> error');
          console.log(error);
          this.setState({
            loading: false,
          });
        });
    });
  };
  GetAll_KTKL = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    this.setState({ loading: true }, () => {
      API_HR.Rewards_ListAll(this.state.staticParam)
        .then(res => {
          const data = JSON.parse(res.data.data).data;
          this.setState({ list: data, loading: false });
        })
        .catch(error => {
          console.log('===========> error');
          console.log(error);
          this.setState({ loading: false });
        });
    });
  };
  GetAll_CCCN = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    this.setState({ loading: true }, () => {
      API_HR.Certificates_ListAll(this.state.staticParam)
        .then(res => {
          this.setState({ list: JSON.parse(res.data.data).data, loading: false });
        })
        .catch(error => {
          console.log('===========> error');
          console.log(error);
          this.setState({ loading: false });
        });
    });
  };
  GetAll_GTGC = () => {
    this.state.staticParam.EmployeeGuid = this.state.EmployeeGuid;
    this.setState({ loading: true }, () => {
      API_HR.FamilyDeductions_ListAll(this.state.staticParam)
        .then(res => {
          this.setState({ list: JSON.parse(res.data.data).data, loading: false });
        })
        .catch(error => {
          console.log('===========> error');
          console.log(error);
          this.setState({ loading: false });
        });
    });
  };
  sumPCSalaries(data) {
    if (data !== null && data !== '' && data !== undefined) {
      data.AllowPro =
        data.AllowPro != null && data.AllowPro > 0
          ? parseFloat(data.AllowPro)
          : 0;
      data.AllowRes =
        data.AllowRes != null && data.AllowRes > 0
          ? parseFloat(data.AllowRes)
          : 0;
      data.AllowEffective =
        data.AllowEffective != null && data.AllowEffective > 0
          ? parseFloat(data.AllowEffective)
          : 0;
      data.AllowSen =
        data.AllowSen != null && data.AllowSen > 0
          ? parseFloat(data.AllowSen)
          : 0;
      data.AllowEat =
        data.AllowEat != null && data.AllowEat > 0
          ? parseFloat(data.AllowEat)
          : 0;
      data.AllowTravel =
        data.AllowTravel != null && data.AllowTravel > 0
          ? parseFloat(data.AllowTravel)
          : 0;
      data.AllowCar =
        data.AllowCar != null && data.AllowCar > 0
          ? parseFloat(data.AllowCar)
          : 0;
      data.AllowPhone =
        data.AllowPhone != null && data.AllowPhone > 0
          ? parseFloat(data.AllowPhone)
          : 0;
      data.AllowFamily =
        data.AllowFamily != null && data.AllowFamily > 0
          ? parseFloat(data.AllowFamily)
          : 0;
      data.AllowChild =
        data.AllowChild != null && data.AllowChild > 0
          ? parseFloat(data.AllowChild)
          : 0;
      data.AllowHouse =
        data.AllowHouse != null && data.AllowHouse > 0
          ? parseFloat(data.AllowHouse)
          : 0;
      data.AllowChildCare =
        data.AllowChildCare != null && data.AllowChildCare > 0
          ? parseFloat(data.AllowChildCare)
          : 0;
      data.AllowMarriage =
        data.AllowMarriage != null && data.AllowMarriage > 0
          ? parseFloat(data.AllowMarriage)
          : 0;
      data.AllowBirthDay =
        data.AllowBirthDay != null && data.AllowBirthDay > 0
          ? parseFloat(data.AllowBirthDay)
          : 0;
      data.Allow5S =
        data.Allow5S != null && data.Allow5S > 0 ? parseFloat(data.Allow5S) : 0;
      data.AllowIso =
        data.AllowIso != null && data.AllowIso > 0
          ? parseFloat(data.AllowIso)
          : 0;
      data.AllowKaizen =
        data.AllowKaizen != null && data.AllowKaizen > 0
          ? parseFloat(data.AllowKaizen)
          : 0;
      data.AllowSafty =
        data.AllowSafty != null && data.AllowSafty > 0
          ? parseFloat(data.AllowSafty)
          : 0;
      data.AllowAccident =
        data.AllowAccident != null && data.AllowAccident > 0
          ? parseFloat(data.AllowAccident)
          : 0;
      data.AllowDisadvantaged =
        data.AllowDisadvantaged != null && data.AllowDisadvantaged > 0
          ? parseFloat(data.AllowDisadvantaged)
          : 0;
      data.AllowWorking =
        data.AllowWorking != null && data.AllowWorking > 0
          ? parseFloat(data.AllowWorking)
          : 0;
      data.AllowProject =
        data.AllowProject != null && data.AllowProject > 0
          ? parseFloat(data.AllowProject)
          : 0;
      data.AllowLunch =
        data.AllowLunch != null && data.AllowLunch > 0
          ? parseFloat(data.AllowLunch)
          : 0;
      data.AllowCoefficient1 =
        data.AllowCoefficient1 != null && data.AllowCoefficient1 > 0
          ? parseFloat(data.AllowCoefficient1)
          : 0;
      data.AllowCoefficient2 =
        data.AllowCoefficient2 != null && data.AllowCoefficient2 > 0
          ? parseFloat(data.AllowCoefficient2)
          : 0;
      data.AllowCoefficient3 =
        data.AllowCoefficient3 != null && data.AllowCoefficient3 > 0
          ? parseFloat(data.AllowCoefficient3)
          : 0;
      data.AllowOther =
        data.AllowOther != null && data.AllowOther > 0
          ? parseFloat(data.AllowOther)
          : 0;
      data.AllowRegion =
        data.AllowRegion != null && data.AllowRegion > 0
          ? parseFloat(data.AllowRegion)
          : 0;
      data.AllowEnvironment =
        data.AllowEnvironment != null && data.AllowEnvironment > 0
          ? parseFloat(data.AllowEnvironment)
          : 0;
      data.AllowExperience =
        data.AllowExperience != null && data.AllowExperience > 0
          ? parseFloat(data.AllowExperience)
          : 0;
      return (
        data.AllowPro +
        data.AllowRes +
        data.AllowEffective +
        data.AllowSen +
        data.AllowEat +
        data.AllowTravel +
        data.AllowCar +
        data.AllowPhone +
        data.AllowFamily +
        data.AllowChild +
        data.AllowHouse +
        data.AllowChildCare +
        data.AllowMarriage +
        data.AllowBirthDay +
        data.Allow5S +
        data.AllowIso +
        data.AllowKaizen +
        data.AllowSafty +
        data.AllowAccident +
        data.AllowDisadvantaged +
        data.AllowWorking +
        data.AllowProject +
        data.AllowLunch +
        data.AllowCoefficient1 +
        data.AllowCoefficient2 +
        data.AllowCoefficient3 +
        data.AllowOther +
        data.AllowRegion +
        data.AllowEnvironment +
        data.AllowExperience
      );
    } else {
      return 0;
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
  },
  TabarApprove: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    width: 80,
  },
  ClickApprove: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    width: 80,
  },
  TabarApprove1: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    width: 100,
  },
  ClickApprove1: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    width: 100,
  },
  TabarApprove_BH: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 100,
  },
  ClickApprove_BH: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 100,
  },
  FormSize: {
    fontSize: 15,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileComponent);
