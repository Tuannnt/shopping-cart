import React, {Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  Image,
  Keyboard,
} from 'react-native';
// import Icon from "react-native-vector-icons/FontAwesome";
import {Actions, ActionConst} from 'react-native-router-flux';
import {Header, Divider, Input, SearchBar} from 'react-native-elements';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Icon,
  Left,
  Body,
  Right,
} from 'native-base';
import {connect} from 'react-redux';
import {Back} from '@component';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import DropDownItem from 'react-native-drop-down-item';
import TabBar_Title from '../../component/TabBar_Title';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
const IC_ARR_DOWN = require('@images/logo/ic_arr_down.png');
const IC_ARR_UP = require('@images/logo/ic_arr_up.png');

type Props = {};
class GetViewSalary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: {},
      status: null,
      EmployeeGuid: null,
      FullName: null,
      bankDataItem: null,
      loading: false,
      contents: [],
    };
  }
  componentDidMount(): void {
    console.log('55555555 : ' + this.props.EmployeeGuid);
    if (this.props.EmployeeGuid != '' && this.props.EmployeeGuid != null) {
      this.GetEmployeeById(this.props.EmployeeGuid);
    } else {
      this.setState({loading: false});
    }
  }
  renderLeftComponent = () => {
    let leftIcon = 'ios-arrow-round-back';
    let leftIconSet = 'ionicon';
    return (
      <TouchableOpacity
        style={[
          styles.squareContainer,
          {alignItems: 'center', justifyContent: 'center'},
        ]}
        onPress={() => this.onPressback()}>
        <Icon name={leftIcon} type={leftIconSet} color="black" size={30} />
      </TouchableOpacity>
    );
  };
  renderRightComponent = () => {};
  renderTitle = () => {
    let title = 'Thông tin chi tiết bảng lương';
    return (
      <View
        style={{
          alignItems: 'center',
          flex: 1,
          flexDirection: 'row',
        }}>
        <Text
          style={[
            AppStyles.headerStyle,
            {fontWeight: 'bold', marginLeft: 0, fontSize: 18, color: 'black'},
          ]}>
          {title}
        </Text>
      </View>
    );
  };
  onPressback() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  render() {
    return (
      <Container>
        <TabBar_Title
          title={'Thông tin chi tiết bảng lương'}
          callBack={() => this.onPressback()}
        />
        <Content style={{marginStart: 0, marginEnd: 0}}>
          <Content style={{marginStart: 15, marginEnd: 15}}>
            <View thumbnail style={{marginTop: 10}}>
              <Body style={{flex: 1, alignItems: 'center'}}>
                <Thumbnail
                  source={API_HR.GetPicApplyLeaves(
                    this.state.Data?.EmployeeGuid,
                  )}
                />
                <Text numberOfLines={1}>{this.state.Data?.EmployeeName}</Text>
              </Body>
            </View>
            <Divider style={{marginBottom: 10, marginTop: 10}} />
            <View style={{flex: 10}}>
              <View style={{padding: 5}}>
                <View style={{flexDirection: 'row', padding: 1}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Mã nhân viên :</Text>
                  </View>
                  <View style={{flex: 7, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data?.EmployeeId}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Bộ phận :</Text>
                  </View>
                  <View style={{flex: 7, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data?.DepartmentName}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Chức danh :</Text>
                  </View>
                  <View style={{flex: 7, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data?.JobTitleName}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Cấp bậc :</Text>
                  </View>
                  <View style={{flex: 7, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data?.RankId}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Công đủ :</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data?.Workday}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày nghỉ không lương:
                    </Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.NgayNghiKL)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày nghỉ nguyên lương:
                    </Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.NgayNghiNL)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Phép còn lại:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.PhepConLai)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontSize: 19, fontWeight: 'bold'}}>
                    Lương và phụ cấp tháng {this.state.Data?.Thang} :{' '}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Lương cơ bản:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.BasicSalary)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Phụ cấp chức vụ:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.AllowPro)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Phụ cấp nhà ở:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.AllowHouse)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Phụ cấp độc hại:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.AllowDisadvantaged)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>
                      Thưởng công trình:
                    </Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.WorkingDay)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Phụ cấp khác:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.AllowOther)}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <Divider style={{marginBottom: 10, marginTop: 10}} />

            <View style={{flex: 10}}>
              <View style={{padding: 5}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontSize: 19, fontWeight: 'bold'}}>
                    Các khoản giảm trừ tháng {this.state.Data?.Thang}:{' '}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Phí Công đoàn:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.FeeOfUnion)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>
                      Thuế thu nhập cá nhân:
                    </Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.PersonalTax)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Bảo hiểm xã hội:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.InsuranceSalary)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Khoản trừ khác:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.Other)}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <Divider style={{marginBottom: 10, marginTop: 10}} />
            <View style={{flex: 10}}>
              <View style={{padding: 5}}>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Tổng thu nhập:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.addPeriod(this.state.Data?.TotalSalary)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 1, marginTop: 8}}>
                  <View style={{flex: 8}}>
                    <Text style={AppStyles.Labeldefault}>Thực lĩnh:</Text>
                  </View>
                  <View style={{flex: 0}}>
                    <Text style={styles.bigBlue}>
                      {this.addPeriod(this.state.Data?.ActualSalary)}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </Content>
        </Content>
      </Container>
    );
  }

  GetEmployeeById = Id => {
    let obj = {
      EmployeeGuid: Id,
    };
    this.setState({loading: true});
    API_HR.getViewSalary(obj)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
        });
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  bigBlue: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 15,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GetViewSalary);
