import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  Linking,
} from 'react-native';
import {Header, Divider, Button, Input, SearchBar} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions, ActionConst} from 'react-native-router-flux';
import {Back} from '@component';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import {
  Container,
  Content,
  List,
  Left,
  Right,
  Form,
  ListItem,
  Thumbnail,
  Body,
  Item,
  Label,
  Picker,
} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import {AppStyles, AppSizes, AppColors} from '@theme';
import MultiSelect_Component from '../../component/MultiSelect_Component';
import TabBar from '../../component/TabBar';

class ViewSalaryComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 15;
    this.TotalRow = 0;
    this.state = {
      name: '',
      loading: false,
      refreshing: false,
      employeeData: [],
      bankDataItem: null,
      search: '',
      StatusOfWork: ['OM'],
      EvenFromSearch: false,
      StatusOfWorkData: [
        {
          id: 'OM',
          name: 'Nhân viên chính thức',
        },
        {
          id: 'PE',
          name: 'Nhân viên thử việc',
        },
        {
          id: 'PT',
          name: 'Nhân viên thời vụ',
        },
        {
          id: 'ML',
          name: 'Nghỉ thai sản',
        },
        {
          id: 'OL',
          name: 'Nghỉ khác',
        },
        {
          id: 'LJ',
          name: 'Nhân viên nghỉ việc',
        },
        {
          id: 'EC',
          name: 'Hết hạn hợp đồng',
        },
      ],
    };
  }

  componentDidMount() {
    this.getAllEmployeePage(this.page);
  }

  keyExtractor = (item, index) => index.toString();

  renderItem = ({item}) => {
    return (
      <ListItem avatar onPress={() => this.editItem(item)}>
        <Left>
          <Thumbnail source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
        </Left>
        <Body>
          <Text style={[AppStyles.Titledefault]}>{item.FullName}</Text>
          <Text style={[AppStyles.Textdefault]}>
            {'Mã nhân viên: ' + item.EmployeeID}
          </Text>
          <Text style={[AppStyles.Textdefault]}>
            Chức vụ: {item.JobTitlesName != null ? item.JobTitlesName : null}
          </Text>
          <Text
            style={[AppStyles.Textdefault]}
            onPress={() => {
              Linking.openURL('tel:' + item.Phone);
            }}>
            Sđt : {item.Phone != undefined ? item.Phone : null}
          </Text>
        </Body>
        <Right>
          <Icon name="right" style={{fontSize: 20}} />
        </Right>
      </ListItem>
    );
  };
  _onRefresh = () => {
    this.getAllEmployeePage(this.page);
  };

  searchFilterFunction = text => {
    this.setState({employeeData: []});
    this.state.search = text;
    this.getAllEmployeePage(1);
  };
  onPressback() {
    Actions.app();
  }
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({EvenFromSearch: true});
    } else {
      this.setState({EvenFromSearch: false});
    }
  }
  render() {
    let arrayholder = [];
    return (
      <Container>
        <TabBar
          title={'Danh sách nhân sự'}
          FormSearch={true}
          CallbackFormSearch={callback =>
            this.setState({EvenFromSearch: callback})
          }
          BackModuleByCode={'HR'}
        />
        <View>
          {this.state.EvenFromSearch == true ? (
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.searchFilterFunction(text)}
              value={this.state.search}
            />
          ) : null}
        </View>
        <View style={{marginStart: 10, marginEnd: 10}}>
          {this.state.EvenFromSearch == true ? (
            <MultiSelect_Component
              ListCombobox={this.state.StatusOfWorkData}
              CallBackListCombobox={callback =>
                this.onValueChangeGroup(callback)
              }
            />
          ) : null}
        </View>

        {this.state.employeeData ? (
          <FlatList
            keyExtractor={this.keyExtractor}
            data={this.state.employeeData}
            refreshing={this.state.loading}
            renderItem={this.renderItem}
            onEndReached={this.handleLoadMore}
            onRefresh={this._onRefresh}
            ListFooterComponent={this.renderFooter.bind(this)}
            onEndReachedThreshold={0.4}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
        ) : null}
      </Container>
    );
  }
  clickBack() {
    Actions.app();
  }
  onValueChangeGroup(value) {
    if (value.includes(',')) {
      let stamp = value.split(',');
      this.setState({
        StatusOfWork: stamp,
        employeeData: [],
      });
      this.state.StatusOfWork = stamp;
    } else {
      this.setState({
        StatusOfWork: value,
        employeeData: [],
      });
      this.state.StatusOfWork = value;
    }
    this.getAllEmployeePage(1);
  }
  StatusOfWorkData = () => {
    return this.state.StatusOfWorkData.map((x, i) => {
      return <Picker.Item label={x.text} key={i} value={x.value} />;
    });
  };
  editItem(item) {
    Actions.getViewSalary({EmployeeGuid: item.EmployeeGuid});
  }
  addItem() {
    Actions.additem({EmployeeGuid: ''});
  }
  getAllEmployeePage = NumberPage => {
    let obj = {
      NumberPage: NumberPage,
      Length: this.Length,
      Keyword: this.state.search,
      Status: this.state.StatusOfWork,
      lstDepartmentID: [],
    };
    console.log('555555 ' + this.state.StatusOfWork);

    this.setState({loading: true});
    API_HR.getAllEmployeePage(obj)
      .then(response => {
        console.log(
          '==============ketqua: ' +
            JSON.stringify(
              JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
            ),
        );
        let listData = this.state.employeeData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
          ),
        );
        let data = listData.concat(data1);
        this.setState({employeeData: data, loading: false});
        this.arrayholder = data;
        let TotalRow = JSON.parse(
          JSON.parse(JSON.stringify(response.data)).data,
        );
        this.TotalRow = TotalRow.Count;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  handleLoadMore = () => {
    if (!this.state.loading && this.TotalRow > this.state.employeeData.length) {
      this.page = this.page + 1; // increase page by 1
      this.getAllEmployeePage(this.page); // method for API call
    }
  };
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
}
const colors = {
  loginGreen: '#347F16',
};
const styles = StyleSheet.create({
  loginContainer: {
    marginTop: 10,
    marginEnd: 10,
    alignItems: 'flex-end',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewSalaryComponent);
