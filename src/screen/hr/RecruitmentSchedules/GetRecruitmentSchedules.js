import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import { Icon, Header } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import { API_HR } from '@network';
import API from '../../../network/API';
import { DatePicker, Container, Item, Label, Picker } from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import { WebView } from 'react-native-webview';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import LoadingComponent from '../../component/LoadingComponent';
const ItemCode = {
  PERSONNEL: 0,
  SELL: 1,
  CUSTOMER: 2,
  PURCHASE: 3,
  ACCOUNTANT: 4,
  WAREHOUSE: 5,
  OPERATIONAL: 6,
  DOCUMENT: 7,
  MARKETING: 8,
  PROJECT: 9,
  MANUFACTURING: 10,
  PROPERTY: 11,
  ESTABLISH: 12,
  LOGOUT: 13,
};
class GetRecruitmentSchedules extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      loading: true,
      Data: [],
      RecruitmentScheduleId: '',
    };
    viewId: '';
  }
  componentDidMount(): void {
    this.state.viewId = this.props.viewId;
    this.getItem();
  }
  getItem() {
    let id = this.state.viewId;
    var obj = {
      RecruitmentScheduleId: this.state.viewId,
    };
    API_HR.getRecruitmentSchedules(obj)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
          loading: false,
        });
        console.log('===========> error' + res.data.data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  keyExtractor = (item, index) => index.toString();
  render() {
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <Container>
        <TouchableWithoutFeedback
          style={{ flex: 30 }}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View
            style={{
              flex: 1,
              fontSize: 11,
              fontFamily: Fonts.base.family,
              backgroundColor: 'white',
            }}>
            <TabBar_Title
              title={'Chi tiết lịch'}
              callBack={() => this.onPressBack()}
            //FormAttachment={true}
            //CallbackFormAttachment={() => this.openAttachments()}
            />
            <View style={{ flex: 10 }}>
              <View style={{ padding: 20 }}>
                <View style={{ flexDirection: 'row', padding: 5 }}>
                  <View style={{ flex: 3 }}>
                    <Text style={AppStyles.Labeldefault}>Tiêu đề :</Text>
                  </View>
                  <View style={{ flex: 4 }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data.Title}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', padding: 5 }}>
                  <View style={{ flex: 3 }}>
                    <Text style={AppStyles.Labeldefault}>Vòng phỏng vấn :</Text>
                  </View>
                  <View style={{ flex: 4 }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data.InterviewTitle}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', padding: 5 }}>
                  <View style={{ flex: 3 }}>
                    <Text style={AppStyles.Labeldefault}>Người giám sát :</Text>
                  </View>
                  <View style={{ flex: 4 }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data.Interviewer}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', padding: 5 }}>
                  <View style={{ flex: 3 }}>
                    <Text style={AppStyles.Labeldefault}>Ngày bắt đầu :</Text>
                  </View>
                  <View style={{ flex: 4 }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.customDate(this.state.Data.StartTime)}
                    </Text>
                  </View>
                </View>
                {/* <View style={{flexDirection: 'row', marginTop: 10}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Labeldefault}>Ngày bắt đầu : </Text>
                  </View>
                  <View style={[AppStyles.Textdefault]}>
                    <Text>{this.customDate (this.state.Data.StartTime)}</Text>
                  </View>
                </View> */}
                <View style={{ flexDirection: 'row', padding: 5 }}>
                  <View style={{ flex: 3 }}>
                    <Text style={AppStyles.Labeldefault}>Ngày kết thúc :</Text>
                  </View>
                  <View style={{ flex: 4 }}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.customDate(this.state.Data.EndTime)}
                    </Text>
                  </View>
                </View>
                {/* <View style={{flexDirection: 'row', padding: 5}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Ngày tạo lịch :</Text>
                  </View>
                  <View style={{flex: 4}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.customDate(this.state.Data.CreatedDate)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 5}}>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Labeldefault}>Người giám sát :</Text>
                  </View>
                  <View style={{flex: 4}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.state.Data.Location}
                    </Text>
                  </View>
                </View> */}
                <View style={{ flexDirection: 'row', padding: 5 }}>
                  <View style={{ flex: 3 }}>
                    <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
                  </View>
                  <View
                    style={{
                      flex: 4,
                      minHeight: 500,
                      marginTop: -5,
                      marginLeft: -10,
                    }}>
                    <WebView
                      originWhitelist={['*']}
                      source={{
                        html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>${this
                          .state.Data.Note || '<div></div>'}</body></html>`,
                      }}
                    // containerStyle={{ fontSize: 23 }}
                    // style={{ fontSize: 23 }}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
  openAttachments() {
    var obj = {
      ModuleId: '6',
      RecordGuid: this.props.ContractGuid,
    };
    Actions.attachmentComponent(obj);
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'back', ActionTime: new Date().getTime() });
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  closeComment() {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    fontSize: 14,
    marginLeft: 5,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const mapStateToProps = state => ({ Loginname: state.user.account });

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetRecruitmentSchedules);
