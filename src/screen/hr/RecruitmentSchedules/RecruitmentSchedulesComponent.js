import React, { Component } from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import {
  Header,
  Divider,
  Icon,
  ListItem,
  SearchBar,
  Badge,
} from 'react-native-elements';
import { API_HR, API } from '@network';
import TabBar from '../../component/TabBar';
import Fonts from '../../../theme/fonts';
import { Picker } from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import LoadingComponent from '../../component/LoadingComponent';

class RecruitmentSchedulesComponent extends Component {
  constructor(props) {
    super(props);
    //khai báo biến
    this.state = {
      refreshing: false,
      Data: null,
      ListAll: [],
      loading: true,
      ListCountNoti: [],
      ChoDuyet: '',
      search: '',
      TraLai: '',
      openSearch: false,
    };
    // dữ liệu trueyenf vao APi
    this._search = {
      NumberPage: 0,
      txtSearch: '',
      Length: 7,
      Query: 'Date DESC',
    };
  }
  onClick(data) {
    if (data == 'ok') {
      this.setState({
        stylex: 2,
      });
      this._search.Status = 'Y';
      this.setState({ ListAll: [] });
      this.nextPage();
    } else if (data == 'no') {
      this.setState({
        stylex: 1,
      });
      this._search.Status = 'W';
      this.setState({ ListAll: [] });
      this.nextPage();
    }
  }
  //khi vào trang chạy đầu tiên
  componentDidMount(): void {
    this.nextPage();
  }
  // khi trả lại trang,thì load vào danh sách
  componentWillReceiveProps(nextProps) {
    this.nextPage();
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.txtSearch = search;
    this._search.NumberPage = 0;
    this.nextPage();
  };
  nextPage() {
    this._search.NumberPage++;
    API.CountStatus()
      .then(res => {
        var listCount = JSON.parse(res.data.data);
        this.setState({ ListCountNoti: listCount });
        var _choduyet = '';
        var _tralai = '';
        var data = listCount.find(x => x.Name == 'ChoDuyet');
        if (data !== null && data !== undefined) {
          if (data.Value > 99) {
            _choduyet = '99+';
          } else {
            _choduyet = data.Value;
          }
          this.setState({ ChoDuyet: _choduyet });
        }
        //tra lai
        var dataTraLai = listCount.find(x => x.Name == 'RegisterEatsNot');
        if (dataTraLai !== null && dataTraLai !== undefined) {
          if (dataTraLai.Value > 99) {
            _tralai = '9999';
          } else {
            _tralai = dataTraLai.Value;
          }
          this.setState({ TraLai: _tralai });
        }
      })
      .catch(error => {
        console.log(error.data.data);
      });
    API_HR.getAllRecruitmentSchedules(this._search)
      .then(res => {
        var _listall = this.state.ListAll;
        for (
          let index = 0;
          index < JSON.parse(res.data.data).data.length;
          index++
        ) {
          _listall.push(JSON.parse(res.data.data).data[index]);
        }
        this.setState({
          ListAll: _listall,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log(error);
        this.setState({ loading: false });
      });
  }

  // khi kéo danh sách,thì phân trang
  loadData() {
    //if((this._search.NumberPage * this._search.Length) <= this.state.ListAll.length){
    this.nextPage();
    //}
  }
  // view hiển thị
  CustomeListAll = item => (
    <View style={{ flex: 30 }}>
      <FlatList
        ListEmptyComponent={this.ListEmpty}
        data={item}
        style={{ height: '100%', marginBottom: 20 }}
        renderItem={({ item, index }) => (
          <View style={{ fontSize: 11, fontFamily: Fonts.base.family }}>
            <ListItem
              subtitle={() => {
                return (
                  <View style={{ flexDirection: 'row', marginTop: -25 }}>
                    <View style={{ flex: 3, flexDirection: 'row' }}>
                      <View>
                        <Text
                          style={[
                            AppStyles.Titledefault,
                            {
                              //color: '#2E77FF',
                            },
                          ]}>
                          {item.Title}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          {item.InterviewTitle}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          Thời gian: {this.customDate(item.StartTime)}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text
                        style={[
                          styles.subtitleStyle,
                          { textAlign: 'right', justifyContent: 'center' },
                        ]}>
                        {/* Ngày tạo
                      {this.customDate (item.CreatedDate)} */}
                      </Text>
                      {item.Status == 'Y' ? (
                        <Text
                          style={{
                            textAlign: 'right',
                            justifyContent: 'center',
                            color: 'green',
                          }}>
                          {item.StatusWF}
                        </Text>
                      ) : (
                        <Text
                          style={{
                            textAlign: 'right',
                            justifyContent: 'center',
                            color: 'red',
                          }}>
                          {item.StatusWF}
                        </Text>
                      )}
                    </View>
                  </View>
                );
              }}
              bottomDivider
              //chevron
              onPress={() => this.onViewItem(item.RecruitmentScheduleId)}
            />
          </View>
        )}
        onEndReached={() => this.loadData()}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['black']}
          />
        }
      />
    </View>
  );
  render() {
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <TouchableWithoutFeedback
        style={{ flex: 1 }}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Lịch phỏng vấn'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({ EvenFromSearch: callback })
            }
          // FormQR={true}
          // CallbackFormQR={callback => this.setState ({EvenFormQR: callback})}
          //addForm={'addregistereats'}
          />
          <Divider />
          <View>
            {this.state.EvenFromSearch == true ? (
              <SearchBar
                placeholder="Tìm kiếm..."
                lightTheme
                round
                inputContainerStyle={{ backgroundColor: '#e1ecf4' }}
                containerStyle={AppStyles.FormSearchBar}
                onChangeText={search => this.updateSearch(search)}
                value={this._search.txtSearch}
              />
            ) : null}
          </View>
          <View style={{ flex: 20 }}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  ListEmpty = () => {
    if (this.state.ListAll.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  onPressHome() {
    Actions.home();
  }
  openSearch() {
    if (this.state.openSearch === false) {
      this.setState({ openSearch: true });
    } else {
      this.setState({ openSearch: false });
    }
  }
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'W';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'profile',
    });
    this.nextPage();
  };
  // sự kiện trả lại
  onKhongDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'R';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'export2',
    });
    this.nextPage();
  };
  // sự kiện đã  duyệt
  onDaDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'Y';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'checkcircleo',
    });
    this.nextPage();
  };
  //view item,click sang view khác
  onViewItem(id) {
    Actions.GetRecruitmentSchedules({ viewId: id });
  }

  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }
  renderLeftMenu = () => {
    return (
      <View>
        <Icon
          style={{ color: 'black' }}
          name={'chevron-thin-left'}
          type="entypo"
          size={25}
          onPress={() => Actions.home()}
        />
      </View>
    );
  };
  renderRightMenu = () => {
    return (
      <View>
        <Icon
          style={{ color: 'black' }}
          name={'plus'}
          type="antdesign"
          size={30}
          onPress={() => Actions.addregistereats()}
        />
      </View>
    );
  };
  // xóa tìm kiếm
  onclickreturnSearch() {
    this._search.Search = '';
    this.updateSearch('');
  }
  //
  onValueChange(item) {
    console.log('============>>' + item);
    switch (item) {
      case 'Status': {
        break;
      }
      case 'StartDate': {
        break;
      }
      case 'EndDate': {
        break;
      }
      default: {
      }
    }
  }

  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  //Load lại
  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.nextPage();
  };
}

//custom giao diện
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  searchcontainer: {
    backgroundColor: 'white',
    height: 50,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0, //no effect
    shadowColor: 'white', //no effect
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '100',
    fontSize: 30,
    color: '#f6b801',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 2,
    paddingBottom: 5,
    paddingLeft: 10,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#CCCCCC',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  ClickApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#CCCCCC',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
});
const mapStateToProps = state => ({});

const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(RecruitmentSchedulesComponent);
