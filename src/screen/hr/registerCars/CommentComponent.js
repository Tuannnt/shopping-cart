import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Image,
  TextInput,
  TouchableWithoutFeedback,
  RefreshControl,
} from 'react-native';
import {ListView} from 'deprecated-react-native-listview';
import {connect} from 'react-redux';
import {Icon, Header} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Tabbar from 'react-native-tabbar-bottom';
import DocumentPicker from 'react-native-document-picker';
import {WebView} from 'react-native-webview';
import TabBar_Title from '../../component/TabBar_Title';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
} from 'native-base';
import {API_RegisterCars} from '@network';

class CommentComponent extends Component {
  constructor() {
    super();
    this.page = 1;
    this.Length = 15;
    this.TotalRow = 0;
    this.state = {
      Comment: '',
      loading: false,
      listWofflowData: [],
    };
  }
  //handling onPress action
  componentDidMount() {
    this.getAllCommentPage(this.page);
  }
  onClickBack() {
    Actions.pop();
  }
  sendComment() {
    console.log('Send mess: ' + this.state.Comment);

    let commentInput = this.refs['commentInput'];
    console.log(commentInput);
    commentInput.clear();
    this.state.Comment = '';
  }
  keyExtractor = (item, index) => index.toString();
  renderItem = ({item}) => {
    // just standard if statement
    if (item.LoginName == this.state.LoginName) {
      return (
        <ListItem avatar>
          <Body style={styles.boxComment}>
            <Text style={{color: 'white'}} note numberOfLines={1}>
              {item.Comment}
            </Text>
          </Body>
          <Left>
            <Thumbnail
              style={{width: 40, height: 40}}
              source={API_RegisterCars.GetPicApplyLeaves(item.EmployeeGuid)}
            />
          </Left>
        </ListItem>
      );
    }
    return (
      <ListItem avatar>
        <Left>
          <Thumbnail
            style={{width: 40, height: 40}}
            source={API_RegisterCars.GetPicApplyLeaves(item.EmployeeGuid)}
          />
        </Left>
        <Body style={styles.boxComment}>
          <Text style={{color: 'white'}} note numberOfLines={1}>
            {item.Comment}
          </Text>
        </Body>
      </ListItem>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        <Header
          leftComponent={this.renderLeftComponent()}
          centerComponent={{
            text: 'Nội dung trao đổi',
            style: {color: 'black', fontSize: 22},
          }}
          containerStyle={{backgroundColor: '#fff'}}
        />
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <Container>
            <FlatList
              style={{marginBottom: 55}}
              keyExtractor={this.keyExtractor}
              data={this.state.listWofflowData}
              refreshing={this.state.loading}
              renderItem={this.renderItem}
              onEndReached={this.handleLoadMore}
              onRefresh={this._onRefresh}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.loading}
                  tintColor="#f5821f"
                  titleColor="#fff"
                  colors={['red', 'green', 'blue']}
                />
              }
            />
          </Container>
        </View>
      </View>
    );
  }

  GetAllCommentPage = Page => {
    let obj = {
      Page: this.page,
      ItemPage: this.Length,
      RecordGuid: this.props.ApplyLeaveGuid,
    };
    this.setState({loading: true});
    API_RegisterCars.GetAllCommentPage(obj)
      .then(rs => {
        console.log(
          '==============ketquaCommnet: ' +
            JSON.stringify(
              JSON.parse(JSON.parse(JSON.stringify(rs.data)).data).Data,
            ),
        );
        let listData = this.state.listWofflowData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(rs.data)).data).Data,
          ),
        );
        let data = listData.concat(data1);
        this.setState({listWofflowData: data, loading: false});
        this.arrayholder = data;
        let TotalRow = JSON.parse(JSON.parse(JSON.stringify(rs.data)).data);
        this.TotalRow = TotalRow.Count;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  };
  handleLoadMore = () => {
    if (
      !this.state.loading &&
      this.TotalRow > this.state.listWofflowData.length
    ) {
      this.page = this.page + 1;
      this.GetAllCommentPage(this.page);
    }
  };
  renderFooter = () => {
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'chevron-left'}
          type="entypo"
          size={35}
          onPress={() => this.onPressBack()}
        />
      </View>
    );
  }
  onPressBack() {
    Actions.openRegistercar();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerContent: {
    flex: 1,
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  boxSearch: {
    flexDirection: 'row',
  },
  line: {
    borderBottomColor: '#eff0f1',
    borderBottomWidth: 1,
    marginBottom: 3,
    marginTop: 3,
  },
  boxComment: {
    flex: 1,
    padding: 3,
    justifyContent: 'center',
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    margin: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#24c79f',
  },
  boxBottom: {
    flex: 1,
    width: '100%',
    position: 'absolute',
    height: 50,
    bottom: 0,
    borderTopColor: '#eff0f1',
    borderTopWidth: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommentComponent);
