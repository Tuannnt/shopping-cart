import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {Header, SearchBar, Icon, Badge, Divider} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_RegisterCars} from '@network';
import API from '../../../network/API';
import {Container, ListItem, Button} from 'native-base';
import MenuSearchDate from '../../component/MenuSearchDate';
import Combobox from '../../component/Combobox';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import DatePicker from 'react-native-date-picker';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import {FuncCommon} from '../../../utils';

const ListStatus = [
  {value: 'XeHN', text: 'Xe Hà Nội'},
  {value: 'XeHCM', text: 'Xe HCM'},
];
const LiRegType = [
  {
    value: 'A',
    text: 'Việc cá nhân',
  },
  {
    value: 'B',
    text: 'Việc công ty',
  },
  {
    value: 'C',
    text: 'Lab',
  },
  {
    value: 'D',
    text: 'Sale',
  },
];
const SCREEN_WIDTH = Dimensions.get('window').width;
class indexComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      name: '',
      refreshing: false,
      list: [],
      ListCountNoti: [],
      Pending: '',
      NotApprove_RegisterCars: '',
      selectedTab: 'profile',
      isFetching: false,
      EvenFromSearch: false,
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
    this.staticParam = {
      CurrentPage: 1,
      StartTime: new Date(),
      EndTime: new Date(),
      Status: 'W',
      Length: 10,
      search: {
        value: '',
      },
      CarId: 'XeHN',
      QueryOrderBy: 'CreatedDate DESC',
      RegType: 'A',
    };
  }
  loadMoreData = () => {
    this.nextPage();
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartTime = new Date(callback.start);
      this.staticParam.EndTime = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  setStartTime = date => {
    this.staticParam.StartTime = date;
    this.updateSearch('');
  };
  setEndTime = date => {
    this.staticParam.EndTime = date;
    this.updateSearch('');
  };
  _onRefresh = () => {
    this.updateSearch('');
    this.setState({refreshing: true});
  };
  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  _openComboboxRegType() {}
  openComboboxRegType = d => {
    this._openComboboxRegType = d;
  };
  onActionComboboxRegType() {
    this._openComboboxRegType();
  }
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Phiếu đăng ký xe'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            addForm={true}
            CallbackFormAdd={() => Actions.addRegistercar()}
            BackModuleByCode={'MyProfile'}
          />
          <Divider />

          {this.state.EvenFromSearch == true ? (
            <View style={{flexDirection: 'column'}}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    padding: 5,
                  }}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian bắt đầu
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventStartTime: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.staticParam.StartTime, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian kết thúc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventEndTime: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.staticParam.EndTime, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column', padding: 5}}>
                  <Text
                    style={[
                      AppStyles.Labeldefault,
                      {textAlign: 'center'},
                      styles.timeHeader,
                    ]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              {/* <TouchableOpacity
                style={[AppStyles.FormInput, {marginHorizontal: 10}]}
                onPress={() => this.onActionComboboxType()}>
                <Text
                  style={[
                    AppStyles.TextInput,
                    this.staticParam.CarName
                      ? {color: 'black'}
                      : {color: AppColors.gray},
                  ]}>
                  {this.staticParam.CarName
                    ? this.staticParam.CarName
                    : 'Tìm kiếm...'}
                </Text>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  <Icon
                    color={AppColors.gray}
                    name={'chevron-thin-down'}
                    type="entypo"
                    size={20}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  AppStyles.FormInput,
                  {marginHorizontal: 10, marginTop: 5},
                ]}
                onPress={() => this.onActionComboboxRegType()}>
                <Text
                  style={[
                    AppStyles.TextInput,
                    this.staticParam.RegTypeName
                      ? {color: 'black'}
                      : {color: AppColors.gray},
                  ]}>
                  {this.staticParam.RegTypeName
                    ? this.staticParam.RegTypeName
                    : 'Tìm kiếm...'}
                </Text>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  <Icon
                    color={AppColors.gray}
                    name={'chevron-thin-down'}
                    type="entypo"
                    size={20}
                  />
                </View>
              </TouchableOpacity> */}
              <SearchBar
                placeholder="Tìm kiếm..."
                lightTheme
                round
                inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                containerStyle={AppStyles.FormSearchBar}
                onChangeText={text => this.updateSearch(text)}
                value={this.staticParam.search.value}
              />
            </View>
          ) : null}
          {this.state.list ? (
            <View style={{flex: 1}}>
              <FlatList
                style={{flex: 1}}
                ref={ref => {
                  this.ListView_Ref = ref;
                }}
                ListEmptyComponent={this.ListEmpty}
                //ListHeaderComponent={this.renderHeader}
                ListFooterComponent={this.renderFooter}
                keyExtractor={item => item.BankGuid}
                data={this.state.list}
                renderItem={({item, index}) => (
                  <View>
                    <ListItem onPress={() => this.openView(item)}>
                      <View style={{flexDirection: 'row'}}>
                        <View style={{flex: 6}}>
                          <Text style={[AppStyles.Titledefault]}>
                            {item.FullName}
                          </Text>
                          <Text style={[AppStyles.Textdefault]}>
                            Nơi đi: {item.PickUpLocation}
                          </Text>
                          <Text style={[AppStyles.Textdefault]}>
                            Nơi đến: {item.GetOffLocation}
                          </Text>
                        </View>
                        <View style={{flex: 6, alignItems: 'flex-end'}}>
                          {item.Status == 'Y' ? (
                            <Text
                              style={[
                                {
                                  justifyContent: 'center',
                                  color: AppColors.AcceptColor,
                                },
                                AppStyles.Textdefault,
                              ]}>
                              {item.StatusWF}
                            </Text>
                          ) : (
                            <Text
                              style={[
                                {
                                  justifyContent: 'center',
                                  color: AppColors.PendingColor,
                                },
                                AppStyles.Textdefault,
                              ]}>
                              {item.StatusWF}
                            </Text>
                          )}
                          <Text style={[AppStyles.Textdefault]}>
                            {this.customDate(item.Date)}
                          </Text>
                          {/* <Text style={[AppStyles.Textdefault]}>
                            {this.addPeriod(item.Tolls)}
                          </Text> */}
                        </View>
                      </View>
                    </ListItem>
                  </View>
                )}
                onEndReached={() => this.loadMoreData()}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['black']}
                  />
                }
              />
              {/* hiển thị nút tuỳ chọn */}
            </View>
          ) : null}
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.onCallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.setEventStartTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventStartTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.StartTime}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartTime(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventEndTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.EndTime}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndTime(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {/* {ListStatus.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeType}
              data={ListStatus}
              nameMenu={'Chọn trạng thái'}
              eOpen={this.openComboboxType}
              position={'bottom'}
              value={this.staticParam.CarId}
            />
          ) : null}
          {LiRegType.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeRegType}
              data={LiRegType}
              nameMenu={'Chọn loại'}
              eOpen={this.openComboboxRegType}
              position={'bottom'}
              value={this.staticParam.RegType}
            />
          ) : null} */}
        </View>
      </TouchableWithoutFeedback>
    );
  }
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.staticParam.CarId = rs.value;
    this.staticParam.CarName = rs.text;
    this.updateSearch('');
  };
  ChangeRegType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.staticParam.RegType = rs.value;
    this.staticParam.RegTypeName = rs.text;
    this.updateSearch('');
  };
  onCallbackValueBottom(value) {
    switch (value) {
      case 'profile':
        this.onClickPending();
        break;
      case 'checkcircleo':
        this.onClickAprover();
        break;
      case 'export2':
        this.onClickNotApprove();
        break;
      default:
        break;
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.updateSearch('');
    }
  }
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  // sự kiện chờ duyệt
  onClickPending = () => {
    this.staticParam.Status = 'W';
    this.updateSearch('');
  };
  onClickAprover = () => {
    this.staticParam.Status = 'Y';
    this.updateSearch('');
  };
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({EvenFromSearch: true});
    } else {
      this.setState({EvenFromSearch: false});
    }
  }
  onClickNotApprove = () => {
    this.staticParam.Status = 'R';
    this.state.selectedTab = 'export2';
    this.setState({
      stylex: 3,
    });
    this.setState({
      list: [],
      selectedTab: this.state.selectedTab,
    });
    this.GetAll();
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  updateSearch = text => {
    this.staticParam.search.value = text;
    this.staticParam.CurrentPage = 1;
    this.setState(
      {
        list: [],
      },
      () => {
        this.GetAll('reload');
      },
    );
  };
  nextPage(type) {
    this.staticParam.CurrentPage++;
    this.GetAll(type);
  }
  GetAll = type => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_RegisterCars.Registercar_ListAll(this.staticParam)
          .then(res => {
            FuncCommon.Data_Offline_Set('Registercar_ListAll', res);
            this._GetAll_Registercar_ListAll(res, type);
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('Registercar_ListAll');
        this._GetAll_Registercar_ListAll(x, type);
      }
    });
  };
  _GetAll_Registercar_ListAll = (res, type) => {
    let _data = [];
    if (this.staticParam.CurrentPage === 1) {
      _data = JSON.parse(res.data.data).data;
    } else {
      _data = this.state.list.concat(JSON.parse(res.data.data).data);
    }
    if (this.staticParam.Status === 'W') {
      this.listtabbarBotom[0].Badge = JSON.parse(res.data.data).recordsTotal;
    }
    this.setState({list: _data, refreshing: false});
  };

  openView(item) {
    Actions.openRegistercar({
      RecordGuid: item.RegisterCarGuid,
    });
  }
  clickItem(item) {
    switch (item) {
      case 'Back':
        Actions.app();
        break;
      case 'Open':
        Actions.openRegistercar();
        break;
      default:
        break;
    }
  }
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 5,
    paddingVertical: 5,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
  timeHeader: {
    marginBottom: 3,
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(indexComponent);
