import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  Keyboard,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {Header, Button, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_RegisterCars} from '@network';
import {Container, Content, Item, Label, Picker} from 'native-base';
import {FuncCommon} from '../../../utils';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';
import RequiredText from '../../component/RequiredText';
import Combobox from '../../component/Combobox';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Toast from 'react-native-simple-toast';
import _ from 'lodash';
import moment from 'moment';
import controller from './controller';

const LiRegType = [
  {
    value: 'A',
    label: 'Việc cá nhân',
  },
  {
    value: 'B',
    label: 'Việc công ty',
  },
  {
    value: 'C',
    label: 'Lab',
  },
  {
    value: 'D',
    label: 'Sale',
  },
];
var width = Dimensions.get('window').width; //full width
class addComponent extends Component {
  constructor(props) {
    super(props);
    this.DataProcess = FuncCommon.DataProcess();
    this.state = {
      refreshing: false,
      bankData: [],
      bankDataItem: null,
      search: '',
      Licars: [],
      Date: new Date(),
      StartTime: new Date(),
      EndTime: new Date(),
      Tolls: '0',
      ListEmp: [],
      // RegType: 'A',
      ListReplate: [],
      modalPickerTimeVisible: false,
      isEdit: false,
      CarId: null,
    };
  }
  Submit() {
    FuncCommon.Data_Offline(async d => {
      if (!d) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      } else {
        this.Add();
      }
    });
  }
  Add = () => {
    if (!this.state.Date) {
      Toast.showWithGravity(
        'Bạn chưa chọn ngày đăng ký',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.RegisterCarId) {
      Toast.showWithGravity(
        'Bạn chưa điền mã phiếu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.PickUpLocation) {
      Toast.showWithGravity('Bạn chưa điền nơi đi', Toast.SHORT, Toast.CENTER);
      return;
    }
    if (!this.state.GetOffLocation) {
      Toast.showWithGravity('Bạn chưa điền nơi đến', Toast.SHORT, Toast.CENTER);
      return;
    }
    if (this.state.EndTime.getTime() < this.state.StartTime.getTime()) {
      Toast.showWithGravity(
        'Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (this.props.itemData) {
      this.Update();
      return;
    }
    let _obj = {
      CarId: this.state.CarId ? this.state.CarId : '',
      // RegType: this.state.RegType,
      PickUpLocation: this.state.PickUpLocation,
      GetOffLocation: this.state.GetOffLocation,
      Date: FuncCommon.ConDate(this.state.Date, 2),
      StartTime: FuncCommon.ConDate(this.state.StartTime, 2),
      EndTime: FuncCommon.ConDate(this.state.EndTime, 2),
      ReplaceBy: this.state.ReplaceBy,
      ReplaceGuid: null,
      // Tolls: this.state.Tolls,
      Description: this.state.Description,
      Note: this.state.Note,
      Tolls: 0,
      RegisterConfirm: 0,
      IsConfirm: 0,
      RegisterCarId: this.state.RegisterCarId,
    };
    let auto = {
      Value: this.state.ApplyOvertimeId,
      IsEdit: this.state.isEdit,
    };
    let _dataForm = new FormData();
    _dataForm.append('submit', JSON.stringify(_obj));
    _dataForm.append('auto', JSON.stringify(auto));
    _dataForm.append(
      'submitListReplate',
      JSON.stringify(this.state.ListReplate),
    );
    API_RegisterCars.InsertRegisterCars(_dataForm)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          Toast.showWithGravity(_rs.Message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.callBackList(JSON.parse(rs.data.data));
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  Update = () => {
    let _obj = {
      ...this.props.itemData,
      // RegType: this.state.RegType,
      CarId: this.state.CarId,
      PickUpLocation: this.state.PickUpLocation,
      GetOffLocation: this.state.GetOffLocation,
      Date: FuncCommon.ConDate(this.state.Date, 2),
      StartTime: FuncCommon.ConDate(this.state.StartTime, 2),
      EndTime: FuncCommon.ConDate(this.state.EndTime, 2),
      // Tolls: this.state.Tolls,
      Description: this.state.Description,
      Note: this.state.Note,
      ReplaceBy: this.state.ReplaceBy,
      ReplaceGuid: null,
    };
    let auto = {
      Value: this.state.ApplyOvertimeId,
      IsEdit: this.state.isEdit,
    };
    let _dataForm = new FormData();
    _dataForm.append('edit', JSON.stringify(_obj));
    _dataForm.append('auto', JSON.stringify(auto));
    _dataForm.append(
      'submitListReplate',
      JSON.stringify(this.state.ListReplate),
    );
    API_RegisterCars.Registercars_Update(_dataForm)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          Toast.showWithGravity(_rs.Message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Cập nhật thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };
  componentDidMount() {
    const {itemData, listReplate = []} = this.props;
    Promise.all([this.getCar(), this.GetEmployeeByDepartment()]);
    if (!itemData) {
      this.getNumberAuto();
    } else {
      // if (listReplate && listReplate.length > 0) {
      //   let Replate = listReplate.map(list => {
      //     return {
      //       text: list.ReplaceBy,
      //       value: list.LstReplaceGuid,
      //     };
      //   });
      //   this.ChangeEmp(Replate);
      // }
      this.setState({
        RegisterCarId: itemData.RegisterCarId,
        PickUpLocation: itemData.PickUpLocation,
        GetOffLocation: itemData.GetOffLocation,
        Date: new Date(itemData.Date),
        StartTime: new Date(FuncCommon.ConDate(itemData.StartTime, 14)),
        EndTime: new Date(FuncCommon.ConDate(itemData.EndTime, 14)),
        ReplaceBy: itemData.ReplaceBy,
        Tolls: itemData.Tolls + '',
        Description: itemData.Description,
        Note: itemData.Note,
        FullName: itemData.FullName,
        DepartmentName: itemData.DepartmentName,
        CarId: itemData.CarId,
        // RegType: itemData.RegType,
      });
    }
  }
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_DKX',
      Prefixs: null,
      VoucherType: '',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({
        isEdit: data.IsEdit,
        RegisterCarId: data.Value,
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
      });
    });
  };
  getCar = () => {
    let _obj = {};
    API_RegisterCars.GetCars(_obj)
      .then(rs => {
        var _listCars = rs.data;
        let _reListCars = [
          {
            label: 'Chọn',
            value: '',
          },
        ];
        for (var i = 0; i < _listCars.length; i++) {
          _reListCars.push({
            label: _listCars[i].CarName,
            value: _listCars[i].CarId,
          });
        }

        this.setState({
          Licars: _reListCars,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  GetEmployeeByDepartment = () => {
    API_RegisterCars.GetEmp()
      .then(rs => {
        let _data = JSON.parse(rs.data.data) || [];

        let _listEmp = _data.map(emp => {
          return {
            value: emp.EmployeeGuid,
            text: `[${emp.EmployeeId}] ${emp.FullName}`,
          };
        });
        this.setState({
          ListEmp: _listEmp,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  //#region hàm Quay lại
  callBackList(rs) {
    this.setState({loading: false});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      Data: !this.state.itemData
        ? {
            Module: 'HR_RegisterCars',
            // list all key
            Subject: this.state.Description,
            // title
            StartDate:
              this.state.StartTime === '' || this.state.StartTime === null
                ? null
                : FuncCommon.ConDate(this.state.StartTime, 0),
            EndDate:
              this.state.EndTime === '' || this.state.EndTime === null
                ? null
                : FuncCommon.ConDate(this.state.EndTime, 0),
            RowGuid: rs,
            AssignTo: [],
          }
        : {},
      ActionTime: new Date().getTime(),
    });
  }
  onValueChangeRegType = val => {
    this.setState({RegType: val});
  };
  //#endregion
  render() {
    const {RegisterCarId, isEdit} = this.state;
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <View style={[AppStyles.container]}>
          <Container>
            {this.props.itemData ? (
              <TabBar_Title
                title={'Chỉnh sửa đăng ký xe'}
                callBack={() => Actions.pop()}
              />
            ) : (
              <TabBar_Title
                title={'Thêm mới đăng ký xe'}
                callBack={() => Actions.pop()}
              />
            )}

            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Mã phiếu <RequiredText />
                    </Text>
                  </View>
                  {isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={RegisterCarId}
                      autoCapitalize="none"
                      onChangeText={Title =>
                        this.setState({RegisterCarId: Title})
                      }
                    />
                  )}
                  {!isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                        {RegisterCarId}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Tên nhân viên</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>

                <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>Loại xe</Text>
                  <View
                    style={[
                      {
                        ...AppStyles.FormInputPicker,
                        justifyContent: 'center',
                      },
                    ]}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value => this.onValueChange2(value)}
                      items={this.state.Licars}
                      value={this.state.CarId}
                      placeholder={{}}
                      style={{
                        inputIOS: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                        inputAndroid: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                      }}
                      // useNativeAndroidPickerStyle={false}
                    />
                  </View>
                </View>
                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>
                    Đối tượng chịu chi phí
                  </Text>
                  <View
                    style={[
                      {
                        ...AppStyles.FormInputPicker,
                        justifyContent: 'center',
                      },
                    ]}>
                    <RNPickerSelect
                        doneText="Xong"
                      onValueChange={value => this.onValueChangeRegType(value)}
                      items={LiRegType}
                      value={this.state.RegType}
                      placeholder={{}}
                      style={{
                        inputIOS: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                        inputAndroid: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                      }}
                      // useNativeAndroidPickerStyle={false}
                    />
                  </View>
                </View> */}
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text className="required" style={AppStyles.Labeldefault}>
                      Nơi đi <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    value={this.state.PickUpLocation}
                    autoCapitalize="none"
                    onChangeText={PickUpLocation =>
                      this.setPickUpLocation(PickUpLocation)
                    }
                  />
                </View>

                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Nơi đến <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.GetOffLocation}
                    onChangeText={GetOffLocation =>
                      this.setGetOffLocation(GetOffLocation)
                    }
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người đi cùng</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.ReplaceBy}
                    onChangeText={ReplaceBy =>
                      this.onChangeState('ReplaceBy', ReplaceBy)
                    }
                  />
                  {/* <TouchableOpacity
                    style={[AppStyles.FormInput, {marginTop: 5}]}
                    onPress={() => this.onActionComboboxEmp()}>
                    <Text
                      style={[
                        AppStyles.TextInput,
                        this.state.ListEmpName
                          ? {color: 'black'}
                          : {color: AppColors.gray},
                      ]}>
                      {this.state.ListEmpName
                        ? this.state.ListEmpName
                        : 'Chọn người đi cùng...'}
                    </Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Icon
                        color={AppColors.gray}
                        name={'chevron-thin-down'}
                        type="entypo"
                        size={20}
                      />
                    </View>
                  </TouchableOpacity> */}
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày đăng ký <RequiredText />
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.Date}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        disabled={true}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setDate(date)}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Thời gian bắt đầu
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <TouchableOpacity
                        onPress={() => {
                          this.handlePickTimeOpen('StartTime');
                        }}
                        style={[
                          AppStyles.table_td_custom,
                          {padding: 12, borderWidth: 1},
                        ]}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'center'},
                          ]}>
                          {(this.state.StartTime &&
                            moment(this.state.StartTime).format(
                              'DD/MM/YYYY HH:mm:ss',
                            )) ||
                            ''}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Thời gian kết thúc
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <TouchableOpacity
                        onPress={() => {
                          this.handlePickTimeOpen('EndTime');
                        }}
                        style={[
                          AppStyles.table_td_custom,
                          {padding: 12, borderWidth: 1},
                        ]}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'center'},
                          ]}>
                          {(this.state.EndTime &&
                            moment(this.state.EndTime).format(
                              'DD/MM/YYYY HH:mm:ss',
                            )) ||
                            ''}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Nội dung đi xe</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    value={this.state.Description}
                    onChangeText={Description =>
                      this.setDescription(Description)
                    }
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    value={this.state.Note}
                    onChangeText={Note => this.setNote(Note)}
                  />
                </View>
              </View>
            </ScrollView>

            {/* {this.state.ListEmp.length > 0 ? (
            <Combobox
              TypeSelect={'multiple'} // single or multiple
              callback={this.ChangeEmp}
              data={this.state.ListEmp}
              nameMenu={'Chọn nhóm'}
              eOpen={this.openComboboxEmp}
              position={'bottom'}
              value={[]}
            />
          ) : null} */}

            <View>
              <Button
                buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
                title="Lưu"
                onPress={() => this.Submit()}
              />
            </View>
            {this.state.modalPickerTimeVisible && (
              <DateTimePickerModal
                locale="vi-VN"
                cancelTextIOS="Huỷ"
                confirmTextIOS="Chọn"
                headerTextIOS="Chọn thời gian"
                isVisible={this.state.modalPickerTimeVisible}
                mode="datetime"
                onConfirm={this.handleConfirmTime}
                onCancel={this.hideDatePicker}
              />
            )}
          </Container>
        </View>
      </KeyboardAvoidingView>
    );
  }
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  handlePickTimeOpen = type => {
    this.setState({
      typeTime: type,
      modalPickerTimeVisible: true,
    });
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      typeTime: null,
    });
  };
  handleConfirmTime = date => {
    const {typeTime} = this.state;
    this.setState({[typeTime]: date, typeTime: null});
    this.hideDatePicker();
  };
  _openComboboxEmp() {}
  openComboboxEmp = d => {
    this._openComboboxEmp = d;
  };
  onActionComboboxEmp() {
    this._openComboboxEmp();
  }
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'arrowleft'}
          type="antdesign"
          size={30}
          onPress={() => this.indexRegistercar()}
        />
      </View>
    );
  }
  ChangeEmp = rs => {
    if (!rs || !rs[0].value) {
      return;
    }
    var value = [];
    var list = [];
    for (let i = 0; i < rs.length; i++) {
      if (rs[i].value !== null) {
        list.push(rs[i]);
        value.push({LstReplaceGuid: rs[i].value, ReplaceBy: rs[i].text});
      }
    }
    var name =
      list.length !== 0 && list.length > 1
        ? list.length + ' lựa chọn'
        : list[0].text;
    this.setState({ListEmpName: name, ListReplate: value});
  };
  setPickUpLocation(PickUpLocation) {
    this.setState({PickUpLocation});
  }
  setGetOffLocation(GetOffLocation) {
    this.setState({GetOffLocation});
  }
  setReplaceBy(ReplaceBy) {
    this.setState({ReplaceBy});
  }
  setTolls(Tolls) {
    this.setState({Tolls});
  }
  setDescription(Description) {
    this.setState({Description});
  }
  setNote(Note) {
    this.setState({Note});
  }
  setDate(D) {
    this.setState({Date: new Date(D)});
  }

  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  onValueChange2(value) {
    this.setState({
      CarId: value,
    });
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(addComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
  },
});
