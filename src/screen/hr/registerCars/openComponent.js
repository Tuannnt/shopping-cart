import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {FuncCommon} from '../../../utils';
import {Divider, Icon, Header} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {API_RegisterCars} from '@network';
import API from '../../../network/API';
import {Container, Item, Label, Picker} from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import DatePicker from 'react-native-datepicker';
import _, {endsWith} from 'lodash';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import Toast from 'react-native-simple-toast';

var width = Dimensions.get('window').width; //full width
const ItemCode = {
  PERSONNEL: 0,
  SELL: 1,
  CUSTOMER: 2,
  PURCHASE: 3,
  ACCOUNTANT: 4,
  WAREHOUSE: 5,
  OPERATIONAL: 6,
  DOCUMENT: 7,
  MARKETING: 8,
  PROJECT: 9,
  MANUFACTURING: 10,
  PROPERTY: 11,
  ESTABLISH: 12,
  LOGOUT: 13,
};
class openComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      Data: [],
      listWofflowData: [],
      loading: false,
      isModalVisible: false,
      txtComment: '',
      checkInLogin: '',
      LoginName: '',
      Date: null,
      StartTime: FuncCommon.ConDate(new Date(), 0),
      EndTime: FuncCommon.ConDate(new Date(), 0),
      selected2: [],
      Reciever: [],
      Licars: [],
      itemData: {},
      listReplate: [],
      listReplateString: '',
    };
  }
  componentDidMount(): void {
    Promise.all([
      this.getItem(),
      // this.listReplate()
    ]);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      Promise.all([
        this.getItem(),
        // this.listReplate()
      ]);
    }
  }
  // mo form edit
  callbackOpenUpdate = () => {
    Actions.addRegistercar({
      itemData: this.state.itemData,
      listReplate: this.state.listReplate,
    });
  };
  listReplate = () => {
    API_RegisterCars.GetByListReplate(this.props.RecordGuid)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        this.setState({
          listReplate: _data,
          listReplateString: _data.map(x => x.ReplaceBy).join(','),
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
  getItem() {
    let id = {Id: this.props.RecordGuid, WorkFlowGuid: this.props.WorkFlowGuid};
    API_RegisterCars.Regitercar_Items(id)
      .then(res => {
        let CarId1 = JSON.parse(res.data.data).CarId;
        let date1 = JSON.parse(res.data.data).Date;
        let Date77 = new Date(date1);
        this.state.Data = JSON.parse(res.data.data);
        this.setState({
          Data: this.state.Data,
          itemData: JSON.parse(res.data.data),
          selected2: CarId1,
          Date: Date77,
        });
        this.state.Data.Tolls = this.state.Data.Tolls
          ? this.state.Data.Tolls.toString()
          : '';
        let checkin = {
          RegisterCarGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_RegisterCars.CheckLogin(checkin)
          .then(res => {
            this.setState({checkInLogin: res.data.data});
          })
          .catch(error => {
            Toast.showWithGravity(error.data, Toast.SHORT, Toast.CENTER);
          });
        API.getProfile().then(rs => {
          this.setState({LoginName: rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log(error);
      });
    // this.GetCommentRegisterCars(this.page);
  }
  keyExtractor = (item, index) => index.toString();

  Delete = () => {
    let id = {Id: this.props.RecordGuid};
    this.setState({loading: true});
    API_RegisterCars.Registercars_Delete(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            RegisterCarGuid: this.props.RecordGuid,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        Toast.showWithGravity(error.data, Toast.SHORT, Toast.CENTER);
      });
  };
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#fff',
          }}>
          <TabBar_Title
            title={'Chi tiết phiếu đăng ký xe'}
            callBack={() => this.callBackList()}
          />
          <Divider />
          <View style={{flex: 8}}>
            <View style={{padding: 15}}>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Loại xe :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.Data.CarName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Nơi đi :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.Data.PickUpLocation}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Nơi đến :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.Data.GetOffLocation}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Ngày đăng ký :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.customDate(this.state.Data.Date)}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Thời gian đi :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.ConDate(this.state.Data.StartTime, 1, 'iso')}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Thời gian về :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.ConDate(this.state.Data.EndTime, 1, 'iso')}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Người đi cùng :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {/* {this.state.listReplateString} */}
                    {this.state.Data.ReplaceBy}
                  </Text>
                </View>
              </View>
              {/* <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Phí cầu đường :</Text>
                </View>

                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.addPeriod(this.state.Data.Tolls)}
                  </Text>
                </View>
              </View> */}
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                </View>
                <View style={{flex: 2}}>
                  {this.state.Data.Status === 'Y' ? (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.AcceptColor},
                      ]}>
                      {this.state.Data.StatusWF}
                    </Text>
                  ) : (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.PendingColor},
                      ]}>
                      {this.state.Data.StatusWF}
                    </Text>
                  )}
                </View>
              </View>
            </View>
          </View>

          <View style={{flex: 1}}>
            {this.state.checkInLogin !== '' ? (
              <TabBarBottom
                //key để quay trở lại danh sách
                backListByKey="RegisterCars"
                onDelete={() => this.Delete()}
                callbackOpenUpdate={this.callbackOpenUpdate}
                onUpdate={() => this.Update()}
                keyCommentWF={{
                  ModuleId: 38,
                  RecordGuid: this.props.RecordGuid,
                  Title: this.state.Data.CarId,
                }}
                // tiêu đề hiển thị trong popup xử lý phiếu
                Title={'phiếu đăng ký xe này'}
                //kiểm tra quyền xử lý
                Permisstion={this.state.Data.Permisstion}
                //kiểm tra bước đầu quy trình
                checkInLogin={this.state.checkInLogin}
                onSubmitWF={(callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
                }
              />
            ) : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  ConDate = function(data, number) {
    try {
      if (data == null || data == '') {
        return '';
      }
      if (data !== null && data != '' && data != undefined) {
        try {
          if (data.indexOf('SA') != -1 || data.indexOf('CH') != -1) {
            if (data.indexOf('SA') != -1) {
            }
            if (data.indexOf('CH') != -1) {
            }
          }
          if (data.indexOf('Date') != -1) {
            data = data.substring(data.indexOf('Date') + 5);
            data = parseInt(data);
          }
        } catch (ex) {}
        var newdate = new Date(data);
        if (number == 3) {
          return newdate;
        }
        if (number == 2) {
          return '/Date(' + newdate.getTime() + ')/';
        }
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();
        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;
        if (mm < 10) mm = '0' + mm;
        if (number == 0) {
          return (todayDate = day + '/' + month + '/' + year);
        }
        if (number == 1) {
          return (todayDate =
            day + '/' + month + '/' + year + ' ' + hh + ':' + mm);
        }
        if (number == 4) {
          return new Date(year, month - 1, day);
        }
      } else {
        return '';
      }
    } catch (ex) {
      return '';
    }
  };
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if (csStatus === 'W') {
      stringstatus = 'Chờ duyệt';
    } else if (csStatus == 'R') {
      stringstatus = 'Trả lại';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  onClick(data) {
    this.setState({selectedTab: data, check: false});
    if (data == 'home') {
      Actions.app();
    } else if (data == 'List') {
      Actions.indexRegistercar();
    } else if (data == 'ok' || data == 'no') {
      this.setState({isModalVisible: !this.state.isModalVisible});
    } else if (data == 'comment') {
      Actions.commentRegistercar({RegisterCarGuid: this.props.RegisterCarGuid});
    }
  }
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      RegisterCarGuid: this.state.Data.RegisterCarGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'D') {
      API_RegisterCars.Approve(obj)
        .then(rs => {
          if (rs.data.errorCode === 200) {
            this.closeComment();
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          Toast.showWithGravity(error.data, Toast.SHORT, Toast.CENTER);
        });
    } else {
      API_RegisterCars.NotApprove(obj)
        .then(rs => {
          if (rs.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          Toast.showWithGravity(error.data, Toast.SHORT, Toast.CENTER);
        });
    }
  }
  closeComment() {
    this.setState({isModalVisible: !this.state.isModalVisible});
  }
  GetCommentRegisterCars = page => {
    let obj = {
      page: page,
      ItemPage: this.Length,
      RecordGuid: this.state.Data.RegisterCarGuid,
    };
    this.setState({loading: true});
    API_RegisterCars.GetCommentRegisterCars(obj)
      .then(rs => {
        let listData = this.state.listWofflowData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(rs.data)).data).Data,
          ),
        );
        let data = listData.concat(data1);
        this.setState({listWofflowData: data, loading: false});
        this.arrayholder = data;
        let TotalRow = JSON.parse(JSON.parse(JSON.stringify(rs.data)).data);
        this.TotalRow = TotalRow.Count;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  };
}
const styles = StyleSheet.create({
  styletextLable: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  styletext: {
    fontSize: 14,
    padding: 5,
  },
  textLableCol2: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  textCol2: {
    fontSize: 14,
    padding: 5,
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
  },
});
const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(openComponent);
