import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {
  Header,
  Divider,
  Icon,
  ListItem,
  Badge,
  SearchBar,
} from 'react-native-elements';
import {API_HR, API} from '@network';
import Fonts from '../../../theme/fonts';
import {AppStyles, AppColors} from '@theme';
import {
  TabBar,
  TabBarBottomCustom,
  MenuSearchDate,
  FormSearch,
  LoadingComponent,
} from '@Component';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '@utils';
import controller from './controller';
import listCombobox from './listCombobox';
const SCREEN_WIDTH = Dimensions.get('window').width;
class TimeKeepingsConfirmComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4',
      refreshing: false,
      Data: null,
      ListAll: [],
      ListCountNoti: [],
      ChoDuyet: '',
      TraLai: '',
      EvenFromSearch: false,
    };
    this._search = {
      StartDate: new Date(),
      EndDate: new Date(),
      Status: 'W',
      search: {value: ''},
      CurrentPage: 0,
      Length: 20,
      QueryOrderBy: 'RequiredDate DESC',
      Draw: 2,
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
  }
  componentWillReceiveProps = nextProps => {
    if (nextProps.moduleId == 'Back') {
      this.updateSearch('');
    }
  };

  setStartDate = date => {
    this._search.StartDate = date;
    this.updateSearch(this._search.search.value);
  };
  setEndDate = date => {
    this._search.EndDate = date;
    this.updateSearch(this._search.search.value);
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);
      this.setState({ValueSearchDate: callback.value});
      this.updateSearch(this._search.search.value);
    }
  };
  updateSearch = txt => {
    this._search.search.value = txt;
    this._search.CurrentPage = 0;
    this.setState({ListAll: [], refreshing: true}, () => {
      this.nextPage();
    });
  };
  getDataOnline = () => {
    this._search.CurrentPage++;
    let obj = {
      ...this._search,
      StartDate: FuncCommon.ConDate(this._search.StartDate, 2),
      EndDate: FuncCommon.ConDate(this._search.EndDate, 2),
    };
    controller.getAllTimeKeepingsConfirm(
      obj,
      this.state.ListAll,
      (rs, total) => {
        this.Total = total;
        if (this._search.Status === 'W') {
          this.listtabbarBotom[0].Badge = total;
        }
        FuncCommon.Data_Offline_Set('TimeKeepingsConfirm', rs);
        let res = rs;
        if (this._search.CurrentPage > 1) {
          res = this.state.ListAll.concat(rs);
        }
        this.setState({
          ListAll: res,
          refreshing: false,
        });
      },
    );
  };
  nextPage() {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.getDataOnline();
      } else {
        let data = await FuncCommon.Data_Offline_Get('TimeKeepingsConfirm');
        this.setState({
          ListAll: data,
          refreshing: false,
        });
      }
    });
  }

  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={AppStyles.container}>
          <TabBar
            title={'Xác nhận chấm công'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            addForm={true}
            CallbackFormAdd={() => Actions.timeKeepingsComfirm_Add()}
            BackModuleByCode={'MyProfile'}
          />
          <Divider />
          <View>
            {this.state.EvenFromSearch !== true ? null : (
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                    <Text style={[AppStyles.Labeldefault]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventStartDate: true})}>
                      <Text>
                        {FuncCommon.ConDate(this._search.StartDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                    <Text style={[AppStyles.Labeldefault]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventEndDate: true})}>
                      <Text>{FuncCommon.ConDate(this._search.EndDate, 0)}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'column', padding: 5}}>
                    <Text
                      style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <FormSearch
                  CallbackSearch={callback => this.updateSearch(callback)}
                />
              </View>
            )}
          </View>
          <View style={{flex: 1}}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.onCallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  // view hiển thị
  CustomeListAll = item => (
    <View style={{flex: 30}}>
      <FlatList
        ListEmptyComponent={this.ListEmpty}
        data={item}
        style={{height: '100%', marginBottom: 60}}
        renderItem={({item, index}) => (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              borderBottomWidth: 0.5,
              borderBottomColor: AppColors.gray,
            }}
            onPress={() => this.onViewItem(item.RowGuid)}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View>
                <Text style={[AppStyles.Titledefault]}>
                  {item.TimeConfirmId}
                </Text>
                <Text style={AppStyles.Textdefault}>{item.EmployeeName}</Text>
                <Text style={AppStyles.Textdefault}>{item.DepartmentName}</Text>
              </View>
            </View>
            <View
              style={{justifyContent: 'flex-start', alignItems: 'flex-end'}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {
                    textAlign: 'right',
                    color:
                      item.Status == 'Y'
                        ? AppColors.AcceptColor
                        : AppColors.PendingColor,
                  },
                ]}>
                {item.StatusWF}
              </Text>
              <Text style={[AppStyles.Textdefault]}>
                {FuncCommon.ConDate(item.RequiredDate, 0)}
              </Text>
            </View>
          </TouchableOpacity>
        )}
        onEndReached={() => {
          if (this.Total > this.state.ListAll.length) {
            this.nextPage(false);
          }
        }}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    </View>
  );
  ListEmpty = () => {
    if (this.state.ListAll.length > 0) return null;
    return (
      <View>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  onCallbackValueBottom(value) {
    switch (value) {
      case 'profile':
        this._search.Status = 'W';
        this.updateSearch('');
        break;
      case 'checkcircleo':
        this._search.Status = 'Y';
        this.updateSearch('');
        break;
      default:
        break;
    }
  }
  //view item,click sang view khác
  onViewItem(id) {
    Actions.GetTimeKeepingsConfirm({RowGuid: id});
  }
  //Load lại
  _onRefresh = () => {
    this.setState({refreshing: true});
    this.updateSearch(this._search.search.value);
  };
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(TimeKeepingsConfirmComponent);
