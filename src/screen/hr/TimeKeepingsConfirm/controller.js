import {API_TimeKeepingsComfirm, API} from '@network';
import Toast from 'react-native-simple-toast';
import {FuncCommon} from '../../../utils';
import moment from 'moment';
import _ from 'lodash';
export default {
  getAllTimeKeepingsConfirm(data, list, callback) {
    API_TimeKeepingsComfirm.getAllTimeKeepingsConfirm(data)
      .then(res => {
        console.log(res);
        let _data = JSON.parse(res.data.data);
        var data = _data.data;
        for (let i = 0; i < data.length; i++) {
          list.push(data[i]);
        }
        let total = _data.recordsFiltered || 0;
        callback(list, total);
      })
      .catch(error => {
        console.log(error);
      });
  },
  Insert_TimeKeepings(state, callback) {
    let isNotValidated = false;
    if (!state.rows || state.rows.length === 0) {
      Toast.showWithGravity(`Yêu cầu nhập chi tiết`, Toast.SHORT, Toast.CENTER);
      return;
    }
    for (let i = 0; i < state.rows.length; i++) {
      let lst = state.rows[i];
      if (!lst.EmployeeId) {
        Toast.showWithGravity(
          `Bạn chưa chọn nhân viên ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
      if (!lst.StartTime) {
        Toast.showWithGravity(
          `Bạn chưa chọn giờ điều chỉnh vào ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
      if (!lst.EndTime) {
        Toast.showWithGravity(
          `Bạn chưa chọn giờ điều chỉnh ra ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }

    var insert = {
      RequiredDate: FuncCommon.ConDate(state.RequiredDate, 2),
    };
    let detail = _.cloneDeep(state.rows).map(row => ({
      Id: null,
      EmployeeId: row.EmployeeId,
      StartTime: FuncCommon.ConDate(row.StartTime, 6),
      EndTime: FuncCommon.ConDate(row.EndTime, 6),
      Note: row.Note,
    }));
    let _data = new FormData();
    _data.append('Insert', JSON.stringify(insert));
    _data.append('Detail', JSON.stringify(detail));
    API_TimeKeepingsComfirm.Insert_TimeKeepings(_data)
      .then(res => {
        if (res.data.errorCode !== 200) {
          Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
          return;
        }
        Toast.showWithGravity('Thêm mới thành công', Toast.SHORT, Toast.CENTER);
        callback(res);
      })
      .catch(error => {
        console.log(error);
      });
  },
  Insert(state, callback) {
    let isNotValidated = false;
    if (!state.rows || state.rows.length === 0) {
      Toast.showWithGravity(`Yêu cầu nhập chi tiết`, Toast.SHORT, Toast.CENTER);
      return;
    }
    for (let i = 0; i < state.rows.length; i++) {
      let lst = state.rows[i];
      // if (!lst.RequiredDate) {
      //   Toast.showWithGravity(
      //     `Bạn chưa chọn ngày ở dòng ${i + 1}`,
      //     Toast.SHORT,
      //     Toast.CENTER,
      //   );
      //   isNotValidated = true;
      //   return;
      // }
      if (!lst.CheckInDc) {
        Toast.showWithGravity(
          `Bạn chưa chọn giờ điều chỉnh vào ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
      if (!lst.CheckOutDc) {
        Toast.showWithGravity(
          `Bạn chưa chọn giờ điều chỉnh ra ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }
    var insert = {
      IsLocked: null,
      Status: null,
      DepartmentID: null,
      EmployeeId: state.EmployeeId,
      EmployeeName: state.EmployeeName || '',
      FileAttachments: [],
      Description: state.Description,
      RequiredDate: FuncCommon.ConDate(state.RequiredDate, 2),
    };
    let detail = [...state.rows].map(row => {
      return {
        Id: null,
        EmployeeId: row.EmployeeId,
        StartTime: row.CheckInDc
          ? moment(row.CheckInDc).format('HH:mm:ss')
          : null,
        EndTime: row.CheckOutDc
          ? moment(row.CheckOutDc).format('HH:mm:ss')
          : null,
        Note: row.Note,
        // RequiredDate: FuncCommon.ConDate(row.RequiredDate, 2),
      };
    });
    let _data = new FormData();
    _data.append('Insert', JSON.stringify(insert));
    _data.append('Detail', JSON.stringify(detail));
    API_TimeKeepingsComfirm.InsertResignationForms(_data)
      .then(res => {
        if (res.data.errorCode !== 200) {
          Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
          return;
        }
        Toast.showWithGravity('Thêm mới thành công', Toast.SHORT, Toast.CENTER);
        callback(res);
      })
      .catch(error => {
        console.log(error);
      });
  },
  Update({state, itemData}, callback) {
    let isNotValidated = false;
    if (!state.rows || state.rows.length === 0) {
      Toast.showWithGravity(`Yêu cầu nhập chi tiết`, Toast.SHORT, Toast.CENTER);
      return;
    }
    for (let i = 0; i < state.rows.length; i++) {
      let lst = state.rows[i];
      // if (!lst.RequiredDate) {
      //   Toast.showWithGravity(
      //     `Bạn chưa chọn ngày ở dòng ${i + 1}`,
      //     Toast.SHORT,
      //     Toast.CENTER,
      //   );
      //   isNotValidated = true;
      //   return;
      // }
      if (!lst.CheckInDc) {
        Toast.showWithGravity(
          `Bạn chưa chọn giờ điều chỉnh vào ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
      if (!lst.CheckOutDc) {
        Toast.showWithGravity(
          `Bạn chưa chọn giờ điều chỉnh ra ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }
    var insert = {
      ...itemData,
      EmployeeId: state.EmployeeId,
      Description: state.Description,
      RequiredDate: FuncCommon.ConDate(state.RequiredDate, 2),
    };
    let detail = _.cloneDeep(state.rows).map(row => ({
      Id: null,
      EmployeeId: row.EmployeeId,
      StartTime: FuncCommon.ConDate(row.CheckInDc, 6),
      EndTime: FuncCommon.ConDate(row.CheckOutDc, 6),
      Note: row.Note,
      // RequiredDate: FuncCommon.ConDate(row.RequiredDate, 2),
    }));
    let _data = new FormData();
    _data.append('Insert', JSON.stringify(insert));
    _data.append('Detail', JSON.stringify(detail));
    _data.append('detaildelete', JSON.stringify(state.DeleteDetail));
    API_TimeKeepingsComfirm.Update(_data)
      .then(res => {
        if (res.data.errorCode !== 200) {
          Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
          return;
        }
        callback(res);
        return;
      })
      .catch(error => {
        console.log(error);
      });
  },
  TimeKeepingsConfirm_Items(RowGuid, callback) {
    let id = {Id: RowGuid};
    API_TimeKeepingsComfirm.TimeKeepingsConfirm_Items(id)
      .then(res => {
        callback(res);
      })
      .catch(error => {
        console.log(error);
      });
  },
};
