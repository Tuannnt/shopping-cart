import _ from 'lodash';
import React, {Component} from 'react';
import {
  KeyboardAvoidingView,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text,
  ScrollView,
  View,
  Dimensions,
  TextInput,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {DatePicker as DatePicker2} from 'react-native-date-picker';
import DatePicker from 'react-native-datepicker';
import {AppStyles, AppColors} from '@theme';
import {Button, Icon} from 'react-native-elements';
import {
  LoadingComponent,
  TabBar_Title,
  RequiredText,
  Combobox,
  ComboboxV2,
  OpenPhotoLibrary,
} from '@Component';
import controller from './controller';
import {FuncCommon} from '../../../utils';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import {API_TimeKeepingsComfirm, API} from '@network';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
const SCREEN_WIDTH = Dimensions.get('window').width;
const styles = StyleSheet.create({
  sty_Date: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    position: 'absolute',
    bottom: 0,
    zIndex: 1000,
    width: SCREEN_WIDTH,
  },
});
const headerTable = [
  {title: 'STT', width: 40},
  // {title: 'Ngày', width: 200},
  {title: 'Nhân viên', width: 250},
  {title: 'Điều chỉnh vào', width: 100},
  {title: 'Điều chỉnh ra', width: 100},
  {title: 'Ghi chú', width: 200},
  {title: 'Hành động', width: 80},
];
class TimeKeepingsComfirm_Add_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      RequiredDate: new Date(),
      setCheckInDc: false,
      CheckInDc: new Date(),
      setCheckOutDc: false,
      CheckOutDc: new Date(),
      Note: '',
      rows: [{}],
      Date: new Date(),
      ListEmployee: [],
    };
  }
  componentDidMount = () => {
    this.getAllEmployeeByDepart();
    this.setState({RequiredDate: new Date(this.props.RequiredDate)});
  };
  getAllEmployeeByDepart = () => {
    API_TimeKeepingsComfirm.getEmployeeJexcel()
      .then(rs => {
        let data = JSON.parse(rs.data.data).map(item => ({
          value: item.id,
          text: item.name,
        }));

        console.log(data);
        this.setState({ListEmployee: data});
      })
      .catch(error => console.log(error.data.data));
  };
  GetTimekeeping = () => {
    let obj = {
      totalItems: 0,
      CurrentPage: 1,
      pageSize: 15,
      maxSize: 5,
      Length: 1000,
      Title: '',
      lstDepartmentID: null,
      Day: this.state.RequiredDate.getDate() + '',
      Month: this.state.RequiredDate.getMonth() + 1 + '',
      Years: this.state.RequiredDate.getFullYear() + '',
      DepartmentID: [],
      TimeKeepingGuid: null,
      EmployeeName: null,
    };

    API_TimeKeepingsComfirm.GetTimekeeping(obj)
      .then(res => {
        console.log(res);
        let _data =
          JSON.parse(res.data.data).filter(
            time => !time.CheckIn || !time.CheckOut,
          ) || [];
        let rows = _data.map(row => {
          if (row.CheckInDc) {
            row.CheckInDc = FuncCommon.ConDate(row.CheckInDc, 8);
          }
          if (row.CheckOutDc) {
            console.log(row.CheckOutDc);
            row.CheckOutDc = FuncCommon.ConDate(row.CheckOutDc, 8);
          }
          return row;
        });
        console.log(_data);
        this.setState({rows});
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  setDate(D) {
    this.setState({RequiredDate: FuncCommon.ConDate(D, 99)});
  }
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{backgroundColor: '#fff', height: '100%'}}>
        <TabBar_Title
          title={'Thêm nhân viên chấm công'}
          callBack={() => this.BackList()}
        />
        <ScrollView>
          <View
            style={{
              padding: 10,
              paddingBottom: 0,
              flexDirection: 'column',
            }}>
            <View style={{flexDirection: 'row', marginBottom: 5}}>
              <Text style={AppStyles.Labeldefault}>
                Ngày yêu cầu <RequiredText />
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '100%'}}>
                <DatePicker
                  locale="vie"
                  locale={'vi'}
                  style={{width: '100%'}}
                  date={this.state.RequiredDate}
                  mode="date"
                  androidMode="spinner"
                  placeholder="Chọn ngày"
                  format="DD/MM/YYYY"
                  confirmBtnText="Xong"
                  cancelBtnText="Huỷ"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      right: 0,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      marginRight: 36,
                    },
                  }}
                  disabled={true}
                  onDateChange={date => this.setDate(date)}
                />
              </View>
            </View>
          </View>

          <Text style={{fontSize: 16, fontWeight: 'bold', padding: 10}}>
            Chi tiết phiếu
          </Text>
          {/* Table */}
          <View style={{marginBottom: 20}}>
            {!this.props.itemData && (
              <View style={{width: 150, marginLeft: 10}}>
                <Button
                  title="Thêm mới dòng"
                  type="outline"
                  size={15}
                  onPress={() => this.addRows()}
                  buttonStyle={{padding: 5, marginBottom: 5}}
                  titleStyle={{
                    marginLeft: 5,
                    fontSize: 14,
                    fontWeight: 'normal',
                  }}
                  icon={
                    <Icon
                      name="pluscircleo"
                      type="antdesign"
                      color={AppColors.blue}
                      size={14}
                    />
                  }
                />
              </View>
            )}
            <ScrollView
              horizontal={true}
              style={{marginTop: 5, marginHorizontal: 10}}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  {headerTable.map(item => {
                    return (
                      <TouchableOpacity
                        key={item.title}
                        style={[AppStyles.table_th, {width: item.width}]}>
                        <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>
                <View>
                  {this.state.rows.length > 0 ? (
                    <View>
                      <FlatList
                        data={this.state.rows}
                        renderItem={({item, index}) => {
                          return this.itemFlatList(item, index);
                        }}
                        keyExtractor={(rs, index) => index.toString()}
                      />
                    </View>
                  ) : (
                    <TouchableOpacity style={[AppStyles.table_foot]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                        Không có dữ liệu!
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ScrollView>
          </View>
        </ScrollView>

        <View style={{flexDirection: 'row', margin: 10}}>
          <TouchableOpacity
            style={[
              AppStyles.containerCentered,
              {
                flex: 1,
                padding: 10,
                borderRadius: 10,
                marginLeft: 5,
                backgroundColor: AppColors.ColorButtonSubmit,
              },
            ]}
            onPress={() => this.Insert()}>
            <Text style={[AppStyles.Titledefault, {color: '#fff'}]}>Lưu</Text>
          </TouchableOpacity>
        </View>
        {this.state.modalPickerTimeVisible && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisible}
            mode="time"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        {this.state.modalPickerTimeVisibleDate && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisibleDate}
            mode="date"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        {this.state.ListEmployee.length > 0 && (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeOrder}
            data={this.state.ListEmployee}
            nameMenu={'Chọn'}
            eOpen={this.openComboboxOrder}
            position={'bottom'}
            value={null}
          />
        )}
      </KeyboardAvoidingView>
    );
  }
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({
      rows: res,
      typeTime: null,
      currentIndex: null,
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
    });
    // this.hideDatePicker();
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  onDelete = index => {
    const {rows} = this.state;
    if (rows.length === 1) {
      Toast.showWithGravity(
        'Không được xóa dòng cuối cùng này',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);
    this.setState({rows: res});
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  itemFlatList = (row, index) => {
    const stylePicker = {
      inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
      },
      inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderWidth: 0,
        borderColor: 'black',
        borderRadius: 5,
        color: 'black',
        paddingRight: 10,
        // to ensure the text is never behind the icon
      },
    };

    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.getWidth(1)}]}
          onPress={() => {
            this.openOrders(index);
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.EmployeeId,
                {color: 'black', fontSize: 14},
              ]}>
              {row.EmployeeId ? row.EmployeeName : ''}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <Icon
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('StartTime', index);
          }}
          style={[AppStyles.table_td_custom, {width: this.getWidth(2)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.StartTime && moment(row.StartTime).format('HH:mm')) || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('EndTime', index);
          }}
          style={[AppStyles.table_td_custom, {width: this.getWidth(3)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.EndTime && moment(row.EndTime).format('HH:mm')) || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.getWidth(4)}]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              {color: 'black', marginVertical: 2},
            ]}
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={Note => {
              this.handleChangeRows(Note, index, 'Note');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td_custom, {width: 80}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  _openComboboxOrder() {}
  openComboboxOrder = d => {
    this._openComboboxOrder = d;
  };
  openOrders(index) {
    this.setState({currentIndexItem: index}, () => {
      this._openComboboxOrder();
    });
  }
  ChangeOrder = rs => {
    const {currentIndexItem, rows} = this.state;
    let {value, text} = rs;
    if (!rs) {
      return;
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.EmployeeId = value;
        row.EmployeeName = text;
      }
      return row;
    });
    this.setState({rows: data, currentIndexItem: null});
  };
  getWidth = index => {
    return headerTable[index].width;
  };
  settxt = (key, txt) => {
    this.setState({[key]: txt});
  };
  BackList() {
    Actions.pop();
    Actions.refresh({moduleId: 'BackToAdd', ActionTime: new Date().getTime()});
  }
  Insert = () => {
    controller.Insert_TimeKeepings(this.state, rs => {
      if (rs.data.errorCode === 200) {
        this.BackList();
      }
    });
  };
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(TimeKeepingsComfirm_Add_Component);
