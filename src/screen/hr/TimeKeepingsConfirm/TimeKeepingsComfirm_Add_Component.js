import _ from 'lodash';
import React, {Component} from 'react';
import {
  KeyboardAvoidingView,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text,
  ScrollView,
  View,
  Dimensions,
  TextInput,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {DatePicker as DatePicker2} from 'react-native-date-picker';
import DatePicker from 'react-native-datepicker';
import {AppStyles, AppColors} from '@theme';
import {Button, Icon} from 'react-native-elements';
import {
  LoadingComponent,
  TabBar,
  RequiredText,
  Combobox,
  ComboboxV2,
  OpenPhotoLibrary,
} from '@Component';
import controller from './controller';
import {FuncCommon} from '../../../utils';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import {API_TimeKeepingsComfirm, API} from '@network';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import RNPickerSelect from 'react-native-picker-select';

const SCREEN_WIDTH = Dimensions.get('window').width;
const picker = {
  inputIOS: {
    padding: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    padding: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
    borderRadius: 2,
  },
};
const styles = StyleSheet.create({
  sty_Date: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    position: 'absolute',
    bottom: 0,
    zIndex: 1000,
    width: SCREEN_WIDTH,
  },
});
const headerTable = [
  {title: 'STT', width: 40},
  // {title: 'Ngày', width: 200},
  {title: 'Mã nhân viên', width: 200},
  {title: 'Tên nhân viên', width: 200},
  {title: 'Giờ vào', width: 100},
  {title: 'Giờ ra', width: 100},
  {title: 'Điều chỉnh vào', width: 100},
  {title: 'Điều chỉnh ra', width: 100},
  {title: 'Ghi chú', width: 200},
  {title: 'Hành động', width: 80},
];
class TimeKeepingsComfirm_Add_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      setEventRequiredDate: false,
      RequiredDate: new Date(),
      setCheckInDc: false,
      CheckInDc: new Date(),
      setCheckOutDc: false,
      CheckOutDc: new Date(),
      Note: '',
      rows: [],
      Date: new Date(),
      DeleteDetail: [],
      ListEmployee: [],
    };
    this.refreshing = false;
  }
  componentWillReceiveProps = nextProps => {
    if (nextProps.moduleId == 'BackToAdd') {
      this.GetTimekeeping();
    }
  };
  componentDidMount = () => {
    const {itemData, rowData} = this.props;
    this.getEmployees();
    if (itemData && rowData) {
      this.setState({
        rows: _.cloneDeep(rowData).map(row => {
          if (row.CheckInDc) {
            row.CheckInDc = FuncCommon.ConDate(row.CheckInDc, 8);
          }
          if (row.CheckOutDc) {
            row.CheckOutDc = FuncCommon.ConDate(row.CheckOutDc, 8);
          }
          // if (row.DateCheck) {
          //   row.RequiredDate = FuncCommon.ConDate('' + row.DateCheck, 12);
          // }
          return row;
        }),
        Description: itemData.Description,
        EmployeeId: itemData.EmployeeId,
        RequiredDate: new Date(itemData.RequiredDate),
      });
    } else {
      this.GetTimekeeping();
      this.setState({
        EmployeeId: global.__appSIGNALR.SIGNALR_object.USER.EmployeeId,
      });
    }
  };
  GetTimekeeping = () => {
    let obj = {
      totalItems: 0,
      CurrentPage: 1,
      pageSize: 15,
      maxSize: 5,
      Length: 1000,
      Title: '',
      lstDepartmentID: null,
      Day: this.state.RequiredDate.getDate() + '',
      Month: this.state.RequiredDate.getMonth() + 1 + '',
      Years: this.state.RequiredDate.getFullYear() + '',
      DepartmentID: [],
      TimeKeepingGuid: null,
      EmployeeName: null,
    };

    API_TimeKeepingsComfirm.GetTimekeeping(obj)
      .then(res => {
        let _data = JSON.parse(res.data.data).map(value => {
          let CheckInDc = null;
          let CheckOutDc = null;
          if (value.CheckInDc != null && value.CheckOutDc != null) {
            CheckInDc =
              value.CheckInDc != null && value.CheckInDc != ''
                ? '2020-01-01' +
                  ' ' +
                  value.CheckInDc.split(':')[0] +
                  ':' +
                  value.CheckInDc.split(':')[1] +
                  ':00'
                : null;
            CheckOutDc =
              value.CheckOutDc != null && value.CheckOutDc != ''
                ? '2020-01-01' +
                  ' ' +
                  value.CheckOutDc.split(':')[0] +
                  ':' +
                  value.CheckOutDc.split(':')[1] +
                  ':00'
                : null;
          } else {
            CheckInDc =
              value.CheckIn != null && value.CheckIn != ''
                ? '2020-01-01' +
                  ' ' +
                  (+value.CheckIn.split(':')[0] < 10
                    ? '0' + value.CheckIn.split(':')[0]
                    : value.CheckIn.split(':')[0]) +
                  ':' +
                  value.CheckIn.split(':')[1] +
                  ':00'
                : null;
            CheckOutDc =
              value.CheckOut != null && value.CheckOut != ''
                ? '2020-01-01' +
                  ' ' +
                  (+value.CheckOut.split(':')[0] < 10
                    ? '0' + value.CheckOut.split(':')[0]
                    : value.CheckOut.split(':')[0]) +
                  ':' +
                  value.CheckOut.split(':')[1] +
                  ':00'
                : null;
          }
          if (
            value.HoursWork === null ||
            value.HoursWork === 0 ||
            value.CheckIn === null ||
            value.CheckIn === '' ||
            value.CheckOut === null ||
            value.CheckOut === ''
          ) {
            value.append = true;
          }
          value.CheckInDc = CheckInDc;
          value.CheckOutDc = CheckOutDc;
          return value;
        });
        let rows = [..._data.filter(x => x.append)];
        this.setState({rows});
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  getEmployees = () => {
    API_TimeKeepingsComfirm.GetEmployees()
      .then(res => {
        let _data = JSON.parse(res.data.data);
        let ListEmployee =
          _data.map(emp => ({
            value: emp.EmployeeId,
            text: `[${emp.EmployeeId}]-${emp.FullName}`,
          })) || [];
        this.setState({ListEmployee});
      })
      .catch(error => {
        console.log(error.data);
      });
  };
  setDate(D) {
    this.setState({RequiredDate: FuncCommon.ConDate(D, 99)}, () => {
      this.GetTimekeeping();
    });
  }
  _openComboboxEmployees() {}
  openComboboxEmployees = d => {
    this._openComboboxEmployees = d;
  };
  onActionEmployees() {
    this._openComboboxEmployees();
  }
  ChangeEmployees = rs => {
    if (rs.value !== null) {
      this.onChangeState('EmployeeId', rs.value);
      this.onChangeState('EmployeeName', rs.text);
    }
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <View style={[AppStyles.container]}>
          <TabBar
            title={
              this.props.itemData
                ? 'Sửa xác nhận chấm công'
                : 'Thêm xác nhận chấm công'
            }
            callBack={() => this.BackList()}
            addForm={true}
            CallbackFormAdd={() =>
              Actions.AddEmployeeCheckIn({
                RequiredDate: this.state.RequiredDate,
              })
            }
          />
          <ScrollView>
            <View
              style={{
                padding: 10,
                paddingBottom: 0,
                flexDirection: 'column',
              }}>
              <View style={{flexDirection: 'row', marginBottom: 5}}>
                <Text style={AppStyles.Labeldefault}>
                  Ngày <RequiredText />
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{width: '100%'}}>
                  <DatePicker
                    locale="vie"
                    locale={'vi'}
                    style={{width: '100%'}}
                    date={this.state.RequiredDate}
                    mode="date"
                    androidMode="spinner"
                    placeholder="Chọn ngày"
                    format="DD/MM/YYYY"
                    confirmBtnText="Xong"
                    cancelBtnText="Huỷ"
                    disabled={this.props.itemData}
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: 4,
                        marginLeft: 0,
                      },
                      dateInput: {
                        marginRight: 36,
                      },
                    }}
                    onDateChange={date => this.setDate(date)}
                  />
                </View>
              </View>
            </View>
            <View style={{padding: 10}}>
              <Text style={AppStyles.Labeldefault}>
                Nhân viên yêu cầu <RequiredText />
              </Text>
              <TouchableOpacity
                style={[AppStyles.FormInput]}
                onPress={() =>
                  this.props.itemData ? null : this.onActionEmployees()
                }>
                <Text
                  style={[
                    AppStyles.TextInput,
                    this.state.EmployeeId == null
                      ? {color: AppColors.gray}
                      : {color: 'black'},
                  ]}>
                  {this.state.EmployeeId !== null
                    ? this.state.EmployeeName
                    : 'Chọn nhân viên'}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: 1, padding: 10}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
              </View>
              <TextInput
                style={[AppStyles.FormInput, {color: 'black'}]}
                underlineColorAndroid="transparent"
                placeholder="Nhập lý do"
                autoCapitalize="none"
                value={this.state.Description}
                onChangeText={txt => this.settxt('Description', txt)}
              />
            </View>

            <Text style={{fontSize: 16, fontWeight: 'bold', padding: 10}}>
              Chi tiết phiếu
            </Text>
            {/* Table */}
            <View style={{marginBottom: 20}}>
              {/* {!this.props.itemData && (
                  <View style={{ width: 150, marginLeft: 10 }}>
                    <Button
                      title="Thêm mới dòng"
                      type="outline"
                      size={15}
                      onPress={() => this.addRows()}
                      buttonStyle={{ padding: 5, marginBottom: 5 }}
                      titleStyle={{ marginLeft: 5, fontSize: 14, fontWeight: 'normal' }}
                      icon={
                        <Icon
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>
                )} */}
              {/* Chi them dong chi tiet khi them moi, chinh sua ko them */}
              <ScrollView
                horizontal={true}
                style={{marginTop: 5, marginHorizontal: 10}}>
                <View style={{flexDirection: 'column'}}>
                  <View style={{flexDirection: 'row'}}>
                    {headerTable.map(item => {
                      return (
                        <TouchableOpacity
                          key={item.title}
                          style={[AppStyles.table_th, {width: item.width}]}>
                          <Text style={AppStyles.Labeldefault}>
                            {item.title}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                  <View>
                    {this.state.rows.length > 0 ? (
                      <View>
                        <FlatList
                          data={this.state.rows}
                          renderItem={({item, index}) => {
                            return this.itemFlatList(item, index);
                          }}
                          keyExtractor={(rs, index) => index.toString()}
                        />
                      </View>
                    ) : (
                      <TouchableOpacity style={[AppStyles.table_foot]}>
                        <Text
                          style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                          Không có dữ liệu!
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              </ScrollView>
            </View>
            {this.state.setEventRequiredDate === true ? (
              <View style={styles.sty_Date}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({setEventRequiredDate: false});
                  }}>
                  <Text style={{textAlign: 'right'}}>Xong</Text>
                </TouchableOpacity>
                <DatePicker2
                  locale="vie"
                  date={this.state.RequiredDate}
                  mode="date"
                  style={{width: SCREEN_WIDTH}}
                  onDateChange={txt => {
                    this.settxt('RequiredDate', txt),
                      this.settxt('CheckInDc', txt),
                      this.settxt('CheckOutDc', txt);
                  }}
                />
              </View>
            ) : null}
            {this.state.setCheckInDc === true ? (
              <View style={styles.sty_Date}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({setCheckInDc: false});
                  }}>
                  <Text style={{textAlign: 'right'}}>Xong</Text>
                </TouchableOpacity>
                <DatePicker2
                  locale="vie"
                  date={this.state.CheckInDc}
                  mode="time"
                  style={{width: SCREEN_WIDTH}}
                  onDateChange={txt => {
                    this.settxt('CheckInDc', txt);
                  }}
                />
              </View>
            ) : null}
            {this.state.setCheckOutDc === true ? (
              <View style={styles.sty_Date}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({setCheckOutDc: false});
                  }}>
                  <Text style={{textAlign: 'right'}}>Xong</Text>
                </TouchableOpacity>
                <DatePicker2
                  locale="vie"
                  date={this.state.CheckOutDc}
                  mode="time"
                  style={{width: SCREEN_WIDTH}}
                  onDateChange={txt => this.settxt('CheckOutDc', txt)}
                />
              </View>
            ) : null}
          </ScrollView>

          <View style={{flexDirection: 'row', margin: 10}}>
            <TouchableOpacity
              style={[
                AppStyles.containerCentered,
                {
                  flex: 1,
                  padding: 10,
                  borderRadius: 10,
                  marginLeft: 5,
                  backgroundColor: AppColors.ColorButtonSubmit,
                },
              ]}
              onPress={() => this.Insert()}>
              <Text style={[AppStyles.Titledefault, {color: '#fff'}]}>Lưu</Text>
            </TouchableOpacity>
          </View>
          {this.state.modalPickerTimeVisible && (
            <DateTimePickerModal
              locale="vi-VN"
              cancelTextIOS="Huỷ"
              confirmTextIOS="Chọn"
              headerTextIOS="Chọn thời gian"
              isVisible={this.state.modalPickerTimeVisible}
              mode="time"
              onConfirm={this.handleConfirmTime}
              onCancel={this.hideDatePicker}
            />
          )}
          {this.state.modalPickerTimeVisibleDate && (
            <DateTimePickerModal
              locale="vi-VN"
              cancelTextIOS="Huỷ"
              confirmTextIOS="Chọn"
              headerTextIOS="Chọn thời gian"
              isVisible={this.state.modalPickerTimeVisibleDate}
              mode="date"
              onConfirm={this.handleConfirmTime}
              onCancel={this.hideDatePicker}
            />
          )}
          {this.state.ListEmployee.length === 0 ? null : (
            <Combobox
              value={this.state.EmployeeId || null}
              TypeSelect={'single'}
              callback={this.ChangeEmployees}
              data={this.state.ListEmployee}
              nameMenu={'Chọn nhân viên'}
              eOpen={this.openComboboxEmployees}
              position={'bottom'}
            />
          )}
        </View>
      </KeyboardAvoidingView>
    );
  }
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({
      rows: res,
      typeTime: null,
      currentIndex: null,
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
    });
    // this.hideDatePicker();
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  onDelete = index => {
    const {rows, DeleteDetail} = this.state;
    if (rows.length === 1) {
      Toast.showWithGravity(
        'Không được xóa dòng cuối cùng này',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let _deleteDetail = [];
    let row = rows.find((row, i) => i == index);
    if (row.TimeKeepingGuid) {
      _deleteDetail = [...DeleteDetail, row.TimeKeepingGuid];
    }
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res, DeleteDetail: _deleteDetail});
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: this.getWidth(1)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.EmployeeId || ''}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: this.getWidth(2)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.EmployeeName || ''}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: this.getWidth(3)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.CheckIn || ''}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: this.getWidth(4)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.CheckOut || ''}
          </Text>
        </View>

        {/* <TouchableOpacity
          onPress={() => {
            if (this.props.itemData) {
              return;
            }
            this.handlePickDateOpen('RequiredDate', index);
          }}
          style={[AppStyles.table_td_custom, {width: this.getWidth(1)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.RequiredDate &&
              moment(row.RequiredDate).format('DD/MM/YYYY')) ||
              ''}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('CheckInDc', index);
          }}
          style={[AppStyles.table_td_custom, {width: this.getWidth(5)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.CheckInDc && moment(row.CheckInDc).format('HH:mm')) || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('CheckOutDc', index);
          }}
          style={[AppStyles.table_td_custom, {width: this.getWidth(6)}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.CheckOutDc && moment(row.CheckOutDc).format('HH:mm')) || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.getWidth(7)}]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              {color: 'black', marginVertical: 2},
            ]}
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={Note => {
              this.handleChangeRows(Note, index, 'Note');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td_custom, {width: 80}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  getWidth = index => {
    return headerTable[index].width;
  };
  settxt = (key, txt) => {
    this.setState({[key]: txt});
  };
  BackList() {
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  Insert = () => {
    if (this.refreshing) {
      return;
    }
    this.refreshing = true;
    setTimeout(() => {
      this.refreshing = false;
    }, 2000);
    if (this.props.itemData) {
      controller.Update(
        {
          state: this.state,
          itemData: this.props.itemData,
          rowData: this.props.rowData,
        },
        rs => {
          Toast.showWithGravity(
            'Chỉnh sửa thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.BackList();
        },
      );
      return;
    }
    controller.Insert(this.state, rs => {
      if (rs.data.errorCode === 200) {
        this.BackList();
      }
    });
  };
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(TimeKeepingsComfirm_Add_Component);
