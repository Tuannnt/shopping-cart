import _ from 'lodash';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';
import {Icon, Header, Divider} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {API_TimeKeepingsComfirm, API_HR, API} from '@network';
import {Container, ListItem} from 'native-base';
import {AppStyles, AppColors} from '@theme';
import {TabBar_Title, TabBarBottom, LoadingComponent} from '@Component';
import controller from './controller';
import {FuncCommon} from '../../../utils';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
class GetTimeKeepingsConfirm extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      staticParam: {
        TimeConfirmGuid: '',
      },
      TimeConfirmGuid: props.RowGuid,
      Data: [],
      listWofflowData: [],
      loading: false,
      isModalVisible: false,
      txtComment: '',
      checkInLogin: '',
      LoginName: '',
      Date: null,
      StartTime: null,
      EndTime: null,
      selected2: [],
      Reciever: [],
      Licars: [
        {
          CarName: 'Chọn xe',
          CarId: '',
        },
      ],
    };
  }
  componentDidMount = () => {
    Promise.all([this.getItem(), this.GetAll()]);
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      Promise.all([this.getItem(), this.GetAll()]);
    }
  }
  getItem() {
    let guid = this.props.RowGuid || this.props.RecordGuid;
    controller.TimeKeepingsConfirm_Items(guid, res => {
      if (res.data.data === 'null') {
        return;
      }
      if (res.data.errorCode !== 200) {
        // Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
        this.onPressBack();
      } else {
        let _data = JSON.parse(res.data.data);
        this.setState({
          Data: _data,
        });
        // let obj = {
        //   RowGuid: this.props.RowGuid,
        //   WorkFlowGuid: _data.WorkFlowGuid,
        // };
        // API_TimeKeepingsComfirm.CheckLoginTimeKeeping(obj)
        //   .then(Response => {
        //     this.setState({
        //       checkInLogin: Response.data.data,
        //       Data: _data,
        //     });
        //   })
        //   .catch(error => {
        //     console.log(error.data);
        //   });
      }
    });
  }

  GetAll = () => {
    let guid = this.props.RowGuid || this.props.RecordGuid;
    API_HR.TimeKeepingsConfirm_ListDetail(guid)
      .then(res => {
        this.state.list = JSON.parse(res.data.data);
        console.log('===========> xxxxxxxxx' + res.data.data);
        this.setState({list: this.state.list});
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <TabBar_Title
          title={'Chi tiết xác nhận chấm công'}
          callBack={() => this.onPressBack()}
        />
        <View style={{flex: 8}}>
          <View style={{padding: 10}}>
            {/* <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={AppStyles.Labeldefault}>Mã Phiếu :</Text>
                                </View>
                                <View style={{ flex: 2 }}>
                                    <Text style={[AppStyles.Textdefault]}>{this.state.Data.TimeConfirmId}</Text>
                                </View>
                            </View> */}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày yêu cầu :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {FuncCommon.ConDate(
                    new Date(this.state.Data.RequiredDate),
                    0,
                  )}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Nhân viên YC :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.Data.EmployeeName}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.Data.DepartmentName}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ghi chú :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.Data.Description}
                </Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
              </View>
              <View style={{flex: 2}}>
                {this.state.Data.Status === 'Y' ? (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.AcceptColor},
                    ]}>
                    {this.state.Data.StatusWF}
                  </Text>
                ) : (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.PendingColor},
                    ]}>
                    {this.state.Data.StatusWF}
                  </Text>
                )}
              </View>
            </View>
          </View>
          <Divider style={{marginTop: 10}} />
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginLeft: 10,
                flex: 1,
              }}>
              Chi tiết
            </Text>
          </View>
          <View style={{flex: 1}}>
            <FlatList
              style={{marginBottom: 10, height: '100%'}}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={(rs, index) => index.toString()}
              data={this.state.list}
              renderItem={({item, index}) => (
                <ListItem>
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <View style={{flex: 1}}>
                        <Text style={[AppStyles.Titledefault]}>
                          {item.EmployeeName}
                        </Text>
                      </View>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.checkdate('' + item.DateCheck)}
                      </Text>
                    </View>
                    <View style={{flex: 20}}>
                      <Text style={[AppStyles.Textdefault]}>
                        Mã NV: {item.EmployeeId}
                      </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        Điều chỉnh vào:{' '}
                        {item.CheckInDc ? item.CheckInDc.slice(0, 5) : ''}
                      </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        Điều chỉnh ra:{' '}
                        {item.CheckOutDc ? item.CheckOutDc.slice(0, 5) : ''}
                      </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        Ghi chú: {item.Note}
                      </Text>
                    </View>
                  </View>
                </ListItem>
              )}
              onEndReachedThreshold={0.5}
            />
          </View>
        </View>
        <View style={{flex: 1}}>
          <TabBarBottom
            onDelete={() => this.onDelete()}
            //key để quay trở lại danh sách
            backListByKey="TimeConfirmGuid"
            keyCommentWF={{
              ModuleId: 32,
              dbName: 'HR',
              scheme: 'HR',
              tableName: 'TimeKeepingsProcess',
              RecordGuid: this.state.Data.RowGuid,
              Title: this.state.Data.TimeConfirmId,
              type: 1,
            }}
            // tiêu đề hiển thị trong popup xử lý phiếu
            Title={'phiếu xác nhận chấm công'}
            //kiểm tra quyền xử lý
            Permisstion={this.state.Data.Permisstion}
            //kiểm tra bước đầu quy trình
            checkInLogin={
              this.state.Data.StatusWF === 'Nháp' ||
              (this.state.Data.StatusWF === 'Cấp trên trả lại' &&
                this.state.Data.Permisstion === 1)
                ? '1'
                : '0'
            }
            onSubmitWF={(callback, CommentWF) =>
              this.submitWorkFlow(callback, CommentWF)
            }
            callbackOpenUpdate={this.callbackOpenUpdate}
          />
        </View>
      </View>
    );
  }
  //xóa
  onDelete = () => {
    let id = {Id: this.props.RowGuid || this.props.RecordGuid};
    this.setState({loading: true});
    API_HR.TimeKeeping_Delete(id.Id)
      .then(response => {
        if (!response.data.Error) {
          this.setState({
            loading: false,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity(
            response.data.message,
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error);
      });
  };
  callbackOpenUpdate = () => {
    Actions.timeKeepingsComfirm_Add({
      itemData: _.cloneDeep(this.state.Data),
      rowData: _.cloneDeep(this.state.list),
    });
  };
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  openView(id) {
    Actions.openTrainingPlanDetails({Id: id});
    console.log('===================chuyen Id:' + id);
  }
  openAttachments() {
    var obj = {
      ModuleId: '13',
      RecordGuid: this.props.RowGuid || this.props.RecordGuid,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  checkdate = _date => {
    if (!_date) {
      return '';
    }
    return moment(FuncCommon.ConDate(_date, 12)).format('DD/MM/YYYY');
  };
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if (csStatus === 'W' || csStatus == 'R') {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  onClick(data) {
    this.setState({selectedTab: data, check: false});
    if (data == 'home') {
      Actions.app();
    } else if (data == 'List') {
      Actions.indexRegistercar();
    } else if (data == 'ok' || data == 'no') {
      this.setState({isModalVisible: !this.state.isModalVisible});
    } else if (data == 'comment') {
      Actions.commentRegistercar({TimeConfirmGuid: this.props.TimeConfirmGuid});
    }
  }
  handleEmail(data) {
    this.setState({CommentWF: data});
  }
  setPickUpLocation(PickUpLocation) {
    this.state.Data.PickUpLocation = PickUpLocation;
    this.setState({Data: this.state.Data});
  }
  setGetOffLocation(GetOffLocation) {
    this.state.Data.GetOffLocation = GetOffLocation;
    this.setState({Data: this.state.Data});
  }
  setReplaceBy(ReplaceBy) {
    this.state.Data.ReplaceBy = ReplaceBy;
    this.setState({Data: this.state.Data});
  }
  setTolls(Tolls) {
    this.state.Data.Tolls = Tolls;
    this.setState({Data: this.state.Data});
  }
  setDescription(Description) {
    this.state.Data.Description = Description;
    this.setState({Data: this.state.Data});
  }
  setDate(Date) {
    this.state.Data.Date = Date;
    this.setState({Data: this.state.Data});
  }
  setStartDate(StartTime) {
    this.state.Data.StartTime = StartTime;
    this.setState({Data: this.state.Data});
  }
  setEndDate(EndTime) {
    this.state.Data.EndTime = EndTime;
    this.setState({Data: this.state.Data});
  }
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      RowGuid: this.state.Data.RowGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (this.state.loading) {
      return;
    }
    this.setState({loading: true}, () => {
      if (callback == 'D') {
        API_HR.Approve_TimeKeepingsConfirm(obj)
          .then(rs => {
            console.log(rs);
            if (rs.data.errorCode === 200) {
              Toast.showWithGravity(
                'Xử lý thành công',
                Toast.SHORT,
                Toast.CENTER,
              );
              this.onPressBack();
            } else {
              Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
              this.getItem();
            }
            this.setState({loading: false});
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data);
          });
      } else {
        API_HR.NotApprove_TimeKeepingsConfirm(obj)
          .then(rs => {
            console.log(rs);
            if (rs.data.errorCode === 200) {
              Toast.showWithGravity(
                'Trả lại thành công',
                Toast.SHORT,
                Toast.CENTER,
              );
              this.onPressBack();
            } else {
              Toast.showWithGravity(
                'Lỗi khi trả lại',
                Toast.SHORT,
                Toast.CENTER,
              );
              this.getItem();
            }
            this.setState({loading: false});
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data);
          });
      }
    });
  }
  closeComment() {
    this.setState({isModalVisible: !this.state.isModalVisible});
  }

  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <Text style={{color: '#000'}} />;
  };
}

const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(GetTimeKeepingsConfirm);
