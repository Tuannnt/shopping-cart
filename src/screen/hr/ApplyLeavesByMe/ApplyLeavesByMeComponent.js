import React, {Component} from 'react';
import {
  ToastAndroid,
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
} from 'react-native';
import {
  Header,
  Divider,
  Button,
  Input,
  SearchBar,
  Badge,
} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions, ActionConst} from 'react-native-router-flux';
import {Back} from '@component';
import {API_HR, API} from '@network';
import Fonts from '../../../theme/fonts';
import {ErrorHandler} from '@error';
import {Container, Left, Right, ListItem, Thumbnail, Body} from 'native-base';
import {AppStyles, AppSizes, AppColors} from '@theme';
import ViewBottonHrComponent from '../ApplyLeaves/ViewBottonHrComponent';
import CategoryDetailComponent from '../../main/CategoryDetailComponent';
import TabBar_Title from '../../component/TabBar_Title';
import TabNavigator from 'react-native-tab-navigator';
import Icon from 'react-native-vector-icons/AntDesign';
import TabBar from '../../component/TabBar';
import {FuncCommon} from '../../../utils';
import MenuSearchDate from '../../component/MenuSearchDate';

class ApplyLeavesByMeComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 15;
    this.TotalRow = 0;
    this.componentCurrent = 'ApplyLeavesByMeComponent';
    this.state = {
      name: '',
      loading: false,
      refreshing: false,
      applyLeavesData: [],
      search: '',
      Status: 'W',
      selectedTab: 'home',
      open: false,
      isModalVisible: false,
      selected: false,
      ChoDuyet: '',
      Tralai: '',
      EvenFromSearch: true,
      ValueSearchDate: '4', // 30 ngày trước
      StartDate: new Date(),
      EndDate: new Date(),
    };
  }

  componentDidMount() {
    this.setState({applyLeavesData: []});
    this.getAllApplyLeavesPage(1);
  }
  convertStatus = data => {
    if (data == 'W') {
      return 'Chờ duyệt';
    } else if (data == 'R') {
      return 'Trả lại';
    } else {
      return 'Đã duyệt';
    }
  };
  keyExtractor = (item, index) => index.toString();

  _onRefresh = () => {
    this.getAllApplyLeavesPage(this.page);
  };
  onSelect = data => {
    this.setState(data);
  };
  searchFilterFunction = text => {
    this.setState({applyLeavesData: []});
    this.state.search = text;
    this.getAllApplyLeavesPage(1);
  };
  setSelectedTab = tab => {
    this.setState({selectedTab: tab});
  };

  onPressback() {
    Actions.app();
  }
  addApplyLeaves() {
    Actions.applyLeavesadd();
  }
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  setStartDate = date => {
    this.setState(
      {
        StartDate: date,
      },
      () => {
        this.getAllApplyLeavesPage(1);
      },
    );
  };

  setEndDate = date => {
    this.setState(
      {
        EndDate: date,
      },
      () => {
        this.getAllApplyLeavesPage(1);
      },
    );
  };

  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.setState(
        {
          StartDate: new Date(callback.start),
          EndDate: new Date(callback.end),
          ValueSearchDate: callback.value,
        },
        () => {
          this.getAllApplyLeavesPage(1);
        },
      );
    }
  };
  render() {
    let arrayholder = [];
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Danh sách nghỉ phép'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            BackModuleByCode={'MyProfile'}
            // addForm = 'true'
            // CallbackFormAdd={()=> this.addApplyLeaves()}
          />
          <View>
            {this.state.EvenFromSearch == true ? (
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventStartDate: true})}>
                      <Text>{FuncCommon.ConDate(this.state.StartDate, 0)}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventEndDate: true})}>
                      <Text>{FuncCommon.ConDate(this.state.EndDate, 0)}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'column', padding: 5}}>
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        {textAlign: 'center'},
                        styles.timeHeader,
                      ]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={text => this.searchFilterFunction(text)}
                  value={this.state.search}
                />
              </View>
            ) : null}
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.getAllApplyLeavesPage(1),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.getAllApplyLeavesPage(1),
                    this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}

          <View style={{flex: 1}}>
            {this.state.applyLeavesData.length > 0 ? (
              <FlatList
                keyExtractor={this.keyExtractor}
                data={this.state.applyLeavesData}
                refreshing={this.state.loading}
                renderItem={({item, index}) => (
                  <ListItem
                    avatar
                    // onPress={() => this.editItem(item, item.Permisstion)}
                  >
                    <Left style={{marginRight: 20}}>
                      <Thumbnail
                        source={API_HR.GetPicApplyLeaves(item.EmployeeAskGuid)}
                      />
                    </Left>
                    <Body>
                      <Text>{item.EmployeeAskName}</Text>
                      <Text note numberOfLines={1}>
                        {'Nội dung: ' +
                          (item.Description ? item.Description : '')}
                      </Text>
                      <Text note numberOfLines={1}>
                        Ngày bắt đầu: {item.StartTimeString}
                      </Text>
                      <Text note numberOfLines={1}>
                        Ngày kết thúc: {item.EndTimeString}
                      </Text>
                      <Text note numberOfLines={1}>
                        Trạng thái: {this.convertStatus(item.Status)}
                      </Text>
                    </Body>
                  </ListItem>
                )}
                onEndReached={this.handleLoadMore}
                onRefresh={this._onRefresh}
                // ListFooterComponent={this.renderFooter.bind(this)}
                onEndReachedThreshold={0.4}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.loading}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['red', 'green', 'blue']}
                  />
                }
              />
            ) : (
              <View style={{flex: 1, alignItems: 'center'}}>
                <Text>Không có dữ liệu</Text>
              </View>
            )}
          </View>

          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  clickBack() {
    Actions.pop();
  }

  editItem(item) {
    // Actions.applyLeaveViewItem({ApplyLeaveGuid: item.ApplyLeaveGuid});
  }
  addItem() {
    Actions.additem({ApplyLeaveGuid: ''});
  }
  onClick(data) {
    this.setState({selectedTab: data});
    if (data == 'home') {
      Actions.app();
    } else if (data == 'back') {
      Actions.ApplyLeaves();
    } else if (data == 'ok') {
      this.state.Status = 'Y';
      this.setState({applyLeavesData: []});
      this.getAllApplyLeavesPage(1);
    } else if (data == 'no') {
      this.state.Status = 'W';
      this.setState({applyLeavesData: []});
      this.getAllApplyLeavesPage(1);
    } else if (data == 'return') {
      this.state.Status = 'R';
      console.log('Trạng thái: ' + this.state.Status);
      this.setState({applyLeavesData: []});
      this.getAllApplyLeavesPage(1);
    }
  }
  getAllApplyLeavesPage = NumberPage => {
    this.page = NumberPage;
    let obj = {
      txtSearch: this.state.search,
      NumberPage: NumberPage,
      Length: 15,
      Status: this.state.Status,
      Type: 'AL',
      StartDate: this.state.StartDate,
      EndDate: this.state.EndDate,
    };
    this.setState({loading: true});
    API.CountStatus()
      .then(res => {
        var listCount = JSON.parse(res.data.data);

        this.setState({ListCountNoti: listCount});
        var _choduyet = '';
        var _tralai = '';
        var data = listCount.find(x => x.Name == 'ApplyLeaves');
        console.log('sodem5555 ' + data);
        if (data !== null && data !== undefined) {
          if (data.Value > 99) {
            _choduyet = '99+';
          } else {
            _choduyet = data.Value;
          }
          console.log('sodem33344 ' + _choduyet);
          this.setState({ChoDuyet: _choduyet});
        }
        var dataDaDuyet = listCount.find(x => x.Name == 'ApplyLeavesReturn');
        if (dataDaDuyet !== null && dataDaDuyet !== undefined) {
          if (dataDaDuyet.Value > 99) {
            _tralai = '99+';
          } else {
            _tralai = dataDaDuyet.Value;
          }
          this.setState({Tralai: _tralai});
        }
      })
      .catch(error => {
        console.log(error.data.data);
      });
    API_HR.getAllApplyLeavesByMe(obj)
      .then(response => {
        console.log(
          '==============ketqua: ' +
            JSON.stringify(
              JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
            ),
        );
        let listData = this.state.applyLeavesData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
          ),
        );
        let data = listData.concat(data1);

        this.setState({applyLeavesData: data, loading: false});
        this.arrayholder = data;
        let TotalRow = JSON.parse(
          JSON.parse(JSON.stringify(response.data)).data,
        );
        this.TotalRow = TotalRow.Count;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'W';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
    });
    this.nextPage();
  };
  handleLoadMore = () => {
    if (
      !this.state.loading &&
      this.TotalRow > this.state.applyLeavesData.length
    ) {
      this.page = this.page + 1;
      this.getAllApplyLeavesPage(this.page);
    }
  };
  renderFooter = () => {
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
}
const colors = {
  loginGreen: '#347F16',
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 5,
    paddingVertical: 5,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  loginContainer: {
    marginTop: 10,
    marginEnd: 10,
    alignItems: 'flex-end',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
});
const stylesIcon = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '1%',
    paddingHorizontal: '5%',
  },
  logo: {
    marginTop: '8%',
    alignSelf: 'center',
  },
  loginContainer: {
    marginTop: AppSizes.padding,
    flexDirection: 'row',
    flex: 1,
    backgroundColor: AppColors.white,
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: AppColors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
  textForgotPassword: {
    ...AppStyles.baseText,
    color: AppColors.loginGreen,
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blue,
    width: '40%',
    height: 40,
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ApplyLeavesByMeComponent);
