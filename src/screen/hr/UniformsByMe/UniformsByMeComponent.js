import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  RefreshControl,
  Keyboard,
  FlatList,
  Text,
  Dimensions
} from 'react-native';
import Fonts from '../../../theme/fonts';
// import Icon from "react-native-vector-icons/FontAwesome";
import { Actions, ActionConst } from 'react-native-router-flux';
import { Header, Divider, Input, SearchBar, ListItem } from 'react-native-elements';
import TabBar from '../../component/TabBar';
import { connect } from 'react-redux';
import { Back } from '@component';
import { API_HR, API } from '@network';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { FuncCommon } from '@utils';
import { CustomView } from '@Component';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height
}
class GetUniforms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshingAll: false,
      ListView: [],
      txtsearch: "",
      DataDetail: null
    };
  }
  componentDidMount = () => {
    this.setState({ loading: true, refreshingAll: true });
    this.JTable();
  }
  JTable() {
    var obj = {
      search: {
        value: this.state.txtsearch
      },
      QueryOrderBy: ""
    }
    API_HR.UniformsDetail_JTable(obj).then(res => {
      var data = JSON.parse(res.data.data).data;
      this.setState({ ListView: data, loading: false, refreshingAll: false });
    }).catch(error => {
      this.setState({ loading: false, refreshingAll: false });
      console.log('===========> error' + error);
    });
  };
  render() {
    return (
      this.state.loading === true ? null :
        <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => { Keyboard.dismiss(); }}>
          <View style={AppStyles.container}>
            <TabBar
              title={'Cấp đồ dùng của bạn'}
              BackModuleByCode={'MyProfile'}
            />
            <View style={{ flex: 1 }}>
              {this.state.loading === true ? null : this.state.ListView.length > 0 ? this.ViewListAll(this.state.ListView) : <View style={{ alignItems: 'center' }}><Text>Không có dữ liệu</Text></View>}
            </View>
            <CustomView eOpen={this.openViewDetail}>
              <View style={{ height: 200, width: DRIVER.width - 50 }}>
                <View style={{ alignItems: 'center', padding: 10 }}>
                  <Text style={[AppStyles.Titledefault]}>Thông tin chi tiết đồ dùng</Text>
                </View>
                <Divider />
                {this.state.DataDetail === null ? null :
                  <View style={{ padding: 10 }}>
                    <Text style={[AppStyles.Textdefault, { padding: 5 }]}>Tên đồ dùng: {this.state.DataDetail.UniformTypeName}</Text>
                    <Text style={[AppStyles.Textdefault, { padding: 5 }]}>Mã đồ dùng: {this.state.DataDetail.ItemID}</Text>
                    <Text style={[AppStyles.Textdefault, { padding: 5 }]}>Số lượng: {this.state.DataDetail.RevokeQuantity}</Text>
                    <Text style={[AppStyles.Textdefault, { padding: 5 }]}>Đơn vị tính: {this.state.DataDetail.Unit}</Text>
                    <Text style={[AppStyles.Textdefault, { padding: 5 }]}>Trạng thái: {this.getStatus(this.state.DataDetail.TicketType)}</Text>
                  </View>
                }
              </View>
            </CustomView>
          </View>
        </TouchableWithoutFeedback>
    );
  }
  ViewListAll = (item) => {
    return (
      <FlatList
        data={item}
        renderItem={({ item, index }) => (
          <TouchableOpacity style={[{ flexDirection: 'row' }]} onPress={() => this.openAcctionViewDetail(item)}>
            <View style={[{ flex: 1, padding: 10, borderBottomWidth: 0.5, borderBottomColor: AppColors.gray }]}>
              <Text style={[AppStyles.Labeldefault, { padding: 2 }]}>{item.UniformTypeName}</Text>
              <View style={[{ flex: 1, flexDirection: 'row' }]}>
                <View style={[{ flex: 1 }]}>
                  <Text style={[AppStyles.Textdefault, { padding: 2 }]}>Mã đồ dùng: {item.ItemID}</Text>
                  <Text style={[AppStyles.Textdefault, { padding: 2 }]}>Người cấp phát: {item.IssuedBy}</Text>
                </View>
                <View>
                  <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>{FuncCommon.ConDate(item.UniformDate, 0)}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshingAll}
            onRefresh={() => this.JTable()}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    )
  }
  _openViewDetail() { }
  openViewDetail = (d) => {
    this._openViewDetail = d;
  }
  openAcctionViewDetail(val) {
    this.setState({ DataDetail: val });
    this._openViewDetail();
  }

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!
      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  getStatus = (status) => {
    switch (status) {
      case 'A':
        return "Mới"

      case 'B':
        return "Đã sử dụng"

      case 'C':
        return 'Khác'

      default:
        return ""

    }

  }
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  bigBlue: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 15,
  },
  label: {
    marginBottom: 5,
    fontSize: 16
  }
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GetUniforms);
