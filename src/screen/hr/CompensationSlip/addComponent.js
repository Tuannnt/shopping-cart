import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  Keyboard,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  FlatList,
} from 'react-native';
import {Header, Button, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_CompensationSlip} from '@network';
import {Container, Content, Item, Label, Picker} from 'native-base';
import {FuncCommon} from '../../../utils';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';
import RequiredText from '../../component/RequiredText';
import Combobox from '../../component/Combobox';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Toast from 'react-native-simple-toast';
import _ from 'lodash';
import moment from 'moment';
import controller from './controller';
import {Alert} from 'react-native';
const headerTable = [
  {title: 'STT', width: 40},
  {title: 'Tên Nhân viên', width: 350},
  {title: 'Hành động', width: 80, hideInDetail: true},
];
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
var width = Dimensions.get('window').width; //full width
class addComponent extends Component {
  constructor(props) {
    super(props);
    this.DataProcess = FuncCommon.DataProcess();
    this.state = {
      rows: [],
      refreshing: false,
      bankData: [],
      bankDataItem: null,
      search: '',
      Licars: [],
      ApplyDate: new Date(),
      StartTime: new Date(),
      EndTime: new Date(),
      Tolls: '0',
      ListEmp: [],
      // RegType: 'A',
      ListReplate: [],
      modalPickerTimeVisible: false,
      isEdit: false,
      Vehicle: null,
      LiRegType: [],
      Type: '3',
      DeleteDetail: [],
    };
  }
  Submit() {
    FuncCommon.Data_Offline(async d => {
      if (!d) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      } else {
        this.Add();
      }
    });
  }
  Add = () => {
    // if (!this.state.Date) {
    //   Toast.showWithGravity(
    //     'Bạn chưa chọn ngày đăng ký',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    let lstDetails = [...this.state.rows];

    if (!this.state.CompensationSlipId) {
      Toast.showWithGravity(
        'Bạn chưa điền mã phiếu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (lstDetails.length === 0) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập chi tiết',
        [{text: 'OK', onPress: () => Keyboard.dismiss()}],
        {cancelable: false},
      );
      return;
    }
    let isNotValidated = false;
    for (let i = 0; i < lstDetails.length; i++) {
      let lst = lstDetails[i];
      if (!lst.EmployeeId) {
        Alert.alert(
          'Thông báo',
          `Bạn chưa chọn nhân viên ở dòng ${i + 1}`,
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }
    // if (!this.state.PlaceGo) {
    //   Toast.showWithGravity('Bạn chưa điền nơi đi', Toast.SHORT, Toast.CENTER);
    //   return;
    // }
    // if (!this.state.PlaceArrive) {
    //   Toast.showWithGravity('Bạn chưa điền nơi đến', Toast.SHORT, Toast.CENTER);
    //   return;
    // }
    if (this.state.EndTime.getTime() < this.state.StartTime.getTime()) {
      Toast.showWithGravity(
        'Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (
      moment(this.state.StartTime).format('DD/MM/YYYY') !==
      moment(this.state.EndTime).format('DD/MM/YYYY')
    ) {
      Toast.showWithGravity(
        'Từ ngày và đến ngày cần cùng một ngày',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (this.props.itemData) {
      this.Update(lstDetails);
      return;
    }
    let _obj = {
      Vehicle: this.state.Vehicle ? this.state.Vehicle : '',
      // RegType: this.state.RegType,
      PlaceGo: this.state.PlaceGo,
      PlaceArrive: this.state.PlaceArrive,
      ApplyDate: FuncCommon.ConDate(this.state.ApplyDate, 2),
      StartTime: FuncCommon.ConDate(this.state.StartTime, 2),
      EndTime: FuncCommon.ConDate(this.state.EndTime, 2),
      ReplaceGuid: null,
      // Tolls: this.state.Tolls,
      Description: this.state.Description,
      Purpose: this.state.Purpose,
      Tolls: 0,
      RegisterConfirm: 0,
      IsConfirm: 0,
      CompensationSlipId: this.state.CompensationSlipId,
      Type: this.state.Type,
    };
    let auto = {
      Value: this.state.ApplyOvertimeId,
      IsEdit: this.state.isEdit,
    };
    let _dataForm = new FormData();
    _dataForm.append('submit', JSON.stringify(_obj));
    _dataForm.append('lstDetails', JSON.stringify(lstDetails));
    _dataForm.append('auto', JSON.stringify(auto));
    _dataForm.append(
      'submitListReplate',
      JSON.stringify(this.state.ListReplate),
    );
    API_CompensationSlip.InsertCompensationSlip(_dataForm)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          Toast.showWithGravity(_rs.Message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.callBackList(JSON.parse(rs.data.data));
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  Update = lstDetails => {
    let _obj = {
      ...this.props.itemData,
      // RegType: this.state.RegType,
      Vehicle: this.state.Vehicle,
      PlaceGo: this.state.PlaceGo,
      PlaceArrive: this.state.PlaceArrive,
      ApplyDate: FuncCommon.ConDate(this.state.ApplyDate, 2),
      StartTime: FuncCommon.ConDate(this.state.StartTime, 2),
      EndTime: FuncCommon.ConDate(this.state.EndTime, 2),
      // Tolls: this.state.Tolls,
      Description: this.state.Description,
      Purpose: this.state.Purpose,
      ReplaceBy: this.state.ReplaceBy,
      ReplaceGuid: null,
      Type: this.state.Type,
    };
    let auto = {
      Value: this.state.ApplyOvertimeId,
      IsEdit: this.state.isEdit,
    };
    let _dataForm = new FormData();
    _dataForm.append('edit', JSON.stringify(_obj));
    _dataForm.append('lstDetails', JSON.stringify(lstDetails));
    _dataForm.append('DeleteDetail', JSON.stringify(this.state.DeleteDetail));
    _dataForm.append('auto', JSON.stringify(auto));
    _dataForm.append(
      'submitListReplate',
      JSON.stringify(this.state.ListReplate),
    );
    API_CompensationSlip.CompensationSlip_Update(_dataForm)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          Toast.showWithGravity(_rs.Message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Cập nhật thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };
  componentDidMount() {
    const {itemData, rowData = []} = this.props;
    Promise.all([
      this.getComboBoxVehicle(),
      this.GetComboBoxJexcelEmployee(),
      this.getComboBoxType(),
    ]);
    if (!itemData) {
      this.getNumberAuto();
    } else {
      this.setState({
        CompensationSlipId: itemData.CompensationSlipId,
        PlaceGo: itemData.PlaceGo,
        PlaceArrive: itemData.PlaceArrive,
        Date: new Date(itemData.Date),
        Type: itemData.Type,
        StartTime: new Date(itemData.StartTime),
        EndTime: new Date(itemData.EndTime),
        ReplaceBy: itemData.ReplaceBy,
        Tolls: itemData.Tolls + '',
        Description: itemData.Description,
        Purpose: itemData.Purpose,
        FullName: itemData.EmployeeName,
        DepartmentName: itemData.DepartmentName,
        Vehicle: itemData.Vehicle,
        rows: [...rowData].map(row => {
          return row;
        }),
        // RegType: itemData.RegType,
      });
    }
  }
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_LB',
      Prefixs: null,
      VoucherType: '',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({
        isEdit: data.IsEdit,
        CompensationSlipId: data.Value,
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
      });
    });
  };
  getComboBoxVehicle = () => {
    let _obj = {};
    API_CompensationSlip.ComboBoxVehicle(_obj)
      .then(rs => {
        var _listCars = rs.data;
        let _reListCars = [
          {
            label: 'Chọn',
            value: '',
          },
        ];
        for (var i = 0; i < _listCars.length; i++) {
          _reListCars.push({
            label: _listCars[i].text,
            value: _listCars[i].value,
          });
        }

        this.setState({
          Licars: _reListCars,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  getComboBoxType = () => {
    API_CompensationSlip.ComboBoxType()
      .then(rs => {
        let _data = rs.data || [];
        let _listType = _data.map(x => {
          return {
            value: x.value,
            label: x.text,
          };
        });
        this.setState({
          LiRegType: [
            {
              label: 'Chọn',
              value: '',
            },
            ..._listType,
          ],
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  GetComboBoxJexcelEmployee = () => {
    API_CompensationSlip.ComboBoxJexcelEmployee()
      .then(rs => {
        let _data = rs.data || [];
        let _listEmp = _data.map(emp => {
          return {
            value: emp.id,
            label: `[${emp.id}] ${emp.name}`,
          };
        });
        this.setState({
          ListEmp: [
            {
              label: 'Chọn',
              value: '',
            },
            ..._listEmp,
          ],
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows, DeleteDetail} = this.state;
    let _deleteDetail = [];
    let row = rows.find((__row, i) => i == index);
    if (row.CompensationSlipDetailGuid) {
      _deleteDetail = [...DeleteDetail, row.CompensationSlipDetailGuid];
    }
    let data = _.cloneDeep(rows);
    let res = data.filter((__row, i) => i !== index);
    this.setState({rows: res, DeleteDetail: _deleteDetail});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;

    this.setState({rows: res});
  };
  //#region hàm Quay lại
  callBackList(rs) {
    this.setState({loading: false});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      Data: !this.state.itemData
        ? {
            Module: 'HR_CompensationSlip',
            // list all key
            Subject: this.state.Description,
            // title
            StartDate:
              this.state.StartTime === '' || this.state.StartTime === null
                ? null
                : FuncCommon.ConDate(this.state.StartTime, 0),
            EndDate:
              this.state.EndTime === '' || this.state.EndTime === null
                ? null
                : FuncCommon.ConDate(this.state.EndTime, 0),
            RowGuid: rs,
            AssignTo: [],
          }
        : {},
      ActionTime: new Date().getTime(),
    });
  }
  onValueChangeRegType = val => {
    this.setState({RegType: val});
  };
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 350}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'EmployeeId');
            }}
            items={this.state.ListEmp}
            style={[stylePicker]}
            value={row.EmployeeId}
            placeholder={{}}
          />
        </View>

        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td, {width: 80}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  //#endregion
  render() {
    const {CompensationSlipId, isEdit} = this.state;
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <View style={[AppStyles.container]}>
          <Container>
            {this.props.itemData ? (
              <TabBar_Title
                title={'Chỉnh sửa phiếu ra cổng'}
                callBack={() => Actions.pop()}
              />
            ) : (
              <TabBar_Title
                title={'Thêm mới phiếu ra cổng'}
                callBack={() => Actions.pop()}
              />
            )}

            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Mã phiếu <RequiredText />
                    </Text>
                  </View>
                  {isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={CompensationSlipId}
                      autoCapitalize="none"
                      onChangeText={Title =>
                        this.setState({CompensationSlipId: Title})
                      }
                    />
                  )}
                  {!isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                        {CompensationSlipId}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Tên nhân viên</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>

                <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>Loại xin ra ngoài</Text>
                  <View
                    style={[
                      {
                        ...AppStyles.FormInputPicker,
                        justifyContent: 'center',
                      },
                    ]}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value => this.onValueChangeType(value)}
                      items={this.state.LiRegType}
                      value={this.state.Type}
                      placeholder={{}}
                      style={{
                        inputIOS: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                        inputAndroid: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                      }}
                      // useNativeAndroidPickerStyle={false}
                    />
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>Phương tiện đi</Text>
                  <View
                    style={[
                      {
                        ...AppStyles.FormInputPicker,
                        justifyContent: 'center',
                      },
                    ]}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value => this.onValueChange2(value)}
                      items={this.state.Licars}
                      value={this.state.Vehicle}
                      placeholder={{}}
                      style={{
                        inputIOS: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                        inputAndroid: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                      }}
                      // useNativeAndroidPickerStyle={false}
                    />
                  </View>
                </View>
                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>
                    Đối tượng chịu chi phí
                  </Text>
                  <View
                    style={[
                      {
                        ...AppStyles.FormInputPicker,
                        justifyContent: 'center',
                      },
                    ]}>
                    <RNPickerSelect
                        doneText="Xong"
                      onValueChange={value => this.onValueChangeRegType(value)}
                      items={LiRegType}
                      value={this.state.RegType}
                      placeholder={{}}
                      style={{
                        inputIOS: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                        inputAndroid: {
                          padding: 10,
                          color: 'black',
                          fontSize: 14,
                          paddingRight: 15, // to ensure the text is never behind the icon
                        },
                      }}
                      // useNativeAndroidPickerStyle={false}
                    />
                  </View>
                </View> */}
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text className="required" style={AppStyles.Labeldefault}>
                      Nơi đi
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    value={this.state.PlaceGo}
                    autoCapitalize="none"
                    onChangeText={PlaceGo => this.setPlaceGo(PlaceGo)}
                  />
                </View>

                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Nơi đến</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.PlaceArrive}
                    onChangeText={PlaceArrive =>
                      this.setPlaceArrive(PlaceArrive)
                    }
                  />
                </View>
                {/* <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày đăng ký <RequiredText />
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.Date}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        disabled={true}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setDate(date)}
                      />
                    </View>
                  </View>
                </View> */}
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>Từ ngày giờ</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <TouchableOpacity
                        onPress={() => {
                          this.handlePickTimeOpen('StartTime');
                        }}
                        style={[
                          AppStyles.table_td_custom,
                          {padding: 12, borderWidth: 1},
                        ]}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'center'},
                          ]}>
                          {(this.state.StartTime &&
                            moment(this.state.StartTime).format(
                              'DD/MM/YYYY HH:mm:ss',
                            )) ||
                            ''}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>Đến ngày giờ</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <TouchableOpacity
                        onPress={() => {
                          this.handlePickTimeOpen('EndTime');
                        }}
                        style={[
                          AppStyles.table_td_custom,
                          {padding: 12, borderWidth: 1},
                        ]}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'center'},
                          ]}>
                          {(this.state.EndTime &&
                            moment(this.state.EndTime).format(
                              'DD/MM/YYYY HH:mm:ss',
                            )) ||
                            ''}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Mục đích</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    value={this.state.Purpose}
                    onChangeText={Purpose => this.setPurpose(Purpose)}
                  />
                </View>
                {/* Table */}
                <View style={{marginTop: 5}}>
                  <View style={{width: 150, marginLeft: 10}}>
                    <Button
                      title="Thêm dòng"
                      type="outline"
                      size={15}
                      onPress={() => this.addRows()}
                      buttonStyle={{padding: 5, marginBottom: 5}}
                      titleStyle={{marginLeft: 5}}
                      icon={
                        <Icon
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>
                  <ScrollView
                    horizontal={true}
                    style={{paddingBottom: 25, marginBottom: 20}}>
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        {headerTable.map(item => (
                          <TouchableOpacity
                            key={item.title}
                            style={[AppStyles.table_th, {width: item.width}]}>
                            <Text style={AppStyles.Labeldefault}>
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        ))}
                      </View>
                      <View>
                        {this.state.rows.length > 0 ? (
                          <FlatList
                            data={this.state.rows}
                            renderItem={({item, index}) => {
                              return this.itemFlatList(item, index);
                            }}
                            keyExtractor={(rs, index) => index.toString()}
                          />
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'left'},
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View>
              </View>
            </ScrollView>

            <View>
              <Button
                buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
                title="Lưu"
                onPress={() => this.Submit()}
              />
            </View>
            {this.state.modalPickerTimeVisible && (
              <DateTimePickerModal
                locale="vi-VN"
                cancelTextIOS="Huỷ"
                confirmTextIOS="Chọn"
                headerTextIOS="Chọn thời gian"
                isVisible={this.state.modalPickerTimeVisible}
                mode="datetime"
                onConfirm={this.handleConfirmTime}
                onCancel={this.hideDatePicker}
              />
            )}
          </Container>
        </View>
      </KeyboardAvoidingView>
    );
  }
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  handlePickTimeOpen = type => {
    this.setState({
      typeTime: type,
      modalPickerTimeVisible: true,
    });
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      typeTime: null,
    });
  };
  handleConfirmTime = date => {
    const {typeTime} = this.state;
    this.setState({[typeTime]: date, typeTime: null});
    this.hideDatePicker();
  };
  _openComboboxEmp() {}
  openComboboxEmp = d => {
    this._openComboboxEmp = d;
  };
  onActionComboboxEmp() {
    this._openComboboxEmp();
  }
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'arrowleft'}
          type="antdesign"
          size={30}
          onPress={() => this.indexRegistercar()}
        />
      </View>
    );
  }
  ChangeEmp = rs => {
    if (!rs || !rs[0].value) {
      return;
    }
    var value = [];
    var list = [];
    for (let i = 0; i < rs.length; i++) {
      if (rs[i].value !== null) {
        list.push(rs[i]);
        value.push({LstReplaceGuid: rs[i].value, ReplaceBy: rs[i].text});
      }
    }
    var name =
      list.length !== 0 && list.length > 1
        ? list.length + ' lựa chọn'
        : list[0].text;
    this.setState({ListEmpName: name, ListReplate: value});
  };
  setPlaceGo(PlaceGo) {
    this.setState({PlaceGo});
  }
  setPlaceArrive(PlaceArrive) {
    this.setState({PlaceArrive});
  }
  setReplaceBy(ReplaceBy) {
    this.setState({ReplaceBy});
  }
  setTolls(Tolls) {
    this.setState({Tolls});
  }
  setDescription(Description) {
    this.setState({Description});
  }
  setPurpose(Purpose) {
    this.setState({Purpose});
  }
  setDate(D) {
    this.setState({Date: new Date(D)});
  }

  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  onValueChange2(value) {
    this.setState({
      Vehicle: value,
    });
  }
  onValueChangeType(value) {
    this.setState({
      Type: value,
    });
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(addComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
  },
});
