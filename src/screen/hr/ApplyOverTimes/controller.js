import {API_ApplyOverTimes} from '@network';
import {ErrorHandler} from '@error';
export default {
  // List nhan vien
  getAllEmployee(callback) {
    API_ApplyOverTimes.GetEmp()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getAllEmployeeByDepart(callback) {
    API_ApplyOverTimes.GetEmployeeByDepartment()
      .then(res => {
        console.log(res);
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getAllWorkShift(callback) {
    API_ApplyOverTimes.getAllWorkShift()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getAllOrder(search, callback) {
    let obj = {Search: search || ''};
    API_ApplyOverTimes.getAllOrder(obj)
      .then(res => {
        console.log(res);
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getNumberAuto(value, callback) {
    API_ApplyOverTimes.GetNumberAuto(value)
      .then(res => {
        console.log(res + '=======> get Number auto');
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  removeAttactment(data, list, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },
};
