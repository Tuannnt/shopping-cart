import React, { Component } from 'react';
import {
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Image,
  FlatList,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { Actions } from 'react-native-router-flux';
import { API_ApplyOverTimes } from '../../../network';
import TabBar_Title from '../../component/TabBar_Title';
import { Button, Divider, Icon as IconElement } from 'react-native-elements';
import _ from 'lodash';
import { AppStyles, AppColors } from '@theme';
import DatePicker from 'react-native-datepicker';
import TableAddNew from './TableAddNew';
import moment from 'moment';
import controller from './controller';
import { FuncCommon } from '@utils';
import listCombobox from './listCombobox';
import ComboboxV2 from '../../component/ComboboxV2';
import RequiredText from '../../component/RequiredText';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';
import LoadingComponent from '../../component/LoadingComponent';
import Toast from 'react-native-simple-toast';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Combobox from '../../component/Combobox';
import { Container } from 'native-base';
const styles = StyleSheet.create({
  styletextLable: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  styletext: {
    fontSize: 14,
    padding: 5,
  },
});
const headerTable = [
  { title: 'STT', width: 40 },
  { title: 'Nhân viên', width: 300 },
  { title: 'Ca đăng ký', width: 200 },
  { title: '% ca', width: 70 },
  // { title: 'Loại làm thêm', width: 150 },
  { title: 'TG vào dự kiến', width: 100 },
  { title: 'TG ra dự kiến', width: 100 },
  // { title: 'TG vào thực', width: 100 },
  // { title: 'TG ra thực', width: 100 },
  { title: 'Số giờ làm thêm', width: 100 },
  // { title: 'Tổng TG thực', width: 100 },
  // {title: 'TG tính công', width: 100},
  // {title: 'Kết quả', width: 200},
  // {title: 'Mã sản phẩm', width: 280},
  // {title: 'Số SO', width: 120},
  { title: 'Ghi chú', width: 180 },
  { title: 'Hành động', width: 80 },
];

class ApplyOverTimesAdd_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DeleteDetail: [],
      ListEmployee: [],
      ShiftId: undefined,
      StartTime: '',
      modalPickerTimeVisible: false,
      Result: '',
      rows: [{}],
      workShiftData: [],
      listOrder: [],
      Attachment: [],
      ListType: [],
      Title: '',
      ApplyDate: FuncCommon.ConDate(new Date(), 0),
      Description: '',
      Type: '',
      loading: true,
      Dropdown: [
        {
          value: 'L',
          text: 'Tăng ca ngày lễ',
        },
        {
          value: 'T',
          text: 'Tăng ca cuối tuần',
        },
        {
          value: 'O',
          text: 'Tăng ca ngày thường',
        },
      ],
    };
    this.refreshing = false;
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  componentDidMount = () => {
    const { itemData, rowData = [] } = this.props;
    if (!itemData) {
      this.getNumberAuto();
      this.setApplyDate(this.state.ApplyDate, 'form');
      this.setState({
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        Title:
          global.__appSIGNALR.SIGNALR_object.USER.DepartmentId +
          '_' +
          moment(FuncCommon.ConDate(this.state.ApplyDate, 99)).format(
            'DD.MM.YYYY',
          ),
        JobTitleNameEmployee:
          global.__appSIGNALR.SIGNALR_object.USER.JobTitleName,
        EmployeeGuid: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
      });
    } else {
      this.setState(
        {
          rows: rowData.map(data => {
            if (data.StartTime) {
              data.StartTime =
                '' + data.ApplyDate.slice(0, 11) + data.StartTime;
            }
            if (data.EndTime) {
              data.EndTime = '' + data.ApplyDate.slice(0, 11) + data.EndTime;
            }
            if (data.ActualStartTime) {
              data.ActualStartTime =
                '' + data.ApplyDate.slice(0, 11) + data.ActualStartTime;
            }
            if (data.ActualEndTime) {
              data.ActualEndTime =
                '' + data.ApplyDate.slice(0, 11) + data.ActualEndTime;
            }
            if (data.ItemIDByProvider && data.OrderNumber) {
              data.OrderNumber =
                data.ItemIDByProvider + '-[' + data.OrderNumber + ']';
            }
            if (data.IsCheck !== null && data.IsCheck !== undefined) {
              data.IsCheck = data.IsCheck + '';
            }
            return data;
          }),
          ApplyDate: FuncCommon.ConDate(itemData.ApplyDate, 0),
          ApplyOvertimeId: itemData.ApplyOvertimeId,
          Description: itemData.Description,
          Title: itemData.Title,
          FullName: itemData.EmployeeName,
          DepartmentName: itemData.DepartmentName,
          JobTitleNameEmployee: itemData.JobTitleName,
          EmployeeGuid: itemData.EmployeeGuid,
          EmployeeId: itemData.EmployeeId,
        },
        () => {
          this.setApplyDate(FuncCommon.ConDate(itemData.ApplyDate, 0), 'form');
        },
      );
    }
    Promise.all([
      this.getAllEmployeeByDepart(),
      this.getAllWorkShift(),
      this.getAllOrder(),
      this.getType(),
    ]);
  };

  componentWillUnmount = () => {
    clearTimeout(this.time);
  };
  onPressBack() {
    Actions.pop();
    Actions.refresh({
      moduleId: 'back',
      ActionTime: new Date().getTime(),
    });
  }
  getLabelWorkShift = value => {
    if (!value) {
      return '';
    }
    let res = this.state.Dropdown.findIndex(i => i.value === value);
    if (res === -1) {
      return '';
    }
    return this.state.Dropdown[res].text;
  };
  setApplyDate = (date, type) => {
    let _date = FuncCommon.ConDate(date, 99);
    let obj = { date: this.convertDateNow(_date) };
    API_ApplyOverTimes.ApplyOverTimes_CheckHoliday(obj)
      .then(res => {
        if (res.data.data === 'null' || res.data.data === '') {
          var current_day = new Date(_date).getDay();
          if (current_day === 0) {
            this.setState({ Type: 'T' });
          } else {
            this.setState({ Type: 'O' });
          }
        } else {
          this.setState({
            Type: JSON.parse(res.data.data).HolidayType,
            loading: false,
          });
        }
        if (type === 'form') {
          this.setState({ ApplyDate: date, loading: false });
        }
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
        this.setState({ loading: false });
      });
  };
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_LT',
      Prefixs: null,
      VoucherType: '',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({ isEdit: data.IsEdit, ApplyOvertimeId: data.Value });
    });
  };
  onSummit = () => {
    if (this.refreshing) {
      return;
    }
    this.refreshing = true;
    setTimeout(() => {
      this.refreshing = false;
    }, 2000);
    let tableData = _.cloneDeep(this.state.rows);
    const { Attachment } = this.state;
    let lstDetails = tableData.map(data => {
      if (data.StartTime && data.EndTime) {
        data.TotalTime =
          Math.round(
            FuncCommon.getDiffTime(data.StartTime, data.EndTime) * 100,
          ) / 100;
      }
      if (data.ActualStartTime && data.ActualEndTime) {
        data.ActualTotalTime =
          Math.round(
            FuncCommon.getDiffTime(data.ActualStartTime, data.ActualEndTime) *
            100,
          ) / 100;
      }
      data.ids = undefined;
      if (data.StartTime) {
        data.StartTime = moment(data.StartTime).format('HH:mm:ss');
      }
      if (data.EndTime) {
        data.EndTime = moment(data.EndTime).format('HH:mm:ss');
      }
      if (data.ActualStartTime) {
        data.ActualStartTime = moment(data.ActualStartTime).format('HH:mm:ss');
      }
      if (data.ActualEndTime) {
        data.ActualEndTime = moment(data.ActualEndTime).format('HH:mm:ss');
      }
      if (data.OrderNumber && data.OrderNumber.includes('[')) {
        let arr = data.OrderNumber.split('[');
        data.ItemIDByProvider = arr[0].slice(0, -1);
        data.OrderNumber = arr[1].slice(0, -1);
      } else {
        data.ItemIDByProvider = '';
        data.OrderNumber = '';
      }
      if (data.ShiftId) {
        data.WorkDayRate = this.helperWorkShift(
          data.ShiftId,
          this.state.workShiftData,
        );
      }
      return data;
    });
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    let submit = {
      FileAttachments: [],
      Title: this.state.Title,
      Description: this.state.Description,
      ApplyDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.ApplyDate, 99),
        2,
      ),
      Type: this.state.Type,
      ApplyOvertimeId: this.state.ApplyOvertimeId,
    };
    let isNotValidated = false;
    if (!lstDetails || lstDetails.length === 0) {
      Toast.showWithGravity('Yêu cầu nhập chi tiết', Toast.SHORT, Toast.CENTER);
      return;
    }

    for (let i = 0; i < lstDetails.length; i++) {
      let lst = lstDetails[i];
      if (!lst.EmployeeId) {
        Toast.showWithGravity(
          `Bạn chưa chọn nhân viên ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
      if (!lst.ShiftId) {
        Toast.showWithGravity(
          `Bạn chưa chọn ca đăng ký ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
      if (!lst.StartTime) {
        Toast.showWithGravity(
          `Bạn chưa chọn thời gian bắt đầu dự kiến ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;

        return;
      }
      if (!lst.EndTime) {
        Toast.showWithGravity(
          `Bạn chưa chọn thời gian kết thúc dự kiến ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
    }

    if (isNotValidated) {
      return;
    }
    if (lstDetails.length === 0) {
      Toast.showWithGravity(
        'Bạn chưa chọn nhân viên',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.ApplyOvertimeId) {
      Toast.showWithGravity(
        'Bạn chưa nhập mã phiếu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.Title) {
      Toast.showWithGravity(
        'Bạn chưa nhập tên phiếu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let auto = { Value: this.state.ApplyOvertimeId, IsEdit: this.state.isEdit };
    if (this.props.itemData) {
      this.update(submit, auto, lstDetails);
      return;
    }
    let _data = new FormData();
    _data.append('submit', JSON.stringify(submit));
    _data.append('auto', JSON.stringify(auto));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    _data.append('DeleteAttach', JSON.stringify([]));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    _data.append('lstDetails_Item', JSON.stringify([]));
    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_ApplyOverTimes.ApplyOverTimes_Submit(_data)
      .then(res => {
        console.log('ApplyOverTimes_Submit============:' + res.data.message);
        if (res.data.errorCode !== 200) {
          Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
          return;
        }
        Toast.showWithGravity('Thêm mới thành công', Toast.SHORT, Toast.CENTER);
        this.onPressBack();
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
      });
  };
  update = (submit, auto, lstDetails) => {
    let edit = { ...this.props.itemData, ...submit };
    let _dataEdit = new FormData();
    _dataEdit.append('edit', JSON.stringify(edit));
    _dataEdit.append('auto', JSON.stringify(auto));
    _dataEdit.append('lstTitlefile', JSON.stringify([]));
    _dataEdit.append('DeleteAttach', JSON.stringify([]));
    _dataEdit.append('DeleteDetail', JSON.stringify(this.state.DeleteDetail));
    _dataEdit.append('lstDetails', JSON.stringify(lstDetails));
    _dataEdit.append('lstDetails_Item', JSON.stringify([]));
    API_ApplyOverTimes.ApplyOverTimes_Edit(_dataEdit)
      .then(res => {
        console.log('ApplyOverTimes_Edit============:' + res.data.message);
        if (res.data.errorCode !== 200) {
          Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
          return;
        }
        Toast.showWithGravity(
          'Chỉnh sửa thành công',
          Toast.SHORT,
          Toast.CENTER,
        );
        this.onPressBack();
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
      });
  };
  helperWorkShift = (val, workShiftData) => {
    const { Type } = this.state;
    if (!val) {
      return null;
    }
    let index = workShiftData.findIndex(item => item.value === val);
    if (index === -1) {
      return;
    }
    if (Type === 'O') {
      return workShiftData[index].OvertimeDay;
    }
    if (Type === 'L') {
      return workShiftData[index].OvertimeHoliday;
    }
    if (Type === 'T') {
      return workShiftData[index].OvertimeWeekend;
    }
  };
  //#region openAttachment
  _openCombobox_Att() { }
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#region open img
  _open() { }
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({ Attachment: this.state.Attachment });
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({ Attachment: this.state.Attachment });
    } catch (err) {
      this.setState({ loading: false });
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion
  convertDateNow(datetime) {
    if (datetime !== null) {
      var newdate = new Date(datetime);
      var month = newdate.getMonth() + 1;
      var day = newdate.getDate();
      var year = newdate.getFullYear();
      var hh = newdate.getHours();
      var mm = newdate.getMinutes();
      if (month < 10) {
        month = '0' + month;
      }
      if (day < 10) {
        day = '0' + day;
      }
      return year + '-' + month + '-' + day;
    }
    return '';
  }
  render() {
    const { isEdit, ApplyOvertimeId } = this.state;
    const { itemData } = this.props;
    const IS_CB = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'CB';
    if (this.state.loading) {
      return <LoadingComponent />;
    }
    return (
      <KeyboardAvoidingView
        style={{ flex: 1, backgroundColor: '#fff' }}
        behavior={Platform.OS === 'ios' ? 'padding' : null}>
        <TabBar_Title
          title={`${itemData ? 'Sửa' : 'Thêm'} phiếu tăng ca`}
          callBack={() => this.callBackList()}
        />
        <ScrollView>
          <Divider />
          <View>
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>
                  Mã phiếu <RequiredText />
                </Text>
              </View>
              {isEdit && (
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  value={ApplyOvertimeId}
                  autoCapitalize="none"
                  onChangeText={Title =>
                    this.setState({ ApplyOvertimeId: Title })
                  }
                />
              )}
              {!isEdit && (
                <View style={[AppStyles.FormInput]}>
                  <Text style={[AppStyles.TextInput, { color: AppColors.gray }]}>
                    {ApplyOvertimeId}
                  </Text>
                </View>
              )}
            </View>
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>Người tạo phiếu</Text>
              </View>
              <TextInput
                style={[AppStyles.FormInput, { color: 'black' }]}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                value={this.state.FullName}
                editable={false}
              />
            </View>
            {IS_CB && (
              <View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, { color: 'black' }]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Chức danh</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, { color: 'black' }]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.JobTitleNameEmployee}
                    editable={false}
                  />
                </View>
              </View>
            )}

            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>
                  Tên phiếu
                  <RequiredText />
                </Text>
              </View>
              <TextInput
                style={AppStyles.FormInput}
                underlineColorAndroid="transparent"
                value={this.state.Title}
                autoCapitalize="none"
                onChangeText={Title => this.setState({ Title })}
              />
            </View>
            <View
              style={{
                padding: 10,
                paddingBottom: 0,
                flexDirection: 'column',
              }}>
              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <Text style={AppStyles.Labeldefault}>
                  Ngày đăng ký làm thêm
                </Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ width: '100%' }}>
                  <DatePicker
                    locale="vie"
                    locale={'vi'}
                    style={{ width: '100%' }}
                    date={this.state.ApplyDate}
                    mode="date"
                    androidMode="spinner"
                    placeholder="Chọn ngày"
                    format="DD/MM/YYYY"
                    confirmBtnText="Xong"
                    cancelBtnText="Huỷ"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: 4,
                        marginLeft: 0,
                      },
                      dateInput: {
                        marginRight: 36,
                      },
                    }}
                    onDateChange={date => this.setApplyDate(date, 'form')}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                padding: 10,
                paddingBottom: 0,
              }}>
              <View style={{ flexDirection: 'column', flex: 2 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={AppStyles.Labeldefault}>Loại ca làm thêm</Text>
                </View>
                <View style={[AppStyles.FormInput]}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      !this.state.Type
                        ? { color: AppColors.gray }
                        : { color: 'black' },
                    ]}>
                    {this.state.Type
                      ? this.getLabelWorkShift(this.state.Type)
                      : 'Chọn ngày đăng ký làm thêm...'}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>Nội dung phiếu</Text>
              </View>
              <TextInput
                style={[AppStyles.FormInput, { maxHeight: 70 }]}
                underlineColorAndroid="transparent"
                value={this.state.Description}
                numberOfLines={5}
                multiline={true}
                onChangeText={Description => this.setState({ Description })}
              />
            </View>
            {/* <TableAddNew
              ref={this.tableRef}
              headerTable={headerTable}
              TypeOfDate={this.state.Type}
              dataRowInit={this.state.dataRowInit}
              callBackSearchType={this.setApplyDate}
            /> */}
            <View style={{ marginVertical: 5, marginBottom: 25 }}>
              <View style={{ width: 150, marginLeft: 10 }}>
                <Button
                  title="Thêm mới dòng"
                  type="outline"
                  size={15}
                  onPress={() => this.addRows()}
                  buttonStyle={{ padding: 5, marginBottom: 5 }}
                  titleStyle={{
                    marginLeft: 5,
                    fontSize: 14,
                    fontWeight: 'normal',
                  }}
                  icon={
                    <IconElement
                      name="pluscircleo"
                      type="antdesign"
                      color={AppColors.blue}
                      size={14}
                    />
                  }
                />
              </View>

              {/* Table */}
              <ScrollView horizontal={true} style={{ marginTop: 5 }}>
                <View style={{ flexDirection: 'column', marginHorizontal: 10 }}>
                  <View style={{ flexDirection: 'row' }}>
                    {headerTable.map(item => {
                      if (item.hideInDetail) {
                        return null;
                      }
                      return (
                        <TouchableOpacity
                          key={item.title}
                          style={[AppStyles.table_th, { width: item.width }]}>
                          <Text style={AppStyles.Labeldefault}>
                            {item.title}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                  <View>
                    {this.state.rows.length > 0 ? (
                      <FlatList
                        data={this.state.rows}
                        renderItem={({ item, index }) => {
                          return this.itemFlatList(item, index);
                        }}
                        keyExtractor={(rs, index) => index.toString()}
                      />
                    ) : (
                      <TouchableOpacity style={[AppStyles.table_foot]}>
                        <Text
                          style={[AppStyles.Textdefault, { textAlign: 'left' }]}>
                          Không có dữ liệu!
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              </ScrollView>
            </View>
            {/* attachment */}
            {!this.props.itemData && (
              <View
                style={{
                  flexDirection: 'column',
                  padding: 10,
                  marginBottom: 20,
                }}>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                    <Text style={{ color: 'red' }} />
                  </View>
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      justifyContent: 'flex-end',
                      flexDirection: 'row',
                    }}
                    onPress={() => this.openAttachment()}>
                    <IconElement
                      name="attachment"
                      type="entypo"
                      size={15}
                      color={AppColors.ColorAdd}
                    />
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        { color: AppColors.ColorAdd },
                      ]}>
                      {' '}
                      Chọn file
                    </Text>
                  </TouchableOpacity>
                </View>
                <Divider style={{ marginBottom: 10 }} />
                {this.state.Attachment.length > 0
                  ? this.state.Attachment.map((para, i) => (
                    <View
                      key={i}
                      style={[
                        styles.StyleViewInput,
                        { flexDirection: 'row', marginBottom: 3 },
                      ]}>
                      <View
                        style={[AppStyles.containerCentered, { padding: 10 }]}>
                        <Image
                          style={{
                            width: 30,
                            height: 30,
                            borderRadius: 10,
                          }}
                          source={{
                            uri: this.FileAttackments.renderImage(
                              para.FileName,
                            ),
                          }}
                        />
                      </View>
                      <TouchableOpacity
                        key={i}
                        style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={AppStyles.Textdefault}>
                          {para.FileName}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.containerCentered, { padding: 10 }]}
                        onPress={() => this.removeAttactment(para)}>
                        <IconElement
                          name="close"
                          type="antdesign"
                          size={20}
                          color={AppColors.ColorDelete}
                        />
                      </TouchableOpacity>
                    </View>
                  ))
                  : null}
              </View>
            )}
          </View>

          {/*nút xử lý*/}
        </ScrollView>
        <View>
          <Button
            buttonStyle={{ backgroundColor: AppColors.ColorButtonSubmit }}
            title="Lưu"
            onPress={() => this.onSummit()}
          />
        </View>

        {this.state.modalPickerTimeVisible && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisible}
            mode="time"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        {this.state.modalPickerTimeVisibleDate && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisibleDate}
            mode="date"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        {this.state.listOrder.length > 0 && (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeOrder}
            data={this.state.listOrder}
            nameMenu={'Chọn'}
            eOpen={this.openComboboxOrder}
            position={'bottom'}
            value={null}
          // callback_SearchPaging={(callback, textsearch) => {
          //   this.getAllOrder(callback, textsearch);
          // }}
          />
        )}
        {this.state.ListEmployee.length > 0 && (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeEmployee}
            data={this.state.ListEmployee}
            nameMenu={'Chọn'}
            eOpen={this.openComboboxEmployee}
            position={'bottom'}
            value={null}
          />
        )}
        <OpenPhotoLibrary
          callback={this.callbackLibarary}
          openLibrary={this.openLibrary}
        />
        <ComboboxV2
          callback={this.ChoiceAtt}
          data={listCombobox.ListComboboxAtt}
          eOpen={this.openCombobox_Att}
        />
      </KeyboardAvoidingView>
    );
  }
  getAllOrder = (callback, search = '') => {
    clearTimeout(this.time);
    // debounce call api
    this.time = setTimeout(() => {
      controller.getAllOrder(search, rs => {
        let data = JSON.parse(rs).map(item => ({
          value: item.id,
          text: item.name,
        }));
        data.unshift({
          value: null,
          text: 'Chọn sản phẩm',
        });
        if (callback) {
          callback(data);
        } else {
          this.setState({ listOrder: data });
        }
      });
    }, 300);
  };
  getAllWorkShift = () => {
    controller.getAllWorkShift(rs => {
      let data = JSON.parse(rs).map(item => ({
        ...item,
        value: item.value,
        label: item.text,
      }));

      this.setState({ workShiftData: data });
    });
  };
  getType = () => {
    API_ApplyOverTimes.Type()
      .then(rs => {
        if (rs.data.errorCode === 200) {
          let data = JSON.parse(rs.data.data).map(item => ({
            ...item,
            value: item.id,
            label: item.name,
          }));
          this.setState({ ListType: data });
        }
      })
      .catch(err => console.log(err));
  };
  getAllEmployeeByDepart = () => {
    controller.getAllEmployeeByDepart(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.id,
        text: item.name,
      }));

      this.setState({ ListEmployee: data });
    });
  };
  onDelete = index => {
    const { rows, DeleteDetail } = this.state;
    let _deleteDetail = [];
    let row = rows.find((__row, i) => i == index);
    if (row.Id) {
      _deleteDetail = [...DeleteDetail, row.Id];
    }
    let data = _.cloneDeep(rows);
    let res = data.filter((__row, i) => i !== index);
    this.setState({ rows: res, DeleteDetail: _deleteDetail });
  };
  addRows = () => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({ rows: data });
  };
  _openComboboxEmployee() { }
  openComboboxEmployee = d => {
    this._openComboboxEmployee = d;
  };
  onActionComboboxEmployee(index) {
    this.setState({ currentIndexItem: index }, () => {
      this._openComboboxEmployee();
    });
  }
  ChangeEmployee = rs => {
    const { currentIndexItem, rows } = this.state;
    let { value, text } = rs;
    if (!rs) {
      return;
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.EmployeeId = value;
        row.EmployeeName = text;
      }
      return row;
    });
    this.setState({ rows: data, currentIndexItem: null });
  };
  handleChangeRows = (val, index, name) => {
    const { rows } = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    if (name === 'ShiftId') {
      const shift = this.state.workShiftData.find(x => x.value === val);
      if (shift && shift?.EndTime) {
        res[index].StartTime = FuncCommon.ConDate(shift?.EndTime, 8);
      }
    }
    this.setState({ rows: res });
  };

  handleChangeTimeInExpected = val => {
    this.setState({ StartTime: val });
  };
  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  handleConfirmTime = date => {
    const { typeTime, currentIndex, rows } = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    if (typeTime === 'ApplyDate') {
      this.setApplyDate(FuncCommon.ConDate(date, 0));
    }
    this.setState({
      rows: res,
      typeTime: null,
      currentIndex: null,
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
    });
    // this.hideDatePicker();
  };

  helperSoNumber = txt => {
    if (txt && txt.includes('[')) {
      let arr = txt.split('[');
      return arr[1].slice(0, -1);
    }
    return '';
  };
  _openComboboxOrder() { }
  openComboboxOrder = d => {
    this._openComboboxOrder = d;
  };
  openOrders(index) {
    this.setState({ currentIndexItem: index }, () => {
      this._openComboboxOrder();
    });
  }
  ChangeOrder = rs => {
    const { currentIndexItem, rows } = this.state;
    let { value } = rs;
    if (!rs) {
      return;
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.OrderNumber = value;
      }
      return row;
    });
    this.setState({ rows: data, currentIndexItem: null });
  };
  itemFlatList = (row, index) => {
    const stylePicker = {
      inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderRadius: 4,
        color: 'black',
        paddingRight: 15, // to ensure the text is never behind the icon
      },
      inputAndroid: {
        color: 'black',
        fontSize: 10,
      },
      placeholder: {
        color: 'black',
      },
    };
    return (
      <View style={{ flexDirection: 'row' }} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 40 }]}>
          <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        {/* <View style={[AppStyles.table_td_custom, {width: 300}]}>

            <RNPickerSelect
              doneText="Xong"
              style={{
                ...stylePicker,
                iconContainer: {
                  top: 10,
                  right: 10,
                },
              }}
              onValueChange={value => {
                this.handleChangeRows(value, index, 'EmployeeGuid');
              }}
              placeholderTextColor="black"
              items={this.state.ListEmployee}
              value={row.EmployeeGuid}
              placeholder={{value: null, label: 'Chọn nhân viên'}}


            />

        </View> */}

        {/* <TouchableOpacity
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.handlePickDateOpen('ApplyDate', index);
          }}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.ApplyDate && moment(row.ApplyDate).format('DD/MM/YYYY')) ||
              ''}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: 300 }]}
          onPress={() => {
            this.onActionComboboxEmployee(index);
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.EmployeeName,
                { color: 'black', fontSize: 14 },
              ]}>
              {row.EmployeeName ? row.EmployeeName : 'Chọn nhân viên'}
            </Text>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <IconElement
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, { width: 200 }]}>
          <RNPickerSelect
            doneText="Xong"
            style={{
              ...stylePicker,
              iconContainer: {
                top: 10,
                right: 10,
              },
            }}
            onValueChange={value => {
              this.handleChangeRows(value, index, 'ShiftId');
            }}
            items={this.state.workShiftData}
            value={row.ShiftId}
            placeholder={{ value: null, label: 'Chọn ca' }}
            disabled={this.props.seeDetail}
          // Icon={() => {
          //   return (
          //     <Icon
          //       color={AppColors.gray}
          //       name={'chevron-thin-down'}
          //       type="entypo"
          //       size={15}
          //     />
          //   );
          // }}
          />
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 70 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {row.ShiftId &&
              this.helperWorkShift(row.ShiftId, this.state.workShiftData)}
          </Text>
        </TouchableOpacity>
        {false && (
          <View style={[AppStyles.table_td_custom, { width: 150 }]}>
            <RNPickerSelect
              doneText="Xong"
              style={{
                ...stylePicker,
                iconContainer: {
                  top: 10,
                  right: 10,
                },
              }}
              onValueChange={value => {
                this.handleChangeRows(value, index, 'IsCheck');
              }}
              items={this.state.ListType}
              value={row.IsCheck}
              placeholder={{ value: null, label: 'Chọn' }}
              disabled={this.props.seeDetail}
            />
          </View>
        )}
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('StartTime', index);
          }}
          style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {(row.StartTime && moment(row.StartTime).format('HH:mm')) || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('EndTime', index);
          }}
          style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {(row.EndTime && moment(row.EndTime).format('HH:mm')) || ''}
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('ActualStartTime', index);
          }}
          style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {(row.ActualStartTime &&
              moment(row.ActualStartTime).format('HH:mm')) ||
              ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('ActualEndTime', index);
          }}
          style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {(row.ActualEndTime && moment(row.ActualEndTime).format('HH:mm')) ||
              ''}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {row.StartTime &&
              row.EndTime &&
              Math.round(
                FuncCommon.getDiffTime(row.StartTime, row.EndTime) * 100,
              ) / 100}
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {row.ActualStartTime &&
              row.ActualEndTime &&
              Math.round(
                FuncCommon.getDiffTime(row.ActualStartTime, row.ActualEndTime) *
                100,
              ) / 100}
          </Text>
        </TouchableOpacity> */}
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.ActualStartTime &&
              row.ActualEndTime &&
              Math.round(
                FuncCommon.getDiffTime(row.ActualStartTime, row.ActualEndTime) *
                  100,
              ) / 100}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Result}
            autoCapitalize="none"
            onChangeText={Result => {
              this.handleChangeRows(Result, index, 'Result');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: 280}]}
          onPress={() => {
            this.openOrders(index);
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.OrderNumber,
                {color: 'black', fontSize: 14},
              ]}>
              {row.OrderNumber ? row.OrderNumber : ''}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <IconElement
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {this.helperSoNumber(row.OrderNumber) || ''}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 180 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Description}
            autoCapitalize="none"
            onChangeText={Description => {
              if (this.props.seeDetail) {
                return;
              }
              this.handleChangeRows(Description, index, 'Description');
            }}
          />
        </TouchableOpacity>
        {!this.props.seeDetail && (
          <TouchableOpacity
            onPress={() => {
              this.onDelete(index);
            }}
            style={[AppStyles.table_td_custom, { width: 80 }]}>
            <IconElement
              name="delete"
              type="antdesign"
              color={AppColors.red}
              size={16}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  };
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    setTimeout(() => {
      Actions.refresh({ moduleId: 'back', ActionTime: new Date().getTime() });
    }, 100);
  }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ApplyOverTimesAdd_Component);
