import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
  FlatList,
} from 'react-native';
import { Icon, Button } from 'react-native-elements';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import _ from 'lodash';
import controller from './controller';
import { FuncCommon } from '@utils';
import Combobox from '../../component/Combobox';
import { AppStyles, AppColors } from '@theme';
import { API_ApplyOverTimes } from '../../../network';

const pickerSelectStyles = {
  inputIOS: {
    fontSize: 20,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    color: 'black',
    fontSize: 10,
  },
};
export default class TableAddNew extends Component {
  constructor(props) {
    super(props);
    this._search = {};
    this.state = {
      ListEmployee: [],
      ShiftId: undefined,
      StartTime: '',
      modalPickerTimeVisible: false,
      Result: '',
      rows: [{}],
      workShiftData: [],
      listOrder: [],
      ListType: [],
    };
  }
  componentDidMount = () => {
    Promise.all([
      this.getAllEmployeeByDepart(),
      this.getAllWorkShift(),
      this.getAllOrder(),
      this.getType(),
    ]);
    this.setState({ rows: this.props.dataRowInit });
  };
  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.dataRowInit, nextProps.dataRowInit)) {
      this.setState({
        rows: nextProps.dataRowInit,
      });
    }
  }
  componentWillUnmount = () => {
    clearTimeout(this.time);
  };
  getAllOrder = (callback, search = '') => {
    clearTimeout(this.time);
    // debounce call api
    this.time = setTimeout(() => {
      controller.getAllOrder(search, rs => {
        let data = JSON.parse(rs).map(item => ({
          value: item.id,
          text: item.name,
        }));
        data.unshift({
          value: null,
          text: 'Chọn sản phẩm',
        });
        if (callback) {
          callback(data);
        } else {
          this.setState({ listOrder: data });
        }
      });
    }, 300);
  };
  getAllWorkShift = () => {
    controller.getAllWorkShift(rs => {
      let data = JSON.parse(rs).map(item => ({
        ...item,
        value: item.value,
        label: item.text,
      }));
      data.unshift({
        value: null,
        label: 'Chọn ca',
      });
      this.setState({ workShiftData: data });
    });
  };
  getType = () => {
    API_ApplyOverTimes.Type()
      .then(rs => {
        if (rs.data.errorCode === 200) {
          let data = JSON.parse(rs.data.data).map(item => ({
            ...item,
            value: item.id,
            label: item.name,
          }));
          this.setState({ ListType: data });
        }
      })
      .catch(err => console.log(err));
  };
  getAllEmployeeByDepart = () => {
    controller.getAllEmployeeByDepart(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.value,
        label: item.text,
      }));
      data.unshift({
        value: null,
        label: 'Chọn nhân viên',
      });
      this.setState({ ListEmployee: data });
    });
  };
  onDelete = index => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({ rows: res });
  };
  addRows = () => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({ rows: data });
  };
  _openComboboxEmployee() { }
  openComboboxEmployee = d => {
    this._openComboboxEmployee = d;
  };
  onActionComboboxEmployee() {
    this._openComboboxEmployee();
  }
  handleChangeRows = (val, index, name) => {
    const { rows } = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({ rows: res });
  };

  handleChangeTimeInExpected = val => {
    this.setState({ StartTime: val });
  };
  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  handleConfirmTime = date => {
    const { typeTime, currentIndex, rows } = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    if (typeTime === 'ApplyDate') {
      this.props.callBackSearchType(FuncCommon.ConDate(date, 0));
    }
    this.setState({
      rows: res,
      typeTime: null,
      currentIndex: null,
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
    });
    // this.hideDatePicker();
  };
  helperWorkShift = val => {
    const { TypeOfDate } = this.props;
    const { workShiftData } = this.state;
    if (!val) {
      return null;
    }
    let index = workShiftData.findIndex(item => item.value === val);
    if (index === -1) {
      return;
    }
    if (TypeOfDate === 'O') {
      return workShiftData[index].OvertimeDay;
    }
    if (TypeOfDate === 'L') {
      return workShiftData[index].OvertimeHoliday;
    }
    if (TypeOfDate === 'T') {
      return workShiftData[index].OvertimeWeekend;
    }
  };
  helperSoNumber = txt => {
    if (txt && txt.includes('[')) {
      let arr = txt.split('[');
      return arr[1].slice(0, -1);
    }
    return '';
  };
  _openComboboxOrder() { }
  openComboboxOrder = d => {
    this._openComboboxOrder = d;
  };
  openOrders(index) {
    this.setState({ currentIndexItem: index }, () => {
      this._openComboboxOrder();
    });
  }
  ChangeOrder = rs => {
    const { currentIndexItem, rows } = this.state;
    let { value } = rs;
    if (!rs) {
      return;
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.OrderNumber = value;
      }
      return row;
    });
    this.setState({
      rows: data,
      currentIndexItem: null,
    });
  };
  itemFlatList = (row, index) => {
    const stylePicker = {
      inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderRadius: 4,
        color: 'black',
        paddingRight: 15, // to ensure the text is never behind the icon
      },
      inputAndroid: {
        color: 'black',
        fontSize: 10,
      },
      placeholder: {
        color: 'black',
      },
    };
    return (
      <View style={{ flexDirection: 'row' }} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 40 }]}>
          <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, { width: 300 }]}>
          {this.props.seeDetail ? (
            <Text
              style={[AppStyles.Textdefault, { marginLeft: 10, fontSize: 16 }]}>
              {row.EmployeeName}
            </Text>
          ) : (
            <RNPickerSelect
              doneText="Xong"
              onValueChange={value => {
                this.handleChangeRows(value, index, 'EmployeeGuid');
              }}
              items={this.state.ListEmployee}
              value={row.EmployeeGuid}
              placeholder={{}}
            />
          )}
        </View>
        {/* <TouchableOpacity
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.handlePickDateOpen('ApplyDate', index);
          }}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.ApplyDate && moment(row.ApplyDate).format('DD/MM/YYYY')) ||
              ''}
          </Text>
        </TouchableOpacity> */}
        <View style={[AppStyles.table_td_custom, { width: 200 }]}>
          <RNPickerSelect
            doneText="Xong"
            style={{
              ...stylePicker,
              iconContainer: {
                top: 10,
                right: 10,
              },
            }}
            onValueChange={value => {
              this.handleChangeRows(value, index, 'ShiftId');
            }}
            items={this.state.workShiftData}
            value={row.ShiftId}
            placeholder={{
              value: null,
              label: 'Chọn ca',
            }}
            disabled={this.props.seeDetail}
          // Icon={() => {
          //   return (
          //     <Icon
          //       color={AppColors.gray}
          //       name={'chevron-thin-down'}
          //       type="entypo"
          //       size={15}
          //     />
          //   );
          // }}
          />
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 70 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {row.ShiftId && this.helperWorkShift(row.ShiftId)}
          </Text>
        </TouchableOpacity>
        {/* <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <RNPickerSelect
            doneText="Xong"
            style={{
              ...stylePicker,
              iconContainer: {
                top: 10,
                right: 10,
              },
            }}
            onValueChange={value => {
              this.handleChangeRows(value, index, 'IsCheck');
            }}
            items={this.state.ListType}
            value={row.IsCheck}
            placeholder={{value: null, label: 'Chọn'}}
            disabled={this.props.seeDetail}
          />
        </View> */}
        <TouchableOpacity
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.handlePickTimeOpen('StartTime', index);
          }}
          style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {(row.StartTime && moment(row.StartTime).format('HH:mm')) || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.handlePickTimeOpen('EndTime', index);
          }}
          style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {(row.EndTime && moment(row.EndTime).format('HH:mm')) || ''}
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.handlePickTimeOpen('ActualStartTime', index);
          }}
          style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.ActualStartTime &&
              moment(row.ActualStartTime).format('HH:mm')) ||
              ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.handlePickTimeOpen('ActualEndTime', index);
          }}
          style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.ActualEndTime && moment(row.ActualEndTime).format('HH:mm')) ||
              ''}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {row.StartTime &&
              row.EndTime &&
              Math.round(
                FuncCommon.getDiffTime(row.StartTime, row.EndTime) * 100,
              ) / 100}
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.ActualStartTime &&
              row.ActualEndTime &&
              Math.round(
                FuncCommon.getDiffTime(row.ActualStartTime, row.ActualEndTime) *
                  100,
              ) / 100}
          </Text>
        </TouchableOpacity> */}
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.ActualStartTime &&
              row.ActualEndTime &&
              Math.round(
                FuncCommon.getDiffTime(row.ActualStartTime, row.ActualEndTime) *
                  100,
              ) / 100}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Result}
            autoCapitalize="none"
            onChangeText={Result => {
              if (this.props.seeDetail) {
                return;
              }
              this.handleChangeRows(Result, index, 'Result');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: 280}]}
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.openOrders(index);
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.OrderNumber,
                {color: 'black', fontSize: 14},
              ]}>
              {row.OrderNumber ? row.OrderNumber : ''}
            </Text>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
              }}>
              <Icon
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {this.helperSoNumber(row.OrderNumber) || ''}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 180 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Description}
            autoCapitalize="none"
            onChangeText={Description => {
              if (this.props.seeDetail) {
                return;
              }
              this.handleChangeRows(Description, index, 'Description');
            }}
          />
        </TouchableOpacity>
        {!this.props.seeDetail && (
          <TouchableOpacity
            onPress={() => {
              this.onDelete(index);
            }}
            style={[AppStyles.table_td_custom, { width: 80 }]}>
            <Icon
              name="delete"
              type="antdesign"
              color={AppColors.red}
              size={16}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  };
  render() {
    const { headerTable = [] } = this.props;
    return (
      <View
        {...this.props}
        style={{
          marginVertical: 5,
          marginBottom: 15,
        }}>
        {!this.props.seeDetail && (
          <View style={{ width: 150, marginLeft: 10 }}>
            <Button
              title="Thêm mới dòng"
              type="outline"
              size={15}
              onPress={() => this.addRows()}
              buttonStyle={{
                padding: 5,
                marginBottom: 5,
              }}
              titleStyle={{
                marginLeft: 5,
                fontSize: 14,
                fontWeight: 'normal',
              }}
              icon={
                <Icon
                  name="pluscircleo"
                  type="antdesign"
                  color={AppColors.blue}
                  size={14}
                />
              }
            />
          </View>
        )}
        {/* Table */}
        <ScrollView horizontal={true} style={{ marginTop: 5 }}>
          <View style={{ flexDirection: 'column', marginHorizontal: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              {headerTable.map(item => {
                if (item.hideInDetail) {
                  return null;
                }
                return (
                  <TouchableOpacity
                    key={item.title}
                    style={[AppStyles.table_th, { width: item.width }]}>
                    <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <FlatList
                  data={this.state.rows}
                  renderItem={({ item, index }) => {
                    return this.itemFlatList(item, index);
                  }}
                  keyExtractor={(rs, index) => index.toString()}
                />
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'left' }]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>

        {this.state.modalPickerTimeVisible && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisible}
            mode="time"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        {this.state.modalPickerTimeVisibleDate && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisibleDate}
            mode="date"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        {this.state.listOrder.length > 0 && (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeOrder}
            data={this.state.listOrder}
            nameMenu={'Chọn'}
            eOpen={this.openComboboxOrder}
            position={'bottom'}
            value={null}
            callback_SearchPaging={(callback, textsearch) =>
              this.getAllOrder(callback, textsearch)
            }
          />
        )}
      </View>
    );
  }
}
