import React, { Component } from 'react';
import { Keyboard, Text, View, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_ApplyOverTimes, API } from '../../../network';
import { Divider } from 'react-native-elements';
import TabBarBottom from '../../component/TabBarBottom';
import { Actions } from 'react-native-router-flux';
import { AppStyles, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TableAddNew from './TableAddNew';
import _ from 'lodash';
import Toast from 'react-native-simple-toast';

const headerTable = [
  { title: 'STT', width: 40 },
  { title: 'Nhân viên', width: 300 },
  { title: 'Ca đăng ký', width: 200 },
  { title: '% ca', width: 70 },
  // {title: 'Loại làm thêm', width: 150},
  { title: 'TG vào dự kiến', width: 100 },
  { title: 'TG ra dự kiến', width: 100 },
  // {title: 'TG vào thực', width: 100},
  // {title: 'TG ra thực', width: 100},
  { title: 'Số giờ làm thêm', width: 100 },
  // {title: 'Tổng TG thực', width: 100},
  // {title: 'TG tính công', width: 100},
  // {title: 'Kết quả', width: 200},
  // {title: 'Mã sản phẩm', width: 280},
  // {title: 'Số SO', width: 120},
  { title: 'Ghi chú', width: 180 },
  { title: 'Hành động', width: 80, hideInDetail: true },
];

class ApplyOverTimesItem_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      DataDetail: [],
      Title: '',
      checkInLogin: '',
      LoginName: '',
      Dropdown: [
        {
          value: 'L',
          text: 'Tăng ca ngày lễ',
        },
        {
          value: 'T',
          text: 'Tăng ca cuối tuần',
        },
        {
          value: 'O',
          text: 'Tăng ca ngày thường',
        },
      ],
    };
  }

  componentDidMount = () => {
    this.getItem();
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'back') {
      this.getItem();
    }
  }
  getItem() {
    let id = { Id: this.props.RecordGuid || this.props.RowGuid };
    API_ApplyOverTimes.ApplyOverTimes_Items(id)
      .then(res => {
        this.setState({ Data: JSON.parse(res.data.data).model });
        this.setState({ DataDetail: JSON.parse(res.data.data).detail });
        let checkin = {
          RowGuid: this.props.RecordGuid || this.props.RowGuid,
          WorkFlowGuid: JSON.parse(res.data.data).model.WorkFlowGuid,
        };
        API_ApplyOverTimes.ApplyOverTimes_checkin(checkin)
          .then(res => {
            this.setState({ checkInLogin: res.data.data });
          })
          .catch(error => {
            console.log(error.data.data);
          });
        API.getProfile().then(rs => {
          this.setState({ LoginName: rs.data.LoginName });
        });
      })
      .catch(error => {
        console.log('=======' + error.data);
      });
  }
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();

    Actions.refresh({ moduleId: 'back', ActionTime: new Date().getTime() });
  }
  callbackOpenUpdate = () => {
    Actions.applyOverTimesAdd({
      itemData: _.cloneDeep(this.state.Data),
      rowData: _.cloneDeep(this.state.DataDetail),
    });
  };
  getLabelWorkShift = value => {
    if (!value) {
      return '';
    }
    let res = this.state.Dropdown.findIndex(i => i.value === value);
    if (res === -1) {
      return '';
    }
    return this.state.Dropdown[res].text;
  };
  CustomView = item => {
    const dataRowInit = _.cloneDeep(this.state.DataDetail).map(data => {
      if (data.StartTime) {
        data.StartTime = '' + data.ApplyDate.slice(0, 11) + data.StartTime;
      }
      if (data.EndTime) {
        data.EndTime = '' + data.ApplyDate.slice(0, 11) + data.EndTime;
      }
      if (data.ActualStartTime && data.ActualStartTime !== '00:00:00') {
        data.ActualStartTime =
          '' + data.ApplyDate.slice(0, 11) + data.ActualStartTime;
      } else {
        data.ActualStartTime = null;
      }
      if (data.ActualEndTime && data.ActualEndTime !== '00:00:00') {
        data.ActualEndTime =
          '' + data.ApplyDate.slice(0, 11) + data.ActualEndTime;
      } else {
        data.ActualEndTime = null;
      }
      if (data.ItemIDByProvider && data.OrderNumber) {
        data.OrderNumber =
          data.ItemIDByProvider + '-[' + data.OrderNumber + ']';
      }
      if (data.IsCheck !== null && data.IsCheck !== undefined) {
        data.IsCheck = data.IsCheck + '';
      }
      return data;
    });

    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <TabBar_Title
            title={'Chi tiết phiếu làm thêm'}
            callBack={() => this.callBackList()}
          />
          <Divider />
          {/*hiển thị nội dung chính*/}
          <ScrollView>
            <View style={{ padding: 15 }}>
              {/*Mã phiếu*/}
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Mã phiếu :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.ApplyOvertimeId}
                  </Text>
                </View>
              </View>
              {/*Tiêu đề phiêu*/}
              {/* <View style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Tên phiếu :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>{item.Title}</Text>
                </View>
              </View> */}
              {/*Người tạo phiếu*/}
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.EmployeeName}
                  </Text>
                </View>
              </View>
              {/*Phòng ban*/}
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.DepartmentName}
                  </Text>
                </View>
              </View>
              {/*Chức vụ*/}
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Chức vụ :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.JobTitleName}
                  </Text>
                </View>
              </View>
              {/*Ngày tạo phiếu*/}
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày đăng ký :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.customDate(item.ApplyDate)}
                  </Text>
                </View>
              </View>
              {/* loai ca lam them */}
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Loại ca làm thêm :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.getLabelWorkShift(item.Type)}
                  </Text>
                </View>
              </View>
              {/*nội dung phiếu*/}

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  {this.state.Data.Status == 'Y' ? (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { color: AppColors.AcceptColor },
                      ]}>
                      {item.StatusWF || ''}
                    </Text>
                  ) : (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { color: AppColors.PendingColor },
                      ]}>
                      {item.StatusWF || ''}
                    </Text>
                  )}
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.Description}
                  </Text>
                </View>
              </View>
            </View>
            {/*hiển thị nội dung chi tiết*/}
            <Divider />

            <Text style={{ fontSize: 16, fontWeight: 'bold', padding: 10 }}>
              Chi tiết phiếu
            </Text>
            <View style={{ flex: 5, marginBottom: 15 }}>
              <TableAddNew
                ref={this.tableRef}
                headerTable={headerTable}
                TypeOfDate={this.state.Data.Type}
                dataRowInit={dataRowInit}
                seeDetail={true}
              />
            </View>
          </ScrollView>
          {/*nút xử lý*/}
          <View style={[AppStyles.StyleTabvarBottom]}>
            {this.state.checkInLogin !== '' ? (
              <TabBarBottom
                onDelete={() => this.onDelete()}
                backListByKey="ApplyOverTimes"
                Title={item.Title}
                Permisstion={item.Permisstion}
                onAttachment={() => this.openAttachments()}
                checkInLogin={this.state.checkInLogin}
                callbackOpenUpdate={this.callbackOpenUpdate}
                onSubmitWF={(callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
                }
                keyCommentWF={{
                  ModuleId: 30,
                  RecordGuid: this.props.RecordGuid || this.props.RowGuid,
                  Title: item.ApplyOvertimeId,
                }}
              />
            ) : null}
          </View>
        </View>
      </View>
    );
  };

  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  openAttachments() {
    var obj = {
      ModuleId: '16',
      RecordGuid: this.props.RecordGuid || this.props.RowGuid,
      notEdit: this.state.checkInLogin === '1' ? false : true,
    };
    Actions.attachmentComponent(obj);
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      RowGuid: this.props.RecordGuid || this.props.RowGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'D') {
      API_ApplyOverTimes.ApplyOverTimes_approve(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      API_ApplyOverTimes.ApplyOverTimes_noapprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }
  //xóa
  onDelete = () => {
    let id = { Id: this.props.RecordGuid || this.props.RowGuid };
    this.setState({ loading: true });
    API_ApplyOverTimes.ApplyOverTimes_Delete(id.Id)
      .then(response => {
        if (!response.data.Error) {
          this.setState({
            loading: false,
            RowGuid: this.props.RecordGuid || this.props.RowGuid,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}
const mapStateToProps = state => ({
  applications: state.user.data,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(ApplyOverTimesItem_Component);
