import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  //#endregion
};
