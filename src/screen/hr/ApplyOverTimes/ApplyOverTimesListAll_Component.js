import React, {Component} from 'react';
import {
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, Icon, ListItem, SearchBar} from 'react-native-elements';
import {API_ApplyOverTimes} from '@network';
import Fonts from '../../../theme/fonts';
import {FuncCommon} from '../../../utils';
import Combobox from '../../component/Combobox';
import * as UserAction from '@redux/user/actions';
import {AppStyles, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import controller from './controller';
const SCREEN_WIDTH = Dimensions.get('window').width;
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 5,
    paddingVertical: 5,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
  timeHeader: {
    marginBottom: 3,
  },
});

class ApplyOverTimesListAll_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      isFetching: false,
      Data: null,
      ListAll: [],
      refreshing: false,
      ListEmployee: [],
      EvenFromSearch: false,
      JTableBom: {
        EndDate: new Date(),
        StartDate: new Date(),
        length: 10,
        search: {
          value: '',
        },
        CurrentPage: 1,
      },
    };
    this._search = {
      StartDate: new Date(),
      EndDate: new Date(),
      Status: 'W',
      CurrentPage: 0,
      search: {value: ''},
      Length: 10,
      EmployeeName: '',
      EmployeeId: '',
      QueryOrderBy: 'ApplyOvertimeID DESC',
      Total: 0,
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'CD',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'DD',
        Checkbox: false,
      },
    ];
    this.onEndReachedCalledDuringMomentum = true;
  }

  componentDidMount = () => {
    // this.getAllEmployee();
  };
  EvenFromSearch() {
    this.setState(prevState => ({EvenFromSearch: !prevState.EvenFromSearch}));
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'back') {
      this.nextPage('reload');
    }
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.search.value = search;
    this._search.CurrentPage = 0;
    this.nextPage();
  };
  // List nhan vien
  // getAllEmployee = () => {
  //   controller.getAllEmployee(rs => {
  //     let _rs = JSON.parse(rs);
  //     _rs.unshift({
  //       value: null,
  //       text: 'Tất cả',
  //     });
  //     this.setState({
  //       ListEmployee: _rs,
  //     });
  //   });
  // };
  getAll = type => {
    this.setState(
      {
        refreshing: true,
      },
      () => {
        this._search.CurrentPage++;
        if (type === 'reload') {
          this._search.CurrentPage = 1;
        }
        let _obj = {
          ...this._search,
          StartDate: FuncCommon.ConDate(this._search.StartDate, 2),
          EndDate: FuncCommon.ConDate(this._search.EndDate, 2),
        };
        API_ApplyOverTimes.ApplyOverTimes_ListAll(_obj)
          .then(res => {
            if (res.data.errorCode !== 200) {
              this.setState({
                refreshing: false,
                ListAll: [],
              });
              return;
            }
            let _data = JSON.parse(res.data.data);
            if (type === 'reload') {
              this.state.ListAll = _data.data;
            } else {
              this.state.ListAll = this.state.ListAll.concat(_data.data);
            }
            this._search.Total = _data.recordsTotal;
            if (this._search.Status === 'W') {
              this.listtabbarBotom[0].Badge = _data.recordsTotal;
            }
            FuncCommon.Data_Offline_Set('ApplyOverTimes', this.state.ListAll);
            this.setState({
              ListAll: this.state.ListAll,
              refreshing: false,
            });
          })
          .catch(error => {
            this.setState({
              refreshing: false,
            });
          });
      },
    );
  };
  //List danh sách phân trang
  nextPage(type) {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.getAll(type);
      } else {
        let data = await FuncCommon.Data_Offline_Get('ApplyOverTimes');
        this.setState({
          ListAll: data,
          refreshing: false,
        });
      }
    });
  }

  //#region thay đổi thời gian tìm kiếm
  setStartDate = date => {
    this._search.StartDate = date;
  };
  setEndDate = date => {
    this._search.EndDate = date;
  };
  loadMoreData() {
    if (this._search.Total > this.state.ListAll.length) {
      this.nextPage();
    }
  }
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  ChangeEmployee = rs => {
    if (!rs) {
      return;
    }
    this._search.EmployeeId = rs.value;
    this._search.EmployeeName = rs.text;
    this.nextPage('reload');
  };
  _openComboboxEmployee() {}
  openComboboxEmployee = d => {
    this._openComboboxEmployee = d;
  };
  onActionComboboxEmployee() {
    this._openComboboxEmployee();
  }
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.nextPage('reload');
    }
  };
  CustomeListAll = item => (
    <FlatList
      data={item}
      style={{flex: 1}}
      renderItem={({item, index}) => (
        <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
          <ListItem
            title={() => {
              return (
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 3, flexDirection: 'row'}}>
                    <View style={{flex: 5}}>
                      <Text style={[AppStyles.Titledefault]}>
                        {item.ApplyOvertimeId}
                      </Text>
                      {/* <Text style={[AppStyles.Textdefault]}>
                        Tiêu đề: {item.Title}
                      </Text> */}
                      <Text style={[AppStyles.Textdefault]}>
                        Người tạo: {item.EmployeeName}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 2,
                      alignItems: 'flex-end',
                      justifyContent: 'flex-start',
                    }}>
                    {item.Status == 'Y' ? (
                      <Text
                        style={[
                          {
                            justifyContent: 'flex-start',
                            color: AppColors.AcceptColor,
                          },
                          AppStyles.Textdefault,
                        ]}>
                        {item.StatusWF}
                      </Text>
                    ) : (
                      <Text
                        style={[
                          {
                            justifyContent: 'flex-start',
                            color: AppColors.PendingColor,
                          },
                          AppStyles.Textdefault,
                        ]}>
                        {item.StatusWF}
                      </Text>
                    )}
                    <Text style={[AppStyles.Textdefault]}>
                      {' '}
                      {item.ApplyDateString}
                    </Text>
                  </View>
                </View>
              );
            }}
            bottomDivider
            //chevron
            onPress={() => this.onViewItem(item)}
          />
        </View>
      )}
      onEndReached={() => this.loadMoreData()}
      onEndReachedThreshold={0.5}
      onMomentumScrollBegin={() => {
        this.onEndReachedCalledDuringMomentum = false;
      }}
      keyExtractor={(item, index) => index.toString()}
      ListEmptyComponent={this.ListEmpty}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['red', 'green', 'blue']}
        />
      }
    />
  );
  ListEmpty = () => {
    if (this.state.ListAll && this.state.ListAll.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={[AppStyles.centerAligned, {marginTop: 10}]}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  render() {
    const IS_NV = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'NV';
    return (
      <TouchableWithoutFeedback style={{flex: 1}}>
        <View style={styles.container}>
          <TabBar
            title={'Đăng ký làm thêm giờ'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            BackModuleByCode={'MyProfile'}
            addForm={true}
            CallbackFormAdd={() => Actions.applyOverTimesAdd()}
          />
          <Divider />
          {this.state.EvenFromSearch == true ? (
            <View style={{flexDirection: 'column'}}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    padding: 5,
                  }}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian bắt đầu
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventStartDate: true})}>
                    <Text>{FuncCommon.ConDate(this._search.StartDate, 0)}</Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian kết thúc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventEndDate: true})}>
                    <Text>{FuncCommon.ConDate(this._search.EndDate, 0)}</Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column', padding: 5}}>
                  <Text
                    style={[
                      AppStyles.Labeldefault,
                      {textAlign: 'center'},
                      styles.timeHeader,
                    ]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              {/* {!IS_NV && (
                <TouchableOpacity
                  style={[AppStyles.FormInput, {marginHorizontal: 10}]}
                  onPress={() => this.onActionComboboxEmployee()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this._search.employee
                        ? {color: 'black'}
                        : {color: AppColors.gray},
                    ]}>
                    {this._search.EmployeeName
                      ? this._search.EmployeeName
                      : 'Chọn nhân viên...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      color={AppColors.gray}
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
              )} */}

              <SearchBar
                placeholder="Tìm kiếm..."
                lightTheme
                round
                inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                containerStyle={AppStyles.FormSearchBar}
                onChangeText={text => this.updateSearch(text)}
                value={this._search.search.value}
              />
            </View>
          ) : null}
          <View style={{flex: 1}}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>
          {/* hiển thị nút tuỳ chọn */}
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.onCallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage('reload'),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage('reload'),
                    this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {/* {this.state.ListEmployee.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeEmployee}
              data={this.state.ListEmployee}
              nameMenu={'Chọn nhân viên'}
              eOpen={this.openComboboxEmployee}
              position={'bottom'}
              value={this._search.EmployeeId}
            />
          ) : null} */}
        </View>
      </TouchableWithoutFeedback>
    );
  }
  //view item
  onViewItem(item) {
    Actions.applyOverTimesItem({
      RecordGuid: item.RowGuid,
    });
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'home':
        Actions.app();
        break;
      case 'CD':
        this._search.search.value = '';
        (this._search.Status = 'W'), this.updateSearch('');
        break;
      case 'DD':
        this._search.search.value = '';
        (this._search.Status = 'Y'), this.updateSearch('');
        break;
      default:
        break;
    }
  }
  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }

  onclickreturnSearch() {
    this._search.search.value = '';
    this.updateSearch('');
  }

  onValueChange(item) {
    console.log('============>>' + item);
    switch (item) {
      case 'Status': {
        break;
      }
      case 'StartDate': {
        break;
      }
      case 'EndDate': {
        break;
      }
      default: {
      }
    }
  }

  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  //Load lại
  _onRefresh = () => {
    this.setState({refreshing: true});
    this.nextPage('reload');
  };
}

const mapStateToProps = state => ({
  applications: state.user.data,
});

const mapDispatchToProps = {
  addItem: data => UserAction.applications(data),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ApplyOverTimesListAll_Component);
