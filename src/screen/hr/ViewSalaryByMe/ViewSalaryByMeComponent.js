import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
} from 'react-native';
// import Icon from "react-native-vector-icons/FontAwesome";
import { Actions } from 'react-native-router-flux';
import { Divider, Icon } from 'react-native-elements';
import { Thumbnail, Text } from 'native-base';
import TabBar from '../../component/TabBar';
import { connect } from 'react-redux';
import { API_HR } from '@network';
import { ErrorHandler } from '@error';
import { AppStyles, AppColors } from '@theme';
import LoadingComponent from '../../component/LoadingComponent';
import { FuncCommon } from '../../../utils';
import Combobox from '../../component/Combobox';

class GetViewSalary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: {},
      status: null,
      EmployeeGuid: null,
      FullName: null,
      bankDataItem: null,
      loading: true,
      contents: [],
      EvenFromSearch: false,
    };
    this.DropdownMonth = FuncCommon.getListTime().DropdownMonth;
    this.DropdownYear = FuncCommon.getListTime().DropdownYear;
    this.staticParam = {
      Year: new Date().getFullYear(),
      Month: new Date().getMonth() + 1,
    };
  }
  componentDidMount(): void {
    this.GetEmployeeById();
  }
  renderLeftComponent = () => {
    let leftIcon = 'ios-arrow-round-back';
    let leftIconSet = 'ionicon';
    return (
      <TouchableOpacity
        style={[
          styles.squareContainer,
          { alignItems: 'center', justifyContent: 'center' },
        ]}
        onPress={() => this.onPressback()}>
        <Icon name={leftIcon} type={leftIconSet} color="black" size={30} />
      </TouchableOpacity>
    );
  };
  renderRightComponent = () => { };
  renderTitle = () => {
    let title = 'Thông tin chi tiết bảng lương';
    return (
      <View
        style={{
          alignItems: 'center',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text
          style={[
            AppStyles.headerStyle,
            { fontWeight: 'bold', marginLeft: 0, fontSize: 18, color: 'black' },
          ]}>
          {title}
        </Text>
      </View>
    );
  };
  onPressback() {
    Actions.ViewSalary();
  }
  // combobox tháng
  _openComboboxMonth() { }

  openComboboxMonth = d => {
    this._openComboboxMonth = d;
  };

  onActionComboboxMonth() {
    this._openComboboxMonth();
  }

  // combobox năm
  _openComboboxYear() { }

  openComboboxYear = d => {
    this._openComboboxYear = d;
  };

  onActionComboboxYear() {
    this._openComboboxYear();
  }
  viewSalary = () => {
    // if(Object.keys(this.state.Data).length === 0){
    //   return this.ListEmpty()
    // }
    return (
      <View style={{ flex: 1 }}>
        {this.state.EvenFromSearch == true ? (
          <View style={{ flexDirection: 'column', marginBottom: 10 }}>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 10,
                paddingTop: 5,
              }}>
              <View style={{ flexDirection: 'column', flex: 3 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={AppStyles.Labeldefault}>Tháng</Text>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput, { marginRight: 5 }]}
                  onPress={() => this.onActionComboboxMonth()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.staticParam.Month == '' ||
                        this.staticParam.Month === null
                        ? { color: AppColors.gray }
                        : { color: 'black' },
                    ]}>
                    {this.staticParam.Month !== '' &&
                      this.staticParam.Month !== null
                      ? this.staticParam.Month
                      : 'Chọn Tháng...'}
                  </Text>
                  <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <Icon
                      color="#888888"
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'column', flex: 3 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={AppStyles.Labeldefault}>Năm</Text>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionComboboxYear()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.staticParam.Year == '' ||
                        this.staticParam.Year == null
                        ? { color: AppColors.gray }
                        : { color: 'black' },
                    ]}>
                    {this.staticParam.Year !== '' &&
                      this.staticParam.Year !== null
                      ? this.staticParam.Year
                      : 'Chọn Năm...'}
                  </Text>
                  <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <Icon
                      color="#888888"
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        ) : (
          <View thumbnail style={{ marginTop: 10, alignItems: 'center' }}>
            <Thumbnail
              source={API_HR.GetPicApplyLeaves(
                global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
              )}
            />
            <Text numberOfLines={1}>{this.state.Data.EmployeeName}</Text>
          </View>
        )}
        <Divider />
        <ScrollView style={{ flex: 1, padding: 5 }}>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Mã nhân viên:</Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {global.__appSIGNALR.SIGNALR_object.USER.EmployeeId}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Bộ phận:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {global.__appSIGNALR.SIGNALR_object.USER.DepartmentName}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Cấp bậc:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {global.__appSIGNALR.SIGNALR_object.USER.JobTitleName}
              </Text>
            </View>
          </TouchableOpacity>
          {/* <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Ngày công:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.state.Data.ActualWorkDay}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Ngày nghỉ:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.state.Data.UnpaidLeave}
              </Text>
            </View>
          </TouchableOpacity> */}

          <Divider />
          <TouchableOpacity style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
              Lương, phụ cấp tháng {this.state.Data.Thang}:{' '}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Mức lương(Cb chính):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.BasicSalary)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Mức lương(Cb phụ):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowAccident)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Mức lương(phụ cấp TN):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowRes)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Mức lương xăng xe, nhà ở:
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowEnvironment)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Tổng:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowWorking)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Lương ngày công chính:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.SalaryDay)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Lương ngày công phụ:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowProject)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Lương giờ công:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.OtherSalary)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Ngày công đi làm(Ban ngày):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.WorkingDay)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Ngày công đi làm(Đêm):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.ProbationaryWork)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Ngày công đi làm(Nghỉ chiều thứ 7):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowFamily)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Phụ cấp làm đêm:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.TotalMinus)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Lương chính TT:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.PercentSalary)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Lương phụ TT:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.SalaryWorkDay)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Ngày công đi làm(Phép hưởng lương):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.state.Data.PaidLeave}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Ngày công đi làm(Nghỉ lễ):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.state.Data.UnApplyLeave}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Nghỉ không lương:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.state.Data.AllowMarriage}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Thêm giờ(Ngoài giờ):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.SalaryHours)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Thêm giờ(Chủ nhật):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.OvertimeWeekday)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Thêm giờ(Ngày lễ):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.OvertimeHoliday)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Thêm giờ(Sau đêm):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.SupportBonus)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Lương thêm giờ(Ngoài giờ):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.TotalOvertime)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Lương thêm giờ(Chủ nhật):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.OvertimeWeekend)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Lương thêm giờ(Ngày lễ):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.SalaryOvertimeHoliday)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Lương thêm giờ(Sau đêm):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.TotalSalaryOt)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Thu nhập khác(Phụ cấp ăn ca):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowLunch)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Thu nhập khác(Phụ cấp bữa phụ 1):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowMeal1)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Thu nhập khác(Phụ cấp bữa phụ 2):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowMeal2)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Thu nhập khác(Phụ cấp bữa phụ 3):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowMeal3)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Thu nhập khác(Phụ cấp ăn ngoài):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowMealOut)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Thu nhập khác(Phụ cấp ca kíp):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(
                  this.state.Data.AllowIso > 500000
                    ? 500000
                    : this.state.Data.AllowIso,
                )}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Thu nhập khác(Phụ cấp chuyên cần):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.Allow5S)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Thu nhập khác(Khác):</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.Advances)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>
                Thu nhập khác(Tổng TN khác):
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.OtherBonus)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Tổng thu nhập thực tế:</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.TotalSalary)}
              </Text>
            </View>
          </TouchableOpacity>
          <Divider />
          <TouchableOpacity style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
              Các khoản giảm trừ tháng {this.state.Data.Thang}:{' '}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 8 }}>
              <Text style={AppStyles.Labeldefault}>
                Các khoản giảm trừ(Tạm ứng):
              </Text>
            </View>
            <View style={{ flex: 0, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.PayKeepSalary)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 8 }}>
              <Text style={AppStyles.Labeldefault}>
                Các khoản giảm trừ(Trừ tiền ăn):
              </Text>
            </View>
            <View style={{ flex: 0, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AllowInflation)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 8 }}>
              <Text style={AppStyles.Labeldefault}>
                Các khoản giảm trừ(Công đoàn):
              </Text>
            </View>
            <View style={{ flex: 0, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.PersonalInsurance)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 8 }}>
              <Text style={AppStyles.Labeldefault}>
                Các khoản giảm trừ(BHXH):
              </Text>
            </View>
            <View style={{ flex: 0, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.CompanyInsurance)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 8 }}>
              <Text style={AppStyles.Labeldefault}>
                Các khoản giảm trừ(TNCN):
              </Text>
            </View>
            <View style={{ flex: 0, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.AmountTax)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 8 }}>
              <Text style={AppStyles.Labeldefault}>
                Các khoản giảm trừ(Phạt/ Khác):
              </Text>
            </View>
            <View style={{ flex: 0, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.MonetaryFine)}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 8 }}>
              <Text style={AppStyles.Labeldefault}>
                Các khoản giảm trừ(Tổng):
              </Text>
            </View>
            <View style={{ flex: 0, alignItems: 'flex-end' }}>
              <Text style={([AppStyles.Textdefault], { textAlign: 'right' })}>
                {this.addPeriod(this.state.Data.SalaryLeave)}
              </Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
        <Divider />
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 5,
            marginTop: 5,
            marginRight: 5,
          }}>
          <View style={{ flex: 8 }}>
            <Text style={AppStyles.Labeldefault}>Tổng thực lĩnh:</Text>
          </View>
          <View style={{ flex: 0, alignItems: 'flex-end' }}>
            <Text style={styles.bigBlue}>
              {this.addPeriod(this.state.Data.ActualSalary)}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };
  ListEmpty = () => {
    return (
      <View>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  render() {
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <TouchableWithoutFeedback
        style={{ flex: 1 }}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={AppStyles.container}>
          <TabBar
            title={`Phiếu lương Tháng ${this.staticParam.Month}`}
            BackModuleByCode={'MyProfile'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({ EvenFromSearch: callback })
            }
          />
          {this.viewSalary()}
          {this.DropdownMonth.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.onValueChangeTypeMonth}
              data={this.DropdownMonth}
              nameMenu={'Chọn Tháng'}
              eOpen={this.openComboboxMonth}
              position={'bottom'}
            />
          ) : null}
          {this.DropdownYear.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.onValueChangeTypeYear}
              data={this.DropdownYear}
              nameMenu={'Chọn Năm'}
              eOpen={this.openComboboxYear}
              position={'bottom'}
            />
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }
  onValueChangeTypeMonth = data => {
    if (!data.value) {
      return;
    }
    this.staticParam.Month = data.value;
    this.GetEmployeeById();
  };
  onValueChangeTypeYear = data => {
    if (!data.value) {
      return;
    }
    this.staticParam.Year = data.value;
    this.GetEmployeeById();
  };
  GetEmployeeById = () => {
    let obj = {
      EmployeeGuid: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
      Month: this.staticParam.Month,
      Year: this.staticParam.Year,
    };
    API_HR.getViewSalary(obj)
      .then(res => {
        console.log(res);
        let _data = JSON.parse(res.data.data);
        this.setState({
          loading: false,
          Data: _data ? _data : {},
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  bigBlue: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 15,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GetViewSalary);
