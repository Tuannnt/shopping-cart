import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, View, TextInput } from "react-native";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import API_Announcements from "../../../network/HR/API_Announcements";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';

class ListAnnouncementsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 15,
                Search: "",
                QueryOrderBy: "StartDate DESC",
                StartDate: "2020-02-01T02:13:13.779Z",
                EndDate: "2020-02-29T02:13:13.779Z",
                Status: "Y",
                IsPublic: true,
            },
            openSearch: false
        };
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_Announcements.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        this.GetAll();
    }

    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                Status: this.state.staticParam.Status,
                IsPublic: this.state.staticParam.IsPublic
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Danh sách thông báo'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'HR'}
                />
                {this.state.openSearch == true ?
                    <FormSearch
                        CallbackSearch={(callback) => this.updateSearch(callback)}
                    />
                    : null}
                <View style={{ flex: 9 }}>
                    <FlatList
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.RowGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family, flexDirection: 'row' }}>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Icon style={{ color: 'black', position: 'absolute' }} name={'filetext1'} type='antdesign' size={30} />
                                </View>
                                <ListItem
                                    style={{ flex: 11, alignItems: 'center' }}
                                    title={item.Title}
                                    subtitle={() => {
                                        return (
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ width: '50%' }}>
                                                    <Text style={AppStyles.Textdefault}>Trạng thái: {item.IsPublic == true ? 'Công khai' : item.IsPublic == false ? 'Nội bộ' : ''}</Text>
                                                    <Text style={AppStyles.Textdefault}>Tình trạng: {item.Status == 'Y' ? 'Phát hành' : item.Status == 'N' ? 'Chưa phát hành' : item.Status == 'E' ? 'Hết hạn' : ''}</Text>
                                                </View>
                                                <View style={{ width: '50%' }}>
                                                    <Text style={AppStyles.Textdefault}>Ngày BĐ: {this.customDate(item.StartDate)}</Text>
                                                    <Text
                                                        style={AppStyles.Textdefault}>Ngày KT: {this.customDate(item.EndDate)}</Text>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    titleStyle={AppStyles.Titledefault}
                                    onPress={() => this.openView(item.AnnouncementGuid)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                </View>
            </View>
        );
    }
    onRefresh = () => {
        this.setState({ refreshing: true })
        this.GetAll();
    }
    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                IsQCComfirm: this.state.staticParam.IsQCComfirm,
            }
        });
        this.GetAll();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
    openView(id) {
        Actions.hrItemAnnouncements({ viewId: id });
    }
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListAnnouncementsComponent);
