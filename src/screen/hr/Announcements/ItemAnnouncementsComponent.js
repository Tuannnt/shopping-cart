import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import ErrorHandler from '../../../error/handler';
import API_Announcements from '../../../network/HR/API_Announcements';
import {WebView} from 'react-native-webview';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';

const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});

var width = Dimensions.get('window').width; //full width
class ItemAnnouncementsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
      listDetail: [],
      staticParam: {
        PopaymentGuid: null,
        QueryOrderBy: 'ItemName desc',
      },
      isHidden: false,
      loading: true,
    };
  }

  componentDidMount(): void {
    this.getItem();
  }
  getItem() {
    let id = this.props.viewId;
    API_Announcements.GetItems(id)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log(error.data.data);
      });
  }

  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết thông báo'}
          callBack={() => this.onPressBack()}
          FormAttachment={true}
          CallbackFormAttachment={() => this.openAttachments()}
        />
        <View style={{flexDirection: 'row', padding: 10}}>
          <View style={{flex: 10}}>
            <Text style={AppStyles.Titledefault}>{this.state.Data.Title}</Text>
          </View>
        </View>
        <View style={{paddingLeft: 20, backgroundColor: '#fff'}}>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Bộ phận :</Text>
            <Text style={[AppStyles.Textdefault, {flex: 2}]}>
              {this.state.Data.DepartmentName}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
              Ngày bắt đầu :
            </Text>
            <Text style={[AppStyles.Textdefault, {flex: 2}]}>
              {this.customDate(this.state.Data.StartDate)}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
              Ngày kết thúc :
            </Text>
            <Text style={[AppStyles.Textdefault, {flex: 2}]}>
              {this.customDate(this.state.Data.EndDate)}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Nội dung :</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              height: '100%',
              width: '100%',
            }}>
            <WebView
              style={[AppStyles.Textdefault, {flex: 2}]}
              originWhitelist={['*']}
              source={{html: this.state.Data.Contents}}
              source={{
                html:
                  '<Text style="font-size:250%; color:"#989898"">' +
                  this.state.Data.Contents +
                  '</Text>',
              }}
            />
          </View>
        </View>
      </View>
    );
  }

  openAttachments() {
    var obj = {
      ModuleId: '19',
      RecordGuid: this.state.Data.AnnouncementGuid,
    };
    Actions.attachmentComponent(obj);
  }

  onPressBack() {
    Actions.pop();
  }

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemAnnouncementsComponent);
