import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  Alert,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_TrainingRequests from '../../../network/HR/API_TrainingRequests';
import API from '../../../network/API';
import ErrorHandler from '../../../error/handler';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';

const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});

class ItemTrainingRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.DataView = null;
    this.state = {
      Data: null,
      listDetail: [],
      staticParam: {
        TrainingRequestGuid: null,
        QueryOrderBy: 'TrainingRequestId desc',
      },
      isHidden: false,
      checkInLogin: '',
      selected: undefined,
      RequestComment: '',
      ReasonName: '',
      Type: '',
      Receivers: [],
      FullName: '',
      loading: true,
    };
  }

  componentDidMount(): void {
    this.getItem();
    API.getProfile().then(rs => {
      this.setState({FullName: rs.data.FullName});
    });
  }

  getItem() {
    let id = this.props.RecordGuid;
    API_TrainingRequests.GetItem(id)
      .then(res => {
        this.DataView = JSON.parse(res.data.data);
        let check = {
          TrainingRequestGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_TrainingRequests.CheckLogin(check)
          .then(rs => {
            this.setState({checkInLogin: rs.data.data});
          })
          .catch(error => {
            console.log(error.data.data);
          });
        this.GetDetail();
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  }

  GetDetail = () => {
    this.setState({refreshing: true});
    this.state.staticParam.TrainingRequestGuid = this.props.RecordGuid;
    API_TrainingRequests.ListDetail(this.state.staticParam)
      .then(res => {
        this.state.listDetail = JSON.parse(res.data.data);
        this.setState({
          list: this.state.listDetail,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };

  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };

  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết yêu cầu đào tạo'}
          callBack={() => this.onPressBack()}
        />
        <View style={{flexDirection: 'row', padding: 10}}>
          <View style={{flex: 10}}>
            <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
          </View>
          <View style={{flex: 1}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHidden == false
                  ? this.setState({isHidden: true})
                  : this.setState({isHidden: false})
              }
            />
          </View>
        </View>
        {this.DataView !== null ? (
          <View style={{flex: 1}}>
            <View
              style={[
                this.state.isHidden == true ? styles.hidden : '',
                {padding: 20, backgroundColor: '#fff', flexDirection: 'row'},
              ]}>
              <View style={{flex: 1}}>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Số phiếu :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Người yêu cầu :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Bộ phận yêu cầu :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Loại yêu cầu :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Ngày yêu cầu :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  TT xử lý :
                </Text>
              </View>
              <View style={{flex: 2, alignItems: 'flex-end'}}>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.DataView.TrainingRequestId}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.DataView.EmployeeName}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.DataView.DepartmentName}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {' '}
                  {this.DataView.TypeOfRequest == 'P'
                    ? 'Phát sinh'
                    : this.DataView.TypeOfRequest == 'K'
                    ? 'Kế hoạch năm'
                    : ''}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.customDate(this.DataView.CreatedDate)}
                </Text>
                <Text
                  style={[
                    [AppStyles.Textdefault],
                    this.DataView.Status == 'Y'
                      ? {color: AppColors.AcceptColor}
                      : {color: AppColors.PendingColor},
                  ]}>
                  {this.DataView.StatusWF}
                </Text>
              </View>
            </View>
            <Divider style={{marginBottom: 10}} />
            <View style={{flexDirection: 'row', paddingRight: 10}}>
              <Text
                style={[AppStyles.Titledefault, {flex: 1, paddingLeft: 10}]}>
                Chi tiết
              </Text>
              <Icon
                style={{color: 'black', flex: 1}}
                name={'attach-file'}
                type="MaterialIcons"
                size={20}
                onPress={() => this.openAttachments()}
              />
            </View>
            <View style={{flex: 6}}>
              <ScrollView>
                {this.state.listDetail.map((item, index) => (
                  <View
                    style={{
                      fontSize: 11,
                      fontFamily: Fonts.base.family,
                      flex: 5,
                    }}>
                    <ListItem
                      title={item.Title}
                      titleStyle={AppStyles.Titledefault}
                      subtitle={() => {
                        return (
                          <View>
                            <View style={{flexDirection: 'row'}}>
                              <View style={{width: '50%'}}>
                                <Text
                                  style={[
                                    AppStyles.Textdefault,
                                    {paddingBottom: 5},
                                  ]}>
                                  Nhân viên: {item.EmployeeName}
                                </Text>
                              </View>
                              <View style={{width: '50%'}}>
                                <Text
                                  style={[
                                    AppStyles.Textdefault,
                                    {paddingBottom: 5},
                                  ]}>
                                  Bộ phận: {item.DepartmentName}
                                </Text>
                              </View>
                            </View>
                          </View>
                        );
                      }}
                    />
                  </View>
                ))}
              </ScrollView>
            </View>
            {/*nút xử lý*/}
            <View style={{flex: 1}}>
              <TabBarBottom
                //key để quay trở lại danh sách
                backListByKey="TrainingRequests"
                //Truyễn dữ liệu sang màn hình comment
                keyCommentWF={{
                  ModuleId: '27',
                  RecordGuid: this.DataView.TrainingRequestGuid,
                  Title: this.DataView.TrainingRequestId,
                }}
                // tiêu đề hiển thị trong popup xử lý phiếu
                Title={this.DataView.TrainingRequestId}
                //kiểm tra quyền xử lý
                Permisstion={this.DataView.Permisstion}
                //kiểm tra bước đầu quy trình
                checkInLogin={this.state.checkInLogin}
                onSubmitWF={(callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
                }
              />
            </View>
          </View>
        ) : null}
      </View>
    );
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    console.log('callback==' + callback + CommentWF);
    let obj = {
      TrainingRequestGuid: this.props.RecordGuid,
      WorkFlowGuid: this.DataView.WorkFlowGuid,
      // LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'B') {
      API_TrainingRequests.Approve(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
              cancelable: true,
            });
            this.getItem();
            var obj = {
              RecordGuid: this.DataView.TrainingRequestGuid,
              ModuleId: 'HR_TrainingRequests',
              Data: JSON.parse(res.data.data).Reciever,
              Head: 'Thông báo yêu cầu đào tạo',
              Content:
                'Số phiếu : ' +
                this.DataView.TrainingRequestId +
                '\n' +
                'Ngày đề nghị : ' +
                this.customDate(this.DataView.CreatedDate) +
                '\n' +
                'Người yêu cầu : ' +
                this.state.FullName +
                '\n' +
                'Nội dung : ' +
                CommentWF +
                '\n' +
                'Trạng thái : ' +
                (JSON.parse(res.data.data).Status == 'Y'
                  ? 'Đã duyệt'
                  : 'Chờ duyệt') +
                '\n' +
                'Trạng thái phiếu : ' +
                JSON.parse(res.data.data).StatusWF,
            };
            API.SendNotiCommon(obj)
              .then(rs => {
                console.log(res);
              })
              .catch(error => {
                console.log('Lỗi lỗi====>', error);
              });
          } else {
            Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
              cancelable: true,
            });
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      API_TrainingRequests.NotApprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
              cancelable: true,
            });
            this.getItem();
            var obj = {
              RecordGuid: this.DataView.TrainingRequestGuid,
              ModuleId: 'HR_TrainingRequests',
              Data: JSON.parse(res.data.data).Reciever,
              Head: 'Thông báo yêu cầu đào tạo',
              Content:
                'Số phiếu : ' +
                this.DataView.TrainingRequestId +
                '\n' +
                'Ngày đề nghị : ' +
                this.customDate(this.DataView.CreatedDate) +
                '\n' +
                'Người yêu cầu : ' +
                this.state.FullName +
                '\n' +
                'Nội dung : ' +
                CommentWF +
                '\n' +
                'Trạng thái : ' +
                (JSON.parse(res.data.data).Status == 'Y'
                  ? 'Đã duyệt'
                  : 'Chờ duyệt') +
                '\n' +
                'Trạng thái phiếu : ' +
                JSON.parse(res.data.data).StatusWF,
            };
            API.SendNotiCommon(obj)
              .then(rs => {
                console.log(res);
              })
              .catch(error => {
                console.log('Lỗi lỗi====>', error);
              });
          } else {
            Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
              cancelable: true,
            });
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }

  onPressBack() {
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  openAttachments() {
    var obj = {
      ModuleId: '12',
      RecordGuid: this.DataView.TrainingRequestGuid,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemTrainingRequestsComponent);
