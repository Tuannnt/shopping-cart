import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, View, TouchableOpacity, ScrollView } from "react-native";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import API_TrainingRequests from "../../../network/HR/API_TrainingRequests";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';

class ListTrainingRequestsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 25,
                Search: "",
                QueryOrderBy: "TrainingRequestId desc",
                StartDate: "2020-02-01T02:13:13.779Z",
                EndDate: "2020-02-29T02:13:13.779Z",
                TypeOfRequest: "",
                Status: "W"
            },
            openSearch: false
        };
        this.listtabbarBotom = [

            {
                Title: "Chờ duyệt",
                Icon: "profile",
                Type: "antdesign",
                Value: "profile",
                Checkbox: true
            },
            {
                Title: "Đã duyệt",
                Icon: "checkcircleo",
                Type: "antdesign",
                Value: "checkcircleo",
                Checkbox: false
            },
            {
                Title: "Trả lại",
                Icon: "export2",
                Type: "antdesign",
                Value: "export2",
                Checkbox: false
            }
        ]
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_TrainingRequests.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        this.GetAll();
        API_TrainingRequests.Count({ Status: 'W' })
            .then(res => {
                this.listtabbarBotom[1].Badge = res.data.data;
            })
            .catch(error => {
                console.log(error);
            });
        API_TrainingRequests.Count({ Status: 'M' })
            .then(res => {
                this.listtabbarBotom[3].Badge = res.data.data;
            })
            .catch(error => {
                console.log(error);
            });
    }

    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                TypeOfRequest: this.state.staticParam.TypeOfRequest
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Yêu cầu đào tạo'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'HR'}
                />
                <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, flex: 1, borderWidth: 0.5, flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => this.OnChangeTypeOfRequest('')} style={styles.TabarPending}>
                        <Text style={this.state.staticParam.TypeOfRequest == '' ? { color: AppColors.Maincolor } : ''}>Tất cả</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.OnChangeTypeOfRequest('P')} style={[{ flex: 1, width: 130, alignItems: 'center', justifyContent: 'center', borderColor: '#C0C0C0', borderWidth: 0.5 }]}>
                        <Text style={this.state.staticParam.TypeOfRequest == 'P' ? { color: AppColors.Maincolor } : ''}>Phát sinh</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.OnChangeTypeOfRequest('K')} style={styles.TabarApprove}>
                        <Text style={this.state.staticParam.TypeOfRequest == 'K' ? { color: AppColors.Maincolor } : ''}>Kế hoạch năm</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.openSearch == true ?
                        <FormSearch
                            CallbackSearch={(callback) => this.updateSearch(callback)}
                        />
                        : null
                }
                <View style={{ flex: 14 }}>
                    <FlatList
                        style={{ height: '100%' }}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.TrainingRequestGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family }}>
                                <ListItem
                                    title={item.TrainingRequestId}
                                    subtitle={() => {
                                        return (
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ width: '70%' }}>
                                                    <Text style={AppStyles.Textdefault}>Người yêu cầu: {item.EmployeeName}</Text>
                                                    <Text style={AppStyles.Textdefault}>Loại yêu cầu: {item.TypeOfRequest == 'K' ? 'Kế hoạch năm' : item.TypeOfRequest == 'P' ? 'Phát sinh' : ''}</Text>

                                                </View>
                                                <View style={{ width: '30%' }}>
                                                    <Text style={[AppStyles.Textdefault, item.Status == 'Y' ? { color: AppColors.AcceptColor } : { color: AppColors.PendingColor }]}>{item.StatusWF}</Text>
                                                    <Text
                                                        style={AppStyles.Textdefault}>{this.customDate(item.CreatedDate)}</Text>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    titleStyle={AppStyles.Titledefault}
                                    onPress={() => this.openView(item.TrainingRequestGuid)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }
                        }
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                    <View style={AppStyles.StyleTabvarBottom}>
                        <TabBarBottomCustom
                            ListData={this.listtabbarBotom}
                            onCallbackValueBottom={(callback) => this.onClick(callback)}
                        />
                    </View>
                </View>

            </View >
        );
    }

    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                TypeOfRequest: this.state.staticParam.TypeOfRequest,
                Status: this.state.staticParam.Status
            }
        });
        this.GetAll();
    }
    onClick(data) {
        if (data == 'checkcircleo') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 25;
            this.state.staticParam.Search = '';
            this.state.staticParam.Status = "Y";
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    TypeOfRequest: this.state.staticParam.TypeOfRequest,
                    Status: this.state.staticParam.Status
                }
            });
            this.GetAll();
        }
        else if (data == 'profile') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 25;
            this.state.staticParam.Search = '';
            this.state.staticParam.Status = "W";
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    TypeOfRequest: this.state.staticParam.TypeOfRequest,
                    Status: this.state.staticParam.Status
                }
            });
            this.GetAll();
        }
        else if (data == 'export2') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 25;
            this.state.staticParam.Search = '';
            this.state.staticParam.Status = "M";
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    TypeOfRequest: this.state.staticParam.TypeOfRequest,
                    Status: this.state.staticParam.Status
                }
            });
            this.GetAll();
        }
        else if (data === 'home') {
            Actions.app();
        }
    }
    //Load lại
    onRefresh = () => {
        this.setState({ refreshing: true })
        this.GetAll();
    }
    //Lọc loại đề nghị
    OnChangeTypeOfRequest(data) {
        if (data == '') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 25;
            this.state.staticParam.Search = '';
            this.state.staticParam.TypeOfRequest = '';
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    TypeOfRequest: this.state.staticParam.TypeOfRequest,
                    Status: this.state.staticParam.Status
                },
            });
            this.GetAll();
        }
        else if (data == 'P') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 15;
            this.state.staticParam.Search = '';
            this.state.staticParam.TypeOfRequest = 'P';
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    TypeOfRequest: this.state.staticParam.TypeOfRequest,
                    Status: this.state.staticParam.Status
                }
            });
            this.GetAll();
        }
        else if (data == 'K') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 15;
            this.state.staticParam.Search = '';
            this.state.staticParam.TypeOfRequest = 'K';
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    TypeOfRequest: this.state.staticParam.TypeOfRequest,
                    Status: this.state.staticParam.Status
                }
            });
            this.GetAll();
        }
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }

    openView(id) {
        Actions.hritemTrainingRequests({ RecordGuid: id });
    }
}

const styles = StyleSheet.create({
    TabarPending: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        width: 130
    },
    TabarApprove: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        width: 130
    }
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListTrainingRequestsComponent);
