import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {
  Header,
  Divider,
  Icon,
  ListItem,
  Badge,
  SearchBar,
} from 'react-native-elements';
import {API_HR, API} from '@network';
import TabBar from '../../component/TabBar';
import Fonts from '../../../theme/fonts';
import {Picker} from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import LoadingComponent from '../../component/LoadingComponent';

//custom giao diện
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '100',
    fontSize: 30,
    color: '#f6b801',
  },
  searchcontainer: {
    backgroundColor: 'white',
    height: 50,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0, //no effect
    shadowColor: 'white', //no effect
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 0,
    paddingBottom: 5,
    paddingLeft: 0,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
});

class RecruitmentRequestsComponent extends Component {
  constructor(props) {
    super(props);
    //khai báo biến
    this.state = {
      refreshing: false,
      Data: null,
      ListAll: [],
      ListCountNoti: [],
      ChoDuyet: '',
      TraLai: '',
      loading: true,
    };
    // dữ liệu trueyenf vao APi
    this._search = {
      //OrganizationGuid : '',
      StartTime: '2019-11-24',
      EndTime: '2020-12-24',
      Status: 'W',
      NumberPage: 0,
      txtSearch: '',
      Length: 14,
      Query: 'EmployeeName DESC',
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: false,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
      {
        Title: 'Trả lại',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'export2',
        Checkbox: false,
      },
    ];
  }
  //khi vào trang chạy đầu tiên
  componentDidMount(): void {
    this.nextPage();
  }
  // khi trả lại trang,thì load vào danh sách
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'back') {
      this.nextPage('reload');
    }
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.txtSearch = search;
    this._search.NumberPage = 0;
    this.nextPage();
  };
  //List danh sách phân trang
  nextPage(type) {
    this._search.NumberPage++;
    if (type === 'reload') {
      this._search.NumberPage = 1;
    }
    API_HR.getAllRecruitmentRequests(this._search)
      .then(res => {
        let _data = JSON.parse(res.data.data).data;
        if (type === 'reload') {
          this.state.ListAll = _data;
        } else {
          this.state.ListAll = this.state.ListAll.concat(_data);
        }
        this.setState({
          loading: false,
          ListAll: this.state.ListAll,
          refreshing: false,
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log(error.data.data);
        console.log(error);
      });
  }
  // khi kéo danh sách,thì phân trang
  loadMoreData() {
    this.nextPage();
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'profile':
        this.onChoDuyet();
        break;
      case 'checkcircleo':
        this.onDaDuyet();
        break;
      case 'export2':
        this.onKhongDuyet();
        break;
      default:
        break;
    }
  }
  // view hiển thị
  CustomeListAll = item => (
    <View style={{flex: 30}}>
      <FlatList
        data={item}
        ListEmptyComponent={this.ListEmpty}
        style={{height: '100%', marginBottom: 20}}
        renderItem={({item, index}) => (
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              subtitle={() => {
                return (
                  <View style={{flexDirection: 'row', marginTop: -25}}>
                    <View style={{flex: 2, flexDirection: 'row'}}>
                      <View>
                        <Text style={[AppStyles.Titledefault]}>
                          {item.RecruitmentId}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          Tiêu đề: {item.Title}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          Ngày tạo KH: {item.CreatedDateString}
                        </Text>
                      </View>
                    </View>
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          styles.subtitleStyle,
                          {textAlign: 'right', justifyContent: 'center'},
                        ]}>
                        {item.DateString}
                      </Text>
                      {item.Status == 'Y' ? (
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {
                              textAlign: 'right',
                              justifyContent: 'center',
                              color: AppColors.AcceptColor,
                            },
                          ]}>
                          {item.StatusWF}
                        </Text>
                      ) : (
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {
                              textAlign: 'right',
                              justifyContent: 'center',
                              color: AppColors.PendingColor,
                            },
                          ]}>
                          {item.StatusWF}
                        </Text>
                      )}
                    </View>
                  </View>
                );
              }}
              bottomDivider
              //chevron
              onPress={() => this.onViewItem(item.RecruitmentGuid)}
            />
          </View>
        )}
        onEndReached={() => this.loadMoreData()}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['black']}
          />
        }
      />
      {/* hiển thị nút tuỳ chọn */}
      <View style={AppStyles.StyleTabvarBottom}>
        <TabBarBottomCustom
          ListData={this.listtabbarBotom}
          onCallbackValueBottom={callback =>
            this.onCallbackValueBottom(callback)
          }
        />
      </View>
    </View>
  );
  render() {
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Yêu cầu tuyển dụng'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            BackModuleByCode={'HR'}
            // FormQR={true}
            // CallbackFormQR={callback => this.setState ({EvenFormQR: callback})}
            //addForm={'addregistereats'}
          />
          <Divider />
          <View>
            {this.state.EvenFromSearch == true ? (
              <SearchBar
                placeholder="Tìm kiếm..."
                lightTheme
                round
                inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                containerStyle={AppStyles.FormSearchBar}
                onChangeText={search => this.updateSearch(search)}
                value={this._search.txtSearch}
              />
            ) : null}
          </View>
          <View style={{flex: 10}}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  ListEmpty = () => {
    if (this.state.ListAll.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  openSearch() {
    if (this.state.openSearch === false) {
      this.setState({openSearch: true});
    } else {
      this.setState({openSearch: false});
    }
  }
  onPressHome() {
    Actions.home();
  }
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'W';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'profile',
    });
    this.nextPage();
  };
  // sự kiện chờ duyệt
  onDaDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'Y';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'checkcircleo',
    });
    this.nextPage();
  };
  // sự kiện ko duyệt
  onKhongDuyet = () => {
    this.state.ListAll = [];
    this._search.NumberPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'R';
    this.setState({
      ListAll: this.state.ListAll,
      NumberPage: this._search.NumberPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
      selectedTab: 'export2',
    });
    this.nextPage();
  };
  //view item,click sang view khác
  onViewItem(id) {
    Actions.GetRecruitmentRequests({viewId: id});
  }

  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }
  renderLeftMenu = () => {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'chevron-thin-left'}
          type="entypo"
          size={25}
          onPress={() => Actions.home()}
        />
      </View>
    );
  };
  renderRightMenu = () => {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'plus'}
          type="antdesign"
          size={30}
          onPress={() => Actions.AddResignationForms()}
        />
      </View>
    );
  };
  // xóa tìm kiếm
  onclickreturnSearch() {
    this._search.Search = '';
    this.updateSearch('');
  }
  //
  onValueChange(item) {
    console.log('============>>' + item);
    switch (item) {
      case 'Status': {
        break;
      }
      case 'StartDate': {
        break;
      }
      case 'EndDate': {
        break;
      }
      default: {
      }
    }
  }

  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  //Load lại
  _onRefresh = () => {
    this.nextPage();
    this.setState({refreshing: true});
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(RecruitmentRequestsComponent);
