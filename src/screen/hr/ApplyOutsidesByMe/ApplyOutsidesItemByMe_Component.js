import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  FlatList,
  TouchableOpacity,
  Dimensions,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_ApplyOutsides} from '../../../network';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import {Container} from 'native-base';
import _ from 'lodash';
import moment from 'moment';
import {FuncCommon} from '../../../utils';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import listCombobox from './listCombobox';
import controller from './controller';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import LoadingComponent from '../../component/LoadingComponent';

var width = Dimensions.get('window').width; //full width
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const headerTable = listCombobox.headerTable;
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
const options = [
  {
    component: <Text style={{color: 'crimson', fontSize: 20}}>Đóng</Text>,
  },
  {
    component: <Text style={{color: 'blue', fontSize: 20}}>Duyệt phiếu</Text>,
    height: 80,
  },
  {
    component: <Text style={{color: 'blue', fontSize: 20}}>Trả lại</Text>,
    height: 80,
  },
];

class ApplyOutsidesItemByMe_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      DataDetail: [],
      Title: '',
      checkInLogin: '',
      rows: [],
      ListEmployee: [],
      loading: true,
    };
    this.add = React.createRef();
  }
  componentDidMount(): void {
    Promise.all([this.getItem(), this.getAllEmployeeByDepart()]);
  }
  getAllEmployeeByDepart = () => {
    controller.getAllEmployeeByDepart(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.value,
        label: item.text,
        JobTitleName: item.JobTitleName,
        JobTitleId: item.JobTitleId,
      }));
      data.unshift({
        value: null,
        label: 'Chọn nhân viên',
      });
      this.setState({
        ListEmployee: data,
      });
    });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }
  onUpdate = () => {};
  callbackOpenUpdate = () => {
    Actions.applyOutsidesAdd({
      dataEdit: this.state.Data,
      rows: this.state.rows,
    });
  };
  openAttachments() {
    var obj = {
      ModuleId: '16',
      RecordGuid: this.props.viewId,
    };

    Actions.attachmentComponent(obj);
  }
  helperNumber = num => {
    if (!num) {
      return '';
    }
    return num + '';
  };
  getItem() {
    let id = {Id: this.props.viewId};
    API_ApplyOutsides.ApplyOutsides_Items(id)
      .then(res => {
        if (!JSON.parse(res.data.data).model) {
          return;
        }
        let rows = [];
        let details = JSON.parse(res.data.data).detail;
        if (details) {
          rows = details.map(detail => ({
            ...detail,
            AllowTravel: this.helperNumber(detail.AllowTravel),
            AllowLunch: this.helperNumber(detail.AllowLunch),
            AllowHouse: this.helperNumber(detail.AllowHouse),
            AllowWork: this.helperNumber(detail.AllowWork),
            OtherCost: this.helperNumber(detail.OtherCost),
            Quantity: this.helperNumber(detail.Quantity),
          }));
        }
        this.setState({Data: JSON.parse(res.data.data).model});
        this.setState({rows});
        let checkin = {
          ApplyOutsideGuid: this.props.viewId,
          WorkFlowGuid: JSON.parse(res.data.data).model.WorkFlowGuid,
        };
        API_ApplyOutsides.ApplyOutsides_checkin(checkin)
          .then(res => {
            this.setState({checkInLogin: res.data.data, loading: false});
            console.log('===checkin====' + res.data.data);
            console.log('--------' + this.state.Data.Permisstion);
          })
          .catch(error => {
            console.log(error.data.data);
          });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
      });
  }
  showActionSheet = () => this.actionSheet.show();
  getActionSheetRef = ref => (this.actionSheet = ref);
  handleChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  onChangeText = (key, value) => {
    this.setState({[key]: value});
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  helperPosition = id => {
    if (!id) {
      return '';
    }
    let employee = this.state.ListEmployee.find(emp => emp.value === id);
    if (!employee) {
      return '';
    }
    return employee.JobTitleName;
  };
  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({rows: res, typeTime: null, currentIndex: null});
    this.hideDatePicker();
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  CustomViewDetailRoutings = () => {
    const item = this.state.Data;
    return (
      <ScrollView>
        <View>
          <View style={{padding: 15}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                {/*Mã phiếu*/}
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Labeldefault}>Mã:</Text>
                  </View>
                  <View style={{flex: 2}}>
                    <Text style={[AppStyles.Textdefault, {fontWeight: 'bold'}]}>
                      {item.WorkOutsideId}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            {/*Tiêu đề phiêu*/}

            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Tiêu đề phiếu :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.Title}</Text>
              </View>
            </View>
            {/*Người tạo phiếu*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text>
              </View>
            </View>
            {/*Phòng ban*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {item.DepartmentName}
                </Text>
              </View>
            </View>
            {/*Chức vụ*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Chức vụ :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.JobTitleName}</Text>
              </View>
            </View>
            {/*Ngày tạo phiếu*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày tạo :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.customDate(item.Date)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>TG bắt đầu :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {FuncCommon.ConDate(item.StartTime, 0) || ''}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>TG kết thúc :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>
                  {FuncCommon.ConDate(item.EndTime, 0) || ''}
                </Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
              </View>
              <View style={{flex: 2}}>
                {item.Status == 'Y ' ? (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.AcceptColor},
                    ]}>
                    {item.StatusWF || ''}
                  </Text>
                ) : (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.PendingColor},
                    ]}>
                    {item.StatusWF || ''}
                  </Text>
                )}
              </View>
            </View>
            {/*nội dung phiếu*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={[AppStyles.Textdefault]}>{item.Description}</Text>
              </View>
            </View>
          </View>
          {/*hiển thị nội dung chi tiết*/}
          <Text style={{fontSize: 16, fontWeight: 'bold', padding: 10}}>
            Chi tiết phiếu
          </Text>
          <Divider />
          <View style={{flex: 5}}>
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  {headerTable.map(item => {
                    if (item.hideInDetail) {
                      return null;
                    }
                    return (
                      <TouchableOpacity
                        key={item.title}
                        style={[AppStyles.table_th, {width: item.width}]}>
                        <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>
                <View
                // style={{
                //   maxHeight: Dimensions.get('window').height / 7,
                // }}
                >
                  {this.state.rows.length > 0 ? (
                    <FlatList
                      data={this.state.rows}
                      renderItem={({item, index}) => {
                        return this.itemFlatList(item, index);
                      }}
                      keyExtractor={(rs, index) => index.toString()}
                    />
                  ) : (
                    <TouchableOpacity style={[AppStyles.table_foot]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                        Không có dữ liệu!
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </ScrollView>
    );
  };
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 300}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'EmployeeGuid');
            }}
            items={this.state.ListEmployee}
            style={[stylePicker]}
            value={row.EmployeeGuid}
            placeholder={{}}
            disabled
          />
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.EmployeeGuid && this.helperPosition(row.EmployeeGuid)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.AssetName}
            autoCapitalize="none"
            onChangeText={AssetName => {
              this.handleChangeRows(AssetName, index, 'AssetName');
            }}
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {}}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.ActualStartTime &&
              moment(row.ActualStartTime).format('DD/MM/YYYY')) ||
              'Chọn thời gian'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {}}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.ActualEndTime &&
              moment(row.ActualEndTime).format('DD/MM/YYYY')) ||
              'Chọn thời gian'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 80}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.ActualStartTime &&
              row.ActualEndTime &&
              Math.round(
                FuncCommon.getDiffDay(row.ActualStartTime, row.ActualEndTime) *
                  100,
              ) / 100}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={row.AllowTravel}
            autoCapitalize="none"
            onChangeText={AllowTravel => {
              this.handleChangeRows(AllowTravel, index, 'AllowTravel');
            }}
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={row.AllowLunch}
            autoCapitalize="none"
            onChangeText={AllowLunch => {
              this.handleChangeRows(AllowLunch, index, 'AllowLunch');
            }}
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={row.AllowHouse}
            autoCapitalize="none"
            onChangeText={AllowHouse => {
              this.handleChangeRows(AllowHouse, index, 'AllowHouse');
            }}
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={row.AllowWork}
            autoCapitalize="none"
            onChangeText={AllowWork => {
              this.handleChangeRows(AllowWork, index, 'AllowWork');
            }}
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={row.OtherCost}
            autoCapitalize="none"
            onChangeText={OtherCost => {
              this.handleChangeRows(OtherCost, index, 'OtherCost');
            }}
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={
              Number(row.AllowTravel || 0) +
              Number(row.AllowLunch || 0) +
              Number(row.AllowHouse || 0) +
              Number(row.AllowWork || 0) +
              Number(row.OtherCost || 0) +
              ''
            }
            autoCapitalize="none"
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={row.Quantity}
            autoCapitalize="none"
            onChangeText={Quantity => {
              this.handleChangeRows(Quantity, index, 'Quantity');
            }}
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Unit}
            autoCapitalize="none"
            onChangeText={Unit => {
              this.handleChangeRows(Unit, index, 'Unit');
            }}
            editable={false}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={Note => {
              this.handleChangeRows(Note, index, 'Note');
            }}
            editable={false}
          />
        </TouchableOpacity>
      </View>
    );
  };
  CustomView = item => (
    <Container>
      <TouchableWithoutFeedback
        style={{flex: 30}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          {
            <TabBar_Title
              title={'Chi tiết phiếu báo công tác'}
              callBack={() => Actions.pop()}
            />
          }
          <Divider />
          {/*hiển thị nội dung chính*/}
          <View
            style={{
              flex: 10,
            }}>
            {this.CustomViewDetailRoutings()}
          </View>

          {/*nút xử lý*/}
          <View style={{flex: 1}}>
            {this.state.checkInLogin !== '' ? (
              <TabBarBottom
                isComplete={item.Status === 'Y' ? true : false}
                isDraff={item.StatusWF === 'Nháp' ? true : false}
                haveEditButton={item.PermissEdit === 1 ? true : false}
                // callbackOpenUpdate={this.callbackOpenUpdate}
                onDelete={() => this.Delete()}
                backListByKey="ApplyOutsides"
                Title={item.Title}
                Permisstion={item.Permisstion}
                checkInLogin={this.state.checkInLogin}
                onAttachment={() => this.openAttachments()}
                onSubmitWF={(callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
                }
                keyCommentWF={{
                  ModuleId: 35,
                  RecordGuid: this.props.viewId,
                  Title: item.Title,
                }}
              />
            ) : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Container>
  );
  render() {
    return this.state.loading ? (
      <LoadingComponent />
    ) : this.state.Data ? (
      this.CustomView(this.state.Data)
    ) : null;
  }
  onView(id) {
    Actions.OpenApplyOutsideDetails({Id: id});
    console.log('===================chuyen Id:' + id);
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    this.setState({loading: true});
    console.log('callback==' + callback + CommentWF);
    if (callback == 'D') {
      let obj = {
        ApplyOutsideGuid: this.props.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.props.Loginname,
        Comment: CommentWF,
      };
      API_ApplyOutsides.approveApplyOutsides(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      let obj = {
        ApplyOutsideGuid: this.props.viewId,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.props.Loginname,
        Comment: CommentWF,
      };
      API_ApplyOutsides.noapproveApplyOutsides(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
    this.setState({
      loading: false,
    });
  }
  Delete = () => {
    let id = {Id: this.props.viewId};
    this.setState({loading: true});
    API_ApplyOutsides.ApplyOutsides_Delete(id.Id)
      .then(response => {
        console.log('==============ketquaXoa: ' + response.data.Error);
        if (response.data.Error == false) {
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        console.log(error.data.data);
      });
    this.setState({loading: false});
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng ngày và giờ
  customDatetime(strDateTime) {
    if (strDateTime != null) {
      var strSplitDateTime = String(strDateTime).split(' ');
      var datetime = new Date(strSplitDateTime[0]);
      // alert(date);
      var HH = datetime.getHours();
      var MM = datetime.getMinutes();
      var dd = datetime.getDate();
      var mm = datetime.getMonth() + 1; //January is 0!
      var yyyy = datetime.getFullYear();
      if (HH < 10) {
        HH = '0' + HH;
      }
      if (MM < 10) {
        MM = '0' + MM;
      }
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      datetime = dd + '/' + mm + '/' + yyyy + ' ' + HH + ':' + MM;
      return datetime.toString();
    } else {
      return '';
    }
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    setTimeout(() => {
      Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
    }, 100);
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ApplyOutsidesItemByMe_Component);
