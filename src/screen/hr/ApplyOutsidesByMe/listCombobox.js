import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Nhân viên', width: 300},
    {title: 'Chức danh', width: 150},
    {title: 'Tài sản', width: 150},
    {title: 'Thời gian bắt đầu', width: 120},
    {title: 'Thời gian kết thúc', width: 120},
    {title: 'Tổng thời gian', width: 80},
    {title: 'Phụ cấp đi lại', width: 100},
    {title: 'Phụ cấp ăn', width: 100},
    {title: 'Phụ cấp ở', width: 100},
    {title: 'Phụ cấp công tác', width: 100},
    {title: 'Chi phí khác', width: 100},
    {title: 'Tổng chi phí', width: 150},
    {title: 'Số lượng tài sản', width: 100},
    {title: 'Đơn vị tính tài sản', width: 100},
    {title: 'Ghi chú', width: 150},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  //#endregion
};
