import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  Dimensions,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, Icon, ListItem, SearchBar} from 'react-native-elements';
import {API_ApplyOutsides} from '../../../network';
import TabBar from '../../component/TabBar';
import Fonts from '../../../theme/fonts';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import {FuncCommon} from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
const SCREEN_WIDTH = Dimensions.get('window').width;
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 5,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
});

class ApplyOutsidesByMeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      Data: null,
      ListAll: [],
      EvenFromSearch: false,
      ValueSearchDate: '1', // năm nay
      loading: true,
    };
    this._search = {
      StartDate: new Date(),
      EndDate: new Date(),
      Status: 'W',
      NumberPage: 0,
      txtSearch: '',
      Length: 14,
      QueryOrderBy: 'Date DESC',
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'CD',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'DD',
        Checkbox: false,
      },
      {
        Title: 'Trả lại',
        Icon: 'export2',
        Type: 'antdesign',
        Value: 'TL',
        Checkbox: false,
      },
    ];
  }
  componentDidMount(): void {
    this.nextPage();
  }
  componentWillReceiveProps(nextProps) {
    this.nextPage();
  }
  setStartDate = date => {
    this._search.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this._search.EndDate = date;
    this.updateSearch('');
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({EvenFromSearch: true});
    } else {
      this.setState({EvenFromSearch: false});
    }
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.txtSearch = search;
    this._search.NumberPage = 0;
    this.nextPage();
  };

  //List danh sách phân trang
  nextPage() {
    this._search.NumberPage++;
    API_ApplyOutsides.ApplyOutsidesByMe_ListAll(this._search)
      .then(res => {
        console.log(res);
        this.state.ListAll = this.state.ListAll.concat(
          JSON.parse(res.data.data).data,
        );
        this.setState({
          ListAll: this.state.ListAll,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log(error);
      });
  }

  loadMoreData() {
    this.nextPage();
  }
  ListEmpty = () => {
    if (this.state.ListAll.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  CustomeListAll = item => (
    <FlatList
      data={item}
      style={{flex: 1}}
      renderItem={({item, index}) => (
        <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
          <ListItem
            title={() => {
              return (
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 3, flexDirection: 'row'}}>
                    <View style={{flex: 5}}>
                      <Text style={[AppStyles.Titledefault]}>
                        {item.WorkOutsideId}
                      </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        Tiêu đề: {item.Title}
                      </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        Người tạo: {item.EmployeeName}
                      </Text>
                    </View>
                  </View>
                  <View style={{flex: 3, alignItems: 'flex-end'}}>
                    <Text style={[AppStyles.Textdefault]}>
                      {' '}
                      {item.DepartmentName}
                    </Text>
                    <Text style={[AppStyles.Textdefault]}>
                      {' '}
                      {this.customDate(item.Date)}
                    </Text>
                    {item.Status == 'Y' ? (
                      <Text
                        style={[
                          {
                            justifyContent: 'center',
                            color: AppColors.AcceptColor,
                          },
                          AppStyles.Textdefault,
                        ]}>
                        {item.StatusWF}
                      </Text>
                    ) : (
                      <Text
                        style={[
                          {
                            justifyContent: 'center',
                            color: AppColors.PendingColor,
                          },
                          AppStyles.Textdefault,
                        ]}>
                        {item.StatusWF}
                      </Text>
                    )}
                  </View>
                </View>
              );
            }}
            bottomDivider
            //chevron
            onPress={() =>
              this.onViewItem(item.ApplyOutsideGuid, item.Permisstion)
            }
          />
        </View>
      )}
      onEndReached={() => this.loadMoreData()}
      ListEmptyComponent={this.ListEmpty}
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['red', 'green', 'blue']}
        />
      }
    />
  );
  render() {
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Phiếu đăng ký đi công tác'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            // addForm={true}
            // CallbackFormAdd = {() => Actions.applyOutsidesAdd()}
            BackModuleByCode={'MyProfile'}
          />
          <Divider />
          <View>
            {this.state.EvenFromSearch == true ? (
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventStartDate: true})}>
                      <Text>
                        {FuncCommon.ConDate(this._search.StartDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventEndDate: true})}>
                      <Text>{FuncCommon.ConDate(this._search.EndDate, 0)}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'column', padding: 5}}>
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        {textAlign: 'center'},
                        styles.timeHeader,
                      ]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={text => this.updateSearch(text)}
                  value={this._search.txtSearch}
                />
              </View>
            ) : null}
          </View>
          <View style={{flex: 1}}>
            {this.state.ListAll && this.CustomeListAll(this.state.ListAll)}
          </View>
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.onCallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch('');
                  this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch('');
                  this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }

  //view item
  onViewItem(id, permisstion) {
    Actions.ApplyOutsidesItemByMe({viewId: id, Permisstion: permisstion});
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'CD':
        this._search.Search = '';
        (this._search.Status = 'W'), this.updateSearch('');
        break;
      case 'DD':
        this._search.Search = '';
        (this._search.Status = 'Y'), this.updateSearch('');
        break;
      case 'TL':
        this._search.Search = '';
        (this._search.Status = 'R'), this.updateSearch('');
        break;
      default:
        break;
    }
  }
  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }

  onclickreturnSearch() {
    this._search.Search = '';
    this.updateSearch('');
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  onValueChange(item) {
    console.log('============>>' + item);
    switch (item) {
      case 'Status': {
        break;
      }
      case 'StartDate': {
        break;
      }
      case 'EndDate': {
        break;
      }
      default: {
      }
    }
  }

  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  //Load lại
  _onRefresh = () => {
    this.setState({refreshing: true});
    this.updateSearch('');
  };
}

//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ApplyOutsidesByMeComponent);
