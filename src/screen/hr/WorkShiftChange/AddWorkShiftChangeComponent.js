import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  FlatList,
  Keyboard,
  StyleSheet,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Container} from 'native-base';
import {FuncCommon} from '../../../utils';
import DatePicker from 'react-native-datepicker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import _ from 'lodash';
import {AppStyles, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RequiredText from '../../component/RequiredText';
import RNPickerSelect from 'react-native-picker-select';
import LoadingComponent from '../../component/LoadingComponent';
import {API_HR, API_WorkShiftChange} from '@network';
import controller from './controller';
import listCombobox from './listCombobox';
import moment from 'moment';

const headerTable = listCombobox.headerTable;
class AddWorkShiftChangeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [{}],
      Type: 'A',
      ConfirmBy: '',
      WorkDate: new Date(),
      Quantity: 0,
      search: '',
      loading: true,
      ListEmployee: [],
      workShiftData: [],
    };
    this.setDate = this.setDate.bind(this);
  }
  componentDidMount(): void {
    Promise.all([this.getAllEmployeeByDepart(), this.getAllWorkShift()]);
    const {itemData, rowData = []} = this.props;
    if (itemData) {
      this.setState({
        WorkDate: FuncCommon.ConDate(new Date(itemData.WorkDate), 0),
        Title: itemData.Title,
        FullName: itemData.EmployeeName,
        rows: [...rowData],
        isEdit: false,
        WorkShiftChangeId: itemData.WorkShiftChangeId,
      });
    } else {
      this.getNumberAuto();
      this.setState({
        WorkDate: FuncCommon.ConDate(new Date(), 0),
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
      });
    }
  }
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_DC',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      this.setState({isEdit: data.IsEdit, WorkShiftChangeId: data.Value});
    });
  };
  onChangeType(value) {
    this.setState({
      Type: value,
    });
  }
  setNote(Title) {
    this.setState({Title});
  }
  setQuantity(Quantity) {
    this.setState({Quantity});
  }
  onClickBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  getAllEmployeeByDepart = () => {
    controller.getAllEmployeeByDepart(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.value,
        label: item.text,
      }));
      data.unshift({
        value: null,
        label: 'Chọn nhân viên',
      });
      this.setState({ListEmployee: data, loading: false});
    });
  };
  getAllWorkShift = () => {
    controller.getAllWorkShift(rs => {
      let data = JSON.parse(rs).map(item => ({
        ...item,
        value: item.value,
        label: item.text,
      }));
      data.unshift({
        value: null,
        label: 'Chọn ca',
      });
      this.setState({workShiftData: data});
    });
  };
  Submit() {
    let lstDetails = [...this.state.rows];
    if (!this.state.WorkShiftChangeId) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa điền mã phiếu',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    let isNotValidated = false;
    for (let i = 0; i < lstDetails.length; i++) {
      let lst = lstDetails[i];
      if (!lst.EmployeeGuid) {
        Alert.alert(
          'Thông báo',
          `Bạn chưa chọn nhân viên ở dòng ${i + 1}`,
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        isNotValidated = true;
        return;
      }
      if (!lst.DateChange) {
        Alert.alert(
          'Thông báo',
          `Bạn chưa chọn ngày đổi ca ở dòng ${i + 1}`,
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        isNotValidated = true;
        return;
      }
      if (!lst.ShiftIDOld) {
        Alert.alert(
          'Thông báo',
          `Bạn chưa chọn ca cũ ở dòng ${i + 1}`,
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        isNotValidated = true;
        return;
      }
      if (!lst.ShiftId) {
        Alert.alert(
          'Thông báo',
          `Bạn chưa chọn ca mới ở dòng ${i + 1}`,
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }
    let auto = {Value: this.state.WorkShiftChangeId, IsEdit: this.state.isEdit};
    if (this.props.itemData) {
      this.Update();
      return;
    }
    let _obj = {
      FileAttachments: [],
      Quantity: null,
      Type: null,
      Status: null,
      ConfirmBy: null,
      WorkDate: new Date(),
      Title: this.state.Title,
      WorkShiftChangeId: this.state.WorkShiftChangeId,
    };
    let _data = new FormData();
    _data.append('submit', JSON.stringify(_obj));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    _data.append('lstTitlefile', JSON.stringify(undefined));
    _data.append('DeleteAttach', JSON.stringify(undefined));
    _data.append('auto', JSON.stringify(auto));
    API_WorkShiftChange.Add(_data)
      .then(rs => {
        console.log(rs);
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Alert.alert(
            'Thông báo',
            'Có lỗi xảy ra',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Thông báo',
            'Thêm mới thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.onClickBack();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }
  setDate(Date) {
    this.setState({WorkDate: Date});
  }
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  Update = () => {
    let auto = {Value: this.state.WorkShiftChangeId, IsEdit: this.state.isEdit};
    let _obj = {
      ...this.props.itemData,
      FileAttachments: [],
      Quantity: null,
      Type: null,
      Status: null,
      ConfirmBy: null,
      WorkDate: FuncCommon.ConDate(this.state.WorkDate, 99),
      Title: this.state.Title,
      WorkShiftChangeId: this.state.WorkShiftChangeId,
    };
    let _data = new FormData();
    _data.append('edit', JSON.stringify(_obj));
    _data.append('lstDetails', JSON.stringify(this.state.rows));
    _data.append('DeleteDetail', JSON.stringify(undefined));
    _data.append('auto', JSON.stringify(auto));
    API_WorkShiftChange.Edit(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          console.log(_rs);
          Alert.alert(
            'Thông báo',
            'Có lỗi xảy ra',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Thông báo',
            'Chỉnh sửa thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.onClickBack();
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({
      rows: res,
      typeTime: null,
      currentIndex: null,
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
    });
  };
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 300}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'EmployeeGuid');
            }}
            items={this.state.ListEmployee}
            value={row.EmployeeGuid}
            placeholder={{}}
          />
        </View>

        <TouchableOpacity
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.handlePickDateOpen('DateChange', index);
          }}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.DateChange && moment(row.DateChange).format('DD/MM/YYYY')) ||
              'Chọn thời gian'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'ShiftIDOld');
            }}
            items={this.state.workShiftData}
            value={row.ShiftIDOld}
            placeholder={{}}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'ShiftId');
            }}
            items={this.state.workShiftData}
            value={row.ShiftId}
            placeholder={{}}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Description}
            autoCapitalize="none"
            onChangeText={Description => {
              this.handleChangeRows(Description + '', index, 'Description');
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td_custom, {width: 80}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    const {isEdit, WorkShiftChangeId} = this.state;
    const IS_NV = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'NV';
    if (this.state.loading) {
      return <LoadingComponent />;
    }
    return (
      <Container>
        <View style={{flex: 30}}>
          <View style={{flex: 40}}>
            {this.props.itemData ? (
              <TabBar_Title
                title={'Sửa đăng ký đổi ca'}
                callBack={() => Actions.pop()}
              />
            ) : (
              <TabBar_Title
                title={'Thêm đăng ký đổi ca'}
                callBack={() => Actions.pop()}
              />
            )}
            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Mã phiếu <RequiredText />{' '}
                    </Text>
                  </View>
                  {isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={WorkShiftChangeId}
                      autoCapitalize="none"
                      onChangeText={Title =>
                        this.onChangeState('WorkShiftChangeId', Title)
                      }
                    />
                  )}
                  {!isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                        {WorkShiftChangeId}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người tạo phiếu</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Tên phiếu</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.Title}
                    onChangeText={Title => this.setNote(Title)}
                  />
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày tạo phiếu <RequiredText />
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        disabled
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.WorkDate}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setDate(date)}
                      />
                    </View>
                  </View>
                </View>

                {/* Table */}
                <View style={{marginTop: 5}}>
                  <View style={{width: 150, marginLeft: 10}}>
                    <Button
                      title="Thêm dòng"
                      type="outline"
                      size={15}
                      onPress={() => this.addRows()}
                      buttonStyle={{padding: 5, marginBottom: 5}}
                      titleStyle={{marginLeft: 5}}
                      icon={
                        <Icon
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>

                  <ScrollView horizontal={true}>
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        {headerTable.map(item => (
                          <TouchableOpacity
                            key={item.title}
                            style={[AppStyles.table_th, {width: item.width}]}>
                            <Text style={AppStyles.Labeldefault}>
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        ))}
                      </View>
                      <View>
                        {console.log(this.state.rows)}
                        {console.log(this.state.workShiftData)}
                        {this.state.rows.length > 0 ? (
                          <FlatList
                            data={this.state.rows}
                            renderItem={({item, index}) => {
                              return this.itemFlatList(item, index);
                            }}
                            keyExtractor={(rs, index) => index.toString()}
                          />
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'left'},
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View>
              </View>
            </ScrollView>
            {this.state.modalPickerTimeVisibleDate && (
              <DateTimePickerModal
                locale="vi-VN"
                cancelTextIOS="Huỷ"
                confirmTextIOS="Chọn"
                headerTextIOS="Chọn thời gian"
                isVisible={this.state.modalPickerTimeVisibleDate}
                mode="date"
                onConfirm={this.handleConfirmTime}
                onCancel={this.hideDatePicker}
              />
            )}
          </View>
        </View>

        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
      </Container>
    );
  }
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'arrowleft'}
          type="antdesign"
          size={30}
          onPress={() => this.onPressBack()}
        />
      </View>
    );
  }
  // Validate = () => {
  //      if (_obj.Quantity === null || _obj.Quantity === 0 || _obj.Quantity ===  undefined ) return null;
  //       return (
  //           //View to show when list is empty
  //           <View style={styles.MainContainer}>
  //               <Text style={{ textAlign: 'center' }}>nhập số lượng.</Text>
  //           </View>
  //       );
  //   };
  onPressBack() {
    Actions.Registereats();
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddWorkShiftChangeComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
