import {API_ApplyOverTimes, API_HR} from '@network';
import {ErrorHandler} from '@error';

export default {
  // List nhan vien
  getAllEmployeeByDepart(callback) {
    API_ApplyOverTimes.GetEmployeeByDepartment()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getAllWorkShift(callback) {
    API_ApplyOverTimes.getAllWorkShift()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getNumberAuto(value, callback) {
    API_ApplyOverTimes.GetNumberAuto(value)
      .then(res => {
        console.log(res + '=======> get Number auto');
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
};
