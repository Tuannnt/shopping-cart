import React, {Component} from 'react';
import {
  Keyboard,
  Text,
  View,
  ToastAndroid,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_WorkShiftChange} from '@network';
import {Actions} from 'react-native-router-flux';
import {Divider} from 'react-native-elements';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import moment from 'moment';
import listCombobox from './listCombobox';
import Toast from 'react-native-simple-toast';

const headerTable = listCombobox.headerTable;
class GetWorkShiftChangeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [],
      Type: 'A',
      Data: null,
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      Quantity: '',
      viewId: '',
      Type: '',
    };
  }

  componentDidMount = () => {
    Promise.all([this.getItem()]);
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }
  onClickBack() {
    Actions.pop();
  }
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_WorkShiftChange.Delete(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            RegisterEatGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  getItem() {
    let viewId = '';
    if (this.props.RecordGuid !== undefined) {
      viewId = this.props.RecordGuid;
    } else {
      viewId = this.props.viewId;
    }
    API_WorkShiftChange.GetItem(this.props.RecordGuid)
      .then(res => {
        let _data = JSON.parse(res.data.data).model;
        let rows = JSON.parse(res.data.data).detail;
        this.setState({
          Data: _data,
          rows,
          viewId,
          LoginName: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
        });
        let checkin = {
          RowGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).model.WorkFlowGuid,
        };
        API_WorkShiftChange.CheckLogin(checkin)
          .then(res => {
            this.setState({checkInLogin: res.data.data});
          })
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log(error);
      });
  }
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 300}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.EmployeeName}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {row.DateChange && moment(row.DateChange).format('DD/MM/YYYY')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {row.ShiftNameOld || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {row.ShiftName || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {row.Description || ''}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  CustomViewDetailRoutings = () => {
    return (
      <View style={{flex: 1}}>
        <View style={{padding: 15}}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.EmployeeName}
              </Text>
            </View>
          </View>
          {/*Phòng ban*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.DepartmentName}
              </Text>
            </View>
          </View>
          {/* tieu de */}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Tiêu đề:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.Title}
              </Text>
            </View>
          </View>
          {/*Ngày tạo phiếu*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Ngày tạo :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {moment(this.state.Data.CreatedDate).format('DD/MM/YYYY')}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
            </View>
            <View style={{flex: 3}}>
              {this.state.Data.Status == 'Y' ? (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {color: AppColors.AcceptColor},
                  ]}>
                  {this.state.Data.StatusWF}
                </Text>
              ) : (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {color: AppColors.PendingColor},
                  ]}>
                  {this.state.Data.StatusWF}
                </Text>
              )}
            </View>
          </View>
        </View>
        <Text style={{fontSize: 16, fontWeight: 'bold', padding: 10}}>
          Chi tiết phiếu
        </Text>
        <ScrollView horizontal={true}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              {headerTable.map(item => {
                if (item.hideInDetail) {
                  return null;
                }
                return (
                  <TouchableOpacity
                    key={item.title}
                    style={[AppStyles.table_th, {width: item.width}]}>
                    <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <FlatList
                  data={this.state.rows}
                  renderItem={({item, index}) => {
                    return this.itemFlatList(item, index);
                  }}
                  keyExtractor={(rs, index) => index.toString()}
                />
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };

  CustomView = item => (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TabBar_Title
        title={'Chi tiết đổi ca'}
        callBack={() => this.callBackList()}
      />
      <Divider />
      {/*hiển thị nội dung chính*/}
      <ScrollView>{this.CustomViewDetailRoutings()}</ScrollView>
      {/*nút xử lý*/}
      <View style={{maxHeight: 50}}>
        {this.state.checkInLogin !== '' ? (
          <TabBarBottom
            //key để quay trở lại danh sách
            onDelete={() => this.Delete()}
            isComplete={this.state.Data.Status === 'Y' ? true : false}
            isDraff={this.state.Data.StatusWF === 'Nháp' ? true : false}
            haveEditButton={this.state.Data.PermissEdit === 1 ? true : false}
            callbackOpenUpdate={this.callbackOpenUpdate}
            backListByKey="Registereats"
            keyCommentWF={{
              ModuleId: 41,
              RecordGuid: this.state.viewId,
              Title: 'Phiếu đổi ca',
              getByGuid: true,
              //LoginName:this.state.Data.LoginName
            }}
            // tiêu đề hiển thị trong popup xử lý phiếu
            Title="Phiếu đổi ca"
            //kiểm tra quyền xử lý
            Permisstion={this.state.Data.Permisstion}
            //kiểm tra bước đầu quy trình
            checkInLogin={this.state.checkInLogin}
            onSubmitWF={(callback, CommentWF) =>
              this.submitWorkFlow(callback, CommentWF)
            }
          />
        ) : null}
      </View>
    </View>
  );
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  callbackOpenUpdate = () => {
    Actions.AddWorkShiftChange({
      itemData: this.state.Data,
      rowData: this.state.rows,
    });
  };
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();

    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    if (callback == 'D') {
      let obj = {
        RowGuid: this.state.viewId,
        Comment: CommentWF,
      };
      API_WorkShiftChange.Approve(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      let obj = {
        RowGuid: this.state.viewId,
        Comment: CommentWF,
      };
      API_WorkShiftChange.NotApprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetWorkShiftChangeComponent);
