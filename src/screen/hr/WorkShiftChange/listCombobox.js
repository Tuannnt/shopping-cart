export default {
    headerTable: [
      {title: 'STT', width: 40},
      {title: 'Tên Nhân viên', width: 300},
      {title: 'Ngày đổi ca', width: 120},
      {title: 'Ca cũ', width: 200},
      {title: 'Ca mới', width: 200},
      {title: 'Mô tả', width: 200},
      {title: 'Hành động', width: 80, hideInDetail: true},
    ],
  Ration: [
    {
      value: 'A',
      label: 'Xuất ăn 12k',
    },
    {
      value: 'B',
      label: 'Xuất ăn 15k',
    },
    {
      value: 'C',
      label: 'Xuất ăn 28k',
    },
  ],
};
