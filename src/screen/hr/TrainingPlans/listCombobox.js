import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Tên nhân viên', width: 300},
    {title: 'Loại hình đào tạo', width: 150},
    {title: 'Mô tả', width: 200},
    {title: 'Thời lượng học', width: 100},
    {title: 'Ngày bắt đầu', width: 120},
    {title: 'Học phí dự kiến', width: 100},
    {title: 'Lý do đào tạo', width: 100},
    {title: 'Nội dung', width: 150},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  //#endregion
};
