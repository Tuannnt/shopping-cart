import React, { Component } from 'react';
import {
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Alert,
  TextInput,
  ScrollView,
  Switch,
  Image,
  FlatList,
} from 'react-native';
import { Button, Divider, Icon as IconElement } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { API_ApplyOutsides, API_RegisterCars } from '@network';
import { Container, Content, Item, Label, Picker, Form } from 'native-base';
import { FuncCommon } from '../../../utils';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import DatePicker from 'react-native-datepicker';
import listCombobox from './listCombobox';
import ComboboxV2 from '../../component/ComboboxV2';
import RequiredText from '../../component/RequiredText';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';
import controller from './controller';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import _ from 'lodash';

const headerTable = listCombobox.headerTable;
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
class addTrainingPlans extends Component {
  constructor(props) {
    super(props);
    this.DataProcess = FuncCommon.DataProcess();
    this.state = {
      rows: [{}],
      isEdit: true,
      Attachment: [],
      refreshing: false,
      Data: null,
      DataDetail: null,
      search: '',
      Date: FuncCommon.ConDate(new Date(), 0),
      StartTime: FuncCommon.ConDate(new Date(), 0),
      EndTime: FuncCommon.ConDate(new Date(), 0),
      modalPickerTimeVisible: false,
      ListEmployee: [],
    };
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  helperNumber = num => {
    if (!num) {
      return '';
    }
    return num + '';
  };
  componentDidMount(): void {
    const { dataEdit, rows = [] } = this.props;
    this.getAllEmployeeByDepart();
    if (dataEdit) {
      let _row = rows.map(row => ({
        ...row,
        Fee: this.helperNumber(row.Fee),
      }));
      this.setState({
        isEdit: false,
        Title: dataEdit.Title,
        Description: dataEdit.Description,
        Note: dataEdit.Note,
        TrainingPlanId: dataEdit.TrainingPlanId,
        FullName: dataEdit.FullName,
        DepartmentName: dataEdit.DepartmentName,
        JobTitleNameEmployee: dataEdit.JobTitleName,
        rows: _row,
      });
    } else {
      this.getNumberAuto();
      this.setState({
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        JobTitleNameEmployee:
          global.__appSIGNALR.SIGNALR_object.USER.JobTitleName,
      });
    }
  }
  getAllEmployeeByDepart = () => {
    controller.getAllEmployeeByDepart(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.value,
        label: item.text,
        JobTitleName: item.JobTitleName,
        JobTitleId: item.JobTitleId,
      }));
      data.unshift({
        value: null,
        label: 'Chọn nhân viên',
      });
      this.setState({
        ListEmployee: data,
      });
    });
  };
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_TPL',
      Prefixs: null,
      VoucherType: '',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({ isEdit: data.IsEdit, TrainingPlanId: data.Value });
    });
  };
  Submit() {
    Keyboard.dismiss();
    const { Title, Description, Note, TrainingPlanId, Attachment } = this.state;
    let data = {
      Title: this.state.Title,
      Description,
      TrainingPlanId,
      Note,
    };
    var _obj = {
      FileAttachments: [],
      ...data,
    };

    if (!Title) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập tiêu đề kế hoạch',
        [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
        { cancelable: false },
      );
      return;
    }
    if (!TrainingPlanId) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập Mã kế hoạch',
        [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
        { cancelable: false },
      );
      return;
    }
    let isNotValidated = false;
    let lstDetails = [...this.state.rows];
    for (let i = 0; i < lstDetails.length; i++) {
      let lst = lstDetails[i];
      if (!lst.EmployeeGuid) {
        Alert.alert(
          'Thông báo',
          'Bạn chưa chọn nhân viên',
          [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
          { cancelable: false },
        );
        isNotValidated = true;
        return;
      }
      if (!lst.StartDate) {
        Alert.alert(
          'Thông báo',
          'Bạn chưa chọn ngày bắt đầu',
          [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
          { cancelable: false },
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }
    let auto = { Value: this.state.TrainingPlanId, IsEdit: this.state.isEdit };
    lstDetails = lstDetails.map(detail => ({
      ...detail,
    }));
    if (this.props.dataEdit) {
      let _edit = {
        ...this.props.dataEdit,
        ...data,
        Id: TrainingPlanId,
      };
      let _dataEdit = new FormData();
      _dataEdit.append('TrainingPlans', JSON.stringify(_edit));
      _dataEdit.append('auto', JSON.stringify({ IsEdit: true }));
      _dataEdit.append('DeleteAttach_orders', JSON.stringify([]));
      _dataEdit.append('lstDetails', JSON.stringify(lstDetails));
      _dataEdit.append('lstTitlefile', JSON.stringify([]));
      _dataEdit.append('ListDetailDelete', JSON.stringify([]));
      API_RegisterCars.TrainingPlans_UpdateAPI(_dataEdit)
        .then(rs => {
          if (rs.data.errorCode === 200) {
            Alert.alert(
              'Thông báo',
              'Cập nhật thành công',
              [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
              { cancelable: false },
            );
            this.onPressBack();
          }
        })
        .catch(error => {
          console.log('Error when call API update Mobile.');
          console.log(error);
        });
      return;
    }
    let _data = new FormData();
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    _data.append('TrainingPlans', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteAttach', JSON.stringify([]));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));

    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_RegisterCars.TrainingPlans_InsertAPI(_data)
      .then(rs => {
        if (rs.data.errorCode === 200) {
          Alert.alert(
            'Thông báo',
            'Thêm mới thành công',
            [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
            { cancelable: false },
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API insert Mobile.');
        console.log(error);
      });
  }
  //#region open img
  _open() { }
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({ Attachment: this.state.Attachment });
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({ Attachment: this.state.Attachment });
    } catch (err) {
      this.setState({ loading: false });
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#region openAttachment
  _openCombobox_Att() { }
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#endregion
  onChangeText = (key, value) => {
    this.setState({ [key]: value });
  };
  addRows = () => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({ rows: data });
  };
  onDelete = index => {
    const { rows } = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({ rows: res });
  };
  handleChangeRows = (val, index, name) => {
    const { rows } = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({ rows: res });
  };
  helperPosition = id => {
    if (!id) {
      return '';
    }
    let employee = this.state.ListEmployee.find(emp => emp.value === id);
    if (!employee) {
      return '';
    }
    return employee.JobTitleName;
  };
  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handleConfirmTime = date => {
    const { typeTime, currentIndex, rows } = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({ rows: res, typeTime: null, currentIndex: null });
    this.hideDatePicker();
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    return (
      <View style={{ flexDirection: 'row' }} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 40 }]}>
          <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        {/* ten nhan vien */}
        <View style={[AppStyles.table_td_custom, { width: 300 }]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'EmployeeGuid');
            }}
            items={this.state.ListEmployee}
            style={[stylePicker]}
            value={row.EmployeeGuid}
            placeholder={{}}
          />
        </View>
        {/* loại hinh đào tạo   */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 150 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.TypeOfTraining}
            autoCapitalize="none"
            onChangeText={TypeOfTraining => {
              this.handleChangeRows(TypeOfTraining, index, 'TypeOfTraining');
            }}
          />
        </TouchableOpacity>
        {/* mô tả */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 200 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Description}
            autoCapitalize="none"
            onChangeText={Description => {
              this.handleChangeRows(Description, index, 'Description');
            }}
          />
        </TouchableOpacity>
        {/* thơi lượng học */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Duration}
            autoCapitalize="none"
            onChangeText={Duration => {
              this.handleChangeRows(Duration, index, 'Duration');
            }}
          />
        </TouchableOpacity>
        {/* ngày bắt đầu */}
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('StartDate', index);
          }}
          style={[AppStyles.table_td_custom, { width: 120 }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {(row.StartDate && moment(row.StartDate).format('DD/MM/YYYY')) ||
              'Chọn thời gian'}
          </Text>
        </TouchableOpacity>
        {/* hoc phi dự kiến */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={row.Fee}
            autoCapitalize="none"
            onChangeText={Fee => {
              this.handleChangeRows(Fee, index, 'Fee');
            }}
          />
        </TouchableOpacity>
        {/* lý do đào tạo */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 100 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Reason}
            autoCapitalize="none"
            onChangeText={Reason => {
              this.handleChangeRows(Reason, index, 'Reason');
            }}
          />
        </TouchableOpacity>
        {/* nội dung */}
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 150 }]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Content}
            autoCapitalize="none"
            onChangeText={Content => {
              this.handleChangeRows(Content, index, 'Content');
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td_custom, { width: 80 }]}>
          <IconElement
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  helperToStringCurrency = (number, n = 2, x = 3, s = '.', c = ',') => {
    if (!number) {
      return '';
    }
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
      num = Number(number).toFixed(Math.max(0, ~~n));
    return (c ? num.replace('.', c) : num).replace(
      new RegExp(re, 'g'),
      '$&' + (s || ','),
    );
  };
  render() {
    const { isEdit, TrainingPlanId } = this.state;
    return (
      <Container style={{ paddingBottom: 10, height: '100%' }}>
        <View
          style={{ flex: 30 }}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={{ flex: 40 }}>
            {!this.props.dataEdit ? (
              <TabBar_Title
                title={'Thêm kế hoạch đào tạo'}
                callBack={() => this.onPressBack()}
              />
            ) : (
              <TabBar_Title
                title={'Chỉnh sửa kế hoạch đào tạo'}
                callBack={() => this.onPressBack()}
              />
            )}
            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Mã kế hoạch</Text>
                  </View>
                  {isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={TrainingPlanId}
                      autoCapitalize="none"
                      onChangeText={TrainingPlanId =>
                        this.onChangeText('TrainingPlanId', TrainingPlanId)
                      }
                    />
                  )}
                  {!isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, { color: AppColors.gray }]}>
                        {TrainingPlanId}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Người tạo phiếu</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, { color: 'black' }]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, { color: 'black' }]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>
                      Tiêu đề kế hoạch <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.Title}
                    onChangeText={Title => this.onChangeText('Title', Title)}
                  />
                </View>

                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Mô tả</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    value={this.state.Description}
                    onChangeText={Description =>
                      this.onChangeText('Description', Description)
                    }
                  />
                </View>
                <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    value={this.state.Note}
                    onChangeText={Note => this.onChangeText('Note', Note)}
                  />
                </View>

                <View style={{ marginTop: 5 }}>
                  <View style={{ width: 150, marginLeft: 10 }}>
                    <Button
                      title="Thêm dòng"
                      type="outline"
                      size={15}
                      onPress={() => this.addRows()}
                      buttonStyle={{ padding: 5, marginBottom: 5 }}
                      titleStyle={{ marginLeft: 5 }}
                      icon={
                        <IconElement
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>

                  {/* Table */}
                  <ScrollView horizontal={true}>
                    <View style={{ flexDirection: 'column' }}>
                      <View style={{ flexDirection: 'row' }}>
                        {headerTable.map(item => (
                          <TouchableOpacity
                            key={item.title}
                            style={[AppStyles.table_th, { width: item.width }]}>
                            <Text style={AppStyles.Labeldefault}>
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        ))}
                      </View>
                      <View
                      // style={{
                      //   maxHeight: Dimensions.get('window').height / 7,
                      // }}
                      >
                        {this.state.rows.length > 0 ? (
                          <FlatList
                            data={this.state.rows}
                            renderItem={({ item, index }) => {
                              return this.itemFlatList(item, index);
                            }}
                            keyExtractor={(rs, index) => index.toString()}
                          />
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                { textAlign: 'left' },
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View>
                {/* attachment */}
                {!this.props.dataEdit && (
                  <View
                    style={{
                      flexDirection: 'column',
                      padding: 10,
                      marginTop: 5,
                    }}>
                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                        <Text style={{ color: 'red' }}> </Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          flexDirection: 'row',
                        }}
                        onPress={() => this.openAttachment()}>
                        <IconElement
                          name="attachment"
                          type="entypo"
                          size={15}
                          color={AppColors.ColorAdd}
                        />
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            { color: AppColors.ColorAdd },
                          ]}>
                          {' '}
                          Chọn file
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <Divider style={{ marginBottom: 10 }} />
                    {this.state.Attachment.length > 0
                      ? this.state.Attachment.map((para, i) => (
                        <View
                          key={i}
                          style={[
                            styles.StyleViewInput,
                            { flexDirection: 'row', marginBottom: 3 },
                          ]}>
                          <View
                            style={[
                              AppStyles.containerCentered,
                              { padding: 10 },
                            ]}>
                            <Image
                              style={{
                                width: 30,
                                height: 30,
                                borderRadius: 10,
                              }}
                              source={{
                                uri: this.FileAttackments.renderImage(
                                  para.FileName,
                                ),
                              }}
                            />
                          </View>
                          <TouchableOpacity
                            key={i}
                            style={{ flex: 1, justifyContent: 'center' }}>
                            <Text style={AppStyles.Textdefault}>
                              {para.FileName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[
                              AppStyles.containerCentered,
                              { padding: 10 },
                            ]}
                            onPress={() => this.removeAttactment(para)}>
                            <IconElement
                              name="close"
                              type="antdesign"
                              size={20}
                              color={AppColors.ColorDelete}
                            />
                          </TouchableOpacity>
                        </View>
                      ))
                      : null}
                  </View>
                )}
              </View>
            </ScrollView>
          </View>
        </View>

        <OpenPhotoLibrary
          callback={this.callbackLibarary}
          openLibrary={this.openLibrary}
        />
        <ComboboxV2
          callback={this.ChoiceAtt}
          data={listCombobox.ListComboboxAtt}
          eOpen={this.openCombobox_Att}
        />
        {this.state.modalPickerTimeVisible && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisible}
            mode="date"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        <View>
          <Button
            buttonStyle={{ backgroundColor: AppColors.ColorButtonSubmit }}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
      </Container>
    );
  }
  setTitle(Title) {
    this.setState({ Title });
  }
  setDate(Date) {
    this.setState({ Date: Date });
  }
  setStartTime(StartTime) {
    this.setState({ StartTime: StartTime });
  }
  setEndTime(EndTime) {
    this.setState({ EndTime: EndTime });
  }
  onPressBack() {
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      Data: {},
      ActionTime: new Date().getTime(),
    });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  //#region  customFloat
  customFloat = val => {
    if (val !== null) {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        return valview + ',' + string[1].substring(0, 2);
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(addTrainingPlans);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 10,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
