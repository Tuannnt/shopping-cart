import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  ToastAndroid,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';
import {Button, Divider, Icon as IconElement} from 'react-native-elements';

import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {API_RegisterCars} from '@network';
import API from '../../../network/API';
import {DatePicker, Container, ListItem, Label, Picker} from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import controller from './controller';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import listCombobox from './listCombobox';
const ItemCode = {
  PERSONNEL: 0,
  SELL: 1,
  CUSTOMER: 2,
  PURCHASE: 3,
  ACCOUNTANT: 4,
  WAREHOUSE: 5,
  OPERATIONAL: 6,
  DOCUMENT: 7,
  MARKETING: 8,
  PROJECT: 9,
  MANUFACTURING: 10,
  PROPERTY: 11,
  ESTABLISH: 12,
  LOGOUT: 13,
};
const {headerTable} = listCombobox;
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
class openTrainingPlans extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      ListEmployee: [],
      list: [],
      staticParam: {
        TrainingPlanGuid: '',
      },
      TrainingPlanGuid: null,
      Data: [],
      listWofflowData: [],
      loading: false,
      isModalVisible: false,
      txtComment: '',
      checkInLogin: '',
      LoginName: '',
      Date: null,
      StartTime: null,
      EndTime: null,
      selected2: [],
      Reciever: [],
      Licars: [
        {
          CarName: 'Chọn xe',
          CarId: '',
        },
      ],
    };
  }
  componentDidMount(): void {
    this.state.TrainingPlanGuid = this.props.RecordGuid;
    this.getAllEmployeeByDepart();
    this.getItem();
    this.GetAll();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }
  getAllEmployeeByDepart = () => {
    controller.getAllEmployeeByDepart(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.value,
        label: item.text,
        JobTitleName: item.JobTitleName,
        JobTitleId: item.JobTitleId,
      }));
      data.unshift({
        value: null,
        label: 'Chọn nhân viên',
      });
      this.setState({
        ListEmployee: data,
      });
    });
  };
  GetAll = () => {
    this.state.staticParam.TrainingPlanGuid = this.state.TrainingPlanGuid;
    API_RegisterCars.TrainingPlanDetails_ListDetail(this.state.staticParam)
      .then(res => {
        this.state.list = JSON.parse(res.data.data);
        this.setState({list: this.state.list});
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  getItem() {
    let id = {Id: this.props.RecordGuid};
    API_RegisterCars.TrainingPlans_Items(id)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
        });
        console.log('===========> error' + res.data.data);
        let checkin = {
          TrainingPlanGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_RegisterCars.CheckLogin(checkin)
          .then(res => {
            this.setState({checkInLogin: res.data.data});
            console.log('===checkin====' + res.data.data);
            console.log('--------' + this.state.Data.Permisstion);
          })
          .catch(error => {
            console.log(error.data.data);
          });
        API.getProfile().then(rs => {
          console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
          this.setState({LoginName: rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
    this.GetCommentRegisterCars(this.page);
  }
  keyExtractor = (item, index) => index.toString();
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        {/* ten nhan vien */}
        <View style={[AppStyles.table_td_custom, {width: 300}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {}}
            items={this.state.ListEmployee}
            style={[stylePicker]}
            value={row.EmployeeGuid}
            placeholder={{}}
            disabled
          />
        </View>
        {/* loại hinh đào tạo   */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.TypeOfTraining}
            autoCapitalize="none"
            maxLength={50}
          />
        </TouchableOpacity>
        {/* mô tả */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Description}
            autoCapitalize="none"
          />
        </TouchableOpacity>
        {/* thơi lượng học */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Duration}
            autoCapitalize="none"
          />
        </TouchableOpacity>
        {/* ngày bắt đầu */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.StartDate && moment(row.StartDate).format('DD/MM/YYYY')) ||
              'Chọn thời gian'}
          </Text>
        </TouchableOpacity>
        {/* hoc phi dự kiến */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={row.Fee}
            autoCapitalize="none"
          />
        </TouchableOpacity>
        {/* lý do đào tạo */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Reason}
            autoCapitalize="none"
          />
        </TouchableOpacity>
        {/* nội dung */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Content}
            autoCapitalize="none"
          />
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    return (
      <Container>
        <View
          style={{
            flex: 1,
            fontSize: 11,
            fontFamily: Fonts.base.family,
            backgroundColor: 'white',
          }}>
          <TabBar_Title
            title={'Chi tiết kế hoạch đào tạo'}
            callBack={() => this.onPressBack()}
          />
          <View style={{flex: 10}}>
            <View style={{padding: 20}}>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Mã kế hoạch :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault, {fontWeight: 'bold'}]}>
                    {this.state.Data.TrainingPlanId}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Tiêu đề :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.Data.Title}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Tên nhân viên :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.Data.FullName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.Data.DepartmentName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                </View>
                <View style={{flex: 2}}>
                  {this.state.Data.Status == 'Y' ? (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.AcceptColor},
                      ]}>
                      {this.state.Data.StatusWF}
                    </Text>
                  ) : (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.PendingColor},
                      ]}>
                      {this.state.Data.StatusWF}
                    </Text>
                  )}
                </View>
              </View>

              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Ghi chú :</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.Data.Note}
                  </Text>
                </View>
              </View>
            </View>
            <Divider style={{marginTop: 10}} />
            <View style={{flexDirection: 'row', paddingRight: 10}}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  marginLeft: 10,
                  flex: 1,
                }}>
                Chi tiết
              </Text>
            </View>
            <View style={{flex: 1}}>
              <View style={{marginTop: 5, marginBottom: 10, paddingBottom: 10}}>
                {/* Table */}
                <ScrollView horizontal={true}>
                  <View style={{flexDirection: 'column'}}>
                    <View style={{flexDirection: 'row'}}>
                      {headerTable.map(item => {
                        if (item.hideInDetail) {
                          return null;
                        }
                        return (
                          <TouchableOpacity
                            key={item.title}
                            style={[AppStyles.table_th, {width: item.width}]}>
                            <Text style={AppStyles.Labeldefault}>
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        );
                      })}
                    </View>
                    <View>
                      {this.state.list.length > 0 ? (
                        <FlatList
                          data={this.state.list}
                          renderItem={({item, index}) => {
                            return this.itemFlatList(item, index);
                          }}
                          keyExtractor={(rs, index) => index.toString()}
                        />
                      ) : (
                        <TouchableOpacity style={[AppStyles.table_foot]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'left'},
                            ]}>
                            Không có dữ liệu!
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  </View>
                </ScrollView>
              </View>
            </View>
          </View>
          <View style={{flex: 1}}>
            {this.state.checkInLogin !== '' ? (
              <TabBarBottom
                onDelete={() => this.onDelete()}
                isDraff={this.state.Data.StatusWF === 'Nháp' ? true : false}
                isComplete={this.state.Data.Status === 'Y' ? true : false}
                haveEditButton={
                  this.state.Data.Permisstion === 1 ? true : false
                }
                onAttachment={() => this.openAttachments()}
                callbackOpenUpdate={this.callbackOpenUpdate}
                //key để quay trở lại danh sách
                backListByKey="TrainingPlans"
                keyCommentWF={{
                  ModuleId: 39,
                  RecordGuid: this.props.RecordGuid,
                  Title: this.state.Data.Title,
                }}
                // tiêu đề hiển thị trong popup xử lý phiếu
                Title={this.state.Data.Title}
                //kiểm tra quyền xử lý
                Permisstion={this.state.Data.Permisstion}
                //kiểm tra bước đầu quy trình
                checkInLogin={this.state.checkInLogin}
                onSubmitWF={(callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
                }
              />
            ) : null}
          </View>
        </View>
      </Container>
    );
  }
  callbackOpenUpdate = () => {
    Actions.addTrainingPlans({
      dataEdit: this.state.Data,
      rows: this.state.list,
    });
  };
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  openView(id) {
    Actions.openTrainingPlanDetails({Id: id});
    console.log('===================chuyen Id:' + id);
  }
  openAttachments() {
    var obj = {
      ModuleId: '13',
      RecordGuid: this.props.RecordGuid,
      notEdit: this.state.Data.PermissEdit !== 1 ? true : false,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if (csStatus === 'W' || csStatus == 'R') {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  onClick(data) {
    this.setState({selectedTab: data, check: false});
    if (data == 'home') {
      Actions.app();
    } else if (data == 'List') {
      Actions.indexRegistercar();
    } else if (data == 'ok' || data == 'no') {
      this.setState({isModalVisible: !this.state.isModalVisible});
    } else if (data == 'comment') {
      Actions.commentRegistercar({
        TrainingPlanGuid: this.props.TrainingPlanGuid,
      });
    }
  }
  handleEmail(data) {
    this.setState({CommentWF: data});
  }
  setPickUpLocation(PickUpLocation) {
    this.state.Data.PickUpLocation = PickUpLocation;
    this.setState({Data: this.state.Data});
  }
  setGetOffLocation(GetOffLocation) {
    this.state.Data.GetOffLocation = GetOffLocation;
    this.setState({Data: this.state.Data});
  }
  setReplaceBy(ReplaceBy) {
    this.state.Data.ReplaceBy = ReplaceBy;
    this.setState({Data: this.state.Data});
  }
  setTolls(Tolls) {
    this.state.Data.Tolls = Tolls;
    this.setState({Data: this.state.Data});
  }
  setDescription(Description) {
    this.state.Data.Description = Description;
    this.setState({Data: this.state.Data});
  }
  setDate(Date) {
    this.state.Data.Date = Date;
    this.setState({Data: this.state.Data});
  }
  setStartDate(StartTime) {
    this.state.Data.StartTime = StartTime;
    this.setState({Data: this.state.Data});
  }
  setEndDate(EndTime) {
    this.state.Data.EndTime = EndTime;
    this.setState({Data: this.state.Data});
  }
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      TrainingPlanGuid: this.state.Data.TrainingPlanGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'D') {
      API_RegisterCars.Approve_TrainingPlans(obj)
        .then(rs => {
          if (rs.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data.data);
        });
    } else {
      API_RegisterCars.NotApprove_TrainingPlans(obj)
        .then(rs => {
          if (rs.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data.data);
        });
    }
  }
  closeComment() {
    this.setState({isModalVisible: !this.state.isModalVisible});
  }
  onDelete = () => {
    Toast.showWithGravity(
      'Pending Api',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
  };
  GetCommentRegisterCars = page => {
    let obj = {
      page: page,
      ItemPage: this.Length,
      RecordGuid: this.state.Data.TrainingPlanGuid,
    };
    this.setState({loading: true});
    console.log(this.state);
    API_RegisterCars.GetCommentRegisterCars(obj)
      .then(rs => {
        console.log(
          '=======ThongBao: ' +
            JSON.stringify(
              JSON.parse(JSON.parse(JSON.stringify(rs.data)).data).Data,
            ),
        );
        let listData = this.state.listWofflowData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(rs.data)).data).Data,
          ),
        );
        let data = listData.concat(data1);
        this.setState({listWofflowData: data, loading: false});
        this.arrayholder = data;
        let TotalRow = JSON.parse(JSON.parse(JSON.stringify(rs.data)).data);
        this.TotalRow = TotalRow.Count;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  handleLoadMore = () => {
    if (
      !this.state.loading &&
      this.TotalRow > this.state.listWofflowData.length
    ) {
      this.page = this.page + 1;
      this.GetCommentRegisterCars(this.page);
    }
  };
  renderFooter = () => {
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    fontSize: 14,
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const mapStateToProps = state => ({Loginname: state.user.account});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(openTrainingPlans);
