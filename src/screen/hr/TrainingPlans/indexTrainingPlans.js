
import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, RefreshControl, ScrollView, TouchableWithoutFeedback, Keyboard, TouchableOpacity } from 'react-native';
import { Header, SearchBar, Icon, Badge } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { API_RegisterCars } from '@network';
import API from "../../../network/API";
import { Container, ListItem, Button } from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import LoadingComponent from '../../component/LoadingComponent';

class indexTrainingPlans extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staticParam: {
                Status: "W",
                NumberPage: 1,
                Length: 15,
                search: {
                    value: ""
                }
            },
            name: '',
            refreshing: false,
            list: [],
            ListCountNoti: [],
            Pending_TrainingPlans: '',
            Notapprove_TrainingPlans: '',
            selectedTab: 'profile',
            isFetching: false,
            EvenFromSearch: false,
            loading: true
        };
        this.listtabbarBotom = [
            {
                Title: "Chờ duyệt",
                Icon: "profile",
                Type: "antdesign",
                Value: "profile",
                Checkbox: true
            },
            {
                Title: "Đã duyệt",
                Icon: "checkcircleo",
                Type: "antdesign",
                Value: "checkcircleo",
                Checkbox: false
            },
            {
                Title: "Trả lại",
                Icon: "export2",
                Type: "antdesign",
                Value: "export2",
                Checkbox: false
            }
        ]
    }

    loadMoreData() {
        this.nextPage();
    }
    _onRefresh = () => {
        this.nextPage('reload');

    }
    render() {
        let arrayholder = [];
        return (
            this.state.loading === true ?
                <LoadingComponent />
                :
                <Container>
                    <View style={{ flex: 3 }}>
                        <TouchableWithoutFeedback style={{ flex: 30 }} onPress={() => {
                            Keyboard.dismiss()
                        }}>
                            <View style={{ flex: 1 }}>
                                <TabBar
                                    title={'Kế hoạch đào tạo'}
                                    FormSearch={true}
                                    CallbackFormSearch={(callback) => this.setState({ EvenFromSearch: callback })}
                                    BackModuleByCode={'HR'}
                                    CallbackFormAdd={() => Actions.addTrainingPlans()}
                                    addForm={true}
                                />
                                {this.state.EvenFromSearch == true ?
                                    <SearchBar
                                        placeholder="Tìm kiếm..."
                                        lightTheme
                                        round
                                        inputContainerStyle={{ backgroundColor: '#e1ecf4' }}
                                        containerStyle={AppStyles.FormSearchBar}
                                        onChangeText={text => this.updateSearch(text)}
                                        value={this.state.staticParam.search.value}
                                    />
                                    : null}
                                {this.state.list ?
                                    <View style={{ flex: 1 }}>
                                        <FlatList
                                            style={{ marginBottom: 20, height: '100%' }}
                                            ref={(ref) => {
                                                this.ListView_Ref = ref;
                                            }}
                                            keyExtractor={item => item.BankGuid}
                                            data={this.state.list}
                                            renderItem={({ item, index }) => (
                                                <ScrollView style={{}}>
                                                    <View >
                                                        <ListItem onPress={() => this.openView(item.TrainingPlanGuid)}>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                {item.Status == 'W' || item.Status == 'R' ?
                                                                    <View style={{ width: 15, height: 50 }}>
                                                                        <Text style={styles.statusNew} />
                                                                    </View>
                                                                    : null}
                                                                <View style={{ flex: 3 }}>
                                                                    <Text style={[AppStyles.Titledefault]}>{item.TrainingPlanId}</Text>
                                                                    <Text style={[AppStyles.Textdefault]}>Tiêu đề: {item.Title}</Text>
                                                                    <Text style={[AppStyles.Textdefault]}>Tên nhân viên: {item.FullName}</Text>
                                                                </View>
                                                                <View style={{ flex: 2, alignItems: 'flex-end' }}>
                                                                    <Text style={[AppStyles.Textdefault]}>{item.DepartmentName}</Text>
                                                                    {item.Status == "Y" ?
                                                                        <Text style={[{ justifyContent: 'center', color: AppColors.AcceptColor }, AppStyles.Textdefault]}>{item.StatusWF}</Text>
                                                                        : <Text style={[{ justifyContent: 'center', color: AppColors.PendingColor }, AppStyles.Textdefault]}>{item.StatusWF}</Text>}
                                                                </View>
                                                            </View>
                                                        </ListItem>
                                                    </View>
                                                </ScrollView>
                                            )}
                                            onEndReached={() => this.loadMoreData()}
                                            refreshControl={
                                                <RefreshControl
                                                    refreshing={this.state.refreshing}
                                                    onRefresh={this._onRefresh}
                                                    tintColor="#f5821f"
                                                    titleColor="#fff"
                                                    colors={["red", "green", "blue"]}
                                                />
                                            }
                                        />
                                        {/* hiển thị nút tuỳ chọn */}
                                        <View style={AppStyles.StyleTabvarBottom}>
                                            <TabBarBottomCustom
                                                ListData={this.listtabbarBotom}
                                                onCallbackValueBottom={(callback) => this.onCallbackValueBottom(callback)}
                                            />
                                        </View>
                                    </View>
                                    : null}
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </Container>
        )
    }
    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };
    onCallbackValueBottom(value) {
        switch (value) {
            case 'profile':
                this.onClickPending();
                break;
            case 'checkcircleo':
                this.onClickAprover();
                break;
            case 'export2':
                this.onClickNotApprove();
                break;
            default:
                break;
        }
    }
    // sự kiện chờ duyệt
    onClickPending = () => {
        this.state.staticParam.Status = 'W';
        this.state.selectedTab = "profile"
        this.setState({
            stylex: 1
        })
        this.setState({
            list: [],
            selectedTab: this.state.selectedTab
        });
        this.nextPage('reload');
    };
    onClickAprover = () => {
        this.state.staticParam.Status = 'Y';
        this.state.selectedTab = "checkcircleo"
        this.setState({
            stylex: 2
        })
        this.setState({
            list: [],
            selectedTab: this.state.selectedTab
        });
        this.nextPage('reload');
    };
    EvenFromSearch() {
        if (this.state.EvenFromSearch === false) {
            this.setState({ EvenFromSearch: true })
        }
        else {
            this.setState({ EvenFromSearch: false })
        }
    }
    onClickNotApprove = () => {
        this.state.staticParam.Status = 'R';
        this.state.selectedTab = "export2"
        this.setState({
            stylex: 3
        })
        this.setState({
            list: [],
            selectedTab: this.state.selectedTab
        });
        this.nextPage('reload');
    };
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    //định dạng tiền tệ
    addPeriod(nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    updateSearch = text => {
        this.state.staticParam.search.value = text;
        this.setState({
            list: [],
            staticParam: {
                Status: this.state.staticParam.Status,
                NumberPage: 1,
                Length: 15,
                search: {
                    value: this.state.staticParam.search.value
                }
            }
        });
        console.log('keyword: ' + this.state.staticParam.Search);
        this.GetAll();
    }
    nextPage(type) {
        this.state.staticParam.NumberPage++;
        if (type === 'reload') {
            this.state.staticParam.NumberPage = 1;
        }
        this.setState({
            staticParam: {
                EndDate: this.state.staticParam.EndDate,
                Status: this.state.staticParam.Status,
                NumberPage: this.state.staticParam.NumberPage,
                Length: 15,
                search: {
                    value: this.state.staticParam.search.value
                }
            }
        });
        this.GetAll(type);
    }
    GetAll = (type) => {
        API_RegisterCars.TrainingPlans_ListAll(this.state.staticParam)
            .then(res => {
                let _data = JSON.parse(res.data.data).data
                if (type === 'reload') {
                    this.state.list = _data
                } else {
                    this.state.list = this.state.list.concat(_data)
                }
                this.setState({ list: this.state.list, loading: false })
            })
            .catch(error => {
                console.log('===========> error');
                console.log(error);
            });

    }
    openView(id) {
        Actions.openTrainingPlans({ RecordGuid: id });
        console.log('===================chuyen Id:' + id);
    }
    componentDidMount() {
        this.GetAll()
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.moduleId == 'Back') {
            this.nextPage('reload');
        }
    }
    clickItem(item) {
        console.log('================> ' + JSON.stringify(item));
        switch (item) {
            case 'Back':
                Actions.app();
                break;
            case 'Open':
                Actions.openTrainingPlans();
                break;
            default:
                break;
        }
    }
}
const styles = StyleSheet.create({
    searchcontainer: {
        backgroundColor: 'white',
        height: 50,
        paddingLeft: 10,
        paddingRight: 10,
        borderWidth: 0, //no effect
        shadowColor: 'white', //no effect
        borderBottomColor: 'transparent',
        borderTopColor: 'transparent',
    },
    statusNew: {
        marginTop: 5,
        width: 8,
        height: 8,
        borderRadius: 4,
        borderWidth: 0.1,
        borderColor: 'white',
        backgroundColor: '#4baf57',
    },
    TabarPending: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10
    },
    TabarApprove: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10
    },
    tabNavigatorItem: {
        justifyContent: 'space-between',
        alignSelf: 'center',
    },
    itemIcon: {
        fontWeight: '600',
        fontSize: 23,
        color: 'black',
    },
    itemIconClick: {
        fontWeight: '600',
        fontSize: 23,
        color: '#f6b801',
    },

})
//Redux
const mapStateToProps = state => ({
})
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(indexTrainingPlans);