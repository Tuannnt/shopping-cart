import React, { Component } from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { ActionConst, Actions } from 'react-native-router-flux';
import { Header, Icon } from 'react-native-elements';
import RNPickerSelect from 'react-native-picker-select';

class TabBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //Search
      offSearch: false,
      //lịch
      Formcalendar: false,
      offCalendar: false,
      //QA
      FormQR: false,
      offQR: false,
      QRMultiple: false,
      //tắt quay lại
      OffBack: false,
      StartPop: this.props.StartPop,
      //task
      offTask: false,
      //checkbox
      FormCheckBox: false,
      offCheckBox: false,
    };
    this.CallbackFormSearch = this.props.CallbackFormSearch;
    this.CallbackFormQR = this.props.CallbackFormQR;
    this.CallbackFormAdd = this.props.CallbackFormAdd;
    this.CallbackFormcalendar = this.props.CallbackFormcalendar;
    this.CallbackFormTask = this.props.CallbackFormTask;
    this.CallbackFormCheckBox = this.props.CallbackFormCheckBox;
  }
  componentDidMount() {
    if (this.props.QRMultiple) {
      this.setState({ QRMultiple: this.props.QRMultiple });
    }
    if (this.props.Formcalendar) {
      this.setState({ Formcalendar: this.props.Formcalendar });
    }
    if (this.props.task) {
      this.setState({ offCalendar: this.props.task });
    }
    if (this.props.FormQR == true) {
      this.setState({ FormQR: this.props.FormQR });
    }
    if (this.props.OffBack == true) {
      this.setState({ OffBack: this.props.OffBack });
    }
    if (this.props.FormCheckBox == true) {
      this.setState({ FormCheckBox: this.props.FormCheckBox });
    }
  }
  render() {
    const {
      hideLeft,
      leftIcon,
      leftIconSet,
      rightIcon,
      rightIconSet,
      title,
      subTitle,
    } = this.props;
    return (
      <Header
        // containerStyle={{shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 5,
        // },
        // shadowOpacity: 0.2,
        // shadowRadius: 10.97,
        // elevation: 2,}}
        statusBarProps={AppStyles.statusbarProps}
        {...AppStyles.headerProps}
        leftComponent={this.renderLeftComponent(
          hideLeft,
          leftIcon,
          leftIconSet,
        )}
        centerComponent={this.renderTitle(title, subTitle)}
        rightComponent={this.renderRightComponent(rightIcon, rightIconSet)}
      />
    );
  }
  renderLeftComponent = (hideLeft, leftIcon, leftIconSet) => {
    if (!leftIcon) {
      leftIcon = 'arrowleft';
      leftIconSet = 'antdesign';
    }
    return (
      <View style={{ flexDirection: 'row' }}>
        {this.state.OffBack == false ? (
          <TouchableOpacity
            style={[AppStyles.TabbarTop_left, { flex: 1, width: 50 }]}
            onPress={() => this.onPressback()}>
            {!hideLeft && (
              <Icon
                name={'arrow-left'}
                type="feather"
                size={AppSizes.TabBar_SizeIcon}
                color={AppColors.black}
              />
            )}
          </TouchableOpacity>
        ) : null}
      </View>
    );
  };
  renderRightComponent = (addForm, rightIcon, rightIconSet) => {
    const { listYear = [], currentYear = new Date().getFullYear() } = this.props;
    const stylePicker = {
      inputIOS: {
        fontSize: 16,
        paddingVertical: 3,
        paddingHorizontal: 3,
        textAlign: 'center',
        marginTop: 12,
        paddingLeft: 5,
        color: 'black',
        width: 120,
      },
      inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 0,
        paddingVertical: 0,
        borderRadius: 5,
        color: 'black',
        paddingLeft: 5,
        width: 120,
      },
    };
    return (
      <View
        style={[
          AppStyles.TabbarTop_Right,
          {
            flexDirection: 'row',
            minWidth: 120,
            justifyContent: 'flex-end',
            marginRight: -20,
            alignItems: 'center',
          },
        ]}>
        <RNPickerSelect
          doneText="Xong"
          onValueChange={value => {
            this.callBackGetYear(value);
          }}
          items={listYear}
          style={stylePicker}
          value={currentYear}
          placeholder={{}}
          useNativeAndroidPickerStyle={false}
        />
      </View>
    );
  };
  callBackGetYear = value => {
    this.props.callBackGetYear(value);
  };
  renderTitle = (title, subTitle) => {
    return (
      <View
        style={[
          AppStyles.TabbarTop_Center,
          { marginLeft: -20, textAlign: 'right' },
        ]}>
        <Text
          style={[
            AppStyles.Titledefault,
            { textAlign: 'right', color: AppColors.TextColorMSS },
          ]}>
          {title}
        </Text>
      </View>
    );
  };
  onPressback() {
    Actions.home()
  }
  openFormSearch() {
    var _offSearch = !this.state.offSearch;
    this.setState({ offSearch: _offSearch });
    this.CallbackFormSearch(_offSearch);
  }
  openFormCheckBox = () => {
    this.setState({ offCheckBox: !this.state.offCheckBox });
    this.CallbackFormCheckBox(this.state.offCheckBox);
  };
  openFormQR() {
    if (this.state.QRMultiple === true) {
      Actions.qrCode({ typeQR: 'multiple' });
    } else {
      Actions.qrCode({ typeQR: 'single' });
    }

    // var _offQR = !this.state.offQR;
    // this.setState({ offQR: _offQR })
    // this.CallbackFormQR(_offQR)
  }
  openFormAdd() {
    this.CallbackFormAdd();
  }
  openFormcalendar() {
    var _offCalendar = !this.state.offCalendar;
    this.setState({ offCalendar: _offCalendar });
    this.CallbackFormcalendar(_offCalendar);
  }
  openFormTask = () => {
    this.CallbackFormTask('');
  };
}

// CSS
const navHeight = Platform.OS === 'ios' ? 80 : 80 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
const styles = StyleSheet.create({
  renderRightComponent: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 1000,
    marginRight: 10,
  },
});

export default TabBar;
