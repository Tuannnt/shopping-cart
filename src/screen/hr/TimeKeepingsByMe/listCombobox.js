export default {
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Ngày', width: 100},
    {title: 'Thứ', width: 100},
    {title: 'Giờ vào', width: 100},
    {title: 'Giờ ra', width: 100},
    {title: 'Trễ', width: 100},
    {title: 'Sớm', width: 100},
    {title: 'Công', width: 100},
    {title: 'T.giờ', width: 100},
    {title: 'Giờ LT', width: 100},
    {title: 'Ca', width: 100},
  ],
};
