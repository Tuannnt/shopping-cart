import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Platform,
} from 'react-native';
import { Divider, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import TabBar from './TabBar';
import { Actions } from 'react-native-router-flux';
import { API_HR, API } from '@network';
import { ErrorHandler } from '@error';
import { Container, ListItem, Body, Label, Picker } from 'native-base';
// import Icon from 'react-native-vector-icons/AntDesign';
import { AppStyles, AppSizes, AppColors } from '@theme';
import { FlatGrid } from 'react-native-super-grid';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';
import { LocaleConfig } from 'react-native-calendars';
import { CustomView } from '@Component';
import { FuncCommon } from '@utils';
import Modal, {
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
  SlideAnimation,
} from 'react-native-modals';
import LoadingComponent from '../../component/LoadingComponent';
const SCREEN_WIDTH = Dimensions.get('window').width;
const dayNames = [
  'Chủ nhật',
  'Thứ hai',
  'Thứ ba',
  'Thứ tư',
  'Thứ năm',
  'Thứ sáu',
  'Thứ bảy',
];
LocaleConfig.locales.vi = {
  monthNames: [
    'Tháng 1',
    'Tháng 2',
    'Tháng 3',
    'Tháng 4',
    'Tháng 5',
    'Tháng 6',
    'Tháng 7',
    'Tháng 8',
    'Tháng 9',
    'Tháng 10',
    'Tháng 11',
    'Tháng 12',
  ],
  monthNamesShort: [
    '1.',
    '2.',
    '3.',
    '4.',
    '5.',
    '6.',
    '7.',
    '8.',
    '9.',
    '10.',
    '11.',
    '12.',
  ],
  dayNames,
  dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
  today: 'Hôm nay',
};
LocaleConfig.defaultLocale = 'vi';
class TimeKeepingsByMeComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 31;
    this.TotalRow = 0;
    var d = new Date();
    var m = d.getMonth() + 1;
    var y = d.getFullYear();
    this.state = {
      Per: 0,
      ListAll: {},
      current: moment(new Date()).format('YYYY-MM-DD'),
      showModal: false,
      name: '',
      loading: true,
      refreshing: false,
      employeeData: [],
      search: '',
      EmployeeItem: [],
      Month: m,
      Year: y,
      TotalCong: 0,
      TotalTre: 0,
      TotalSom: 0,
      TongNgayNghi: 0,
      TongDaNghiNV: 0,
      PhepConLai: 0,
      currentYear: new Date().getFullYear(),
      listYear: [
        {
          value: 2020,
          label: 'Năm 2020',
        },
        {
          value: 2021,
          label: 'Năm 2021',
        },
        {
          value: 2022,
          label: 'Năm 2022',
        },
        {
          value: 2023,
          label: 'Năm 2023',
        },
        {
          value: 2024,
          label: 'Năm 2024',
        },
        {
          value: 2025,
          label: 'Năm 2025',
        },
        {
          value: 2026,
          label: 'Năm 2026',
        },
        {
          value: 2027,
          label: 'Năm 2027',
        },
        {
          value: 2028,
          label: 'Năm 2028',
        },
        {
          value: 2029,
          label: 'Năm 2029',
        },

        {
          value: 2030,
          label: 'Năm 2030',
        },
      ],

      list: [
        {
          Title: 'Tổng công',
          Icon: 'barschart',
          type: 'antdesign',
          value: 0,
        },
        {
          Title: 'Trễ ',
          Icon: 'clockcircle',
          type: 'antdesign',
          value: 0,
        },
        {
          Title: 'Sớm',
          Icon: 'clockcircleo',
          type: 'antdesign',
          value: 0,
        },
      ],
    };
    this.timeIn = 'Vào sớm';
    this.timeOut = 'Ra sớm';
  }
  componentDidMount = () => {
    Promise.all([this.viewTimeKeeperListAll()]);
  };

  viewTimeKeeperListAll = () => {
    let obj = {
      DepartmentID: [],
      EndDate: this.formatDateToFetchApi(this.state.current, '31'),
      StartDate: this.formatDateToFetchApi(this.state.current, '01'),
      TypeMachine: 'M',
      length: this.Length,
      Search: '',
    };
    this.setState({ loading: true }, () => {
      API_HR.viewTimeKeeperListAll(obj)
        .then(rs => {
          let res = JSON.parse(rs.data.data);
          if (!res) {
            this.setState({
              list: [
                {
                  Title: 'Tổng công',
                  Icon: 'barschart',
                  type: 'antdesign',
                  value: 0,
                },
                {
                  Title: 'Trễ',
                  Icon: 'clockcircle',
                  type: 'antdesign',
                  value: this.convertMinutesToHoursAndMinutes(0),
                },
                {
                  Title: 'Sớm',
                  Icon: 'clockcircleo',
                  type: 'antdesign',
                  value: this.convertMinutesToHoursAndMinutes(0),
                },
              ],
              ListAll: {},
              loading: false,
            });
            return;
          }
          let data = res.list;
          let _data = {};
          // -------------
          data.map(item => {
            if (item.DateCheck) {
              _data[this.formatDate(item.DateCheck)] = {
                status: !item.CheckIn || !item.CheckOut ? false : true,
                StartTimeWorking: item.StartTimeWorking,
                EndTimeWorking: item.EndTimeWorking,
                CheckIn: item.CheckIn,
                CheckOut: item.CheckOut,
                chechInEarly: item.chechInEarly,
                chechInLate: item.chechInLate,
                chechOutEarly: item.chechOutEarly,
                chechOutLate: item.chechOutLate,
              };
            }
          });
          let list = [
            {
              Title: 'Tổng công',
              Icon: 'barschart',
              type: 'antdesign',
              value: Number.parseFloat(res.total).toFixed(2) || 0,
            },
            {
              Title: 'Trễ',
              Icon: 'clockcircle',
              type: 'antdesign',
              value: this.convertMinutesToHoursAndMinutes(res.totalLate),
            },
            {
              Title: 'Sớm',
              Icon: 'clockcircleo',
              type: 'antdesign',
              value: this.convertMinutesToHoursAndMinutes(res.totalEarly),
            },
          ];
          this.setState({
            ListAll: _data,
            Data_fake: res.CheckDC,
            Per: res.Permissions,
            loading: false,
            list,
          });
        })
        .catch(error => {
          console.log('error:', error);
          this.setState({ loading: false });
        });
    });
  };
  formatDate = _date => {
    // if (!date || !date.includes('/')) {
    //   return;
    // }
    var date = _date.toString();
    var yyyy = date.substring(0, 4);
    var MM = date.substring(4, 6);
    var dd = date.substring(6, 8);
    return `${yyyy}-${MM}-${dd}`;
    // let arr = date.split('/');
    // return `${arr[2]}-${arr[1]}-${arr[0]}`;
  };
  formatDateToFetchApi = (date, dayOfMonth) => {
    if (!date || !date.includes('-')) {
      return '';
    }
    let arr = date.split('-');
    return `${arr[0]}${arr[1]}${dayOfMonth}`;
  };
  formatDateToRender = date => {
    if (!date || !date.includes('-')) {
      return '';
    }
    let arr = date.split('-');
    return `${arr[2]}/${arr[1]}/${arr[0]}`;
  };

  searchFilterFunction = text => {
    this.setState({ employeeData: [] });
    this.state.search = text;
  };

  onPressback() {
    Actions.TimeKeepings();
  }
  YearList = () => {
    return this.state.listYear.map((x, i) => {
      return <Picker.Item label={x.text} key={i} value={x.value} />;
    });
  };
  callBackGetYear = value => {
    const { current } = this.state;
    let _current = moment(new Date()).format('YYYY-MM-DD');
    if (current) {
      _current = value + current.slice(4, 10);
    }
    this.setState({ currentYear: value, current: _current }, () => {
      this.viewTimeKeeperListAll();
    });
  };

  showModal = day => {
    this.setState({
      showModal: true,
      dataItem: day,
    });
  };
  openAcctionViewDetail(val) {
    this.setState({ showModal: true, dataItem: val });
  }
  close = () => {
    this.setState({ showModal: false, dataItem: { date: {}, item: {} } });
  };
  handleChangeMonth = date => {
    const { currentYear } = this.state;
    let year = date.year;
    if (year !== currentYear) {
      if (year > currentYear) {
        this.setState({ currentYear: year, current: `${year}-01-01` }, () => {
          this.viewTimeKeeperListAll();
        });
        return;
      } else {
        this.setState({ currentYear: year, current: `${year}-12-01` }, () => {
          this.viewTimeKeeperListAll();
        });
        return;
      }
    }
    this.setState({ current: date.dateString }, () => {
      this.viewTimeKeeperListAll();
    });
  };
  getDiffHour = (timeActual, timeRegular, type) => {
    let duration = moment.duration(
      moment(timeActual, 'HH:mm:ss').diff(moment(timeRegular, 'HH:mm:ss')),
    );
    let res = parseInt(duration.asMinutes());

    alert(timeActual);
    if (type === 'in') {
      if (timeActual > 0) {
        this.timeIn = 'Vào sớm';
      } else {
        this.timeIn = 'Vào trễ';
      }
    }
    if (type === 'out') {
      if (timeActual > 0) {
        this.timeOut = 'Ra sớm';
      } else {
        this.timeOut = 'Ra trễ';
      }
    }
    return this.convertMinutesToHoursAndMinutes(Math.abs(res));
  };
  convertMinutesToHoursAndMinutes = n => {
    var num = n;
    var hours = num / 60;
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    if (+rhours > 0) {
      return `${rhours}h${rminutes}p`;
    }
    return `${rminutes}p`;
  };
  render() {
    let today = moment(new Date()).format('YYYY-MM-DD');
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <Container>
        {console.log('rerender')}
        <View
          style={[
            AppStyles.container,
            { flex: 1, justifyContent: 'space-between' },
          ]}>
          <TabBar
            title={'Nhật ký chấm công'}
            BackModuleByCode={'HR'}
            listYear={this.state.listYear}
            currentYear={this.state.currentYear}
            callBackGetYear={this.callBackGetYear}
          />
          <ScrollView>
            <Calendar
              ref={ref => {
                this.calendar = ref;
              }}
              current={new Date(this.state.current)}
              minDate={'2020-01-01'}
              maxDate={'2030-12-31'}
              onMonthChange={month => {
                this.handleChangeMonth(month);
              }}
              hideExtraDays={true}
              firstDay={1}
              onPressArrowLeft={subtractMonth => subtractMonth()}
              onPressArrowRight={addMonth => addMonth()}
              disableAllTouchEventsForDisabledDays={true}
              renderHeader={date => {
                return (
                  <View>
                    <Text>{`Tháng ${new Date(date).getMonth() + 1}`}</Text>
                  </View>
                );
              }}
              enableSwipeMonths={true}
              dayComponent={({ date, state }) => {
                let IS_SUNDAY = new Date(date.timestamp).getDay() === 0;
                let IS_TODAY = date.dateString === today ? true : false;
                let IS_CHECKIN = this.state.ListAll[date.dateString]?.status
                  ? true
                  : false;
                let IS_PASS =
                  FuncCommon.getDiffDayCompare(
                    new Date(date.dateString),
                    new Date(today),
                  ) >= 0
                    ? true
                    : false;
                return (
                  <TouchableOpacity
                    onPress={() => {
                      this.openAcctionViewDetail({
                        date,
                        item: this.state.ListAll[date.dateString]
                          ? this.state.ListAll[date.dateString]
                          : null,
                      });
                    }}
                    style={[styles.dayContainer]}>
                    <View
                      style={{
                        textAlign: 'center',
                        color: IS_SUNDAY
                          ? AppColors.gray
                          : IS_TODAY
                            ? 'white'
                            : 'black',
                        fontWeight: '500',
                        fontSize: 18,
                        backgroundColor: IS_TODAY
                          ? AppColors.Maincolor
                          : 'white',
                        borderRadius: IS_TODAY ? 50 : 0,
                        width: 27,
                        height: 27,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text>{date.day}</Text>
                    </View>
                    <View
                      style={[
                        styles.markDot,
                        {
                          backgroundColor: IS_CHECKIN
                            ? 'green'
                            : IS_PASS
                              ? 'red'
                              : 'gray',
                        },
                      ]}
                    />
                  </TouchableOpacity>
                );
              }}
            />
          </ScrollView>
          {this.state.Per !== 1 ? null : (
            <View style={{ alignItems: 'flex-end' }}>
              <TouchableOpacity
                style={{
                  backgroundColor: AppColors.Maincolor,
                  padding: 10,
                  marginRight: 10,
                  borderRadius: 10,
                }}
                onPress={() => this.ChangeDataView()}>
                <Text style={{ color: '#fff' }}>
                  {this.state.Data_fake
                    ? 'Chuyển sang giờ thực tế'
                    : 'Chuyển sang giờ điều chỉnh'}
                </Text>
              </TouchableOpacity>
            </View>
          )}
          <View>
            <FlatGrid
              itemDimension={100}
              items={this.state.list}
              renderItem={({ item, index }) => (
                <View
                  style={{
                    borderRadius: 10,
                    backgroundColor: '#FFFFFF',
                    height: 70,
                    borderColor: AppColors.Maincolor,
                    borderWidth: 1,
                  }}>
                  <View style={[styles.itemContainerdetail]}>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {
                          fontSize: 12,
                          alignItems: 'center',
                          textAlign: 'center',
                          fontWeight: '700',
                        },
                      ]}>
                      {item.Title}
                    </Text>
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Icon
                          name={item.Icon}
                          type={item.type}
                          size={20}
                          color={AppColors.Maincolor}
                        />
                      </View>
                      <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Text
                          style={{
                            fontSize: 12,
                            fontWeight: 'bold',
                          }}>
                          {item.value}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />
          </View>
          <Modal
            width={0.8}
            visible={this.state.showModal}
            onTouchOutside={this.close}
            onHardwareBackPress={this.close}
            modalTitle={
              <ModalTitle title={'Thông tin chấm công'} align="center" />
            }
            modalAnimation={
              new SlideAnimation({
                slideFrom: 'bottom',
              })
            }>
            <ModalContent>
              <View
                style={{
                  padding: 10,
                  marginTop: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    { padding: 5, textAlign: 'center' },
                  ]}>
                  {this.state.dataItem?.date.dateString &&
                    `${dayNames[
                    new Date(this.state.dataItem.date.timestamp).getDay()
                    ]
                    } ngày ${this.formatDateToRender(
                      this.state.dataItem.date.dateString,
                    )}`}
                </Text>
                {this.state.dataItem?.item?.CheckIn ||
                  this.state.dataItem?.item?.CheckOut ? (
                  <View>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { padding: 5, textAlign: 'left' },
                      ]}>
                      Lần 1 - {`${this.state.dataItem?.item?.CheckIn || ''}`}
                    </Text>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { padding: 5, textAlign: 'left' },
                      ]}>
                      Lần 2 - {`${this.state.dataItem?.item?.CheckOut || ''}`}
                    </Text>
                    {/* {this.state.dataItem?.item?.StartTimeWorking === "" || this.state.dataItem?.item?.EndTimeWorking === "" ?
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            { padding: 5, textAlign: 'center' },
                          ]}>
                          Thông tin ca làm việc của bạn chưa đầy đủ dữ liệu
                        </Text>
                        : */}
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { padding: 5, textAlign: 'center' },
                      ]}>
                      {/* {this.ViewtimeCheck(this.state.dataItem?.item)} */}
                      {/* {`${this.timeIn} ${timeIn}`} - {`${this.timeOut} ${timeOut}`} */}
                    </Text>
                    {/* } */}
                  </View>
                ) : (
                  <Text
                    style={[
                      AppStyles.Labeldefault,
                      { padding: 5, textAlign: 'center' },
                    ]}>
                    Không có thông tin chấm công
                  </Text>
                )}
              </View>
            </ModalContent>
          </Modal>
        </View>
      </Container>
    );
  }
  ViewtimeCheck = item => {
    var string = '';
    if (item.chechInEarly > 0) {
      string +=
        'Vào sớm: ' + this.convertMinutesToHoursAndMinutes(item.chechInEarly);
    } else if (item.chechInLate > 0) {
      string +=
        'Vào muộn: ' + this.convertMinutesToHoursAndMinutes(item.chechInLate);
    }

    if (item.chechOutEarly > 0) {
      string += ' - ';
      string +=
        'Ra sớm: ' + this.convertMinutesToHoursAndMinutes(item.chechOutEarly);
    } else if (item.chechOutLate > 0) {
      string += ' - ';
      string +=
        'Ra muộn: ' + this.convertMinutesToHoursAndMinutes(item.chechOutLate);
    }
    return string;
  };
  clickBack() {
    Actions.app();
  }
  ChangeDataView = () => {
    API_HR.UpdateCheckTimeAdjust({ IdI: [this.state.Data_fake === true ? 0 : 1] })
      .then(rs => {
        this.viewTimeKeeperListAll();
      })
      .catch(error => {
        console.log('error:', error);
      });
  };
  onValueChangeMonth = Month => {
    this.setState({ employeeData: [] });
    this.state.Month = Month;
    this.state.Year = this.state.Year;
    this.page = 1;
  };
  onValueChangeYear = Year => {
    this.setState({ employeeData: [] });
    this.state.Month = this.state.Month;
    this.state.Year = Year;
    this.page = 1;
  };
  editItem(item) {
    // Actions.empviewitem({EmployeeGuid: item.EmployeeGuid});
  }
  addItem() {
    Actions.additem({ EmployeeGuid: '' });
  }

  handleLoadMore = () => {
    if (!this.state.loading && this.TotalRow > this.state.employeeData.length) {
      this.page = this.page + 1; // increase page by 1
    }
  };
  ConvertDateview = data => {
    if (data !== null && data !== undefined && data !== '') {
      return (
        data.toString().substring(6, 8) +
        '/' +
        data.toString().substring(4, 6) +
        '/' +
        data.toString().substring(0, 4)
      );
    } else {
      return "';";
    }
  };
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;

      nStr += '';
      if (nStr.indexOf(',') >= 0) {
        var x = nStr.split(',');
      } else {
        var x = nStr.split('.');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}
const colors = {
  loginGreen: '#347F16',
};
const styles = StyleSheet.create({
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 10,
    backgroundColor: 'white',
  },
  loginContainer: {
    marginTop: 10,
    marginEnd: 10,
    alignItems: 'flex-end',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
  itemContainerdetail: {
    justifyContent: 'center',
    alignItems: 'stretch',
    marginTop: 10,
    flexDirection: 'column',
  },
  dayContainer: {
    padding: 5,
    margin: 0,
    width: SCREEN_WIDTH / 7,
    height: SCREEN_WIDTH / 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  markDot: {
    width: 7,
    height: 7,
    borderRadius: 50,
    marginTop: 2,
  },
  label: { marginBottom: 5 },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TimeKeepingsByMeComponent);
