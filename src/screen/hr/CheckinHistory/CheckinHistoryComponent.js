import React, { Component } from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  Platform,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Header,
  Divider,
  Icon,
  ListItem,
  Badge,
  SearchBar,
} from 'react-native-elements';
import { API_HR, API_TM_TASKS } from '@network';
import TabBar from '../../component/TabBar';
import Fonts from '../../../theme/fonts';
import { Picker } from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { WebView } from 'react-native-webview';
import { FuncCommon } from '../../../utils';
import Combobox from '../../component/Combobox';
import SearchDay from '../../component/SearchDay';
import moment from 'moment';
import Toast from 'react-native-simple-toast';

const KeyLog = 'LogTimeKeeping';
const SCREEN_WIDTH = Dimensions.get('window').width;
//custom giao diện
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '100',
    fontSize: 30,
    color: '#f6b801',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 2,
    paddingBottom: 5,
    paddingLeft: 0,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
});

class ResignationFormsComponent extends Component {
  constructor(props) {
    super(props);
    //khai báo biến
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      refreshing: false,
      Data: null,
      ListAll: [],
      ListCountNoti: [],
      ChoDuyet: '',
      TraLai: '',
      EvenFromSearch: false,
      ListStatus: [],
    };
    // dữ liệu trueyenf vao APi
    this._search = {
      Search: '',
      RoomId: 'HN',
      RoomName: '',
    };
    this.onEndReachedCalledDuringMomentum = true;
    this.TotalRow = 0;
    this.listtabbarBotom = [
      {
        Title: 'Hà Nội',
        Icon: 'upcircleo',
        Type: 'antdesign',
        Value: 'upcircleo',
        Checkbox: true,
      },
      {
        Title: 'HCM',
        Icon: 'downcircleo',
        Type: 'antdesign',
        Value: 'downcircleo',
        Checkbox: false,
      },
    ];
  }
  // khi trả lại trang,thì load vào danh sách
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.updateSearch('');
    }
  }
  componentDidMount = () => {
    this.getAll();
  };
  //Tìm kiếm
  updateSearch = search => {
    this._search.Search = search;
    this.nextPage(1);
  };
  getAll = async () => {
    let res = [];
    const empGuid = global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid;
    let _d = await AsyncStorage.getItem(KeyLog);
    if (!_d) {
      console.log('Không tìm thấy dữ liệu');
      this.setState({ refreshing: false });
      return;
    }
    let arr = JSON.parse(_d) || [];
    res = arr.filter(day => {
      return (
        FuncCommon.ConDate(
          FuncCommon.ConDate(day.DateTimeRecord.slice(0, 10), 99),
          9,
        ) === this.state.nowString && day.EmployeeGuid === empGuid
      );
    });
    this.setState({ ListAll: res || [], refreshing: false });
  };
  //List danh sách phân trang

  Searchday(value) {
    console.log(value);
    this.setState({ nowString: value }, () => {
      this.getAll();
    });
  }
  // khi kéo danh sách,thì phân trang
  loadMoreData() { }
  _openMenuSearchDate() { }
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.nextPage(1);
    }
  };
  setStartDate = date => {
    this._search.StartDate = date;
  };
  setEndDate = date => {
    this._search.EndDate = date;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  onCallbackValueBottom(value) {
    switch (value) {
      case 'upcircleo':
        this.onChoDuyet();
        break;
      case 'downcircleo':
        this.onDaDuyet();
        break;
      default:
        break;
    }
  }
  _openComboboxType() { }
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  addTimeKeeper = item => {
    API_TM_TASKS.TimeKeeperLogs_addFix([item])
      .then(rs => {
        Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
        if (rs.data.errorCode === 200) {
        }
      })
      .catch(error => {
        console.log('Lỗi chấm công :' + error);
      });
  };
  // view hiển thị
  CustomeListAll = data => (
    <View style={{ flex: 30 }}>
      <FlatList
        data={data}
        style={{ height: '100%', marginBottom: 20 }}
        renderItem={({ item, index }) => (
          <View style={{ fontSize: 11, fontFamily: Fonts.base.family }}>
            <ListItem
              subtitle={() => {
                return (
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: Platform.OS === 'ios' ? -10 : -25,
                    }}>
                    <View style={{ flex: 3 }}>
                      <Text style={AppStyles.Textdefault}>
                        {`Lần ${index + 1}: ${item.TimeOnlyRecord}`}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        paddingRight: 5,
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                      }}>
                      <Icon
                        onPress={() => {
                          if (item.check) {
                            Toast.showWithGravity(
                              'Bạn đã chấm công thành công thời gian này',
                              Toast.SHORT,
                              Toast.CENTER,
                            );
                            return;
                          }

                          this.addTimeKeeper(item);
                        }}
                        name={'enviromento'}
                        type="antdesign"
                        color={AppColors.ColorEdit}
                        size={20}
                      />
                    </View>
                  </View>
                );
              }}
              bottomDivider
              //chevron
              onPress={() => { }}
            />
          </View>
        )}
        // onEndReachedThreshold={0.5}
        // onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
        // onEndReached={() => this.loadMoreData()}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={this.ListEmpty}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
      {/* hiển thị nút tuỳ chọn */}
    </View>
  );
  render() {
    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Lịch sử'}
            // FormSearch={true}
            // CallbackFormSearch={callback =>
            //   this.setState({ EvenFromSearch: callback })
            // }
            // addForm={true}
            // CallbackFormAdd={() => Actions.AddMeetingCalendars()}
            // backToHome={true}
            BackModuleByCode={'MyProfile'}
          //addForm={'AddResignationForms'}
          />
          <Divider />
          <SearchDay CallBackValue={value => this.Searchday(value)} />
          <View>
            {this.state.EvenFromSearch == true ? (
              <View style={{ flexDirection: 'column' }}>
                <TouchableOpacity
                  style={[
                    AppStyles.FormInput,
                    { marginHorizontal: 10, marginTop: 5 },
                  ]}
                  onPress={() => this.onActionComboboxType()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this._search.RoomName
                        ? { color: 'black' }
                        : { color: AppColors.gray },
                    ]}>
                    {this._search.RoomName
                      ? this._search.RoomName
                      : 'Tìm kiếm...'}
                  </Text>
                  <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <Icon
                      color={AppColors.gray}
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{ backgroundColor: '#e1ecf4' }}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={search => this.updateSearch(search)}
                  value={this._search.Search}
                />
              </View>
            ) : null}
          </View>
          <View style={{ flex: 10 }}>
            {this.state.ListAll &&
              this.state.ListAll.length > 0 &&
              this.CustomeListAll(this.state.ListAll)}
          </View>

          {this.state.ListStatus.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeType}
              data={this.state.ListStatus}
              nameMenu={'Chọn trạng thái'}
              eOpen={this.openComboboxType}
              position={'bottom'}
              value={undefined}
            />
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this._search.RoomId = rs.value;
    this._search.RoomName = rs.text;
    this.updateSearch('');
  };
  ListEmpty = () => {
    if (this.state.ListAll && this.state.ListAll.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={[AppStyles.centerAligned, { marginTop: 10 }]}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  onPressHome() {
    Actions.home();
  }
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this.state.ListAll = [];
    this._search.RoomId = 'HN';
    this.setState({
      ListAll: this.state.ListAll,
      selectedTab: 'upcircleo',
    });
    this.nextPage(1);
  };
  // sự kiện chờ duyệt
  onDaDuyet = () => {
    this.state.ListAll = [];
    this._search.RoomId = 'HCM';
    this.setState({
      ListAll: this.state.ListAll,
      selectedTab: 'downcircleo',
    });
    this.nextPage(1);
  };
  // sự kiện ko duyệt
  // onKhongDuyet = () => {
  //   this.state.ListAll = [];
  //   this._search.CurrentPage = 0;
  //   this._search.Search = '';
  //   this._search.Status = 'R';
  //   this.setState({
  //     ListAll: this.state.ListAll,
  //     CurrentPage: this._search.CurrentPage,
  //     Status: this._search.Status,
  //     selectedTab: 'export2',
  //   });
  //   this.nextPage(1);
  // };
  //view item,click sang view khác
  onViewItem(id) {
    Actions.GetMeetingCalendars({ RecordGuid: id });
  }

  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }
  renderLeftMenu = () => {
    return (
      <View>
        <Icon
          style={{ color: 'black' }}
          name={'chevron-thin-left'}
          type="entypo"
          size={25}
          onPress={() => Actions.home()}
        />
      </View>
    );
  };
  renderRightMenu = () => {
    return (
      <View>
        <Icon
          style={{ color: 'black' }}
          name={'plus'}
          type="antdesign"
          size={30}
          onPress={() => Actions.AddResignationForms()}
        />
      </View>
    );
  };
  // xóa tìm kiếm
  onclickreturnSearch() {
    this._search.Search = '';
    this.updateSearch('');
  }
  //
  onValueChange(item) {
    console.log('============>>' + item);
    switch (item) {
      case 'Status': {
        break;
      }
      case 'StartDate': {
        break;
      }
      case 'EndDate': {
        break;
      }
      default: {
      }
    }
  }

  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  //Load lại
  _onRefresh = () => {
    this.getAll();
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(ResignationFormsComponent);
