import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { Avatar, Badge, Icon, withBadge } from 'react-native-elements'

class test extends Component {

    render() {
        return (
            <View>
                <Avatar
                    rounded
                    source={{
                        uri: 'https://randomuser.me/api/portraits/men/41.jpg',
                    }}
                    size="large"
                />
                <Badge
                    status="error"
                    value="99+"
                    containerStyle={{ position: 'absolute',left:50}}
                />
            </View>
        );
    }
}


const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(test);
