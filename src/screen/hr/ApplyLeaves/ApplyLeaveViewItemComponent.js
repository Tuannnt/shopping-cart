import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  Image,
  FlatList,
  Keyboard,
  RefreshControl,
  Dimensions,
} from 'react-native';
// import Icon from "react-native-vector-icons/FontAwesome";
import { AppStyles, AppSizes, AppColors } from '@theme';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Header, Icon, Divider, Input, SearchBar } from 'react-native-elements';
import {
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Form,
  Item,
  Label,
  Picker,
  DatePicker,
} from 'native-base';
import { connect } from 'react-redux';
import { API_HR, API_ApplyLeaves } from '@network';
import { ErrorHandler } from '@error';
import _ from 'lodash';
//import Icon from 'react-native-vector-icons/AntDesign';
import Dialog from 'react-native-dialog';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottom from '../../component/TabBarBottom';
import LoadingComponent from '../../component/LoadingComponent';
import Toast from 'react-native-simple-toast';
import { FuncCommon } from '../../../utils';

class ApplyLeaveViewItemComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      check: false,
      txtComment: '',
      checkLogin: '',
      status: null,
      ApplyLeaveGuid: null,
      StartTime: null,
      EndTime: null,
      Type: null,
      Description: null,
      Total: null,
      FullName: null,
      loading: false,
      ApplyLeaveItem: [],
      listWofflowData: [],
      isModalVisible: false,
      isModalDelete: false,
      visible: false,
      TongNgayNghi: 0,
      PhepConLai: '',
      SoNgayNghi: '',
      Dropdown: [
        {
          value: 'PN',
          label: 'Phép năm',
        },
        {
          value: 'NO',
          label: 'Nghỉ chế độ',
        },
        {
          value: 'KL',
          label: 'Nghỉ không lương',
        },
        {
          value: 'CN',
          label: 'Nghỉ chế độ con nhỏ',
        },
      ],
    };
  }

  employeeList = () => {
    return this.state.ListEmployees.map((x, i) => {
      return <Picker.Item label={x.text} key={i} value={x.value} />;
    });
  };
  componentDidMount = () => {
    if (this.props.RecordGuid != '' && this.props.RecordGuid != null) {
      this.GetApplyLeavesById(this.props.RecordGuid);
    } else if (this.props.RecordGuid != '' && this.props.RecordGuid != null) {
      this.GetApplyLeavesById(this.props.RecordGuid);
    } else {
      this.setState({ loading: false });
    }
    this.getApplyLeavesTypes();
  };
  getApplyLeavesTypes = () => {
    // API_ApplyLeaves.getApplyLeavesTypes()
    //   .then(response => {
    //     console.log(response);
    //     let data = response.data;
    //     data = data.map(item => ({
    //       value: item.value,
    //       label: item.text,
    //     }));
    //     this.setState({Dropdown: data});
    //   })
    //   .catch(error => {
    //     console.log(error.data);
    //   });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      if (this.props.RecordGuid != '' && this.props.RecordGuid != null) {
        this.GetApplyLeavesById(this.props.RecordGuid);
      } else if (this.props.RecordGuid != '' && this.props.RecordGuid != null) {
        this.GetApplyLeavesById(this.props.RecordGuid);
      } else {
        this.setState({ loading: false });
      }
    }
  }
  setSelectedTab = tab => {
    this.setState({ selectedTab: tab });
  };
  renderLeftComponent = () => {
    let leftIcon = 'ios-arrow-round-back';
    let leftIconSet = 'ionicon';
    return (
      <TouchableOpacity
        style={[
          styles.squareContainer,
          { alignItems: 'center', justifyContent: 'center' },
        ]}
        onPress={() => this.callBackList()}>
        <Icon name={leftIcon} type={leftIconSet} color="black" size={30} />
      </TouchableOpacity>
    );
  };
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back_ApplyLeaves',
      ActionTime: new Date().getTime(),
    });
  }
  renderRightComponent = () => { };
  renderTitle = () => {
    let title = 'Thông tin phiếu nghỉ phép';
    return (
      <View
        style={{
          alignItems: 'center',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text
          style={[
            AppStyles.headerStyle,
            { fontWeight: 'bold', marginLeft: 0, fontSize: 18, color: 'black' },
          ]}>
          {title}
        </Text>
      </View>
    );
  };
  onPressback() {
    Keyboard.dismiss();
    Actions.pop();
    setTimeout(() => {
      Actions.refresh({
        moduleId: 'Back',
        ActionTime: new Date().getTime(),
      });
    }, 100);
  }
  keyExtractor = (item, index) => index.toString();

  renderItem = ({ item }) => {
    return (
      <ListItem avatar>
        <Left>
          <Thumbnail
            style={{ width: 40, height: 40 }}
            source={API_HR.GetPicApplyLeaves(item.EmployeeAskGuid)}
          />
        </Left>
        <Body style={{ marginTop: -3 }}>
          <Text note numberOfLines={1}>
            Mã phiếu: {item.ApplyLeaveId}
          </Text>
          <Text note numberOfLines={1}>
            Từ ngày: {item.StartTimeString}
          </Text>
          <Text note numberOfLines={1}>
            Đến ngày: {item.EndTimeString}
          </Text>
          {/* <Text note numberOfLines={1}>
            Ý kiến: {item.Comment}
          </Text> */}
        </Body>
      </ListItem>
    );
  };
  handleSubmitConfirm = value => {
    if (value === 'H') {
      return;
    }
    let _obj = {
      ...this.state.ApplyLeaveItem,
      LeaveReason: value,
    };
    let auto = { IsEdit: true };
    let _data = new FormData();
    _data.append('ApplyLeaves', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    API_HR.UpdateApplyLeavesTask(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity('Xử lý thành công', Toast.SHORT, Toast.CENTER);
          this.onPressback();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  openAttachments() {
    var obj = {
      ModuleId: '16',
      RecordGuid: this.props.RecordGuid || this.props.RowGuid,
      notEdit: this.state.checkLogin === '1' ? false : true,
    };
    Actions.attachmentComponent(obj);
  }
  getStatus = value => {
    const { Dropdown } = this.state;
    if (!value) {
      return '';
    }
    let res = Dropdown.findIndex(i => i.value == value);
    if (res === -1) {
      return '';
    }
    return Dropdown[res].label;
  };
  CheckTotalApp = _obj => {
    API_HR.CheckTotalApp(_obj)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        let { SoNgayNghi } = _data;
        this.setState({ SoNgayNghi });
      })
      .catch(error => console.log(error));
  };
  CheckApplyLeaveInYear = _obj => {
    API_ApplyLeaves.ApplyLeavesInYear(_obj)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        let { PhepConLai } = _data;
        this.setState({ PhepConLai });
      })
      .catch(error => console.log(error));
  };
  render() {
    const isEdit = this.state.ApplyLeaveItem.PermissEdit === 1 ? true : false;
    const IS_NV = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'NV';
    let isReceiverAccepted = false;
    if (
      this.state.ApplyLeaveItem.ReceiverTaskGuid != '' &&
      this.state.ApplyLeaveItem.LeaveReason === 'C'
    ) {
      isReceiverAccepted = true;
    }
    if (
      this.state.ApplyLeaveItem.ReceiverTaskGuid == null ||
      this.state.ApplyLeaveItem.ReceiverTaskGuid == undefined ||
      this.state.ApplyLeaveItem.ReceiverTaskGuid == ''
    ) {
      isReceiverAccepted = true;
    }

    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <View style={{ backgroundColor: '#fff', flex: 1 }}>
        <TabBar_Title
          title={'Xem chi tiết phiếu nghỉ phép'}
          callBack={() => this.callBackList()}
        />
        <ScrollView>
          <View style={{ flexDirection: 'column', flex: 1 }}>
            {/* Hình ảnh chân dung và tên */}
            <View style={{ padding: 10 }}>
              {/*<Text style={{fontWeight:'bold',paddingBottom:3,paddingTop:5}}>Thông tin chung</Text>*/}
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Labeldefault]}>Mã phiếu :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { fontWeight: 'bold' }]}>
                    {this.state.ApplyLeaveItem.ApplyLeaveId}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Labeldefault]}>Người tạo :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.CreatedBy
                      ? this.state.ApplyLeaveItem.CreatedBy.split('#')[1]
                      : ''}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.DepartmentName}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    Nhân viên xin nghỉ :
                  </Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.EmployeeAskName}
                  </Text>
                </View>
              </View>
              {/* <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 2}}>
                  <Text style={AppStyles.Labeldefault}>
                    Người nhận bàn giao:
                  </Text>
                </View>
                <View style={{flex: 3}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.ReceiverTaskName}
                  </Text>
                </View>
              </View> */}
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Từ ngày :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.StartTimeRead +
                      ' ' +
                      (this.state.ApplyLeaveItem.StatusStartTime == 'A'
                        ? '(Cả ngày)'
                        : '(Nửa ngày)')}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Đến ngày :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.EndTimeRead +
                      ' ' +
                      (this.state.ApplyLeaveItem.StatusEndTime == 'A'
                        ? '(Cả ngày)'
                        : '(Nửa ngày)')}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Tổng số ngày :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.Total}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Số ngày đã nghỉ :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.SoNgayNghi}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Phép còn lại :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.PhepConLai}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Hình thức :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.getStatus(this.state.ApplyLeaveItem.Type)}
                  </Text>
                </View>
              </View>
              {/* <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 2}}>
                  <Text style={AppStyles.Labeldefault}>Xác nhận BG:</Text>
                </View>
                <View style={{flex: 3}}>
                  {this.state.ApplyLeaveItem.LeaveReason === 'C' ? (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.AcceptColor},
                      ]}>
                      {'Nhận bàn giao'}
                    </Text>
                  ) : (
                    <Text
                      style={[AppStyles.Textdefault, {color: AppColors.red}]}>
                      {'Không nhận BG'}
                    </Text>
                  )}
                </View>
              </View> */}
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  {this.state.Status == 'Y' ? (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { color: AppColors.AcceptColor },
                      ]}>
                      {this.state.ApplyLeaveItem.StatusWF || ''}
                    </Text>
                  ) : (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { color: AppColors.PendingColor },
                      ]}>
                      {this.state.ApplyLeaveItem.StatusWF || ''}
                    </Text>
                  )}
                </View>
              </View>
              {/* <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Nơi nghỉ:</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.Location}
                  </Text>
                </View>
              </View> */}
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Lý do:</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.Description}
                  </Text>
                </View>
              </View>
              {/* <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 2}}>
                  <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
                </View>
                <View style={{flex: 3}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.ApplyLeaveItem.Description}
                  </Text>
                </View>
              </View> */}
            </View>
          </View>
        </ScrollView>
        <View style={{ maxHeight: 50 }}>
          {this.state.checkInLogin !== '' ? (
            <TabBarBottom
              //key để quay trở lại danh sách
              onDelete={() => this.DeleteApplyLeavesById()}
              callbackOpenUpdate={this.callbackOpenUpdate}
              backListByKey="ApplyLeaves"
              keyCommentWF={{
                ModuleId: 37,
                RecordGuid: this.props.RowGuid || this.props.RecordGuid,
                Title: 'Phiếu nghỉ phép',
              }}
              Title={this.state.ApplyLeaveItem.ApplyLeaveId}
              //kiểm tra quyền xử lý
              Permisstion={this.state.ApplyLeaveItem.Permisstion}
              //kiểm tra bước đầu quy trình
              checkInLogin={this.state.checkLogin}
              onAttachment={() => this.openAttachments()}
              onSubmitWF={
                // isReceiverAccepted
                //   ? (callback, CommentWF) =>
                //       this.submitWorkFlow(callback, CommentWF)
                //   : undefined
                // bat dieu kien nguoi xac nhan ban giao
                (callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
              }
              isHaveConfirm={
                this.state.ApplyLeaveItem.ReceiverTaskGuid ===
                  global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid
                  ? true
                  : false
              }
              btnsConfirm={[
                {
                  name: 'Nhận BG',
                  color: '#3366CC',
                  value: 'C',
                },
                {
                  name: 'Không nhận BG',
                  color: '#EE0000',
                  value: 'K',
                },
                {
                  name: 'Hủy',
                  color: '#979797',
                  value: 'H',
                },
              ]}
              handleConfirm={this.handleSubmitConfirm}
            />
          ) : null}
        </View>
      </View>
    );
  }
  convertTextToUpperCase(text) {
    return text.toLowerCase();
  }
  onValueChangeGroup(value) {
    this.setState({
      EmployeeAskGuid: value,
    });
  }
  onValueChangeType(value) {
    this.setState({ Type: value });
  }
  setStartDate(startDate) {
    let TotalStart =
      Math.floor((this.state.EndTime - startDate) / (1000 * 60 * 60 * 24)) + 1;
    if (TotalStart < 0 || TotalStart == NaN) {
      TotalStart = 0;
    }
    this.setState({ Total: TotalStart, StartTime: startDate });
    console.log('Total là1:' + TotalStart);
  }
  setEndDate(endDate) {
    let TotalStart =
      Math.floor((endDate - this.state.StartTime) / (1000 * 60 * 60 * 24)) + 1;
    if (TotalStart < 0) {
      TotalStart = 0;
    }
    this.setState({ Total: TotalStart, EndTime: endDate });
    console.log('Total là444:' + TotalStart);
  }
  submitWorkFlow(callback, CommentWF) {
    if (callback !== 'D') {
      this.saveKhongduyet(CommentWF);
      return;
    }
    let obj = {
      RowGuid: this.state.ApplyLeaveItem.RowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
      DepartmentGuid: this.state.ApplyLeaveItem.DepartmentGuid,
      WorkFlowGuid: this.state.ApplyLeaveItem.WorkFlowGuid,
    };
    this.setState({ loading: true });
    API_HR.approveApplyLeaves(obj)
      .then(response => {
        let errorCode = response.data.errorCode;
        if (errorCode == 200) {
          Toast.showWithGravity('Xử lý thành công', Toast.SHORT, Toast.CENTER);
          this.onPressback();
        } else {
          Toast.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error);
        Toast.showWithGravity(
          'Có lỗi khi xử lý phiếu',
          Toast.SHORT,
          Toast.CENTER,
        );
      });
  }

  onClick(data) {
    this.setState({ selectedTab: data, check: false });
    if (data == 'home') {
      Actions.app();
    } else if (data == 'back') {
      Actions.applyLeaves();
    } else if (data == 'ok' || data == 'no') {
      this.setState({ isModalVisible: !this.state.isModalVisible });
    } else if (data == 'delete') {
      this.setState({ isModalDelete: !this.state.isModalDelete });
    } else if (data == 'comment') {
      Actions.viewBottonHr({ ApplyLeaveGuid: this.props.RecordGuid });
    }
  }
  handleEmail(data) {
    this.setState({ txtComment: data });
  }
  callbackOpenUpdate = () => {
    Actions.applyLeavesadd({ itemData: this.state.ApplyLeaveItem });
  };

  DeleteApplyLeavesById = () => {
    let obj = {
      RowGuid: this.props.RecordGuid,
    };
    this.setState({ loading: true });
    API_HR.DeleteApplyLeavesById(obj)
      .then(response => {
        let errorCode = JSON.parse(JSON.stringify(response.data.errorCode));
        console.log('==============ketquaXoa-errorCode: ' + errorCode);
        if (errorCode == 200) {
          this.setState({
            loading: false,
            isModalDelete: !this.state.isModalDelete,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressback();
        } else {
          Toast.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error);
      });
  };

  saveKhongduyet(CommentWF) {
    let obj = {
      RowGuid: this.state.ApplyLeaveItem.RowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
      WorkFlowGuid: this.state.ApplyLeaveItem.WorkFlowGuid,
    };
    this.setState({ loading: true });
    API_HR.NotApproveApplyLeaves(obj)
      .then(response => {
        console.log(
          '==============ketquaKhongduỵet: ' +
          JSON.stringify(response.data.Data),
        );
        let errorCode = JSON.parse(JSON.stringify(response.data.errorCode));
        console.log('==============ketquaduỵet-errorCode: ' + errorCode);
        if (errorCode == 200) {
          Toast.showWithGravity(
            'Trả lại thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressback();
        } else {
          Toast.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error);
        Toast.showWithGravity(
          'Có lỗi khi xử lý phiếu',
          Toast.SHORT,
          Toast.CENTER,
        );
      });
  }
  closeComment() {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  closeDelete() {
    this.setState({ isModalDelete: !this.state.isModalDelete });
  }
  GetApplyLeavesById = Id => {
    const IS_NV = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'NV';
    let obj = {
      RowGuid: Id,
    };
    this.setState({ loading: true });
    API_HR.GetApplyLeavesById(obj)
      .then(response => {
        let data = JSON.parse(JSON.parse(JSON.stringify(response.data)).data);
        this.setState({
          Status: data.Status,
        });
        if (data.Type == 'NL') {
          data.TypeTitle = 'Nghỉ có lương';
        } else {
          data.TypeTitle = 'Nghỉ không lương';
        }
        data.CreatedDateRead = this.getParsedDate(data.CreatedDate, 0);
        data.StartTimeRead = this.getParsedDate(data.StartTime, 0);
        data.EndTimeRead = this.getParsedDate(data.EndTime, 0);
        let StartTime = new Date(data.StartTime);
        let EndTime = new Date(data.EndTime);
        data.StartTime = StartTime;
        data.EndTime = EndTime;
        if (data.EmployeeAskGuid) {
          let _obj = {
            Id: data.EmployeeAskGuid,
            StartDate: FuncCommon.ConDate(StartTime, 2),
          };
          let q = {
            EmployeeAskGuid: data.EmployeeAskGuid,
          };
          Promise.all([
            this.CheckTotalApp(q),
            this.CheckApplyLeaveInYear(_obj),
          ]);
        }
        this.setState({
          ApplyLeaveItem: data,
          StartTime: StartTime,
          EndTime: EndTime,
          Total: data.Total,
          Type: data.Type,
          Description: data.Description,
          loading: false,
        });
        if (IS_NV) {
          this.getAllCommentPage(this.page);
        }
        let objCheck = {
          RowGuid: this.props.RecordGuid,
          WorkFlowGuid: this.state.ApplyLeaveItem.WorkFlowGuid,
        };
        API_HR.CheckLogin(objCheck)
          .then(response => {
            this.setState({
              checkLogin: response.data.data,
              loading: false,
            });
          })
          .catch(error => {
            this.setState({ loading: false });
          });
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  };
  getAllCommentPage = Page => {
    this.page = Page;
    let obj = {
      Page: Page,
      ItemPage: this.Length,
      EmployeeAskGuid: this.state.ApplyLeaveItem.EmployeeAskGuid,
    };
    this.setState({ loading: true });

    API_HR.CountApplyLeavesInYear(obj)
      .then(response => {
        console.log(
          '==============ketquaBanghi: ' +
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
          ),
        );
        let listData = this.state.listWofflowData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(response.data)).data),
          ),
        );
        let data = listData.concat(data1);
        this.setState({ listWofflowData: data, loading: false });
        this.arrayholder = data;
        let TotalRow = JSON.parse(
          JSON.parse(JSON.stringify(response.data)).data,
        );
        this.TotalRow = TotalRow.Count;
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error);
      });
  };
  handleLoadMore = () => {
    if (
      !this.state.loading &&
      this.TotalRow > this.state.listWofflowData.length
    ) {
      this.page = this.page + 1;
      this.getAllCommentPage(this.page);
    }
  };
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <Text style={{ color: '#000' }} />;
  };
  convertStatus = data => {
    if (data == 'W') {
      return 'Chờ duyệt';
    } else if (data == 'R') {
      return 'Trả lại';
    } else {
      return 'Đã duyệt';
    }
  };
  getParsedDate(strDate, data) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!
      var hours = date.getHours();
      var min = date.getMinutes();
      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (data == '1') {
        // "1":  dd-mm-yyy HH:mm
        if (hours < 10) {
          hours = '0' + hours;
        }
        if (min < 10) {
          min = '0' + min;
        }
        date = dd + '-' + mm + '-' + yyyy + ' ' + hours + ':' + min;
        return date.toString();
      } else {
        date = dd + '-' + mm + '-' + yyyy;
        return date.toString();
      }
    } else {
      return '';
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ApplyLeaveViewItemComponent);
