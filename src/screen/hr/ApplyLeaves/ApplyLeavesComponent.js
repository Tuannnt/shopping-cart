import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  Dimensions,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {SearchBar, ListItem, Icon as IconElement} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_HR, API_ApplyLeaves} from '@network';
import {ErrorHandler} from '@error';
import MenuSearchDate from '../../component/MenuSearchDate';
import {AppStyles, AppSizes, AppColors} from '@theme';
import Icon from 'react-native-vector-icons/AntDesign';
import TabBar from '../../component/TabBar';
import {FuncCommon} from '../../../utils';
import Combobox from '../../component/Combobox';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import DatePicker from 'react-native-date-picker';
const SCREEN_WIDTH = Dimensions.get('window').width;
const ListType = [
  {value: 'AL', text: 'Tất cả'},

  {
    value: 'PN',
    text: 'Nghỉ phép',
  },
  {
    value: 'NO',
    text: 'Nghỉ chế độ',
  },
  {
    value: 'KL',
    text: 'Nghỉ không lương',
  },
  {
    value: 'CN',
    text: 'Nghỉ chế độ con nhỏ',
  },
];
class ApplyLeavesComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.componentCurrent = 'ApplyLeavesComponent';
    this.CurrentPage = 0;
    this._search = {
      StartDate: new Date(),
      EndDate: new Date(),
      TypeId: 'AL',
      TypeName: '',
      Status: 'W',
    };
    this.state = {
      EvenFromSearch: false,
      ValueSearchDate: '4', // 30 ngày trước
      name: '',
      loading: false,
      refreshing: false,
      applyLeavesData: [],
      search: '',
      Status: 'W',
      selectedTab: 'home',
      open: false,
      isModalVisible: false,
      selected: false,
      ChoDuyet: '',
      Tralai: '',
      TongNgayNghi: 0,
      TongDaNghiNV: 0,
      PhepConLai: 0,
    };
    this.onEndReachedCalledDuringMomentum = true;
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
  }
  componentDidMount = () => {
    // this.getTotalLeaveInYear();
  };
  getTotalLeaveInYear = () => {
    const {FullName, EmployeeGuid} = global.__appSIGNALR.SIGNALR_object.USER;
    var objLeave = {
      EmployeeGuid: EmployeeGuid,
    };
    API_ApplyLeaves.GetTotalLeaveInYear(objLeave)
      .then(response => {
        let rs = JSON.parse(JSON.parse(JSON.stringify(response.data)).data);
        this.setState({
          TongNgayNghi: rs.TongNgayNghi,
          TongDaNghiNV: rs.TongDaNghiNV,
          PhepConLai: rs.PhepConLai,
          EmployeeGuid,
          FullName,
        });
        this.getAllApplyLeavesPage();
      })
      .catch(error => {
        console.log(error.data);
      });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.searchFilterFunction('');
    }
  }
  convertStatus = data => {
    if (data == 'W') {
      return 'Chờ duyệt';
    } else if (data == 'R') {
      return 'Trả lại';
    } else {
      return 'Đã duyệt';
    }
  };
  keyExtractor = (item, index) => index.toString();

  renderItem = ({item}) => {
    let maxlimit = 50;
    // so ki tu show ra
    return (
      <View>
        <ListItem
          title={() => {
            return (
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={[AppStyles.Titledefault]}>
                    {item.ApplyLeaveId}
                  </Text>
                  <Text
                    style={[
                      {
                        flex: 1,
                        textAlign: 'right',
                        color:
                          item.Status == 'Y'
                            ? AppColors.AcceptColor
                            : AppColors.PendingColor,
                      },
                      AppStyles.Textdefault,
                    ]}>
                    {item.StatusWF}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.EmployeeAskName}
                  </Text>
                  <Text style={[AppStyles.Textdefault]}>
                    Từ ngày: {item.StartTimeString}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.DepartmentName}
                  </Text>

                  <Text style={[AppStyles.Textdefault]}>
                    Đến ngày: {item.EndTimeString}
                  </Text>
                </View>
              </View>
            );
          }}
          bottomDivider
          //chevron
          onPress={() => this.editItem(item)}
        />
      </View>
    );
  };
  _onRefresh = () => {
    this.getAllApplyLeavesPage(1);
  };
  onSelect = data => {
    this.setState(data);
  };
  searchFilterFunction = text => {
    this.setState({applyLeavesData: []});
    this.state.search = text;
    this.getAllApplyLeavesPage(1);
  };
  setSelectedTab = tab => {
    this.setState({selectedTab: tab});
  };

  onPressback() {
    Actions.app();
  }
  addApplyLeaves() {
    Actions.applyLeavesadd();
  }
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (!rs.value) {
      return;
    }
    this._search.TypeId = rs.value;
    this._search.TypeName = rs.text;
    this.getAllApplyLeavesPage(1);
  };
  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  CallbackSearchDate = callback => {
    if (callback.start) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);

      //1
      this.setState(
        {
          ValueSearchDate: callback.value,
        },
        () => {
          this.getAllApplyLeavesPage(1);
        },
      );
    }
  };
  //#region thay đổi thời gian tìm kiếm
  setStartDate = date => {
    this._search.StartDate = date;
  };
  setEndDate = date => {
    this._search.EndDate = date;
  };
  onPressHome() {
    Actions.home();
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'profile':
        this.onClick('no');
        break;
      case 'checkcircleo':
        this.onClick('ok');
        break;
      default:
        break;
    }
  }
  EvenFromSearch() {
    this.setState(prevState => ({EvenFromSearch: !prevState.EvenFromSearch}));
  }
  render() {
    const IS_NV = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'NV';
    return (
      <TouchableWithoutFeedback style={{flex: 1}}>
        <View style={styles.container}>
          <TabBar
            title={'Danh sách nghỉ phép'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            addForm="true"
            backToHome={true}
            BackModuleByCode={'MyProfile'}
            CallbackFormAdd={() => this.addApplyLeaves()}
          />
          <View>
            {this.state.EvenFromSearch ? (
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventStartDate: true})}>
                      <Text>
                        {FuncCommon.ConDate(this._search.StartDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventEndDate: true})}>
                      <Text>{FuncCommon.ConDate(this._search.EndDate, 0)}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'column', padding: 5}}>
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        {textAlign: 'center'},
                        styles.timeHeader,
                      ]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                {/* <TouchableOpacity
                  style={[AppStyles.FormInput, {marginHorizontal: 10}]}
                  onPress={() => this.onActionComboboxType()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this._search.TypeName
                        ? {color: 'black'}
                        : {color: AppColors.gray},
                    ]}>
                    {this._search.TypeName
                      ? this._search.TypeName
                      : 'Chọn loại...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <IconElement
                      color={AppColors.gray}
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity> */}
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={text => this.searchFilterFunction(text)}
                  value={this.state.search}
                />
              </View>
            ) : null}
          </View>

          {/* {IS_NV && (
            <View style={styles.tabHeaderInfo}>
              <View style={{padding: 10, flexDirection: 'column'}}>
                <View style={{alignItems: 'center', marginBottom: 10}}>
                  <Image
                    style={{width: 50, height: 50, borderRadius: 50}}
                    source={API_HR.GetPicApplyLeaves(this.state.EmployeeGuid)}
                  />
                  <Text>{this.state.FullName}</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text>Số phép: </Text>
                    <Text style={{fontWeight: 'bold'}}>
                      {this.state.TongNgayNghi}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text>Đã nghỉ: </Text>
                    <Text style={{fontWeight: 'bold'}}>
                      {this.state.TongDaNghiNV}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text>Còn lại: </Text>
                    <Text style={{fontWeight: 'bold'}}>
                      {this.state.PhepConLai}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          )} */}
          <View style={{flex: 1}}>
            {this.state.applyLeavesData ? (
              <FlatList
                style={{marginBottom: 10}}
                keyExtractor={this.keyExtractor}
                data={this.state.applyLeavesData}
                ListEmptyComponent={this.ListEmpty}
                renderItem={this.renderItem}
                onEndReached={this.handleLoadMore}
                onEndReachedThreshold={0.5}
                onMomentumScrollBegin={() => {
                  this.onEndReachedCalledDuringMomentum = false;
                }}
                refreshControl={
                  <RefreshControl
                    onRefresh={this._onRefresh}
                    refreshing={this.state.loading}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['red', 'green', 'blue']}
                  />
                }
              />
            ) : null}
          </View>
          <View style={[AppStyles.StyleTabvarBottom]}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.onCallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.getAllApplyLeavesPage(1);
                  this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.getAllApplyLeavesPage(1);
                  this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {/* {ListType && ListType.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeType}
              data={ListType}
              nameMenu={'Chọn'}
              eOpen={this.openComboboxType}
              position={'bottom'}
              value={undefined}
            />
          ) : null} */}
        </View>
      </TouchableWithoutFeedback>
    );
  }
  ListEmpty = () => {
    if (this.state.applyLeavesData && this.state.applyLeavesData.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={[AppStyles.centerAligned, {marginTop: 10}]}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  clickBack() {
    Actions.pop();
  }

  editItem(item) {
    Actions.applyLeaveViewItem({
      RecordGuid: item.RowGuid,
    });
  }

  onClick(data) {
    this.CurrentPage = 0;
    this.setState({selectedTab: data});
    if (data == 'ok') {
      this._search.Status = 'Y';
      this.setState({applyLeavesData: []}, () => {
        this.getAllApplyLeavesPage();
      });
    } else if (data == 'no') {
      this._search.Status = 'W';
      this.setState({applyLeavesData: []});

      this.getAllApplyLeavesPage();
    }
  }
  getAll = numberPage => {
    this.setState({loading: true}, () => {
      if (numberPage) {
        this.CurrentPage = numberPage;
      } else {
        this.CurrentPage++;
      }
      let obj = {
        search: {value: this.state.search},
        CurrentPage: this.CurrentPage,
        Length: this.Length,
        Status: this._search.Status,
        Type: this._search.TypeId,
        StartDate: this._search.StartDate,
        EndDate: this._search.EndDate,
        QueryOrderBy: 'CreatedDate DESC',
      };
      API_HR.getAllApplyLeavesPage(obj)
        .then(response => {
          let listData = this.state.applyLeavesData;
          let _data = JSON.parse(response.data.data);
          let data1 = _data.list.data;
          let data = [];
          data = listData.concat(data1);
          if (numberPage) {
            data = data1;
          }
          if (this._search.Status === 'W') {
            this.listtabbarBotom[0].Badge = _data.list.recordsTotal;
          }
          FuncCommon.Data_Offline_Set('ApplyLeaves', data);
          this.setState({applyLeavesData: data, loading: false});
          this.arrayholder = data;
          this.TotalRow = _data.list.Count;
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data);
        });
    });
  };
  getAllApplyLeavesPage = numberPage => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.getAll(numberPage);
      } else {
        let data = await FuncCommon.Data_Offline_Get('ApplyLeaves');
        this.setState({
          applyLeavesData: data,
          loading: false,
        });
      }
    });
  };
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this.state.ListAll = [];
    this._search.CurrentPage = 0;
    this._search.txtSearch = '';
    this._search.Status = 'W';
    this.setState({
      ListAll: this.state.ListAll,
      CurrentPage: this._search.CurrentPage,
      txtSearch: this._search.Search,
      Status: this._search.Status,
    });

    this.getAllApplyLeavesPage();
  };
  handleLoadMore = () => {
    if (this.TotalRow > this.state.applyLeavesData.length) {
      this.getAllApplyLeavesPage();
    }
  };
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <Text style={{color: '#000'}} />;
  };
}
const colors = {
  loginGreen: '#347F16',
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 5,
    paddingVertical: 5,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  loginContainer: {
    marginTop: 10,
    marginEnd: 10,
    alignItems: 'flex-end',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
  tabHeaderInfo: {
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: 'gray',
    marginHorizontal: 30,
    marginVertical: 5,
  },
});
const stylesIcon = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '1%',
    paddingHorizontal: '5%',
  },
  logo: {
    marginTop: '8%',
    alignSelf: 'center',
  },
  loginContainer: {
    marginTop: AppSizes.padding,
    flexDirection: 'row',
    flex: 1,
    backgroundColor: AppColors.white,
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: AppColors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
  textForgotPassword: {
    ...AppStyles.baseText,
    color: AppColors.loginGreen,
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blue,
    width: '40%',
    height: 40,
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ApplyLeavesComponent);
