import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Image,
  TextInput,
  TouchableWithoutFeedback,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import {ListView} from 'deprecated-react-native-listview';
import {connect} from 'react-redux';
import {AppStyles, AppSizes, AppColors} from '@theme';
import {Header, Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Tabbar from 'react-native-tabbar-bottom';
import DocumentPicker from 'react-native-document-picker';
import {WebView} from 'react-native-webview';
import TabBar_Title from '../../component/TabBar_Title';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
} from 'native-base';
import {API_HR} from '@network';
import _ from 'lodash';
import {ErrorHandler} from '@error';

class ViewBottonHrComponent extends Component {
  constructor() {
    super();
    this.page = 1;
    this.Length = 15;
    this.TotalRow = 0;
    this.state = {
      Comment: '',
      LoginName: '',
      loading: false,
      listWofflowData: [],
    };
  }
  componentDidMount() {
    this.getAllCommentPage(this.page);
  }
  onClickBack() {
    Actions.pop();
  }
  sendComment() {
    console.log('Send mess: ' + this.state.Comment);
    let commentInput = this.refs['commentInput'];
    let obj = {
      RecordGuid: this.props.ApplyLeaveGuid,
      Comment: this.state.Comment,
    };
    API_HR.InsertCommentApplyLeaves(obj)
      .then(response => {
        console.log('==============Comment222: ' + this.props.ApplyLeaveGuid);
        this.page = 1;
        this.setState({loading: false, listWofflowData: []});
        this.getAllCommentPage(1);
        commentInput.clear();
        this.state.Comment = '';
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  }
  renderLeftComponent = () => {
    let leftIcon = 'ios-arrow-round-back';
    let leftIconSet = 'ionicon';
    return (
      <TouchableOpacity
        style={[
          styles.squareContainer,
          {alignItems: 'center', justifyContent: 'center'},
        ]}
        onPress={() => this.onPressback()}>
        <Icon name={leftIcon} type={leftIconSet} color="black" size={30} />
      </TouchableOpacity>
    );
  };
  renderRightComponent = () => {};
  renderTitle = () => {
    let title = 'Nội dung trao đổi';
    return (
      <View
        style={{
          alignItems: 'center',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text
          style={[
            AppStyles.headerStyle,
            {fontWeight: 'bold', marginLeft: 0, fontSize: 18, color: 'black'},
          ]}>
          {title}
        </Text>
      </View>
    );
  };
  onPressback() {
    Actions.pop();
    Actions.refresh({moduleId: 'back'});
  }
  keyExtractor = (item, index) => index.toString();
  renderItem = ({item}) => {
    // just standard if statement
    if (item.LoginName == this.state.LoginName) {
      return (
        <ListItem avatar>
          <Body style={styles.boxComment}>
            <Text style={{color: 'white'}} note numberOfLines={1}>
              {item.Comment}
            </Text>
          </Body>
          <Left>
            <Thumbnail
              style={{width: 40, height: 40}}
              source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
            />
          </Left>
        </ListItem>
      );
    }
    return (
      <ListItem avatar>
        <Left>
          <Thumbnail
            style={{width: 40, height: 40}}
            source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
          />
        </Left>
        <Body style={styles.boxComment}>
          <Text style={{color: 'white'}} note numberOfLines={1}>
            {item.Comment}
          </Text>
        </Body>
      </ListItem>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        <Header
          statusBarProps={AppStyles.statusbarProps}
          {...AppStyles.headerProps}
          placement="left"
          leftComponent={this.renderLeftComponent()}
          centerComponent={this.renderTitle()}
          rightComponent={this.renderRightComponent()}
        />
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <Container>
            <FlatList
              style={{marginBottom: 55}}
              keyExtractor={this.keyExtractor}
              data={this.state.listWofflowData}
              refreshing={this.state.loading}
              renderItem={this.renderItem}
              onEndReached={this.handleLoadMore}
              onRefresh={this._onRefresh}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.loading}
                  tintColor="#f5821f"
                  titleColor="#fff"
                  colors={['red', 'green', 'blue']}
                />
              }
            />
          </Container>
        </View>
        {/*<View style={styles.boxBottom}>*/}
        {/*    <TextInput*/}
        {/*        style={{ flex: 1 }}*/}
        {/*        placeholder="Nhập nội dung"*/}
        {/*        onChangeText={(text) => this.setState({Comment : text})}*/}
        {/*        value={this.state.Comment}*/}
        {/*        ref="commentInput"*/}
        {/*    ></TextInput>*/}
        {/*    <View style={{ width: 45, height: 50, textAlign: 'center', justifyContent: "center", }} onPress={() => this.onClickBack()}>*/}
        {/*        <Icon onPress={() => this.sendComment()}*/}
        {/*              name='send'*/}
        {/*              type='Feather'*/}
        {/*        />*/}
        {/*    </View>*/}
        {/*</View>*/}
      </View>
    );
  }

  getAllCommentPage = Page => {
    this.page = Page;
    let obj = {
      Page: Page,
      ItemPage: this.Length,
      RecordGuid: this.props.ApplyLeaveGuid,
    };

    this.setState({loading: true});
    API_HR.getAllCommentPage(obj)
      .then(response => {
        console.log(
          '==============ketquaCommnet: ' +
            JSON.stringify(
              JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
            ),
        );
        let listData = this.state.listWofflowData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(response.data)).data).Data,
          ),
        );
        let data = listData.concat(data1);
        this.setState({listWofflowData: data, loading: false});
        this.arrayholder = data;
        let TotalRow = JSON.parse(
          JSON.parse(JSON.stringify(response.data)).data,
        );
        this.TotalRow = TotalRow.Count;
        this.setState({LoginName: TotalRow.LoginName});
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  handleLoadMore = () => {
    if (
      !this.state.loading &&
      this.TotalRow > this.state.listWofflowData.length
    ) {
      this.page = this.page + 1;
      this.getAllCommentPage(this.page);
    }
  };
  renderFooter = () => {
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerContent: {
    flex: 1,
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  boxSearch: {
    flexDirection: 'row',
  },
  line: {
    borderBottomColor: '#eff0f1',
    borderBottomWidth: 1,
    marginBottom: 3,
    marginTop: 3,
  },
  boxComment: {
    flex: 1,
    padding: 3,
    justifyContent: 'center',
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    margin: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#24c79f',
  },
  boxBottom: {
    flex: 1,
    width: '100%',
    position: 'absolute',
    height: 50,
    bottom: 0,
    borderTopColor: '#eff0f1',
    borderTopWidth: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewBottonHrComponent);
