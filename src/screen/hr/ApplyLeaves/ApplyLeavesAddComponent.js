import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Keyboard,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { Button, Icon, Divider } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { API_ApplyLeaves, API_HR } from '@network';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { FuncCommon } from '../../../utils';
import { AppStyles, AppColors } from '@theme';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';
import controller from './controller';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import listCombobox from './listCombobox';
import { Combobox, RequiredText, TabBar_Title, ComboboxV2 } from '@Component';
import configApp from '../../../configApp';

const TypeOfDay = [
  { label: 'Cả ngày', value: 'A' },
  { label: 'Nửa ngày', value: 'B' },
];
const styleCustomIos =
  Platform.OS === 'ios'
    ? {
      inputIOS: {
        fontSize: 16,
        paddingVertical: 10,
        paddingHorizontal: 10,
        color: 'black',
        paddingRight: 10, // to ensure the text is never behind the icon
      },
    }
    : {};
class ApplyLeavesAddComponent extends Component {
  constructor(props) {
    super(props);
    this.DataProcess = FuncCommon.DataProcess();
    this.loading = false;
    this.state = {
      refreshing: false,
      bankData: [],
      PhepConLai: '',
      search: '',
      selected2: null,
      Date: new Date(),
      StartDate: FuncCommon.ConDate(new Date(), 0),
      EndDate: FuncCommon.ConDate(new Date(), 0),
      EmployeeAskGuid: null,
      ReceiverTaskGuid: null,
      ListEmployees: [],
      ListReceiverTask: [],
      StatusEndTime: 'A',
      StatusStartTime: 'A',
      Type: 'PN',
      Total: '1',
      Attachment: false,
      EmployeeAskGuidName: '',
      ListEmployeesReceiver: [],
      EmployeeAskGuidName: '',
      Attachment: [],
      ListType: [
        {
          value: 'PN',
          label: 'Phép năm',
        },
        {
          value: 'NO',
          label: 'Nghỉ chế độ',
        },
        {
          value: 'KL',
          label: 'Nghỉ không lương',
        },
        {
          value: 'CN',
          label: 'Nghỉ chế độ con nhỏ',
        },
      ],
    };
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }

  componentDidMount() {
    const { itemData } = this.props;
    Promise.all([
      this.getEmployeeReceiverTask(),
      this.getEmployee(),
      // this.getApplyLeavesTypes(),
    ]);
    if (!itemData) {
      this.setState({
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        EmployeeAskGuid: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
      });
      this.getNumberAuto();
    } else {
      this.setState({
        isEdit: false,
        Attachment: itemData.Attachment,
        ApplyLeaveId: itemData.ApplyLeaveId,
        Date: new Date(itemData.Date),
        StartDate:
          itemData.StartTime === '' || itemData.StartTime === null
            ? null
            : FuncCommon.ConDate(new Date(itemData.StartTime), 0),
        EndDate:
          itemData.EndTime === '' || itemData.EndTime === null
            ? null
            : FuncCommon.ConDate(new Date(itemData.EndTime), 0),
        Description: itemData.Description,
        Note: itemData.Note,
        FullName: itemData.EmployeeName,
        DepartmentName: itemData.DepartmentName,
        EmployeeAskGuid: itemData.EmployeeAskGuid,
        ReceiverTaskGuid: itemData.ReceiverTaskGuid,
        StatusEndTime: itemData.StatusEndTime,
        Type: itemData.Type,
        StatusStartTime: itemData.StatusStartTime,
        Location: itemData.Location,
        Total: itemData.Total + '',
      });
    }
  }

  onChangeState = (key, value) => {
    this.setState({ [key]: value }, () => {
      if (key === 'StatusEndTime' || key === 'StatusStartTime') {
        this.countTotal();
      }
    });
  };
  getWeekendCountBetweenDates = (startDate, endDate) => {
    // đếm các ngày CN trong khoảng thời gian đã chọn
    var totalWeekends = 0;
    for (var i = startDate; i <= endDate; i.setDate(i.getDate() + 1)) {
      var a = i.getDay();
      if (i.getDay() == 0) {
        totalWeekends++;
      }
    }
    return totalWeekends;
  };
  countTotal = () => {
    let Total = 0;
    const { EndDate, StartDate } = this.state;
    let TempTotal =
      Number(
        moment
          .duration(
            moment(EndDate, 'DD/MM/YYYY').diff(moment(StartDate, 'DD/MM/YYYY')),
          )
          .asDays(),
      ) + 1;
    if (TempTotal === 0) {
      Total = TempTotal;
    } else {
      if (
        (this.state.StatusStartTime == 'B' ||
          this.state.StatusEndTime == 'B') &&
        this.state.StatusStartTime !== this.state.StatusEndTime
      ) {
        Total = TempTotal - 0.5;
      } else if (
        this.state.StatusStartTime == 'B' &&
        this.state.StatusEndTime == 'B' &&
        this.state.StartTime !== this.state.EndTime
      ) {
        Total = TempTotal - 1;
      } else if (
        this.state.StatusStartTime == 'B' &&
        this.state.StatusEndTime == 'B' &&
        this.state.StartTime === this.state.EndTime
      ) {
        Total = 0.5;
      } else {
        Total = TempTotal;
      }
      var s = FuncCommon.ConDate(this.state.StartDate, 99);
      var e = FuncCommon.ConDate(this.state.EndDate, 99);
      var NumberOfSunDay = this.getWeekendCountBetweenDates(s, e);
      Total = Total - NumberOfSunDay; // bỏ qua các ngày CN không tính vào tổng số ngày nghỉ
      this.setState({ Total: Total + '' });
    }
  };
  onChangeStateAttachment = value => {
    let guid = this.state.EmployeeAskGuid;
    if (!value) {
      guid = global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid;
    }
    this.setState({
      Attachment: value,
      EmployeeAskGuid: guid,
    });
  };
  _openComboboxType() { }
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  _openActionComboboxReceiverTask() { }
  openActionComboboxReceiverTask = d => {
    this._openActionComboboxReceiverTask = d;
  };
  onActionComboboxReceiverTask() {
    this._openActionComboboxReceiverTask();
  }
  render() {
    const { isEdit, ApplyLeaveId, StartDate, EndDate } = this.state;
    let total = '0';
    if (StartDate && EndDate) {
      let res =
        Number(
          moment
            .duration(
              moment(EndDate, 'DD/MM/YYYY').diff(
                moment(StartDate, 'DD/MM/YYYY'),
              ),
            )
            .asDays(),
        ) + 1;
      total = res + '';
    }
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{ flex: 1 }}>
        <TabBar_Title
          title={`${this.props.itemData ? 'Sửa' : 'Thêm'} phiếu nghỉ phép`}
          callBack={() => Actions.pop()}
        />
        <ScrollView>
          <View style={styles.containerContent}>
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>Mã nghỉ phép</Text>
              </View>
              {isEdit && (
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  value={ApplyLeaveId}
                  autoCapitalize="none"
                  onChangeText={Title => this.setState({ ApplyLeaveId: Title })}
                />
              )}
              {!isEdit && (
                <View style={[AppStyles.FormInput]}>
                  <Text style={[AppStyles.TextInput, { color: AppColors.gray }]}>
                    {ApplyLeaveId}
                  </Text>
                </View>
              )}
            </View>
            {/* <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={this.state.DepartmentName}
                  editable={false}
                />
              </View> */}
            {/* <View style={{ padding: 10, paddingBottom: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={AppStyles.Labeldefault}>Người đăng ký</Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, { color: 'black' }]}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={this.state.FullName}
                  editable={false}
                />
              </View> */}
            {/* {
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Xin nghỉ hộ</Text>
                  </View>
                  <Switch
                    trackColor={{false: '#767577', true: '#81b0ff'}}
                    thumbColor={
                      this.state.Attachment ? AppColors.ColorAdd : '#f4f3f4'
                    }
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={Attachment =>
                      this.onChangeStateAttachment(Attachment)
                    }
                    value={this.state.Attachment}
                  />
                </View>
              } */}

            {/* {this.state.Attachment && ( */}
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <Text style={AppStyles.Labeldefault}>
                Nhân viên xin nghỉ <RequiredText />
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.onActionComboboxType();
                }}>
                <Text
                  style={[
                    AppStyles.FormInput,
                    {
                      color: this.state.EmployeeAskGuidName ? 'black' : 'gray',
                      padding: 10,
                    },
                  ]}>
                  {this.state.EmployeeAskGuidName
                    ? this.state.EmployeeAskGuidName
                    : 'Chọn nhân viên...'}
                </Text>
              </TouchableOpacity>
              {/* <View
                    style={{
                      ...AppStyles.FormInputPicker,
                      justifyContent: 'center',
                    }}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value => this.onChangeState('EmployeeAskGuid', value)}
                      items={this.state.ListEmployees || []}
                      placeholder={{}}
                      value={this.state.EmployeeAskGuid}
                      style={styleCustomIos}
                    />
                  </View> */}
            </View>
            {/* <View style={{padding: 10, paddingBottom: 0}}>
              <Text style={AppStyles.Labeldefault}>Người nhận bàn giao</Text>
              <TouchableOpacity
                onPress={() => {
                  this.onActionComboboxReceiverTask();
                }}>
                <Text
                  style={[
                    AppStyles.FormInput,
                    {
                      color: this.state.ReceiverTaskGuidName ? 'black' : 'gray',
                      padding: 10,
                    },
                  ]}>
                  {this.state.ReceiverTaskGuidName
                    ? this.state.ReceiverTaskGuidName
                    : 'Chọn nhân viên...'}
                </Text>
              </TouchableOpacity>
            </View> */}
            {/* )} */}

            {/* <View style={{ padding: 10, paddingBottom: 0 }}>
                  <Text style={AppStyles.Labeldefault}>Người nhận bàn giao </Text>
                  <View
                    style={{
                      ...AppStyles.FormInputPicker,
                      justifyContent: 'center',
                    }}>
                    <RNPickerSelect
                doneText="Xong" onValueChange={value => this.onChangeState('ReceiverTaskGuid', value)}
                      items={this.state.ListReceiverTask || []}
                      placeholder={{}}
                      value={this.state.ReceiverTaskGuid}

                    />
                  </View>
                </View> */}
            <View
              style={{
                padding: 10,
                paddingBottom: 0,
                flexDirection: 'column',
              }}>
              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <Text style={AppStyles.Labeldefault}>
                  Từ ngày <RequiredText />
                </Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={[
                    {
                      flex: 1.5,
                      ...AppStyles.FormInputPickerSmall,
                      justifyContent: 'center',
                    },
                  ]}>
                  <RNPickerSelect
                    doneText="Xong"
                    onValueChange={value =>
                      this.onChangeState('StatusStartTime', value)
                    }
                    items={TypeOfDay}
                    placeholder={{}}
                    value={this.state.StatusStartTime}
                    useNativeAndroidPickerStyle={false}
                    style={pickerSelectStyles}
                  />
                </View>
                <View style={{ flex: 2 }}>
                  <DatePicker
                    locale="vie"
                    locale={'vi'}
                    style={{ width: '100%' }}
                    date={this.state.StartDate}
                    mode="date"
                    androidMode="spinner"
                    placeholder="Chọn ngày"
                    format="DD/MM/YYYY"
                    confirmBtnText="Xong"
                    cancelBtnText="Huỷ"
                    onDateChange={date => this.setStartDate(date)}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                padding: 10,
                paddingBottom: 0,
                flexDirection: 'column',
              }}>
              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <Text style={AppStyles.Labeldefault}>
                  Đến ngày <RequiredText />
                </Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={[
                    {
                      flex: 1.5,
                      ...AppStyles.FormInputPickerSmall,
                      justifyContent: 'center',
                    },
                  ]}>
                  <RNPickerSelect
                    doneText="Xong"
                    onValueChange={value =>
                      this.onChangeState('StatusEndTime', value)
                    }
                    items={TypeOfDay}
                    placeholder={{}}
                    value={this.state.StatusEndTime}
                    useNativeAndroidPickerStyle={false}
                    style={pickerSelectStyles}
                  />
                </View>
                <View style={{ flex: 2 }}>
                  <DatePicker
                    locale="vie"
                    locale={'vi'}
                    style={{ width: '100%' }}
                    date={this.state.EndDate}
                    mode="date"
                    androidMode="spinner"
                    placeholder="Chọn ngày"
                    format="DD/MM/YYYY"
                    confirmBtnText="Xong"
                    cancelBtnText="Huỷ"
                    onDateChange={date => this.setEndDate(date)}
                  />
                </View>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <Text style={AppStyles.Labeldefault}>Hình thức</Text>

              <View
                style={{
                  ...AppStyles.FormInputPicker,
                  justifyContent: 'center',
                }}>
                <RNPickerSelect
                  doneText="Xong"
                  onValueChange={value => {
                    this.onChangeState('Type', value);
                  }}
                  items={this.state.ListType}
                  placeholder={{}}
                  value={this.state.Type}
                  style={styleCustomIos}
                />
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>Số ngày còn lại</Text>
              </View>
              <TextInput
                style={[AppStyles.FormInput, { color: 'black' }]}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                value={this.state.PhepConLai}
                editable={false}
              />
            </View>
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>Tổng số ngày (.)</Text>
              </View>
              <TextInput
                style={[AppStyles.FormInput, { color: 'black' }]}
                underlineColorAndroid="transparent"
                value={this.state.Total}
                autoCapitalize="none"
                onChangeText={value => this.onChangeState('Total', value)}
              />
            </View>

            {/* <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Nơi nghỉ</Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={this.state.Note}
                  onChangeText={value => this.onChangeState('Note', value)}
                />
              </View> */}
            {/* <View style={{padding: 10, paddingBottom: 0}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>Nơi nghỉ </Text>
              </View>
              <TextInput
                style={AppStyles.FormInput}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                value={this.state.Location}
                onChangeText={value => this.onChangeState('Location', value)}
              />
            </View> */}
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>
                  Lý do <RequiredText />{' '}
                </Text>
              </View>
              <TextInput
                style={AppStyles.FormInput}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                numberOfLines={4}
                multiline
                value={this.state.Description}
                onChangeText={Desc => this.setDescription(Desc)}
              />
            </View>
            {/* <View style={{ padding: 10, paddingBottom: 0 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    multiline
                    value={this.state.Note}
                    onChangeText={Note => this.setNote(Note)}
                  />
                </View> */}
            {false && !this.props.itemData && (
              <View
                style={{
                  flexDirection: 'column',
                  padding: 10,
                  marginBottom: 20,
                }}>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                    <Text style={{ color: 'red' }} />
                  </View>
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      justifyContent: 'flex-end',
                      flexDirection: 'row',
                    }}
                    onPress={() => this.openAttachment()}>
                    <Icon
                      name="attachment"
                      type="entypo"
                      size={15}
                      color={AppColors.ColorAdd}
                    />
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        { color: AppColors.ColorAdd },
                      ]}>
                      {' '}
                      Chọn file
                    </Text>
                  </TouchableOpacity>
                </View>
                <Divider style={{ marginBottom: 10 }} />
                {this.state.Attachment.length > 0
                  ? this.state.Attachment.map((para, i) => (
                    <View
                      key={i}
                      style={[
                        styles.StyleViewInput,
                        { flexDirection: 'row', marginBottom: 3 },
                      ]}>
                      <View
                        style={[AppStyles.containerCentered, { padding: 10 }]}>
                        <Image
                          style={{
                            width: 30,
                            height: 30,
                            borderRadius: 10,
                          }}
                          source={{
                            uri: this.FileAttackments.renderImage(
                              para.FileName,
                            ),
                          }}
                        />
                      </View>
                      <TouchableOpacity
                        key={i}
                        style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={AppStyles.Textdefault}>
                          {para.FileName}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.containerCentered, { padding: 10 }]}
                        onPress={() => this.removeAttactment(para)}>
                        <Icon
                          name="close"
                          type="antdesign"
                          size={20}
                          color={AppColors.ColorDelete}
                        />
                      </TouchableOpacity>
                    </View>
                  ))
                  : null}
              </View>
            )}
          </View>
        </ScrollView>

        <View>
          <Button
            buttonStyle={{ backgroundColor: AppColors.ColorButtonSubmit }}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
        <OpenPhotoLibrary
          callback={this.callbackLibarary}
          openLibrary={this.openLibrary}
        />
        <ComboboxV2
          callback={this.ChoiceAtt}
          data={listCombobox.ListComboboxAtt}
          eOpen={this.openCombobox_Att}
        />
        {this.state.ListEmployees.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeType}
            data={this.state.ListEmployees}
            nameMenu={'Chọn nhân viên'}
            eOpen={this.openComboboxType}
            position={'bottom'}
            value={this.state.EmployeeAskGuid}
          />
        ) : null}
        {this.state.ListEmployeesReceiver.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeTypeReceiver}
            data={this.state.ListEmployeesReceiver}
            nameMenu={'Chọn nhân viên'}
            eOpen={this.openActionComboboxReceiverTask}
            position={'bottom'}
            value={this.state.ReceiverTaskGuid}
          />
        ) : null}
        {/* </View> */}
      </KeyboardAvoidingView>
    );
  }
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value) {
      this.GetApplyLeavesInYear(rs.value);
    }
    this.setState({
      EmployeeAskGuid: rs.value,
      EmployeeAskGuidName: rs.text,
    });
  };
  ChangeTypeReceiver = rs => {
    if (!rs) {
      return;
    }
    this.setState({
      ReceiverTaskGuid: rs.value,
      ReceiverTaskGuidName: rs.text,
    });
  };
  setStartDate(date) {
    this.setState({ StartDate: date }, () => {
      this.countTotal();
      this.GetApplyLeavesInYear(this.state.EmployeeAskGuid);
    });
  }
  setEndDate(date) {
    this.setState({ EndDate: date }, () => {
      this.countTotal();
    });
  }
  setNote(Note) {
    this.setState({ Note });
  }
  setDescription(Description) {
    this.setState({ Description });
  }

  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'Back', ActionTime: new Date().getTime() });
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_AL',
      Prefixs: null,
      VoucherType: '',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({ isEdit: data.IsEdit, ApplyLeaveId: data.Value });
    });
  };
  getEmployee = () => {
    API_ApplyLeaves.GetEmp()
      .then(response => {
        let data = response.data;
        data = data.map(item => ({
          text: '[' + item.EmployeeId + '] ' + item.FullName,
          value: item.EmployeeGuid,
        }));

        this.setState({ ListEmployees: data });
      })
      .catch(error => {
        console.log(error.data);
      });
  };
  getEmployeeReceiverTask = () => {
    API_ApplyLeaves.GetReceiverTask()
      .then(response => {
        console.log(response);
        let data = response.data;
        data = data.map(item => ({
          text: '[' + item.EmployeeId + '] ' + item.FullName,
          value: item.EmployeeGuid,
        }));

        this.setState({ ListEmployeesReceiver: data });
      })
      .catch(error => {
        console.log(error.data);
      });
  };
  // getApplyLeavesTypes = () => {
  //   API_ApplyLeaves.getApplyLeavesTypes()
  //     .then(response => {
  //       console.log(response);
  //       let data = response.data;
  //       data = data.map(item => ({
  //         value: item.value,
  //         label: item.text,
  //       }));
  //       const Type = this.props.itemData?.Type
  //         ? this.props.itemData?.Type
  //         : 'KL';
  //       this.setState({
  //         ListType: data,
  //         Type: Type,
  //       });
  //     })
  //     .catch(error => {
  //       console.log(error.data);
  //     });
  // };
  //#region openAttachment
  _openCombobox_Att() { }
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#region open img
  _open() { }
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({ Attachment: this.state.Attachment });
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({ Attachment: this.state.Attachment });
    } catch (err) {
      this.setState({ loading: false });
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  Add = () => {
    const { Attachment } = this.state;
    setTimeout(() => {
      this.loading = false;
    }, 2000);
    if (!this.state.EmployeeAskGuid) {
      Toast.showWithGravity(
        'Bạn chưa chọn nhân viên xin nghỉ',
        Toast.SHORT,
        Toast.CENTER,
      );

      return;
    }
    // if (!this.state.ReceiverTaskGuid) {
    //   Toast.showWithGravity(
    //     'Bạn chưa chọn người nhận bàn giao',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    if (!this.state.Description) {
      Toast.showWithGravity('Bạn chưa nhập lý do', Toast.SHORT, Toast.CENTER);
      return;
    }
    if (!this.state.ApplyLeaveId) {
      Toast.showWithGravity(
        'Bạn chưa nhập mã nghỉ phép',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (this.state.EndDate && this.state.StartDate) {
      var data1 = FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.StartDate, 99),
        4,
      );
      var data2 = FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.EndDate, 99),
        4,
      );
      if (data1 > data2) {
        Toast.showWithGravity(
          'Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu',
          Toast.SHORT,
          Toast.CENTER,
        );

        return;
      }
    }
    if (this.props.itemData) {
      this.Update();
      return;
    }
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    let _obj = {
      EmployeeAskGuid: this.state.EmployeeAskGuid,
      ReceiverTaskGuid: this.state.ReceiverTaskGuid,
      StartTime: this.state.StartDate
        ? FuncCommon.ConDate(FuncCommon.ConDate(this.state.StartDate, 99), 2)
        : null,
      EndTime: this.state.EndDate
        ? FuncCommon.ConDate(FuncCommon.ConDate(this.state.EndDate, 99), 2)
        : null,
      Type: this.state.Type,
      Total: +this.state.Total,
      Description: this.state.Description,
      Note: this.state.Note,
      ApplyLeaveId: this.state.ApplyLeaveId,
      FileAttachments: [],
      StatusEndTime: this.state.StatusEndTime,
      StatusStartTime: this.state.StatusStartTime,
      EmployeeGuid: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
      IsChecked:
        this.state.EmployeeAskGuid !==
          global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid
          ? true
          : false,
      Location: this.state.Location,
    };
    let auto = { Value: this.state.ApplyLeaveId, IsEdit: this.state.isEdit };
    let _data = new FormData();
    _data.append('insert', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    _data.append('DeleteAttach', JSON.stringify([]));
    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_ApplyLeaves.InsertApplyLeaves(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          console.log(_rs);
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.callBackList();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  Submit = () => {
    if (this.loading) {
      return;
    }
    this.loading = true;
    FuncCommon.Data_Offline(async d => {
      if (!d) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      } else {
        this.Add();
      }
    });
  };
  Update = () => {
    let _obj = {
      ...this.props.itemData,
      EmployeeAskGuid: this.state.EmployeeAskGuid,
      ReceiverTaskGuid: this.state.ReceiverTaskGuid,
      StartTime: this.state.StartDate
        ? FuncCommon.ConDate(FuncCommon.ConDate(this.state.StartDate, 99), 2)
        : null,
      EndTime: this.state.EndDate
        ? FuncCommon.ConDate(FuncCommon.ConDate(this.state.EndDate, 99), 2)
        : null,
      Type: this.state.Type,
      Total: +this.state.Total,
      Description: this.state.Description,
      Note: this.state.Note,
      ApplyLeaveId: this.state.ApplyLeaveId,
      FileAttachments: [],
      StatusEndTime: this.state.StatusEndTime,
      StatusStartTime: this.state.StatusStartTime,
      Attachment: this.state.Attachment,
      IsChecked:
        this.state.EmployeeAskGuid !==
          global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid
          ? true
          : false,
      Location: this.state.Location,
    };

    const updateTask = '0';
    let auto = { Value: this.state.ApplyLeaveId, IsEdit: this.state.isEdit };
    let _data = new FormData();
    _data.append('ApplyLeaves', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('updateTask', JSON.stringify(updateTask));
    _data.append('lstTitlefile', JSON.stringify([]));
    _data.append('DeleteAttach', JSON.stringify([]));
    API_HR.UpdateApplyLeaves(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Chỉnh sửa thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  callBackList(rs) {
    this.setState({ loading: false });
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      // Data: !this.state.itemData
      //   ? {
      //       Module: 'HR_ApplyLeaves',
      //       Subject: this.state.Description,
      //       StartDate:
      //         this.state.StartDate === '' || this.state.StartDate === null
      //           ? null
      //           : FuncCommon.ConDate(this.state.StartDate, 0),
      //       EndDate:
      //         this.state.EndDate === '' || this.state.EndDate === null
      //           ? null
      //           : FuncCommon.ConDate(this.state.EndDate, 0),
      //       RowGuid: rs,
      //       AssignTo: [],
      //     }
      //   : {},
      ActionTime: new Date().getTime(),
    });
  }
  GetApplyLeavesInYear = id => {
    let _obj = {
      Id: id,
      StartDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.StartDate, 99),
        2,
      ),
    };
    API_ApplyLeaves.ApplyLeavesInYear(_obj)
      .then(rs => {
        var _rs = JSON.parse(rs.data.data);
        console.log(_rs.PhepConLai);
        this.setState({ PhepConLai: _rs.PhepConLai + '' });
      })
      .catch(error => {
        console.log(error);
      });
  };
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  ConDate = function (data, number) {
    try {
      if (data == null || data == '') {
        return '';
      }
      if (data !== null && data != '' && data != undefined) {
        try {
          if (data.indexOf('SA') != -1 || data.indexOf('CH') != -1) {
            if (data.indexOf('SA') != -1) {
            }
            if (data.indexOf('CH') != -1) {
            }
          }

          if (data.indexOf('Date') != -1) {
            data = data.substring(data.indexOf('Date') + 5);
            data = parseInt(data);
          }
        } catch (ex) { }
        var newdate = new Date(data);
        if (number == 3) {
          return newdate;
        }
        if (number == 2) {
          return '/Date(' + newdate.getTime() + ')/';
        }
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();
        if (month < 10) {
          month = '0' + month;
        }
        if (day < 10) {
          day = '0' + day;
        }
        if (mm < 10) {
          mm = '0' + mm;
        }
        if (number == 0) {
          return (todayDate = day + '/' + month + '/' + year);
        }
        if (number == 1) {
          return (todayDate =
            day + '/' + month + '/' + year + ' ' + hh + ':' + mm);
        }
        if (number == 4) {
          return new Date(year, month - 1, day);
        }
      } else {
        return '';
      }
    } catch (ex) {
      return '';
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ApplyLeavesAddComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
  },
});
const pickerSelectStyles = {
  inputIOS: {
    padding: 0,
    marginLeft: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    padding: 0,
    marginLeft: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
  },
};
