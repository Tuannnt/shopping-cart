import { API_ApplyOverTimes, API_ApplyOutsides } from '@network';
import { ErrorHandler } from '@error';
export default {
  // List nhan vien
  getAllEmployee(callback) {
    API_ApplyOverTimes.GetEmp()
      .then(res => {
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  },
  getAllEmployeeByDepart(callback) {
    API_ApplyOutsides.GetEmployeeByDepartment()
      .then(res => {
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  },
  getAllWorkShift(callback) {
    API_ApplyOverTimes.getAllWorkShift()
      .then(res => {
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  },
  getAllOrder(callback) {
    API_ApplyOutsides.getAllOrder()
      .then(res => {
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  },
  getNumberAuto(value, callback) {
    API_ApplyOverTimes.GetNumberAuto(value)
      .then(res => {
        console.log(res + '=======> get Number auto');
        const { data } = res.data;
        callback(data);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  },
  removeAttactment(data, list, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },
};
