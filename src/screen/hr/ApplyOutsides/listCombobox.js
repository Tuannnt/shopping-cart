import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
  ],
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Nhân viên', width: 250},
    {title: 'Chức danh', width: 200},
    {title: 'Phương tiện', width: 200},
    {title: 'Quãng đường', width: 100},
    {title: 'Ngày công tác', width: 120},
    {title: 'Thời gian vào', width: 120},
    {title: 'Thời gian ra', width: 120},
    {title: 'Tổng số giờ', width: 80},
    {title: 'Tổng chi phí', width: 200},
    {title: 'Số lượng tài sản', width: 100},
    {title: 'Ghi chú', width: 150},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  ListVehice: [
    {value: null, label: 'Chọn'},
    {
      value: 0,
      label: 'Không có',
    },
    {
      value: 1,
      label: 'Xe máy',
    },
  ],
  //#endregion
};
