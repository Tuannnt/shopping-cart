import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  Text,
  Keyboard,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {API_ApplyOutsides} from '../../../network';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import {Container} from 'native-base';
import _ from 'lodash';
import moment from 'moment';
import {FuncCommon} from '../../../utils';
import {Actions} from 'react-native-router-flux';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import listCombobox from './listCombobox';
import controller from './controller';
import Toast from 'react-native-simple-toast';
import LoadingComponent from '../../component/LoadingComponent';

const headerTable = listCombobox.headerTable;

class ApplyOutsidesItem_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      DataDetail: [],
      Title: '',
      checkInLogin: '',
      rows: [],
      ListEmployee: [],
      loading: true,
    };
    this.add = React.createRef();
  }
  componentDidMount(): void {
    Promise.all([this.getItem()]);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }
  onUpdate = () => {};
  callbackOpenUpdate = () => {
    Actions.applyOutsidesAdd({
      dataEdit: _.cloneDeep(this.state.Data),
      rows: _.cloneDeep(this.state.rows),
    });
  };
  openAttachments() {
    var obj = {
      ModuleId: '43',
      RecordGuid: this.props.RecordGuid,
      notEdit: this.state.checkInLogin === '1' ? false : true,
    };

    Actions.attachmentComponent(obj);
  }
  helperNumber = num => {
    if (!num) {
      return '';
    }
    return num + '';
  };
  getItem() {
    let id = {Id: this.props.RecordGuid};
    API_ApplyOutsides.ApplyOutsides_Items(id)
      .then(res => {
        if (!JSON.parse(res.data.data).model) {
          return;
        }
        let rows = [];
        let details = JSON.parse(res.data.data).detail;
        if (details) {
          rows = details.map(detail => ({
            ...detail,
          }));
        }
        this.setState({Data: JSON.parse(res.data.data).model, rows});
        let checkin = {
          ApplyOutsideGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).model.WorkFlowGuid,
        };
        API_ApplyOutsides.ApplyOutsides_checkin(checkin)
          .then(res => {
            this.setState({checkInLogin: res.data.data, loading: false});
            console.log('===checkin====' + res.data.data);
            console.log('--------' + this.state.Data.Permisstion);
          })
          .catch(error => {
            console.log(error.data.data);
          });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
      });
  }
  showActionSheet = () => this.actionSheet.show();
  getActionSheetRef = ref => (this.actionSheet = ref);
  handleChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  onChangeText = (key, value) => {
    this.setState({[key]: value});
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };

  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({rows: res, typeTime: null, currentIndex: null});
    this.hideDatePicker();
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  CustomViewDetailRoutings = () => {
    const item = this.state.Data;
    return (
      <ScrollView>
        <View>
          <View style={{padding: 15}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                {/*Mã phiếu*/}
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Labeldefault}>Mã:</Text>
                  </View>
                  <View style={{flex: 2, paddingLeft: 10}}>
                    <Text style={[AppStyles.Textdefault, {fontWeight: 'bold'}]}>
                      {item.WorkOutsideId}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            {/*Tiêu đề phiêu*/}

            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Tiêu đề phiếu :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>{item.Title}</Text>
              </View>
            </View>
            {/*Người tạo phiếu*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Người tạo :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text>
              </View>
            </View>
            {/*Phòng ban*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Phòng ban :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>
                  {item.DepartmentName}
                </Text>
              </View>
            </View>
            {/*Chức vụ*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Chức vụ :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>{item.JobTitleName}</Text>
              </View>
            </View>
            {/*Ngày tạo phiếu*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày đăng ký :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.customDate(item.Date)}
                </Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>
                  Tên đối tác/khách hàng :
                </Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>{item.CustomerName}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Địa chỉ công tác :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>{item.Address}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Đơn hàng nội bộ :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>{item.VehicleName}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>
                  Có mang theo tài sản :
                </Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>
                  {item.BringAsset
                    ? 'Mang theo tài sản'
                    : 'Không mang theo tài sản'}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Phí công tác :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>
                  {FuncCommon.addPeriod(item.Distance)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                {item.Status == 'Y ' ? (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.AcceptColor},
                    ]}>
                    {item.StatusWF || ''}
                  </Text>
                ) : (
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: AppColors.PendingColor},
                    ]}>
                    {item.StatusWF || ''}
                  </Text>
                )}
              </View>
            </View>
            {/*nội dung phiếu*/}
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
              </View>
              <View style={{flex: 2, paddingLeft: 10}}>
                <Text style={[AppStyles.Textdefault]}>{item.Description}</Text>
              </View>
            </View>
          </View>
          {/*hiển thị nội dung chi tiết*/}
          <Text style={{fontSize: 16, fontWeight: 'bold', padding: 10}}>
            Chi tiết phiếu
          </Text>
          <Divider />
          <View style={{flex: 5}}>
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  {headerTable.map(item => {
                    if (item.hideInDetail) {
                      return null;
                    }
                    return (
                      <TouchableOpacity
                        key={item.title}
                        style={[AppStyles.table_th, {width: item.width}]}>
                        <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>
                <View
                // style={{
                //   maxHeight: Dimensions.get('window').height / 7,
                // }}
                >
                  {this.state.rows.length > 0 ? (
                    <FlatList
                      data={this.state.rows}
                      renderItem={({item, index}) => {
                        return this.itemFlatList(item, index);
                      }}
                      keyExtractor={(rs, index) => index.toString()}
                    />
                  ) : (
                    <TouchableOpacity style={[AppStyles.table_foot]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                        Không có dữ liệu!
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </ScrollView>
    );
  };
  helperVehice = value => {
    let {ListVehice} = listCombobox;
    let vehice = ListVehice.find(item => item.value === value);
    if (!vehice) {
      return '';
    }
    return vehice.label;
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 250}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.EmployeeName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.JobTitleName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.AssetName === '0'
              ? 'Không có'
              : row.AssetName === '1'
              ? 'Xe máy'
              : ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Distance}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {moment(new Date(row.ActualEndTime)).format('DD/MM/YYYY')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.HourInReal}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.HourOutReal}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 80}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.TotalHour}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {FuncCommon.addPeriod(row.TotalAllowance)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Quantity}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Note}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  CustomView = item => (
    <Container>
      <TouchableWithoutFeedback
        style={{flex: 30}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          {
            <TabBar_Title
              title={'Chi tiết phiếu báo công tác'}
              callBack={() => Actions.pop()}
            />
          }
          <Divider />
          {/*hiển thị nội dung chính*/}
          <View
            style={{
              flex: 10,
            }}>
            {this.CustomViewDetailRoutings()}
          </View>

          {/*nút xử lý*/}
          <View style={{flex: 1}}>
            {this.state.checkInLogin !== '' ? (
              <TabBarBottom
                callbackOpenUpdate={this.callbackOpenUpdate}
                onDelete={() => this.Delete()}
                backListByKey="ApplyOutsides"
                Title={item.WorkOutsideId}
                Permisstion={item.Permisstion}
                checkInLogin={this.state.checkInLogin}
                onAttachment={() => this.openAttachments()}
                onSubmitWF={(callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
                }
                keyCommentWF={{
                  ModuleId: 44,
                  RecordGuid: this.props.RecordGuid,
                  Title: item.WorkOutsideId,
                }}
              />
            ) : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Container>
  );
  render() {
    return this.state.loading ? (
      <LoadingComponent />
    ) : this.state.Data ? (
      this.CustomView(this.state.Data)
    ) : null;
  }
  onView(id) {
    Actions.OpenApplyOutsideDetails({Id: id});
    console.log('===================chuyen Id:' + id);
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    this.setState({loading: true});
    console.log('callback==' + callback + CommentWF);
    if (callback == 'D') {
      let obj = {
        ApplyOutsideGuid: this.props.RecordGuid,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.props.Loginname,
        Comment: CommentWF,
      };
      API_ApplyOutsides.approveApplyOutsides(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      let obj = {
        ApplyOutsideGuid: this.props.RecordGuid,
        WorkFlowGuid: this.state.Data.WorkFlowGuid,
        LoginName: this.props.Loginname,
        Comment: CommentWF,
      };
      API_ApplyOutsides.noapproveApplyOutsides(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
    this.setState({
      loading: false,
    });
  }
  Delete = () => {
    let id = {Id: this.props.RecordGuid};
    this.setState({loading: true});
    API_ApplyOutsides.ApplyOutsides_Delete(id.Id)
      .then(response => {
        console.log('==============ketquaXoa: ' + response.data.Error);
        if (response.data.errorCode === 200) {
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        console.log(error.data.data);
      });
    this.setState({loading: false});
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng ngày và giờ
  customDatetime(strDateTime) {
    if (strDateTime != null) {
      var strSplitDateTime = String(strDateTime).split(' ');
      var datetime = new Date(strSplitDateTime[0]);
      // alert(date);
      var HH = datetime.getHours();
      var MM = datetime.getMinutes();
      var dd = datetime.getDate();
      var mm = datetime.getMonth() + 1; //January is 0!
      var yyyy = datetime.getFullYear();
      if (HH < 10) {
        HH = '0' + HH;
      }
      if (MM < 10) {
        MM = '0' + MM;
      }
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      datetime = dd + '/' + mm + '/' + yyyy + ' ' + HH + ':' + MM;
      return datetime.toString();
    } else {
      return '';
    }
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ApplyOutsidesItem_Component);
