import React, {Component} from 'react';
import {
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Alert,
  TextInput,
  ScrollView,
  Switch,
  Image,
  FlatList,
} from 'react-native';
import {Button, Divider, Icon as IconElement} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_ApplyOutsides} from '@network';
import {Container, Content, Item, Label, Picker, Form} from 'native-base';
import {FuncCommon} from '../../../utils';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import DatePicker from 'react-native-datepicker';
import listCombobox from './listCombobox';
import {
  Combobox,
  TabBar_Title,
  RequiredText,
  OpenPhotoLibrary,
  ComboboxV2,
} from '@Component';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';
import controller from './controller';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import _ from 'lodash';
import Toast from 'react-native-simple-toast';

const headerTable = listCombobox.headerTable;
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});

class ApplyOutsidesAdd_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [{}],
      isEdit: true,
      Attachment: [],
      refreshing: false,
      Data: null,
      DataDetail: null,
      search: '',
      Date: new Date(),
      // StartTime: FuncCommon.ConDate(new Date(), 0),
      // EndTime: FuncCommon.ConDate(new Date(), 0),
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      ListEmployee: [],
      Distance: '3000',
      OrderName: '',
      ListOrder: [],
      BringAsset: true,
    };
    this.loading = false;
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  helperNumber = num => {
    if (!num) {
      return '';
    }
    return num + '';
  };
  componentDidMount(): void {
    const {dataEdit, rows = []} = this.props;
    Promise.all([this.getAllEmployeeByDepart(), this.getAllOrder()]);
    if (dataEdit && rows) {
      let _row = _.cloneDeep(rows).map(row => {
        if (row.HourInReal) {
          row.HourInReal = FuncCommon.ConDate(row.HourInReal, 8);
        }
        if (row.HourOutReal) {
          row.HourOutReal = FuncCommon.ConDate(row.HourOutReal, 8);
        }
        if (row.Quantity) {
          row.Quantity = this.helperNumber(row.Quantity);
        }
        if (row.AssetName || row.AssetName === '0') {
          row.AssetName = +row.AssetName;
        }
        return row;
      });
      this.setState({
        isEdit: false,
        Title: dataEdit.Title,
        Date: new Date(dataEdit.Date),
        Vehicle: dataEdit.Vehicle,
        OrderName: dataEdit.VehicleName,
        // StartTime: FuncCommon.ConDate(new Date(dataEdit.StartTime), 0),
        // EndTime: FuncCommon.ConDate(new Date(dataEdit.EndTime), 0),
        BringAsset: Boolean(dataEdit.BringAsset),
        WorkOutsideId: dataEdit.WorkOutsideId,
        Distance: dataEdit.Distance ? dataEdit.Distance + '' : '',
        CustomerName: dataEdit.CustomerName,
        Address: dataEdit.Address,
        FullName: dataEdit.EmployeeName,
        DepartmentName: dataEdit.DepartmentName,
        JobTitleNameEmployee: dataEdit.JobTitleName,
        Description: dataEdit.Description,
        rows: _row,
      });
    } else {
      this.getNumberAuto();
      this.setState({
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        JobTitleNameEmployee:
          global.__appSIGNALR.SIGNALR_object.USER.JobTitleName,
      });
    }
  }
  getAllOrder = () => {
    controller.getAllOrder(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.OrderNumber,
        text: `[${item.OrderNumber}] ${item.Type}`,
      }));
      this.setState({
        ListOrder: data,
      });
    });
  };
  getAllEmployeeByDepart = () => {
    controller.getAllEmployeeByDepart(rs => {
      let data = JSON.parse(rs).map(item => ({
        value: item.EmployeeGuid,
        label: `${item.EmployeeId}-${item.FullName}`,
        JobTitleName: item.JobTitleName,
      }));
      data.unshift({
        value: null,
        label: 'Chọn nhân viên',
      });
      this.setState({
        ListEmployee: data,
      });
    });
  };
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_CT',
      Prefixs: null,
      VoucherType: '',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({isEdit: data.IsEdit, WorkOutsideId: data.Value});
    });
  };
  Submit() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 2000);
    const {
      Title,
      // StartTime,
      // EndTime,
      BringAsset,
      WorkOutsideId,
      Vehicle,
      Distance,
      CustomerName,
      Address,
      Attachment,
      Description,
      Date,
    } = this.state;

    // let _startTime = FuncCommon.ConDate(
    //   FuncCommon.ConDate(this.state.StartTime, 99),
    //   2,
    // );
    // let _endTime = FuncCommon.ConDate(
    //   FuncCommon.ConDate(this.state.EndTime, 99),
    //   2,
    // );
    let data = {
      Vehicle,
      Title,
      Date: FuncCommon.ConDate(Date, 2),
      // StartTime: StartTime === '' || StartTime === null ? null : _startTime,
      // EndTime: EndTime === '' || EndTime === null ? null : _endTime,
      BringAsset: BringAsset + '',
      WorkOutsideId,
      // Vehicle,
      Distance: Distance ? Number(Distance) : undefined,
      CustomerName,
      Address,
      Description,
    };
    var _obj = {
      FileAttachments: [],
      ...data,
    };
    if (!Title) {
      Toast.showWithGravity('Bạn chưa nhập tiêu đề', Toast.SHORT, Toast.CENTER);
      return;
    }
    if (!Description) {
      Toast.showWithGravity(
        'Bạn chưa nhập nội dung',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (
      (!Attachment || Attachment.length === 0) &&
      this.state.BringAsset &&
      !this.props.dataEdit
    ) {
      Toast.showWithGravity(
        'Bạn chưa thêm file đính kèm',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    // if (_startTime > _endTime) {
    //   Toast.showWithGravity(
    //     'Thông báo',
    //     'Thời gian bắt đầu dự kiến phải nhỏ hơn thời gian kết thúc dự kiến',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    let isNotValidated = false;
    let lstDetails = _.cloneDeep(this.state.rows);
    for (let i = 0; i < lstDetails.length; i++) {
      let lst = lstDetails[i];
      if (!lst.EmployeeGuid) {
        Toast.showWithGravity(
          `Bạn chưa chọn nhân viên ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
      if (!lst.ActualEndTime) {
        Toast.showWithGravity(
          `Bạn chưa chọn ngày công tác ở dòng ${i + 1}`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
      // if (!lst.ActualStartTime) {
      //   Toast.showWithGravity(
      //     'Thông báo',
      //     'Bạn chưa chọn thời gian bắt đầu',
      //     Toast.SHORT,
      //     Toast.CENTER,
      //   );
      //   isNotValidated = true;
      //   return;
      // }
      // if (!lst.ActualEndTime) {
      //   Toast.showWithGravity(
      //     'Thông báo',
      //     'Bạn chưa chọn thời gian kết thúc',
      //     Toast.SHORT,
      //     Toast.CENTER,
      //   );
      //   isNotValidated = true;
      //   return;
      // }
      // if (
      //   FuncCommon.ConDate(lst.ActualStartTime, 2) >
      //   FuncCommon.ConDate(lst.ActualEndTime, 2)
      // ) {
      //   Toast.showWithGravity(
      //     'Thông báo',
      //     'Yêu cầu thời gian bắt đầu nhỏ hơn thời gian kết thúc.',
      //   );
      //   isNotValidated = true;
      //   return;
      // }
    }
    if (isNotValidated) {
      return;
    }
    let auto = {Value: this.state.WorkOutsideId, IsEdit: this.state.isEdit};
    lstDetails = lstDetails.map(detail => ({
      Id: null,
      ...detail,
      JobTitleName: this.helperPosition(detail.EmployeeGuid),
      TotalHour:
        detail.HourInReal && detail.HourOutReal
          ? Math.round(
              FuncCommon.getDiffTime(detail.HourInReal, detail.HourOutReal) *
                100,
            ) /
              100 +
            ''
          : '',
      TotalAllowance:
        detail.Distance && this.state.Distance
          ? (+detail.Distance * +this.state.Distance)
              .toFixed(2)
              .replace(/\d(?=(\d{3})+\.)/g, '$&,')
          : '0',
      HourInReal:
        detail.HourInReal && moment(detail.HourInReal).format('HH:mm:ss'),
      HourOutReal:
        detail.HourOutReal && moment(detail.HourOutReal).format('HH:mm:ss'),
      AssetName:
        detail.AssetName || detail.AssetName === 0
          ? detail.AssetName + ''
          : undefined,
      Quantity: detail.Quantity
        ? detail.Quantity.replace(/[.,]/g, '') + ''
        : '0',
      Distance: detail.Distance ? detail.Distance + '' : '0',
    }));
    if (this.props.dataEdit) {
      let _edit = {
        ...this.props.dataEdit,
        ...data,
        Id: WorkOutsideId,
      };
      let _dataEdit = new FormData();
      _dataEdit.append('edit', JSON.stringify(_edit));
      _dataEdit.append('auto', JSON.stringify({IsEdit: true}));
      _dataEdit.append('DeleteAttach', JSON.stringify([]));
      _dataEdit.append('lstDetails', JSON.stringify(lstDetails));
      _dataEdit.append('lstTitlefile', JSON.stringify([]));
      _dataEdit.append('DeleteDetail', JSON.stringify([]));
      API_ApplyOutsides.ApplyOutsides_Update(_dataEdit)
        .then(res => {
          console.log('ApplyOverTimes_Edit============:' + res.data.message);
          if (res.data.errorCode !== 200) {
            Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
            return;
          }
          Toast.showWithGravity(
            'Chỉnh sửa thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
          return;
        })
        .catch(error => {
          console.log(error.data.data);
          console.log('=======' + error.data);
        });
      return;
    }
    let _data = new FormData();
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    _data.append('submit', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteAttach', JSON.stringify([]));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));

    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_ApplyOutsides.ApplyOutsides_Submit(_data)
      .then(rs => {
        if (rs.data.errorCode === 200) {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        } else {
          Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        console.log('Error when call API insert Mobile.');
        console.log(error);
      });
  }
  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#endregion
  onChangeText = (key, value) => {
    this.setState({[key]: value});
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  helperPosition = id => {
    if (!id) {
      return '';
    }
    let employee = this.state.ListEmployee.find(emp => emp.value === id);
    if (!employee) {
      return '';
    }
    return employee.JobTitleName;
  };
  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({rows: res, typeTime: null, currentIndex: null});
    this.hideDatePicker();
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    let TotalAllowance = '';
    if (row.Distance && this.state.Distance) {
      TotalAllowance =
        +row.Distance.replace(/\./g, '') *
        +this.state.Distance.replace(/\./g, '');
    }
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 250}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'EmployeeGuid');
            }}
            items={this.state.ListEmployee}
            value={row.EmployeeGuid}
            placeholder={{}}
          />
        </View>

        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.EmployeeGuid && this.helperPosition(row.EmployeeGuid)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'AssetName');
            }}
            items={listCombobox.ListVehice}
            value={row.AssetName}
            placeholder={{}}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {textAlign: 'right'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={
              !row.Distance
                ? row.Distance
                : FuncCommon.addPeriodTextInput(row.Distance)
            }
            autoCapitalize="none"
            onChangeText={Distance => {
              this.handleChangeRows(Distance, index, 'Distance');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (this.props.itemData) {
              return;
            }
            this.handlePickDateOpen('ActualEndTime', index);
          }}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.ActualEndTime &&
              moment(row.ActualEndTime).format('DD/MM/YYYY')) ||
              'Chọn ngày'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('HourInReal', index);
          }}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.HourInReal && moment(row.HourInReal).format('HH:mm')) ||
              'Chọn thời gian'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('HourOutReal', index);
          }}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.HourOutReal && moment(row.HourOutReal).format('HH:mm')) ||
              'Chọn thời gian'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 80}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.HourInReal &&
              row.HourOutReal &&
              Math.round(
                FuncCommon.getDiffTime(row.HourInReal, row.HourOutReal) * 100,
              ) / 100}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {textAlign: 'right'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={
              !TotalAllowance
                ? TotalAllowance
                : FuncCommon.addPeriodTextInput(TotalAllowance)
            }
            autoCapitalize="none"
            onChangeText={AllowTravel => {
              return;
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {textAlign: 'right'}]}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            value={
              !row.Quantity
                ? row.Quantity
                : FuncCommon.addPeriodTextInput(row.Quantity)
            }
            autoCapitalize="none"
            onChangeText={Quantity => {
              this.handleChangeRows(Quantity, index, 'Quantity');
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={Note => {
              this.handleChangeRows(Note, index, 'Note');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td_custom, {width: 80}]}>
          <IconElement
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  helperToStringCurrency = (number, n = 2, x = 3, s = '.', c = ',') => {
    if (!number) {
      return '';
    }
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
      num = Number(number).toFixed(Math.max(0, ~~n));
    return (c ? num.replace('.', c) : num).replace(
      new RegExp(re, 'g'),
      '$&' + (s || ','),
    );
  };
  render() {
    const {isEdit, WorkOutsideId} = this.state;
    return (
      <Container style={{paddingBottom: 10, height: '100%'}}>
        <View
          style={{flex: 30}}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={{flex: 40}}>
            {!this.props.dataEdit ? (
              <TabBar_Title
                title={'Đăng ký phiếu công tác'}
                callBack={() => Actions.pop()}
              />
            ) : (
              <TabBar_Title
                title={'Chỉnh sửa phiếu công tác'}
                callBack={() => Actions.pop()}
              />
            )}
            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Mã phiếu</Text>
                  </View>
                  {isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={WorkOutsideId}
                      autoCapitalize="none"
                      onChangeText={WorkOutsideId =>
                        this.onChangeText('WorkOutsideId', WorkOutsideId)
                      }
                    />
                  )}
                  {!isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                        {WorkOutsideId}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người tạo phiếu</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Chức danh</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.JobTitleNameEmployee}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Tiêu đề phiếu <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.Title}
                    onChangeText={Title => this.onChangeText('Title', Title)}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Tên đối tác/ khách hàng (nếu có)
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.CustomerName}
                    onChangeText={CustomerName =>
                      this.onChangeText('CustomerName', CustomerName)
                    }
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Địa chỉ công tác</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.Address}
                    onChangeText={Address =>
                      this.onChangeText('Address', Address)
                    }
                  />
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>Ngày đăng ký</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.Date}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        disabled={false}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setDate(date)}
                      />
                    </View>
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Đơn hàng nội bộ</Text>
                  </View>
                  <TouchableOpacity onPress={() => this.onActionComboboxType()}>
                    <Text style={[AppStyles.FormInput, {color: 'black'}]}>
                      {this.state.OrderName
                        ? this.state.OrderName
                        : 'Chọn đơn hàng...'}
                    </Text>
                  </TouchableOpacity>
                </View>
                {/* <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Thời gian bắt đầu dự kiến
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                        <DatePicker
                locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.StartTime}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        disabled={false}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setStartTime(date)}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Thời gian kết thúc dự kiến
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                        <DatePicker
                locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.EndTime}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        disabled={false}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setEndTime(date)}
                      />
                    </View>
                  </View>
                </View> */}

                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Phương tiện</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.Vehicle}
                    onChangeText={Vehicle =>
                      this.onChangeText('Vehicle', Vehicle)
                    }
                  />
                </View> */}
                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Quãng đường</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    value={this.state.Distance}
                    onChangeText={Distance =>
                      this.onChangeText('Distance', Distance)
                    }
                  />
                </View> */}
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Phí công tác</Text>
                  </View>

                  <TextInput
                    style={[AppStyles.FormInput, {textAlign: 'right'}]}
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    value={
                      !this.state.Distance
                        ? this.state.Distance
                        : FuncCommon.addPeriodTextInput(this.state.Distance)
                    }
                    onChangeText={Distance =>
                      this.onChangeText('Distance', Distance)
                    }
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Nội dung <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    value={this.state.Description}
                    onChangeText={Description =>
                      this.onChangeText('Description', Description)
                    }
                  />
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    marginVertical: 5,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <IconElement
                        name="hdd"
                        type="antdesign"
                        color={this.state.BringAsset ? AppColors.green : 'gray'}
                        size={26}
                      />
                      <Text style={[AppStyles.Labeldefault, {marginLeft: 10}]}>
                        {this.state.BringAsset
                          ? 'Mang theo tài sản'
                          : 'Không mang theo tài sản'}
                      </Text>
                    </View>
                    <Switch
                      trackColor={{false: '#767577', true: '#81b0ff'}}
                      thumbColor={
                        this.state.BringAsset ? AppColors.ColorAdd : '#f4f3f4'
                      }
                      ios_backgroundColor="#3e3e3e"
                      onValueChange={BringAsset =>
                        this.onChangeText('BringAsset', BringAsset)
                      }
                      value={this.state.BringAsset}
                    />
                  </View>
                </View>
                <View style={{marginTop: 5}}>
                  <View style={{width: 150, marginLeft: 10}}>
                    <Button
                      title="Thêm dòng"
                      type="outline"
                      size={15}
                      onPress={() => this.addRows()}
                      buttonStyle={{padding: 5, marginBottom: 5}}
                      titleStyle={{marginLeft: 5}}
                      icon={
                        <IconElement
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>

                  {/* Table */}
                  <ScrollView horizontal={true}>
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        {headerTable.map(item => (
                          <TouchableOpacity
                            key={item.title}
                            style={[AppStyles.table_th, {width: item.width}]}>
                            <Text style={AppStyles.Labeldefault}>
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        ))}
                      </View>
                      <View
                      // style={{
                      //   maxHeight: Dimensions.get('window').height / 7,
                      // }}
                      >
                        {this.state.rows.length > 0 ? (
                          <FlatList
                            data={this.state.rows}
                            renderItem={({item, index}) => {
                              return this.itemFlatList(item, index);
                            }}
                            keyExtractor={(rs, index) => index.toString()}
                          />
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'left'},
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View>
                {/* attachment */}
                {!this.props.dataEdit && (
                  <View
                    style={{
                      flexDirection: 'column',
                      padding: 10,
                      marginTop: 5,
                    }}>
                    <View style={{flexDirection: 'row', marginBottom: 5}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                        <Text style={{color: 'red'}}> </Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          flexDirection: 'row',
                        }}
                        onPress={() => this.openAttachment()}>
                        <IconElement
                          name="attachment"
                          type="entypo"
                          size={15}
                          color={AppColors.ColorAdd}
                        />
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {color: AppColors.ColorAdd},
                          ]}>
                          {' '}
                          Chọn file
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <Divider style={{marginBottom: 10}} />
                    {this.state.Attachment.length > 0
                      ? this.state.Attachment.map((para, i) => (
                          <View
                            key={i}
                            style={[
                              styles.StyleViewInput,
                              {flexDirection: 'row', marginBottom: 3},
                            ]}>
                            <View
                              style={[
                                AppStyles.containerCentered,
                                {padding: 10},
                              ]}>
                              <Image
                                style={{
                                  width: 30,
                                  height: 30,
                                  borderRadius: 10,
                                }}
                                source={{
                                  uri: this.FileAttackments.renderImage(
                                    para.FileName,
                                  ),
                                }}
                              />
                            </View>
                            <TouchableOpacity
                              key={i}
                              style={{flex: 1, justifyContent: 'center'}}>
                              <Text style={AppStyles.Textdefault}>
                                {para.FileName}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[
                                AppStyles.containerCentered,
                                {padding: 10},
                              ]}
                              onPress={() => this.removeAttactment(para)}>
                              <IconElement
                                name="close"
                                type="antdesign"
                                size={20}
                                color={AppColors.ColorDelete}
                              />
                            </TouchableOpacity>
                          </View>
                        ))
                      : null}
                  </View>
                )}
              </View>
            </ScrollView>
          </View>
        </View>

        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
        <OpenPhotoLibrary
          callback={this.callbackLibarary}
          openLibrary={this.openLibrary}
        />
        <ComboboxV2
          callback={this.ChoiceAtt}
          data={listCombobox.ListComboboxAtt}
          eOpen={this.openCombobox_Att}
        />
        {this.state.modalPickerTimeVisible && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisible}
            mode="time"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        {this.state.modalPickerTimeVisibleDate && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisibleDate}
            mode="date"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        {this.state.ListOrder.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeType}
            data={this.state.ListOrder}
            nameMenu={'Chọn đơn hàng'}
            eOpen={this.openComboboxType}
            position={'bottom'}
            value={undefined}
          />
        ) : null}
      </Container>
    );
  }
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.setState({
      Vehicle: rs.value,
      OrderName: rs.text,
    });
  };
  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  setTitle(Title) {
    this.setState({Title});
  }
  setDate(Date) {
    this.setState({Date: FuncCommon.ConDate(Date, 99)});
  }
  setStartTime(StartTime) {
    this.setState({StartTime: StartTime});
  }
  setEndTime(EndTime) {
    this.setState({EndTime: EndTime});
  }
  onPressBack() {
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  //#region  customFloat
  customFloat = val => {
    if (val !== null) {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        return valview + ',' + string[1].substring(0, 2);
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ApplyOutsidesAdd_Component);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 10,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
