import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, View, TouchableOpacity, ScrollView } from "react-native";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import API_ListPO from "../../../network/BM/API_ListPO";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';

class ListPOComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 25,
                Search: "",
                QueryOrderBy: "Ponumber desc",
                StartDate: "2020-01-01T02:13:13.779Z",
                EndDate: "2020-12-31T02:13:13.779Z",
                POType: null,
                IsManager: null,
                PurchaseFrom: null,
                ObjectId: null
            },
            ChoDuyet: null,
            selectedTab: 'profile',
            openSearch: false,
            refreshing: false
        };
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_ListPO.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data).data) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        this.GetAll();
    }

    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                POType: this.state.staticParam.POType,
                IsManager: this.state.staticParam.IsManager,
                PurchaseFrom: this.state.staticParam.PurchaseFrom,
                ObjectId: this.state.staticParam.ObjectId
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'PO chờ thanh toán'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'BM'}
                />
                <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, borderWidth: 0.5, flexDirection: 'row' }}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{}}>
                        <TouchableOpacity onPress={() => this.OnChangeAll()} style={styles.TabarPending}>
                            <Text style={this.state.staticParam.POType == null ? { color: AppColors.Maincolor } : { color: 'black' }}>Tất cả</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.OnChangeTM()} style={styles.TabCenter}>
                            <Text style={this.state.staticParam.POType == 1 ? { color: AppColors.Maincolor } : { color: 'black' }}>Thương mại</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.OnChangeSX()} style={styles.TabCenter}>
                            <Text style={this.state.staticParam.POType == 2 ? { color: AppColors.Maincolor } : { color: 'black' }}>Sản xuất</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.OnChangeGCN()} style={styles.TabCenter}>
                            <Text style={this.state.staticParam.POType == 3 ? { color: AppColors.Maincolor } : { color: 'black' }}>GC ngoài</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.OnChangeHTSX()} style={styles.TabarApprove}>
                            <Text style={this.state.staticParam.POType == 4 ? { color: AppColors.Maincolor } : { color: 'black' }}>Hỗ trợ SX</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
                {this.state.openSearch == true ?
                    <FormSearch
                        CallbackSearch={(callback) => this.updateSearch(callback)}
                    />
                    : null}
                <View style={{ flex: 14 }}>
                    <FlatList
                        style={{ height: '100%' }}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family }}>
                                <ListItem
                                    title={item.Ponumber}
                                    subtitle={() => {
                                        return (
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ width: '50%' }}>
                                                    <Text style={AppStyles.Textdefault}>Nhân viên: {item.EmployeeName}</Text>
                                                    <Text style={AppStyles.Textdefault}>Loại đơn hàng: {item.POType == 1 ? 'Thương mại' : item.POType == 2 ? 'Sản xuất' : item.POType == 3 ? 'Gia công ngoài' : item.POType == 4 ? 'Hỗ trợ sản xuất' : ''}</Text>
                                                </View>
                                                <View style={{ width: '50%' }}>
                                                    <Text style={AppStyles.Textdefault}>Ngày đặt hàng: {this.customDate(item.Podate)}</Text>
                                                    <Text style={AppStyles.Textdefault}>Ngày duyệt: {this.customDate(item.ApprovedDate)}</Text>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    titleStyle={AppStyles.Titledefault}
                                    //chevron
                                    onPress={() => this.openView(item.PurchaseOrderGuid)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                </View>
            </View>
        );
    }

    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                POType: this.state.staticParam.POType,
                IsManager: this.state.staticParam.IsManager,
                PurchaseFrom: this.state.staticParam.PurchaseFrom,
                ObjectId: this.state.staticParam.ObjectId
            }
        });
        this.GetAll();
    }//Load lại
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.GetAll();
    };
    onPressHome() {
        Actions.app();
    }
    //Lọc loại đề nghị
    OnChangeAll() {
        this.state.list = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        this.state.staticParam.POType = null;
        this.setState({
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                POType: this.state.staticParam.POType,
                IsManager: this.state.staticParam.IsManager,
                PurchaseFrom: this.state.staticParam.PurchaseFrom,
                ObjectId: this.state.staticParam.ObjectId
            }
        });
        this.GetAll();
    }
    OnChangeTM() {
        this.state.list = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        this.state.staticParam.POType = 1;
        this.setState({
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                POType: this.state.staticParam.POType,
                IsManager: this.state.staticParam.IsManager,
                PurchaseFrom: this.state.staticParam.PurchaseFrom,
                ObjectId: this.state.staticParam.ObjectId
            }
        });
        this.GetAll();
    }
    OnChangeSX() {
        this.state.list = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        this.state.staticParam.POType = 2;
        this.setState({
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                POType: this.state.staticParam.POType,
                IsManager: this.state.staticParam.IsManager,
                PurchaseFrom: this.state.staticParam.PurchaseFrom,
                ObjectId: this.state.staticParam.ObjectId
            }
        });
        this.GetAll();
    }
    OnChangeGCN() {
        this.state.list = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        this.state.staticParam.POType = 3;
        this.setState({
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                POType: this.state.staticParam.POType,
                IsManager: this.state.staticParam.IsManager,
                PurchaseFrom: this.state.staticParam.PurchaseFrom,
                ObjectId: this.state.staticParam.ObjectId
            }
        });
        this.GetAll();
    }
    OnChangeHTSX() {
        this.state.list = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        this.state.staticParam.POType = 4;
        this.setState({
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                POType: this.state.staticParam.POType,
                IsManager: this.state.staticParam.IsManager,
                PurchaseFrom: this.state.staticParam.PurchaseFrom,
                ObjectId: this.state.staticParam.ObjectId
            }
        });
        this.GetAll();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }

    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
    openView(id) {
        Actions.bmitemPurchaseOrders({ RecordGuid: id });
    }
}

const styles = StyleSheet.create({
    TabarPending: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        width: 80,
        paddingTop: 10,
        paddingBottom: 10
    },
    TabarApprove: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        width: 80,
        paddingTop: 10,
        paddingBottom: 10
    },
    TabCenter: {
        flex: 1,
        width: 80,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#C0C0C0',
        borderWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 10
    }
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListPOComponent);
