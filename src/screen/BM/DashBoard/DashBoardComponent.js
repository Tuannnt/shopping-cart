import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, ListItem, SearchBar, Badge} from 'react-native-elements';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Dimensions,
  Button,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import API_HomeBM from '../../../network/BM/API_HomeBM';
import {FlatGrid} from 'react-native-super-grid';
import {DatePicker, Container, Content, Item, Label, Picker} from 'native-base';
import {ECharts} from 'react-native-echarts-wrapper';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import RNPickerSelect from 'react-native-picker-select';
import Combobox from '../../component/Combobox';

var width = Dimensions.get('window').width; //full width
class DashBoardComponent extends Component {
  constructor(props) {
    super(props);
    (this.optionchart = {
      grid: {
        left: '3%',
        right: '4%',
        containLabel: true,
      },
      xAxis: {
        type: 'category',
        data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          data: [],
          type: 'bar',
          color: 'rgb(255, 117, 63)',
          textStyle: {
            size: 10,
          },
          label: {
            show: true,
            position: 'top',
            color: 'black',
            fontSize: 10,
            marginTop: 0,
          },
        },
      ],
    }),
      (this.state = {
        list: [
          {
            Title: 'Số đề nghị mua',
            Icon: 'file-document-outline',
            type: 'material-community',
            value: 0,
          },
          {
            Title: 'Số đơn đặt hàng ',
            Icon: 'shoppingcart',
            type: 'antdesign',
            value: 0,
          },
          {
            Title: 'Đề nghị kiểm tra',
            Icon: 'checkcircleo',
            type: 'antdesign',
            value: 0,
          },
          {
            Title: 'ĐN thanh toán',
            Icon: 'book',
            type: 'font-awesome-5',
            value: 0,
          },
          {
            Title: 'Đề nghị tạm ứng',
            Icon: 'address-book',
            type: 'foundation',
            value: 0,
          },
          {
            Title: 'PO chờ thanh toán',
            Icon: 'ticket',
            type: 'foundation',
            value: 0,
          },
        ],
        Dropdown: [
          {
            value: 'Year',
            text: 'Năm',
          },
          {
            value: 'Week',
            text: 'Tuần',
          },
          {
            value: 'Quarter',
            text: 'Quý',
          },
        ],
        selected: undefined,
        staticParam: {
          Year: 2020,
          Type: 'Year',
          IsLock: true,
        },
      });
  }

  CountMenu = () => {
    API_HomeBM.HomeReport()
      .then(res => {
        this.setState({
          list: [
            {
              Title: 'Số đề nghị mua',
              Icon: 'file-document-outline',
              type: 'material-community',
              value: JSON.parse(res.data.data).ProposedPurchases,
              key: 'ProposedPurchases',
            },
            {
              Title: 'Số đơn đặt hàng ',
              Icon: 'shoppingcart',
              type: 'antdesign',
              value: JSON.parse(res.data.data).PurchaseOrders,
              key: 'PurchaseOrders',
              //   key: '',
            },
            {
              Title: 'Đề nghị kiểm tra',
              Icon: 'checkcircleo',
              type: 'antdesign',
              value: JSON.parse(res.data.data).CheckRequests,
              // key: 'CheckRequests'
              key: '',
            },
            {
              Title: 'ĐN thanh toán',
              Icon: 'book',
              type: 'font-awesome-5',
              value: JSON.parse(res.data.data).POPaymentsAfter,
              // key: 'POPaymentsAfterBM',
              key: '',
            },
            {
              Title: 'Đề nghị tạm ứng',
              Icon: 'address-book',
              type: 'foundation',
              value: JSON.parse(res.data.data).POPaymentsBefore,
              // key: 'POPaymentsBeforeBM',
              key: '',
            },
            {
              Title: 'PO chờ thanh toán',
              Icon: 'ticket',
              type: 'foundation',
              value: JSON.parse(res.data.data).ListPO,
              // key: 'ListPurchaseOrders'
              key: '',
            },
          ],
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  onRef = ref => {
    if (ref) {
      this.chart = ref;
    }
  };
  GetDataChart = () => {
    API_HomeBM.TKDashboard(this.state.staticParam)
      .then(res => {
        let obj = JSON.parse(res.data.data);
        if (this.state.staticParam.Type == 'Year') {
          var _option = this.optionchart;
          _option.xAxis.data = [
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11',
            '12',
          ];
          _option.series[0].data = [
            obj[0].Data1,
            obj[0].Data2,
            obj[0].Data3,
            obj[0].Data4,
            obj[0].Data5,
            obj[0].Data6,
            obj[0].Data7,
            obj[0].Data8,
            obj[0].Data9,
            obj[0].Data10,
            obj[0].Data11,
            obj[0].Data12,
          ];
          this.optionchart = _option;
        } else if (this.state.staticParam.Type == 'Week') {
          var _option = this.optionchart;
          _option.xAxis.data = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];
          _option.series[0].data = [
            obj[0].Data1,
            obj[0].Data2,
            obj[0].Data3,
            obj[0].Data4,
            obj[0].Data5,
            obj[0].Data6,
            obj[0].Data7,
          ];
          this.optionchart = _option;
        } else if (this.state.staticParam.Type == 'Quarter') {
          var _option = this.optionchart;
          _option.xAxis.data = ['1', '2', '3', '4'];
          _option.series[0].data = [
            obj[0].Data1,
            obj[0].Data2,
            obj[0].Data3,
            obj[0].Data4,
          ];
          this.optionchart = _option;
        }
        this.chart.setOption(this.optionchart);
      })
      .catch(error => {
        console.log(error);
      });
  };
  componentDidMount(): void {
    this.CountMenu();
    // this.GetDataChart();
  }

  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar title={'Dashboard'} BackModuleByCode={'BM'} />
        <View style={{flex: 1, flexDirection: 'column'}}>
          <View style={{}}>
            <FlatGrid
              itemDimension={100}
              items={this.state.list}
              style={styles.gridView}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => this.openPage(item.key)}
                  style={{
                    borderRadius: 10,
                    backgroundColor: '#FFFFFF',
                    height: 80,
                    borderColor: AppColors.Maincolor,
                    borderWidth: 1,
                  }}>
                  <View style={[styles.itemContainerdetail]}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <Icon
                          name={item.Icon}
                          type={item.type}
                          size={20}
                          color={AppColors.Maincolor}
                        />
                      </View>
                      <View style={{flex: 1}}>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            fontSize: 17,
                            textAlign: 'right',
                            paddingRight: 5,
                          }}>
                          {item.value}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {
                            textAlign: 'center',
                            paddingBottom: 5,
                            paddingTop: 5,
                          },
                        ]}>
                        {item.Title}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
              onPress
            />
          </View>
          {/* <View
            style={{
              flexDirection: 'row',
              paddingRight: 10,
              backgroundColor: '#fff',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={[AppStyles.Titledefault, {paddingLeft: 10}]}>
                Tổng chi phí
              </Text>
            </View>
            <TouchableOpacity
              style={[AppStyles.FormInput, {flex: 1, flexDirection: 'row'}]}
              onPress={() => this.onActionCombobox()}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.state.dropdownName == ''
                    ? {color: AppColors.gray, flex: 3}
                    : {flex: 3},
                ]}>
                {this.state.dropdownName !== ''
                  ? this.state.dropdownName
                  : 'Chọn...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  color="#888888"
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                />
              </View>
            </TouchableOpacity>
          </View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{height: '100%', width: width}}>
              <ECharts option={this.optionchart} ref={this.onRef} />
            </View>
          </ScrollView> */}
        </View>
        <Combobox
          value={this.state.staticParam.Type}
          TypeSelect={'single'} // single or multiple
          callback={this.ChangeDropDown}
          data={this.state.Dropdown}
          nameMenu={'Chọn'}
          eOpen={this.openCombobox}
          position={'bottom'}
        />
      </View>
    );
  }
  ChangeDropDown = value => {
    if (value.value === 'Year') {
      this.state.staticParam.Type = value.value;
      this.setState({
        dropdownName: value.text,
        staticParam: {
          Year: this.state.staticParam.Year,
          Type: this.state.staticParam.Type,
          IsLock: this.state.staticParam.IsLock,
        },
      });
      this.GetDataChart();
    } else if (value.value === 'Quarter') {
      this.state.staticParam.Type = value.value;
      this.setState({
        dropdownName: value.text,
        staticParam: {
          Year: this.state.staticParam.Year,
          Type: this.state.staticParam.Type,
          IsLock: this.state.staticParam.IsLock,
        },
      });
      this.GetDataChart();
    } else if (value.value === 'Week') {
      this.state.staticParam.Type = value.value;
      this.setState({
        dropdownName: value.text,
        staticParam: {
          Year: this.state.staticParam.Year,
          Type: this.state.staticParam.Type,
          IsLock: this.state.staticParam.IsLock,
        },
      });
      this.GetDataChart();
    }
  };
  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCombobox() {
    this._openCombobox();
  }
  onPressHome() {
    Actions.app();
  }
  openPage(value) {
    if (!value) {
      return;
    }
    if (value === 'ProposedPurchases') {
      Actions.ProposedPurchases();
    } else if (value === 'PurchaseOrders') {
      Actions.PurchasingOrder();
    } else if (value === 'CheckRequests') {
      Actions.CheckRequests();
    } else if (value === 'POPaymentsAfterBM') {
      Actions.POPaymentsAfterBM();
    } else if (value === 'POPaymentsBeforeBM') {
      Actions.POPaymentsBeforeBM();
    } else if (value === 'ListPurchaseOrders') {
      Actions.ListPurchaseOrders();
    }
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  openView(id) {
    Actions.smitemCustomers({RecordGuid: id});
  }
}

const styles = StyleSheet.create({
  itemContainerdetail: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 17,
    flexDirection: 'column',
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashBoardComponent);
