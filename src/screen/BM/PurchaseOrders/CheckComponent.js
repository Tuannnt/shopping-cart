import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Platform,
  ScrollView,
  TextInput,
  Switch,
  Keyboard,
  KeyboardAvoidingView,
  FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import { API_PurchaseOrders, API_TicketRequests, API } from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import { Button, ListItem, Icon, Divider } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { FuncCommon } from '../../../utils';
import RNPickerSelect from 'react-native-picker-select';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import Combobox from '../../component/Combobox';
import LoadingComponent from '../../component/LoadingComponent';
import RequiredText from '../../component/RequiredText';
import _ from 'lodash';
import Toast from 'react-native-simple-toast';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const styles = StyleSheet.create({
  totalText: { textAlign: 'right', paddingRight: 5, fontWeight: 'bold' },
  hidden: {
    display: 'none',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 90,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 90,
  },
});
const headerTable = [
  { title: 'STT', width: 40 },
  { title: 'Mã Đặt Hàng', width: 200 },
  { title: 'Tên Đặt Hàng', width: 300 },
  { title: 'ĐVT', width: 70 },
  { title: 'Bộ phận kiểm tra', width: 250 },
  { title: 'S.L Mua', width: 100, name: 'buyQuantity' },
  { title: 'S.L Đề nghị', width: 100 },
  { title: 'S.L Còn lại', width: 100, name: 'QuantityReceipt' },
  { title: 'Giá Mua', width: 100 },
  { title: 'Thành tiền', width: 200, name: 'total' },
  { title: 'Ngày giao', width: 200 },
  { title: 'Tiêu chuẩn kĩ thuật', width: 200 },
  { title: 'Kế hoạch giao hàng', width: 200 },
  { title: 'Kho', width: 300 },
  { title: 'Hành động', width: 80 },
];
class CheckComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [{}],
      ListVoucherType: [
        {
          text: 'Nhập mua',
          value: 'N',
        },
        {
          text: 'Nhập hàng bán bị trả lại',
          value: 'L',
        },
        {
          text: 'Nhập thành phẩm sản xuất',
          value: 'M',
        },
        {
          text: 'Nhập khác',
          value: 'O',
        },
      ],
      selectItem: null,
      position: null,
      dVoucherType: '',
      dCustomers: '',
      tabTTC: true,
      tabCT: false,
      ListCustomers: [],
      ListTicketRequestDetail: [],
      TicketRequestNo: '',
      RequestType: 'N',
      Title: '',
      RequestDate: new Date(),
      EmployeeName: '',
      DepartmentName: '',
      VoucherType: 'N',
      ObjectGuid: null,
      ObjectId: '',
      ObjectName: '',
      ObjectAddress: '',
      Description: '',
      ItemGuid: null,
      ItemId: '',
      ItemName: '',
      POGuid: null,
      OrderNumberId: '',
      UnitBefore: '',
      UnitName: '',
      Quantity: 0,
      QuantityActual: 0,
      UnitPriceBefore: 0,
      Amount: 0,
      SortOrder: 0,
      VoucherGuid: null,
      WarehouseId: '',
      PurchaseOrderId: '',
      editItem: null,
      pageItems: 1,
      pageCustomers: 1,
      length: 20,
      keyWordItems: '',
      keyWordCustomers: '',
      modelauto: {
        Value: null,
        IsEdit: null,
      },
      loading: true,
      listEmp: [],
      empMH: null,
      listDep: [],
      listPlanDate: [],
      listWarehouse: [],
    };
    this.state.dataMenu = [
      {
        key: 'delete',
        name: 'Xóa',
        icon: {
          name: 'delete',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
      // {
      //     key: 'edit',
      //     name: 'Sửa',
      //     icon: {
      //         name: 'edit',
      //         type: 'antdesign',
      //         color: AppColors.Maincolor,
      //     }
      // }
    ];
  }

  componentDidMount() {
    this.setState({
      note: this.props.DataView.Note || '',
      EmployeeName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
      DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
    });
    // this.GetPODetailsCheck();
    // this.getEmployees();
    // this.GetCustomersAll();
    Promise.all([
      this.GetPlanDate(),
      this.GetTRNumberAuto(),
      this.GetDepartmentsJexcel(),
      this.GetWarehousesJexcel(),
      this.GetDetail(),
    ]);
  }
  GetDetail = () => {
    let obj = {
      PurchaseOrderGuid: this.props.DataView.PurchaseOrderGuid,
    };
    API_PurchaseOrders.PurchaseOrdersDetails_ListDetail(obj)
      .then(res => {
        let listDetail = JSON.parse(res.data.data);
        this.setState({
          rows: listDetail || [],
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };
  getEmployees = () => {
    debugger;
    API_PurchaseOrders.GetEmployeesMH()
      .then(res => {
        let _data = JSON.parse(res.data.data).map(x => ({
          value: x.EmployeeGuid,
          label: x.EmployeeId + ' ' + x.FullName,
        }));
        _data.unshift({
          label: 'Chọn',
          value: '',
        });
        this.setState({ listEmp: _data });
      })
      .catch(err => {
        console.log(err.data.data);
      });
  };
  GetDepartmentsJexcel = () => {
    API_PurchaseOrders.GetDepartmentsJexcel()
      .then(res => {
        let _data = JSON.parse(res.data.data).map(x => ({
          value: x.id,
          text: x.name,
        }));
        _data.unshift({
          text: 'Bỏ chọn',
          value: '',
        });
        this.setState({ listDep: _data });
      })
      .catch(err => {
        console.log(err.data.data);
      });
  };
  GetWarehousesJexcel = () => {
    API_PurchaseOrders.GetWarehousesJexcel()
      .then(res => {
        let _data = JSON.parse(res.data.data).map(x => ({
          value: x.id,
          label: x.name,
        }));
        this.setState({ listWarehouse: _data });
      })
      .catch(err => {
        console.log(err.data.data);
      });
  };
  GetPlanDate = () => {
    let obj = {
      PurchaseOrderGuid: this.props.DataView.PurchaseOrderGuid,
    };
    API_PurchaseOrders.GetPlanDate(obj)
      .then(res => {
        console.log(res);
        let _data = JSON.parse(res.data.data).map(x => ({
          value: x.id,
          label: x.name,
        }));
        this.setState({ listPlanDate: _data });
      })
      .catch(err => {
        console.log(err.data.data);
      });
  };
  GetPODetailsCheck() {
    let id = this.props.RecordGuid;
    API_PurchaseOrders.GetPODetailsCheck(id)
      .then(res => {
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
          var val = data[i];
          this.state.ListTicketRequestDetail.push({
            PodetailGuid: val.PodetailGuid,
            ProposedPurchaseGuid: val.ProposedPurchaseGuid,
            ProposedPurchaseId: val.ProposedPurchaseId,
            ItemGuid: val.ItemGuid,
            ItemId: val.ItemId,
            ItemName: val.ItemName,
            UnitId: val.UnitId,
            UnitName: val.UnitName,
            MakerId: val.MakerId,
            MarkerName: val.MarkerName,
            Quantity: (
              parseFloat(val.Quantity) - parseFloat(val.QuantityReceipt)
            ).toString(),
            QuantityCL: (
              parseFloat(val.Quantity) - parseFloat(val.QuantityReceipt)
            ).toString(),
            UnitPriceBefore: val.UnitPrice,
            IsQccomfirm: 0,
            Amount: val.Amount,
            DateNeed: val.DateNeed,
            DeliveryDate: val.DeliveryDate,
            WarehouseDate: val.WarehouseDate,
            OrderNumber: val.OrderNumber,
            Potype: val.Potype,
            PurchaseFrom: val.PurchaseFrom,
            Specification: val.Specification,
            PlanDate: val.PlanDate,
            Note: val.Note,
            Sort: i,
          });
        }
        this.setState({
          ListTicketRequestDetail: this.state.ListTicketRequestDetail,
        });
      })
      .catch(error => { });
  }
  GetTRNumberAuto() {
    API_PurchaseOrders.GetNumberAuto({
      AliasId: 'QTDSCT_DNN',
      VoucherType: 'N',
    })
      .then(rs => {
        console.log(rs);
        if (
          rs.data.data !== null &&
          rs.data.data !== '' &&
          rs.data.data !== undefined
        ) {
          this.state.modelauto.Value = JSON.parse(rs.data.data).Value;
          this.state.modelauto.IsEdit = JSON.parse(rs.data.data).IsEdit;
          this.setState({
            TicketRequestNo: JSON.parse(rs.data.data).Value,
            modelauto: {
              Value: this.state.modelauto.Value,
              IsEdit: this.state.modelauto.IsEdit,
            },
            loading: false,
          });
        }
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  }
  GetCustomersAll() {
    var obj = {
      CurrentPage: this.state.pageCustomers,
      Length: this.state.length,
      Search: this.state.keyWordCustomers,
      VoucherType: this.state.VoucherType,
    };
    API_TicketRequests.GetCustomersAll(obj)
      .then(rs => {
        var _lstCustomers = JSON.parse(rs.data.data);
        var _reListCustomers = [
          {
            text: 'Chọn đối tượng',
            value: null,
          },
        ];
        for (var i = 0; i < _lstCustomers.length; i++) {
          _reListCustomers.push({
            text:
              _lstCustomers[i].ObjectId + ' - ' + _lstCustomers[i].ObjectName,
            value: _lstCustomers[i].ObjectGuid,
            ObjectId: _lstCustomers[i].ObjectId,
            ObjectName: _lstCustomers[i].ObjectName,
            ObjectAddress: _lstCustomers[i].Address,
          });
        }
        this.setState({
          ListCustomers: _reListCustomers,
        });
      })
      .catch(error => { });
  }
  nextpageCustomers = callback => {
    this.state.pageCustomers++;
    this.setState({
      pageCustomers: this.state.pageCustomers,
    });
    var obj = {
      CurrentPage: this.state.pageCustomers,
      Length: this.state.length,
      Search: this.state.keyWordCustomers,
      VoucherType: this.state.VoucherType,
    };
    API_TicketRequests.GetCustomersAll(obj)
      .then(rs => {
        var _lstCustomers = JSON.parse(rs.data.data);
        var _reListCustomers = [];
        for (var i = 0; i < _lstCustomers.length; i++) {
          _reListCustomers.push({
            text:
              _lstCustomers[i].ObjectId + ' - ' + _lstCustomers[i].ObjectName,
            value: _lstCustomers[i].ObjectGuid,
            ObjectId: _lstCustomers[i].ObjectId,
            ObjectName: _lstCustomers[i].ObjectName,
            ObjectAddress: _lstCustomers[i].Address,
          });
        }
        callback(_reListCustomers);
      })
      .catch(error => { });
  };
  searchCustomers = (callback, textsearch) => {
    this.setState({
      pageCustomers: 1,
      keyWordCustomers: textsearch,
    });
    var obj = {
      CurrentPage: this.state.pageCustomers,
      Length: this.state.length,
      Search: this.state.keyWordCustomers,
      VoucherType: this.state.VoucherType,
    };
    API_TicketRequests.GetCustomersAll(obj)
      .then(rs => {
        var _lstCustomers = JSON.parse(rs.data.data);
        var _reListCustomers = [];
        for (var i = 0; i < _lstCustomers.length; i++) {
          _reListCustomers.push({
            text:
              _lstCustomers[i].ObjectId + ' - ' + _lstCustomers[i].ObjectName,
            value: _lstCustomers[i].ObjectGuid,
            ObjectId: _lstCustomers[i].ObjectId,
            ObjectName: _lstCustomers[i].ObjectName,
            ObjectAddress: _lstCustomers[i].Address,
          });
        }
        callback(_reListCustomers);
      })
      .catch(error => { });
  };
  getWidth = index => {
    return headerTable[index].width;
  };
  CustomTTC = () => (
    <View style={{ flexDirection: 'column', flex: 1 }}>
      {/* Số phiếu */}
      <View style={{ flexDirection: 'row' }}>
        <View style={{ padding: 10, flex: 1 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={AppStyles.Labeldefault}>
              Số phiếu <RequiredText />
            </Text>
            {/* <Text style={{ color: 'red' }}> *</Text> */}
          </View>
          <TextInput
            style={AppStyles.FormInput}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            multiline={true}
            value={this.state.TicketRequestNo}
            editable={this.state.modelauto.IsEdit}
          />
        </View>
        {Platform.OS === 'ios' ? (
          <View style={{ padding: 10, flex: 1 }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={AppStyles.Labeldefault}>Ngày yêu cầu</Text>
              {/* <Text style={{ color: 'red' }}> *</Text> */}
            </View>
            {/* <View style={AppStyles.FormInput}> */}
            <TextInput
              style={[AppStyles.FormInput]}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              multiline={true}
              value={FuncCommon.ConDate(this.state.RequestDate, 0)}
              editable={false}
            />
          </View>
        ) : (
          <View style={{ padding: 10, flex: 1 }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={AppStyles.Labeldefault}>Ngày yêu cầu</Text>
              {/* <Text style={{ color: 'red' }}> *</Text> */}
            </View>
            {/* <View style={AppStyles.FormInput}> */}
            <View style={{ flexDirection: 'row' }}>
              <TextInput
                style={[AppStyles.FormInput, { flex: 4 }]}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                multiline={true}
                value={FuncCommon.ConDate(this.state.RequestDate, 0)}
                editable={false}
              />
              <View style={{ flex: 1 }}>
                <DatePicker
                  locale="vie"
                  locale="vie"
                  style={{ width: 150 }}
                  date={this.state.RequestDate}
                  androidMode="spinner"
                  mode="date"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      marginLeft: 36,
                    },
                  }}
                  hideText={true}
                  onDateChange={date => {
                    this.setState({ RequestDate: date });
                  }}
                  disabled
                />
              </View>
            </View>
          </View>
        )}
      </View>
      {/* Người yêu cầu */}
      <View style={{ padding: 10 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={AppStyles.Labeldefault}>Người đề nghị</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.state.EmployeeName}
          editable={false}
        />
      </View>

      <View style={{ padding: 10 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={AppStyles.Labeldefault}>Số đơn hàng</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.props.DataView.Ponumber}
          editable={false}
        />
      </View>
      {/* Bộ phận */}
      <View style={{ padding: 10 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={AppStyles.Labeldefault}>Bộ phận</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.state.DepartmentName}
          editable={false}
        />
      </View>
      <View style={{ padding: 10 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={AppStyles.Labeldefault}>
            Diễn giải <RequiredText />
          </Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.state.note}
          onChangeText={Note => this.onChangeText('note', Note)}
        />
      </View>
      <View
        style={{
          padding: 10,
          paddingBottom: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text style={[AppStyles.Labeldefault, { marginLeft: 5 }]}>
              {'Kiểm tra tất cả'}
            </Text>
          </View>
          <Switch
            trackColor={{ false: '#767577', true: '#81b0ff' }}
            thumbColor={this.state.CheckAll ? AppColors.ColorAdd : '#f4f3f4'}
            ios_backgroundColor="#3e3e3e"
            onValueChange={CheckAll => this.onChangeCheckAll(CheckAll)}
            value={this.state.CheckAll}
          />
        </View>
      </View>
      {/* <View style={{padding: 10, paddingBottom: 0}}>
        <Text style={AppStyles.Labeldefault}>Thủ kho</Text>
        <View
          style={[{...AppStyles.FormInputPicker, justifyContent: 'center'}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value =>
              this.setState({
                empMH: value,
              })
            }
            items={this.state.listEmp}
            value={this.state.empMH}
            placeholder={{}}
            style={{
              inputIOS: {
                padding: 10,
                color: 'black',
                fontSize: 14,
                paddingRight: 15, // to ensure the text is never behind the icon
              },
              inputAndroid: {
                padding: 10,
                color: 'black',
                fontSize: 14,
                paddingRight: 15, // to ensure the text is never behind the icon
              },
            }}
            // useNativeAndroidPickerStyle={false}
          />
        </View>
      </View> */}
    </View>
  );
  onChangeText = (key, value) => {
    this.setState({ [key]: value });
  };
  onChangeCheckAll = value => {
    let rows = _.cloneDeep(this.state.rows);
    rows.map(row => {
      if (value === true) {
        row.QuantityOk = +row.Quantity - +row.QuantityReceipt + '';
      } else {
        row.QuantityOk = '0';
      }
      return row;
    });
    this.setState({ CheckAll: value, rows });
  };
  CustomCT = () => (
    <ScrollView horizontal={false} showsHorizontalScrollIndicator={true}>
      {this.state.ListTicketRequestDetail.length > 0
        ? this.state.ListTicketRequestDetail.map((item, index) => (
          <ListItem
            title={() => {
              return (
                <TouchableOpacity style={{ flexDirection: 'row' }}>
                  <Text
                    style={[
                      AppStyles.Titledefault,
                      { width: '100%', flex: 10 },
                    ]}>
                    {item.ItemName}
                  </Text>
                  <View style={{ flex: 1 }}>
                    <Icon
                      onPress={() => this.onAction(item, index)}
                      color="#888888"
                      name={'ellipsis1'}
                      type="antdesign"
                    />
                  </View>
                </TouchableOpacity>
              );
            }}
            subtitle={() => {
              return (
                <TouchableOpacity style={{ height: 50 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={[AppStyles.Textdefault, { flex: 1 }]}>
                      <Text>Mã: {item.ItemId}</Text>
                    </View>
                    <View style={[AppStyles.Textdefault, { flex: 1 }]}>
                      <Text>ĐVT: {item.UnitName}</Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <Text style={[AppStyles.Textdefault]}>
                        SL đề nghị:{' '}
                      </Text>
                      <TextInput
                        underlineColorAndroid="transparent"
                        keyboardType="numeric"
                        autoCapitalize="none"
                        multiline={true}
                        value={item.Quantity}
                        onChangeText={value => this.setQuantity(value, index)}
                      />
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <Text style={[AppStyles.Textdefault]}>
                        SL còn lại:{' '}
                      </Text>
                      <TextInput
                        underlineColorAndroid="transparent"
                        keyboardType="numeric"
                        autoCapitalize="none"
                        multiline={true}
                        value={item.QuantityCL}
                        editable={false}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              );
            }}
            bottomDivider
            titleStyle={AppStyles.Titledefault}
          />
        ))
        : null}
    </ScrollView>
  );
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{ flex: 1 }}>
        <View style={[AppStyles.container, { flex: 1 }]}>
          <TabBar_Title
            title={'Yêu cầu kiểm tra'}
            callBack={() => this.onPressBack()}
            //FormQR={true}
            //QRMultiple={true}
            IconSubmit={false}
          //CallbackIconSubmit={() => this.AddDetail()}
          />
          {/* <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, flex: 1, borderWidth: 0.5, flexDirection: 'row', height: 40 }}>
                                <TouchableOpacity onPress={() => this.OnChangeTab('1')} style={styles.TabarPending}>
                                    <Text style={this.state.tabTTC === true ? { color: AppColors.Maincolor } : { color: 'black' }}>Thông tin chung</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.OnChangeTab('2')} style={styles.TabarApprove}>
                                    <Text style={this.state.tabCT === true ? { color: AppColors.Maincolor } : { color: 'black' }}>Chi tiết</Text>
                                </TouchableOpacity>
                            </View> */}
          {/* hiển thị thông tin chung */}
          <ScrollView>
            <View style={{ flex: 1 }}>{this.CustomTTC()}</View>
            <Divider />
            <Text style={{ fontSize: 16, fontWeight: 'bold', padding: 10 }}>
              Chi tiết phiếu
            </Text>
            {/* Table */}
            <View style={{ marginBottom: 20 }}>
              <ScrollView horizontal={true} style={{ marginTop: 5 }}>
                <View style={{ flexDirection: 'column', marginHorizontal: 10 }}>
                  <View style={{ flexDirection: 'row' }}>
                    {headerTable.map(item => {
                      if (item.hideInDetail) {
                        return null;
                      }
                      return (
                        <TouchableOpacity
                          key={item.title}
                          style={[AppStyles.table_th, { width: item.width }]}>
                          <Text style={AppStyles.Labeldefault}>
                            {item.title}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                  <View>
                    {this.state.rows.length > 0 ? (
                      <View>
                        <FlatList
                          data={this.state.rows}
                          renderItem={({ item, index }) => {
                            return this.itemFlatList(item, index);
                          }}
                          keyExtractor={(rs, index) => index.toString()}
                        />
                        {this.rowTotal()}
                      </View>
                    ) : (
                      <TouchableOpacity style={[AppStyles.table_foot]}>
                        <Text
                          style={[AppStyles.Textdefault, { textAlign: 'left' }]}>
                          Không có dữ liệu!
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              </ScrollView>
            </View>
            {/* Submit*/}
          </ScrollView>
          <View>
            <Button
              buttonStyle={{ backgroundColor: AppColors.ColorButtonSubmit }}
              title="Lưu"
              onPress={() => this.Submit()}
            />
          </View>
          <MenuActionCompoment
            callback={this.actionMenuCallback}
            data={this.state.dataMenu}
            name={this.state.NameMenu}
            eOpen={this.openMenu}
            position={'bottom'}
          />
          <Combobox
            value={this.state.VoucherType}
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeVoucherType}
            data={this.state.ListVoucherType}
            nameMenu={'Chọn nội dung nhập'}
            eOpen={this.openComboboxVoucherType}
            position={'bottom'}
          />
          {this.state.ListCustomers.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeCustomers}
              data={this.state.ListCustomers}
              nameMenu={'Chọn đối tượng'}
              eOpen={this.openComboboxCustomers}
              position={'bottom'}
              paging={true}
              callback_Paging={callBack => this.nextpageCustomers(callBack)}
              callback_SearchPaging={(callback, textsearch) =>
                this.searchCustomers(callback, textsearch)
              }
            />
          ) : null}
          {this.state.listDep.length > 0 && (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeEmp}
              data={this.state.listDep}
              nameMenu={'Chọn'}
              eOpen={this.openComboboxEmp}
              position={'bottom'}
              value={null}
            />
          )}
          {this.state.modalPickerTimeVisibleDate && (
            <DateTimePickerModal
              locale="vi-VN"
              cancelTextIOS="Huỷ"
              confirmTextIOS="Chọn"
              headerTextIOS="Chọn thời gian"
              isVisible={this.state.modalPickerTimeVisibleDate}
              mode="date"
              onConfirm={this.handleConfirmTime}
              onCancel={this.hideDatePicker}
            />
          )}
        </View>
      </KeyboardAvoidingView>
    );
  }
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  handleConfirmTime = date => {
    const { typeTime, currentIndex, rows } = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({
      rows: res,
      typeTime: null,
      currentIndex: null,
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
    });
    // this.hideDatePicker();
  };
  handleChangeRows = (val, index, name) => {
    const { rows } = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({ rows: res });
  };
  _openComboboxEmp() { }
  openComboboxEmp = d => {
    this._openComboboxEmp = d;
  };

  openEmps(index) {
    this.setState({ currentIndexItem: index }, () => {
      this._openComboboxEmp();
    });
  }
  ChangeEmp = rs => {
    let rsdetal = {};
    const { currentIndexItem, rows } = this.state;
    let { value, text } = rs;
    if (!rs) {
      return;
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.DepartmentName = text;
        row.DepartmentId = value;
      }
      return row;
    });
    this.setState({ rows: data, currentIndexItem: null });
  };
  itemFlatList = (row, index) => {
    const stylePicker = {
      inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
      },
      inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderWidth: 0,
        borderColor: 'black',
        borderRadius: 5,
        color: 'black',
        paddingRight: 10,
        // to ensure the text is never behind the icon
      },
    };

    return (
      <View style={{ flexDirection: 'row' }} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, { width: 40 }]}>
          <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(1) }]}>
          <Text style={[AppStyles.TextTable, { textAlign: 'center' }]}>
            {' '}
            {row.ItemId}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(2) }]}>
          <Text style={[AppStyles.TextTable, { textAlign: 'center' }]}>
            {' '}
            {row.ItemName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(3) }]}>
          <Text style={[AppStyles.TextTable, { textAlign: 'center' }]}>
            {' '}
            {row.UnitName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(4) }]}
          onPress={() => {
            this.openEmps(index);
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.DepartmentId,
                { color: 'black', fontSize: 14 },
              ]}>
              {row.DepartmentId ? row.DepartmentName : ''}
            </Text>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Icon
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(5) }]}>
          <Text style={[AppStyles.TextTable, { textAlign: 'right' }]}>
            {' '}
            {row.Quantity}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(6) }]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              { color: 'black', marginVertical: 2, textAlign: 'right' },
            ]}
            underlineColorAndroid="transparent"
            keyboardType="numeric"
            value={
              !row.QuantityOk
                ? row.QuantityOk
                : FuncCommon.addPeriodTextInput(row.QuantityOk)
            }
            autoCapitalize="none"
            onChangeText={QuantityOk => {
              this.handleChangeRows(QuantityOk, index, 'QuantityOk');
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(7) }]}>
          <Text
            style={[
              AppStyles.TextTable,
              {
                textAlign: 'right',
                color:
                  +row.Quantity - +row.QuantityReceipt > 0 ? 'black' : 'red',
              },
            ]}>
            {' '}
            {+row.Quantity - +row.QuantityReceipt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(8) }]}>
          <Text style={[AppStyles.TextTable, { textAlign: 'right' }]}>
            {' '}
            {FuncCommon.addPeriod(row.UnitPrice)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(9) }]}>
          <Text style={[AppStyles.TextTable, { textAlign: 'right' }]}>
            {' '}
            {FuncCommon.addPeriod(row.Amount)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickDateOpen('DeliveryDate', index);
          }}
          style={[AppStyles.table_td_custom, { width: this.getWidth(10) }]}>
          <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
            {(row.DeliveryDate &&
              moment(row.DeliveryDate).format('DD/MM/YYYY')) ||
              ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(11) }]}>
          <TextInput
            style={[AppStyles.FormInput_custom, { color: 'black' }]}
            underlineColorAndroid="transparent"
            value={row.Specification}
            autoCapitalize="none"
            onChangeText={Specification => {
              this.handleChangeRows(Specification, index, 'Specification');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(12) }]}>
          <RNPickerSelect
            doneText="Xong"
            style={{
              ...stylePicker,
              iconContainer: {
                top: 10,
                right: 10,
              },
            }}
            onValueChange={value => {
              this.handleChangeRows(value, index, 'PPDeliveryDateId');
            }}
            items={this.state.listPlanDate}
            value={row.PPDeliveryDateId}
            placeholder={{ value: null, label: 'Chọn' }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, { width: this.getWidth(13) }]}>
          <RNPickerSelect
            doneText="Xong"
            style={{
              ...stylePicker,
              iconContainer: {
                top: 10,
                right: 10,
              },
            }}
            onValueChange={value => {
              this.handleChangeRows(value, index, 'WarehouseId');
            }}
            items={this.state.listWarehouse}
            value={row.WarehouseId}
            placeholder={{ value: null, label: 'Chọn' }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            // this.onDelete(index);
          }}
          style={[AppStyles.table_td_custom, { width: 80 }]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  rowTotal = () => {
    const { rows } = this.state;
    return (
      <View style={{ flexDirection: 'row' }}>
        {headerTable.map((item, index) => {
          if (item.name === 'buyQuantity') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  { width: item.width, backgroundColor: '#f5f2f2' },
                ]}>
                <Text style={[AppStyles.Textdefault, styles.totalText]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.Quantity || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          if (item.name === 'QuantityReceipt') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  { width: item.width, backgroundColor: '#f5f2f2' },
                ]}>
                <Text style={[AppStyles.Textdefault, styles.totalText]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) =>
                        total + (+row.Quantity - +row.QuantityReceipt || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          if (item.name === 'total') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  { width: item.width, backgroundColor: '#f5f2f2' },
                ]}>
                <Text style={[AppStyles.Textdefault, styles.totalText]}>
                  {FuncCommon.addPeriod(
                    rows.reduce((total, row) => total + (+row.Amount || 0), 0),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }

          return (
            <TouchableOpacity
              key={index}
              style={[
                AppStyles.table_td_custom,
                { width: item.width, backgroundColor: '#f5f2f2' },
              ]}
            />
          );
        })}
      </View>
    );
  };
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'POrders', ActionTime: new Date().getTime() });
  }
  ChangeVoucherType = value => {
    this.setState({
      dVoucherType: value.text,
      VoucherType: value.value,
      ListCustomers: [],
    });
    //this.GetCustomersAll();
  };
  _openComboboxVoucherType() { }
  openComboboxVoucherType = d => {
    this._openComboboxVoucherType = d;
  };
  onActionComboboxVoucherType() {
    this._openComboboxVoucherType();
  }

  ChangeCustomers = value => {
    this.setState({
      dCustomers: value.text,
      ObjectGuid: value.value,
    });
    var rs = this.state.ListCustomers.find(x => x.value === value.value);
    if (rs !== null && rs !== undefined) {
      this.setState({
        ObjectId: rs.ObjectId,
        ObjectName: rs.ObjectName,
        ObjectAddress: rs.ObjectAddress,
      });
    }
  };
  _openComboboxCustomers() { }
  openComboboxCustomers = d => {
    this._openComboboxCustomers = d;
  };
  onActionComboboxCustomers() {
    this._openComboboxCustomers();
  }
  setQuantity(value, index) {
    this.state.ListTicketRequestDetail[index].Quantity =
      value !== '' && value !== null ? parseFloat(value) : 0;
    this.state.ListTicketRequestDetail[index].Amount =
      this.state.ListTicketRequestDetail[index].Quantity *
      this.state.ListTicketRequestDetail[index].UnitPriceBefore;
    this.setState(
      (ListTicketRequestDetail = this.state.ListTicketRequestDetail),
    );
  }
  Submit = () => {
    let lstDetails = _.cloneDeep(this.state.rows);
    if (!this.state.note) {
      Toast.showWithGravity(
        'Bạn chưa điền diễn giải',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let isNotValidated = false;
    for (let i = 0; i < lstDetails.length; i++) {
      let lst = lstDetails[i];
      if (!lst.QuantityOk || +lst.QuantityOk <= 0) {
        Toast.showWithGravity(
          'Số lượng đề nghị phải lớn hơn 0',
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }

      if (+lst.QuantityOk > +lst.Quantity - +lst.QuantityReceipt) {
        Toast.showWithGravity(
          `Dòng ${i +
          1} số lượng đề nghị phải nhỏ hơn hoặc bằng số lượng còn lại`,
          Toast.SHORT,
          Toast.CENTER,
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }
    let rowsDetail = _.cloneDeep(lstDetails).map(row => {
      row.Quantity = row.QuantityOk;
      row.QuantityOK = undefined;
      row.Note = row.Specification;
      row.UnitBefore = row.UnitId;
      row.UnitPriceBefore = row.UnitPrice;
      return row;
    });
    // let emp = this.state.listEmp.find(e => e.value == this.state.empMH);
    // let objEmp = {};
    // if (emp) {
    //   (objEmp.StockerId = emp.EmployeeId),
    //     (objEmp.StockerGuid = emp.EmployeeGuid),
    //     (objEmp.StockerName = emp.FullName);
    // }
    console.log(this.props.DataView);
    let _model = {
      ...this.props.DataView,
      TicketRequestNo: this.state.modelauto.Value,
      // ...objEmp,
      RequestDate: FuncCommon.ConDate(this.state.RequestDate, 2),
      PurchaseOrderId: this.props.DataView.Ponumber,
      IsStatus: '0',
    };
    let _data = new FormData();
    _data.append('model', JSON.stringify(_model));
    _data.append('TicketRequestDetails', JSON.stringify(rowsDetail));
    _data.append('auto', JSON.stringify(this.state.modelauto));
    API_PurchaseOrders.CheckRequest(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Yêu cầu thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  OnChangeTab(data) {
    if (data == '1') {
      this.setState({
        tabCT: false,
        tabTTC: true,
      });
    } else if (data == '2') {
      this.setState({
        tabTTC: false,
        tabCT: true,
      });
    }
  }
  //Menu Item
  actionMenuCallback = d => {
    if (d === 'delete') {
      var list = this.state.ListTicketRequestDetail;
      list.splice(this.state.position, 1);
      this.setState({
        ListTicketRequestDetail: list,
      });
    } else if (d === 'edit') {
      this.setState({
        ItemGuid: this.state.selectItem.ItemGuid,
        ItemId: this.state.selectItem.ItemId,
        ItemName: this.state.selectItem.ItemName,
        POGuid: this.state.selectItem.POGuid,
        OrderNumberId: this.state.selectItem.OrderNumberId,
        UnitBefore: this.state.selectItem.UnitBefore,
        UnitName: this.state.selectItem.UnitName,
        Quantity: this.state.selectItem.Quantity,
        QuantityActual: this.state.selectItem.QuantityActual,
        UnitPriceBefore: this.state.selectItem.UnitPriceBefore,
        Amount: this.state.selectItem.Amount,
        SortOrder: this.state.selectItem.SortOrder,
        VoucherGuid: this.state.selectItem.VoucherGuid,
        WarehouseId: this.state.selectItem.WarehouseId,
        PurchaseOrderId: this.state.selectItem.PurchaseOrderId,
        editItem: this.state.position,
      });
    }
    console.log('Event: ', d);
  };
  _openMenu() { }
  openMenu = d => {
    this._openMenu = d;
  };
  onAction(item, index) {
    this.setState({
      selectItem: item,
      position: index,
    });
    var _option = {
      isChangeAction: true,
      data: this.state.dataMenu,
      NameMenu: 'Chức năng',
    };
    this._openMenu(_option);
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
}
const mapStateToProps = state => ({
  applications: state.user.data,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(CheckComponent);
