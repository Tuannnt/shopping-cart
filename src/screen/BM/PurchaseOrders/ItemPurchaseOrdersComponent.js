import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Keyboard,
  View,
  Alert,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import { Button, Icon, Divider, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import API_PurchaseOrders from '../../../network/BM/API_PurchaseOrders';
import TabBarBottom from '../../component/TabBarBottom';
import API from '../../../network/API';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';
import Toast from 'react-native-simple-toast';
import { FuncCommon } from '@utils';
const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});

class ItemPurchaseOrdersComponent extends Component {
  constructor(props) {
    super(props);
    this.DataView = null;
    this.state = {
      Data: null,
      listDetail: [],
      staticParam: {
        PurchaseOrderGuid: null,
        QueryOrderBy: 'ItemName desc',
      },
      isHidden: false,
      FullName: '',
      check: false,
      loading: true,
    };
  }

  componentDidMount(): void {
    this.getItem();
    API.getProfile().then(rs => {
      this.setState({ FullName: rs.data.FullName });
    });
  }
  getItem() {
    let id = this.props.RecordGuid || this.props.RowGuid;
    API_PurchaseOrders.PurchaseOrders_GetItems(id)
      .then(res => {
        console.log(res);
        this.DataView = JSON.parse(res.data.data);
        if (this.DataView.IsLock === true && this.DataView.POStatus === 'B') {
          this.setState({
            check: true,
          });
        }
        let check = {
          PurchaseOrderGuid: this.DataView.PurchaseOrderGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_PurchaseOrders.CheckLogin(check)
          .then(rs => {
            this.setState({ checkInLogin: rs.data.data });
          })
          .catch(error => {
            console.log(error);
          });
        this.GetDetail();
      })
      .catch(error => {
        console.log(error.data.data);
        this.setState({ loading: false });
      });
  }
  GetDetail = () => {
    this.setState({ refreshing: true });
    this.state.staticParam.PurchaseOrderGuid =
      this.props.RecordGuid || this.props.RowGuid;
    API_PurchaseOrders.GetPurchaseOrderDetails(
      this.state.staticParam.PurchaseOrderGuid,
    )
      .then(res => {
        this.state.listDetail = JSON.parse(res.data.data);
        this.setState({
          list: this.state.listDetail,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };

  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết đơn hàng'}
          callBack={() => this.onPressBack()}
          IconSubmit={this.state.check}
          CallbackIconSubmit={() =>
            Actions.bmCheck({
              RecordGuid: this.DataView.PurchaseOrderGuid,
              PONumber: this.DataView.Ponumber,
              DataView: this.DataView,
              listDetail: this.state.listDetail,
            })
          }
        />
        <View style={{ flexDirection: 'row', padding: 5 }}>
          <View style={{ flex: 10 }}>
            <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
          </View>
          <View style={{ flex: 1 }}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHidden == false
                  ? this.setState({ isHidden: true })
                  : this.setState({ isHidden: false })
              }
            />
          </View>
        </View>
        {this.DataView !== null ? (
          <View style={{ flex: 1 }}>
            <View
              style={[
                this.state.isHidden == true ? styles.hidden : '',
                {
                  padding: 15,
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                },
              ]}>
              <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <Text style={AppStyles.Labeldefault}>Số đơn hàng :</Text>
                  </View>
                  <View style={{ flex: 3, paddingLeft: 10 }}>
                    <Text style={[AppStyles.Textdefault, { textAlign: 'left' }]}>
                      {this.DataView.Ponumber}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <Text style={AppStyles.Labeldefault}>Người đặt hàng :</Text>
                  </View>
                  <View style={{ flex: 3, paddingLeft: 10 }}>
                    <Text style={[AppStyles.Textdefault, { textAlign: 'left' }]}>
                      {this.DataView.EmployeeName}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <Text style={AppStyles.Labeldefault}>
                      Loại đơn đặt hàng :
                    </Text>
                  </View>
                  <View style={{ flex: 3, paddingLeft: 10 }}>
                    <Text style={[AppStyles.Textdefault, { textAlign: 'left' }]}>
                      {this.DataView.TypeName}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <Text style={AppStyles.Labeldefault}>Ngày đặt hàng :</Text>
                  </View>
                  <View style={{ flex: 3, paddingLeft: 10 }}>
                    <Text style={[AppStyles.Textdefault, { textAlign: 'left' }]}>
                      {this.customDate(this.DataView.Podate)}
                    </Text>
                  </View>
                </View>
                {/* <View style={{flexDirection: 'row', marginTop: 5}}>
                  <View style={{flex: 2, justifyContent: 'center'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Chi phí vận chuyển :
                    </Text>
                  </View>
                  <View style={{flex: 3, paddingLeft: 10}}>
                    <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                      {this.addPeriod(this.DataView.AmountCost)}
                    </Text>
                  </View>
                </View> */}
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <Text style={AppStyles.Labeldefault}>TT đặt hàng :</Text>
                  </View>
                  <View style={{ flex: 3, paddingLeft: 10 }}>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { textAlign: 'left' },
                        this.DataView.POStatus === 'B'
                          ? { color: AppColors.AcceptColor }
                          : { color: AppColors.PendingColor },
                      ]}>
                      {this.DataView.POStatus === 'A'
                        ? 'Chưa đặt hàng'
                        : this.DataView.POStatus === 'B'
                          ? 'Đã đặt hàng'
                          : this.DataView.POStatus === 'C'
                            ? 'Quá hạn'
                            : this.DataView.POStatus === 'D'
                              ? 'Đến hạn'
                              : this.DataView.POStatus === 'E'
                                ? 'Đã nhận một phần'
                                : this.DataView.POStatus === 'F'
                                  ? 'Hoàn tất'
                                  : ''}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <Text style={AppStyles.Labeldefault}>TT xử lý :</Text>
                  </View>
                  <View style={{ flex: 3, paddingLeft: 10 }}>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        { textAlign: 'left' },
                        this.DataView.IsLock == true
                          ? { color: AppColors.AcceptColor }
                          : { color: AppColors.PendingColor },
                      ]}>
                      {this.DataView.StatusWF}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <Divider style={{ marginBottom: 10 }} />
            <View style={{ flexDirection: 'row', paddingRight: 10 }}>
              <Text
                style={[AppStyles.Titledefault, { flex: 1, paddingLeft: 10 }]}>
                Chi tiết
              </Text>
              <Icon
                style={{ color: 'black', flex: 1 }}
                name={'attach-file'}
                type="MaterialIcons"
                size={20}
                onPress={() => this.openAttachments()}
              />
            </View>
            <View style={{ flex: 6 }}>
              <ScrollView>
                {this.state.listDetail.map((item, index) => (
                  <View style={{}}>
                    <ListItem
                      title={() => {
                        return (
                          <View style={{ flexDirection: 'row' }}>
                            <Text
                              style={[
                                AppStyles.Titledefault,
                                { width: '100%', flex: 10 },
                              ]}>
                              {item.ItemName}
                            </Text>
                            <View style={{ flex: 1 }}>
                              <Icon
                                color="#888888"
                                name={'chevron-thin-right'}
                                type="entypo"
                                size={20}
                                onPress={() => this.openDetail(item)}
                              />
                            </View>
                          </View>
                        );
                      }}
                      subtitle={() => {
                        return (
                          <View>
                            <View
                              style={{
                                flexDirection: 'row',
                              }}>
                              <View style={{ width: '50%' }}>
                                <Text style={AppStyles.Textdefault}>
                                  ĐVT: {item.UnitName}
                                </Text>
                                <Text style={AppStyles.Textdefault}>
                                  Số lượng mua: {this.addPeriod(item.Quantity)}
                                </Text>
                              </View>
                              <View style={{ width: '50%' }}>
                                <Text style={AppStyles.Textdefault}>
                                  Ngày giao:{' '}
                                  {item.DeliveryDate
                                    ? FuncCommon.ConDate(item.DeliveryDate, 0)
                                    : ''}
                                </Text>
                                <Text style={AppStyles.Textdefault}>
                                  TT sau VAT: {this.addPeriod(item.VatamountOc)}
                                </Text>
                              </View>
                            </View>
                          </View>
                        );
                      }}
                      bottomDivider
                      titleStyle={AppStyles.Titledefault}
                    />
                  </View>
                ))}
              </ScrollView>
            </View>
            {/* nút xử lý */}
            {this.DataView.IsLock === true && this.DataView.POStatus !== 'B' && (
              <View style={{ paddingHorizontal: 10, marginBottom: 5 }}>
                <Button
                  buttonStyle={{
                    backgroundColor: AppColors.ColorButtonSubmit,
                  }}
                  title="Xác nhận đặt hàng"
                  onPress={() => this.onConfirm()}
                />
              </View>
            )}

            <View style={[AppStyles.StyleTabvarBottom]}>
              <TabBarBottom
                onDelete={() => this.onDelete()}
                //key để quay trở lại danh sách
                backListByKey="PurchaseOrders"
                //Truyễn dữ liệu sang màn hình comment
                keyCommentWF={{
                  ModuleId: '24',
                  RecordGuid: this.DataView.PurchaseOrderGuid,
                  Title: this.DataView.Ponumber,
                  Type: 1,
                }}
                onAttachment={() => this.openAttachments()}
                // tiêu đề hiển thị trong popup xử lý phiếu
                Title={this.DataView.Ponumber}
                //kiểm tra quyền xử lý
                Permisstion={this.DataView.Permisstion}
                //kiểm tra bước đầu quy trình
                checkInLogin={this.state.checkInLogin}
                onSubmitWF={(callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
                }
              />
            </View>
          </View>
        ) : null}
      </View>
    );
  }
  onSummit = () => {
    let obj = {
      PurchaseOrderGuid: this.DataView.PurchaseOrderGuid,
      POStatus: 'B',
    };
    API_PurchaseOrders.UpdateStatus(obj)
      .then(res => {
        if (res.data.errorCode === 200) {
          Toast.showWithGravity('Thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
  onConfirm = () => {
    Alert.alert(
      //title
      'Xác nhận thông tin',
      //body
      'Bạn có chắc chắn muốn đặt hàng ' + this.DataView.Ponumber + '?',
      [
        { text: 'Xác nhận', onPress: () => this.onSummit() },
        { text: 'Huỷ', onPress: () => console.log('No Pressed') },
      ],
      { cancelable: true },
    );
  };
  onDelete = () => {
    let id = { Id: this.props.RecordGuid };
    this.setState({ loading: true });
    API_PurchaseOrders.IsDeleted(id.Id)
      .then(response => {
        if (!response.data.Error) {
          this.setState({
            loading: false,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  };
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      PurchaseOrderGuid: this.props.RowGuid || this.props.RecordGuid,
      WorkFlowGuid: this.DataView.WorkFlowGuid,
      Comment: CommentWF,
    };
    if (callback == 'D') {
      API_PurchaseOrders.Approve(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      API_PurchaseOrders.NotApprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'POrders', ActionTime: new Date().getTime() });
  }

  openDetail = _data => {
    Actions.bmitemPODetails({ Data: _data });
  };
  openAttachments() {
    var obj = {
      ModuleId: '3',
      RecordGuid: this.DataView.PurchaseOrderGuid,
      notEdit: this.state.checkInLogin !== '1' ? true : false,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ItemPurchaseOrdersComponent);
