import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
} from 'react-native';
import { Header, Icon, Divider, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import API_PurchaseOrders from '../../../network/BM/API_PurchaseOrders';
import ErrorHandler from '../../../error/handler';
import Timeline from 'react-native-timeline-flatlist';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import { FuncCommon } from '@utils';
const styles = StyleSheet.create({
  hidden: { display: 'none' },
});

class ItemPurchaseOrderDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      Data: [],
      isHidden: false,
      isHiddenDeliveryDates: false,
      dataTimeline: [],
    };
  }

  componentDidMount(): void {
    this.setState({ Data: this.props.Data });
  }

  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết vật tư'}
          callBack={() => this.onPressBack()}
        />
        <View style={{ flexDirection: 'row', padding: 10 }}>
          <View style={{ flex: 10 }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
              Thông tin chung
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHidden == false
                  ? this.setState({ isHidden: true })
                  : this.setState({ isHidden: false })
              }
            />
          </View>
        </View>
        <View
          style={[
            this.state.isHidden == true ? styles.hidden : '',
            { padding: 20, backgroundColor: '#fff' },
          ]}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Mã SP :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.state.Data.ItemId}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Tên SP :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.state.Data.ItemName}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>ĐVT :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.state.Data.UnitName}
            </Text>
          </View>
          {/* <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
              Tiêu chuẩn kỹ thuật :
            </Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.state.Data.Specification}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Hãng SX :</Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.state.Data.MarkerName}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Vật liệu :</Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.state.Data.Material}
            </Text>
          </View> */}
          {/* <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
              Thông số kỹ thuật :
            </Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.state.Data.Specification}
            </Text>
          </View> */}
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Số lượng mua:
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.state.Data.Quantity)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Giá mua:</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.state.Data.UnitPrice)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Thành tiền:</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.state.Data.AmountOc)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>VAT:</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.state.Data.Vatrate)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Tiền VAT:</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(
                ((this.state.Data.Amount + this.state.Data.ImportTaxAmountOc) *
                  this.state.Data.Vatrate) /
                100,
              )}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Thành tiền sau VAT:
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.state.Data.VatamountOc)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Ngày giao:</Text>
            <Text style={[AppStyles.Textdefault, ,]}>
              {this.state.Data.DeliveryDate
                ? FuncCommon.ConDate(this.state.Data.DeliveryDate, 0)
                : ''}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Ghi chú :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.state.Data.Note}
            </Text>
          </View>
        </View>
        {/* <Divider style={{marginBottom: 10}} /> */}
        {/* <View style={{flexDirection: 'row', padding: 10}}>
          <View style={{flex: 10}}>
            <Text style={AppStyles.Titledefault}>Thông tin giao hàng</Text>
          </View>
          <View style={{flex: 1}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHiddenDeliveryDates == false
                  ? this.setState({isHiddenDeliveryDates: true})
                  : this.setState({isHiddenDeliveryDates: false})
              }
            />
          </View>
        </View>
        <View
          style={[
            this.state.isHiddenDeliveryDates == true ? styles.hidden : '',
            {flex: 1, padding: 15},
          ]}>
          {this.state.dataTimeline.length > 0 ? (
            <Timeline
              data={this.state.dataTimeline}
              circleSize={20}
              innerCircle={'dot'}
              circleColor="rgb(45,156,219)"
              lineColor="rgb(45,156,219)"
              timeContainerStyle={{minWidth: 52}}
              timeStyle={{
                textAlign: 'center',
                backgroundColor: '#F5DA81',
                color: 'black',
                padding: 5,
                borderRadius: 13,
              }}
              descriptionStyle={{color: 'black'}}
              style={[
                this.state.isHiddenDeliveryDates == true ? styles.hidden : '',
                {},
              ]}
            />
          ) : (
            <View style={AppStyles.centerAligned}>
              <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
            </View>
          )}
        </View> */}
      </View>
    );
  }
  onPressBack() {
    Actions.pop();
  }
  //Lấy thông tin giao hàng
  GetPODeliveryDates = () => {
    API_PurchaseOrders.GetPODeliveryDates(this.state.id)
      .then(res => {
        var _PODeliveryDates = JSON.parse(res.data.data);
        var _rePODeliveryDates = [];
        for (let i = 0; i < _PODeliveryDates.length; i++) {
          _rePODeliveryDates.push({
            time: this.customDate(_PODeliveryDates[i].PlanDate),
            title: _PODeliveryDates[i].ItemId,
            description:
              'SL yêu cầu: ' +
              this.addPeriod(_PODeliveryDates[i].Quantity) +
              '\n' +
              'SL đã giao : ' +
              this.addPeriod(_PODeliveryDates[i].ActualQuantity) +
              '\n' +
              'Chú thích : ' +
              (_PODeliveryDates[i].Description != null &&
                _PODeliveryDates[i].Description != ''
                ? _PODeliveryDates[i].Description
                : ''),
          });
        }
        this.setState({ dataTimeline: _rePODeliveryDates });
      })
      .catch(error => { });
  };

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ItemPurchaseOrderDetailsComponent);
