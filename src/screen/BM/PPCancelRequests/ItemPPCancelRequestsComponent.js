import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Alert,
  Keyboard,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import API_ProposePurchases from '../../../network/BM/API_ProposePurchases';
import API_PPCancelRequests from '../../../network/BM/API_PPCancelRequests';
import ErrorHandler from '../../../error/handler';
import Dialog from 'react-native-dialog';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import TabBarBottom from '../../component/TabBarBottom';
import Modal, {
  ModalTitle,
  ModalContent,
  ModalFooter,
  ModalButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-modals';
import LoadingComponent from '../../component/LoadingComponent';
import Toast from 'react-native-simple-toast';

const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});

class ItemPPCancelRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
      listDetail: [],
      staticParam: {
        ProposedPurchaseGuid: null,
        QueryOrderBy: 'ItemName desc',
      },
      isHidden: false,
      checkInLogin: '',
      visible: false,
      Comment: '',
      loading: true,
    };
    this.listtabbarBotom = [
      {
        Title: 'Xử lý yêu cầu',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
  }

  componentDidMount(): void {
    this.getItem();
  }

  getItem() {
    let id = this.props.RecordGuid;
    API_ProposePurchases.ProposedPurchases_GetItem(id)
      .then(res => {
        this.setState({Data: JSON.parse(res.data.data)});
        let check = {
          ProposedPurchaseGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_ProposePurchases.CheckLogin(check)
          .then(res => {
            this.setState({checkInLogin: res.data.data});
          })
          .catch(error => {
            console.log(error.data.data);
          });
        this.GetDetail();
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  }

  GetDetail = () => {
    this.setState({refreshing: true});
    this.state.staticParam.ProposedPurchaseGuid = this.props.RecordGuid;
    API_ProposePurchases.ProposedPurchaseDetails_ListDetail(
      this.state.staticParam,
    )
      .then(res => {
        this.state.listDetail = JSON.parse(res.data.data);
        this.setState({
          list: this.state.listDetail,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };

  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };

  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết đề nghị'}
          callBack={() => this.onPressBack()}
        />
        <View style={{flexDirection: 'row', padding: 10}}>
          <View style={{flex: 10}}>
            <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
          </View>
          <View style={{flex: 1}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHidden == false
                  ? this.setState({isHidden: true})
                  : this.setState({isHidden: false})
              }
            />
          </View>
        </View>
        <View
          style={[
            this.state.isHidden == true ? styles.hidden : '',
            {padding: 20, backgroundColor: '#fff', flexDirection: 'row'},
          ]}>
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Số đề nghị :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Người đề nghị :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Bộ phận :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Loại đề nghị :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Ngày đề nghị :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              TT xử lý :
            </Text>
          </View>
          <View style={{flex: 2, alignItems: 'flex-end'}}>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.ProposedPurchaseId}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.EmployeeName}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.DepartmentName}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.TypeOfRequest == 'SX'
                ? 'Sản xuất'
                : this.state.Data.TypeOfRequest == 'TM'
                ? 'Thương mại'
                : this.state.Data.TypeOfRequest == 'GCN'
                ? 'Gia công ngoài'
                : this.state.Data.TypeOfRequest == 'GT'
                ? 'Hỗ trọ sản xuất'
                : ''}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.customDate(this.state.Data.CreatedDate)}
            </Text>
            <Text
              style={[
                [AppStyles.Textdefault],
                this.state.Data.Status == 'Y'
                  ? {color: AppColors.AcceptColor}
                  : {color: AppColors.PendingColor},
              ]}>
              {this.state.Data.StatusWF}
            </Text>
          </View>
        </View>
        <Divider style={{marginBottom: 10}} />
        <Text style={[AppStyles.Titledefault, {paddingLeft: 10}]}>
          Chi tiết
        </Text>
        <View style={{flex: 6}}>
          <ScrollView>
            {this.state.listDetail.map((item, index) => (
              <View style={{}}>
                <ListItem
                  title={item.ItemName}
                  subtitle={() => {
                    return (
                      <View>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{width: '65%'}}>
                            <Text style={AppStyles.Textdefault}>
                              Ngày cần : {this.customDate(item.DateNeed)}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              ĐVT : {item.UnitName}
                            </Text>
                          </View>
                          <View style={{width: '35%'}}>
                            <Text style={AppStyles.Textdefault}>
                              Số lượng: {this.addPeriod(item.QuantityUsed)}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              Đơn giá: {this.addPeriod(item.UnitPrice)}
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  }}
                  bottomDivider
                  titleStyle={AppStyles.Titledefault}
                />
              </View>
            ))}
          </ScrollView>
        </View>
        {/*nút xử lý*/}
        <View style={AppStyles.StyleTabvarBottom}>
          {this.state.checkInLogin !== '' ? (
            <TabBarBottom
              Permisstion={this.state.Data.Permisstion}
              isComplete={this.state.Data.Status === 'Y' ? true : false}
              onAttachment={() => this.openAttachments()}
              checkInLogin={this.state.checkInLogin}
              onSubmitWF={(callback, CommentWF) =>
                this.submitWorkFlow(callback, CommentWF)
              }
              keyCommentWF={{
                ModuleId: 30,
                RecordGuid: this.props.RecordGuid,
              }}
            />
          ) : null}
        </View>
        {/* Popup */}
        <View>
          {/* <Dialog.Container visible={this.state.visible}>
                        <Dialog.Title style={{ textAlign: 'center' }}>{this.state.Data.ProposedPurchaseId}</Dialog.Title>
                        <Dialog.Description>
                            Đề nghị sửa, hủy {this.state.Data.ProposedPurchaseId}
                        </Dialog.Description>
                        <Dialog.Input style={{ borderBottomWidth: 1, borderBottomColor: '#3366CC' }}
                            label={this.state.desciption}
                            onChangeText={text => this.setComment(text)}
                            value={this.state.Comment}
                        />
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Dialog.Button style={{ fontSize: 12, backgroundColor: '#055a70', color: '#fff', borderRadius: 5, marginLeft: 5 }} label="Xác nhận" onPress={() => this.onYes()} />
                            <Dialog.Button style={{ fontSize: 12, backgroundColor: '#055a70', color: '#fff', borderRadius: 5, marginLeft: 5 }} label="Từ chối" onPress={() => this.onNo()} />
                            <Dialog.Button style={{ fontSize: 12, backgroundColor: '#dc3545', color: '#fff', borderRadius: 5, marginLeft: 5 }} label="Đóng" onPress={() => this.onClose()} />
                        </View>
                    </Dialog.Container> */}

          <Modal
            width={0.9}
            visible={this.state.visible}
            rounded
            actionsBordered
            onTouchOutside={() => {
              this.setState({visible: false});
            }}
            modalTitle={
              <ModalTitle
                title={'Đề nghị sửa, hủy ' + this.state.Data.ProposedPurchaseId}
                align="left"
              />
            }
            footer={
              <ModalFooter style={{flexDirection: 'row', height: 60}}>
                <ModalButton
                  text="Xác nhận"
                  style={{flex: 1}}
                  bordered
                  onPress={() => this.onYes()}
                  key="button-1"
                />
                <ModalButton
                  text="Từ chối"
                  style={{flex: 1}}
                  bordered
                  onPress={() => this.onNo()}
                  key="button-2"
                />
                <ModalButton
                  text="Đóng"
                  style={{flex: 1}}
                  bordered
                  onPress={() => {
                    this.setState({visible: false});
                  }}
                  key="button-3"
                />
              </ModalFooter>
            }>
            <ModalContent style={{backgroundColor: '#fff', height: 80}}>
              <View style={{paddingLeft: 10, paddingRight: 10}}>
                <TextInput
                  style={{borderColor: '#BDBDBD', marginTop: 10}}
                  underlineColorAndroid="transparent"
                  placeholderTextColor="#C0C0C0"
                  numberOfLines={1}
                  multiline={true}
                  placeholder="Nhập ý kiến"
                  onChangeText={text => this.setComment(text)}
                />
              </View>
            </ModalContent>
          </Modal>
        </View>
      </View>
    );
  }

  submitWorkFlow(callback, CommentWF) {
    // this.setState({ loading: true });
    // console.log('callback==' + callback + CommentWF);
    // if (callback == 'D') {
    //   let obj = {
    //     ApplyOutsideGuid: this.props.viewId,
    //     WorkFlowGuid: this.state.Data.WorkFlowGuid,
    //     LoginName: this.props.Loginname,
    //     Comment: CommentWF,
    //   };
    //   API_ApplyOutsides.approveApplyOutsides(obj)
    //     .then(res => {
    //       if (res.data.errorCode === 200) {
    //         Toast.showWithGravity(
    //           'Xử lý thành công',
    //           ToastAndroid.SHORT,
    //           ToastAndroid.CENTER,
    //         );
    //         this.onPressBack();
    //       } else {
    //         Toast.showWithGravity(
    //           'Lỗi khi xử lý',
    //           ToastAndroid.SHORT,
    //           ToastAndroid.CENTER,
    //         );
    //         this.getItem();
    //       }
    //     })
    //     .catch(error => {
    //       console.log(error.data.data);
    //     });
    // } else {
    //   let obj = {
    //     ApplyOutsideGuid: this.props.viewId,
    //     WorkFlowGuid: this.state.Data.WorkFlowGuid,
    //     LoginName: this.props.Loginname,
    //     Comment: CommentWF,
    //   };
    //   API_ApplyOutsides.noapproveApplyOutsides(obj)
    //     .then(res => {
    //       if (res.data.errorCode === 200) {
    //         Toast.showWithGravity(
    //           'Trả lại thành công',
    //           ToastAndroid.SHORT,
    //           ToastAndroid.CENTER,
    //         );
    //         this.onPressBack();
    //       } else {
    //         Toast.showWithGravity(
    //           'Lỗi khi trả lại',
    //           ToastAndroid.SHORT,
    //           ToastAndroid.CENTER,
    //         );
    //         this.getItem();
    //       }
    //     })
    //     .catch(error => {
    //       console.log(error.data.data);
    //     });
    // }
    // this.setState({
    //   loading: false,
    // });
  }
  openAttachments() {
    var obj = {
      ModuleId: '16',
      RecordGuid: this.props.viewId,
    };

    Actions.attachmentComponent(obj);
  }
  handleCancel = para => {
    var obj = this.state;
    if (para == 'D') {
      if (obj.text == '') {
        Alert.alert('Thông báo', 'Yêu cầu nhập ý kiến', [{text: 'Đóng'}], {
          cancelable: true,
        });
      } else {
        this.submit(para, obj);
      }
    } else {
      this.submit(para, obj);
    }
  };

  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'PPCancel', ActionTime: new Date().getTime()});
  }
  //Xác nhận thông tin
  onConfim() {
    this.setState({
      visible: true,
    });
  }
  onClose() {
    this.setState({
      visible: false,
    });
  }
  setComment = text => {
    this.setState({Comment: text});
  };
  onYes = () => {
    let obj = {
      ProposedPurchaseGuid: this.state.Data.ProposedPurchaseGuid,
      EmployeeGuid: this.state.Data.EmployeeGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      Comment: this.state.Comment,
    };
    API_PPCancelRequests.Confirm(obj)
      .then(res => {
        this.setState({
          visible: false,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      })
      .catch(error => {
        this.setState({
          visible: true,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      });
  };
  onNo = () => {
    API_PPCancelRequests.NotConfirm({
      ProposedPurchaseGuid: this.state.Data.ProposedPurchaseGuid,
    })
      .then(res => {
        this.setState({
          visible: false,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      })
      .catch(error => {
        this.setState({
          visible: true,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      });
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemPPCancelRequestsComponent);
