import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, ListItem, SearchBar, Badge} from 'react-native-elements';
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import TabNavigator from 'react-native-tab-navigator';
import API_PPCancelRequests from '../../../network/BM/API_PPCancelRequests';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import Combobox from '../../component/Combobox';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '../../../utils';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';

const styles = StyleSheet.create({
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabCenter: {
    flex: 1,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    paddingTop: 10,
    paddingBottom: 10,
  },
});
const SCREEN_WIDTH = Dimensions.get('window').width;
class ListPPCancelRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      model: {},
      list: [],
      isFetching: false,
      openSearch: false,
      refreshing: false,
      dTypeOfRequest: [
        {
          value: '',
          text: 'Tất cả',
        },
        {
          value: 'TM',
          text: 'Thương mại',
        },
        {
          value: 'SX',
          text: 'Sản xuất',
        },
        {
          value: 'GCN',
          text: 'Gia công ngoài',
        },
        {
          value: 'DV',
          text: 'Dịch vụ - GCN',
        },
      ],
    };
    (this.staticParam = {
      CurrentPage: 1,
      Length: 25,
      Search: '',
      QueryOrderBy: 'ProposedPurchaseId desc',
      StartDate: new Date(),
      EndDate: new Date(),
      Type: '',
      IsRequestStatus: 'W',
    }),
      (this.listtabbarBotom = [
        {
          Title: 'Chờ xác nhận',
          Icon: 'profile',
          Type: 'antdesign',
          Value: 'profile',
          Checkbox: true,
        },
        {
          Title: 'Đã xác nhận',
          Icon: 'checkcircleo',
          Type: 'antdesign',
          Value: 'checkcircleo',
          Checkbox: false,
        },
        {
          Title: 'Không xác nhận',
          Icon: 'exception1',
          Type: 'antdesign',
          Value: 'exception1',
          Checkbox: false,
        },
      ]);
  }

  GetAll = () => {
    this.setState({refreshing: true});
    API_PPCancelRequests.ListAll(this.staticParam)
      .then(res => {
        this.state.list =
          res.data.data != 'null'
            ? this.state.list.concat(JSON.parse(res.data.data))
            : [];
        this.setState({list: this.state.list});
        this.setState({refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };

  componentDidMount(): void {
    this.GetAll();
    API_PPCancelRequests.GetCancelRequestsCount({IsRequestStatus: 'W'})
      .then(res => {
        this.listtabbarBotom[0].Badge = res.data.data;
      })
      .catch(error => {
        console.log(error);
      });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'PPCancel') {
      this.updateSearch('');
      API_PPCancelRequests.GetCancelRequestsCount({IsRequestStatus: 'W'})
        .then(res => {
          this.listtabbarBotom[0].Badge = res.data.data;
        })
        .catch(error => {
          console.log(error);
        });
    }
  }
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  updateSearch = text => {
    this.staticParam.Search = text;
    this.setState({
      list: [],
    });
    this.GetAll();
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Yêu cầu sửa đề nghị mua'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'BM'}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  padding: 5,
                }}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian bắt đầu
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventStartDate: true})}>
                  <Text>
                    {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian kết thúc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventEndDate: true})}>
                  <Text>{FuncCommon.ConDate(this.staticParam.EndDate, 0)}</Text>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', padding: 5}}>
                <Text
                  style={[
                    AppStyles.Labeldefault,
                    {textAlign: 'center'},
                    styles.timeHeader,
                  ]}>
                  Lọc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionSearchDate()}>
                  <Icon
                    name={'down'}
                    type={'antdesign'}
                    size={18}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              style={[AppStyles.FormInput, {marginHorizontal: 10}]}
              onPress={() => this.onActionComboboxType()}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.staticParam.TypeName
                    ? {color: 'black'}
                    : {color: AppColors.gray},
                ]}>
                {this.staticParam.TypeName
                  ? this.staticParam.TypeName
                  : 'Chọn loại đề nghị...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  color={AppColors.gray}
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                />
              </View>
            </TouchableOpacity>
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.updateSearch(text)}
              value={this.staticParam.Search}
            />
          </View>
        ) : null}
        <View style={{flex: 9}}>
          <FlatList
            style={{height: '100%'}}
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListFooterComponent={this.renderFooter}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={item => item.ProposedPurchaseGuid}
            data={this.state.list}
            renderItem={({item, index}) => (
              <View style={{fontSize: 13, fontFamily: Fonts.base.family}}>
                <ListItem
                  title={item.ProposedPurchaseId}
                  subtitle={() => {
                    return (
                      <View style={{flexDirection: 'row'}}>
                        <View sstyle={{flex: 3}}>
                          <Text style={AppStyles.Textdefault}>
                            Nhân viên: {item.EmployeeName}
                          </Text>
                          <Text style={AppStyles.Textdefault}>
                            Loại đề nghị:{' '}
                            {item.TypeOfRequest == 'TM'
                              ? 'Thương mại'
                              : item.TypeOfRequest == 'SX'
                              ? 'Sản xuất'
                              : item.TypeOfRequest == 'GCN'
                              ? 'Gia công ngoài'
                              : item.TypeOfRequest == 'GT'
                              ? 'Hỗ trợ sản xuất'
                              : ''}
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 2,
                            alignItems: 'flex-end',
                            justifyContent: 'flex-start',
                          }}>
                          <Text style={AppStyles.Textdefault}>
                            {item.DepartmentName}
                          </Text>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              item.IsRequestStatus == 'Y'
                                ? {color: AppColors.AcceptColor}
                                : item.IsRequestStatus == 'W'
                                ? {color: AppColors.PendingColor}
                                : {color: AppColors.UntreatedColor},
                            ]}>
                            {item.IsRequestStatus == 'W'
                              ? 'Chờ duyệt'
                              : item.IsRequestStatus == 'Y'
                              ? 'Đã duyệt'
                              : item.IsRequestStatus == 'N'
                              ? 'Không duyệt'
                              : ''}
                          </Text>
                        </View>
                      </View>
                    );
                  }}
                  bottomDivider
                  titleStyle={AppStyles.Titledefault}
                  onPress={() => this.openView(item.ProposedPurchaseGuid)}
                />
              </View>
            )}
            onEndReachedThreshold={0.5}
            onEndReached={({distanceFromEnd}) => {
              if (this.staticParam.CurrentPage !== 1) {
                this.nextPage();
              }
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback => this.onClick(callback)}
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {this.state.dTypeOfRequest.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeType}
              data={this.state.dTypeOfRequest}
              nameMenu={'Chọn loại đề nghị'}
              eOpen={this.openComboboxType}
              position={'bottom'}
              value={this.staticParam.Type}
            />
          ) : null}
        </View>
      </View>
    );
  }
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    this.staticParam.Type = rs.value;
    this.staticParam.TypeName = rs.text;
    this.updateSearch('');
  };

  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetAll();
  }
  //Load lại
  onRefresh = () => {
    this.setState({refreshing: true});
    this.GetAll();
  };
  onClick(data) {
    if (data == 'checkcircleo') {
      this.state.list = [];
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.Search = '';
      this.staticParam.IsRequestStatus = 'Y';
      this.GetAll();
    } else if (data == 'profile') {
      this.state.list = [];
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 15;
      this.staticParam.Search = '';
      this.staticParam.IsRequestStatus = 'W';
      this.GetAll();
    } else if (data == 'exception1') {
      this.state.list = [];
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.Search = '';
      this.staticParam.IsRequestStatus = 'N';
      this.GetAll();
    } else if (data === 'home') {
      Actions.app();
    }
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  openView(id) {
    Actions.bmitemPPCancelRequests({RecordGuid: id});
  }
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListPPCancelRequestsComponent);
