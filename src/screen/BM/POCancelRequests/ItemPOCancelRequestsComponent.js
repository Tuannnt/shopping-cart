import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  Keyboard,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  Alert,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_PurchaseOrders from '../../../network/BM/API_PurchaseOrders';
import API_POCancelRequests from '../../../network/BM/API_POCancelRequests';
import ErrorHandler from '../../../error/handler';
import Dialog from 'react-native-dialog';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import LoadingComponent from '../../component/LoadingComponent';

const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});

class ItemPOCancelRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
      listDetail: [],
      staticParam: {
        PurchaseOrderGuid: null,
        QueryOrderBy: 'ItemName desc',
      },
      isHidden: false,
      checkInLogin: '',
      visible: false,
      Comment: '',
      loading: true,
    };
    this.listtabbarBotom = [
      {
        Title: 'Xử lý yêu cầu',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
  }

  componentDidMount(): void {
    this.getItem();
  }

  getItem() {
    let id = this.props.RecordGuid;
    API_PurchaseOrders.PurchaseOrders_GetItems(id)
      .then(res => {
        this.setState({Data: JSON.parse(res.data.data)});
        let check = {
          PurchaseOrderGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_PurchaseOrders.CheckLogin(check)
          .then(res => {
            this.setState({checkInLogin: res.data.data});
          })
          .catch(error => {
            console.log(error.data.data);
          });
        this.GetDetail();
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }

  GetDetail = () => {
    this.setState({refreshing: true});
    this.state.staticParam.PurchaseOrderGuid = this.props.RecordGuid;
    API_PurchaseOrders.PurchaseOrdersDetails_ListDetail(this.state.staticParam)
      .then(res => {
        this.state.listDetail = JSON.parse(res.data.data);
        this.setState({
          list: this.state.listDetail,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };

  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết đề nghị'}
          callBack={() => this.onPressBack()}
        />
        <View style={{flexDirection: 'row', padding: 10}}>
          <View style={{flex: 10}}>
            <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
          </View>
          <View style={{flex: 1}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHidden == false
                  ? this.setState({isHidden: true})
                  : this.setState({isHidden: false})
              }
            />
          </View>
        </View>
        <View
          style={[
            this.state.isHidden == true ? styles.hidden : '',
            {padding: 20, backgroundColor: '#fff', flexDirection: 'row'},
          ]}>
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Số PO :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Người đặt hàng :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Loại đơn hàng :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Ngày mua :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Tổng tiền :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              TT xử lý :
            </Text>
          </View>
          <View style={{flex: 2, alignItems: 'flex-end'}}>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.PONumber}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.EmployeeName}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.TypeName}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.customDate(this.state.Data.PODate)}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.addPeriod(this.state.Data.Amount)}
            </Text>
            <Text
              style={[
                [AppStyles.Textdefault],
                this.state.Data.IsLock == true
                  ? {color: AppColors.AcceptColor}
                  : {color: AppColors.PendingColor},
              ]}>
              {this.state.Data.StatusWF}
            </Text>
          </View>
        </View>
        <Divider style={{marginBottomF: 10}} />
        <Text style={[AppStyles.Titledefault, {paddingLeft: 10}]}>
          Chi tiết
        </Text>
        <View style={{flex: 6}}>
          {this.state.listDetail.length > 0 ? (
            <ScrollView>
              {this.state.listDetail.map((item, index) => (
                <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
                  <ListItem
                    title={item.ItemName}
                    subtitle={() => {
                      return (
                        <View>
                          <View style={{flexDirection: 'row'}}>
                            <View style={{width: '60%'}}>
                              <Text style={AppStyles.Textdefault}>
                                ĐVT : {item.UnitName}
                              </Text>
                              <Text style={AppStyles.Textdefault}>
                                Số lượng: {this.addPeriod(item.Quantity)}
                              </Text>
                            </View>
                            <View style={{width: '40%'}}>
                              <Text style={AppStyles.Textdefault}>
                                Đơn giá: {this.addPeriod(item.UnitPrice)}
                              </Text>
                              <Text style={AppStyles.Textdefault}>
                                Tổng tiền: {this.addPeriod(item.Amount)}
                              </Text>
                            </View>
                          </View>
                        </View>
                      );
                    }}
                    bottomDivider
                    titleStyle={AppStyles.Titledefault}
                  />
                </View>
              ))}
            </ScrollView>
          ) : (
            <View style={AppStyles.centerAligned}>
              <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
            </View>
          )}
        </View>
        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onConfim()}
          />
        </View>
        {/* Popup */}
        <View>
          <Dialog.Container visible={this.state.visible}>
            <Dialog.Title style={{textAlign: 'center'}}>
              {this.state.Title}
            </Dialog.Title>
            <Dialog.Description>
              Đề nghị sửa, hủy {this.state.Data.PONumber}
            </Dialog.Description>
            <Dialog.Input
              style={{borderBottomWidth: 1, borderBottomColor: '#3366CC'}}
              label={this.state.desciption}
              onChangeText={text => this.setComment(text)}
              value={this.state.Comment}
            />
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
              <Dialog.Button
                style={{
                  fontSize: 12,
                  backgroundColor: '#055a70',
                  color: '#fff',
                  borderRadius: 5,
                  marginLeft: 5,
                }}
                label="Xác nhận"
                onPress={() => this.onYes()}
              />
              <Dialog.Button
                style={{
                  fontSize: 12,
                  backgroundColor: '#055a70',
                  color: '#fff',
                  borderRadius: 5,
                  marginLeft: 5,
                }}
                label="Từ chối"
                onPress={() => this.onNo()}
              />
              <Dialog.Button
                style={{
                  fontSize: 12,
                  backgroundColor: '#dc3545',
                  color: '#fff',
                  borderRadius: 5,
                  marginLeft: 5,
                }}
                label="Đóng"
                onPress={() => this.onClose()}
              />
            </View>
          </Dialog.Container>
        </View>
      </View>
    );
  }
  handleCancel = para => {
    var obj = this.state;
    if (para == 'D') {
      if (obj.text == '') {
        Alert.alert('Thông báo', 'Yêu cầu nhập ý kiến', [{text: 'Đóng'}], {
          cancelable: true,
        });
      } else {
        this.submit(para, obj);
      }
    } else {
      this.submit(para, obj);
    }
  };

  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'POCancel', ActionTime: new Date().getTime()});
  }
  //Xác nhận thông tin
  onConfim() {
    this.setState({
      visible: true,
    });
  }
  onClose() {
    this.setState({
      visible: false,
    });
  }
  setComment = text => {
    this.setState({Comment: text});
  };
  onYes = () => {
    let obj = {
      PurchaseOrderGuid: this.state.Data.PurchaseOrderGuid,
      EmployeeGuid: this.state.Data.EmployeeGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      Comment: this.state.Comment,
    };
    API_POCancelRequests.Confirm(obj)
      .then(res => {
        this.setState({
          visible: false,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      })
      .catch(error => {
        this.setState({
          visible: true,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      });
  };
  onNo = () => {
    API_POCancelRequests.NotConfirm({
      PurchaseOrderGuid: this.state.Data.PurchaseOrderGuid,
    })
      .then(res => {
        this.setState({
          visible: false,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      })
      .catch(error => {
        this.setState({
          visible: true,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      });
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemPOCancelRequestsComponent);
