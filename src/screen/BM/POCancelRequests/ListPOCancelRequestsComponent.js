import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, View } from "react-native";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import API_POCancelRequests from "../../../network/BM/API_POCancelRequests";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';

class ListPOCancelRequestsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 25,
                Search: "",
                QueryOrderBy: "PODate desc",
                StartDate: "2020-02-01T02:13:13.779Z",
                EndDate: "2020-02-29T02:13:13.779Z",
                Type: null,
                IsRequestStatus: 'W'
            },
            openSearch: false,
            refreshing: false
        };
        this.listtabbarBotom = [
            {
                Title: "Trang chủ",
                Icon: "home",
                Type: "antdesign",
                Value: "home",
                Checkbox: false
            },
            {
                Title: "Chờ xác nhận",
                Icon: "profile",
                Type: "antdesign",
                Value: "profile",
                Checkbox: true
            },
            {
                Title: "Đã xác nhận",
                Icon: "checkcircleo",
                Type: "antdesign",
                Value: "checkcircleo",
                Checkbox: false
            },
            {
                Title: "Không xác nhận",
                Icon: "exception1",
                Type: "antdesign",
                Value: "exception1",
                Checkbox: false
            }
        ]
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_POCancelRequests.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        this.GetAll();
        API_POCancelRequests.GetCancelRequestsCount({ IsRequestStatus: 'W' })
            .then(res => {
                this.listtabbarBotom[1].Badge = res.data.data;
            })
            .catch(error => {
                console.log(error);
            });
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.moduleId == "POCancel") {
            this.GetAll();
            API_POCancelRequests.GetCancelRequestsCount({ IsRequestStatus: 'W' })
                .then(res => {
                    this.listtabbarBotom[1].Badge = res.data.data;
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }
    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                Type: this.state.staticParam.Type,
                IsRequestStatus: this.state.staticParam.IsRequestStatus
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Yêu cầu sửa đơn hàng'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'BM'}
                />
                {this.state.openSearch == true ?
                    <FormSearch
                        CallbackSearch={(callback) => this.updateSearch(callback)}
                    />
                    : null}
                <View style={{ flex: 9 }}>
                    <FlatList
                        style={{ height: '100%' }}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.PurchaseOrderGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family }}>
                                <ListItem
                                    title={item.PONumber}
                                    subtitle={() => {
                                        return (
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ width: '70%' }}>
                                                    <Text style={AppStyles.Textdefault}>Nhân
                                                        viên: {item.EmployeeName}</Text>
                                                    <Text style={AppStyles.Textdefault}>Loại đơn
                                                        hàng: {item.TypeName}</Text>
                                                </View>
                                                <View style={{ width: '30%' }}>
                                                    <Text style={[AppStyles.Textdefault, item.IsRequestStatus == 'Y' ? { color: AppColors.AcceptColor } : item.IsRequestStatus == 'W' ? { color: AppColors.PendingColor } : { color: AppColors.UntreatedColor }]}>{item.IsRequestStatus == 'W' ? 'Chờ duyệt' : item.IsRequestStatus == 'Y' ? 'Đã duyệt' : item.IsRequestStatus == 'N' ? 'Không duyệt' : ''}</Text>
                                                    <Text
                                                        style={AppStyles.Textdefault}>{this.customDate(item.PODate)}</Text>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    titleStyle={AppStyles.Titledefault}
                                    onPress={() => this.openView(item.PurchaseOrderGuid)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                    <View style={AppStyles.StyleTabvarBottom}>
                        <TabBarBottomCustom
                            ListData={this.listtabbarBotom}
                            onCallbackValueBottom={(callback) => this.onClick(callback)}
                        />
                    </View>
                </View>
            </View>
        );
    }

    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                Type: this.state.staticParam.Type,
                IsRequestStatus: this.state.staticParam.IsRequestStatus
            }
        });
        this.GetAll();
    }
    //Load lại
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.GetAll();
    };
    onClick(data) {
        if (data == 'checkcircleo') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 25;
            this.state.staticParam.Search = '';
            this.state.staticParam.IsRequestStatus = 'Y';
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    Type: this.state.staticParam.Type,
                    IsRequestStatus: this.state.staticParam.IsRequestStatus
                }
            });
            this.GetAll();
        }
        else if (data == 'profile') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 25;
            this.state.staticParam.Search = '';
            this.state.staticParam.IsRequestStatus = 'W';
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    Type: this.state.staticParam.Type,
                    IsRequestStatus: this.state.staticParam.IsRequestStatus
                }
            });
            this.GetAll();
        }
        else if (data == 'exception1') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 25;
            this.state.staticParam.Search = '';
            this.state.staticParam.IsRequestStatus = 'N';
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    Type: this.state.staticParam.Type,
                    IsRequestStatus: this.state.staticParam.IsRequestStatus
                }
            });
            this.GetAll();
        }
        else if (data === 'home') {
            Actions.app();
        }
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    openView(id) {
        Actions.bmitemPOCancelRequests({ RecordGuid: id });
    }
}

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListPOCancelRequestsComponent);
