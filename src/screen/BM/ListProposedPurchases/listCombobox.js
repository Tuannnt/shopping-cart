import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Mã hàng', width: 200},
    {title: 'Tên hàng', width: 150},
    {title: 'Tên thành phẩm', width: 150, hideInCommercialType: true},
    {title: 'Đơn hàng SO', width: 200},
    {title: 'Tiêu chuẩn kĩ thuật', width: 100},
    {title: 'ĐVT', width: 150},
    {title: 'S.L tồn TT', width: 100},
    {title: 'Số lượng PO', width: 100},
    {title: 'S.L tồn đáp ứng', width: 100},
    {title: 'S.L đề nghị', width: 100},
    {title: 'Đơn giá', width: 150},
    {title: 'Ngày YC', width: 120},
    {title: 'Ghi chú', width: 150},
    {title: 'Thông tin thay thế', width: 150},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  list: [
    {width: 40},
    {width: 200},
    {width: 150},
    {width: 150, hideInCommercialType: true},
    {width: 200},
    {width: 100},
    {width: 150},
    {id: 'tonTT', width: 100},
    {id: 'po', width: 100},
    {id: 'tonDU', width: 100},
    {width: 100},
    {width: 150},
    {width: 120},
    {width: 150},
    {width: 150},
    {width: 80},
  ],
  ListStatus: [
    {
      value: null,
      label: 'Chọn trạng thái',
    },
    {
      value: '2',
      label: 'Không mua được',
    },
    {
      value: '3',
      label: 'Tồn đủ',
    },
  ],
};
