import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Keyboard,
  View,
  Text,
  ScrollView,
  Alert,
} from 'react-native';
import { Header, Icon, Divider, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_ProposePurchases from '../../../network/BM/API_ProposePurchases';
import API from '../../../network/API';
import ErrorHandler from '../../../error/handler';
import TabBarBottom from '../../component/TabBarBottom';
import Dialog from 'react-native-dialog';
import { DatePicker, Container, Content, Item, Label, Picker } from 'native-base';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RNPickerSelect from 'react-native-picker-select';
import LoadingComponent from '../../component/LoadingComponent';
import { ToastAndroid } from 'react-native';
import { FuncCommon } from '@utils';
import Toast from 'react-native-simple-toast';

const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});

class ItemProposedPurchasesComponent extends Component {
  constructor(props) {
    super(props);
    this.DataView = null;
    this.state = {
      Data: null,
      listDetail: [],
      staticParam: {
        ProposedPurchaseGuid: null,
        QueryOrderBy: 'ItemName desc',
      },
      isHidden: false,
      checkInLogin: '',
      visible: false,
      ListReasons: [
        {
          ReasonName: 'Chọn',
          ReasonId: '',
        },
      ],
      selected: undefined,
      RequestComment: '',
      ReasonName: '',
      Type: '',
      Receivers: [],
      FullName: '',
      loading: true,
      CancelRequest: false,
    };
  }

  componentDidMount(): void {
    this.getItem();
    API_ProposePurchases.GetCancelReasons()
      .then(rs => {
        var _listReasons = rs.data;
        let _reListReasons = [
          {
            ReasonName: 'Danh sách lý do',
            ReasonId: '',
          },
        ];
        for (var i = 0; i < _listReasons.length; i++) {
          _reListReasons.push({
            ReasonName: _listReasons[i].ReasonName,
            ReasonId: _listReasons[i].ReasonId,
            Type: _listReasons[i].Type,
          });
        }
        this.setState({
          ListReasons: _reListReasons,
        });
      })
      .catch(error => {
        console.log(error);
      });
    API.getProfile().then(rs => {
      this.setState({ FullName: rs.data.FullName });
    });
  }

  getItem() {
    let id = this.props.RowGuid || this.props.RecordGuid;
    API_ProposePurchases.ProposedPurchases_GetItem(id)
      .then(res => {
        this.DataView = JSON.parse(res.data.data);
        let check = {
          ProposedPurchaseGuid: this.props.RowGuid || this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_ProposePurchases.CheckLogin(check)
          .then(rs => {
            this.setState({ checkInLogin: rs.data.data });
          })
          .catch(error => {
            console.log(error.data.data);
          });
        this.GetDetail();
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  }

  GetDetail = () => {
    this.setState({ refreshing: true });
    this.state.staticParam.ProposedPurchaseGuid =
      this.props.RowGuid || this.props.RecordGuid;
    API_ProposePurchases.ProposedPurchaseDetails_ListDetail(
      this.state.staticParam,
    )
      .then(res => {
        this.state.listDetail = JSON.parse(res.data.data);
        this.setState({
          list: this.state.listDetail,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };

  ListEmpty = () => {
    if (this.state.listDetail.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  callbackOpenUpdate = () => {
    Actions.bmitemAddProposedPurchase({
      // itemData: this.DataView.Data,
      // rowData: this.DataView.DataDetail,
    });
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết đề nghị mua'}
          callBack={() => this.onPressBack()}
          CallbackFormEdit={() => this.onCancelRequests()}
        />
        <View style={{ flexDirection: 'row', padding: 10 }}>
          <View style={{ flex: 10 }}>
            <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
          </View>
          <View style={{ flex: 1 }}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHidden == false
                  ? this.setState({ isHidden: true })
                  : this.setState({ isHidden: false })
              }
            />
          </View>
        </View>
        {this.DataView !== null ? (
          <View style={{ flex: 1 }}>
            <View
              style={[
                this.state.isHidden == true ? styles.hidden : '',
                { padding: 20, backgroundColor: '#fff', flexDirection: 'row' },
              ]}>
              <View style={{ flex: 1 }}>
                <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                  Số đề nghị :
                </Text>
                <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                  Người đề nghị :
                </Text>
                <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                  Bộ phận :
                </Text>
                <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                  Loại đề nghị :
                </Text>
                <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                  Ngày đề nghị :
                </Text>
                <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                  TT xử lý :
                </Text>
                <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                  Mục đích :
                </Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-start' }}>
                <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                  {this.DataView.ProposedPurchaseId}
                </Text>
                <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                  {this.DataView.EmployeeName}
                </Text>
                <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                  {this.DataView.DepartmentName}
                </Text>
                <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                  {this.DataView.TypeOfRequest == 'SX'
                    ? 'Sản xuất'
                    : this.DataView.TypeOfRequest == 'TM'
                      ? 'Thương mại'
                      : this.DataView.TypeOfRequest == 'GCN'
                        ? 'Gia công ngoài'
                        : this.DataView.TypeOfRequest == 'GT'
                          ? 'Hỗ trọ sản xuất'
                          : ''}
                </Text>
                <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                  {this.customDate(this.DataView.CreatedDate)}
                </Text>
                <Text
                  style={[
                    [AppStyles.Textdefault],
                    this.DataView.Status == 'Y'
                      ? { color: AppColors.AcceptColor }
                      : { color: AppColors.PendingColor },
                  ]}>
                  {this.DataView.StatusWF}
                </Text>
                <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                  {this.DataView.Description}
                </Text>
              </View>
            </View>
            <Divider style={{ marginBottom: 10 }} />
            <View style={{ flexDirection: 'row', paddingRight: 10 }}>
              <Text
                style={[AppStyles.Titledefault, { flex: 1, paddingLeft: 10 }]}>
                Chi tiết
              </Text>
              <Icon
                style={{ color: 'black', flex: 1 }}
                name={'attach-file'}
                type="MaterialIcons"
                size={20}
                onPress={() => this.openAttachments()}
              />
            </View>
            <View style={{ flex: 6 }}>
              <ScrollView>
                {this.state.listDetail.map((item, index) => (
                  <View key={index}>
                    <ListItem
                      title={() => {
                        return (
                          <View style={{ flexDirection: 'row' }}>
                            <Text
                              style={[
                                AppStyles.Titledefault,
                                { width: '100%', flex: 10 },
                              ]}>
                              {item.ItemName}
                            </Text>
                            <View style={{ flex: 1 }}>
                              <Icon
                                color="#888888"
                                name={'chevron-thin-right'}
                                type="entypo"
                                size={20}
                              />
                            </View>
                          </View>
                        );
                      }}
                      subtitle={() => {
                        return (
                          <View>
                            <View
                              style={{
                                flexDirection: 'row',
                              }}>
                              <View
                                style={{
                                  flex: 1,
                                  paddingRight: 5,
                                }}>
                                <View
                                  style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                  }}>
                                  <Text style={[AppStyles.Textdefault]}>
                                    Ngày cần SD:{' '}
                                  </Text>
                                  <Text style={[AppStyles.Textdefault]}>
                                    {FuncCommon.ConDate(item.RequestDate, 0)}
                                  </Text>
                                </View>

                                <View
                                  style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                  }}>
                                  <Text style={[AppStyles.Textdefault]}>
                                    ĐVT:{' '}
                                  </Text>
                                  <Text style={[AppStyles.Textdefault]}>
                                    {item.UnitName}
                                  </Text>
                                </View>
                              </View>
                              <View
                                style={{
                                  flex: 1,
                                  paddingLeft: 5,
                                }}>
                                <View
                                  style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                  }}>
                                  <Text style={[AppStyles.Textdefault]}>
                                    SL cần mua:{' '}
                                  </Text>
                                  <Text style={[AppStyles.Textdefault]}>
                                    {this.addPeriod(item.QuantityUsed)}
                                  </Text>
                                </View>
                                <View
                                  style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                  }}>
                                  <Text style={[AppStyles.Textdefault]}>
                                    Đơn giá:{' '}
                                  </Text>
                                  <Text style={[AppStyles.Textdefault]}>
                                    {this.addPeriod(item.UnitPrice)}
                                  </Text>
                                </View>
                              </View>
                            </View>
                          </View>
                        );
                      }}
                      bottomDivider
                      titleStyle={AppStyles.Titledefault}
                      onPress={() => this.openDetail(item)}
                    />
                  </View>
                ))}
              </ScrollView>
            </View>
            {/*nút xử lý*/}
            <View style={{ flex: 1 }}>
              <TabBarBottom
                onDelete={() => this.onDelete()}
                onAttachment={() => this.openAttachments()}
                // callbackOpenUpdate={this.callbackOpenUpdate}
                //key để quay trở lại danh sách
                backListByKey="ProposedPurchases"
                //Truyễn dữ liệu sang màn hình comment
                keyCommentWF={{
                  ModuleId: '23',
                  RecordGuid: this.DataView.ProposedPurchaseGuid,
                  Title: this.DataView.ProposedPurchaseId,
                  Type: 1,
                }}
                // tiêu đề hiển thị trong popup xử lý phiếu
                Title={this.DataView.ProposedPurchaseId}
                //kiểm tra quyền xử lý
                Permisstion={this.DataView.Permisstion}
                //kiểm tra bước đầu quy trình
                checkInLogin={this.state.checkInLogin}
                onSubmitWF={(callback, CommentWF) =>
                  this.submitWorkFlow(callback, CommentWF)
                }
              />
            </View>
            {/* Popup */}
            <View>
              <Dialog.Container visible={this.state.visible}>
                <Dialog.Title style={AppStyles.Titledefault}>
                  {this.state.Title}
                </Dialog.Title>
                <Dialog.Description>
                  Yêu cầu sửa, hủy {this.DataView.ProposedPurchaseId}
                </Dialog.Description>
                <Item stackedLabel>
                  <Label style={[AppStyles.Labeldefault, { paddingLeft: 5 }]}>
                    Nguyên nhân
                  </Label>
                  {/* <RNPickerSelect
                        doneText="Xong"
                                        onValueChange={(value) => this.onValueChange(value)}
                                        items={this.state.ListReasons}
                                        placeholder={{}}
                                    /> */}
                  <Item picker>{this.renderReasons()}</Item>
                </Item>
                <Dialog.Input
                  style={{ borderBottomWidth: 1, borderBottomColor: '#3366CC' }}
                  label={this.state.desciption}
                  onChangeText={text => this.setState({ RequestComment: text })}
                  value={this.state.RequestComment}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <Dialog.Button
                    style={{
                      fontSize: 12,
                      backgroundColor: '#055a70',
                      color: '#fff',
                      borderRadius: 5,
                      marginLeft: 5,
                    }}
                    label="Xác nhận"
                    onPress={() => this.onYes()}
                  />
                  <Dialog.Button
                    style={{
                      fontSize: 12,
                      backgroundColor: '#dc3545',
                      color: '#fff',
                      borderRadius: 5,
                      marginLeft: 5,
                    }}
                    label="Đóng"
                    onPress={() => this.onClose()}
                  />
                </View>
              </Dialog.Container>
            </View>
          </View>
        ) : null}
      </View>
    );
  }
  onDelete = () => {
    let obj = {
      ProposedPurchaseGuid: this.DataView.ProposedPurchaseGuid,
    };
    this.setState({ loading: true });
    API_ProposePurchases.DeleteProposedPurchaseById(obj.ProposedPurchaseGuid)
      .then(response => {
        let errorCode = response.data.errorCode;
        if (errorCode == 200) {
          this.setState(
            {
              loading: false,
            },
            () => {
              Toast.showWithGravity(
                'Xóa thành công',
                Toast.SHORT,
                Toast.CENTER,
              );
              this.onPressBack();
            },
          );
        } else {
          Toast.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data.data);
      });
  };
  onPressBack = () => {
    Keyboard.dismiss();
    Actions.pop();

    Actions.refresh({
      moduleId: 'PPurchase',
      ActionTime: new Date().getTime(),
    });
  };
  renderReasons = () => (
    <Picker
      mode="dropdown"
      iosIcon={<Icon name="arrow-down" />}
      style={AppStyles.Textdefault}
      placeholder="Chọn nguyên nhân"
      placeholderStyle={{ color: '#bfc6ea' }}
      placeholderIconColor="#007aff"
      selectedValue={this.state.selected}
      onValueChange={this.onValueChange.bind(this)}>
      {this.state.ListReasons && this.state.ListReasons.length > 0 ? (
        this.state.ListReasons.map((_item, _index) => (
          <Picker.Item
            key={_index}
            label={_item.ReasonName}
            value={_item.ReasonId}
          />
        ))
      ) : (
        <Picker.Item key={_index} label="Chọn nguyên nhân" value="" />
      )}
    </Picker>
  );
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    console.log('callback==' + callback + CommentWF);
    let obj = {
      ProposedPurchaseGuid: this.props.RowGuid || this.props.RecordGuid,
      WorkFlowGuid: this.DataView.WorkFlowGuid,
      // LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'D') {
      API_ProposePurchases.Approve(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      API_ProposePurchases.NotApprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }

  onValueChange(value) {
    this.setState({
      selected: value,
    });
    let rs = this.state.ListReasons.find(x => x.ReasonId === value);
    if (rs != null) {
      this.setState({ ReasonName: rs.ReasonName });
      this.setState({ Type: rs.Type });
    }
  }
  onCancelRequests() {
    if (this.state.checkInLogin === 1) {
      Alert.alert('Thông báo', '', [{ text: 'Đóng' }], { cancelable: true });
    } else {
      this.setState({
        visible: true,
      });
    }
  }
  onYes() {
    let obj = {
      RecordGuid: this.DataView.ProposedPurchaseGuid,
      RequestComment: this.state.RequestComment,
      ReasonId: this.state.selected,
      ReasonName: this.state.ReasonName,
      Type: this.state.Type,
    };
    API_ProposePurchases.CancelRequest(obj)
      .then(res => {
        this.setState({
          visible: false,
        });
        Alert.alert('Thông báo', res.data.message, [{ text: 'Đóng' }], {
          cancelable: true,
        });
      })
      .catch(error => {
        this.setState({
          visible: true,
        });
        Alert.alert('Thông báo', res.data.message, [{ text: 'Đóng' }], {
          cancelable: true,
        });
      });
  }
  onClose() {
    this.setState({
      visible: false,
    });
  }
  openDetail(data) {
    Actions.bmitemPPDetails({ Data: data });
  }
  openAttachments() {
    var obj = {
      ModuleId: '2',
      RecordGuid: this.DataView.ProposedPurchaseGuid,
      notEdit: this.state.checkInLogin !== '1' ? true : false,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ItemProposedPurchasesComponent);
