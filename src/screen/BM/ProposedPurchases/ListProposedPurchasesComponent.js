import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, ListItem, SearchBar, Badge} from 'react-native-elements';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import TabNavigator from 'react-native-tab-navigator';
import API_ProposePurchases from '../../../network/BM/API_ProposePurchases';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Combobox from '../../component/Combobox';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '../../../utils';

const SCREEN_WIDTH = Dimensions.get('window').width;
class ListProposedPurchasesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      model: {},
      list: [],
      isFetching: false,
      openSearch: false,
      dTypeOfRequest: [
        {
          value: '',
          text: 'Tất cả',
        },
        {
          value: 'TM',
          text: 'Thương mại',
        },
        {
          value: 'SX',
          text: 'Sản xuất',
        },
        {
          value: 'GCN',
          text: 'Gia công ngoài',
        },
        {
          value: 'DV',
          text: 'CCDC_VPP',
        },
      ],
    };
    this.staticParam = {
      CurrentPage: 1,
      Length: 10,
      search: {value: ''},
      QueryOrderBy: 'CreatedDate desc',
      StartDate: new Date(),
      EndDate: new Date(),
      Status: 'W',
      TypeOfRequest: null,
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
    this.Total = 0;
  }

  listAll = () => {
    this.setState({refreshing: true}, () => {
      API_ProposePurchases.ProposedPurchases_ListAll(this.staticParam)
        .then(res => {
          let _data = JSON.parse(res.data.data);
          if (!_data.data || _data.data.length === 0) {
            this.setState({refreshing: false});
            return;
          }
          let _list = _data.data;
          this.state.list = this.state.list.concat(_list);
          if (this.staticParam.Status === 'W') {
            this.listtabbarBotom[0].Badge = _data.recordsTotal;
          }
          this.Total = _data.recordsTotal || 0;
          FuncCommon.Data_Offline_Set('ProposedPurchases', this.state.list);
          this.setState({list: this.state.list, refreshing: false});
        })
        .catch(error => {
          console.log(error);
          this.setState({refreshing: false});
        });
    });
  };

  GetAll = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.listAll();
      } else {
        let data = await FuncCommon.Data_Offline_Get('ProposedPurchases');
        this.setState({
          list: data,
          refreshing: false,
        });
      }
    });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'PPurchase') {
      this.updateSearch('');
    }
  }
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  updateSearch = text => {
    this.staticParam.search.value = text;
    this.setState(
      {
        list: [],
      },
      () => {
        this.GetAll();
      },
    );
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  handleLoadMore = () => {
    if (this.Total > this.state.list.length) {
      this.nextPage();
    }
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Đề nghị mua'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'BM'}
          // addForm={true}
          // CallbackFormAdd={() => Actions.bmitemAddProposedPurchase()}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  padding: 5,
                }}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian bắt đầu
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventStartDate: true})}>
                  <Text>
                    {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian kết thúc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventEndDate: true})}>
                  <Text>{FuncCommon.ConDate(this.staticParam.EndDate, 0)}</Text>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', padding: 5}}>
                <Text
                  style={[
                    AppStyles.Labeldefault,
                    {textAlign: 'center'},
                    styles.timeHeader,
                  ]}>
                  Lọc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionSearchDate()}>
                  <Icon
                    name={'down'}
                    type={'antdesign'}
                    size={18}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              style={[AppStyles.FormInput, {marginHorizontal: 10}]}
              onPress={() => this.onActionComboboxType()}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.staticParam.TypeName
                    ? {color: 'black'}
                    : {color: AppColors.gray},
                ]}>
                {this.staticParam.TypeName
                  ? this.staticParam.TypeName
                  : 'Chọn loại đề nghị...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  color={AppColors.gray}
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                />
              </View>
            </TouchableOpacity>
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.updateSearch(text)}
              value={this.staticParam.search.value}
            />
          </View>
        ) : null}
        <View style={{flex: 14}}>
          <FlatList
            style={{height: '100%'}}
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListFooterComponent={this.renderFooter}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.list}
            renderItem={({item, index}) => (
              <ListItem
                title={
                  <View
                    style={[
                      AppStyles.ListItemTitle,
                      {
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      },
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {item.ProposedPurchaseId}
                    </Text>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        item.Status == 'Y'
                          ? {color: AppColors.AcceptColor}
                          : {color: AppColors.PendingColor},
                      ]}>
                      {item.StatusWF}
                    </Text>
                  </View>
                }
                subtitle={() => {
                  return (
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 3}}>
                        <Text style={AppStyles.Textdefault}>
                          Người YC: {item.EmployeeName}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          Loại đề nghị:{' '}
                          {item.TypeOfRequest == 'TM'
                            ? 'Thương mại'
                            : item.TypeOfRequest == 'SX'
                            ? 'Sản xuất'
                            : item.TypeOfRequest == 'GCN'
                            ? 'Gia công ngoài'
                            : item.TypeOfRequest == 'GT'
                            ? 'Hỗ trợ sản xuất'
                            : ''}
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 2,
                          alignItems: 'flex-end',
                          justifyContent: 'flex-start',
                        }}>
                        <Text style={AppStyles.Textdefault}>
                          {FuncCommon.ConDate(item.CreatedDate, 0, 'iso')}
                        </Text>
                      </View>
                    </View>
                  );
                }}
                bottomDivider
                titleStyle={AppStyles.Titledefault}
                onPress={() => this.openView(item.ProposedPurchaseGuid)}
              />
            )}
            onEndReachedThreshold={0.5}
            onEndReached={this.handleLoadMore}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback => this.onClick(callback)}
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {this.state.dTypeOfRequest.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeType}
              data={this.state.dTypeOfRequest}
              nameMenu={'Chọn loại đề nghị'}
              eOpen={this.openComboboxType}
              position={'bottom'}
              value={null}
            />
          ) : null}
        </View>
      </View>
    );
  }

  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === null) {
      return;
    }
    this.staticParam.TypeOfRequest = rs.value;
    this.staticParam.TypeName = rs.text;
    this.updateSearch('');
  };

  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetAll();
  }
  //Load lại
  onRefresh = () => {
    this.setState({
      list: [],
    });
    this.GetAll();
  };
  onClick(data) {
    if (data == 'checkcircleo') {
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.search.value = '';
      this.staticParam.Status = 'Y';
      this.updateSearch('');
    } else if (data == 'profile') {
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.search.value = '';
      this.staticParam.Status = 'W';
      this.updateSearch('');
    } else if (data == 'export2') {
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.search.value = '';
      this.staticParam.Status = 'M';
      this.updateSearch('');
    } else if (data == 'combobox') {
      this.onActionCB();
    } else if (data === 'home') {
      Actions.app();
    }
  }
  //Lọc loại đề nghị
  onValueChange = value => {
    this.state.list = [];
    this.staticParam.CurrentPage = 1;
    this.staticParam.Length = 25;
    this.staticParam.search.value = '';
    this.staticParam.Type = value.value;
    this.setState({
      list: this.state.list,
    });
    this.GetAll();
  };

  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCB(item) {
    this._openCombobox();
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  openView(id) {
    Actions.bmitemProposedPurchases({RecordGuid: id, prevStack: 'listAll'});
  }
}

const styles = StyleSheet.create({
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabCenter: {
    flex: 1,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    paddingTop: 10,
    paddingBottom: 10,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListProposedPurchasesComponent);
