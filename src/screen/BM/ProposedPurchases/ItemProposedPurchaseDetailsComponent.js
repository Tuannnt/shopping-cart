import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
} from 'react-native';
import { Header, Icon, Divider, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_ProposePurchases from '../../../network/BM/API_ProposePurchases';
import { FuncCommon } from '@utils';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';

class ItemProposedPurchaseDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
    };
  }

  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar_Title title={'Chi tiết'} callBack={() => this.onPressBack()} />
        <ScrollView style={{ padding: 20, backgroundColor: '#fff' }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Mã nội bộ :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.ItemIDByProvider}
            </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Mã vật tư/Hàng hóa :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.ItemId}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Tên vật tư/Hàng hóa :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.ItemName}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>ĐVT :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.UnitName}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Đơn hàng SO :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.OrderNumber}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Nhóm VT :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.GroupName}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>KT dài :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.Length}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>KT rộng :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.Width}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>KT cao :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.Height}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Dung sai dài :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.ToleranceHighDiameter}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Dung sai rộng :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.ToleranceInnerDiameter}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Dung sai cao :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.ToleranceLength}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Tiêu chuẩn KT :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.Specification}
            </Text>
          </View>

          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Hãng SX :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.MarkerName}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              SL ĐM vật tư :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.props.Data.InventoryQuotaMin)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              SL theo đơn hàng :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.props.Data.Quantity)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              SL tồn kho :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.props.Data.InventoryNumber)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              S.L tồn đáp ứng :
            </Text>
            <Text
              style={[
                AppStyles.Textdefault,
                { flex: 2, textAlign: 'right', color: AppColors.red },
              ]}>
              {this.addPeriod(this.props.Data.QuantityReamin)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Số lượng cần mua :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.props.Data.QuantityUsed)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Số lượng phê duyệt :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.props.Data.QuantityApproved)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Đơn giá :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.addPeriod(this.props.Data.UnitPrice)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Ghi chú :</Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.Note}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 100 }}>
            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>
              Mục đích sử dụng :
            </Text>
            <Text
              style={[AppStyles.Textdefault, { flex: 2, textAlign: 'right' }]}>
              {this.props.Data.AlternateInformation}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }

  onPressBack() {
    Actions.pop();
  }

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ItemProposedPurchaseDetailsComponent);
