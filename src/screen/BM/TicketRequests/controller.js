import API_ProposePurchases from '../../../network/BM/API_ProposePurchases';
import {ErrorHandler} from '@error';
import {API_ApplyOverTimes} from '@network';

export default {
  getNumberAuto(value, callback) {
    API_ProposePurchases.GetNumberAuto(value)
      .then(res => {
        console.log(res + '=======> get Number auto');
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getAllOrder(search, callback) {
    let obj = {Search: search || ''};
    API_ApplyOverTimes.getAllOrder(obj)
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  GetAllOrders(value, callback) {},
  GetAllUnits(value, callback) {},
};
