import React, {Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API} from '../../../network';
import API_TicketRequests from '../../../network/AM/API_TicketRequests';
import TabBar_Title from '../../component/TabBar_Title';
import {Button, ListItem, Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import {FuncCommon} from '../../../utils';
import _ from 'lodash';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import Combobox from '../../component/Combobox';
import LoadingComponent from '../../component/LoadingComponent';
import listCombobox from './listCombobox';
import controller from './controller';
import RequiredText from '../../component/RequiredText';
const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 90,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 90,
  },
});
const headerTable = listCombobox.headerTable;

class AddTicketRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ListVoucherType: [
        {
          text: 'Nhập mua',
          value: 'N',
        },
        {
          text: 'Nhập hàng bán bị trả lại',
          value: 'L',
        },
        {
          text: 'Nhập thành phẩm sản xuất',
          value: 'M',
        },
        {
          text: 'Nhập khác',
          value: 'O',
        },
      ],
      selectItem: null,
      position: null,
      dVoucherType: '',
      dCustomers: '',
      dItems: '',
      dPO: '',
      dUnits: '',
      selectedItems: [],
      tabTTC: true,
      tabCT: false,
      ListCustomers: [],
      ListOrders: [],
      ListUnits: [],
      ListTicketRequestDetail: [],
      TicketRequestNo: '',
      RequestType: 'N',
      Title: '',
      RequestDate: new Date(),
      EmployeeName: '',
      DepartmentName: '',
      VoucherType: 'N',
      ObjectGuid: null,
      ObjectId: '',
      ObjectName: '',
      Description: '',
      ItemGuid: null,
      ItemId: '',
      ItemName: '',
      POGuid: null,
      OrderNumberId: '',
      UnitBefore: '',
      UnitName: '',
      Quantity: 0,
      QuantityActual: 0,
      UnitPriceBefore: 0,
      Amount: 0,
      SortOrder: 0,
      VoucherGuid: null,
      WarehouseId: '',
      PurchaseOrderId: '',
      editItem: null,
      pageItems: 1,
      pageCustomers: 1,
      length: 20,
      keyWordItems: '',
      keyWordCustomers: '',
      modelauto: {
        Value: null,
        IsEdit: null,
      },
      loading: true,
      rows: [],
      items: [],
    };
    this.state.dataMenu = [
      {
        key: 'delete',
        name: 'Xóa',
        icon: {
          name: 'delete',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
      // {
      //     key: 'edit',
      //     name: 'Sửa',
      //     icon: {
      //         name: 'edit',
      //         type: 'antdesign',
      //         color: AppColors.Maincolor,
      //     }
      // }
    ];
  }

  componentDidMount() {
    // setTimeout(() => {
    //     if (this.props.Id !== undefined) {
    //         var data = [{ data: this.props.Id }];
    //         this.QRCheck(data);
    //     }
    // }, 100);
    API.getProfile()
      .then(rs => {
        this.setState({
          EmployeeName: rs.data.FullName,
          DepartmentName: rs.data.DepartmentName,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
    Promise.all([
      this.GetTRNumberAuto(),
      this.GetCustomersAll(),
      this.GetUnitsAllJExcel(),
      this.getAllOrder(),
    ]);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'QRCode') {
      var link = nextProps.Data.Value; // dữ liệu QR quét được
      this.QRCheck(nextProps.Data.Value);
    }
  }
  //#region hàm Quay lại
  callBack(rs) {
    this.setState({loading: false});
    Keyboard.dismiss();
    +Actions.pop();
    Actions.refresh({
      moduleId: 'BackTicket',
      Data: {
        Module: 'TicketRequestsImport',
        Subject: this.state.DataView.Description,
        StartDate:
          this.state.DataView.RequestDate === '' ||
          this.state.DataView.RequestDate === null
            ? null
            : FuncCommon.ConDate(this.state.DataView.RequestDate, 0),
        EndDate:
          this.state.DataView.RequestDate === '' ||
          this.state.DataView.RequestDate === null
            ? null
            : FuncCommon.ConDate(this.state.DataView.RequestDate, 0),
        RowGuid: rs,
        AssignTo: [],
      },
      ActionTime: new Date().getTime(),
    });
  }
  //endregion
  GetTRNumberAuto() {
    API_TicketRequests.GetNumberAuto({AliasId: 'QTDSCT_DNN', VoucherType: 'N'})
      .then(rs => {
        if (
          rs.data.data !== null &&
          rs.data.data !== '' &&
          rs.data.data !== undefined
        ) {
          this.state.modelauto.Value = JSON.parse(rs.data.data).Value;
          this.state.modelauto.IsEdit = JSON.parse(rs.data.data).IsEdit;
          this.setState({
            TicketRequestNo: JSON.parse(rs.data.data).Value,
            modelauto: {
              Value: this.state.modelauto.Value,
              IsEdit: this.state.modelauto.IsEdit,
            },
            loading: false,
          });
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  }
  GetCustomersAll() {
    var obj = {
      CurrentPage: this.state.pageCustomers,
      Length: this.state.length,
      Search: this.state.keyWordCustomers,
      VoucherType: this.state.VoucherType,
    };
    API_TicketRequests.GetCustomersAll(obj)
      .then(rs => {
        var _lstCustomers = JSON.parse(rs.data.data);
        var _reListCustomers = [
          {
            text: 'Chọn đối tượng',
            value: null,
          },
        ];
        for (var i = 0; i < _lstCustomers.length; i++) {
          _reListCustomers.push({
            text:
              _lstCustomers[i].ObjectId + ' - ' + _lstCustomers[i].ObjectName,
            value: _lstCustomers[i].ObjectGuid,
            ObjectId: _lstCustomers[i].ObjectId,
            ObjectName: _lstCustomers[i].ObjectName,
          });
        }
        this.setState({
          ListCustomers: _reListCustomers,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);
    this.setState({rows: res});
  };
  nextpageCustomers = callback => {
    this.state.pageCustomers++;
    this.setState({
      pageCustomers: this.state.pageCustomers,
    });
    var obj = {
      CurrentPage: this.state.pageCustomers,
      Length: this.state.length,
      Search: this.state.keyWordCustomers,
      VoucherType: this.state.VoucherType,
    };

    API_TicketRequests.GetCustomersAll(obj)
      .then(rs => {
        var _lstCustomers = JSON.parse(rs.data.data);
        var _reListCustomers = [];
        for (var i = 0; i < _lstCustomers.length; i++) {
          _reListCustomers.push({
            text:
              _lstCustomers[i].ObjectId + ' - ' + _lstCustomers[i].ObjectName,
            value: _lstCustomers[i].ObjectGuid,
            ObjectId: _lstCustomers[i].ObjectId,
            ObjectName: _lstCustomers[i].ObjectName,
          });
        }
        callback(_reListCustomers);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  searchCustomers = (callback, textsearch) => {
    this.setState({
      pageCustomers: 1,
      keyWordCustomers: textsearch,
    });
    var obj = {
      CurrentPage: this.state.pageCustomers,
      Length: this.state.length,
      Search: this.state.keyWordCustomers,
      VoucherType: this.state.VoucherType,
    };
    API_TicketRequests.GetCustomersAll(obj)
      .then(rs => {
        var _lstCustomers = JSON.parse(rs.data.data);
        var _reListCustomers = [];
        for (var i = 0; i < _lstCustomers.length; i++) {
          _reListCustomers.push({
            text:
              _lstCustomers[i].ObjectId + ' - ' + _lstCustomers[i].ObjectName,
            value: _lstCustomers[i].ObjectGuid,
            ObjectId: _lstCustomers[i].ObjectId,
            ObjectName: _lstCustomers[i].ObjectName,
          });
        }
        callback(_reListCustomers);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  GetUnitsAllJExcel() {
    API_TicketRequests.GetUnitsAllJExcel('A')
      .then(rs => {
        var _lstUnits = JSON.parse(rs.data.data);
        var _reListUnits = [];
        for (var i = 0; i < _lstUnits.length; i++) {
          _reListUnits.push({
            text: _lstUnits[i].UnitName,
            value: _lstUnits[i].UnitId,
          });
        }
        this.setState({
          ListUnits: _reListUnits,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  openOrders(index) {
    this.setState({currentIndexItem: index}, () => {
      this._openComboboxOrder();
    });
  }
  _openComboboxOrder() {}
  openComboboxOrder = d => {
    this._openComboboxOrder = d;
  };
  ChangeOrder = rs => {
    const {currentIndexItem, rows} = this.state;
    let {value} = rs;
    if (!rs) {
      return;
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.OrderNumberId = value;
      }
      return row;
    });
    this.setState({rows: data, currentIndexItem: null});
  };
  CustomTTC = () => (
    <ScrollView horizontal={false} style={{flexDirection: 'column'}}>
      {/* Số phiếu */}
      <View style={{flexDirection: 'row'}}>
        <View style={{padding: 10, flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={AppStyles.Labeldefault}>Số phiếu</Text>
            {/* <Text style={{ color: 'red' }}> *</Text> */}
          </View>
          <TextInput
            style={AppStyles.FormInput}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            multiline={true}
            value={this.state.TicketRequestNo}
            editable={this.state.modelauto.IsEdit}
          />
        </View>
        <View style={{padding: 10, flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={AppStyles.Labeldefault}>Ngày yêu cầu</Text>
            {/* <Text style={{ color: 'red' }}> *</Text> */}
          </View>
          {/* <View style={AppStyles.FormInput}> */}
          <View style={{flexDirection: 'row'}}>
            <TextInput
              style={[AppStyles.FormInput, {flex: 4}]}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              multiline={true}
              value={FuncCommon.ConDate(this.state.RequestDate, 0)}
              editable={false}
            />
            <View style={{flex: 1}}>
              <DatePicker
                locale="vie"
                style={{width: 150}}
                date={this.state.RequestDate}
                androidMode="spinner"
                mode="date"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 36,
                  },
                }}
                hideText={true}
                onDateChange={date => {
                  this.setState({RequestDate: date});
                }}
              />
            </View>
          </View>
        </View>
      </View>
      {/* Ngày yêu cầu */}

      {/* Người yêu cầu */}
      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Người yêu cầu</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.state.EmployeeName}
          editable={false}
        />
      </View>
      {/* Bộ phận */}
      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Bộ phận</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.state.DepartmentName}
          editable={false}
        />
      </View>

      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>
            Nội dung nhập <RequiredText />
          </Text>
        </View>
        <TouchableOpacity
          style={[AppStyles.FormInput, {flexDirection: 'row'}]}
          onPress={() => this.onActionComboboxVoucherType()}>
          <Text
            style={[
              AppStyles.TextInput,
              this.state.dVoucherType == ''
                ? {color: AppColors.gray, flex: 3}
                : {flex: 3},
            ]}>
            {this.state.dVoucherType !== ''
              ? this.state.dVoucherType
              : 'Chọn nội dung nhập...'}
          </Text>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
            />
          </View>
        </TouchableOpacity>
      </View>

      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>
            Tên đối tượng <RequiredText />
          </Text>
        </View>
        <TouchableOpacity
          style={[AppStyles.FormInput, {flexDirection: 'row'}]}
          onPress={() => this.onActionComboboxCustomers()}>
          <Text
            style={[
              AppStyles.TextInput,
              this.state.dCustomers == ''
                ? {color: AppColors.gray, flex: 3}
                : {flex: 3},
            ]}>
            {this.state.dCustomers !== ''
              ? this.state.dCustomers
              : 'Chọn đối tượng...'}
          </Text>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
            />
          </View>
        </TouchableOpacity>
      </View>
      {/* diễn giải */}
      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Diễn giải</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.state.Description}
          onChangeText={text => this.setState({Description: text})}
        />
      </View>
    </ScrollView>
  );
  helperGetUnit = id => {
    let index = this.state.ListUnits.findIndex(i => i.value == id);
    if (index === -1) {
      return '';
    }
    return this.state.ListUnits[index].text;
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.ItemId}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 250}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.ItemName}
          </Text>
        </View>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: 250}]}
          onPress={() => {
            if (this.props.rowData) {
              return;
            }
            this.openOrders(index);
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.OrderNumber,
                {color: 'black', fontSize: 14},
              ]}>
              {row.OrderNumberId ? row.OrderNumberId : 'Chọn đơn hàng'}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 70}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {this.helperGetUnit(row.UnitId)}
          </Text>
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom]}
            underlineColorAndroid="transparent"
            value={row.Quantity}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={Quantity => {
              this.handleChangeRows(Quantity + '', index, 'Quantity');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom]}
            underlineColorAndroid="transparent"
            value={row.UnitPriceBefore}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={UnitPriceBefore => {
              this.handleChangeRows(
                UnitPriceBefore + '',
                index,
                'UnitPriceBefore',
              );
            }}
          />
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextInput]}>
            {row.Quantity && row.UnitPriceBefore
              ? +row.Quantity * +row.UnitPriceBefore
              : ''}
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td, {width: 80}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  CustomCT = () => (
    <View style={{marginTop: 15}}>
      <ScrollView
        horizontal={true}
        style={{paddingBottom: 25, marginBottom: 20}}>
        <View style={{flexDirection: 'column'}}>
          <View style={{flexDirection: 'row'}}>
            {headerTable.map(item => (
              <TouchableOpacity
                key={item.title}
                style={[AppStyles.table_th, {width: item.width}]}>
                <Text style={AppStyles.Labeldefault}>{item.title}</Text>
              </TouchableOpacity>
            ))}
          </View>
          <View>
            {this.state.rows.length > 0 ? (
              <FlatList
                data={this.state.rows}
                renderItem={({item, index}) => {
                  return this.itemFlatList(item, index);
                }}
                keyExtractor={(rs, index) => index.toString()}
              />
            ) : (
              <TouchableOpacity style={[AppStyles.table_foot]}>
                <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                  Không có dữ liệu!
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </ScrollView>
    </View>
  );
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={[AppStyles.container, {flex: 1}]}>
            <TabBar_Title
              title={'Thêm đề nghị nhập'}
              callBack={() => this.onPressBack()}
              FormQR={true}
              QRMultiple={true}
              IconSubmit={false}
              CallbackIconSubmit={() => this.AddDetail()}
            />
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderRadius: 10,
                flex: 1,
                borderWidth: 0.5,
                flexDirection: 'row',
                height: 40,
              }}>
              <TouchableOpacity
                onPress={() => this.OnChangeTab('1')}
                style={styles.TabarPending}>
                <Text
                  style={
                    this.state.tabTTC === true
                      ? {color: AppColors.Maincolor}
                      : {color: 'black'}
                  }>
                  Thông tin chung
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.OnChangeTab('2')}
                style={styles.TabarApprove}>
                <Text
                  style={
                    this.state.tabCT === true
                      ? {color: AppColors.Maincolor}
                      : {color: 'black'}
                  }>
                  Chi tiết
                </Text>
              </TouchableOpacity>
            </View>
            {/* hiển thị thông tin chung */}
            <View style={{flex: 15}}>
              {this.state.tabTTC === true
                ? this.CustomTTC()
                : this.state.tabCT === true
                ? this.CustomCT()
                : null}
            </View>
            {/* Submit*/}
            <View>
              <Button
                buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
                title="Lưu"
                onPress={() => this.Submit()}
              />
            </View>
            <MenuActionCompoment
              callback={this.actionMenuCallback}
              data={this.state.dataMenu}
              name={this.state.NameMenu}
              eOpen={this.openMenu}
              position={'bottom'}
            />
            <Combobox
              value={this.state.VoucherType}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeVoucherType}
              data={this.state.ListVoucherType}
              nameMenu={'Chọn nội dung nhập'}
              eOpen={this.openComboboxVoucherType}
              position={'bottom'}
            />
            {this.state.ListCustomers.length > 0 ? (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeCustomers}
                data={this.state.ListCustomers}
                nameMenu={'Chọn đối tượng'}
                eOpen={this.openComboboxCustomers}
                position={'bottom'}
                paging={true}
                callback_Paging={callBack => this.nextpageCustomers(callBack)}
                callback_SearchPaging={(callback, textsearch) =>
                  this.searchCustomers(callback, textsearch)
                }
              />
            ) : null}

            {this.state.ListOrders.length > 0 ? (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeOrder}
                data={this.state.ListOrders}
                nameMenu={'Chọn'}
                eOpen={this.openComboboxOrder}
                position={'bottom'}
                value={null}
                callback_SearchPaging={(callback, textsearch) =>
                  this.getAllOrder(callback, textsearch)
                }
              />
            ) : null}
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  getAllOrder = (callback, search = '') => {
    clearTimeout(this.time);
    // debounce call api
    this.time = setTimeout(() => {
      controller.getAllOrder(search, rs => {
        let data = JSON.parse(rs).map(item => ({
          value: item.id,
          text: item.name,
        }));
        data.unshift({
          value: null,
          text: 'Chọn sản phẩm',
        });
        if (callback) {
          callback(data);
        } else {
          this.setState({ListOrders: data});
        }
      });
    }, 300);
  };
  ChangeVoucherType = value => {
    this.setState(
      {
        dVoucherType: value.text,
        VoucherType: value.value,
      },
      () => {
        this.GetCustomersAll();
      },
    );
  };
  _openComboboxVoucherType() {}
  openComboboxVoucherType = d => {
    this._openComboboxVoucherType = d;
  };
  onActionComboboxVoucherType() {
    this._openComboboxVoucherType();
  }

  ChangeCustomers = value => {
    this.setState({
      dCustomers: value.text,
      ObjectGuid: value.value,
    });
    var rs = this.state.ListCustomers.find(x => x.value === value.value);
    if (rs !== null && rs !== undefined) {
      this.setState({
        ObjectId: rs.ObjectId,
        ObjectName: rs.ObjectName,
      });
    }
  };
  _openComboboxCustomers() {}
  openComboboxCustomers = d => {
    this._openComboboxCustomers = d;
  };
  onActionComboboxCustomers() {
    this._openComboboxCustomers();
  }
  GetItemById = async (id, callback) => {
    await API_TicketRequests.GetItemById(id)
      .then(res => {
        console.log('1');
        if (res.data.errorCode === 200) {
          callback(JSON.parse(res.data.data)[0] || {});
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
  QRCheck = async data => {
    let {rows} = this.state;
    await Promise.all(
      data.map(async item => {
        if (!item.data) {
          return;
        }
        await this.GetItemById(item.data, res => {
          console.log(res);
          rows.push({
            UnitBefore: res.UnitId,
            ItemId: res.ItemId,
            ItemName: res.ItemName || '',
            ItemGuid: res.ItemGuid,
            UnitId: res.UnitId || '',
            UnitName: res.UnitName || '',
            WarehouseId: res.WarehouseId || '',
            WarehouseAccountId: res.WarehouseAccountId || '',
          });
        });
      }),
    )
      .then(response => {
        console.log(rows);
      })
      .catch(err => {
        console.log(err);
      });
    this.setState({rows});
  };
  ChangeItems = value => {
    if (value.value !== null) {
      var obj = this.state.ListTicketRequestDetail[this.state.position];
      obj.ItemGuid = value.value;
      this.setState({
        dItems: value.text,
      });
      var rs = this.state.ListItems.find(x => x.value === value.value);
      if (rs !== null && rs !== undefined) {
        obj.UnitBefore = rs.UnitId;
        obj.UnitName = rs.UnitName;
        obj.ItemId = rs.text.split(' -')[0];
        obj.ItemName = rs.ItemName;
        this.setState({
          ListTicketRequestDetail: this.state.ListTicketRequestDetail,
        });
      }
    }
    // this.ChangeUnit(this.state.UnitBefore)
  };
  _openComboboxItems() {}
  openComboboxItems = d => {
    this._openComboboxItems = d;
  };
  onActionComboboxItems(index) {
    this.setState({
      position: index,
    });
    this._openComboboxItems();
  }
  _openComboboxUnits() {}
  openComboboxUnits = d => {
    this._openComboboxUnits = d;
  };
  onActionComboboxUnits(index) {
    this.setState({
      position: index,
    });
    this._openComboboxUnits();
  }

  _openComboboxPO() {}
  openComboboxPO = d => {
    this._openComboboxPO = d;
  };
  onActionComboboxPO() {
    this._openComboboxPO();
  }
  setQuantity(value, index) {
    this.state.ListTicketRequestDetail[index].Quantity =
      value !== '' && value !== null ? parseFloat(value) : 0;
    this.state.ListTicketRequestDetail[index].Amount =
      this.state.ListTicketRequestDetail[index].Quantity *
      this.state.ListTicketRequestDetail[index].UnitPriceBefore;
    this.setState(
      (ListTicketRequestDetail = this.state.ListTicketRequestDetail),
    );
  }
  setUnitPrice(value, index) {
    this.state.ListTicketRequestDetail[index].UnitPriceBefore =
      value !== '' && value !== null ? parseFloat(value) : 0;
    this.state.ListTicketRequestDetail[index].Amount =
      this.state.ListTicketRequestDetail[index].Quantity *
      this.state.ListTicketRequestDetail[index].UnitPriceBefore;
    this.setState(
      (ListTicketRequestDetail = this.state.ListTicketRequestDetail),
    );
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'TKImport', ActionTime: new Date().getTime()});
  }
  //push vào mảng ListTicketRequestDetail
  AddDetail() {
    var obj = this.state.ListTicketRequestDetail;
    if (this.state.editItem !== null) {
      obj[this.state.editItem] = {
        ItemGuid: this.state.ItemGuid,
        ItemId: this.state.ItemId,
        ItemName: this.state.ItemName,
        POGuid: this.state.POGuid,
        OrderNumberId: this.state.OrderNumberId,
        UnitBefore: this.state.UnitBefore,
        UnitName: this.state.UnitName,
        Quantity: this.state.Quantity,
        QuantityRemain: this.state.Quantity,
        QuantityActual: this.state.QuantityActual,
        UnitPriceBefore: this.state.UnitPriceBefore,
        Amount: this.state.Amount,
        SortOrder: this.state.SortOrder,
        VoucherGuid: this.state.VoucherGuid,
        WarehouseId: this.state.WarehouseId,
        PurchaseOrderId: this.state.PurchaseOrderId,
      };
      this.setState({editItem: null});
    } else {
      if (this.state.ItemGuid !== '' && this.state.ItemGuid !== null) {
        obj.push({
          ItemGuid: this.state.ItemGuid,
          ItemId: this.state.ItemId,
          ItemName: this.state.ItemName,
          POGuid: this.state.POGuid,
          OrderNumberId: this.state.OrderNumberId,
          UnitBefore: this.state.UnitBefore,
          UnitName: this.state.UnitName,
          Quantity: this.state.Quantity,
          QuantityRemain: this.state.Quantity,
          QuantityActual: this.state.QuantityActual,
          UnitPriceBefore: this.state.UnitPriceBefore,
          Amount: this.state.Amount,
          SortOrder: this.state.SortOrder + 1,
          VoucherGuid: this.state.VoucherGuid,
          WarehouseId: this.state.WarehouseId,
          PurchaseOrderId: this.state.PurchaseOrderId,
        });
      }
    }
    this.setState({
      ListTicketRequestDetail: obj,
      ItemGuid: null,
      ItemId: '',
      ItemName: '',
      POGuid: null,
      OrderNumberId: '',
      UnitBefore: '',
      UnitName: '',
      Quantity: '0',
      QuantityActual: '0',
      UnitPriceBefore: '0',
      Amount: '0',
      VoucherGuid: null,
      WarehouseId: '',
      PurchaseOrderId: '',
    });
  }
  Submit = () => {
    let {rows} = this.state;
    if (!this.state.ObjectId) {
      Alert.alert(
        'Thông báo',
        'Yêu cầu chọn đối tượng',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (!this.state.rows || this.state.rows.length === 0) {
      Alert.alert(
        'Thông báo',
        'Yêu cầu nhập thông tin chi tiết',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    let _row = rows.map(row => {
      row.QuantityRemain = row.Quantity;
      if (row.Quantity && row.UnitPriceBefore) {
        row.Amount = (+row.Quantity * +row.UnitPriceBefore).toString();
      }
      return row;
    });
    var model = {
      TicketRequestNo: this.state.TicketRequestNo,
      RequestType: this.state.RequestType,
      Title: this.state.Title,
      RequestDate: this.state.RequestDate,
      VoucherType: this.state.VoucherType,
      ObjectGuid: this.state.ObjectGuid,
      ObjectId: this.state.ObjectId,
      ObjectName: this.state.ObjectName,
      Description: this.state.Description,
    };
    let _data = new FormData();
    _data.append('model', JSON.stringify(model));
    _data.append('TicketRequestDetails', JSON.stringify(rows));
    _data.append('auto', JSON.stringify(this.state.modelauto));
    API_TicketRequests.Insert(_data)
      .then(res => {
        if (res.data.errorCode === 200) {
          Alert.alert(
            'Thông báo',
            'Thêm mới thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.callBackList(JSON.parse(res.data.data));
        } else {
          Alert.alert(
            'Thông báo',
            'Có lỗi xảy ra',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  //#region hàm Quay lại
  callBackList(rs) {
    this.setState({loading: false});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'TKImport',
      Data: {
        Module: 'AM_TicketRequestsImport',
        Subject: this.state.Description || '',
        StartDate:
          this.state.RequestDate === '' || this.state.RequestDate === null
            ? null
            : FuncCommon.ConDate(this.state.RequestDate, 0),
        EndDate:
          this.state.RequestDate === '' || this.state.RequestDate === null
            ? null
            : FuncCommon.ConDate(this.state.RequestDate, 0),
        RowGuid: rs,
        AssignTo: [],
      },
      ActionTime: new Date().getTime(),
    });
  }
  //endregion
  OnChangeTab(data) {
    if (data == '1') {
      this.setState({
        tabCT: false,
        tabTTC: true,
      });
    } else if (data == '2') {
      this.setState({
        tabTTC: false,
        tabCT: true,
      });
    }
  }
  //Menu Item
  actionMenuCallback = d => {
    if (d === 'delete') {
      var list = this.state.ListTicketRequestDetail;
      list.splice(this.state.position, 1);
      this.setState({
        ListTicketRequestDetail: list,
      });
    } else if (d === 'edit') {
      this.setState({
        ItemGuid: this.state.selectItem.ItemGuid,
        ItemId: this.state.selectItem.ItemId,
        ItemName: this.state.selectItem.ItemName,
        POGuid: this.state.selectItem.POGuid,
        OrderNumberId: this.state.selectItem.OrderNumberId,
        UnitBefore: this.state.selectItem.UnitBefore,
        UnitName: this.state.selectItem.UnitName,
        Quantity: this.state.selectItem.Quantity,
        QuantityActual: this.state.selectItem.QuantityActual,
        UnitPriceBefore: this.state.selectItem.UnitPriceBefore,
        Amount: this.state.selectItem.Amount,
        SortOrder: this.state.selectItem.SortOrder,
        VoucherGuid: this.state.selectItem.VoucherGuid,
        WarehouseId: this.state.selectItem.WarehouseId,
        PurchaseOrderId: this.state.selectItem.PurchaseOrderId,
        editItem: this.state.position,
      });
    }
    console.log('Event: ', d);
  };
  _openMenu() {}
  openMenu = d => {
    this._openMenu = d;
  };
  onAction(item, index) {
    this.setState({
      selectItem: item,
      position: index,
    });
    var _option = {
      isChangeAction: true,
      data: this.state.dataMenu,
      NameMenu: 'Chức năng',
    };
    this._openMenu(_option);
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
}
const mapStateToProps = state => ({
  applications: state.user.data,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(AddTicketRequestsComponent);
