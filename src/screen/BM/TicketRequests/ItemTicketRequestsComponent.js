import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  Keyboard,
  View,
  FlatList,
  Text,
  Alert,
  RefreshControl,
  Animated,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import API_TicketRequests from '../../../network/BM/API_TicketRequests';
import Dialog from 'react-native-dialog';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import LoadingComponent from '../../component/LoadingComponent';
import {FuncCommon} from '../../../utils';
import Toast from 'react-native-simple-toast';

const styles = StyleSheet.create({
  hidden: {display: 'none'},
});
class ItemTicketRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TicketRequestGuid: '',
      model: {},
      List: [],
      isFetching: false,
      isHidden: false,
      check: false,
      Title: 'Thông báo',
      desciption: 'Bạn có muốn xác nhận thông tin',
      text: '3463',
      loading: true,
      //hiển thị thông tin chung
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
    };
    this.listtabbarBotom = [
      {
        Title: 'Xác nhận thông tin',
        Icon: 'checksquareo',
        Type: 'antdesign',
        Value: 'checksquareo',
        Checkbox: false,
        OffEditcolor: true,
      },
    ];
  }
  componentDidMount = () => {
    this.GetTicketRequestsById();
    this.Skill();
    this.setViewOpen('ViewTotal');
  };
  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      {toValue: 0, duration: 333},
    );
  };
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ViewAll: !this.state.ViewAll});
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ViewDetail: !this.state.ViewDetail});
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ViewDetail: true, ViewAll: true});
    }
  };
  //#endregion

  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <TabBar_Title
          title={'Chi tiết đề nghị'}
          callBack={() => this.onPressBack()}
        />
        <View style={{flex: 1}}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewAll')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
            </View>
            <Animated.View
              style={{
                transform: [{rotate: rotateStart_ViewAll}, {perspective: 4000}],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewAll !== true ? null : (
            <View style={[{padding: 10}]}>
              <View style={[{flexDirection: 'row'}]}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                    Số phiếu :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                    Ngày yêu cầu :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                    Người yêu cầu :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                    Bộ phận :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                    Nội dung nhập :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                    Tên đối tượng :
                  </Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                    {this.state.model.TicketRequestNo}
                  </Text>
                  <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                    {FuncCommon.ConDate(this.state.model.RequestDate, 0)}
                  </Text>
                  <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                    {this.state.model.EmployeeName}
                  </Text>
                  <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                    {this.state.model.DepartmentName}
                  </Text>
                  <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                    {this.state.model.VoucherType === 'N'
                      ? 'Nhập mua'
                      : this.state.model.VoucherType === 'L'
                      ? 'Nhập hàng bán bị trả lại'
                      : this.state.model.VoucherType === 'M'
                      ? 'Nhập thành phẩm'
                      : this.state.model.VoucherType === 'O'
                      ? 'Nhập khác'
                      : ''}
                  </Text>
                  <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                    {this.state.model.ObjectName}
                  </Text>
                </View>
              </View>
              <View style={[{}]}>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Diễn giải:
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.state.model.Description}
                </Text>
              </View>
            </View>
          )}
          <Divider />
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewDetail')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewDetail},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewDetail !== true ? null : (
            <View style={{flex: 1}}>
              <FlatList
                data={this.state.List}
                style={{flex: 1}}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      padding: 10,
                      borderBottomColor: AppColors.gray,
                      borderBottomWidth: 0.5,
                    }}
                    onPress={() =>
                      Actions.listTicketRequestsDetailsImportComponent({
                        item,
                      })
                    }>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <View style={{flexDirection: 'row'}}>
                          <Text
                            style={[
                              AppStyles.Titledefault,
                              {width: '100%', flex: 10},
                            ]}>
                            {item.ItemName}
                          </Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                          <Text style={[AppStyles.Textdefault, {width: '50%'}]}>
                            Số lượng: {item.Quantity}
                          </Text>
                          <Text style={[AppStyles.Textdefault, {width: '50%'}]}>
                            ĐVT: {item.UnitName}
                          </Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                          <Text style={[AppStyles.Textdefault, {width: '50%'}]}>
                            Đ.Giá: {FuncCommon.addPeriod(item.UnitPriceBefore)}
                          </Text>
                          <Text style={[AppStyles.Textdefault, {width: '50%'}]}>
                            T.Tiền:{' '}
                            <Text style={{fontWeight: 'bold'}}>
                              {FuncCommon.addPeriod(item.Amount)}
                            </Text>
                          </Text>
                        </View>
                      </View>
                      <View style={{justifyContent: 'center'}}>
                        <Icon
                          color="#888888"
                          name={'chevron-thin-right'}
                          type="entypo"
                          size={20}
                          onPress={() =>
                            Actions.listTicketRequestsDetailsImportComponent({
                              item,
                            })
                          }
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['red', 'green', 'blue']}
                  />
                }
              />
            </View>
          )}
        </View>
        {/* <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onConfirm()}
          />
        </View> */}
        {/* Thông báo */}
        <Dialog.Container visible={this.state.check}>
          <Dialog.Title>Thông báo</Dialog.Title>
          <Dialog.Description>
            Bạn có muốn xác nhận thông tin ?
          </Dialog.Description>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Dialog.Button
              style={{color: '#055a70', marginLeft: 5}}
              label="Xác nhận"
              onPress={() => this.onYes()}
            />
            <Dialog.Button
              style={{color: '#445464', marginLeft: 5}}
              label="Không xác nhận"
              onPress={() => this.onNo()}
            />
            <Dialog.Button
              style={{color: '#dc3545', marginLeft: 5}}
              label="Đóng"
              onPress={() => this.onClose()}
            />
          </View>
        </Dialog.Container>
      </View>
    );
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'TKImport', ActionTime: new Date().getTime()});
  }
  //Lấy thông tin đề nghị nhập
  GetTicketRequestsById = () => {
    API_TicketRequests.GetTicketRequestsById(
      this.props.RowGuid || this.props.RecordGuid,
    )
      .then(res => {
        console.log(res);
        this.setState({
          model: JSON.parse(res.data.data),
        });
        this.GetTicketRequestsDetailsAll();
      })
      .catch(error => {});
  };
  //Lấy danh sách đề nghị nhập
  GetTicketRequestsDetailsAll = () => {
    this.setState({refreshing: true});
    API_TicketRequests.GetTicketRequestsDetailsAll({
      TicketRequestGuid: this.props.RowGuid || this.props.RecordGuid,
    })
      .then(res => {
        this.state.List = this.state.List.concat(JSON.parse(res.data.data));
        this.setState({
          List: this.state.List,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };
  //Xác nhận thông tin
  onConfirm() {
    if (this.state.model.Status === 'Y') {
      Alert.alert(
        'Thông báo',
        'Phiếu đã duyệt',
        [{text: 'OK', onPress: () => Keyboard.dismiss()}],
        {cancelable: false},
      );
      return;
    }
    this.setState({
      check: true,
    });
  }
  onClose() {
    this.setState({
      check: false,
    });
  }
  onYes = () => {
    API_TicketRequests.CheckStatus({
      TicketRequestGuid: this.state.model.TicketRequestGuid,
      Status: 'Y',
    })
      .then(res => {
        if (res.data.errorCode == 200) {
          this.setState(
            {
              check: false,
            },
            () => {
              setTimeout(() => {
                Toast.showWithGravity(
                  'Xử lý thành công',
                  Toast.SHORT,
                  Toast.CENTER,
                );
              }, 1000);
            },
          );
        }
      })
      .catch(error => {
        this.setState({
          check: true,
        });
      });
  };
  onNo = () => {
    API_TicketRequests.CheckStatus({
      TicketRequestGuid: this.state.model.TicketRequestGuid,
      Status: 'N',
    })
      .then(res => {
        if (res.data.errorCode == 200) {
          this.setState(
            {
              check: false,
            },
            () => {
              setTimeout(() => {
                Toast.showWithGravity(
                  'Xử lý thành công',
                  Toast.SHORT,
                  Toast.CENTER,
                );
              }, 1000);
            },
          );
        }
      })
      .catch(error => {
        this.setState({
          check: true,
        });
        alert(res.data.message);
      });
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemTicketRequestsComponent);
