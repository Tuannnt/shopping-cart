import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  StyleSheet,
  ToastAndroid,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import API_CheckRequests from '../../../network/BM/API_CheckRequests';
import {Container, Content} from 'native-base';
import {FuncCommon} from '../../../utils';
import TabBar_Title from '../../component/TabBar_Title';
import RequiredText from '../../component/RequiredText';
import {AppStyles, AppColors} from '@theme';
import Combobox from '../../component/Combobox';
import Toast from 'react-native-simple-toast';

class CheckRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.DataProcess = FuncCommon.DataProcess();
    this.state = {
      model: {},
      refreshing: false,
      ListReasons: [],
      itemName: '',
      selected: undefined,
      QuantityNG: null,
      ChoDuyet: null,
      selectedTab: 'profile',
      dropdownName: '',
    };
  }
  componentDidMount() {
    let id = this.props.RecordGuid;
    API_CheckRequests.GetCheck(id)
      .then(res => {
        this.setState({model: JSON.parse(res.data.data)});
        this.setState({QuantityNG: this.state.model.QuantityNG});
        this.setState({refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
    API_CheckRequests.GetNGReasons()
      .then(rs => {
        var _listReasons = rs.data;
        let _reListReasons = [
          {
            text: 'Bỏ chọn',
            value: null,
          },
        ];
        for (var i = 0; i < _listReasons.length; i++) {
          _reListReasons.push({
            text: _listReasons[i].Title,
            value: _listReasons[i].NgreasonId,
          });
        }
        this.setState({
          ListReasons: _reListReasons,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
  onValueChange = value => {
    this.setState({
      dropdownName: value.text,
      selected: value.value,
    });
  };
  render() {
    return (
      <Container>
        <TouchableWithoutFeedback
          style={{flex: 30}}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={{flex: 40}}>
            <TabBar_Title
              title={this.props.ItemName}
              callBack={() => this.onPressBack()}
            />
            <View style={styles.containerContent}>
              <Content padder>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Mã đơn hàng</Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.props.ItemId}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Số lượng yêu cầu</Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    value={this.addPeriod(this.state.model.Quantity)}
                    onChangeText={Quantity => this.setQuantity(Quantity)}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Số lượng OK</Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    value={this.addPeriod(this.state.model.QuantityOK)}
                    onChangeText={QuantityOK => this.setQuantityOK(QuantityOK)}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>
                    Số lượng NG <RequiredText />
                  </Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    value={this.addPeriod(this.state.model.QuantityNG)}
                    onChangeText={QuantityNG => this.setQuantityNG(QuantityNG)}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Lý do NG</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput, {flexDirection: 'row'}]}
                    onPress={() => this.onActionCombobox()}>
                    <Text
                      style={[
                        AppStyles.TextInput,
                        this.state.dropdownName == ''
                          ? {color: AppColors.gray}
                          : null,
                      ]}>
                      {this.state.dropdownName !== ''
                        ? this.state.dropdownName
                        : 'Chọn kho...'}
                    </Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Icon
                        color="#888888"
                        name={'chevron-thin-down'}
                        type="entypo"
                        size={20}
                      />
                    </View>
                  </TouchableOpacity>
                  {/* <RNPickerSelect
                        doneText="Xong"
                                        onValueChange={(value) => this.onValueChange(value)}
                                        items={this.state.ListReasons}
                                        placeholder={{}}
                                        doneText={'Chọn'}
                                    /> */}
                </View>
              </Content>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Xác nhận"
            onPress={() => this.onSubmit()}
          />
        </View>
        {this.state.ListReasons.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.onValueChange}
            data={this.state.ListReasons}
            nameMenu={'Chọn lý do'}
            eOpen={this.openCombobox}
            position={'bottom'}
          />
        ) : null}
      </Container>
    );
  }
  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCombobox() {
    this._openCombobox();
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Check', ActionTime: new Date().getTime()});
  }
  setQuantity(Quantity) {
    this.state.model.Quantity = Quantity;
    this.setState({model: this.state.model});
  }
  setQuantityOK(QuantityOK) {
    this.state.model.QuantityOK = QuantityOK;
    this.setState({model: this.state.model});
  }
  setQuantityNG(QuantityNG) {
    this.state.model.QuantityNG = QuantityNG;
    this.setState({model: this.state.model});
    this.state.model.QuantityOK = parseFloat(
      this.state.model.Quantity - this.state.model.QuantityNG,
    );
  }
  onSubmit() {
    if (this.state.model.Quantity < this.state.model.QuantityNG) {
      Alert.alert(
        'Thông báo',
        'Số lượng NG không được lớn hơn số lượng đề nghị',
        [{text: 'Đóng'}],
        {cancelable: true},
      );
      return;
    }
    if (!this.state.model.QuantityNG) {
      Alert.alert('Thông báo', 'Đề nghị nhập số lượng NG', [{text: 'Đóng'}], {
        cancelable: true,
      });
      return;
    }
    this.state.model.NgreasonId = this.state.selected;
    API_CheckRequests.Check(this.state.model)
      .then(res => {
        if (res.data.errorCode === 200) {
          Toast.showWithGravity('Xử lý thành công', Toast.SHORT, Toast.CENTER);
          Keyboard.dismiss();
          Actions.pop();
          Actions.refresh({
            moduleId: 'Check',
            ActionTime: new Date().getTime(),
          });
        } else {
          Toast.showWithGravity('Có lỗi xảy ra', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckRequestsComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  buttonContainer: {
    margin: 10,
    paddingTop: 20,
  },
});
