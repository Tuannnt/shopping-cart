import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Icon, ListItem, SearchBar} from 'react-native-elements';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import API_CheckRequests from '../../../network/BM/API_CheckRequests';
import {AppStyles, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import Combobox from '../../component/Combobox';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '../../../utils';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import {Alert} from 'react-native';
import {Keyboard} from 'react-native';

class ListCheckRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      model: {},
      list: [],
      isFetching: false,
      itemName: '',
      visible: false,
      model: {},
      ListReasons: [
        {
          Title: 'Chọn',
          NgreasonId: '',
        },
      ],
      ListDepartment: [],
      selected: undefined,
      QuantityNG: null,
      openSearch: false,
      refreshing: false,
      isApproved: false,
      dTypeOfRequest: [
        {
          value: null,
          text: 'Tất cả',
        },
        {
          value: 1,
          text: 'Thương mại',
        },
        {
          value: 2,
          text: 'Sản xuất',
        },
        {
          value: 3,
          text: 'Gia công ngoài',
        },
        {
          value: 6,
          text: 'Dịch vụ - GCN',
        },
      ],
    };
    this.staticParam = {
      CurrentPage: 1,
      Length: 15,
      Search: '',
      QueryOrderBy: 'PurchaseOrderId desc',
      StartTime: new Date(),
      EndTime: new Date(),
      Department: '',
      IsQCComfirm: 0,
      Potype: null,
      Department: global.__appSIGNALR.SIGNALR_object.USER.DepartmentId,
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ kiểm tra',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã kiểm tra',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
    this.Total = 0;
  }

  GetAll = () => {
    this.setState({refreshing: true});
    API_CheckRequests.ListAll(this.staticParam)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        if (!_data) {
          this.setState({refreshing: false});
          return;
        }
        let _list = _data.data;
        if (this.staticParam.CurrentPage === 1) {
          this.state.list = _list;
        } else {
          this.state.list = this.state.list.concat(_list);
        }
        this.Total = _data.recordsTotal || 0;
        if (this.staticParam.IsQCComfirm === 0) {
          this.listtabbarBotom[0].Badge = _data.recordsTotal;
        }
        this.setState({list: this.state.list, refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };

  componentDidMount(): void {
    this.GetAll();
    this.GetPermission();
    this.GetAllDepartments();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Check') {
      this.updateSearch('');
    }
  }
  GetPermission = () => {
    API_CheckRequests.GetPermission()
      .then(rs => {
        let _rs = JSON.parse(rs.data.data) || [];
        let isApproved = _rs.includes('FULLCONTROL');
        this.setState({
          isApproved,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  GetAllDepartments = () => {
    API_CheckRequests.GetAllDepartments()
      .then(rs => {
        let _rs = JSON.parse(rs.data.data) || [];
        this.setState({
          ListDepartment: _rs,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  updateSearch = text => {
    this.staticParam.Search = text;
    this.staticParam.CurrentPage = 1;
    this.setState({
      list: [],
    });
    this.GetAll();
  };
  loadMoreData() {
    if (this.Total > this.state.list.length) {
      this.nextPage();
    }
  }
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Yêu cầu kiểm tra'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'BM'}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  padding: 5,
                }}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian bắt đầu
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventStartTime: true})}>
                  <Text>
                    {FuncCommon.ConDate(this.staticParam.StartTime, 0)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian kết thúc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventEndTime: true})}>
                  <Text>{FuncCommon.ConDate(this.staticParam.EndTime, 0)}</Text>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', padding: 5}}>
                <Text
                  style={[
                    AppStyles.Labeldefault,
                    {textAlign: 'center'},
                    styles.timeHeader,
                  ]}>
                  Lọc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionSearchDate()}>
                  <Icon
                    name={'down'}
                    type={'antdesign'}
                    size={18}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              style={[AppStyles.FormInput, {marginHorizontal: 10}]}
              onPress={() => this.onActionComboboxType()}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.staticParam.TypeName
                    ? {color: 'black'}
                    : {color: AppColors.gray},
                ]}>
                {this.staticParam.TypeName
                  ? this.staticParam.TypeName
                  : 'Chọn loại đơn đặt hàng...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  color={AppColors.gray}
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                AppStyles.FormInput,
                {marginHorizontal: 10, marginTop: 5},
              ]}
              onPress={() => {
                if (!this.state.isApproved) {
                  return;
                }
                this.onActionComboboxDepartment();
              }}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.staticParam.DepartmentName && this.state.isApproved
                    ? {color: 'black'}
                    : {color: AppColors.gray},
                ]}>
                {this.staticParam.DepartmentName
                  ? this.staticParam.DepartmentName
                  : 'Chọn phòng ban...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  color={AppColors.gray}
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                />
              </View>
            </TouchableOpacity>
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.updateSearch(text)}
              value={this.staticParam.Search}
            />
          </View>
        ) : null}
        <View style={{flex: 9}}>
          <FlatList
            style={{height: '100%'}}
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListFooterComponent={this.renderFooter}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.list}
            renderItem={({item, index}) => (
              <View style={{}}>
                <ListItem
                  title={() => (
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                      }}>
                      <View style={{flex: 3}}>
                        <Text style={AppStyles.Labeldefault}>
                          {item.ItemName}
                        </Text>
                      </View>
                      <View style={{flex: 2}}>
                        {item.IsQCComfirm == 1 ? (
                          <Text
                            style={[
                              {
                                justifyContent: 'flex-end',
                                alignItems: 'flex-end',
                                textAlign: 'right',
                                color: AppColors.AcceptColor,
                              },
                              AppStyles.Textdefault,
                            ]}>
                            {'Đã kiểm tra'}
                          </Text>
                        ) : (
                          <Text
                            style={[
                              {
                                justifyContent: 'flex-end',
                                alignItems: 'flex-end',
                                textAlign: 'right',
                                color: AppColors.PendingColor,
                              },
                              AppStyles.Textdefault,
                            ]}>
                            {'Chờ kiểm tra'}
                          </Text>
                        )}
                        <Text
                          style={[AppStyles.Textdefault, {textAlign: 'right'}]}>
                          {this.customDate(item.CreatedDate)}
                        </Text>
                      </View>
                    </View>
                  )}
                  titleStyle={AppStyles.Titledefault}
                  subtitle={() => {
                    return (
                      <View style={{flexDirection: 'row'}}>
                        <View
                          style={{
                            flex: 3,
                            flexDirection: 'row',
                          }}>
                          <View style={{flex: 5}}>
                            <Text style={AppStyles.Textdefault}>
                              Mã: {item.ItemId}
                            </Text>
                            {/* <Text style={AppStyles.Textdefault}>
                              YC: {this.addPeriod(item.Quantity)}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              OK: {this.addPeriod(item.QuantityOK)}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              NG: {this.addPeriod(item.QuantityNG)}
                            </Text> */}
                          </View>
                        </View>

                        <View
                          style={{
                            flex: 2,
                            alignItems: 'flex-end',
                            justifyContent: 'flex-start',
                          }}
                        />
                      </View>
                    );
                  }}
                  bottomDivider
                  // onPress={() => {
                  //   if (item.IsQCComfirm == 1) {
                  //     Alert.alert(
                  //       'Thông báo',
                  //       'Đã kiểm tra',
                  //       [{text: 'OK', onPress: () => Keyboard.dismiss()}],
                  //       {cancelable: false},
                  //     );
                  //     return;
                  //   }
                  //   this.openCheck(item.RowGuid, item.ItemName, item.ItemId);
                  // }}
                  onPress={() => {
                    Actions.OpenCheckRequests({Data: item});
                  }}
                />
              </View>
            )}
            onEndReachedThreshold={0.5}
            onEndReached={() => this.loadMoreData()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback => this.onClick(callback)}
            />
          </View>
          {this.state.setEventStartTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventStartTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.StartTime}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartTime(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventEndTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.EndTime}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndTime(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {this.state.dTypeOfRequest.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeType}
              data={this.state.dTypeOfRequest}
              nameMenu={'Chọn loại đơn đặt hàng'}
              eOpen={this.openComboboxType}
              position={'bottom'}
              value={this.staticParam.Potype}
            />
          ) : null}
          {this.state.ListDepartment.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeDepartment}
              data={this.state.ListDepartment}
              nameMenu={'Chọn phòng ban'}
              eOpen={this.openComboboxDepartment}
              position={'bottom'}
              value={this.staticParam.Department}
            />
          ) : null}
        </View>
      </View>
    );
  }

  nextPage() {
    this.staticParam.CurrentPage++;
    this.staticParam = {
      CurrentPage: this.staticParam.CurrentPage,
      Length: this.staticParam.Length,
      Search: this.staticParam.Search,
      QueryOrderBy: this.staticParam.QueryOrderBy,
      StartTime: this.staticParam.StartTime,
      EndTime: this.staticParam.EndTime,
      IsQCComfirm: this.staticParam.IsQCComfirm,
    };
    this.GetAll();
  }
  ChangeType = rs => {
    if (rs.value === undefined) {
      return;
    }
    this.staticParam.Potype = rs.value;
    this.staticParam.TypeName = rs.text;
    this.updateSearch('');
  };
  ChangeDepartment = rs => {
    if (rs.value === undefined) {
      return;
    }
    this.staticParam.Department = rs.value;
    this.staticParam.DepartmentName = rs.text;
    this.updateSearch('');
  };
  //Load lại
  onRefresh = () => {
    this.setState({refreshing: true});
    this.GetAll();
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  _openComboboxDepartment() {}
  openComboboxDepartment = d => {
    this._openComboboxDepartment = d;
  };
  onActionComboboxDepartment() {
    this._openComboboxDepartment();
  }
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartTime = new Date(callback.start);
      this.staticParam.EndTime = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  setStartTime = date => {
    this.staticParam.StartTime = date;
    this.updateSearch('');
  };
  setEndTime = date => {
    this.staticParam.EndTime = date;
    this.updateSearch('');
  };
  onClick(data) {
    if (data == 'checkcircleo') {
      this.state.list = [];
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.Search = '';
      this.staticParam.IsQCComfirm = 1;
      this.GetAll();
    } else if (data == 'profile') {
      this.state.list = [];
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.Search = '';
      this.staticParam.IsQCComfirm = 0;
      this.GetAll();
    }
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  openCheck(id, itemName, itemId) {
    Actions.bmCheckRequests({
      RecordGuid: id,
      ItemName: itemName,
      ItemId: itemId,
    });
  }
}

const styles = StyleSheet.create({
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListCheckRequestsComponent);
