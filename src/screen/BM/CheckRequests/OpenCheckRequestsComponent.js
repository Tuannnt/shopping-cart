import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  Keyboard,
  Alert,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import {AppStyles, AppColors} from '@theme';
import {Actions} from 'react-native-router-flux';
import {API_ListProposePurchases} from '@network';
import {ListItem, Thumbnail, Text, Left, Body, Picker} from 'native-base';
import {connect} from 'react-redux';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import _ from 'lodash';
import {FuncCommon} from '../../../utils';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import RNPickerSelect from 'react-native-picker-select';
import Dialog from 'react-native-dialog';
import RequiredText from '../../component/RequiredText';
import {PlatformColor} from 'react-native';

const Dropdown = [
  {
    value: 'NO',
    label: 'Nghỉ ốm, chữa bệnh, nghỉ dưỡng thai',
  },
  {
    value: 'TN',
    label: 'Nghỉ tai nạn',
  },
  {
    value: 'KL',
    label: 'Nghỉ không lương',
  },
  {
    value: 'NP',
    label: 'Nghỉ phép',
  },
  {
    value: 'VR',
    label: 'Nghỉ việc riêng có lương',
  },
];

const pickerSelectStyles = {
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    color: 'black',
    fontSize: 10,
  },
};
class ItemProposedPurchasesComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {};
  }
  onPressback() {
    Keyboard.dismiss();
    Actions.pop();

    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }

  getStatus = value => {
    if (!value) {
      return '';
    }
    let res = Dropdown.findIndex(i => i.value == value);
    if (res === -1) {
      return '';
    }
    return Dropdown[res].label;
  };
  render() {
    return (
      <View style={{backgroundColor: '#fff', flex: 1}}>
        <TabBar_Title
          title={'Chi tiết YC kiểm tra'}
          callBack={() => this.onPressback()}
        />
        <ScrollView>
          <View style={{flexDirection: 'column', flex: 1}}>
            {/* Hình ảnh chân dung và tên */}
            <View style={{padding: 10}}>
              {/*<Text style={{fontWeight:'bold',paddingBottom:3,paddingTop:5}}>Thông tin chung</Text>*/}
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Mã đơn hàng:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        fontWeight: 'bold',
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.PurchaseOrderId}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Số ĐNN:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.TicketRequestNo}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Đối tượng:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.ObjectName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Mã sản phẩm:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.ItemId}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Tên sản phẩm:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.ItemName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>
                    Tiêu chuẩn kỹ thuật:
                  </Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.Note}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Đơn vị tính:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.UnitName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Kho:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.WarehouseName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>SL định mức:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.InventoryQuotaMin || '0'}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Số lượng:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.Quantity}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>
                    Số lượng đạt tiêu chuẩn:
                  </Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.QuantityOK}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>
                    Số lượng không đạt tiêu chuẩn:
                  </Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {this.props.Data.QuantityNG || '0'}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Labeldefault]}>Đơn giá:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        textAlign: 'left',
                        paddingLeft: 10,
                      },
                    ]}>
                    {FuncCommon.addPeriod(this.props.Data.UnitPriceBefore)}
                  </Text>
                </View>
              </View>

              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                </View>
                <View style={{flex: 2}}>
                  {this.props.Data.IsQCComfirm == '1' ? (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.AcceptColor},
                        {textAlign: 'left', paddingLeft: 10},
                      ]}>
                      {'Đã kiểm tra'}
                    </Text>
                  ) : (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.PendingColor},
                        {textAlign: 'left', paddingLeft: 10},
                      ]}>
                      {this.props.Data.IsQCComfirm == '0' ? 'Chờ kiểm tra' : ''}
                    </Text>
                  )}
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ItemProposedPurchasesComponent);
