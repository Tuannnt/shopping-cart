import MainComponent from './core/MainComponent';
import LoginComponent from './account/LoginComponent';
import SplashComponent from './account/SplashComponent';
import SignupComponent from './account/SignupComponent';
import DrawerComponent from './main/DrawerComponent';
import HomeComponent from './main/HomeComponent';
import FakeHome from './fakeHome/FakeHome';
import Notification_Detail_Components from './main/Notification_Detail_Components';
import Edit_PassWord_Component from './main/Edit_PassWord_Component';
import Dialog from './lightbox/Dialog';
//CommentWF
import CommentWFComponent from './component/CommentWFComponent';
import HrComponent from './hr/HrComponent';
import EmployeesComponent from './hr/Employees/EmployeesComponent';
import EmpViewItemComponent from './hr/Employees/EmpViewItemComponent';
import openContracts from './hr/Employees/openContracts';
import openInsurances from './hr/Employees/openInsurances';
import openSalaries from './hr/Employees/openSalaries';
//TinhLV
import CheckinHistoryComponent from './hr/CheckinHistory/CheckinHistoryComponent';
import indexComponent from './hr/registerCars/indexComponent';
import indexChartDashboard from './hr/ChartDashboard/indexChartDashboard';
import addComponent from './hr/registerCars/addComponent';
import openComponent from './hr/registerCars/openComponent';
import indexCompensationSlipComponent from './hr/CompensationSlip/indexComponent';
import addCompensationSlipComponent from './hr/CompensationSlip/addComponent';
import openCompensationSlipComponent from './hr/CompensationSlip/openComponent';
import addAdvancesAM from './AM/Advances/addAdvances';
import indexAdvancesAM from './AM/Advances/indexAdvances';
import openAdvancesAM from './AM/Advances/openAdvances';
import addAdvances from './hr/Advances/addAdvances';
import indexAdvances from './hr/Advances/indexAdvances';
import openAdvances from './hr/Advances/openAdvances';
import indexCandidates from './hr/Candidates/indexCandidates';
import openCandidates from './hr/Candidates/openCandidates';
import indexTrainingPlans from './hr/TrainingPlans/indexTrainingPlans';
import addTrainingPlans from './hr/TrainingPlans/addTrainingPlans';
import openTrainingPlans from './hr/TrainingPlans/openTrainingPlans';
import openTrainingPlanDetails from './hr/TrainingPlans/openTrainingPlanDetails';
import indexQuotations from './SM/Quotations/indexQuotations';
import openQuotations from './SM/Quotations/openQuotations';
import openQuotationDetails from './SM/Quotations/openQuotationDetails';
import indexDashboard from './SM/ChartDashboard_SM/indexDashboard';
import ConfirmPayment from './SM/ConfirmPayments/ConfirmComponent';
import ConfirmPayments from './SM/ConfirmPayments/ConfirmPaymentsComponent';
import Quotations_Form_Component from './SM/Quotations/Quotations_Form_Component';
import InvoiceOrdersComponent from './SM/InvoiceOrders/InvoiceOrdersComponent';
import GetInvoiceOrdersComponent from './SM/InvoiceOrders/GetInvoiceOrdersComponent';
import OpenApplyOutsideDetails from './hr/ApplyOutsides/OpenApplyOutsideDetails';
import ListProjects from './PM/Projects/ListProjects';
import OpenProject from './PM/Projects/OpenProject';
import ListProjectItems from './PM/ProjectItems/ListProjectItems';
import OpenProjectItems from './PM/ProjectItems/OpenProjectItems';
import openHistories from './hr/Employees/openHistories';
import openRewards from './hr/Employees/openRewards';
import openCertificates from './hr/Employees/openCertificates';
//begin using for tm
// import TasksComponent from './TM/Tasks/TasksComponent';
// import TasksHomeComponent from './TM/Tasks/TasksHomeComponent';
// import TasksDetailComponent from './TM/Tasks/TasksDetailComponent';
// import TasksAddChildComponent from './TM/Tasks/TasksAddChildComponent';
// import TasksAttachmentComponent from './TM/Tasks/TasksAttachmentComponent';
// import TasksCommentComponent from './TM/Tasks/TasksCommentComponent';
// import TasksForwardComponent from './TM/Tasks/TasksForwardComponent';
// import TasksProcessComponent from './TM/Tasks/TasksProcessComponent';
//end using for tm
//begin using for orders
import ListOrdersComponent from './SM/Orders/ListOrdersComponent';
import ViewOrderComponent from './SM/Orders/ViewOrderComponent';
import ListOrderDetailsComponent from './SM/Orders/ListOrderDetailsComponent';
import OrdersAttachmentComponent from './SM/Orders/OrdersAttachmentComponent';
import OrderDetailsSXComponent from './SM/OrderDetailsSX/OrderDetailsSXComponent';
import OpenOrderDetailsSXComponent from './SM/OrderDetailsSX/OpenOrderDetailsSXComponent';
//end using for orders
//Thanhtt@esvn.com.vn
import WorkShiftChangeComponent from './hr/WorkShiftChange/WorkShiftChangeComponent';
import AddWorkShiftChangeComponent from './hr/WorkShiftChange/AddWorkShiftChangeComponent';
import GetWorkShiftChangeComponent from './hr/WorkShiftChange/GetWorkShiftChangeComponent';
import QuotationRequestComponent from './SM/QuotationRequest/QuotationRequestComponent';
import AddQuotationRequestComponent from './SM/QuotationRequest/AddQuotationRequestComponent';
import GetQuotationRequestComponent from './SM/QuotationRequest/GetQuotationRequestComponent';
import QuotationRequestProComponent from './PRO/QuotationRequest/QuotationRequestComponent';
import AddQuotationRequestProComponent from './PRO/QuotationRequest/AddQuotationRequestComponent';
import GetQuotationRequestProComponent from './PRO/QuotationRequest/GetQuotationRequestComponent';
import RegisterEatsComponent from './hr/RegisterEats/RegisterEatsComponent';
import AddEatsComponent from './hr/RegisterEats/AddEatsComponent';
import GetEatComponent from './hr/RegisterEats/GetEatComponent';
import ResignationFormsComponent from './hr/ResignationForms/ResignationFormsComponent';
import GetResignationForms from './hr/ResignationForms/GetResignationForms';
import AddResignationForms from './hr/ResignationForms/AddResignationForms';
import GetTrainingResults from './hr/TrainingResults/GetTrainingResults';
import TrainingResultsComponent from './hr/TrainingResults/TrainingResultsComponent';
import GetRecruitmentRequests from './hr/RecruitmentRequests/GetRecruitmentRequests';
import RecruitmentRequestsComponent from './hr/RecruitmentRequests/RecruitmentRequestsComponent';
import RecruitmentPlansComponent from './hr/RecruitmentPlans/RecruitmentPlansComponent';
import GetRecruitmentPlans from './hr/RecruitmentPlans/GetRecruitmentPlans';
import RecruitmentSchedulesComponent from './hr/RecruitmentSchedules/RecruitmentSchedulesComponent';
import GetRecruitmentSchedules from './hr/RecruitmentSchedules/GetRecruitmentSchedules';
import ViewSalaryComponent from './hr/ViewSalary/ViewSalaryComponent';
import GetViewSalary from './hr/ViewSalary/GetViewSalary';
import ChartDashboardAMComponent from './AM/ChartDashboardAM/ChartDashboardAMComponent';
import UniformsComponent from './hr/Uniforms/UniformsComponent';
import GetUniforms from './hr/Uniforms/GetUniforms';
import ViewInsurancesComponent from './hr/ViewInsurances/ViewInsurancesComponent';
import GetInsurances from './hr/ViewInsurances/GetInsurances';
import RewardsComponent from './hr/Rewards/RewardsComponent';
import GetRewards from './hr/Rewards/GetRewards';
import AddEmployeeCheckIn from './hr/TimeKeepingsConfirm/AddEmployeeCheckIn';
import TimeKeepingsConfirmComponent from './hr/TimeKeepingsConfirm/TimeKeepingsConfirmComponent';
import GetTimeKeepingsConfirm from './hr/TimeKeepingsConfirm/GetTimeKeepingsConfirm';
import timeKeepingsComfirm_Add from './hr/TimeKeepingsConfirm/TimeKeepingsComfirm_Add_Component';
import ProfileComponent from './hr/Profile/ProfileComponent';
import ViewSalaryByMeComponent from './hr/ViewSalaryByMe/ViewSalaryByMeComponent';
import UniformsByMeComponent from './hr/UniformsByMe/UniformsByMeComponent';
import InsurancesByMeComponent from './hr/InsurancesByMe/InsurancesByMeComponent';
import TimeKeepingsByMeComponent from './hr/TimeKeepingsByMe/TimeKeepingsByMeComponent';
import ApplyOverTimesByMeComponent from './hr/ApplyOverTimesByMe/ApplyOverTimesByMeComponent';
import ApplyOverTimesItemByMe_Component from './hr/ApplyOverTimesByMe/ApplyOverTimesItemByMe_Component';
import ApplyLeavesByMeComponent from './hr/ApplyLeavesByMe/ApplyLeavesByMeComponent';
import ApplyOutsidesByMeComponent from './hr/ApplyOutsidesByMe/ApplyOutsidesByMeComponent';
import ApplyOutsidesItemByMe_Component from './hr/ApplyOutsidesByMe/ApplyOutsidesItemByMe_Component';
import index_ToDoByMe_Component from './hr/ToDoByMe/index_ToDoByMe_Component';
import MeetingCalendarsComponent from './hr/MeetingCalendars/MeetingCalendarsComponent';
import GetMeetingCalendars from './hr/MeetingCalendars/GetMeetingCalendars';
import AddMeetingCalendars from './hr/MeetingCalendars/AddMeetingCalendars';
import GetRewardsByMe from './hr/RewardsByMe/GetRewardsByMe';
import DisciplineByMeComponent from './hr/DisciplineByMe/DisciplineByMeComponent';
import MaintenanceRequestComponent from './PRO/MaintenanceRequest/MaintenanceRequestComponent';
import GetMaintenanceRequest from './PRO/MaintenanceRequest/GetMaintenanceRequest';
import openMaintenanceManagement from './PRO/MaintenanceRequest/openMaintenanceManagement';
//#region trongtq@esvn.com.vn
//#region AM
import View_VouChers_Component from './AM/VouChers/View_VouChers_Component';
import Open_VouChers_Component from './AM/VouChers/Open_VouChers_Component';
import GetAllPaging_Component from './AM/VouchersAM/GetAllPaging_Component';
import VoucherAM_ViewItem_Component from './AM/VouchersAM/VoucherAM_ViewItem_Component';
import VoucherAM_ViewDetail_Component from './AM/VouchersAM/VoucherAM_ViewDetail_Component';
import Transfer_ListAll_Component from './AM/Transfer/Transfer_ListAll_Component';
import Transfer_ViewItem_Component from './AM/Transfer/Transfer_ViewItem_Component';

import ListPOPaymentsBeforeComponent from './AM/POPaymentsBefore/POPaymentsBeforeComponent';
import ItemPOPaymentsBeforeComponent from './AM/POPaymentsBefore/GetPOPaymentsBeforeComponent';
import ListPOPaymentsAfterComponent from './AM/POPaymentsAfter/POPaymentsAfterComponent';
import ItemPOPaymentsAfterComponent from './AM/POPaymentsAfter/GetPOPaymentsAfterComponent';
import ListRequirePaymentsComponent from './AM/RequirePayments/ListRequirePaymentsComponent';
import ViewRequirePaymentsComponent from './AM/RequirePayments/ViewRequirePaymentsComponent';
//#endregion
//#region HR
import ApplyOutsidesListAll_Compoment from './hr/ApplyOutsides/ApplyOutsidesListAll_Compoment';
import ApplyOutsidesItem_Component from './hr/ApplyOutsides/ApplyOutsidesItem_Component';
import ApplyOutsidesAdd_Component from './hr/ApplyOutsides/ApplyOutsidesAdd_Component';
import ApplyOverTimesListAll_Component from './hr/ApplyOverTimes/ApplyOverTimesListAll_Component';
import ApplyOverTimesItem_Component from './hr/ApplyOverTimes/ApplyOverTimesItem_Component';
import ApplyOverTimesAdd_Component from './hr/ApplyOverTimes/ApplyOverTimesAdd_Component';
//#endregion
//#region MS
import ListAll_Messange_Component from './MS/ListAll_Messange_Component';
import ViewItem_Messange_Component from './MS/ViewItem_Messange_Component';
import AddGroup_Component from './MS/AddGroup_Component';
import Information_Component from './MS/Information_Component';
import ViewAllGroupByLoginName_Compoment from './MS/ViewAllGroupByLoginName_Compoment';
import ViewDetailAllUser_Component from './MS/ViewDetailAllUser_Component';
import AddUserOfGroup_Component from './MS/AddUserOfGroup_Component';
import ViewAllImage_Component from './MS/ViewAllImage_Component';
import TestSound_Component from './MS/TestSound_Component';

//#endregion
//#region Task - calendars
import Task_ListAll_Component from './TM/Task/Task_ListAll_Component';
import Task_Add_Component from './TM/Task/Task_Form_Component';
import Task_Detail_Component from './TM/Task/Task_Detail_Component';
import Task_Comment_Component from './TM/Task/Task_Comment_Component';
import Task_UpdateProcess_Component from './TM/Task/Task_UpdateProcess_Component';
import Calendar_Detail_Component from './TM/Calendars/Calendar_Detail_Component';
import Calendar_Add_Component from './TM/Calendars/Calendar_Add_Component';
import ListCalendars_Component from './TM/Calendars/ListCalendars_Component';
import CheckInComponent from './TM/Timekeeping/CheckInComponent';
//#endregion
//#region Email
import Mail_index_Component from './TM/Mail/Mail_index_Component';
import Mail_Message_Component from './TM/Mail/Mail_Message_Component';
import Mail_open_Component from './TM/Mail/Mail_open_Component';
//#endregion
//#region Todo
import index_ToDo_Component from './TM/To_Do/index_ToDo_Component';
import MyDay_ToDo_Component from './TM/To_Do/MyDay_ToDo_Component';
import open_ToDo_Component from './TM/To_Do/open_ToDo_Component';
import ListData_ToDo_Component from './TM/To_Do/ListData_ToDo_Component';
//#endregion
//#region Component
import QRCode from './component/QRCode';
import QRCodeV2 from './component/QRCodeV2';
//#endregion
//#region PRO
import PlanTotal_List_Component from './PRO/PlanTotal/PlanTotal_List_Component';
import PlanTotal_Open_Component from './PRO/PlanTotal/PlanTotal_Open_Component';
import ProductionPlanDay_List_Component from './PRO/ProductionPlanDay/ProductionPlanDay_List_Component';
import ProductionPlanDay_Open_Component from './PRO/ProductionPlanDay/ProductionPlanDay_Open_Component';
import ProductionPlanDayUpdateComponent from './PRO/ProductionPlanDay/ProductionPlanDayUpdateComponent';
import BomCategories_List_Component from './PRO/BOMCategories/BomCategories_List_Component';
import BomCategories_Open_Component from './PRO/BOMCategories/BomCategories_Open_Component';
import ListProposedPurchasesProComponent from './PRO/ProposedPurchases/ListProposedPurchasesComponent';
import ItemProposedPurchasesProComponent from './PRO/ProposedPurchases/ItemProposedPurchasesComponent';
import ListPuschaseOderProgress2Component from './PRO/PuschaseOderProgress2/ListPuschaseOderProgress2Component';
import ItemPuschaseOderProgress2Component from './PRO/PuschaseOderProgress2/ItemPuschaseOderProgress2Component';
import ProductionPlan_List_Component from './PRO/ProductionPlan/ProductionPlan_List_Component';
import ProductionPlan_Open_Component from './PRO/ProductionPlan/ProductionPlan_Open_Component';
import ProductionPlanCNC_List_Component from './PRO/ProductionPlanCNC/ProductionPlanCNC_List_Component';
import ProductionPlanCNC_Open_Component from './PRO/ProductionPlanCNC/ProductionPlanCNC_Open_Component';
import Operator_index_Component from './PRO/Operator/Operator_index_Component';
import Operator_Employee_Component from './PRO/Operator/Employee/Operator_Employee_Component';
import Operator_MachinesTools_Component from './PRO/Operator/MachinesTools/Operator_MachinesTools_Component';
import Operator_UpdateMachidnes_Component from './PRO/Operator/MachinesTools/Operator_UpdateMachidnes_Component';
import Operator_Supplies_Component from './PRO/Operator/Supplies/Operator_Supplies_Component';
import Operator_indexSupplies_Component from './PRO/Operator/Supplies/Operator_indexSupplies_Component';
import Operator_Document_Component from './PRO/Operator/Documents/Operator_Document_Component';
import Operator_indexDocument_Component from './PRO/Operator/Documents/Operator_indexDocument_Component';
import Operator_Print_Component from './PRO/Operator/PrintTasks/Operator_Print_Component';
import Operator_ItemPrint_Component from './PRO/Operator/PrintTasks/Operator_ItemPrint_Component';
import Operator_ItemJobCard_Component from './PRO/Operator/Report/Operator_ItemJobCard_Component';
import Operator_QCCheck_Component from './PRO/Operator/QC/Operator_QCCheck_Component';
import Operator_Warehouses_Component from './PRO/Operator/Warehouses/Operator_Warehouses_Component';
import Operator_addOverTime_Component from './PRO/Operator/Employee/Operator_addOverTime_Component';
import Operator_addEmployee_Component from './PRO/Operator/Employee/Operator_addEmployee_Component';
import Operator_sendBCEmployee_Component from './PRO/Operator/Employee/Operator_sendBCEmployee_Component';
import Operator_EditTools_Component from './PRO/Operator/MachinesTools/Operator_EditTools_Component';
import Operator_AlertMachines_Component from './PRO/Operator/MachinesTools/Operator_AlertMachines_Component';
import Operator_addReportPlan_Component from './PRO/Operator/Report/Operator_addReportPlan_Component';
import Operator_ListPlanJobCards_Component from './PRO/Operator/QC/Operator_ListPlanJobCards_Component';
import Operator_StopProduction_Component from './PRO/Operator/QC/Operator_StopProduction_Component';
import Operator_addTicketRequests_Component from './PRO/Operator/Warehouses/Operator_addTicketRequests_Component';
import Operator_addTicketRequestsExport_Component from './PRO/Operator/Supplies/Operator_addTicketRequestsExport_Component';
import Operator_addMaterialErrors_Component from './PRO/Operator/Supplies/Operator_addMaterialErrors_Component';
import Operator_addTicketRequestsExportVT_Component from './PRO/Operator/Supplies/Operator_addTicketRequestsExportVT_Component';
import ListOrderProgessComponent from './PRO/OrderProgress/ListOrderProgessComponent';
import ItemOrderProgessComponent from './PRO/OrderProgress/ItemOrderProgressComponent';
import QCCheckComponent from './PRO/QCCheck/QCCheckComponent';
//#endregion
//#endregion\
import ApplyLeavesComponent from './hr/ApplyLeaves/ApplyLeavesComponent';
import ApplyLeaveViewItemComponent from './hr/ApplyLeaves/ApplyLeaveViewItemComponent';
import ViewBottonHrComponent from './hr/ApplyLeaves/ViewBottonHrComponent';
import ApplyLeavesAddComponent from './hr/ApplyLeaves/ApplyLeavesAddComponent';
import TimeKeepingsComponent from './hr/TimeKeepings/TimeKeepingsComponent';
import TimeKeepingbyMonthComponent from './hr/TimeKeepings/TimeKeepingbyMonthComponent';
// tiecnq
import ProductionOrdersComponent from './PRO/ProductionOrders/ProductionOrdersComponent';
import ProductionOrders_Open_Component from './PRO/ProductionOrders/ProductionOrders_Open_Component';
//begin using for sontd-BM
import ListProposedPurchaseComponent from './BM/ListProposedPurchases/ListProposedPurchasesComponent';
import ItemProposedPurchaseComponent from './BM/ListProposedPurchases/ItemProposedPurchasesComponent';
import ListProposedPurchasesComponent from './BM/ProposedPurchases/ListProposedPurchasesComponent';
import ItemProposedPurchasesComponent from './BM/ProposedPurchases/ItemProposedPurchasesComponent';
import ItemProposedPurchaseDetailsComponent from './BM/ProposedPurchases/ItemProposedPurchaseDetailsComponent';
import AddProposedPurchase from './BM/ProposedPurchases/AddProposedPurchase';
import ListPurchaseOrdersComponent from './BM/PurchaseOrders/ListPurchaseOrdersComponent';
import ItemPurchaseOrdersComponent from './BM/PurchaseOrders/ItemPurchaseOrdersComponent';
import ItemPurchaseOrderDetailsComponent from './BM/PurchaseOrders/ItemPurchaseOrderDetailsComponent';
import CheckComponent from './BM/PurchaseOrders/CheckComponent';
import ListPPCancelRequestsComponent from './BM/PPCancelRequests/ListPPCancelRequestsComponent';
import ItemPPCancelRequestsComponent from './BM/PPCancelRequests/ItemPPCancelRequestsComponent';
import ListCheckRequestsComponent from './BM/CheckRequests/ListCheckRequestsComponent';
import CheckRequestsComponent from './BM/CheckRequests/CheckRequestsComponent';
import ListPOPaymentsAfterBMComponent from './BM/POPaymentsAfter/POPaymentsAfterComponent';
import ListPOPaymentsBeforeBMComponent from './BM/POPaymentsBefore/POPaymentsBeforeComponent';
import ItemPOPaymentsAfterBMComponent from './BM/POPaymentsAfter/GetPOPaymentsAfterComponent';
import ItemPOPaymentsBeforeBMComponent from './BM/POPaymentsBefore/GetPOPaymentsBeforeComponent';

import ListPOCancelRequestsComponent from './BM/POCancelRequests/ListPOCancelRequestsComponent';
import ItemPOCancelRequestsComponent from './BM/POCancelRequests/ItemPOCancelRequestsComponent';
import DashBoardComponent from './BM/DashBoard/DashBoardComponent';
import ListPOComponent from './BM/ListPurchaseOrders/ListPOComponent';
import ListTicketRequestsComponent from './BM/TicketRequests/ListTicketRequestsComponent';
import AddTicketRequestsComponent from './BM/TicketRequests/AddTicketRequestsComponent';
import ItemTicketRequestsComponent from './BM/TicketRequests/ItemTicketRequestsComponent';
import ItemTicketRequestsDetailsComponent from './BM/TicketRequests/ItemTicketRequestsDetailsComponent';
import OpenCheckRequestsComponent from './BM/CheckRequests/OpenCheckRequestsComponent';

//end using for sontd-BM
//begin using for sontd-HR
import ListAnnouncementsComponent from './hr/Announcements/ListAnnouncementsComponent';
import ItemAnnouncementsComponent from './hr/Announcements/ItemAnnouncementsComponent';
import ListTrainingRequestsComponent from './hr/TrainingRequests/ListTrainingRequestsComponent';
import ItemTrainingRequestsComponent from './hr/TrainingRequests/ItemTrainingRequestsComponent';
//end using for sontd-HR
//begin using for sontd-AM
import ListItemsComponent from './AM/Items/ListItemsComponent';
import ViewItemsComponent from './AM/Items/ViewItemsComponent';
import ListPOContractsComponent from './AM/POContracts/ListPOContractsComponent';
import ItemPOContractsComponent from './AM/POContracts/ItemPOContractsComponent';
import ListContractsComponent from './AM/Contracts/ListContractsComponent';
import ItemContractsComponent from './AM/Contracts/ItemContractsComponent';
import ListObjectProvidersComponent from './AM/ObjectProviders/ListObjectProvidersComponent';
import ItemObjectProvidersComponent from './AM/ObjectProviders/ItemObjectProvidersComponent';
import ListTicketCheckingComponent from './AM/TicketChecking/ListTicketCheckingComponent';
import ViewTicketCheckingComponent from './AM/TicketChecking/ViewTicketCheckingComponent';
import AddTicketCheckingComponent from './AM/TicketChecking/AddTicketCheckingComponent';
//end using for sontd-AM
//begin using for sontd-SM
import ListPOPaymentsCustomerComponent from './SM/POPaymentsCustomer/POPaymentsCustomerComponent';
import ItemPOPaymentsCustomerComponent from './SM/POPaymentsCustomer/GetPOPaymentsCustomerComponent';
import ListCustomersComponent from './SM/Customers/ListCustomersComponent';
import ItemCustomersComponent from './SM/Customers/ItemCustomersComponent';
import ListCompaignsComponent from './SM/Compaigns/ListCompaignsComponent';
import ItemCompaignsComponent from './SM/Compaigns/ItemCompaignsComponent';
import KanbanLeadsComponet from './SM/Leads/KanbanLeadsComponet';
import ItemLeadsComponent from './SM/Leads/ItemLeadsComponent';
import Dashboard_KPI from './SM/ReportCRM/Dashboard_KPI';
import Customers_Add_Component from './SM/Customers/Customers_Add_Component';
import Orders_Form_Component from './SM/Orders/Orders_Form_Component';

//end using for sontd-SM
//begin using for sontd-PRO
import ListProductionResultsComponent from './PRO/ProductionResult/ListProductionResultsComponent';
import ItemProductionResultComponent from './PRO/ProductionResult/ItemProductionResultComponent';
//end using for sontd-PRO
//Begin habx@esvn.com.vn
import ProductionFollowComponent from './PRO/ProductionResult/ProductionFollowComponent';
//End habx@esvn.com.vn
//begin using for sontd-TM
import ListDocumentComponent from './TM/Document/ListDocumentComponent';
import AddFolderComponent from './TM/Document/AddFolderComponent';
//end using for sontd-TM
//begin using for TicketRequests
import ListTicketRequestsImportComponent from './AM/TicketRequestsImport/ListTicketRequestsImportComponent';
import ViewTicketRequestsImportComponent from './AM/TicketRequestsImport/ViewTicketRequestsImportComponent';
import ListTicketRequestsDetailsImportComponent from './AM/TicketRequestsImport/ListTicketRequestsDetailsImportComponent';
import AddTicketRequestsImportComponent from './AM/TicketRequestsImport/AddTicketRequestsImportComponent';
import ListTicketRequestsExportComponent from './AM/TicketRequestsExport/ListTicketRequestsExportComponent';
import ViewTicketRequestsExportComponent from './AM/TicketRequestsExport/ViewTicketRequestsExportComponent';
import ListTicketRequestsDetailsExportComponent from './AM/TicketRequestsExport/ListTicketRequestsDetailsExportComponent';
//end using for TicketRequests
//begin using for Comment
import CommentComponent from './component/CommentComponent';
//end using for Comment
//begin using for Attachment
import AttachmentComponent from './component/AttachmentComponent';
//end using for Attachment
//begin using for Attachment
import AttendancesComponent from './PRO/Attendances/AttendancesComponent';
//end using for Attachment

export {
  MainComponent,
  LoginComponent,
  SplashComponent,
  HomeComponent,
  FakeHome,
  SignupComponent,
  DrawerComponent,
  Edit_PassWord_Component,
  Notification_Detail_Components,
  Dialog,
  //CommentWF
  CommentWFComponent,
  //begin using for orders
  ListOrdersComponent,
  ViewOrderComponent,
  ListOrderDetailsComponent,
  OrdersAttachmentComponent,
  OrderDetailsSXComponent,
  OpenOrderDetailsSXComponent,
  //end using for orders
  //begin using for tm
  // TasksComponent,
  // TasksHomeComponent,
  // TasksDetailComponent,
  // TasksAddChildComponent,
  // TasksAttachmentComponent,
  // TasksCommentComponent,
  // TasksForwardComponent,
  // TasksProcessComponent,
  //end using for tm
  HrComponent,
  EmployeesComponent,
  EmpViewItemComponent,
  View_VouChers_Component,
  Open_VouChers_Component,
  //HR-trongtq@esvn.com.vn
  ApplyOutsidesListAll_Compoment,
  ApplyOutsidesItem_Component,
  ApplyOutsidesAdd_Component,
  ApplyOverTimesListAll_Component,
  ApplyOverTimesItem_Component,
  //End HR-trongtq@esvn.com.vn
  ApplyLeavesComponent,
  ApplyLeaveViewItemComponent,
  ViewBottonHrComponent,
  ApplyLeavesAddComponent,
  TimeKeepingsComponent,
  TimeKeepingbyMonthComponent,
  MeetingCalendarsComponent,
  GetMeetingCalendars,
  AddMeetingCalendars,
  // tiecnq
  ProductionOrdersComponent,
  //begin using for BM
  ItemProposedPurchaseComponent,
  ListProposedPurchaseComponent,
  ListProposedPurchasesComponent,
  ItemProposedPurchasesComponent,
  ItemProposedPurchaseDetailsComponent,
  AddProposedPurchase,
  ListPurchaseOrdersComponent,
  ItemPurchaseOrdersComponent,
  ItemPurchaseOrderDetailsComponent,
  CheckComponent,
  ListPPCancelRequestsComponent,
  ItemPPCancelRequestsComponent,
  ListCheckRequestsComponent,
  CheckRequestsComponent,
  ListPOPaymentsCustomerComponent,
  ItemPOPaymentsCustomerComponent,
  ListPOPaymentsAfterBMComponent,
  ItemPOPaymentsAfterBMComponent,
  ListPOPaymentsBeforeBMComponent,
  ItemPOPaymentsBeforeBMComponent,
  ListPOPaymentsAfterComponent,
  ItemPOPaymentsAfterComponent,
  ListRequirePaymentsComponent,
  ViewRequirePaymentsComponent,
  ListPOPaymentsBeforeComponent,
  ItemPOPaymentsBeforeComponent,
  ListPOCancelRequestsComponent,
  ItemPOCancelRequestsComponent,
  DashBoardComponent,
  ListPOComponent,
  ListTicketRequestsComponent,
  AddTicketRequestsComponent,
  ItemTicketRequestsComponent,
  ItemTicketRequestsDetailsComponent,
  OpenCheckRequestsComponent,
  Quotations_Form_Component,
  Orders_Form_Component,
  //end using for BM
  //begin using for sontd-AM
  ListItemsComponent,
  ViewItemsComponent,
  ListPOContractsComponent,
  ItemPOContractsComponent,
  ListContractsComponent,
  ItemContractsComponent,
  ListObjectProvidersComponent,
  ItemObjectProvidersComponent,
  ListTicketCheckingComponent,
  ViewTicketCheckingComponent,
  AddTicketCheckingComponent,
  //end using for sontd-AM
  //begin using for sontd-SM
  ListCustomersComponent,
  ItemCustomersComponent,
  ListCompaignsComponent,
  ItemCompaignsComponent,
  KanbanLeadsComponet,
  ItemLeadsComponent,
  Dashboard_KPI,
  Customers_Add_Component,
  //end using for sontd-SM
  //begin using for sontd-PRO
  ListProductionResultsComponent,
  ItemProductionResultComponent,
  //end using for sontd-PRO
  //begin using for sontd-TM
  ListDocumentComponent,
  AddFolderComponent,
  //end using for sontd-TM
  //TinhLV
  CheckinHistoryComponent,
  indexComponent,
  addComponent,
  openComponent,
  indexCompensationSlipComponent,
  addCompensationSlipComponent,
  openCompensationSlipComponent,
  addAdvances,
  indexAdvances,
  openAdvances,
  addAdvancesAM,
  indexAdvancesAM,
  openAdvancesAM,
  openContracts,
  openInsurances,
  openSalaries,
  indexCandidates,
  openCandidates,
  indexTrainingPlans,
  addTrainingPlans,
  openTrainingPlans,
  indexQuotations,
  openQuotations,
  openQuotationDetails,
  openTrainingPlanDetails,
  indexChartDashboard,
  indexDashboard,
  ConfirmPayment,
  ConfirmPayments,
  InvoiceOrdersComponent,
  GetInvoiceOrdersComponent,
  OpenApplyOutsideDetails,
  ListProjects,
  OpenProject,
  ListProjectItems,
  OpenProjectItems,
  openHistories,
  openRewards,
  openCertificates,
  //end using for BM
  //begin using for TicketRequests
  ListTicketRequestsImportComponent,
  ViewTicketRequestsImportComponent,
  ListTicketRequestsDetailsImportComponent,
  AddTicketRequestsImportComponent,
  ListTicketRequestsExportComponent,
  ViewTicketRequestsExportComponent,
  ListTicketRequestsDetailsExportComponent,
  //end using for TicketRequests
  ApplyOverTimesAdd_Component,
  //begin using for Comment
  CommentComponent,
  //end using for Comment
  ListAll_Messange_Component,
  ViewItem_Messange_Component,
  AddGroup_Component,
  //begin using for sontd-HR
  ListAnnouncementsComponent,
  ItemAnnouncementsComponent,
  Information_Component,
  ListTrainingRequestsComponent,
  ItemTrainingRequestsComponent,
  //begin using for sontd-HR
  //thanhtt@esvn.com.vn
  WorkShiftChangeComponent,
  AddWorkShiftChangeComponent,
  GetWorkShiftChangeComponent,
  QuotationRequestComponent,
  AddQuotationRequestComponent,
  GetQuotationRequestComponent,
  QuotationRequestProComponent,
  AddQuotationRequestProComponent,
  GetQuotationRequestProComponent,
  RegisterEatsComponent,
  AddEatsComponent,
  GetEatComponent,
  ResignationFormsComponent,
  GetResignationForms,
  AddResignationForms,
  RecruitmentPlansComponent,
  GetRecruitmentPlans,
  RecruitmentRequestsComponent,
  GetRecruitmentRequests,
  TrainingResultsComponent,
  GetTrainingResults,
  RecruitmentSchedulesComponent,
  GetRecruitmentSchedules,
  ViewSalaryComponent,
  GetViewSalary,
  ChartDashboardAMComponent,
  UniformsComponent,
  GetUniforms,
  ViewInsurancesComponent,
  GetInsurances,
  RewardsComponent,
  GetRewards,
  AddEmployeeCheckIn,
  TimeKeepingsConfirmComponent,
  GetTimeKeepingsConfirm,
  timeKeepingsComfirm_Add,
  ProfileComponent,
  ViewSalaryByMeComponent,
  UniformsByMeComponent,
  InsurancesByMeComponent,
  TimeKeepingsByMeComponent,
  ApplyOverTimesByMeComponent,
  ApplyOverTimesItemByMe_Component,
  ApplyOutsidesItemByMe_Component,
  ApplyLeavesByMeComponent,
  ApplyOutsidesByMeComponent,
  GetRewardsByMe,
  DisciplineByMeComponent,
  MaintenanceRequestComponent,
  GetMaintenanceRequest,
  openMaintenanceManagement,
  //to doby me
  index_ToDoByMe_Component,
  //end
  //begin using for Attachment
  AttachmentComponent,
  //end using for Attachment
  ViewAllGroupByLoginName_Compoment,
  ViewDetailAllUser_Component,
  AddUserOfGroup_Component,
  ViewAllImage_Component,
  TestSound_Component,
  GetAllPaging_Component,
  VoucherAM_ViewItem_Component,
  Transfer_ListAll_Component,
  Transfer_ViewItem_Component,
  VoucherAM_ViewDetail_Component,
  Task_Add_Component,
  Task_Detail_Component,
  Task_Comment_Component,
  Calendar_Detail_Component,
  Calendar_Add_Component,
  Task_ListAll_Component,
  AttendancesComponent,
  Task_UpdateProcess_Component,
  QRCode,
  QRCodeV2,
  index_ToDo_Component,
  MyDay_ToDo_Component,
  open_ToDo_Component,
  ListData_ToDo_Component,
  CheckInComponent,
  Mail_index_Component,
  Mail_Message_Component,
  Mail_open_Component,
  ListCalendars_Component,
  PlanTotal_List_Component,
  ProductionPlanDay_List_Component,
  ProductionOrders_Open_Component,
  PlanTotal_Open_Component,
  ProductionPlanDay_Open_Component,
  ProductionPlanDayUpdateComponent,
  ListProposedPurchasesProComponent,
  ItemProposedPurchasesProComponent,
  ItemPuschaseOderProgress2Component,
  ListPuschaseOderProgress2Component,
  BomCategories_List_Component,
  BomCategories_Open_Component,
  ProductionPlan_List_Component,
  ProductionPlan_Open_Component,
  ProductionPlanCNC_List_Component,
  ProductionPlanCNC_Open_Component,
  Operator_index_Component,
  Operator_Employee_Component,
  Operator_MachinesTools_Component,
  Operator_Supplies_Component,
  Operator_Document_Component,
  Operator_indexDocument_Component,
  Operator_ItemPrint_Component,
  Operator_Print_Component,
  Operator_ItemJobCard_Component,
  Operator_QCCheck_Component,
  Operator_Warehouses_Component,
  Operator_UpdateMachidnes_Component,
  Operator_addOverTime_Component,
  Operator_addEmployee_Component,
  Operator_sendBCEmployee_Component,
  Operator_EditTools_Component,
  Operator_AlertMachines_Component,
  Operator_addReportPlan_Component,
  Operator_ListPlanJobCards_Component,
  Operator_StopProduction_Component,
  Operator_addTicketRequests_Component,
  Operator_addTicketRequestsExport_Component,
  Operator_addMaterialErrors_Component,
  Operator_addTicketRequestsExportVT_Component,
  Operator_indexSupplies_Component,
  ListOrderProgessComponent,
  ItemOrderProgessComponent,
  QCCheckComponent,
  //Begin habx@esvn.com.vn
  ProductionFollowComponent,
  //End habx@esvn.com.vn
};
