import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import WeatherInfo from './WeatherInfo';
import ReloadIcon from './ReloadIcon';
import WeatherDetails from './WeatherDetails';
import { Icon } from 'react-native-elements';
import { ActionConst, Actions } from 'react-native-router-flux';
const WEATHER_API_KEY = '0b6da490707e7298a84fd66506c14566';
const BASE_WEATHER_URL = 'https://api.openweathermap.org/data/2.5/weather?';

export default function App() {
  const [errorMessage, setErrorMessage] = useState(null);
  const [currentWeather, setCurrentWeather] = useState(null);
  const [currentWeatherDetails, setCurrentWeatherDetails] = useState(null);
  const [unitsSystem, setUnitsSystem] = useState('metric');
  useEffect(() => {
    load();
  }, [unitsSystem]);
  async function load() {
    setCurrentWeatherDetails(null);
    setCurrentWeather(null);
    setErrorMessage(null);
    try {
      const weatherUrl = `${BASE_WEATHER_URL}lat=21.030653&lon=105.847130&units=${unitsSystem}&appid=${WEATHER_API_KEY}`;
      const response = await fetch(weatherUrl);
      const result = await response.json();
      if (response.ok) {
        setCurrentWeather(result.main.temp);
        setCurrentWeatherDetails(result);
      } else {
        setErrorMessage(result.message);
      }
    } catch (error) {
      setErrorMessage(error.message);
    }
  }
  if (currentWeatherDetails) {
    //const  {main : temp} = currentWeather
    return (
      <View style={styles.container}>
        <View
          style={{
            alignItems: 'flex-end',
            justifyContent: 'flex-end',
            marginTop: 40,
            marginRight: 10,
          }}>
          <Icon
            onPress={() => {
              Actions.pop();
            }}
            color="black"
            name={'logout'}
            type="antdesign"
            size={20}
          />
        </View>
        <View style={styles.main}>
          <ReloadIcon load={load} />
          <WeatherInfo
            currentWeather={currentWeather}
            currentWeatherDetails={currentWeatherDetails}
          />
        </View>
        <WeatherDetails
          currentWeather={currentWeather}
          currentWeatherDetails={currentWeatherDetails}
          unitsSystem={unitsSystem}
        />
      </View>
    );
  } else if (errorMessage) {
    return (
      <View style={styles.container}>
        <Text>{errorMessage}</Text>
      </View>
    );
  } else {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color={'blue'} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  main: {
    flex: 1,
    justifyContent: 'center',
  },
});
