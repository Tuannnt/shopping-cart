import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Icon} from 'react-native-elements';

export default function WeatherDetails({
  currentWeather,
  currentWeatherDetails,
  unitsSystem,
}) {
  const {
    main: {feels_like, humidity, pressure},
    wind: {speed},
  } = currentWeatherDetails;

  const windSpeed =
    unitsSystem === 'metric'
      ? `${Math.round(speed)} m/s`
      : `${Math.round(speed)} miles/h`;

  return (
    <View style={styles.weatherDetails}>
      <View style={styles.weatherDetailsRow}>
        <View
          style={{
            ...styles.weatherDetailsBox,
            borderRightWidth: 1,
            borderRightColor: 'black',
          }}>
          <View style={styles.weatherDetailsRow}>
            <Icon color="blue" name={'user'} type="antdesign" size={20} />
            <View style={styles.weatherDetailsItems}>
              <Text>Feels like :</Text>
              <Text style={styles.textSecondary}>{feels_like} °</Text>
            </View>
          </View>
        </View>
        <View style={styles.weatherDetailsBox}>
          <View style={styles.weatherDetailsRow}>
            <Icon color="blue" name={'cloud'} type="antdesign" size={20} />
            <View style={styles.weatherDetailsItems}>
              <Text>Humidity :</Text>
              <Text style={styles.textSecondary}>{humidity} %</Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          ...styles.weatherDetailsRow,
          borderTopWidth: 1,
          borderTopColor: 'black',
        }}>
        <View
          style={{
            ...styles.weatherDetailsBox,
            borderRightWidth: 1,
            borderRightColor: 'black',
          }}>
          <View style={styles.weatherDetailsRow}>
            <Icon color="blue" name={'wind'} type="feather" size={20} />
            <View style={styles.weatherDetailsItems}>
              <Text>Wind Speed :</Text>
              <Text style={styles.textSecondary}>{windSpeed}</Text>
            </View>
          </View>
        </View>
        <View style={styles.weatherDetailsBox}>
          <View style={styles.weatherDetailsRow}>
            <Icon color="blue" name={'dashboard'} type="antdesign" size={20} />
            <View style={styles.weatherDetailsItems}>
              <Text>Pressure :</Text>
              <Text style={styles.textSecondary}>{pressure} hPa</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  weatherDetails: {
    marginTop: 'auto',
    margin: 15,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
  },
  weatherDetailsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  weatherDetailsBox: {
    flex: 1,
    padding: 20,
  },
  weatherDetailsItems: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  textSecondary: {
    fontSize: 15,
    color: 'gray',
    fontWeight: '700',
    margin: 7,
  },
});
