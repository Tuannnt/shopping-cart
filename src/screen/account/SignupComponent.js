import React, {Component} from 'react';
import {
  Keyboard,
  ScrollView,
  TouchableOpacity,
  View,
  TextInput,
  Text,
  StyleSheet,
  AppState,
} from 'react-native';
import {Icon, Divider, Button} from 'react-native-elements';
import {connect} from 'react-redux';
import {Back} from '@component';
import {AppStyles, AppSizes, AppColors} from '@theme';
import {messages} from '@i18n';
import TabBar_Title from '../component/TabBar_Title';
import {Actions} from 'react-native-router-flux';
import Toast from 'react-native-simple-toast';
import {API_HR} from '@network';

class SignupComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DataUp: {
        email: '',
        Password: '',
        ConfirmPassword: '',
      },
      showPassword: true,
      showConfirmPassword: true,
    };
  }
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
  }
  callBackSuccess() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'SignUp',
      Data: this.state.DataUp,
      ActionTime: new Date().getTime(),
    });
  }
  setEmail = email => {
    this.state.DataUp.email = email;
    this.setState({DataUp: this.state.DataUp});
  };
  setPassword = val => {
    this.state.DataUp.Password = val;
    this.setState({DataUp: this.state.DataUp});
  };
  setPasswordReNew = val => {
    this.state.DataUp.ConfirmPassword = val;
    this.setState({DataUp: this.state.DataUp});
  };
  validateEmail = email => {
    return email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );
  };
  validatePassword = pass => {
    var regex = /^(?=.*?[0-9])(?=.*?[A-Z]).{8,}$/;
    return regex.test(pass);
  };
  onPressSignup = () => {
    if (!this.state.DataUp.email) {
      Toast.showWithGravity('Yêu cầu nhập email', Toast.SHORT, Toast.CENTER);
      return;
    }
    if (!this.validateEmail(this.state.DataUp.email)) {
      Toast.showWithGravity(
        'Yêu cầu nhập đúng định dạng email',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.DataUp.Password) {
      Toast.showWithGravity('Yêu cầu nhập mật khẩu', Toast.SHORT, Toast.CENTER);
      return;
    }
    if (!this.validatePassword(this.state.DataUp.Password)) {
      Toast.showWithGravity(
        'Mật khẩu yêu cầu tối thiểu 6 kí tự, tối đa 32 kí tự, bao gồm cả chữ, số và có ký tự in hoa',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.DataUp.ConfirmPassword) {
      Toast.showWithGravity(
        'Yêu cầu xác nhận lại mật khẩu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (this.state.DataUp.Password !== this.state.DataUp.ConfirmPassword) {
      Toast.showWithGravity(
        'Xác nhận mật khẩu không trùng khớp với mật khẩu.',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let obj = {
      UserName: this.state.DataUp.email,
      Password: this.state.DataUp.Password,
    };
    API_HR.SignUp(obj)
      .then(res => {
        console.log(res);
        if (res.data.errorCode === 200) {
          Toast.showWithGravity('Thành công', Toast.SHORT, Toast.CENTER);
          this.callBackSuccess();
        } else {
          Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  render() {
    return (
      <View style={[AppStyles.container]}>
        <TabBar_Title
          title={'Tạo tài khoản'}
          callBack={() => this.callBackList()}
        />
        <Divider />
        <ScrollView style={{flex: 1, paddingHorizontal: 20}}>
          <View style={{justifyContent: 'center', padding: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={AppStyles.Labeldefault}>Email</Text>
              <Text style={{color: 'red'}}>*</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <TextInput
                style={[AppStyles.FormInput, {flex: 1, marginRight: 10}]}
                underlineColorAndroid="transparent"
                value={this.state.DataUp.email}
                placeholder={'Email'}
                autoFocus={true}
                autoCapitalize="none"
                onChangeText={text => this.setEmail(text)}
              />
            </View>
          </View>
          <View style={{justifyContent: 'center', padding: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={AppStyles.Labeldefault}>Mật khẩu</Text>
              <Text style={{color: 'red'}}> *</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <TextInput
                style={[AppStyles.FormInput, {flex: 1, marginRight: 10}]}
                underlineColorAndroid="transparent"
                value={this.state.DataUp.Password}
                placeholder={'Nhập mật khẩu'}
                secureTextEntry={this.state.showPassword}
                autoCapitalize="none"
                onChangeText={text => this.setPassword(text)}
              />
              {/* {this.state.showPassword ? (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({showPassword: false});
                  }}>
                  <Icon
                    name={'eye-off'}
                    type={'feather'}
                    size={20}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({showPassword: true});
                  }}>
                  <Icon
                    name={'eye'}
                    type={'feather'}
                    size={20}
                    color={'black'}
                  />
                </TouchableOpacity>
              )} */}
            </View>
          </View>
          <View style={{justifyContent: 'center', padding: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={AppStyles.Labeldefault}>Xác nhận mật khẩu</Text>
              <Text style={{color: 'red'}}> *</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <TextInput
                style={[AppStyles.FormInput, {flex: 1, marginRight: 10}]}
                underlineColorAndroid="transparent"
                value={this.state.DataUp.ConfirmPassword}
                placeholder={'Nhập lại mật khẩu'}
                secureTextEntry={this.state.showConfirmPassword}
                onChangeText={text => this.setPasswordReNew(text)}
              />
              {/* {this.state.showConfirmPassword ? (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({showConfirmPassword: false});
                  }}>
                  <Icon
                    name={'eye-off'}
                    type={'feather'}
                    size={20}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({showConfirmPassword: true});
                  }}>
                  <Icon
                    name={'eye'}
                    type={'feather'}
                    size={20}
                    color={'black'}
                  />
                </TouchableOpacity>
              )} */}
            </View>
          </View>

          <View>
            <Button
              buttonStyle={{
                backgroundColor: AppColors.ColorButtonSubmit,
                marginTop: 20,
              }}
              title={messages.signup.toUpperCase()}
              onPress={this.onPressSignup}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const colors = {
  loginGreen: '#347F16',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: '8%',
    paddingHorizontal: '5%',
  },
  logo: {
    marginTop: '8%',
    alignSelf: 'center',
  },
  buttonContainer: {
    marginTop: AppSizes.padding,
    flexDirection: 'row',
  },
  button: {
    paddingHorizontal: 25,
    height: AppSizes.buttonHeight,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
  textForgotPassword: {
    ...AppStyles.baseText,
    color: colors.loginGreen,
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignupComponent);
