import React, { Component } from 'react';
import {
  Keyboard,
  TouchableOpacity,
  Image,
  View,
  ImageBackground,
  Dimensions,
  StyleSheet,
  PermissionsAndroid,
  TouchableWithoutFeedback,
} from 'react-native';
import { Header, Divider, Button, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Actions, ActionConst } from 'react-native-router-flux';
import { AppStyles, AppSizes, AppColors } from '@theme';
import { messages } from '@i18n';
import { ErrorHandler } from '@error';
import { API } from '@network';
import { AccessTokenManager } from '@bussiness';

//Begin created by habx@esvn.com.vn
import OneSignal from 'react-native-onesignal';
import { Content } from 'native-base';
//End created by habx@esvn.com.vn
var DEVICE = '';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null,
      check: false,
      infoOnesignal: {},
    };
    OneSignal.init('3459ec28-1ef3-45d5-914a-81716539984a');
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.configure();
  }
  //Begin created by habx@esvn.com.vn
  // componentWillMount() {
  //     OneSignal.init("90ec0b5b-f92d-4bfa-84c7-4bf71cdc3d87");
  //     OneSignal.addEventListener("ids", this.onIds);
  //     OneSignal.configure();
  //   }
  //   componentWillUnmount() {
  //     OneSignal.removeEventListener("ids", this.onIds);
  //   }
  onIds(device) {
    DEVICE = device;
  }
  //End created by habx@esvn.com.vn

  render() {
    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View
          style={[AppStyles.container, { justifyContent: 'space-between' }]}
          pointerEvents={this.state.processing ? 'none' : 'auto'}>
          <View>
            <Image
              resizeMode="contain"
              source={require('../../images/logo/eastern-sun-logo.png')}
              style={[styles.logo]}
            />
            <View style={[{ paddingLeft: 20, paddingRight: 20 }]}>
              <Input
                ref="usernameInput"
                inputStyle={[AppStyles.textInput]}
                inputContainerStyle={[AppStyles.textInput]}
                leftIcon={
                  <Icon
                    name="user"
                    size={AppSizes.Login_Size_Icon}
                    color={AppColors.gray}
                    style={{ marginRight: 10 }}
                  />
                }
                containerStyle={{ width: '100%' }}
                autoCorrect={false}
                keyboardType="email-address"
                returnKeyType="next"
                placeholder={messages.login.UserName}
                onChangeText={text =>
                  this.setState({ username: text, check: false })
                }
                errorMessage={
                  _.isEmpty(this.state.username) && this.state.check
                    ? messages.error.user_invalid
                    : null
                }
              />
              <Input
                ref="passwordInput"
                inputStyle={[AppStyles.textInput]}
                inputContainerStyle={[AppStyles.textInput]}
                leftIcon={
                  <Icon
                    name="lock"
                    size={AppSizes.Login_Size_Icon}
                    color={AppColors.gray}
                    style={{ marginRight: 10 }}
                  />
                }
                containerStyle={{
                  width: '100%',
                  marginVertical: AppSizes.padding,
                }}
                autoCorrect={false}
                secureTextEntry={true}
                onSubmitEditing={this.onSubmited}
                returnKeyType="go"
                placeholder={messages.login.password}
                onChangeText={text =>
                  this.setState({ password: text, check: false })
                }
                errorMessage={
                  _.isEmpty(this.state.password) && this.state.check
                    ? messages.error.password_invalid
                    : null
                }
              />
              <TouchableOpacity>
                <Button
                  title={messages.login.login.toUpperCase()}
                  buttonStyle={[
                    styles.button,
                    { backgroundColor: AppColors.Maincolor },
                  ]}
                  titleStyle={styles.buttonText}
                  onPress={() => this.onPressLogin()}
                  loading={this.state.processing}
                />
              </TouchableOpacity>
            </View>
          </View>

          <ImageBackground
            source={require('../../images/Login/CN401.jpg')}
            style={{ width: DRIVER.width, height: 350 }}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  onPressSignup() {
    Actions.signup();
  }
  onPressLogin() {
    Keyboard.dismiss();
    // Validate empty email and password
    this.setState({ check: true });
    if (_.isEmpty(this.state.username) || _.isEmpty(this.state.password)) {
      return;
    }
    this.setState({ processing: true });
    API.login(this.state.username, this.state.password)
      .then(res => {
        this.setState({ processing: false });
        if (res.error) {
          ErrorHandler.handleError('Tên đăng nhập hoặc mật khẩu không đúng');
          return;
        }
        AccessTokenManager.saveAccessToken(res.access_token);
        this.checkPermission();
        this.getprofile(res.access_token);
        Actions.home();
      })
      .catch(error => {
        console.log(error);
        this.setState({ processing: false });
        ErrorHandler.handleError('Lỗi đăng nhập');
      });
  }
  async checkPermission() {
    await this.request_storage_runtime_permission();
  }
  getprofile(t) {
    API.getProfileByToken(t)
      .then(res => {
        this.registerDevice(res);
      })
      .catch(error => {
        ErrorHandler.handleError('Lỗi lấy dữ liệu người đăng nhập', error);
      });
  }
  //Create init notification for onesignal
  registerDevice(res) {
    try {
      var _item = {
        DeviceId: DEVICE.userId,
        LoginName: res.data.LoginName,
        OrganizationGuid: res.data.OrganizationGuid,
        ModuleId: 'MOBILE',
      };
      API.InitNotiCommon(_item)
        .then(rs => { })
        .catch(error => {
          console.log('Lỗi khởi tạo thông báo', error);
        });
    } catch (ex) {
      console.log('Module : LOGIN_FORM, ERROR: "Can not load user login"', ex);
    }
  }
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
      console.log(err);
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: '8%',
    paddingHorizontal: '5%',
  },
  logo: {
    marginTop: 70,
    alignSelf: 'center',
    width: DRIVER.width,
    height: 110,
  },
  button: {
    margin: 10,
    marginBottom: 0,
    borderRadius: 20,
  },
  buttonText: {
    color: '#fff',
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginComponent);

/////// Have register ==============================================>

// import React, {Component} from 'react';
// import {
//   Keyboard,
//   TouchableOpacity,
//   Image,
//   View,
//   ImageBackground,
//   Dimensions,
//   StyleSheet,
//   PermissionsAndroid,
//   TouchableWithoutFeedback,
// } from 'react-native';
// import {Header, Divider, Button, Input} from 'react-native-elements';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import {connect} from 'react-redux';
// import _ from 'lodash';
// import {Actions, ActionConst} from 'react-native-router-flux';
// import {AppStyles, AppSizes, AppColors} from '@theme';
// import {messages} from '@i18n';
// import {ErrorHandler} from '@error';
// import {API, API_HR} from '@network';
// import {AccessTokenManager} from '@bussiness';

// //Begin created by habx@esvn.com.vn
// import OneSignal from 'react-native-onesignal';
// import {Content} from 'native-base';
// //End created by habx@esvn.com.vn
// var DEVICE = '';
// const DRIVER = {
//   width: Dimensions.get('window').width,
//   height: Dimensions.get('window').height,
// };
// const logo = '@images/TM/CalendarNull.jpg';
// class LoginComponent extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       username: null,
//       password: null,
//       check: false,
//       infoOnesignal: {},
//     };
//     OneSignal.init('3459ec28-1ef3-45d5-914a-81716539984a');
//     OneSignal.addEventListener('ids', this.onIds);
//     OneSignal.configure();
//   }
//   //Begin created by habx@esvn.com.vn
//   // componentWillMount() {
//   //     OneSignal.init("90ec0b5b-f92d-4bfa-84c7-4bf71cdc3d87");
//   //     OneSignal.addEventListener("ids", this.onIds);
//   //     OneSignal.configure();
//   //   }
//   //   componentWillUnmount() {
//   //     OneSignal.removeEventListener("ids", this.onIds);
//   //   }
//   onIds(device) {
//     DEVICE = device;
//   }
//   //End created by habx@esvn.com.vn
//   componentWillReceiveProps(nextProps) {
//     if (nextProps.moduleId == 'SignUp' && nextProps.Data) {
//       this.setState({
//         username: nextProps.Data.email,
//         password: nextProps.Data.Password,
//       });
//     }
//   }
//   render() {
//     return (
//       <TouchableWithoutFeedback
//         style={{flex: 1}}
//         onPress={() => {
//           Keyboard.dismiss();
//         }}>
//         <View
//           style={[AppStyles.container, {justifyContent: 'space-between'}]}
//           pointerEvents={this.state.processing ? 'none' : 'auto'}>
//           <View>
//             <Image
//               resizeMode="contain"
//               source={require(logo)}
//               style={[styles.logo]}
//             />
//             <View style={[{paddingLeft: 20, paddingRight: 20}]}>
//               <Input
//                 ref="usernameInput"
//                 inputStyle={[AppStyles.textInput]}
//                 inputContainerStyle={[AppStyles.textInput]}
//                 leftIcon={
//                   <Icon
//                     name="user"
//                     size={AppSizes.Login_Size_Icon}
//                     color={AppColors.gray}
//                     style={{marginRight: 10}}
//                   />
//                 }
//                 containerStyle={{width: '100%'}}
//                 autoCorrect={false}
//                 keyboardType="email-address"
//                 returnKeyType="next"
//                 placeholder={messages.login.UserName}
//                 value={this.state.username}
//                 onChangeText={text =>
//                   this.setState({username: text, check: false})
//                 }
//                 errorMessage={
//                   _.isEmpty(this.state.username) && this.state.check
//                     ? messages.error.user_invalid
//                     : null
//                 }
//               />
//               <Input
//                 ref="passwordInput"
//                 inputStyle={[AppStyles.textInput]}
//                 inputContainerStyle={[AppStyles.textInput]}
//                 leftIcon={
//                   <Icon
//                     name="lock"
//                     size={AppSizes.Login_Size_Icon}
//                     color={AppColors.gray}
//                     style={{marginRight: 10}}
//                   />
//                 }
//                 containerStyle={{
//                   width: '100%',
//                   marginVertical: AppSizes.padding,
//                 }}
//                 autoCorrect={false}
//                 secureTextEntry={true}
//                 onSubmitEditing={this.onSubmited}
//                 returnKeyType="go"
//                 placeholder={messages.login.password}
//                 value={this.state.password}
//                 onChangeText={text =>
//                   this.setState({password: text, check: false})
//                 }
//                 errorMessage={
//                   _.isEmpty(this.state.password) && this.state.check
//                     ? messages.error.password_invalid
//                     : null
//                 }
//               />
//               <TouchableOpacity>
//                 <Button
//                   title={messages.login.login.toUpperCase()}
//                   buttonStyle={[
//                     styles.button,
//                     {backgroundColor: AppColors.Maincolor},
//                   ]}
//                   titleStyle={styles.buttonText}
//                   onPress={() => this.onPressLogin()}
//                   loading={this.state.processing}
//                 />
//               </TouchableOpacity>
//               <TouchableOpacity>
//                 <Button
//                   title={messages.login.signup.toUpperCase()}
//                   buttonStyle={[
//                     styles.button,
//                     {backgroundColor: AppColors.blue},
//                   ]}
//                   titleStyle={styles.buttonText}
//                   onPress={() => this.onPressSignup()}
//                 />
//               </TouchableOpacity>
//             </View>
//           </View>
//         </View>
//       </TouchableWithoutFeedback>
//     );
//   }
//   onPressSignup() {
//     Actions.signup();
//   }
//   onPressLogin() {
//     Keyboard.dismiss();
//     // Validate empty email and password
//     this.setState({check: true});
//     if (_.isEmpty(this.state.username) || _.isEmpty(this.state.password)) {
//       return;
//     }
//     this.setState({processing: true});
//     API_HR.SignIn({
//       UserName: this.state.username,
//       Password: this.state.password,
//     })
//       .then(res => {
//         console.log(res);
//         this.setState({processing: false});
//         if (res.data.errorCode === 500) {
//           ErrorHandler.handleError('Tên đăng nhập hoặc mật khẩu không đúng');
//           return;
//         }
//         Actions.home2();
//       })
//       .catch(error => {
//         console.log(error);
//         this.setState({processing: false});
//         ErrorHandler.handleError('Lỗi đăng nhập');
//       });
//   }
//   async checkPermission() {
//     await this.request_storage_runtime_permission();
//   }
//   getprofile(t) {
//     API.getProfileByToken(t)
//       .then(res => {
//         this.registerDevice(res);
//       })
//       .catch(error => {
//         ErrorHandler.handleError('Lỗi lấy dữ liệu người đăng nhập', error);
//       });
//   }
//   //Create init notification for onesignal
//   registerDevice(res) {
//     try {
//       var _item = {
//         DeviceId: DEVICE.userId,
//         LoginName: res.data.LoginName,
//         OrganizationGuid: res.data.OrganizationGuid,
//         ModuleId: 'MOBILE',
//       };
//       API.InitNotiCommon(_item)
//         .then(rs => {})
//         .catch(error => {
//           console.log('Lỗi khởi tạo thông báo', error);
//         });
//     } catch (ex) {
//       console.log('Module : LOGIN_FORM, ERROR: "Can not load user login"', ex);
//     }
//   }
//   async request_storage_runtime_permission() {
//     try {
//       const granted = await PermissionsAndroid.request(
//         PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
//         {
//           title: 'ReactNativeCode Storage Permission',
//           message:
//             'ReactNativeCode App needs access to your storage to download Photos.',
//         },
//       );
//       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//         console.log('Storage Permission Granted.');
//       } else {
//         console.log('Storage Permission Not Granted');
//       }
//     } catch (err) {
//       console.warn(err);
//       console.log(err);
//     }
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     marginTop: '8%',
//     paddingHorizontal: '5%',
//   },
//   logo: {
//     marginTop: 70,
//     alignSelf: 'center',
//     width: DRIVER.width,
//     height: DRIVER.height / 2,
//   },
//   button: {
//     margin: 10,
//     marginBottom: 0,
//     borderRadius: 20,
//   },
//   buttonText: {
//     color: '#fff',
//   },
// });
// //Redux
// const mapStateToProps = state => ({});
// const mapDispatchToProps = {};
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(LoginComponent);
