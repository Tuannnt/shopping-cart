import React, { Component } from 'react';
import { StatusBar, View, Image, Text, StyleSheet } from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { messages } from '@i18n';
import { AppColors, AppSizes, AppStyles } from '@theme';
import { AccessTokenManager } from '@bussiness';
import _ from 'lodash';
import { API } from '@network';
//Begin created by habx@esvn.com.vn
global.__appSIGNALR = new FuncCommon.SignalRCommon();
import signalr from 'react-native-signalr';
import configApp from '../../configApp';
import OneSignal from 'react-native-onesignal';
import { FuncCommon } from '../../utils';
//End created by habx@esvn.com.vn
class SplashComponent extends Component {
  constructor(props) {
    super(props);
    OneSignal.init('3459ec28-1ef3-45d5-914a-81716539984a');
    // OneSignal.addEventListener("received", (e) => this.onReceived(e));
    OneSignal.addEventListener('opened', e => this.onReceived(e));
    OneSignal.configure();
    global.__appSIGNALR.init(signalr, configApp.url_signalr_mb);
  }
  onReceived(e) {
    switch (e.action.actionID) {
      case 'id_ok':
        url =
          Platform.OS === 'android'
            ? e.notification.payload.launchURL
            : e.notification.payload.launchURL.replace('erpapp/', '');
        if (!url) {
          return;
        }
        FuncCommon.RouterLink(url, Actions);
        break;
      case '__DEFAULT__':
        break;
      default:
        return;
    }
  }
  render() {
    return (
      <View
        style={[
          AppStyles.container,
          { alignItems: 'center', justifyItems: 'center' },
        ]}>
        <StatusBar hidden={true} />
        <Image
          source={require('../../images/logo/logoesvn.png')}
          style={{ marginTop: '50%' }}
        />
        <Text style={styles.textSlogan}> {messages.splash.slogan}</Text>
        <Text style={styles.textCopyright}>{messages.splash.copyright}</Text>
      </View>
    );
  }

  componentDidMount() {
    AccessTokenManager.initialize()
      .then(() => {
        if (!_.isEmpty(AccessTokenManager.getAccessToken())) {
          Actions.app({ type: ActionConst.RESET });
          // API.getConfig().then(data => console.log('config', data));
        } else {
          Actions.login({ type: ActionConst.RESET });
        }
      })
      .catch(err => {
        Actions.login({ type: ActionConst.RESET });
      });
  }
}

const styles = StyleSheet.create({
  textSlogan: {
    ...AppStyles.boldText,
    color: AppColors.text_lighgreen,
    padding: AppSizes.paddingXSmall,
  },

  textCopyright: {
    ...AppStyles.boldText,
    fontSize: AppSizes.textSmall,
    color: AppColors.text_gray,
    position: 'absolute',
    bottom: 0,
    padding: AppSizes.padding,
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(SplashComponent);
