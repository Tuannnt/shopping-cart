import React, { Component } from 'react';
import {
  Keyboard,
  Platform,
  View,
  FlatList,
  Text,
  StyleSheet,
  AppState,
} from 'react-native';
import { connect } from 'react-redux';
import OneSignal from 'react-native-onesignal'; // Import package from node modules

/**
 * Empty component to handle notification and some common task
 */

class MainComponent extends Component {
  constructor(properties) {
    super(properties);
    OneSignal.init('3459ec28-1ef3-45d5-914a-81716539984a');

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log('Notification received: ', notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  render() {
    return <View />;
  }
}

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(MainComponent);
