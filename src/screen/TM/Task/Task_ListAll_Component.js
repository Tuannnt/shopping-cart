import React, { Component } from 'react';
import { FlatList, Keyboard, RefreshControl, Text, TextInput, TouchableWithoutFeedback, View, Dimensions, TouchableOpacity, Alert, ScrollView, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Divider, Icon, ListItem, } from 'react-native-elements';
import { API_TM_TASKS, API_TM_CALENDAR, API } from '@network';
import TabBar from '../../component/TabBar';
import { AppStyles, AppColors } from '@theme';
import FormSearch from '../../component/FormSearch';
import SearchDay from '../../component/SearchDay';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import * as Progress from 'react-native-progress';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import { FuncCommon } from '@utils';
import { DataCombobox,  } from './DataCombobox';
import { controller } from './controller';
import LoadingComponent from '../../component/LoadingComponent';
import ComboboxV2 from '../../component/ComboboxV2';
import Kanban_Component from './Kanban_Component';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Task_ListAll_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            loadingSearch: false,
            InforUserLogin: global.__appSIGNALR.SIGNALR_object.USER,
            EvenCalendar: false,
            EvenKanban: false,
            EvenFromSearch: false,
            ListtabbarBotom: [],
            StartDate: FuncCommon.ConDate(new Date(), 2),
            EndDate: FuncCommon.ConDate(new Date(), 2),
            Process: "",
            selectItem: null,
            KanbanView: [],
            Count_Tab: 0,
            SearchKanban: false,
            ListCalendar: [],
            selectItemCalendar: null,
            DataMenuBottom: [],
            offload: false,
            NameMenuCalendar: '',
            refreshing: false,
            ChoiceDate: '',
            ListTask: [],
            OffSearchDay: false,
            ListTask_ParentId: [],

        };
        this.jtableTasks = {
            txtSearch: "",
            Page: 1,
            StartDate: FuncCommon.ConDate(new Date(), 2),
            EndDate: FuncCommon.ConDate(new Date(), 2),
            Process: null
        };
        this.jtableCalendar = {
            Search: '',
            StartDateTime: null,
            EndDateTime: null,
        };
        this._search_Kanban = {
            Page: 0,
            Search: "",
            Process: "",
        };

    }
    componentDidMount() {
        this.setState({ loading: true });
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.moduleId == "Taskadd") {
            if (this.state.EvenKanban === true) {
                this._ReloadKanban();
            }
        } else if (nextProps.moduleId === "Calendar_Back") {
            this.setState({ EvenCalendar: true })
            this.Calendar_GetAll();
        }
        this.nextPage(true);
    }
    _ReloadKanban() { };
    ReloadKanban = (d) => {
        this._ReloadKanban = d;
    }
    CountTask = () => {
        //Đếm từng trạng thái của task
        var data = {
            OrganizationGuid: global.__appSIGNALR.SIGNALR_object.USER.OrganizationGuid,
            LoginName: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
            EmployeeGuid: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid
        };
        controller.CountTask(data, rs => {
            this.setState({ ListtabbarBotom: rs });
            this.nextPage(true)
        });
    }
    //#region Lọc ngày 
    Search_day(value) {
        this.setState({ OffSearchDay: false, loadingSearch: true })
        controller.Searchday(value, rs => {
            this.state.StartDate = rs.StartDate.split(" ")[0];
            this.state.EndDate = rs.EndDate.split(" ")[0];
            this.setState({ StartDate: this.state.StartDate, EndDate: this.state.EndDate });
            this.jtableCalendar.StartDateTime = rs.StartDate.split(" ")[0];
            this.jtableCalendar.EndDateTime = rs.EndDate.split(" ")[0];
            this.jtableTasks.StartDate = rs.StartDate.split(" ")[0];
            this.jtableTasks.EndDate = rs.EndDate.split(" ")[0];
            if (this.state.EvenCalendar == true) {
                this.Calendar_GetAll()
            } else {
                this.nextPage(true)
            }
        });
    };
    //#endregion
    //#region Search
    updateSearch = search => {
        if (this.state.EvenKanban == true) {
            this._search_Kanban.Search = search
            this.ListTask_Kanban(true)
        } else if (this.state.EvenCalendar) {
            this.jtableCalendar.Search = search
            this.Calendar_GetAll()
        } else {
            this.jtableTasks.txtSearch = search
            this.nextPage(true)
        }
    }
    //#endregion
    //#region Lấy danh sách Task
    nextPage(status) {
        var list = [];
        if (status === true) {
            this.jtableTasks.Page = 1;
            this.setState({ loadingSearch: true, ListTask: [] });
        } else {
            this.jtableTasks.Page++;
            list = this.state.ListTask;
        }
        var data = {
            OrganizationGuid: global.__appSIGNALR.SIGNALR_object.USER.OrganizationGuid,
            LoginName: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
            EmployeeGuid: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid
        };
        if (this.state.ListtabbarBotom.length === 0) {
            controller.CountTask(data, rs => {
                this.setState({ ListtabbarBotom: rs });
                controller.GetTasks(this.jtableTasks, rs => {
                    for (let i = 0; i < rs.length; i++) {
                        list.push(rs[i]);
                    };
                    this.setState({ ListTask: list, refreshing: false, loading: false, loadingSearch: false, });
                });
            });
        } else {
            controller.GetTasks(this.jtableTasks, rs => {
                for (let i = 0; i < rs.length; i++) {
                    list.push(rs[i]);
                };
                this.setState({ ListTask: list, refreshing: false, loading: false, loadingSearch: false, });
            });
        }
    }
    //#endregion
    //#region Lấy danh sách Calendar
    Calendar_GetAll() {
        this.setState({ ListCalendar: [] });
        controller.Calendar_GetAll(this.jtableCalendar, rs => {
            this.setState({ ListCalendar: rs });
        });
    }
    //#endregion

    //======================================================================VIEW========================================================================
    //#region View Tổng phân biệt các View 
    render() {
        return (
            <TouchableWithoutFeedback
                style={{ flex: 1 }}
                onPress={() => {
                    Keyboard.dismiss();
                }}>
                <View style={AppStyles.container}>
                    {this.state.EvenKanban == true ?
                        <TabBar
                            title={'Quản lý công việc'}
                            BackModuleByCode={'TM'}
                            Formcalendar={true}
                            FormTask={true}
                            CallbackFormTask={(callback) => this.CallbackFormTask(callback)}
                            CallbackFormcalendar={(callback) => this.CallbackFormcalendar(callback)}
                        />
                        :
                        <TabBar
                            title={this.state.EvenCalendar === true ? 'Lịch làm việc' : 'Công việc'}
                            BackModuleByCode={'TM'}
                            FormSearch={true}
                            //Formcalendar={true}
                            addForm={true}
                            CallbackFormSearch={(callback) => this.setState({ EvenFromSearch: callback })}
                            //CallbackFormcalendar={(callback) => this.CallbackFormcalendar(callback)}
                            CallbackFormAdd={() => this.CallbackFormAdd()}
                        />
                    }
                    <Divider />
                    {this.state.EvenKanban == true ? null :
                        <SearchDay Data={new Date().setDate(new Date().getDate() + 1)} CallBackValue={(value) => this.Search_day(value)} />}
                    {this.state.EvenFromSearch == true ?
                        <FormSearch CallbackSearch={(callback) => this.updateSearch(callback)} />
                        : null}
                    {this.state.loading === false ?
                        <View style={{ flex: 6 }}>
                            {/* view lịch */}
                            {this.state.EvenCalendar ? this.state.ListCalendar.length > 0 ? this.CustomCalendar(this.state.ListCalendar)
                                :
                                <View style={[AppStyles.centerAligned]}>
                                    <ImageBackground source={require('../../../images/TM/CalendarNull.jpg')} style={[AppStyles.centerAligned, { width: "100%", height: "70%" }]} />
                                    <Text style={[AppStyles.Titledefault]}>Không tìm thấy lịch làm việc</Text>
                                    <Text style={[AppStyles.Textdefault, { textAlign: 'center', padding: 10 }]}>Hãy tạo lịch làm việc mới để sắp xếp thời gian khoa học cho mình nhé.</Text>
                                    <TouchableOpacity
                                        style={
                                            [AppStyles.centerAligned,
                                            {
                                                marginTop: 20,
                                                backgroundColor: AppColors.ColorAdd,
                                                width: '60%',
                                                padding: 10,
                                                borderRadius: 10
                                            }
                                            ]}
                                        onPress={() => this.CallbackFormAdd()} >
                                        <Text style={[AppStyles.Titledefault, { color: "#fff" }]}>Thêm mới</Text>
                                    </TouchableOpacity>
                                </View>

                                // View kanban
                                : this.state.EvenKanban ?
                                    <Kanban_Component ReloadKanban={this.ReloadKanban} />
                                    //View công việc
                                    : this.state.loading !== true && this.state.loadingSearch !== true ? this.state.ListTask.length > 0 ? this.CustomListAll(this.state.ListTask)
                                        : <View style={[AppStyles.centerAligned]}>
                                            <ImageBackground source={require('../../../images/TM/TaskNull.jpg')} style={[AppStyles.centerAligned, { width: "100%", height: "70%" }]} />
                                            <Text style={[AppStyles.Titledefault]}>Không tìm thấy công việc nào</Text>
                                            <Text style={[AppStyles.Textdefault, { textAlign: 'center', padding: 10 }]}>Hãy tạo công việc mới để phát triển bản thân</Text>
                                            <TouchableOpacity
                                                style={
                                                    [AppStyles.centerAligned,
                                                    {
                                                        marginTop: 20,
                                                        backgroundColor: AppColors.ColorAdd,
                                                        width: '60%',
                                                        padding: 10,
                                                        borderRadius: 10
                                                    }
                                                    ]}
                                                onPress={() => this.CallbackFormAdd()} >
                                                <Text style={[AppStyles.Titledefault, { color: "#fff" }]}>Thêm mới</Text>
                                            </TouchableOpacity>
                                        </View>
                                        : <LoadingComponent backgroundColor={'#fff'} />
                            }
                        </View>
                        :
                        <LoadingComponent backgroundColor={'#fff'} />
                    }
                    {this.state.EvenCalendar || this.state.EvenKanban ?
                        null :
                        this.state.ListtabbarBotom.length > 0 ?
                            <View style={AppStyles.StyleTabvarBottom}>
                                <TabBarBottomCustom
                                    ListData={this.state.ListtabbarBotom}
                                    onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                                />

                            </View>
                            : null
                    }
                    <MenuActionCompoment
                        callback={this.actionMenuCallbackCalendar}
                        data={DataCombobox.DataMenuBottomCalendar}
                        eOpen={this.openMenuCalendar}
                        position={'bottom'}
                    />
                    {this.state.DataMenuBottom.length == 0 ? null :
                        <MenuActionCompoment
                            callback={this.actionMenuCallback}
                            data={this.state.DataMenuBottom}
                            eOpen={this.openMenu}
                            position={'bottom'}
                        />
                    }
                    {/* tab thêm */}
                    <MenuActionCompoment
                        callback={this.CallbackMenuBottom}
                        data={DataCombobox.MenuBottom}
                        nameMenu={'Tuỳ chọn'}
                        eOpen={this.openMenuBottom}
                        position={'bottom'}
                    />
                    {/* ComboboxProcess N */}
                    <ComboboxV2
                        callback={this.ChangeComboboxProcess}
                        data={DataCombobox.ListComboboxProcess_N}
                        eOpen={this.openComboboxProcess_N}
                    />
                    {/* ComboboxProcess P */}
                    <ComboboxV2
                        callback={this.ChangeComboboxProcess}
                        data={DataCombobox.ListComboboxProcess_P}
                        eOpen={this.openComboboxProcess_P}
                    />
                    {/* ComboboxProcess C */}
                    <ComboboxV2
                        callback={this.ChangeComboboxProcess}
                        data={DataCombobox.ListComboboxProcess_C}
                        eOpen={this.openComboboxProcess_C}
                    />
                    {/* ComboboxProcess X */}
                    <ComboboxV2
                        callback={this.ChangeComboboxProcess}
                        data={DataCombobox.ListComboboxProcess_X}
                        eOpen={this.openComboboxProcess_X}
                    />
                    {/* ComboboxProcess D */}
                    <ComboboxV2
                        callback={this.ChangeComboboxProcess}
                        data={DataCombobox.ListComboboxProcess_D}
                        eOpen={this.openComboboxProcess_D}
                    />
                    {/* ComboboxProcess W */}
                    <ComboboxV2
                        callback={this.ChangeComboboxProcess}
                        data={DataCombobox.ListComboboxProcess_W}
                        eOpen={this.openComboboxProcess_W}
                    />
                    {/* ComboboxProcess F */}
                    <ComboboxV2
                        callback={this.ChangeComboboxProcess}
                        data={DataCombobox.ListComboboxProcess_F}
                        eOpen={this.openComboboxProcess_F}
                    />
                    {/* ComboboxProcess E */}
                    <ComboboxV2
                        callback={this.ChangeComboboxProcess}
                        data={DataCombobox.ListComboboxProcess_E}
                        eOpen={this.openComboboxProcess_E}
                    />
                </View>
            </TouchableWithoutFeedback >
        );
    }
    //#endregion
    //#region View Task
    CustomListAll = (item) => (
        <FlatList
            data={item}
            renderItem={({ item, index }) => (
                <TouchableOpacity style={[{ flexDirection: 'column', borderBottomWidth: 0.5, borderBottomColor: AppColors.gray }]}>
                    {item.IsRecall === true ?
                        <TouchableOpacity style={{ position: 'absolute', width: DRIVER.width, height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 18 }}>Đã thu hồi</Text>
                        </TouchableOpacity>
                        : null}
                    {item.ProcessDelete === true ?
                        <TouchableOpacity style={{ position: 'absolute', width: DRIVER.width, height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 18 }}>Đã xoá, chờ xác nhận</Text>
                        </TouchableOpacity>
                        : null}

                    <View style={[(item.IsRecall === true || item.ProcessDelete === true) ? { backgroundColor: "#660000", opacity: 0.05 } : null, { padding: 10, flexDirection: 'row' }]}>
                        {item.CheckParentId == false ?
                            <View style={{ flex: 0.5, alignItems: 'flex-start', justifyContent: 'center' }}>
                                <Icon
                                    name={item.Icon}
                                    type={item.TypeIcon}
                                    size={20}
                                    color={
                                        item.StatusCompletedTaskOfUser === 'X' ?
                                            AppColors.StatusTask_CG
                                            : item.StatusCompletedTaskOfUser === 'N' ?
                                                AppColors.StatusTask_CXL
                                                : item.StatusCompletedTaskOfUser === 'P' ?
                                                    AppColors.StatusTask_DTH
                                                    : item.StatusCompletedTaskOfUser === 'D' ?
                                                        AppColors.StatusTask_HL
                                                        : item.StatusCompletedTaskOfUser === 'C' ?
                                                            AppColors.StatusTask_HT
                                                            : item.StatusCompletedTaskOfUser === 'W' ?
                                                                AppColors.StatusTask_CNK
                                                                : item.StatusCompletedTaskOfUser === 'F' ?
                                                                    AppColors.StatusTask_TD
                                                                    : item.StatusCompletedTaskOfUser === 'E' ?
                                                                        AppColors.StatusTask_QH : AppColors.gray
                                    } />
                            </View>
                            :
                            <View style={{ flex: 0.5, marginLeft: item.StyleparantId.margin, marginRight: 5, borderRightWidth: 1, borderRightColor: item.StatusCompletedTaskOfUser == 'X' ? AppColors.StatusTask_CG : item.StatusCompletedTaskOfUser == 'N' ? AppColors.StatusTask_CXL : item.StatusCompletedTaskOfUser == 'P' ? AppColors.StatusTask_DTH : item.StatusCompletedTaskOfUser == 'D' ? AppColors.StatusTask_HL : item.StatusCompletedTaskOfUser == 'C' ? AppColors.StatusTask_HT : item.StatusCompletedTaskOfUser == 'W' ? AppColors.StatusTask_CNK : item.StatusCompletedTaskOfUser == 'F' ? AppColors.StatusTask_TD : AppColors.gray }}>
                            </View>
                        }
                        <View style={{ flex: 5, flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity style={{ flex: 1 }} onPress={() => this.onViewItem(item.TaskGuid)}>
                                    <Text style={AppStyles.Titledefault}>{item.Subject}</Text>
                                </TouchableOpacity>
                                {((item.IsAcceptTask === "W" || item.IsAcceptTask === "D") && item.LoginName !== global.__appSIGNALR.SIGNALR_object.USER.LoginName) ?
                                    < TouchableOpacity style={{ alignItems: 'flex-end', justifyContent: 'center', padding: 5 }} onPress={() => this.ChangeIsAcceptTask(item)}>
                                        <Icon
                                            name="warning"
                                            type="antdesign"
                                            size={15}
                                            color={AppColors.red} />
                                    </TouchableOpacity>
                                    : <TouchableOpacity style={{ alignItems: 'flex-end', justifyContent: 'center', padding: 5 }} onPress={() => this.onAction(item)}>
                                        <Icon
                                            name="dots-three-horizontal"
                                            type="entypo"
                                            size={15}
                                            color={AppColors.gray} />
                                    </TouchableOpacity>
                                }

                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Progress.Bar
                                    style={{ marginBottom: 0, marginRight: 0 }}
                                    progress={(item.PercentCompleteTaskOfUser / 100)}
                                    height={2}
                                    width={250 - item.StyleparantId.margin}
                                    color={item.StatusCompletedTaskOfUser == 'X' ? AppColors.StatusTask_CG : item.StatusCompletedTaskOfUser == 'N' ? AppColors.StatusTask_CXL : item.StatusCompletedTaskOfUser == 'P' ? AppColors.StatusTask_DTH : item.StatusCompletedTaskOfUser == 'D' ? AppColors.StatusTask_HL : item.StatusCompletedTaskOfUser == 'C' ? AppColors.StatusTask_HT : item.StatusCompletedTaskOfUser == 'W' ? AppColors.StatusTask_CNK : item.StatusCompletedTaskOfUser == 'F' ? AppColors.StatusTask_TD : item.StatusCompletedTaskOfUser == 'E' ? AppColors.StatusTask_QH : AppColors.gray}
                                />
                                <Text style={[AppStyles.Textdefault, {
                                    color: item.StatusCompletedTaskOfUser == 'X' ? AppColors.StatusTask_CG : item.StatusCompletedTaskOfUser == 'N' ? AppColors.StatusTask_CXL : item.StatusCompletedTaskOfUser == 'P' ? AppColors.StatusTask_DTH : item.StatusCompletedTaskOfUser == 'D' ? AppColors.StatusTask_HL : item.StatusCompletedTaskOfUser == 'C' ? AppColors.StatusTask_HT : item.StatusCompletedTaskOfUser == 'W' ? AppColors.StatusTask_CNK : item.StatusCompletedTaskOfUser == 'F' ? AppColors.StatusTask_TD : item.StatusCompletedTaskOfUser == 'E' ? AppColors.StatusTask_QH : AppColors.gray
                                }]}> {item.PercentCompleteTaskOfUser} %</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flexDirection: 'row', flex: 5 }}>
                                    <View style={{ flexDirection: 'row', flex: 1 }}>
                                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }} onPress={() => this.onViewItem(item.TaskGuid)}>
                                            <Icon
                                                name="list"
                                                type="entypo"
                                                size={15}
                                                color={AppColors.gray} />
                                            <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.CountCLDone == null ? 0 : item.CountCLDone}/{item.CheckLists.length}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}
                                            onPress={() => Actions.taskComment({
                                                data: {
                                                    TaskGuid: item.TaskGuid,
                                                    TaskName: item.Subject
                                                }
                                            })}>
                                            <Icon
                                                name="comments-o"
                                                type="font-awesome"
                                                size={15}
                                                color={item.CountComment > 0 ? AppColors.red : AppColors.gray} />
                                            <Text style={[AppStyles.Textdefault, { color: item.CountComment > 0 ? AppColors.red : AppColors.gray }]}> {item.CountComment}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}
                                            onPress={() => Actions.attachmentComponent({
                                                ModuleId: '18',
                                                RecordGuid: item.TaskGuid
                                            })}>
                                            <Icon
                                                name="attachment"
                                                type="entypo"
                                                size={15}
                                                color={AppColors.gray} />
                                            <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.CountAtt}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }} onPress={() => this.UpdateFollow(item)}>
                                            <Icon
                                                name="flag"
                                                type="antdesign"
                                                size={15}
                                                color={item.IsFollow == true ? AppColors.Maincolor : AppColors.gray} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                        <Text style={AppStyles.Textdefault}>{FuncCommon.ConDate(item.EndDate, 0)}</Text>
                                    </View>
                                </View>
                                {item.CheckChild > 0 ?
                                    <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} onPress={() => { item.OpenparantId ? this.onClearParentId(item) : this.onViewParentId(item) }}>
                                        <Icon
                                            name={item.OpenparantId ? "angle-double-down" : "angle-double-right"}
                                            type="font-awesome"
                                            size={15}
                                            color={AppColors.ColorAdd} />
                                    </TouchableOpacity>
                                    :
                                    <View style={{ alignItems: 'flex-end', justifyContent: 'center', flex: 1 }} />
                                }
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            )}
            onEndReached={() => { item.length >= (this.jtableTasks.Page * 20) ? this.nextPage(false) : null }}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.nextPage(true)}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={[AppColors.Maincolor]}
                />
            }
        />
    );
    //#endregion
    //#region View Calendar
    CustomCalendar = (item) => (
        <FlatList
            data={item}
            renderItem={({ item, index }) => (
                <TouchableOpacity style={{ padding: 10, borderBottomColor: AppColors.gray, borderBottomWidth: 0.5 }} onPress={() => this.CalendarViewItem(item.CalendarGuid)}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center' }}>
                            <Text style={AppStyles.Titledefault}>{this.customdateTime(item.StartTime)}</Text>
                            <Text style={AppStyles.Textdefault}>{this.customdateTime(item.EndTime)}</Text>
                        </View>
                        <View style={{
                            flex: 5,
                            flexDirection: 'column',
                            borderLeftWidth: 2,
                            paddingLeft: 10,
                            borderLeftColor: AppColors.StatusTask_CXL
                        }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 5 }}>
                                    <Text style={AppStyles.Titledefault}>{item.Title}</Text>
                                </View>
                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}>
                                    <Icon
                                        name="attachment"
                                        type="entypo"
                                        size={15}
                                        color={AppColors.gray} />
                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.count}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flex: 0.5, alignItems: 'flex-end', justifyContent: 'center' }} onPress={() => this.onActionCalendar(item)}>
                                    <Icon
                                        name="dots-three-horizontal"
                                        type="entypo"
                                        size={15}
                                        color={AppColors.gray} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 3 }}>
                                <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>{this.checkText(item.CalenderContent, 10)}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            )}
            // onEndReached={() => this.loadMoreData()}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['black']}
                />
            }
        />
    )
    //#endregion
    //============================================================= Chức năng dùng chung ===============================================================
    //#region 
    //#region Dẫn đến form add của các module
    CallbackFormAdd() {
        this.state.EvenCalendar ?
            Actions.calendarAdd({ TypeForm: 'add' })
            :
            Actions.taskAdd({ TypeForm: 'add' })
    }
    //#endregion
    //#region Độ dài văn bản
    checkText(str, i) {
        try {
            if (str !== null && str !== "") {
                var splitted = str.split(" ");
                var strreturn = "";
                var j = 0;
                for (var a = 0; a < splitted.length; a++) {
                    j = j + 1;
                    if (j == i) {
                        strreturn = strreturn + " " + splitted[a] + "...";
                        break
                    }
                    else {
                        strreturn = strreturn + " " + splitted[a];
                    }
                }
                return strreturn.trim();
            } else {
                return "Không có nội dung"
            }
        } catch (error) {
            return "Không có nội dung"
        }
    }
    //#endregion
    //#region  Tuỳ chỉnh thời gian
    customdateTime(DataTime) {
        if (DataTime != null) {
            var date = FuncCommon.ConDate(DataTime, 1)
            var time = date.split(" ");
            return time[1]
        } else {
            return "";
        }
    }
    //#endregion
    //#endregion

    //============================================================ Chức năng của tab Tasks =============================================================
    //#region  
    CallbackFormTask = (cal) => {
        this.setState({ EvenKanban: false });
        this.nextPage(true)
    }
    //#region Chức năng của menu bottom
    CallbackValueBottom = (value) => {
        this.setState({ Process: value });
        switch (value) {
            case 'T':
                this.onActionMenuBottom();
                break;
            case 'N':
                this.ActionComboboxProcess_N();
                break;
            case 'P':
                this.ActionComboboxProcess_P();
                break;
            case 'C':
                this.ActionComboboxProcess_C();
                break;
            case 'X':
                this.ActionComboboxProcess_X();
                break;
            case 'D':
                this.ActionComboboxProcess_D();
                break;
            case 'W':
                this.ActionComboboxProcess_W();
                break;
            case 'FOLLOW':
                this.ActionComboboxProcess_F();
                break;
            case 'E':
                this.ActionComboboxProcess_E();
                break;
            case 'S':
                this.ActionComboboxProcess_S();
                break;
            case 'Kanban':
                this.setState({ EvenKanban: true })
                break;
            default:
                break;
        }
    };
    //tab thêm -- hiển thị thêm nhiều tìm kiếm
    _openMenuBottom() { }
    openMenuBottom = (d) => {
        this._openMenuBottom = d;
    }
    onActionMenuBottom() {
        this._openMenuBottom();
    }
    CallbackMenuBottom = (d) => {
        this.CallbackValueBottom(d);
    }
    //#endregion
    //#region openComboboxProcess
    _openComboboxProcess_N() { };
    _openComboboxProcess_P() { };
    _openComboboxProcess_C() { };
    _openComboboxProcess_X() { };
    _openComboboxProcess_D() { };
    _openComboboxProcess_W() { };
    _openComboboxProcess_F() { };
    _openComboboxProcess_E() { };
    _openComboboxProcess_S() { };
    openComboboxProcess_N = (d) => { this._openComboboxProcess_N = d; };
    openComboboxProcess_P = (d) => { this._openComboboxProcess_P = d; };
    openComboboxProcess_C = (d) => { this._openComboboxProcess_C = d; };
    openComboboxProcess_X = (d) => { this._openComboboxProcess_X = d; };
    openComboboxProcess_D = (d) => { this._openComboboxProcess_D = d; };
    openComboboxProcess_W = (d) => { this._openComboboxProcess_W = d; };
    openComboboxProcess_F = (d) => { this._openComboboxProcess_F = d; };
    openComboboxProcess_E = (d) => { this._openComboboxProcess_E = d; };
    openComboboxProcess_S = (d) => { this._openComboboxProcess_S = d; };
    ActionComboboxProcess_N() { this._openComboboxProcess_N(); };
    ActionComboboxProcess_P() { this._openComboboxProcess_P(); };
    ActionComboboxProcess_C() { this._openComboboxProcess_C(); };
    ActionComboboxProcess_X() { this._openComboboxProcess_X(); };
    ActionComboboxProcess_D() { this._openComboboxProcess_D(); };
    ActionComboboxProcess_W() { this._openComboboxProcess_W(); };
    ActionComboboxProcess_F() { this._openComboboxProcess_F(); };
    ActionComboboxProcess_E() { this._openComboboxProcess_E(); };
    ActionComboboxProcess_S() { this._openComboboxProcess_S(); };
    ChangeComboboxProcess = (rs) => {
        if (rs.value !== null) {
            this.jtableTasks.Process = this.state.Process
            switch (rs.value) {
                case '1':
                    this.setState({ loadingSearch: true })
                    this.jtableTasks.StartDate = FuncCommon.ConDate(new Date(), 2),
                        this.jtableTasks.EndDate = FuncCommon.ConDate(new Date(), 2),
                        this.nextPage(true)
                    break;
                case '2':
                    this.setState({ OffSearchDay: true, loadingSearch: true })
                    this.jtableTasks.StartDate = null;
                    this.jtableTasks.EndDate = null;
                    this.nextPage(true)
                    break;
                default:
                    break;
            }
        }
    }
    //#endregion
    //#region Dẫn đến form chi tiết
    onViewItem(item) {
        Actions.taskDetail({ RecordGuid: item })
    }
    //#endregion
    //#region Bật chế độ ghim theo dõi
    UpdateFollow = (item) => {
        controller.UpdateFollow(item, rs => {
            var _list = this.state.ListTask;
            for (let i = 0; i < _list.length; i++) {
                if (_list[i].TaskGuid == item.TaskGuid) {
                    _list[i].IsFollow = !_list[i].IsFollow
                }
            }
            this.setState({ ListTask: _list });
        });
    }
    //#endregion
    //#region Chức năng trong menu Task
    actionMenuCallback = (d) => {
        switch (d) {
            case 'details':
                Actions.taskDetail({ RecordGuid: this.state.selectItem.TaskGuid })
                break;
            case 'edit':
                Actions.taskAdd({ TypeForm: 'edit', data: { TaskGuid: this.state.selectItem.TaskGuid } })
                break;
            case 'delete':
                Alert.alert(
                    'Xác nhận thông tin',
                    'Bạn có muốn xóa ' + this.state.selectItem.Subject + ' không?',
                    [
                        {
                            text: 'Xác nhận',
                            onPress: () => this.DeleteTask(this.state.selectItem)
                        },
                        { text: 'Huỷ', onPress: () => console.log('No Pressed') },
                    ],
                    { cancelable: true }
                );
                break;
            case 'comments':
                Actions.taskComment({
                    data: {
                        TaskGuid: this.state.selectItem.TaskGuid,
                        TaskName: this.state.selectItem.Subject
                    }
                })
                break;
            case 'attachment':
                var obj = {
                    ModuleId: '18',
                    RecordGuid: this.state.selectItem.TaskGuid
                }
                Actions.attachmentComponent(obj);
                break;
            case 'Calendar':
                this.InsertToCalenderOfUser(this.state.selectItem)
                break;
            case 'UpdateProcess':
                Actions.taskUpdateProcess({ data: { TaskGuid: this.state.selectItem.TaskGuid } });
                break;
            case "Recall":
                Alert.alert(
                    'Xác nhận thông tin',
                    'Yêu cầu thu hồi công việc [ ' + this.state.selectItem.Subject + ' ]',
                    [
                        {
                            text: 'Bắt đầu thu hồi',
                            onPress: () => this.RecallTask(this.state.selectItem)
                        },
                        { text: 'Bỏ qua', onPress: () => console.log('No Pressed') },
                    ],
                    { cancelable: true }
                );
                break;
            default:
                break;
        }
    };
    //#endregion
    //#region Xoá 1 bản ghi task
    DeleteTask = (item) => {
        controller.DeleteTask(item, rs => {
            this.updateSearch('');
        });
    };
    //#endregion
    //#region thu hồi 1 bản ghi task
    RecallTask = (item) => {
        controller.RecallTask(item, rs => {
            this.updateSearch('');
        });
    };
    //#endregion
    //#region thêm mới vào Calendar
    InsertToCalenderOfUser = (item) => {
        controller.InsertToCalenderOfUser(item);
    };
    //#endregion
    //#region hiển thị công viêc con
    onViewParentId(item) {
        controller.onViewParentId(item, this.state.ListTask, rs => {
            this.setState({ ListTask: rs });
        });
    };
    onClearParentId(item) {
        controller.onClearParentId(item, this.state.ListTask, rs => {
            this.setState({ ListTask: rs });
        });
    };
    //#endregion
    //#region Mở dropdow
    _openMenu() { }
    openMenu = (d) => {
        this._openMenu = d;
    }
    onAction(item) {
        var rs = item.EmpPermissionCustom;
        if (rs !== null) {
            var DataMenuBottom = [
                rs.IsView !== true ? null : {
                    key: 'details',
                    name: 'Xem',
                    icon: {
                        name: 'eye-outline',
                        type: 'material-community',
                        color: AppColors.Maincolor,
                    }
                },
                rs.IsUpdate !== true ? null : {
                    key: 'edit',
                    name: 'Sửa',
                    icon: {
                        name: 'edit',
                        type: 'antdesign',
                        color: AppColors.Maincolor,
                    }
                },
                rs.IsView !== true ? null : {
                    key: 'attachment',
                    name: 'Đính kèm',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                rs.IsComment !== true ? null : {
                    key: 'comments',
                    name: 'Ý kiến',
                    icon: {
                        name: 'message1',
                        type: 'antdesign',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    key: 'Calendar',
                    name: 'Đặt lịch',
                    icon: {
                        name: 'calendar-check-o',
                        type: 'font-awesome',
                        color: AppColors.Maincolor,
                    }
                },
                rs.IsDelete !== true ? null : {
                    key: 'Recall',
                    name: 'Thu hồi',
                    icon: {
                        name: 'arrow-down',
                        type: 'feather',
                        color: AppColors.Maincolor,
                    }
                },
                rs.IsDelete !== true ? null : {
                    key: 'delete',
                    name: 'Xóa',
                    icon: {
                        name: 'delete',
                        type: 'antdesign',
                        color: AppColors.Maincolor,
                    }
                },
                rs.IsCheck !== true ? null : {
                    key: 'UpdateProcess',
                    name: 'Cập nhật tiến độ',
                    icon: {
                        name: 'check-circle',
                        type: 'feather',
                        color: AppColors.Maincolor,
                    }
                }
            ];
            var list = [];
            for (let i = 0; i < DataMenuBottom.length; i++) {
                if (DataMenuBottom[i] !== null) {
                    list.push(DataMenuBottom[i]);
                }
            }
            this.setState({
                selectItem: item,
                DataMenuBottom: list
            }, () => {
                this._openMenu();
            });
        }
    }
    //#endregion
    //#region mở form xác nhận
    ChangeIsAcceptTask(item) {
        Alert.alert(
            'Chờ xác nhận',
            'Xác nhận công việc được giao: [' + item.Subject + ']',
            [
                {
                    text: 'Nhận việc',
                    onPress: () => this.UpdateIsAcceptTask(item, "Y"),
                    style: { color: 'red' }
                },
                {
                    text: 'Không nhận',
                    onPress: () => this.UpdateIsAcceptTask(item, "N"),
                    style: 'destructive'
                },
                {
                    text: 'Để sau',
                    onPress: () => this.UpdateIsAcceptTask(item, "D"),
                    style: "cancel"
                }
            ],
            { cancelable: true }
        );
    }

    UpdateIsAcceptTask = (item, stt) => {
        var obj = { IdS: [item.TaskGuid, stt] };
        controller.UpdateAccept(obj, rs => {
            var list = this.state.ListTask;
            var view = [];
            if (stt === "N") {
                for (let i = 0; i < list.length; i++) {
                    if (item.TaskGuid !== list[i].TaskGuid) {
                        view.push(list[i]);
                    }
                }
            } else {
                for (let i = 0; i < list.length; i++) {
                    if (item.TaskGuid === list[i].TaskGuid) {
                        list[i].IsAcceptTask = stt;
                    }
                    view.push(list[i]);
                }
            }
            this.setState({ ListTask: view });
        });
    };
    //#endregion
    //#endregion

    //=========================================================== Chức năng của tab Calendar ===========================================================
    //#region 
    //#region Bật tắt form Calendar
    CallbackFormcalendar = (value) => {
        this.setState({ EvenCalendar: value })
        if (value == true) {
            this.Calendar_GetAll()
        }
    }
    //#endregion
    //#region Dẫn đến view chi tiết Calendar
    CalendarViewItem = (value) => {
        Actions.calendarDetail({ data: { CalendarGuid: value } })
    }
    //#endregion
    //#region Menu chọn chức năng Calendar
    _openMenuCalendar() { }
    openMenuCalendar = (d) => {
        this._openMenuCalendar = d;
    }
    onActionCalendar(item) {
        this.state.NameMenuCalendar = item.Title
        this.setState({
            selectItemCalendar: item,
            NameMenuCalendar: item.Title
        })
        this._openMenuCalendar();
    }
    //#endregion
    //#region Chức năng trong menu Calendar
    actionMenuCallbackCalendar = (d) => {
        switch (d) {
            case 'details':
                Actions.calendarDetail({ data: { CalendarGuid: this.state.selectItemCalendar.CalendarGuid } })
                break;
            case 'edit':
                Actions.calendarAdd({ TypeForm: 'edit', data: { CalendarGuid: this.state.selectItemCalendar.CalendarGuid } })
                break;
            case 'delete':
                Alert.alert(
                    'Thông báo',
                    'Bạn có muốn xóa ' + this.state.selectItemCalendar.Title + ' không?',
                    [
                        {
                            text: 'Xác nhận',
                            onPress: () => this.DeleteCalendar(this.state.selectItemCalendar.CalendarGuid)
                        },
                        { text: 'Huỷ', onPress: () => console.log('No Pressed') },
                    ],
                    { cancelable: true }
                );
                break;
            case 'attachment':
                var obj = {
                    ModuleId: '20',
                    RecordGuid: this.state.selectItemCalendar.CalendarGuid
                }
                Actions.attachmentComponent(obj);
                break;
            default:
                break;
        }
    }
    //#endregion
    //#region Xoá 1 bản ghi calendar
    DeleteCalendar = (item) => {
        var obj = {
            Id: item
        }
        API_TM_CALENDAR.Calendar_Delete(obj).then(rs => {
            this.Calendar_GetAll();
        }).catch(error => {
            console.log('Error when call API Mobile.');
        });
    }
    //#endregion
    //#endregion

}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Task_ListAll_Component);
