import React, {Component} from 'react';
import {
  Keyboard,
  Text,
  KeyboardAvoidingView,
  View,
  TouchableOpacity,
  Alert,
  ImageBackground,
  TextInput,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Divider, CheckBox, Icon} from 'react-native-elements';
import {API_TM_TASKS, API_HR} from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import {AppStyles, AppColors, AppSizes} from '@theme';
import _ from 'lodash';
import {ScrollView} from 'react-native-gesture-handler';
import {FuncCommon} from '../../../utils';
import Combobox from '../../component/Combobox';
import DatePicker from 'react-native-datepicker';
import LoadingComponent from '../../component/LoadingComponent';
const ListStatusCompleted = [
  {
    text: 'Chờ giao',
    value: 'X',
    colorStt: AppColors.StatusTask_CG,
  },
  {
    text: 'Chờ xử lý',
    value: 'N',
    colorStt: AppColors.StatusTask_CXL,
  },
  {
    text: 'Đang thực hiện',
    value: 'P',
    colorStt: AppColors.StatusTask_DTH,
  },
  {
    text: 'Hoàn thành',
    value: 'C',
    colorStt: AppColors.StatusTask_HT,
  },
  {
    text: 'Chờ người khác',
    value: 'W',
    colorStt: AppColors.StatusTask_CNK,
  },
  {
    text: 'Hoãn lại',
    value: 'D',
    colorStt: AppColors.StatusTask_HL,
  },
];
class Task_UpdateProcess_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DataView: null,
      StatusCompleted_Text: '',
      colorStt: '',
      PercentComplete: '',
      TimeReminder: new Date(),
      Note: '',
    };
  }
  componentDidMount() {
    this.GetData();
  }
  //#region lấy dữ liệu
  GetData() {
    var obj = {
      TaskGuid: this.props.data.TaskGuid,
    };
    API_TM_TASKS.GetItemTasks(obj)
      .then(res => {
        var _data = JSON.parse(res.data.data);
        this.setState({
          DataView: _data,
          PercentComplete: _data.PercentCompleteTaskOfUser.toString(),
          TimeReminder: new Date(_data.TimeReminder) ?? new Date(),
          Note: _data.Note,
        });
      })
      .catch(error => {
        console.log('Error when call API Mobile.');
      });
  }
  //#endregion
  render() {
    return this.state.DataView !== null ? (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container, {backgroundColor: null}]}>
          <TabBar_Title
            title={'Cập nhật tiến độ công việc'}
            callBack={() => this.callBackList()}
          />
          <Divider />s
          {this.CustomView(this.state.DataView)}
        </View>
        <Combobox
          value={this.state.DataView.StatusCompleted}
          TypeSelect={'single'} // single or multiple
          callback={this.ChangeStatus}
          data={ListStatusCompleted}
          nameMenu={'chọn trạng thái'}
          eOpen={this.openCombobox_Status}
          position={'bottom'}
        />
      </KeyboardAvoidingView>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  CustomView = para => (
    <ScrollView
      style={{flexDirection: 'column', backgroundColor: AppColors.white}}>
      {/* Tên công việc */}
      <View style={{padding: 10, paddingBottom: 0}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Tên công việc</Text>
          <Text style={{color: 'red'}} />
        </View>
        <View style={AppStyles.FormInput}>
          <Text style={[AppStyles.TextInput]}>{para.Subject}</Text>
        </View>
      </View>
      <View style={{padding: 10, paddingBottom: 0}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Trạng thái</Text>
          <Text style={{color: 'red'}} />
        </View>
        <TouchableOpacity
          style={[AppStyles.FormInput]}
          onPress={() => this.onActionCombobox_Status()}>
          <Text style={[AppStyles.TextInput, {color: this.state.colorStt}]}>
            {this.state.StatusCompleted_Text}
          </Text>
        </TouchableOpacity>
      </View>
      {/* Hoàn thành*/}
      <View style={{padding: 10, paddingBottom: 0}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Hoàn thành (%)</Text>
          <Text style={{color: 'red'}}> *</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          value={this.state.PercentComplete}
          // autoCapitalize="none"
          keyboardType={'numeric'}
          maxLength={3}
          onChangeText={text => this.setText(text)}
        />
      </View>
      <View style={{padding: 10, paddingBottom: 0, flexDirection: 'column'}}>
        <View style={{flexDirection: 'row', marginBottom: 5}}>
          <Text style={AppStyles.Labeldefault}>Nhắc xử lý</Text>
          <Text style={{color: 'red'}} />
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={{width: '100%'}}>
            <DatePicker
              locale="vie"
              locale="vie"
              locale={'vi'}
              style={{width: '100%'}}
              date={this.state.TimeReminder}
              mode="date"
              androidMode="spinner"
              placeholder="Chọn ngày nhắc xử lý"
              format="DD/MM/YYYY"
              confirmBtnText="Xong"
              cancelBtnText="Huỷ"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 0,
                  top: 4,
                  marginLeft: 0,
                },
                dateInput: {
                  marginRight: 36,
                },
              }}
              onDateChange={date => this.setEndDate(date)}
            />
          </View>
        </View>
      </View>
      {/* Nội dung */}
      <View style={{padding: 10, paddingBottom: 0}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Nội dung</Text>
          <Text style={{color: 'red'}} />
        </View>
        <TextInput
          style={[AppStyles.FormInput, {maxHeight: 100}]}
          underlineColorAndroid="transparent"
          value={this.state.Note}
          numberOfLines={5}
          multiline={true}
          onChangeText={text => this.setState({Note: text})}
        />
      </View>
      <View style={{flexDirection: 'row', padding: 10, marginBottom: 20}}>
        {/* cancel*/}
        <TouchableOpacity
          style={[
            AppStyles.containerCentered,
            {
              flex: 1,
              padding: 10,
              borderRadius: 10,
              borderColor: AppColors.ColorPrimary,
              borderWidth: 0.5,
              marginRight: 5,
            },
          ]}
          onPress={() => this.Cancel()}>
          <Text
            style={[AppStyles.Titledefault, {color: AppColors.ColorPrimary}]}>
            Huỷ bỏ
          </Text>
        </TouchableOpacity>
        {/* Update*/}
        <TouchableOpacity
          style={[
            AppStyles.containerCentered,
            {
              flex: 1,
              padding: 10,
              borderRadius: 10,
              borderColor: AppColors.ColorEdit,
              borderWidth: 0.5,
              marginLeft: 5,
            },
          ]}
          onPress={() => this.Update()}>
          <Text style={[AppStyles.Titledefault, {color: AppColors.ColorEdit}]}>
            Cập nhật
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
  //#region  combobox Status
  _openCombobox_Status() {}
  openCombobox_Status = d => {
    this._openCombobox_Status = d;
  };
  onActionCombobox_Status() {
    this._openCombobox_Status();
  }
  ChangeStatus = rs => {
    this.state.DataView.StatusCompleted = rs.value;
    this.setState({
      colorStt: rs.colorStt,
      StatusCompleted_Text: rs.text,
    });
    switch (rs.value) {
      case 'C':
        this.state.PercentComplete = '100';
        break;
      case 'P':
        this.state.PercentComplete = '50';
        break;
      case 'N':
        this.state.PercentComplete = '0';
        break;
      default:
        break;
    }
  };
  //#endregion
  setEndDate = date => {
    var splipdate = date.split(' ');
    var cvDate = splipdate[0].split('/');
    var _date = cvDate[2] + '-' + cvDate[1] + '-' + cvDate[0];
    var _dateView = cvDate[0] + '-' + cvDate[1] + '-' + cvDate[2];
    this.state.DataView.TimeReminder = _date;
    this.setState({TimeReminder: _dateView});
  };
  setText = text => {
    var _text = parseInt(text);
    if (_text > 0 && _text < 100) {
      this.state.DataView.StatusCompleted = 'P';
      this.setState({
        colorStt: AppColors.StatusTask_DTH,
        StatusCompleted_Text: 'Đang thực hiện',
      });
      this.setState({PercentComplete: text});
    } else if (_text === 100) {
      this.state.DataView.StatusCompleted = 'C';
      this.setState({
        colorStt: AppColors.StatusTask_HT,
        StatusCompleted_Text: 'Hoàn thành',
      });
      this.setState({PercentComplete: text});
    } else if (_text > 100) {
      Alert.alert('Chú ý', '% hoàn thành từ 0 - 100%');
    } else {
      this.state.DataView.StatusCompleted = 'N';
      this.setState({
        colorStt: AppColors.StatusTask_CXL,
        StatusCompleted_Text: 'Chờ xử lý',
      });
      this.setState({PercentComplete: text});
    }
  };

  Update() {
    var obj = {
      TaskGuid: this.state.DataView.TaskGuid,
      Subject: this.state.DataView.Subject,
      PercentComplete: this.state.PercentComplete,
      StatusCompleted: this.state.DataView.StatusCompleted,
      TimeReminder: this.state.DataView.TimeReminder,
      Note: this.state.Note,
      StatusType: 2,
    };

    let _data = new FormData();
    _data.append('update', JSON.stringify(obj));
    // console.log(_data);
    API_TM_TASKS.UpdateProcess(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          console.log(_rs);
        } else {
          this.callBackList();
          Alert.alert(
            'Thông báo',
            'Cập nhật tiến độ thành công',
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }
  Cancel() {
    Alert.alert(
      'Thông báo',
      'Bạn có chắc chắn huỷ bỏ không?',
      [
        {
          text: 'Xác nhận',
          onPress: () => this.callBackList(),
        },
        {text: 'Huỷ', onPress: () => console.log('No Pressed')},
      ],
      {cancelable: true},
    );
  }
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Task_UpdateProcess_Component);
