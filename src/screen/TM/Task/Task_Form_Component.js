import React, { Component } from 'react';
import {
  Keyboard,
  Dimensions,
  Text,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  PermissionsAndroid,
  Alert,
  TextInput,
  Image,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Divider, Icon } from 'react-native-elements';
import { API_TM_TASKS, API } from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import { AppStyles, AppColors } from '@theme';
import _, { update, values } from 'lodash';
import styles from '../TasksStyle';
import DatePicker from 'react-native-date-picker';
import { FuncCommon } from '../../../utils';
import Combobox from '../../component/Combobox';
import LoadingComponent from '../../component/LoadingComponent';
import configApp from '../../../configApp';
import ComboboxV2 from '../../component/ComboboxV2';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import Toast from 'react-native-simple-toast';
import { DataCombobox,  } from './DataCombobox';
import { controller } from './controller';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

class Task_Form_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      Emp: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
      ViewAll: false, // hiển thị chi tiết để thêm mới
      FormAddChecklist: false, // Tắt/Bật form thêm check list
      TypeForm:
        props.TypeForm !== null && props.TypeForm !== undefined
          ? props.TypeForm
          : 'add', // kiểm tra là form Add hay form Edit
      EventSave: false, // sự kiện update để bắt lỗi
      ListTreeDataCOT: [], // Danh sách nhóm công việc
      ListComboboxApproval: [], // Danh sách nhân viên chủ trì
      ListComboboxProcess: [], // Danh sách nhân viên phối hợp
      ListComboboxFollow: [], // Danh sách nhân viên theo dõi
      IsSendApproval: false, // Trạng thái có gửi mail cho người chủ trì không (Form add)
      IsSendProcess: false, // Trạng thái có gửi mail cho người phối hợp không (Form add)
      IsSendFollow: false, // Trạng thái có gửi mail cho người theo dõi không (Form add)
      ListUserApproval: [],
      ListUserProcess: [],
      ListUserFollow: [],
      ListApproval: [],
      ListProcess: [],
      ListFollow: [],
      ApprovalName: [],
      ProcessName: [],
      FollowName: [],
      OldAtt: [],
      setEventStartDate: false,
      setEventEndDate: false,
      Attachment: [],
      CategoryName: '',
      PriorityName: '',
      CheckList: [],
      listEmp: [],
      // Label: {
      //     LabelId: "",
      //     LabelName: "",
      //     Class: "",
      // },
      Label: [],
      ListLabel: [],
      //dữ liệu
      DataUp: {
        DepartmentGuid: global.__appSIGNALR.SIGNALR_object.USER.DepartmentGuid,
        OrganizationGuid:
          global.__appSIGNALR.SIGNALR_object.USER.OrganizationGuid,
        TaskGuid:
          this.props.data !== undefined
            ? this.props.data.TaskGuid !== undefined
              ? this.props.data.TaskGuid
              : null
            : null,
        Subject: '',
        TaskContent: '',
        StartDate: new Date(),
        EndDate: new Date(),
        CategoryOfTaskGuid: '',
        Priority: '',
        StatusCompleted:
          props.StatusCompleted !== undefined ? props.StatusCompleted : 'N',
        ListApproval: [],
        ListProcess: [],
        ListFollow: [],
        ApprovalName: [],
        ProcessName: [],
        FollowName: [],
      },
    };
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    if (this.state.TypeForm == 'edit') {
      this.GetDataEdit(this.props.data.TaskGuid);
    } else {
      this.GetTreeDataCOT();
    }
  }
  //#region hàm lấy dữ liệu form edit
  GetDataEdit(para) {
    controller.GetItemTasks(para, rs => {
      this.setState({
        Label: rs.DataUp.Labels || [],
        DataUp: {
          ...rs.DataUp,
          StartDate: FuncCommon.ConDate(rs.DataUp.StartDate, 3, 'iso'),
          EndDate: FuncCommon.ConDate(rs.DataUp.EndDate, 3, 'iso')
        },
        CheckList: rs.CheckList,
        ListApproval: rs._listUserApproval,
        ListUserApproval: rs._listUserApproval,
        ListProcess: rs._listUserProcess,
        ListUserProcess: rs._listUserProcess,
        ListFollow: rs._listUserFollow,
        ListUserFollow: rs._listUserFollow,
        loading: false,
      });
      this.GetAttListEdit();
    });
  }
  GetAttListEdit() {
    controller.GetAttList(this.props.data.TaskGuid, rs => {
      this.setState({ Attachment: rs, OldAtt: JSON.parse(JSON.stringify(rs)) });
    });
    this.GetTreeDataCOT();
  }
  //#endregion
  //#region lấy dữ liệu nhóm công việc
  GetTreeDataCOT() {
    controller.GetTreeDataCOT(null, rs => {
      this.setState({ ListTreeDataCOT: rs });
      if (this.state.DataUp.CategoryOfTaskGuid == '') {
        this.state.DataUp.CategoryOfTaskGuid = rs.length > 0 ? rs[0].value : '';
      }
      this.setState({ DataUp: this.state.DataUp });
      this.GetLabels();
    });
  }
  //#endregion
  //#region lấy danh sách nhân viên
  ListCombobox_Employee() {
    controller.GetEmployeeByCategoryOfTask(
      this.state.DataUp.CategoryOfTaskGuid,
      rs => {
        this.setState({
          ListComboboxApproval: rs.ListComboboxApproval,
          ListComboboxProcess: rs.ListComboboxProcess,
          ListComboboxFollow: rs.ListComboboxFollow,
        });
      },
    );
  }
  //#endregion
  //#region lấy danh sách nhãn công việc
  GetLabels() {
    controller.GetLabels(null, rs => {
      this.setState({ ListLabel: rs, loading: false });
    });
  }
  //#endregion

  //#region View tổng
  render() {
    const { isEdit, ApplyOvertimeId } = this.state;
    const { itemData } = this.props;
    const IS_CB = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'CB';
    return this.state.loading !== true ? (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{ flex: 1 }}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={[AppStyles.container]}>
            {this.state.TypeForm == 'add' ? (
              <TabBar_Title
                title={'Thêm mới công việc'}
                callBack={() => this.callBackList(null)}
              />
            ) : this.state.TypeForm == 'edit' ? (
              <TabBar_Title
                title={this.state.DataUp.Subject}
                callBack={() => this.callBackList(null)}
              />
            ) : null}
            <Divider />
            {this.CustomForm(this.state.DataUp)}

            {this.state.ListTreeDataCOT.length > 0 ? (
              <Combobox
                value={
                  this.state.TypeForm === 'edit' &&
                    this.state.DataUp.CategoryOfTaskGuid !== null
                    ? this.state.DataUp.CategoryOfTaskGuid
                    : this.state.ListTreeDataCOT[0].value
                }
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeCategoryOfTask}
                data={this.state.ListTreeDataCOT}
                nameMenu={'Chọn nhóm công việc'}
                eOpen={this.openComboboxCategory}
                position={'bottom'}
              />
            ) : null}
            {/* Approval */}
            {this.state.ListComboboxApproval.length > 0 ? (
              <Combobox
                value={this.state.ListApproval}
                TypeSelect={'multiple'} // single or multiple
                callback={this.ChoiceEmployeeApproval}
                data={this.state.ListComboboxApproval}
                nameMenu={'Chọn người chủ trì'}
                eOpen={this.openCombobox_Approval}
                position={'bottom'}
              />
            ) : null}
            {/* Process */}
            {this.state.ListComboboxProcess.length > 0 ? (
              <Combobox
                value={this.state.ListProcess}
                TypeSelect={'multiple'} // single or multiple
                callback={this.ChoiceEmployeeProcess}
                data={this.state.ListComboboxProcess}
                nameMenu={'Chọn người phối hợp'}
                eOpen={this.openCombobox_Process}
                position={'bottom'}
              />
            ) : null}
            {/* Follow */}
            {this.state.ListComboboxFollow.length > 0 ? (
              <Combobox
                value={this.state.ListFollow}
                TypeSelect={'multiple'} // single or multiple
                callback={this.ChoiceEmployeeFollow}
                data={this.state.ListComboboxFollow}
                nameMenu={'Chọn người theo dõi'}
                eOpen={this.openCombobox_Follow}
                position={'bottom'}
              />
            ) : null}
            {this.state.ListLabel.length > 0 ? (
              <Combobox
                value={this.state.Label.map(x => x.LabelId)}
                TypeSelect={'multiple'}
                callback={this.ChoiceLabel}
                data={this.state.ListLabel}
                nameMenu={'Chọn nhãn công việc'}
                eOpen={this.openComboboxLabel}
                position={'bottom'}
              />
            ) : null}
            <Combobox
              value={
                this.state.DataUp.Priority !== ''
                  ? this.state.DataUp.Priority
                  : 'N'
              }
              TypeSelect={'single'} // single or multiple
              callback={this.ChangePriority}
              data={DataCombobox.Level}
              nameMenu={'Chọn mức độ ưu tiên'}
              eOpen={this.openCombobox_Priority}
              position={'bottom'}
            />
            <ComboboxV2
              callback={this.ChoiceAtt}
              data={DataCombobox.ListComboboxAtt}
              eOpen={this.openCombobox_Att}
            />
            <OpenPhotoLibrary
              callback={this.callbackLibarary}
              openLibrary={this.openLibrary}
            />
            {this.state.setEventStartDate === true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 100,
                  width: DRIVER.width,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({ setEventStartDate: false });
                  }}>
                  <Text style={{ textAlign: 'right' }}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={this.state.DataUp.StartDate}
                  mode="date"
                  style={{ width: DRIVER.width }}
                  onDateChange={setDate => this.setStartDate(setDate)}
                />
              </View>
            ) : null}
            {this.state.setEventEndDate === true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 100,
                  width: DRIVER.width,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({ setEventEndDate: false });
                  }}>
                  <Text style={{ textAlign: 'right' }}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={this.state.DataUp.EndDate}
                  mode="date"
                  style={{ width: DRIVER.width }}
                  onDateChange={setDate => this.setEndDate(setDate)}
                />
              </View>
            ) : null}
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  //#endregion

  //#region  form
  CustomForm = () => (
    <ScrollView horizontal={false} style={{ flexDirection: 'column' }}>
      {/* Tên công việc */}
      <View style={{ padding: 10, paddingBottom: 0 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={AppStyles.Labeldefault}>Tên công việc</Text>
          <Text style={{ color: 'red' }}> *</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          value={this.state.DataUp.Subject}
          placeholder={'Nhập tên công việc . . .'}
          // autoFocus={true}
          autoCapitalize="none"
          onChangeText={text => this.setSubject(text)}
        />
        {this.state.DataUp.Subject == '' && this.state.EventSave == true ? (
          <View style={{ flexDirection: 'row' }}>
            <Text style={AppStyles.TextInputError}>
              Yêu cầu nhập tên công việc
            </Text>
          </View>
        ) : null}
      </View>

      {/* Nội dung */}
      {this.state.TypeForm === 'edit' ? null : (
        <View style={{ padding: 10, paddingBottom: 0 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={AppStyles.Labeldefault}>Nội dung công việc</Text>
            <Text style={{ color: 'red' }} />
          </View>
          <TextInput
            style={[AppStyles.FormInput, { maxHeight: 100 }]}
            underlineColorAndroid="transparent"
            value={this.state.DataUp.TaskContent}
            placeholder="Nhập nội dung công việc . . ."
            numberOfLines={5}
            multiline={true}
            onChangeText={text => this.setTaskContent(text)}
          />
        </View>
      )}
      <View style={{ padding: 5, paddingBottom: 0, flexDirection: 'row' }}>
        <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
          <Text style={AppStyles.Labeldefault}>Ngày bắt đầu</Text>
          <TouchableOpacity
            style={[AppStyles.FormInput, { justifyContent: 'center' }]}
            onPress={() =>
              this.setState({ setEventStartDate: true, setEventEndDate: false })
            }>
            <Text>{FuncCommon.ConDate(this.state.DataUp.StartDate, 0)}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
          <Text style={AppStyles.Labeldefault}>Ngày đến hạn</Text>
          <TouchableOpacity
            style={[AppStyles.FormInput, { justifyContent: 'center' }]}
            onPress={() =>
              this.setState({ setEventStartDate: false, setEventEndDate: true })
            }>
            <Text>{FuncCommon.ConDate(this.state.DataUp.EndDate, 0)}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ flexDirection: 'row', padding: 10, paddingBottom: 0 }}>
        <View style={{ flexDirection: 'column', flex: 3 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={AppStyles.Labeldefault}>Nhóm công việc</Text>
            <Text style={{ color: 'red' }}>*</Text>
          </View>
          <TouchableOpacity
            style={[AppStyles.FormInput, { marginRight: 5 }]}
            onPress={() => this.onActionComboboxCategory()}>
            <Text
              style={[
                AppStyles.TextInput,
                this.state.DataUp.CategoryOfTaskGuid == ''
                  ? { color: AppColors.gray }
                  : { color: 'black' },
              ]}>
              {this.state.DataUp.CategoryOfTaskGuid !== ''
                ? this.state.CategoryName
                : 'Chọn...'}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'column', flex: 2 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={AppStyles.Labeldefault}>Ưu tiên</Text>
            <Text style={{ color: 'red' }} />
          </View>
          <TouchableOpacity
            style={[AppStyles.FormInput]}
            onPress={() => this.onActionCombobox_Priority()}>
            <Text
              style={[
                AppStyles.TextInput,
                this.state.PriorityName == ''
                  ? { color: AppColors.gray }
                  : { color: 'black' },
              ]}>
              {this.state.PriorityName !== ''
                ? this.state.PriorityName
                : 'Chọn...'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>

      {/* người chủ trì  */}
      <View style={{ flexDirection: 'column', padding: 10, paddingBottom: 0 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={AppStyles.Labeldefault}>Người chủ trì</Text>
          <Text style={{ color: 'red' }} />
          <TouchableOpacity
            onPress={() => this.setState({ ViewAll: !this.state.ViewAll })}>
            <Text style={[AppStyles.TextLink, { marginLeft: 10 }]}>
              Thêm chi tiết
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={[
            AppStyles.FormInput,
            {
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-start',
            },
          ]}
          onPress={() => this.onActionCombobox_Approval()}>
          {this.state.ApprovalName.length > 0 ? (
            this.state.ApprovalName.map((item, i) => (
              <View
                key={i}
                style={[
                  AppStyles.FormInput,
                  {
                    flexDirection: 'row',
                    padding: 0,
                    marginLeft: 5,
                    marginBottom: 5,
                  },
                ]}>
                <Text style={[AppStyles.TextInput, { color: AppColors.black }]}>
                  {item.text}
                </Text>
                <TouchableOpacity
                  style={{ justifyContent: 'center' }}
                  onPress={() => this.removeArroval(item)}>
                  <Icon
                    name={'close'}
                    type="antdesign"
                    color={'red'}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            ))
          ) : (
            <View>
              <Text
                style={[AppStyles.TextInput, { flex: 1, color: AppColors.gray }]}>
                Chọn...
              </Text>
            </View>
          )}
        </TouchableOpacity>
      </View>

      {this.state.ViewAll ? (
        // người phối hợp
        <View style={{ flexDirection: 'column', padding: 10, paddingBottom: 0 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={AppStyles.Labeldefault}>Người phối hợp</Text>
            <Text style={{ color: 'red' }} />
          </View>
          <TouchableOpacity
            style={[
              AppStyles.FormInput,
              {
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'flex-start',
              },
            ]}
            onPress={() => this.onActionCombobox_Process()}>
            {this.state.ProcessName.length > 0 ? (
              this.state.ProcessName.map((item, i) => (
                <View
                  key={i}
                  style={[
                    AppStyles.FormInput,
                    {
                      flexDirection: 'row',
                      padding: 0,
                      marginLeft: 5,
                      marginBottom: 5,
                    },
                  ]}>
                  <Text style={[AppStyles.TextInput, { color: AppColors.black }]}>
                    {item.text}
                  </Text>
                  <TouchableOpacity
                    style={{ justifyContent: 'center' }}
                    onPress={() => this.removeProcess(item)}>
                    <Icon
                      name={'close'}
                      type="antdesign"
                      color={'red'}
                      size={20}
                    />
                  </TouchableOpacity>
                </View>
              ))
            ) : (
              <View>
                <Text
                  style={[
                    AppStyles.TextInput,
                    { flex: 1, color: AppColors.gray },
                  ]}>
                  Chọn...
                </Text>
              </View>
            )}
          </TouchableOpacity>
        </View>
      ) : null}

      {this.state.ViewAll ? (
        // người theo dõi
        <View style={{ flexDirection: 'column', padding: 10, paddingBottom: 0 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={AppStyles.Labeldefault}>Người theo dõi</Text>
            <Text style={{ color: 'red' }} />
          </View>
          <TouchableOpacity
            style={[
              AppStyles.FormInput,
              {
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'flex-start',
              },
            ]}
            onPress={() => this.onActionCombobox_Follow()}>
            {this.state.FollowName.length > 0 ? (
              this.state.FollowName.map((item, i) => (
                <View
                  key={i}
                  style={[
                    AppStyles.FormInput,
                    {
                      flexDirection: 'row',
                      padding: 0,
                      marginLeft: 5,
                      marginBottom: 5,
                    },
                  ]}>
                  <Text style={[AppStyles.TextInput, { color: AppColors.black }]}>
                    {item.text}
                  </Text>
                  <TouchableOpacity
                    style={{ justifyContent: 'center' }}
                    onPress={() => this.removeFollow(item)}>
                    <Icon
                      name={'close'}
                      type="antdesign"
                      color={'red'}
                      size={20}
                    />
                  </TouchableOpacity>
                </View>
              ))
            ) : (
              <View>
                <Text
                  style={[
                    AppStyles.TextInput,
                    { flex: 1, color: AppColors.gray },
                  ]}>
                  Chọn...
                </Text>
              </View>
            )}
          </TouchableOpacity>
        </View>
      ) : null}

      {/* nhãn công việc */}
      <View style={{ flexDirection: 'column', padding: 10, paddingBottom: 0 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={AppStyles.Labeldefault}>Nhãn công việc</Text>
          <Text style={{ color: 'red' }} />
        </View>
        <TouchableOpacity
          style={[
            AppStyles.FormInput,
            {
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-start',
            },
          ]}
          onPress={() => this.onActionComboboxLabel()}>
          {this.state.Label.length > 0 ? (
            this.state.Label.map((item, i) =>
              item.LabelId === null ? (
                <View>
                  <Text />
                </View>
              ) : (
                <View
                  key={i}
                  style={[
                    AppStyles.FormInput,
                    {
                      flexDirection: 'row',
                      padding: 0,
                      marginLeft: 5,
                      marginBottom: 5,
                    },
                  ]}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      { color: item.Class ? item.Class : AppColors.gray },
                    ]}>
                    {item.LabelName}
                  </Text>
                  <TouchableOpacity
                    style={{ justifyContent: 'center' }}
                    onPress={() => this.removeLabel(item)}>
                    <Icon
                      name={'close'}
                      type="antdesign"
                      color={'red'}
                      size={20}
                    />
                  </TouchableOpacity>
                </View>
              ),
            )
          ) : (
            <View>
              <Text
                style={[AppStyles.TextInput, { flex: 1, color: AppColors.gray }]}>
                Chọn...
              </Text>
            </View>
          )}
        </TouchableOpacity>
      </View>
      {/* hạng mục công việc */}
      <View style={{ flexDirection: 'column' }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            padding: 10,
            paddingBottom: 0,
          }}>
          <Text style={AppStyles.Labeldefault}>Hạng mục công việc</Text>
          <Text />
        </View>
        <Divider />
        {this.state.CheckList.length > 0
          ? this.state.CheckList.map((item, i) => (
            <View
              key={i}
              style={[
                AppStyles.FormInput,
                {
                  flexDirection: 'row',
                  padding: 0,
                  marginRight: 10,
                  marginLeft: 10,
                  marginBottom: 2,
                },
              ]}>
              <TouchableOpacity
                style={{ width: '10%', justifyContent: 'center' }}
                onPress={() => this.editcheckbox(item)}>
                <Icon
                  name={'check'}
                  type="entypo"
                  color={
                    item.Status == true ? AppColors.ColorEdit : AppColors.gray
                  }
                  size={20}
                />
              </TouchableOpacity>
              <View style={{ width: '80%' }}>
                <TextInput
                  style={{ padding: 10, borderLeftWidth: 0.5 }}
                  ref
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  value={item.CheckListTitle}
                  autoFocus={this.state.FormAddChecklist}
                  placeholder={'Nhập hạng mục công việc'}
                  onSubmitEditing={event => this.Enter(event)}
                  onEndEditing={() => this.cleanForm()}
                  onChangeText={text => this.setCheckList(text, i)}
                />
              </View>
              <TouchableOpacity
                style={{ width: '10%', justifyContent: 'center' }}
                onPress={() => this.CleanChecklist(item)}>
                <Icon
                  name={'close'}
                  type="antdesign"
                  color={'red'}
                  size={20}
                />
              </TouchableOpacity>
            </View>
          ))
          : null}
        {this.state.FormAddChecklist ? null : (
          <TouchableOpacity
            style={{ flexDirection: 'row', padding: 10, paddingBottom: 0 }}
            onPress={() => this.FormAddChecklist()}>
            <View style={{ width: '10%', justifyContent: 'center' }}>
              <Icon
                name={'plus'}
                type="entypo"
                color={AppColors.ColorLink}
                size={20}
              />
            </View>
            <View style={{ width: '90%' }}>
              <Text style={{ color: AppColors.ColorLink }}>
                Thêm hạng mục công việc
              </Text>
            </View>
          </TouchableOpacity>
        )}
      </View>

      {/* Đính kèm */}
      <View style={{ flexDirection: 'column', padding: 10 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
            <Text style={{ color: 'red' }}> </Text>
          </View>
          <TouchableOpacity
            style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}
            onPress={() => this.openAttachment()}>
            <Icon
              name="attachment"
              type="entypo"
              size={15}
              color={AppColors.ColorAdd}
            />
            <Text style={[AppStyles.Labeldefault, { color: AppColors.ColorAdd }]}>
              {' '}
              Chọn file
            </Text>
          </TouchableOpacity>
        </View>
        <Divider />
        {this.state.Attachment.length > 0
          ? this.state.Attachment.map((para, i) => (
            <View key={i} style={[{ flexDirection: 'row', marginBottom: 3 }]}>
              <View style={[AppStyles.containerCentered, { padding: 10 }]}>
                <Image
                  style={{ width: 30, height: 30, borderRadius: 10 }}
                  source={{
                    uri: this.FileAttackments.renderImage(para.FileName),
                  }}
                />
              </View>
              <TouchableOpacity
                key={i}
                style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={AppStyles.Textdefault}>{para.FileName}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[AppStyles.containerCentered, { padding: 10 }]}
                onPress={() => this.removeAttactment(para)}>
                <Icon
                  name="close"
                  type="antdesign"
                  size={20}
                  color={AppColors.ColorDelete}
                />
              </TouchableOpacity>
            </View>
          ))
          : null}
      </View>

      <View style={{ flexDirection: 'row', padding: 10, marginBottom: 20 }}>
        {/* cancel*/}
        <TouchableOpacity
          style={[
            AppStyles.containerCentered,
            {
              flex: 1,
              padding: 10,
              borderRadius: 10,
              borderColor: AppColors.ColorPrimary,
              borderWidth: 0.5,
              marginRight: 5,
            },
          ]}
          onPress={() => this.Cancel()}>
          <Text
            style={[AppStyles.Titledefault, { color: AppColors.ColorPrimary }]}>
            Huỷ bỏ
          </Text>
        </TouchableOpacity>
        {/* Save*/}
        <TouchableOpacity
          style={[
            AppStyles.containerCentered,
            {
              flex: 1,
              padding: 10,
              borderRadius: 10,
              borderColor: AppColors.ColorEdit,
              borderWidth: 0.5,
              marginLeft: 5,
            },
          ]}
          onPress={() =>
            this.state.TypeForm == 'add' ? this.Submit() : this.Update()
          }>
          <Text style={[AppStyles.Titledefault, { color: AppColors.ColorEdit }]}>
            Lưu
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
  //#endregion

  //-------------Dùng chung
  //#region hàm Quay lại
  callBackList(rs) {
    this.setState({ loading: false });
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Taskadd',
      Data:
        this.state.TypeForm == 'add'
          ? {
            Module: 'TM_Task',
            // list all key
            Subject: this.state.DataUp.Subject,
            // title
            StartDate:
              this.state.DataUp.StartDate === '' ||
                this.state.DataUp.StartDate === null
                ? null
                : FuncCommon.ConDate(this.state.DataUp.StartDate, 0),
            EndDate:
              this.state.DataUp.EndDate === '' ||
                this.state.DataUp.EndDate === null
                ? null
                : FuncCommon.ConDate(this.state.DataUp.EndDate, 0),
            RowGuid: rs,
            AssignTo: [],
          }
          : { TaskGuid: this.props.data.TaskGuid },
      ActionTime: new Date().getTime(),
    });
  }
  //#endregion

  //#region truyền thông tin và các model
  setSubject = value => {
    this.state.DataUp.Subject = value;
    this.setState({ DataUp: this.state.DataUp });
  };
  setTaskContent = value => {
    this.state.DataUp.TaskContent = value;
    this.setState({ DataUp: this.state.DataUp });
  };
  setStartDate = date => {
    this.state.DataUp.StartDate = date;
    this.setState({ DataUp: this.state.DataUp });
  };
  setEndDate = date => {
    this.state.DataUp.EndDate = date;
    this.setState({ DataUp: this.state.DataUp });
  };
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  //#endregion
  //#region  combobox Category
  _openComboboxCategory() { }
  openComboboxCategory = d => {
    this._openComboboxCategory = d;
  };
  onActionComboboxCategory() {
    this._openComboboxCategory();
  }
  ChangeCategoryOfTask = rs => {
    if (rs.value !== null) {
      this.state.DataUp.CategoryOfTaskGuid = rs.value;
      this.setState(
        {
          DataUp: this.state.DataUp,
          CategoryName: rs.text,
          ListComboboxApproval: [],
          ListComboboxProcess: [],
          ListComboboxFollow: [],
        },
        () => {
          this.ListCombobox_Employee();
        },
      );
    }
  };
  //#endregion
  //#region  combobox Priority
  _openCombobox_Priority() { }
  openCombobox_Priority = d => {
    this._openCombobox_Priority = d;
  };
  onActionCombobox_Priority() {
    this._openCombobox_Priority();
  }
  ChangePriority = rs => {
    this.state.DataUp.Priority = rs.value;
    this.setState({ DataUp: this.state.DataUp, PriorityName: rs.text });
  };
  //#endregion
  //#region combobox Approval
  _openCombobox_Approval() { }
  openCombobox_Approval = d => {
    this._openCombobox_Approval = d;
  };
  onActionCombobox_Approval() {
    this._openCombobox_Approval();
  }
  ChoiceEmployeeApproval = rs => {
    var value = [];
    var list = [];
    for (let i = 0; i < rs.length; i++) {
      if (rs[i].value !== null) {
        list.push(rs[i]);
        value.push(rs[i].value);
      }
    }
    this.setState({ ListApproval: value, ApprovalName: list });
  };
  removeArroval = item => {
    controller.removeArroval(
      item,
      this.state.ListApproval,
      this.state.ApprovalName,
      rs => {
        this.setState({
          ListApproval: rs.ListApproval,
          ApprovalName: rs.ApprovalName,
        });
      },
    );
  };
  removeLabel = item => {
    let arr = this.state.Label.filter(x => x.LabelId !== item.LabelId);
    this.setState({ Label: arr });
  };
  //#endregion
  //#region combobox Process
  _openCombobox_Process() { }
  openCombobox_Process = d => {
    this._openCombobox_Process = d;
  };
  onActionCombobox_Process() {
    this._openCombobox_Process();
  }
  ChoiceEmployeeProcess = rs => {
    var value = [];
    var list = [];
    for (let i = 0; i < rs.length; i++) {
      if (rs[i].value !== null) {
        list.push(rs[i]);
        value.push(rs[i].value);
      }
    }
    this.setState({ ListProcess: value, ProcessName: list });
  };
  removeProcess = item => {
    controller.removeProcess(
      item,
      this.state.ListProcess,
      this.state.ProcessName,
      rs => {
        this.setState({
          ListProcess: rs.ListProcess,
          ProcessName: rs.ProcessName,
        });
      },
    );
  };
  //#endregion
  //#region combobox Follow
  _openCombobox_Follow() { }
  openCombobox_Follow = d => {
    this._openCombobox_Follow = d;
  };
  onActionCombobox_Follow() {
    this._openCombobox_Follow();
  }
  ChoiceEmployeeFollow = rs => {
    var value = [];
    var list = [];
    for (let i = 0; i < rs.length; i++) {
      if (rs[i].value !== null) {
        list.push(rs[i]);
        value.push(rs[i].value);
      }
    }
    this.setState({ ListFollow: value, FollowName: list });
  };
  removeFollow = item => {
    controller.removeFollow(
      item,
      this.state.ListFollow,
      this.state.FollowName,
      rs => {
        this.setState({ ListFollow: rs.ListFollow, FollowName: rs.FollowName });
      },
    );
  };
  //#endregion
  //#region combobox Label
  _openComboboxLabel() { }
  openComboboxLabel = d => {
    this._openComboboxLabel = d;
  };
  onActionComboboxLabel() {
    this._openComboboxLabel();
  }
  ChoiceLabel = rs => {
    if (!rs || !rs[0]?.value) {
      return;
    }
    var value = [];
    var list = [];
    for (let i = 0; i < rs.length; i++) {
      if (rs[i].value !== null) {
        list.push(rs[i]);
        value.push({
          LabelId: rs[i].value,
          LabelName: rs[i].text,
          Class: rs[i].Class,
        });
      }
    }
    var name =
      list.length !== 0 && list.length > 1
        ? list.length + ' lựa chọn'
        : list[0].text;
    this.setState({ LabelName: name, Label: value });
    //
    // if (rs !== null) {
    //     this.state.Label.LabelId = rs.value;
    //     this.state.Label.LabelName = rs.text;
    //     this.state.Label.Class = rs.Class;
    //     this.setState({ Label: this.state.Label });
    // };
  };
  setnullLabel = () => {
    this.state.Label.LabelId = '';
    this.state.Label.LabelName = '';
    this.state.Label.Class = '';
    this.setState({ Label: this.state.Label });
  };
  //#endregion
  //#region CheckList
  FormAddChecklist = () => {
    var list = this.state.CheckList;
    var stt =
      this.state.CheckList.length === 0
        ? 0
        : this.state.CheckList[this.state.CheckList.length - 1].Id;
    list.push({
      Id: stt + 1,
      CheckListTitle: '',
      Status: false,
    });
    this.setState({ CheckList: this.state.CheckList, FormAddChecklist: true });
  };
  setCheckList = (value, i) => {
    this.state.CheckList[i].CheckListTitle = value;
    this.setState({ CheckList: this.state.CheckList });
  };
  Enter = event => {
    this.state.handleInput = false;
    if (event.nativeEvent.text === '') {
      var listaddtext = this.state.CheckList;
      var list = [];
      for (let i = 0; i < listaddtext.length; i++) {
        if (listaddtext[i].CheckListTitle !== '') {
          list.push(listaddtext[i]);
        }
      }
      this.setState({ CheckList: list, FormAddChecklist: false });
    } else {
      this.FormAddChecklist();
    }
  };
  cleanForm = () => {
    if (!this.state.handleInput) {
      this.state.handleInput = true;
      return;
    }
    var listaddtext = this.state.CheckList;
    var list = [];
    for (let i = 0; i < listaddtext.length; i++) {
      if (listaddtext[i].CheckListTitle !== '') {
        list.push(listaddtext[i]);
      }
    }
    this.setState({ CheckList: list, FormAddChecklist: false });
  };
  CleanChecklist = item => {
    var list = this.state.CheckList;
    var listView = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].Id != item.Id) {
        listView.push(list[i]);
      }
    }
    this.setState({ CheckList: listView });
  };
  editcheckbox = item => {
    var _list = this.state.CheckList;
    for (let i = 0; i < _list.length; i++) {
      if (_list[i].Id == item.Id) {
        if (_list[i].Status === null) {
          _list[i].Status = true;
        } else {
          _list[i].Status = !_list[i].Status;
        }
      }
    }
    this.setState({ CheckList: _list });
  };
  //#endregion
  Submit = () => {
    this.setState({
      EventSave: true,
      loading: true,
    });
    var _v2start = FuncCommon.ConDate(this.state.DataUp.StartDate, 5);
    var _v2end = FuncCommon.ConDate(this.state.DataUp.EndDate, 5);
    var _v2New = FuncCommon.ConDate(new Date(), 5);
    var checkListApproval = this.state.ListApproval.find(
      x => x.value === this.state.Emp,
    );
    var checkListProcess = this.state.ListProcess.find(
      x => x.value === this.state.Emp,
    );
    var checkListFollow = this.state.ListFollow.find(
      x => x.value === this.state.Emp,
    );
    if (this.state.DataUp.Subject === '') {
      Toast.showWithGravity(
        'Yêu cầu nhập tiêu đề phiếu',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.setState({ loading: false });
      return;
    }
    if (_v2start > _v2end) {
      Toast.showWithGravity(
        'Yêu cầu ngày bắt đầu phải nhỏ hơn hoặc bằng ngày đến hạn.',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.setState({ loading: false });
      return;
    }
    if (_v2start < _v2New) {
      Toast.showWithGravity(
        'Yêu cầu ngày bắt đầu lớn hơn hoặc bằng ngày hiện tại.',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.setState({ loading: false });
      return;
    }
    if (checkListApproval !== undefined) {
      Toast.showWithGravity(
        'Danh sách người chủ trì đang trùng với người tạo. Xin hãy kiểm tra lại.',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.setState({ loading: false });
      return;
    }
    if (checkListProcess !== undefined) {
      Toast.showWithGravity(
        'Danh sách người phối hợp đang trùng với người tạo. Xin hãy kiểm tra lại.',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.setState({ loading: false });
      return;
    }
    if (checkListFollow !== undefined) {
      Toast.showWithGravity(
        'Danh sách người theo dõi đang trùng với người tạo. Xin hãy kiểm tra lại.',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.setState({ loading: false });
      return;
    }
    for (let i = 0; i < this.state.ListApproval.length; i++) {
      var _LP = this.state.ListProcess.find(
        x => x === this.state.ListApproval[i],
      );
      var _LF = this.state.ListFollow.find(
        x => x === this.state.ListApproval[i],
      );
      if (_LP !== undefined) {
        Toast.showWithGravity(
          'Danh sách người chủ trì đang bị trùng lặp với người phối hợp. Xin hãy kiểm tra lại.',
          Toast.SHORT,
          Toast.CENTER,
        );
        this.setState({ loading: false });
        return;
      }
      if (_LF !== undefined) {
        Toast.showWithGravity(
          'Danh sách người chủ trì đang bị trùng lặp với Theo dõi. Xin hãy kiểm tra lại.',
          Toast.SHORT,
          Toast.CENTER,
        );
        this.setState({ loading: false });
        return;
      }
    }
    if (this.state.ListProcess.length > 0) {
      for (let i = 0; i < this.state.ListProcess.length; i++) {
        var _LF = this.state.ListFollow.find(
          x => x === this.state.ListProcess[i],
        );
        if (_LF !== undefined) {
          Toast.showWithGravity(
            'Danh sách người phối hợp đang bị trùng lặp với người theo dõi. Xin hãy kiểm tra lại.',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.setState({ loading: false });
          return;
        }
      }
    }
    controller.Submit(
      this.state.ListApproval,
      this.state.ListProcess,
      this.state.ListFollow,
      this.state.DataUp,
      this.state.CheckList,
      this.state.Attachment,
      this.state.Emp,
      this.state.Label,
      rs => {
        if (rs.errorCode === 200) {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.setState({ loading: false });
          this.callBackList(JSON.parse(rs.data));
        } else {
          Toast.showWithGravity(
            'Có lỗi khi thêm mới',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.setState({ loading: false });
        }
      },
    );
  };
  Update = () => {
    this.setState({ loading: true });
    if (this.state.DataUp.Subject === '') {
      Toast.showWithGravity(
        'Yêu cầu nhập tiêu đề phiếu',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.setState({ loading: false });
      return;
    }
    var _v2start = FuncCommon.ConDate(this.state.DataUp.StartDate, 5);
    var _v2end = FuncCommon.ConDate(this.state.DataUp.EndDate, 5);
    if (_v2start > _v2end) {
      Toast.showWithGravity(
        'Yêu cầu ngày bắt đầu phải nhỏ hơn hoặc bằng ngày đến hạn.',
        Toast.SHORT,
        Toast.CENTER,
      );
      this.setState({ loading: false });
      return;
    }
    for (let i = 0; i < this.state.ListApproval.length; i++) {
      var _LP = this.state.ListProcess.find(
        x => x === this.state.ListApproval[i],
      );
      var _LF = this.state.ListFollow.find(
        x => x === this.state.ListApproval[i],
      );
      if (_LP !== undefined) {
        Toast.showWithGravity(
          'Danh sách người chủ trì đang bị trùng lặp với người phối hợp. Xin hãy kiểm tra lại.',
          Toast.SHORT,
          Toast.CENTER,
        );
        this.setState({ loading: false });
        return;
      }
      if (_LF !== undefined) {
        Toast.showWithGravity(
          'Danh sách người chủ trì đang bị trùng lặp với Theo dõi. Xin hãy kiểm tra lại.',
          Toast.SHORT,
          Toast.CENTER,
        );
        this.setState({ loading: false });
        return;
      }
    }
    if (this.state.ListProcess.length > 0) {
      for (let i = 0; i < this.state.ListProcess.length; i++) {
        var _LF = this.state.ListFollow.find(
          x => x === this.state.ListProcess[i],
        );
        if (_LF !== undefined) {
          Toast.showWithGravity(
            'Danh sách người phối hợp đang bị trùng lặp với người theo dõi. Xin hãy kiểm tra lại.',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.setState({ loading: false });
          return;
        }
      }
    }
    this.setState({
      EventSave: true,
      loading: true,
    });
    controller.Tasks_Update(
      this.state.ListUserApproval,
      this.state.ListUserProcess,
      this.state.ListUserFollow,
      this.state.ListApproval,
      this.state.ListProcess,
      this.state.ListFollow,
      this.state.DataUp,
      this.state.CheckList,
      this.state.OldAtt,
      this.state.Attachment,
      this.state.Label,
      rs => {
        Toast.showWithGravity(rs.title, Toast.SHORT, Toast.CENTER);
        if (rs.error === true) {
          this.setState({ loading: false });
          return;
        } else {
          this.callBackList(this.props.data.TaskGuid);
        }
      },
    );
  };
  Cancel() {
    Alert.alert(
      'Thông báo',
      'Bạn có chắc chắn huỷ bỏ không?',
      [
        {
          text: 'Xác nhận',
          onPress: () => this.callBackList(),
        },
        { text: 'Huỷ', onPress: () => console.log('No Pressed') },
      ],
      { cancelable: true },
    );
  }
  //#region openAttachment
  _openCombobox_Att() { }
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };

  //#region open img
  _open() { }
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({ Attachment: this.state.Attachment });
  };
  //#endregion

  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({ Attachment: this.state.Attachment });
    } catch (err) {
      this.setState({ loading: false });
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(DataCombobox.options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion

  //#endregion
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(Task_Form_Component);
