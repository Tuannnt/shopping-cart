import React, {Component} from 'react';
import {
  Keyboard,
  Text,
  View,
  Image,
  TextInput,
  Dimensions,
  LayoutAnimation,
  TouchableHighlight,
  Linking,
  Animated,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Alert,
  TouchableWithoutFeedback,
  PermissionsAndroid,
  KeyboardAvoidingView,
  Easing,
} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import TabBar_Title from '../../component/TabBar_Title';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import styles from '../../MS/StyleMS';
import {API, API_HR, API_Message, API_TM_TASKS} from '../../../network';
import {FlatGrid} from 'react-native-super-grid';
import configApp from '../../../configApp';
import RNFetchBlob from 'rn-fetch-blob';
import TopBarNotification from '../../MS/TopBarNotification';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import {FuncCommon} from '../../../utils';
import ImagePicker from 'react-native-image-picker';
import LoadingComponent from '../../component/LoadingComponent';
import CameraRoll from '@react-native-camera-roll/camera-roll';
import {Icon, Divider, Badge, Header} from 'react-native-elements';
import ComboboxV2 from '../../component/ComboboxV2';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import OpenImages from '../../component/OpenImages';
const Sound = require('react-native-sound');
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const TimeOut = 300;
const ListComboboxAtt = [
  {
    value: 'img',
    text: 'Chọn ảnh từ thư viện',
  },
  {
    value: 'file',
    text: 'Chọn file',
  },
];
const options = {
  title: 'Chọn ảnh',
  cancelButtonTitle: 'huỷ bỏ',
  takePhotoButtonTitle: 'Chụp ảnh',
  chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
  chooseWhichLibraryTitle: 'Chọn thư viện',
};
class Task_Comment_Component extends Component {
  constructor() {
    super();
    this.state = {
      ListdataView: [],
      dataImgView: null,
      IsGroup: '',
      Comment: '',
      Id_User: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
      LoginName: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
      OrganizationGuid:
        global.__appSIGNALR.SIGNALR_object.USER.OrganizationGuid,
      FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
      TaskGuid: '',
      UserSend: '',
      ViewName: '',
      CheckScoll: false,
      LiTaskAttachments: [],
      LiComment: [],
      LiFiles: [],
      ListViewIcon: [],
      notifications: [],
      //-------------------------Custom ảnh
      GroupImage: [], //-danh sách thư mục ảnh
      images: [], //-danh sách ảnh
      CameraRoll_first: 0, //-số lượng ảnh lấy ra
      GroupName: '', //-Tên thư mục ảnh lấy ảnh
      ChoiceImages: false, //-phân biệt ảnh được chọn
      ListImageSubmit: [], //-danh sách ảnh được chọn
      ViewAllImages: false, //-Bật tắt full màn hình
      OpenGroup: false,
      statusloadImage: false,
      loading: true,
      down: false,
      //------------------------Các hiệu ứng chuyển động
      index: false,
      indexIcon: false,
      indexFull: false,
      offsetY: new Animated.Value(2000),
      offsetButtonX: new Animated.Value(100),
      IconOffsetX: new Animated.Value(-SCREEN_WIDTH),
      ViewFullOffsetY: new Animated.Value(SCREEN_HEIGHT),
    };
    this._ListComment = {
      GroupUser: [],
      RecordGuid: '',
      Take: 50,
      Skip: 1,
    };
    this.DataSubmit = {
      Name: '',
      IsGroup: 0,
      detail: [],
    };
    this.state.ListIconComment = [];
    this.state.ListIcontab = [];
    this.ValueType = 'M';
    this.Comment = '';
    this.state.COMMON_ICON_ANIMATION = {
      init: ['QAK', 'mini'],
      QAK: {
        link: '/img/chatbox/icon_q/a',
        name: 'QAK',
        default: '/img/chatbox/icon_q/icon.png',
        type: '.png',
        data: 32,
      },
      mini: {
        link: '/img/chatbox/icon_d/a',
        name: 'mini',
        default: '/img/chatbox/icon_d/icon.png',
        type: '.png',
        data: 40,
      },
      renderCode: function(code, sort) {
        return code + '%' + sort;
      },
      renderImg: function(code) {
        var _a = code.split('%');
        return (
          configApp.url_icon_chat + this[_a[0]].link + _a[1] + this[_a[0]].type
        );
      },
    };
    this.state.FileAttackments = {
      renderImage(check, guid) {
        if (
          check.toLowerCase() === 'png' ||
          check.toLowerCase() == 'jpg' ||
          check.toLowerCase() == 'jpeg'
        ) {
          return configApp.url_Task_ViewImg + guid;
        } else {
          return (
            configApp.url_icon_chat +
            configApp.link_type_icon +
            check +
            '-icon.png'
          );
        }
      },
    };
    global.__appSIGNALR.addReviceMess(this.ReviceMess);
  }
  async componentDidMount() {
    this._ListComment.RecordGuid = this.props.data.TaskGuid;
    this.setState({ViewName: this.props.data.TaskName});
    await this.request_storage_runtime_permission();
    this.ListData();
  }

  //hàm nhận tin nhắn
  ReviceMess = Comment => {
    var _LiComment = this.state.LiComment;
    var CommentString = JSON.parse(Comment);
    if (
      this._ListComment.RecordGuid != null &&
      this._ListComment.RecordGuid != undefined &&
      this._ListComment.RecordGuid != ''
    ) {
      if (CommentString.Code == 'TASK') {
        if (CommentString.TaskGuid == this._ListComment.RecordGuid) {
          if (CommentString.EmployeeGuid != this.state.Id_User) {
            var whoosh = new Sound(
              'tinnhanfb.mp3',
              Sound.MAIN_BUNDLE,
              error => {
                if (error) {
                  return;
                }
                whoosh.play(success => {
                  if (success) {
                  } else {
                  }
                });
              },
            );
          }
          _LiComment.push({
            CommentGuid: CommentString.CommentGuid,
            EmployeeGuid: CommentString.EmployeeGuid,
            Comment: CommentString.Comment,
            CreatedDate: CommentString.CreatedDate,
            EmployeeName: CommentString.EmployeeName,
            TaskCommentAttachments: CommentString.TaskCommentAttachments,
          });
          this.setState({LiComment: _LiComment});
          this.LoadListdataByAvata(_LiComment);
        }
      } else {
      }
    }
  };
  //thêm mới comment
  sendComment() {
    if (this.state.Comment !== '' && this.state.Comment.trim()) {
      this.Comment = this.state.Comment;
    } else {
      if (this.state.LiFiles.length > 0) {
        this.Comment = 'Gửi file đính kèm';
      }
    }
    var objMessage = {
      Comment: this.Comment,
      RecordGuid: this._ListComment.RecordGuid,
    };
    var _data = new FormData();
    _data.append('insert', JSON.stringify(objMessage));
    for (var i = 0; i < this.state.LiFiles.length; i++) {
      _data.append(this.state.LiFiles[i].name, this.state.LiFiles[i]);
    }
    this.setState({Comment: '', LiFiles: []});
    API_TM_TASKS.CommentInsert(_data)
      .then(res => {
        var _dataCmt = [];
        if (JSON.parse(res.data.data) != null) {
          _dataCmt = JSON.parse(res.data.data);
        }
        var _comment = {
          CommentGuid: _dataCmt.CommentGuid,
          TaskGuid: this.props.data.TaskGuid,
          EmployeeGuid: _dataCmt.EmployeeGuid,
          CreatedDate: _dataCmt.CreatedDate,
          Comment: _dataCmt.Comment,
          EmployeeName: _dataCmt.EmployeeName,
          TaskCommentAttachments:
            _dataCmt.TaskCommentAttachments !== null
              ? JSON.parse(_dataCmt.TaskCommentAttachments)
              : [],
          Code: 'TASK',
        };
        var obj = {};
        obj.LoginName = this.state.LoginName;
        obj.OrganizationGuid = this.state.OrganizationGuid;
        obj.ToUser = _dataCmt.UserOfTask;
        obj.DataJson = JSON.stringify(_comment);
        // global.__appSIGNALR.sendMess(obj);
        this.ReviceMess(JSON.stringify(_comment));
      })
      .catch(error => {
        console.log(error.data.data);
        ErrorHandler.handle(error.data);
      });
  }
  removeNotification(id) {
    this.setState(state => {
      return {
        notifications: state.notifications.filter(
          notification => notification.id !== id,
        ),
      };
    });
  }
  //hiển thị danh sách comment
  ListData() {
    API_TM_TASKS.CommentList(this._ListComment)
      .then(res => {
        var _list = this.state.LiComment;
        var _list_v1 = JSON.parse(res.data.data).reverse();
        var _list_v2 = [];
        for (let i = 0; i < _list_v1.length; i++) {
          if (_list_v1[i].TaskCommentAttachments !== null) {
            _list_v1[i].TaskCommentAttachments = JSON.parse(
              _list_v1[i].TaskCommentAttachments,
            );
          }
          _list_v2.unshift(_list_v1[i]);
        }
        for (let i = 0; i < _list_v2.length; i++) {
          _list.unshift(_list_v2[i]);
        }
        this.LoadListdataByAvata(_list);
      })
      .catch(error => {
        console.log(error);
      });
  }
  //#region gộp avatar cùng phút
  LoadListdataByAvata() {
    try {
      var _list = this.state.LiComment;
      var data_time = _list[0];
      var data_Date = _list[0];

      for (let j = 0; j < _list.length; j++) {
        _list[j].ViewDate = false;
        _list[j].ViewTime = false;
        _list[j].ViewName = false;
        _list[j].margin = false;
        if (j === 0) {
          _list[0].margin = true;
          _list[0].ViewName = true;
        }
        //lấy ngày
        if (data_Date.ChatboxMessageGuid != _list[j].ChatboxMessageGuid) {
          if (
            FuncCommon.ConDate(data_Date.CreatedDate, 0) !==
            FuncCommon.ConDate(_list[j].CreatedDate, 0)
          ) {
            _list[j].ViewDate = true;
            data_Date = _list[j];
          }
        }
        //lấy giờ
        if (data_time.ChatboxMessageGuid != _list[j].ChatboxMessageGuid) {
          if (
            FuncCommon.ConDate(data_time.CreatedDate, 7) !=
              FuncCommon.ConDate(_list[j].CreatedDate, 7) &&
            data_time.EmployeeGuid == _list[j].EmployeeGuid
          ) {
            _list[j - 1].ViewTime = true;
            _list[j].ViewName = true;
            _list[j].margin = true;
          } else if (
            FuncCommon.ConDate(data_time.CreatedDate, 7) ===
              FuncCommon.ConDate(_list[j].CreatedDate, 7) &&
            data_time.EmployeeGuid !== _list[j].EmployeeGuid
          ) {
            _list[j - 1].ViewTime = true;
            _list[j].ViewName = true;
            _list[j].margin = true;
          } else if (
            FuncCommon.ConDate(data_time.CreatedDate, 7) !==
              FuncCommon.ConDate(_list[j].CreatedDate, 7) &&
            data_time.EmployeeGuid !== _list[j].EmployeeGuid
          ) {
            _list[j].ViewName = true;
            _list[j - 1].ViewTime = true;
            _list[j].margin = true;
          } else if (
            FuncCommon.ConDate(data_time.CreatedDate, 7) ===
              FuncCommon.ConDate(_list[j].CreatedDate, 7) &&
            data_time.EmployeeGuid === _list[j].EmployeeGuid
          ) {
          } else {
            _list[j].ViewName = true;
            _list[j - 1].ViewTime = true;
          }
          data_time = _list[j];
        }
      }
      this.setState({
        LiComment: _list,
        loading: false,
      });
    } catch (error) {
      console.log(error);
      this.callBack();
    }
  }
  //#endregion

  //cuộn tin nhắn
  handleScroll(Object) {
    if (
      this.state.CheckScoll == true &&
      Object.nativeEvent.contentOffset.y == 0
    ) {
      this.state.CheckScoll = false;
      this._ListComment.NumberPage++;
      this.ListData();
    }
  }
  //#region File ảnh
  OnScrollEndDrag(event, item) {
    var y = event.nativeEvent.contentOffset.y;
    var i = ((SCREEN_WIDTH / 3.03) * item.length) / 3 - 500;
    if (y >= i) {
      if (this.state.statusloadImage) {
        this._storeImages();
      }
    }
  }
  OnScrollEndDragFull(event, item) {
    var y = event.nativeEvent.contentOffset.y;
    var i = ((SCREEN_WIDTH / 3.03) * item.length) / 3 - SCREEN_HEIGHT;
    if (y >= i) {
      if (this.state.statusloadImage) {
        this._storeImages();
      }
    }
  }

  _storeImages() {
    CameraRoll.getPhotos({
      first: 50,
      assetType: 'Photos',
      groupName: this.state.GroupName,
      // toTime:100
      after: this.end_cursor,
    })
      .then(data => {
        this.setState({statusloadImage: data.page_info.has_next_page});
        this.end_cursor = data.page_info.end_cursor;
        var assets = data.edges;
        var _images = this.state.images;
        for (let i = 0; i < assets.length; i++) {
          var name = assets[i].node.image.uri.split('/');
          var check = this.state.ListImageSubmit.find(
            x => x.uri === assets[i].node.image.uri,
          );
          if (check !== undefined) {
            _images.push({
              name:
                assets[i].node.image.filename !== null
                  ? assets[i].node.image.filename
                  : name[name.length - 1].toLowerCase(),
              size: assets[i].node.image.fileSize,
              type: assets[i].node.type.toLowerCase(),
              uri: assets[i].node.image.uri,
              check: true,
              CountSelect: check.CountSelect,
            });
          } else {
            _images.push({
              name:
                assets[i].node.image.filename !== null
                  ? assets[i].node.image.filename
                  : name[name.length - 1].toLowerCase(),
              size: assets[i].node.image.fileSize,
              type: assets[i].node.type.toLowerCase(),
              uri: assets[i].node.image.uri,
            });
          }
        }
        this.setState({
          images: _images,
        });
      })
      .catch(err => {
        this.callBack();
        console.log(err);
        alert(err);
      });
  }
  _selectMultiple(item) {
    var listSubmit = [];
    if (this.state.ListImageSubmit.length > 0) {
      var obj = this.state.ListImageSubmit.find(x => x.name === item.name);
      if (obj !== undefined) {
        var count = 0;
        for (let i = 0; i < this.state.ListImageSubmit.length; i++) {
          if (this.state.ListImageSubmit[i].name !== item.name) {
            listSubmit.push({
              name: this.state.ListImageSubmit[i].name,
              size: this.state.ListImageSubmit[i].size,
              type: this.state.ListImageSubmit[i].type,
              uri: this.state.ListImageSubmit[i].uri,
              check: true,
              CountSelect: count + 1,
            });
            count = count + 1;
          }
        }
        this.state.ListImageSubmit = listSubmit;
      } else {
        this.state.ListImageSubmit.push({
          name: item.name,
          size: item.size,
          type: item.type,
          uri: item.uri,
          check: true,
          CountSelect: this.state.ListImageSubmit.length + 1,
        });
      }
    } else {
      this.state.ListImageSubmit.push({
        name: item.name,
        size: item.size,
        type: item.type,
        uri: item.uri,
        check: true,
        CountSelect: this.state.ListImageSubmit.length + 1,
      });
    }
    var AllListImages = this.state.images;
    for (let y = 0; y < AllListImages.length; y++) {
      if (AllListImages[y].name === item.name) {
        AllListImages[y].check = !AllListImages[y].check;
      }
      for (let z = 0; z < this.state.ListImageSubmit.length; z++) {
        if (AllListImages[y].name === this.state.ListImageSubmit[z].name) {
          AllListImages[y].CountSelect = this.state.ListImageSubmit[
            z
          ].CountSelect;
          break;
        }
      }
    }
    this.setState({
      images: AllListImages,
    });
  }
  _CleanListImages() {
    var ListImages = this.state.images;
    for (let i = 0; i < ListImages.length; i++) {
      ListImages[i].check = false;
    }
    this.setState({
      images: ListImages,
      ListImageSubmit: [],
    });
  }
  Up_Down(e) {
    this.clickDow = e.nativeEvent.contentSize.height;
    var y = e.nativeEvent.contentOffset.y;
    if (y < e.nativeEvent.contentSize.height - SCREEN_HEIGHT) {
      this.setState({down: true});
    } else {
      this.setState({down: false});
    }
  }
  lazyGetHeight = (w, h) => {
    if (this.scrollHeight_isActive && h !== 0) {
      this.scrollRef.scrollToEnd();
      this.scrollHeight_isActive = false;
    } else {
      this.scrollRef.scrollTo(h - this.scrollHeightMess_last);
    }
    setTimeout(() => {
      this.setState({loading: false});
    }, 1000);
  };
  //#endregion

  //#region hiển thị danh sách tin nhắn
  renderComment = () =>
    this.state.LiComment.length > 0
      ? this.state.LiComment.map((item, index) => {
          return (
            <TouchableOpacity style={{flexDirection: 'column'}}>
              {item.ViewDate == true ? (
                <View
                  style={[
                    AppStyles.containerCentered,
                    {marginTop: 20, justifyContent: 'center'},
                  ]}>
                  <View
                    style={[
                      {
                        borderTopWidth: 0.5,
                        borderTopColor: '#CCCCCC',
                        width: SCREEN_WIDTH,
                      },
                    ]}
                  />
                  <View
                    style={[AppStyles.containerCentered, styles.styleDateMess]}>
                    <Text style={[AppStyles.Textsmall]}>
                      {this.customDate(item.CreatedDate)}
                    </Text>
                  </View>
                </View>
              ) : null}
              {item.EmployeeGuid == this.state.Id_User ? (
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 3}} />
                  <View
                    style={[
                      item.margin == true ? {marginTop: 20} : null,
                      {flex: 7, alignItems: 'flex-end'},
                    ]}>
                    {// item.Type == "A" ?
                    //     <View>
                    //         <TouchableOpacity style={styles.boxImage}>
                    //             <Image
                    //                 style={AppStyles.ImageStyle}
                    //                 source={{ uri: this.state.COMMON_ICON_ANIMATION.renderImg(item.Comment) }}
                    //             />
                    //         </TouchableOpacity>
                    //         {item.ViewTime == true ?
                    //             <View style={{ alignItems: "flex-end", marginRight: 10 }}>
                    //                 <Text style={styles.TextViewTime}>{this.customTime(item.CreatedDate)}</Text>
                    //             </View>
                    //             : <View style={{ marginTop: 5 }} />
                    //         }
                    //     </View>
                    //     :
                    item.TaskCommentAttachments &&
                    item.TaskCommentAttachments.length > 0 ? (
                      <View style={{marginTop: 3}}>
                        {item.TaskCommentAttachments.length == 1 ? (
                          <View style={{marginBottom: 3, marginRight: 20}}>
                            <TouchableOpacity
                              onPress={() =>
                                this.AccOpenImg(item.TaskCommentAttachments[0])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.TaskCommentAttachments[0]
                                      .FileExtension,
                                    item.TaskCommentAttachments[0]
                                      .AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        ) : item.TaskCommentAttachments.length == 2 ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              width: '100%',
                              margin: 3,
                              marginRight: 10,
                            }}>
                            <TouchableOpacity
                              style={{marginRight: 5}}
                              onPress={() =>
                                this.AccOpenImg(item.TaskCommentAttachments[0])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.TaskCommentAttachments[0]
                                      .FileExtension,
                                    item.TaskCommentAttachments[0]
                                      .AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() =>
                                this.AccOpenImg(item.TaskCommentAttachments[1])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.TaskCommentAttachments[1]
                                      .FileExtension,
                                    item.TaskCommentAttachments[1]
                                      .AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        ) : item.TaskCommentAttachments.length == 3 ? (
                          <View
                            style={{
                              flexDirection: 'column',
                              marginBottom: 3,
                              marginRight: 10,
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                marginBottom: 5,
                              }}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[0],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[0]
                                        .FileExtension,
                                      item.TaskCommentAttachments[0]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[1],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[1]
                                        .FileExtension,
                                      item.TaskCommentAttachments[1]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row', width: '100%'}}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[2],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[2]
                                        .FileExtension,
                                      item.TaskCommentAttachments[2]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          </View>
                        ) : (
                          <View
                            style={{
                              flexDirection: 'column',
                              marginBottom: 3,
                              marginRight: 10,
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                marginBottom: 5,
                              }}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[0],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[0]
                                        .FileExtension,
                                      item.TaskCommentAttachments[0]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[1],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[1]
                                        .FileExtension,
                                      item.TaskCommentAttachments[1]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row', width: '100%'}}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[2],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[2]
                                        .FileExtension,
                                      item.TaskCommentAttachments[2]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{flex: 1}}
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[0],
                                  )
                                }>
                                <View
                                  style={[
                                    AppStyles.containerCentered,
                                    {
                                      width: 100,
                                      height: 100,
                                      borderRadius: 15,
                                      backgroundColor: AppColors.gray,
                                      opacity: 0.5,
                                    },
                                  ]}>
                                  <Text style={AppStyles.Titledefault}>
                                    + {item.TaskCommentAttachments.length - 3}
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                          </View>
                        )}
                        {item.Comment !== 'File đính kèm' ? (
                          <TouchableOpacity
                            style={[styles.boxComment, {paddingBottom: 5}]}>
                            <Text style={AppStyles.TextMessage}>
                              {item.Comment}
                            </Text>
                            {item.ViewTime == true ? (
                              <Text style={styles.TextViewTime}>
                                {this.customTime(item.CreatedDate)}
                              </Text>
                            ) : null}
                          </TouchableOpacity>
                        ) : item.ViewTime == true ? (
                          <View
                            style={{
                              alignItems: 'flex-end',
                              marginRight: 10,
                              marginTop: 10,
                              marginBottom: 3,
                            }}>
                            <Text style={styles.TextViewTime}>
                              {this.customTime(item.CreatedDate)}
                            </Text>
                          </View>
                        ) : null}
                      </View>
                    ) : item.ViewTime == false ? (
                      <TouchableOpacity style={styles.boxComment}>
                        {item.Type == 'L' ? (
                          <Text
                            style={AppStyles.TextMessage}
                            onPress={() => Linking.openURL(item.Comment)}>
                            {item.Comment}
                          </Text>
                        ) : (
                          <Text style={AppStyles.TextMessage}>
                            {item.Comment}
                          </Text>
                        )}
                      </TouchableOpacity>
                    ) : (
                      <View style={{flexDirection: 'column'}}>
                        <TouchableOpacity
                          style={[styles.boxComment, {paddingBottom: 5}]}>
                          {item.Type == 'L' ? (
                            <Text
                              style={AppStyles.TextMessage}
                              onPress={() => Linking.openURL(item.Comment)}>
                              {item.Comment}
                            </Text>
                          ) : (
                            <Text style={AppStyles.TextMessage}>
                              {item.Comment}
                            </Text>
                          )}
                          <Text style={styles.TextViewTime}>
                            {this.customTime(item.CreatedDate)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>
                </View>
              ) : (
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={[
                      item.margin == true ? {marginTop: 20} : null,
                      {flex: 7, flexDirection: 'row'},
                    ]}>
                    {item.margin == false ? (
                      <View style={{marginTop: 10, marginLeft: 5, opacity: 0}}>
                        <Image
                          style={{width: 20, height: 20, borderRadius: 50}}
                          source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                        />
                      </View>
                    ) : (
                      <View style={{marginLeft: 5}}>
                        <Image
                          style={{width: 20, height: 20, borderRadius: 50}}
                          source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                        />
                      </View>
                    )}

                    {// item.Type == "A" ?
                    //     <View>
                    //         {this.state.IsGroup == "True" && item.ViewTime == true ?
                    //             <View style={{ flexDirection: 'row', alignItems: "flex-start", marginLeft: 5, marginTop: 10, marginBottom: 3 }}>
                    //                 <Text style={AppStyles.Textsmall}>{this.customFullName(item.EmployeeName)} | </Text>
                    //                 <Text style={styles.TextViewTime}>{this.customTime(item.CreatedDate)}</Text>
                    //             </View>
                    //             : null
                    //         }
                    //         <View style={styles.boxImage_user}>
                    //             <Image
                    //                 style={AppStyles.ImageStyle}
                    //                 source={{ uri: this.state.COMMON_ICON_ANIMATION.renderImg(item.Comment) }}
                    //             />
                    //         </View>
                    //     </View>
                    //     :
                    item.TaskCommentAttachments &&
                    item.TaskCommentAttachments.length > 0 ? (
                      <View>
                        {this.state.IsGroup == 'True' &&
                        item.ViewTime == true ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'flex-start',
                              marginLeft: 5,
                              marginTop: 10,
                              marginBottom: 3,
                            }}>
                            <Text style={AppStyles.Textsmall}>
                              {this.customFullName(item.EmployeeName)} |{' '}
                            </Text>
                            <Text style={styles.TextViewTime}>
                              {this.customTime(item.CreatedDate)}
                            </Text>
                          </View>
                        ) : item.ViewTime == true ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'flex-start',
                              marginLeft: 5,
                              marginTop: 10,
                              marginBottom: 3,
                            }}>
                            <Text style={styles.TextViewTime}>
                              {this.customTime(item.CreatedDate)}
                            </Text>
                          </View>
                        ) : (
                          <View style={{marginTop: 5}} />
                        )}
                        {item.TaskCommentAttachments.length == 1 ? (
                          <View style={{marginBottom: 3}}>
                            <TouchableOpacity
                              onPress={() =>
                                this.AccOpenImg(item.TaskCommentAttachments[0])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.TaskCommentAttachments[0]
                                      .FileExtension,
                                    item.TaskCommentAttachments[0]
                                      .AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        ) : item.TaskCommentAttachments.length == 2 ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              width: '100%',
                              margin: 3,
                            }}>
                            <TouchableOpacity
                              style={{marginRight: 5}}
                              onPress={() =>
                                this.AccOpenImg(item.TaskCommentAttachments[0])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.TaskCommentAttachments[0]
                                      .FileExtension,
                                    item.TaskCommentAttachments[0]
                                      .AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() =>
                                this.AccOpenImg(item.TaskCommentAttachments[1])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.TaskCommentAttachments[1]
                                      .FileExtension,
                                    item.TaskCommentAttachments[1]
                                      .AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        ) : item.TaskCommentAttachments.length == 3 ? (
                          <View style={{flexDirection: 'column', margin: 3}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                marginBottom: 5,
                              }}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[0],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[0]
                                        .FileExtension,
                                      item.TaskCommentAttachments[0]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[1],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[1]
                                        .FileExtension,
                                      item.TaskCommentAttachments[1]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row', width: '100%'}}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[2],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[2]
                                        .FileExtension,
                                      item.TaskCommentAttachments[2]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          </View>
                        ) : (
                          <View
                            style={{flexDirection: 'column', marginBottom: 3}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                marginBottom: 5,
                              }}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[0],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[0]
                                        .FileExtension,
                                      item.TaskCommentAttachments[0]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[1],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[1]
                                        .FileExtension,
                                      item.TaskCommentAttachments[1]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row', width: '100%'}}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[2],
                                  )
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.TaskCommentAttachments[2]
                                        .FileExtension,
                                      item.TaskCommentAttachments[2]
                                        .AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{flex: 1}}
                                onPress={() =>
                                  this.AccOpenImg(
                                    item.TaskCommentAttachments[0],
                                  )
                                }>
                                <View
                                  style={[
                                    AppStyles.containerCentered,
                                    {
                                      width: 100,
                                      height: 100,
                                      borderRadius: 15,
                                      backgroundColor: AppColors.gray,
                                      opacity: 0.5,
                                    },
                                  ]}>
                                  <Text style={AppStyles.Titledefault}>
                                    + {item.TaskCommentAttachments.length - 3}
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                          </View>
                        )}
                        {item.Comment !== 'File đính kèm' ? (
                          <TouchableOpacity style={styles.boxComment_user}>
                            <Text style={AppStyles.TextMessage}>
                              {item.Comment}
                            </Text>
                          </TouchableOpacity>
                        ) : null}
                      </View>
                    ) : item.ViewTime == false ? (
                      <TouchableOpacity style={styles.boxComment_user}>
                        {item.Type == 'L' ? (
                          <Text
                            style={AppStyles.TextMessage}
                            onPress={() => Linking.openURL(item.Comment)}>
                            {item.Comment}
                          </Text>
                        ) : (
                          <Text style={AppStyles.TextMessage}>
                            {item.Comment}
                          </Text>
                        )}
                      </TouchableOpacity>
                    ) : (
                      <View style={{flexDirection: 'column'}}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            marginLeft: 5,
                            marginTop: 10,
                          }}>
                          <Text style={AppStyles.Textsmall}>
                            {this.customFullName(item.EmployeeName)} |{' '}
                          </Text>
                          <Text style={styles.TextViewTime}>
                            {this.customTime(item.CreatedDate)}
                          </Text>
                        </View>
                        <TouchableOpacity style={styles.boxComment_user}>
                          {item.Type == 'L' ? (
                            <Text
                              style={AppStyles.TextMessage}
                              onPress={() => Linking.openURL(item.Comment)}>
                              {item.Comment}
                            </Text>
                          ) : (
                            <Text style={AppStyles.TextMessage}>
                              {item.Comment}
                            </Text>
                          )}
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>
                  <View style={{flex: 3}} />
                </View>
              )}
            </TouchableOpacity>
          );
        })
      : null;
  //#endregion
  //#region View Tổng
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={styles.container}>
            {this.state.notifications.length > 0 ? (
              <TopBarNotification
                Listview={this.state.notifications}
                removeNotification={id => this.removeNotification(id)}
              />
            ) : null}
            <TabBar_Title
              title={this.state.ViewName}
              callBack={() => this.callBack()}
            />
            <Divider />
            <View style={[{flex: 1, paddingBottom: 20}]}>
              <ScrollView
                ref={ref => (this.scrollRef = ref)}
                onContentSizeChange={this.lazyGetHeight}
                onScrollEndDrag={e => this.Up_Down(e)}
                onMomentumScrollEnd={event => this.handleScroll(event)}>
                {this.state.LiComment && this.state.LiComment.length > 0
                  ? this.renderComment()
                  : null}
              </ScrollView>
              {this.state.loading == true ? <LoadingComponent /> : null}
              {this.state.down == true ? (
                <TouchableOpacity
                  style={styles.ClickDown}
                  onPress={() => {
                    this.scrollRef.scrollTo(this.clickDow);
                    this.setState({down: false});
                  }}>
                  <Icon
                    color={'#fff'}
                    size={20}
                    name="angle-double-down"
                    type="font-awesome"
                  />
                </TouchableOpacity>
              ) : null}
            </View>
            <View
              style={
                Platform.OS === 'ios'
                  ? styles.boxBottom_contentIOS
                  : styles.boxBottom_content
              }>
              {/* Text */}
              <TouchableOpacity style={{flex: 5, paddingLeft: 5}}>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Nội dung"
                  style={{fontSize: 16}}
                  autoCapitalize="none"
                  multiline={true}
                  value={this.state.Comment}
                  onChangeText={text => this.setState({Comment: text})}
                />
              </TouchableOpacity>
              {/* File */}
              {(this.state.Comment !== '' && this.state.Comment.trim()) ||
              (this.state.LiFiles && this.state.LiFiles.length > 0) ? (
                <TouchableOpacity
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                  }}
                  onPress={() => {
                    this.sendComment(), (this.scrollHeight_isActive = true);
                  }}>
                  <Icon
                    iconStyle={{color: 'blue'}}
                    name="send"
                    type="Feather"
                  />
                  {this.state.ListImageSubmit.length > 0 ? (
                    <Badge
                      badgeStyle={{
                        width: 15,
                        height: 15,
                        borderWidth: 0,
                        borderRadius: 20,
                      }}
                      value={this.state.ListImageSubmit.length}
                      containerStyle={{position: 'absolute', right: 10, top: 3}}
                    />
                  ) : null}
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={[AppStyles.containerCentered, {flex: 1}]}
                  onPress={() => this.openAttachment()}>
                  <Icon
                    iconStyle={{
                      color: this.state.ChoiceImages
                        ? AppColors.Maincolor
                        : '#AAAAAA',
                    }}
                    name="picture"
                    type="antdesign"
                  />
                  {this.state.ListImageSubmit.length > 0 ? (
                    <Badge
                      badgeStyle={{
                        width: 15,
                        height: 15,
                        borderWidth: 0,
                        borderRadius: 20,
                      }}
                      value={this.state.ListImageSubmit.length}
                      containerStyle={{position: 'absolute', right: 5, top: 5}}
                    />
                  ) : null}
                </TouchableOpacity>
              )}
            </View>
            <ComboboxV2
              callback={this.ChoiceAtt}
              data={ListComboboxAtt}
              eOpen={this.openCombobox_Att}
            />
            <OpenPhotoLibrary
              callback={this.callbackLibarary}
              openLibrary={this.openLibrary}
            />
            {this.state.ListdataView.length > 0 ? (
              <OpenImages
                value={this.state.dataImgView}
                data={this.state.ListdataView}
                openImg={this.openImg}
              />
            ) : null}
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  //#endregion

  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };

  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.LiFiles.push({
        name: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({LiFiles: this.state.LiFiles});
  };
  //#endregion

  //#region Xem ảnh
  _openImg() {}
  openImg = d => {
    this._openImg = d;
  };
  ViewImg() {
    this._openImg();
  }
  AccOpenImg = item => {
    let obj = {
      IdS: [this.props.data.TaskGuid],
    };
    API_TM_TASKS.TaskComment_ViewAll_Image(obj)
      .then(res => {
        var _list = [];
        var _listDB = JSON.parse(res.data.data);
        for (let i = 0; i < _listDB.length; i++) {
          _list.unshift(_listDB[i]);
        }
        this.setState(
          {
            ListdataView: _list,
            dataImgView: item,
          },
          () => {
            this.ViewImg();
          },
        );
      })
      .catch(error => {
        ErrorHandler.handle(error.data);
      });
  };

  // callbackLibarary = (rs) => {
  //     for (let i = 0; i < rs.length; i++) {
  //         this.state.LiFiles.push(
  //             {
  //                 name: rs[i].name.toLowerCase(),
  //                 size: rs[i].size,
  //                 type: rs[i].type.toLowerCase(),
  //                 uri: rs[i].uri
  //             });
  //     };
  //     this.setState({ LiFiles: this.state.LiFiles });
  // };
  //#endregion

  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.LiFiles;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          name: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].name = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({LiFiles: this.state.LiFiles});
      this.sendComment();
      this.scrollHeight_isActive = true;
    } catch (err) {
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.LiFiles;
      await ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            name:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
          this.setState({LiFiles: this.state.LiFiles});
          this.sendComment();
          this.scrollHeight_isActive = true;
        }
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion

  //#endregion

  callBack() {
    this.setState({EventBoxIcon: true});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      Data: {TaskGuid: this.props.data.TaskGuid},
      ActionTime: new Date().getTime(),
    });
  }
  //lấy tên
  customFullName(para) {
    if (para != null) {
      var _name = para.split(' ');
      var _nameView = '';
      for (let i = 0; i < _name.length; i++) {
        if (i == _name.length - 1) {
          _nameView = _name[i];
        }
      }
      return _nameView;
    }
  }
  //#region định dạng thời gian
  //định dạng giờ phút
  customTime(Time) {
    if (Time != null) {
      var _split = FuncCommon.ConDate(Time, 6).split(':');
      return _split[0] + ':' + _split[1];
    } else {
      return '';
    }
  }
  //định dạng ngày
  customDate(_item) {
    if (_item != null) {
      var item = FuncCommon.ConDate(_item, 1);
      var SplitTime = String(item).split(' ');
      //định dạng ngày
      var SplitTime_v1 = SplitTime[0].split('/');
      var MM = '';
      if (SplitTime_v1[0].length < 2) {
        MM = '0' + SplitTime_v1[0];
      } else {
        MM = SplitTime_v1[0];
      }
      var dd = '';
      if (SplitTime_v1[1].length < 2) {
        dd = '0' + SplitTime_v1[1];
      } else {
        dd = SplitTime_v1[1];
      }
      var date = dd + '-' + MM + '-' + SplitTime_v1[2];

      //định dạng ngày hiện tại
      var MMNow = '';
      if ((new Date().getMonth() + 1).toString().length < 2) {
        MMNow = '0' + (new Date().getMonth() + 1).toString();
      } else {
        MMNow = (new Date().getMonth() + 1).toString();
      }
      var ddNow = '';
      if (new Date().getDate().toString().length < 2) {
        ddNow = '0' + new Date().getDate().toString();
      } else {
        ddNow = new Date().getDate().toString();
      }
      var SplitDateNow = ddNow + '-' + MMNow + '-' + new Date().getFullYear();
      if (date === SplitDateNow) {
        return 'Hôm nay';
      } else {
        return date;
      }
    } else {
      return '';
    }
  }
  //#region định dạng ngày
  customdateTime(DataTime) {
    if (DataTime != null) {
      var SplitTime = String(DataTime).split(' ');
      //định dạng ngày
      var SplitTime_v1 = SplitTime[0].split('/');
      var MM = '';
      if (SplitTime_v1[0].length < 2) {
        MM = '0' + SplitTime_v1[0];
      } else {
        MM = SplitTime_v1[0];
      }
      var dd = '';
      if (SplitTime_v1[1].length < 2) {
        dd = '0' + SplitTime_v1[1];
      } else {
        dd = SplitTime_v1[1];
      }
      var date = dd + '-' + MM + '-' + SplitTime_v1[2];

      //định dạng giờ
      if (SplitTime[2] == 'PM') {
        var SplitTime_v2 = SplitTime[1].split(':');
        var HH = (parseInt(SplitTime_v2[0]) + 12).toString();
        if (HH == 24) {
          HH = '00';
        }
        var mm = '';
        if (SplitTime_v2[1].length < 2) {
          mm = '0' + SplitTime_v2[1];
        } else {
          mm = SplitTime_v2[1];
        }
        var ss = '';
        if (SplitTime_v2[2].length < 2) {
          ss = '0' + SplitTime_v2[2];
        } else {
          ss = SplitTime_v2[2];
        }
      } else {
        var SplitTime_v2 = SplitTime[1].split(':');
        var HH = SplitTime_v2[0];
        var mm = '';
        if (SplitTime_v2[1].length < 2) {
          mm = '0' + SplitTime_v2[1];
        } else {
          mm = SplitTime_v2[1];
        }
        var ss = '';
        if (SplitTime_v2[2].length < 2) {
          ss = '0' + SplitTime_v2[2];
        } else {
          ss = SplitTime_v2[2];
        }
      }
      var time = HH + ':' + mm;
      //định dạng ngày hiện tại
      var MMNow = '';
      if ((new Date().getMonth() + 1).toString().length < 2) {
        MMNow = '0' + (new Date().getMonth() + 1).toString();
      } else {
        MMNow = (new Date().getMonth() + 1).toString();
      }
      var ddNow = '';
      if (new Date().getDate().toString().length < 2) {
        ddNow = '0' + new Date().getDate().toString();
      } else {
        ddNow = new Date().getDate().toString();
      }
      var SplitDateNow = ddNow + '-' + MMNow + '-' + new Date().getFullYear();
      if (date === SplitDateNow) {
        return time;
      } else {
        return date + ' ' + time;
      }
    } else {
      return '';
    }
  }
  //#endregion

  //#endregion
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Task_Comment_Component);
