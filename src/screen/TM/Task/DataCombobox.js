import {AppStyles, AppColors} from '@theme';
export default {
  DataMenuBottomCalendar: [
    {
      key: 'details',
      name: 'Xem',
      icon: {
        name: 'eye-outline',
        type: 'material-community',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'edit',
      name: 'Sửa',
      icon: {
        name: 'edit',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'delete',
      name: 'Xóa',
      icon: {
        name: 'delete',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'attachment',
      name: 'Đính kèm',
      icon: {
        name: 'attachment',
        type: 'entypo',
        color: AppColors.Maincolor,
      },
    },
  ],
  DataMenuBottomKanban: [
    {
      key: 'waiting',
      name: 'Chờ giao',
      icon: {
        name: 'user',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'notstart',
      name: 'Chờ xử lý',
      icon: {
        name: 'layers',
        type: 'feather',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'process',
      name: 'Đang thực hiện',
      icon: {
        name: 'pushpino',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'complete',
      name: 'Hoàn thành',
      icon: {
        name: 'Safety',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'waitsomeone',
      name: 'Chờ người khác',
      icon: {
        name: 'user-check',
        type: 'feather',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'expiry',
      name: 'Quá hạn',
      icon: {
        name: 'warning',
        type: 'entypo',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'deferred',
      name: 'Hoãn lại',
      icon: {
        name: 'flag',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
  ],
  MenuBottom: [
    // {
    //     key: 'Kanban',
    //     name: 'Kanban',
    //     icon: {
    //         name: 'eye-outline',
    //         type: 'material-community',
    //         color: AppColors.Maincolor,
    //     }
    // },
    {
      key: 'X',
      name: 'Chờ giao',
      icon: {
        name: 'edit',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'FOLLOW',
      name: 'Theo dõi',
      icon: {
        name: 'attachment',
        type: 'entypo',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'FOLLOW',
      name: 'Đã giao',
      icon: {
        name: 'message1',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'S',
      name: 'Công việc lưu',
      icon: {
        name: 'calendar-check-o',
        type: 'font-awesome',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'W',
      name: 'Chờ người khác',
      icon: {
        name: 'delete',
        type: 'antdesign',
        color: AppColors.Maincolor,
      },
    },
    {
      key: 'D',
      name: 'Hoãn lại',
      icon: {
        name: 'check-circle',
        type: 'feather',
        color: AppColors.Maincolor,
      },
    },
  ],
  ListComboboxProcess_N: [
    {
      value: '1',
      text: 'Công việc chờ xử lý theo ngày.',
    },
    {
      value: '2',
      text: 'Tất cả công việc chờ xử lý.',
    },
  ],
  ListComboboxProcess_P: [
    {
      value: '1',
      text: 'Công việc đang thực hiện theo ngày.',
    },
    {
      value: '2',
      text: 'Tất cả công việc đang thực hiện.',
    },
  ],
  ListComboboxProcess_C: [
    {
      value: '1',
      text: 'Công việc hoàn thành theo ngày.',
    },
    {
      value: '2',
      text: 'Tất cả công việc hoàn thành.',
    },
  ],
  ListComboboxProcess_E: [
    {
      value: '1',
      text: 'Công việc quá hạn theo ngày.',
    },
    {
      value: '2',
      text: 'Tất cả công việc quá hạn.',
    },
  ],
  ListComboboxProcess_D: [
    {
      value: '1',
      text: 'Công việc hoãn lại theo ngày.',
    },
    {
      value: '2',
      text: 'Tất cả công việc hoãn lại.',
    },
  ],
  ListComboboxProcess_X: [
    {
      value: '1',
      text: 'Công việc chờ giao theo ngày.',
    },
    {
      value: '2',
      text: 'Tất cả công việc chờ giao.',
    },
  ],
  ListComboboxProcess_W: [
    {
      value: '1',
      text: 'Công việc chờ người khác theo ngày.',
    },
    {
      value: '2',
      text: 'Tất cả công việc chờ người khác.',
    },
  ],
  ListComboboxProcess_F: [
    {
      value: '1',
      text: 'Công việc đã giao theo ngày.',
    },
    {
      value: '2',
      text: 'Tất cả công việc đã giao.',
    },
  ],
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  Level: [
    {value: 'M', text: 'Thấp'},
    {value: 'N', text: 'Trung bình'},
    {value: 'H', text: 'Cao'},
  ],
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //     value: 'camera',
    //     text: "Chụp ảnh"
    // }
  ],
  //#endregion
};
