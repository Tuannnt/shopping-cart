import {API_TM_TASKS, API_TM_CALENDAR, API} from '@network';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '@utils';
import Toast from 'react-native-simple-toast';
// Toast.showWithGravity("", Toast.SHORT, Toast.CENTER);
export default {
  // ====================================================== Tab List ======================================================
  //#region
  CountTask(data, callback) {
    var listtabbarBotom = [
      {
        Title: 'Chờ xử lý',
        Icon: 'layers',
        Type: 'feather',
        Value: 'N',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Đang thực hiện',
        Icon: 'pushpino',
        Type: 'antdesign',
        Value: 'P',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Hoàn thành',
        Icon: 'Safety',
        Type: 'antdesign',
        Value: 'C',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Quá hạn',
        Icon: 'warning',
        Type: 'antdesign',
        Value: 'E',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Thêm',
        Icon: 'dots-three-horizontal',
        Type: 'entypo',
        Value: 'T',
        OffEditcolor: true,
        Checkbox: false,
        Badge: 0,
      },
    ];
    API_TM_TASKS.CountTask(data)
      .then(rss => {
        var listBadge = listtabbarBotom;
        var _data = JSON.parse(rss.data.data);
        for (let i = 0; i < _data.length; i++) {
          if (_data[i].Name == 'notstart') {
            listBadge[0].Badge = _data[i].Value;
          } else if (_data[i].Name == 'process') {
            listBadge[1].Badge += _data[i].Value;
          } else if (_data[i].Name == 'complete') {
            listBadge[2].Badge += _data[i].Value;
          } else if (_data[i].Name == 'expiry') {
            listBadge[3].Badge += _data[i].Value;
          } else {
            // listBadge[4].Badge += _data[i].Value
          }
        }
        callback(listBadge);
      })
      .catch(error => {
        Toast.showWithGravity(
          'Đã xảy ra lỗi khi lấy dữ liệu',
          Toast.SHORT,
          Toast.CENTER,
        );
        Actions.pop();
      });
  },
  GetTasks(data, callback) {
    var dataSearch = {
      CategoryOfTaskGuid: null,
      EndDate: data.EndDate,
      ItemPage: 20,
      Page: data.Page,
      Priority: null,
      Process: data.Process,
      ProfileGuid: null,
      Search: data.txtSearch,
      StartDate: data.StartDate,
      TaskGuid: null,
    };
    API_TM_TASKS.GetTasks(dataSearch)
      .then(rs => {
        var _ListTask = [];
        var _data = JSON.parse(rs.data.data);
        for (let i = 0; i < _data.length; i++) {
          var _CountCLDone = 0;
          if (_data[i].IsAcceptTask !== 'N') {
            // trạng thái chờ chấp nhập xử lý công việc
            if (_data[i].IsAcceptTask === 'Y') {
              _data[i].IsAcceptTaskName = 'Đã chấp nhận';
            } else if (_data[i].IsAcceptTask === 'D') {
              _data[i].IsAcceptTaskName = 'Xác nhận sau';
            } else {
              _data[i].IsAcceptTaskName = 'Chờ xử lý';
            }
            if (_data[i].StatusCompletedTaskOfUser === 'C') {
              _data[i].Icon = 'check';
              _data[i].TypeIcon = 'antdesign';
            } else {
              _data[i].Icon = 'circle';
              _data[i].TypeIcon = 'entypo';
            }
            for (let l = 0; l < _data[i].CheckLists.length; l++) {
              if (_data[i].CheckLists[l].Status == true) {
                _data[i].CountCLDone = _CountCLDone + 1;
                _CountCLDone = _CountCLDone + 1;
              } else {
                _data[i].CountCLDone = _CountCLDone;
              }
            }
            _data[i].CheckParentId = false;
            _data[i].OpenparantId = false;
            _data[i].StyleparantId = {
              margin: 0,
            };
          }
          if (
            FuncCommon.ConDate(_data[i].EndDate, 0) <
              FuncCommon.ConDate(new Date(), 0) &&
            _data[i].StatusCompletedTaskOfUser !== 'C'
          ) {
            _data[i].StatusCompletedTaskOfUser = 'E';
          }
          _ListTask.push(_data[i]);
        }
        callback(_ListTask);
      })
      .catch(error => {
        Toast.showWithGravity(
          'Đã xảy ra lỗi khi lấy dữ liệu',
          Toast.SHORT,
          Toast.CENTER,
        );
        console.log(error);
        Actions.pop();
      });
  },
  Searchday(value, callback) {
    var _item = value.toString();
    if (_item != undefined && _item != '') {
      var yyyy = _item.substring(0, 4);
      var MM = _item.substring(4, 6);
      if (MM.substring(0, 1) == 0) {
        MM = MM.substring(1, 2);
      }
      var dd = _item.substring(6, 8);
      if (dd.substring(0, 1) == 0) {
        dd = dd.substring(1, 2);
      }
      var _startDate = yyyy + '-' + MM + '-' + dd + ' 00:00:00';
      var _EndDate = yyyy + '-' + MM + '-' + dd + ' 23:59:59';
      var data = {
        StartDate: _startDate,
        EndDate: _EndDate,
      };
      callback(data);
    }
  },
  Calendar_GetAll(value, callback) {
    API_TM_CALENDAR.Calendar_GetAll(value)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        // Alert.alert("Thông báo", "Đã xảy ra lỗi khi lấy dữ liệu hàm Calendar_GetAll " + error);
        Actions.pop();
      });
  },
  UpdateFollow(value, callback) {
    var obj = {IdS: [value.TaskGuid]};
    API_TM_TASKS.UpdateFollow(obj)
      .then(rs => {
        if (rs.data.errorCode == 200) {
          Toast.showWithGravity(
            'Cập nhật theo dõi công việc thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          callback();
        }
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Đã xảy ra lỗi khi Bật chế độ ghim theo dõi hàm UpdateFollow ' +
            error,
        );
      });
  },
  DeleteTask(value, callback) {
    let obj = {IdS: [value.TaskGuid]};
    API_TM_TASKS.Delete(obj)
      .then(rs => {
        Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
        callback(rs.data);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Đã xảy ra lỗi xoá công việc hàm DeleteTask ' + error,
        );
      });
  },
  RecallTask(value, callback) {
    let obj = {IdS: [value.TaskGuid]};
    API_TM_TASKS.RecallTask(obj)
      .then(rs => {
        Toast.showWithGravity(rs.data.Title, Toast.SHORT, Toast.CENTER);
        callback(rs.data);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Đã xảy ra lỗi xoá công việc hàm RecallTask ' + error,
        );
      });
  },
  InsertToCalenderOfUser(item) {
    let obj = {IdS: [item.TaskGuid]};
    API_TM_TASKS.InsertToCalenderOfUser(obj)
      .then(rs => {
        Toast.showWithGravity(rs.data.Title, Toast.SHORT, Toast.CENTER);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Đã xảy ra lỗi khi thêm mới vào lịch công việc hàm InsertToCalenderOfUser ' +
            error,
        );
      });
  },
  onViewParentId(item, list, callback) {
    var obj = {IdS: [item.TaskGuid]};
    API_TM_TASKS.GetChildTask(obj)
      .then(rs => {
        var _data = rs.data.Object;
        ListData = [];
        for (let i = 0; i < _data.length; i++) {
          if (_data[i].StatusCompletedTaskOfUser == 'C') {
            _data[i].Icon = 'check';
            _data[i].TypeIcon = 'antdesign';
          } else {
            _data[i].Icon = 'circle';
            _data[i].TypeIcon = 'entypo';
          }
          for (let l = 0; l < _data[i].CheckLists.length; l++) {
            if (_data[i].CheckLists[l].Status == true) {
              _data[i].CountCLDone = _CountCLDone + 1;
            } else {
              _data[i].CountCLDone = _CountCLDone;
            }
          }
          _data[i].CheckParentId = true;
          _data[i].OpenparantId = false;
          _data[i].StyleparantId = {
            margin: 0,
          };
          ListData.push(_data[i]);
        }
        var _list = list;
        var _listNew = [];
        for (let z = 0; z < _list.length; z++) {
          if (_list[z].TaskGuid == item.TaskGuid) {
            _list[z].OpenparantId = !_list[z].OpenparantId;
          }
          _listNew.push(_list[z]);
          for (let y = 0; y < ListData.length; y++) {
            if (_list[z].TaskGuid == ListData[y].ParentId) {
              ListData[y].StyleparantId.margin =
                _list[z].StyleparantId.margin + 10;
              _listNew.push(ListData[y]);
            }
          }
        }
        callback(_listNew);
      })
      .catch(error => {
        console.log('đã xảy ra lỗi khi lấy dữ liệu con');
      });
  },
  onClearParentId(item, list, callback) {
    var _list = list;
    var _listNew = [];
    var listRemove = [];
    for (let z = 0; z < _list.length; z++) {
      if (_list[z].TaskGuid == item.TaskGuid) {
        _list[z].OpenparantId = !_list[z].OpenparantId;
      }
      if (_list[z].ParentId == item.TaskGuid) {
        listRemove.push(_list[z]);
      } else {
        if (listRemove.length > 0) {
          var obj = listRemove.find(x => x.TaskGuid == _list[z].ParentId);
          if (obj !== null && obj !== undefined) {
            listRemove.push(_list[z]);
          } else {
            _listNew.push(_list[z]);
          }
        } else {
          _listNew.push(_list[z]);
        }
      }
    }
    callback(_listNew);
  },
  UpdateAccept(item, callback) {
    API_TM_TASKS.UpdateAccept(item)
      .then(rs => {
        if (rs.data.Error === false) {
          callback();
        }
      })
      .catch(error => {
        Alert.alert('Thông báo', 'Đã xảy ra lỗi hàm UpdateAccept ' + error);
      });
  },
  ListTask_Kanban(item, callback) {
    var dataSearch = {
      EndDate: null,
      ItemPage: 15,
      Page: item.Page,
      Priority: null,
      Process: item.Process,
      ProfileGuid: null,
      Search: item.Search,
      StartDate: null,
      TaskGuid: null,
    };
    API_TM_TASKS.GetTasks(dataSearch)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        Alert.alert('Thông báo', 'Đã xảy ra lỗi hàm ListTask_Kanban ' + error);
      });
  },
  Submit_Kanban(selectItem, _StatusCompletedTaskOfUser, _percentComplete) {
    var obj = {
      TaskGuid: selectItem.TaskGuid,
      Subject: selectItem.Subject,
      PercentComplete: _percentComplete,
      StatusCompletedTaskOfUser: _StatusCompletedTaskOfUser,
      TimeReminder: null,
      Note: '',
      StatusType: 2,
    };
    let _data = new FormData();
    _data.append('update', JSON.stringify(obj));
    API_TM_TASKS.UpdateProcess(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Alert.alert(
            'Thông báo',
            'Có lỗi xảy ra khi cập nhật',
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
        } else {
          Alert.alert(
            'Thông báo',
            'Cập nhật tiến độ thành công',
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
        }
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Có lỗi xảy ra khi cập nhật hàm UpdateProcess ' + error,
        );
      });
  },
  onViewParentId_Kanban(item, data, callback) {
    var obj = {IdS: [item.TaskGuid]};
    API_TM_TASKS.GetAllChildTask(obj)
      .then(rs => {
        var _DataMenuKanban = data;
        var _DataMenuKanbanNew = [];
        for (let i = 0; i < _DataMenuKanban.length; i++) {
          if (_DataMenuKanban[i].code == item.StatusCompletedTaskOfUser) {
            var dataValue = _DataMenuKanban[i].Value;
            var dataValueNew = [];
            for (let y = 0; y < dataValue.length; y++) {
              if (dataValue[y].TaskGuid == item.TaskGuid) {
                dataValue[y].OpenparantId = !dataValue[y].OpenparantId;
                dataValue[y].DataChild = rs.data.Object;
              }
              dataValueNew.push(dataValue[y]);
            }
          }
          _DataMenuKanbanNew.push(_DataMenuKanban[i]);
        }
        callback(_DataMenuKanbanNew);
      })
      .catch(error => {
        console.log('đã xảy ra lỗi khi lấy dữ liệu con');
      });
  },
  onClearParentId_Kanban(item, data, callback) {
    var _DataMenuKanban = data;
    var _DataMenuKanbanNew = [];
    for (let i = 0; i < _DataMenuKanban.length; i++) {
      if (_DataMenuKanban[i].code == item.StatusCompletedTaskOfUser) {
        var dataValue = _DataMenuKanban[i].Value;
        var dataValueNew = [];
        for (let y = 0; y < dataValue.length; y++) {
          if (dataValue[y].TaskGuid == item.TaskGuid) {
            dataValue[y].OpenparantId = !dataValue[y].OpenparantId;
          }
          dataValueNew.push(dataValue[y]);
        }
      }
      _DataMenuKanbanNew.push(_DataMenuKanban[i]);
    }
    callback(_DataMenuKanbanNew);
  },
  sortSubject(status, list, callback) {
    if (status === false) {
      list.sort(function(a, b) {
        if (a.Subject < b.Subject) {
          return -1;
        }
        if (a.Subject > b.Subject) {
          return 1;
        }
        return 0;
      });
    } else {
      list.sort(function(a, b) {
        if (a.Subject > b.Subject) {
          return -1;
        }
        if (a.Subject < b.Subject) {
          return 1;
        }
        return 0;
      });
    }
    status = !status;
    var obj = {
      data: list,
      status: status,
    };
    callback(obj);
  },
  //#endregion

  // ===================================================== Tab Detail ======================================================
  //#region
  GetData(data, callback) {
    var obj = {
      TaskGuid: data,
    };
    API_TM_TASKS.GetItemTasks(obj)
      .then(res => {
        if (res.data.data !== 'null') {
          var _data = JSON.parse(res.data.data);
          var _listUserApproval = [];
          var _listUserProcess = [];
          var _listUserFollow = [];
          for (let i = 0; i < _data.TaskOfUsers.length; i++) {
            if (_data.TaskOfUsers[i].IsOwner == 'A') {
              _listUserApproval.push(_data.TaskOfUsers[i]);
            } else if (_data.TaskOfUsers[i].IsOwner == 'B') {
              _listUserProcess.push(_data.TaskOfUsers[i]);
            } else if (_data.TaskOfUsers[i].IsOwner == 'C') {
              _listUserFollow.push(_data.TaskOfUsers[i]);
            } else {
              null;
            }
          }
          if (
            FuncCommon.ConDate(_data.EndDate, 0) <
              FuncCommon.ConDate(new Date(), 0) &&
            _data.StatusCompletedTaskOfUser !== 'C'
          ) {
            _data.StatusCompletedTaskOfUser = 'E';
          }
          callback({
            DataView: _data,
            ListUserApproval: _listUserApproval,
            ListUserProcess: _listUserProcess,
            ListUserFollow: _listUserFollow,
          });
        } else {
          Toast.showWithGravity(
            'Bản ghi này không còn tồn tại.',
            Toast.SHORT,
            Toast.CENTER,
          );
          Actions.pop();
        }
      })
      .catch(error => {
        Alert.alert('Thông báo', 'Đã xảy ra lỗi khi lấy dữ liệu');
        Actions.pop();
      });
  },
  UpdateCheckList(list, index, callback) {
    var obj = {IdS: [list[index].CheckListGuid]};
    API_TM_TASKS.UpdateCheckList(obj)
      .then(rs => {
        if (rs.data.Error === false) {
          list[index].Status = !list[index].Status;
          callback(list);
        } else {
          callback([]);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log('Error when call API Mobile.');
      });
  },

  //#endregion

  // ===================================================== Tab update ======================================================
  //#region
  GetItemTasks(data, callback) {
    var obj = {
      TaskGuid: data,
    };
    API_TM_TASKS.GetItemTasks(obj)
      .then(res => {
        var _data = JSON.parse(res.data.data);
        var _ListCheckList = [];
        var _listUserApproval = [];
        var _listUserProcess = [];
        var _listUserFollow = [];
        for (let i = 0; i < _data.TaskOfUsers.length; i++) {
          if (_data.TaskOfUsers[i].IsOwner == 'A') {
            _listUserApproval.push(_data.TaskOfUsers[i].EmployeeGuid);
          } else if (_data.TaskOfUsers[i].IsOwner == 'B') {
            _listUserProcess.push(_data.TaskOfUsers[i].EmployeeGuid);
          } else if (_data.TaskOfUsers[i].IsOwner == 'C') {
            _listUserFollow.push(_data.TaskOfUsers[i].EmployeeGuid);
          } else {
            null;
          }
        }
        for (let y = 0; y < _data.CheckList.length; y++) {
          _ListCheckList.push({
            Id: y + 1,
            Status:
              _data.CheckList[y].Status == null
                ? false
                : _data.CheckList[y].Status,
            CheckListTitle: _data.CheckList[y].CheckListTitle,
          });
        }
        var obj = {
          DataUp: _data,
          CheckList: _ListCheckList,
          _listUserApproval: _listUserApproval,
          _listUserProcess: _listUserProcess,
          _listUserFollow: _listUserFollow,
        };
        callback(obj);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Có lỗi xảy ra khi lấy dữ liệu hàm GetItemTasks ' + error,
        );
        Actions.pop();
      });
  },
  GetAttList(data, callback) {
    var _obj = {IdS: [data]};
    API_TM_TASKS.GetAttList(_obj)
      .then(res => {
        var _data = JSON.parse(res.data.data);
        callback(_data);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Có lỗi xảy ra khi lấy dữ liệu hàm GetAttList ' + error,
        );
        Actions.pop();
      });
  },
  ListCombobox_Employee(data, callback) {
    API_TM_TASKS.ListCombobox_Employee()
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var _listComboboxA = [];
        var _listComboboxP = [];
        var _listComboboxL = [];
        for (let i = 0; i < _data.length; i++) {
          _listComboboxA.push({
            value: _data[i].EmployeeGuid,
            text: _data[i].FullName,
            emp: _data[i],
          });
          _listComboboxP.push({
            value: _data[i].EmployeeGuid,
            text: _data[i].FullName,
            emp: _data[i],
          });
          _listComboboxL.push({
            value: _data[i].EmployeeGuid,
            text: _data[i].FullName,
            emp: _data[i],
          });
        }
        var obj = {
          ListComboboxApproval: _listComboboxA,
          ListComboboxProcess: _listComboboxP,
          ListComboboxFollow: _listComboboxL,
        };
        callback(obj);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Có lỗi xảy ra khi lấy danh sách nhân viên hàm ListCombobox_Employee ' +
            error,
        );
        Actions.pop();
      });
  },
  //lấy theo nhóm công việc
  GetEmployeeByCategoryOfTask(data, callback) {
    var obj = {
      CategoryOfTaskGuid: data,
    };
    API_TM_TASKS.GetEmployeeByCategoryOfTask(obj)
      .then(rs => {
        var _data = rs.data.Object;
        var _listComboboxA = [];
        var _listComboboxP = [];
        var _listComboboxL = [];
        for (let i = 0; i < _data.length; i++) {
          _listComboboxA.push({
            value: _data[i].EmployeeGuid,
            text: _data[i].FullName,
            emp: _data[i],
          });
          _listComboboxP.push({
            value: _data[i].EmployeeGuid,
            text: _data[i].FullName,
            emp: _data[i],
          });
          _listComboboxL.push({
            value: _data[i].EmployeeGuid,
            text: _data[i].FullName,
            emp: _data[i],
          });
        }
        var obj = {
          ListComboboxApproval: _listComboboxA,
          ListComboboxProcess: _listComboboxP,
          ListComboboxFollow: _listComboboxL,
        };
        callback(obj);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Có lỗi xảy ra khi lấy danh sách nhân viên hàm GetEmployeeByCategoryOfTask ' +
            error,
        );
        Actions.pop();
      });
  },
  GetTreeDataCOT(data, callback) {
    API_TM_TASKS.GetTreeDataCOT()
      .then(res => {
        var _data = res.data;
        var _listView = [];
        for (let i = 0; i < _data.length; i++) {
          _listView.push({
            value: _data[i].Id,
            text: _data[i].Title,
          });
        }
        callback(_listView);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Có lỗi xảy ra khi lấy dữ liệu hàm GetTreeDataCOT ' + error,
        );
        Actions.pop();
      });
  },
  removeAttactment(data, list, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },
  removeArroval(item, ListApproval, ApprovalName, callback) {
    var list = [];
    var listV2 = [];
    for (let i = 0; i < ListApproval.length; i++) {
      if (ListApproval[i] !== item.value) {
        list.push(ListApproval[i]);
      }
    }
    for (let y = 0; y < ApprovalName.length; y++) {
      if (ApprovalName[y].value !== item.value) {
        listV2.push(ApprovalName[y]);
      }
    }
    var obj = {
      ListApproval: list,
      ApprovalName: listV2,
    };
    callback(obj);
  },
  removeProcess(item, ListProcess, ProcessName, callback) {
    var list = [];
    var listV2 = [];
    for (let i = 0; i < ListProcess.length; i++) {
      if (ListProcess[i] !== item.value) {
        list.push(ListProcess[i]);
      }
    }
    for (let y = 0; y < ProcessName.length; y++) {
      if (ProcessName[y].value !== item.value) {
        listV2.push(ProcessName[y]);
      }
    }
    var obj = {
      ListProcess: list,
      ProcessName: listV2,
    };
    callback(obj);
  },
  removeFollow(item, ListFollow, FollowName, callback) {
    var list = [];
    var listV2 = [];
    for (let i = 0; i < ListFollow.length; i++) {
      if (ListFollow[i] !== item.value) {
        list.push(ListFollow[i]);
      }
    }
    for (let y = 0; y < FollowName.length; y++) {
      if (FollowName[y].value !== item.value) {
        listV2.push(FollowName[y]);
      }
    }
    var obj = {
      ListFollow: list,
      FollowName: listV2,
    };
    callback(obj);
  },
  Tasks_Update(
    ListUserApproval,
    ListUserProcess,
    ListUserFollow,
    ListApproval,
    ListProcess,
    ListFollow,
    DataUp,
    CheckList,
    OldAttach,
    Attachment,
    Label,
    callback,
  ) {
    //insert - delete check list
    var dataInsertChecklist = [];
    for (let x = 0; x < CheckList.length; x++) {
      if (
        CheckList[x].CheckListTitle != undefined &&
        CheckList[x].CheckListTitle != '' &&
        CheckList[x].CheckListTitle != null
      ) {
        dataInsertChecklist.push({
          CheckListTitle: CheckList[x].CheckListTitle,
          Status: CheckList[x].Status,
        });
      }
    }
    // insert-delete  người chủ trì
    var dataApproval = ListApproval;
    var dataUserApproval = ListUserApproval;
    var dataInsertApproval = [];
    var dataDeleteApproval = [];
    for (let e = 0; e < dataUserApproval.length; e++) {
      let rs = dataApproval.find(x => x == dataUserApproval[e]);
      if (rs == null) {
        dataDeleteApproval.push(dataUserApproval[e]);
      }
    }
    for (let w = 0; w < dataApproval.length; w++) {
      let rs = dataUserApproval.find(y => y == dataApproval[w]);
      if (rs == null) {
        dataInsertApproval.push(dataApproval[w]);
      }
    }

    // insert-delete người phối hợp
    var dataProcess = ListProcess;
    var dataUserProcess = ListUserProcess;
    var dataInsertProcess = [];
    var dataDeleteProcess = [];
    for (let r = 0; r < dataUserProcess.length; r++) {
      let rs = dataProcess.find(x => x == dataUserProcess[r]);
      if (rs == null) {
        dataDeleteProcess.push(dataUserProcess[r]);
      }
    }
    for (let t = 0; t < dataProcess.length; t++) {
      let rs = dataUserProcess.find(y => y == dataProcess[t]);
      if (rs == null) {
        dataInsertProcess.push(dataProcess[t]);
      }
    }
    // insert-delete  người theo dõi
    var dataFollow = ListFollow;
    var dataUserFollow = ListUserFollow;
    var dataInsertFollow = [];
    var dataDeleteFollow = [];
    for (let u = 0; u < dataUserFollow.length; u++) {
      let rs = dataFollow.find(x => x == dataUserFollow[u]);
      if (rs == null) {
        dataDeleteFollow.push(dataUserFollow[u]);
      }
    }
    for (let p = 0; p < dataFollow.length; p++) {
      let rs = dataUserFollow.find(y => y == dataFollow[p]);
      if (rs == null) {
        dataInsertFollow.push(dataFollow[p]);
      }
    }
    var _insertTOU = {
      TaskGuid1: dataInsertApproval,
      TaskGuid2: dataInsertProcess,
      TaskGuid3: dataInsertFollow,
    };
    var _deletetTOU = {
      TaskGuid1: dataDeleteApproval,
      TaskGuid2: dataDeleteProcess,
      TaskGuid3: dataDeleteFollow,
    };
    var _activeMail = {
      IsSendApproval: false,
      IsSendProcess: false,
      IsSendFollow: false,
    };
    //insert - delete att
    var Att = Attachment;
    var InsertAtt = [];
    var DeleteAtt = [];
    var _lstTitlefile = [];
    var stt = 0;
    for (let z = 0; z < OldAttach.length; z++) {
      let rs = Att.find(x => x.AttachmentGuid == OldAttach[z].AttachmentGuid);
      if (rs == null) {
        DeleteAtt.push(OldAttach[z].AttachmentGuid);
      }
    }
    for (let x = 0; x < Att.length; x++) {
      let rs = OldAttach.find(y => y.AttachmentGuid == Att[x].AttachmentGuid);
      if (rs == null && rs == undefined) {
        _lstTitlefile.push({
          STT: stt,
          Title: Att[x].FileName,
        });
        InsertAtt.push(Att[x]);
        stt = ++stt;
      }
    }
    var setLabel = [];

    if (Label !== '') {
      for (let i = 0; i < Label.length; i++) {
        setLabel.push({LabelId: Label[i].LabelId});
      }
    }
    var _UpdateV2 = {
      StartDate: FuncCommon.ConDate(DataUp.StartDate, 0),
      EndDate: FuncCommon.ConDate(DataUp.EndDate, 0),
    };
    DataUp.StartDate =
      DataUp.StartDate !== null
        ? FuncCommon.ConDate(DataUp.StartDate, 2)
        : null;
    DataUp.EndDate =
      DataUp.EndDate !== null ? FuncCommon.ConDate(DataUp.EndDate, 2) : null;

    let _data = new FormData();
    _data.append('update', JSON.stringify(DataUp));
    _data.append('updateV2', JSON.stringify(_UpdateV2));
    _data.append('insertTOU', JSON.stringify(_insertTOU));
    _data.append('deletetTOU', JSON.stringify(_deletetTOU));
    _data.append('deletetCL', JSON.stringify([]));
    _data.append('insertCL', JSON.stringify(dataInsertChecklist));
    _data.append('ActiveMail', JSON.stringify(_activeMail));
    _data.append('Label', JSON.stringify(setLabel));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    _data.append('DeleteAttach', JSON.stringify(DeleteAtt));
    _data.append('Recurrence', null);
    for (var i = 0; i < InsertAtt.length; i++) {
      _data.append(InsertAtt[i].FileName, {
        name: InsertAtt[i].FileName,
        size: InsertAtt[i].size,
        type: InsertAtt[i].type,
        uri: InsertAtt[i].uri,
      });
    }
    API_TM_TASKS.Tasks_Update(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          callback({error: true, title: _rs.message});
        } else {
          callback({error: false, title: _rs.message});
        }
      })
      .catch(error => {
        Alert.alert('Thông báo', 'Cập nhật không thành công' + error);
        console.log(error);
      });
  },
  Submit(
    ListApproval,
    ListProcess,
    ListFollow,
    DataUp,
    CheckList,
    Attachment,
    Emp,
    Label,
    callback,
  ) {
    var _insertCl = [];
    var _listCheckList = CheckList;
    for (let i = 0; i < _listCheckList.length; i++) {
      if (
        _listCheckList[i].CheckListTitle !== undefined &&
        _listCheckList[i].CheckListTitle !== null &&
        _listCheckList[i].CheckListTitle !== ''
      ) {
        _insertCl.push({
          CheckListTitle: _listCheckList[i].CheckListTitle,
          Status: _listCheckList[i].Status,
        });
      }
    }
    var _activeMail = {
      IsSendApproval: false,
      IsSendProcess: false,
      IsSendFollow: false,
    };
    var _insertTOU = {
      TaskGuid1: ListApproval,
      TaskGuid2: ListProcess,
      TaskGuid3: ListFollow,
    };
    let _obj = {
      StartDate: FuncCommon.ConDate(DataUp.StartDate, 2),
      EndDate: FuncCommon.ConDate(DataUp.EndDate, 2),
      TaskContent: DataUp.TaskContent == null ? '' : DataUp.TaskContent,
      Priority: DataUp.Priority,
      CategoryOfTaskGuid: DataUp.CategoryOfTaskGuid,
      StatusCompleted: DataUp.StatusCompleted,
      Subject: DataUp.Subject,
    };
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    let _data = new FormData();
    _data.append('TaskInsertOption', JSON.stringify({Option: 'ADD'}));
    _data.append('insertCL', JSON.stringify(_insertCl));
    _data.append('ActiveMail', JSON.stringify(_activeMail));
    _data.append('insertTOU', JSON.stringify(_insertTOU));
    _data.append('insert', JSON.stringify(_obj));
    _data.append('Recurrence', null);
    _data.append('Label', JSON.stringify(Label));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    _data.append('DeleteAttach', JSON.stringify([]));
    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_TM_TASKS.Insert(_data)
      .then(rs => {
        callback(rs.data);
      })
      .catch(error => {
        console.log('Đã xảy ra lỗi khi thêm mới.');
        console.log(error);
      });
  },
  GetLabels(data, callback) {
    API_TM_TASKS.GetLabels()
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        for (let i = 0; i < _data.length; i++) {
          list.push({
            value: _data[i].LabelId,
            text: _data[i].LabelName,
            Class: _data[i].Class,
          });
        }
        callback(list);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Có lỗi xảy ra khi lấy danh sách nhãn công việc hàm GetLabels ' +
            error,
        );
        Actions.pop();
      });
  },
  CheckPermission(data, callback) {
    var obj = {IdS: [data]};
    API_TM_TASKS.CheckPermission(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        Toast.showWithGravity(
          'Có lỗi xảy ra khi lấy danh sách quyền hạn',
          Toast.SHORT,
          Toast.CENTER,
        );
      });
  },
};
