import React, { Component, } from 'react';
import { Text, View, Animated, Dimensions, TouchableHighlight, TouchableOpacity, StyleSheet, ScrollView, KeyboardAvoidingView, Image } from "react-native";
import { AppStyles, AppColors, AppSizes } from '@theme';
import { WebView } from 'react-native-webview';
import { Divider, Icon, ListItem, } from 'react-native-elements';
import HTML from "react-native-render-html";
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
}
const TimeOut = 400
class ViewDetail_TaskContent_Coponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TabViewOffsetY: new Animated.Value(1000),
            opacityTabView: new Animated.Value(0),
            Sort_offsetX: new Animated.Value(DRIVER.width),
            Theme_offsetX: new Animated.Value(DRIVER.width),
            ButtonBack: new Animated.Value(1),
            height: 50 * 5 + 30,
            isOpen: false,
            isOpenTheme: false,
            dataSubmit: null,
            NameMenu: props.nameMenu ? props.nameMenu : 'Tuỳ chỉnh',
            CheckAllValue: false,
            ClickColor: true,
            ClickImage: false,
            data: props.data
        };
        props.eOpen(this.eOpen);
        this.actionClick = props.callback;
    }
    actionClick() { }
    eOpen = (option) => {
        Animated.spring(
            this.state.ButtonBack,
            {
                toValue: 0,
                duration: 0,
                useNativeDriver: true
            }
        ).start();
        if (!this.state.isOpen) {
            Animated.sequence([this.opacityTabView]).start();
            Animated.sequence([this.OpenTabView]).start();
        } else {
            Animated.sequence([this.OffopacityTabView]).start();
            Animated.sequence([this.OffTabView]).start();

        }
        if (option) {
            if (option.isChangeAction) {
                this.setState({
                    data: option.data,
                })
            }
            if (option.NameMenu !== undefined && option.NameMenu !== null) {
                this.setState({
                    NameMenu: option.NameMenu,
                })
            }
        }
        if (!this.state.isOpen) {
            this.setState({
                isOpen: true,
            })
        } else {
            setTimeout(() => {
                this.setState({
                    isOpen: false,
                })
            }, 500);
        }

    }
    componentDidMount() {
        this.SkillAnimated();
    }
    callBack = (d) => {
        this.actionClick(d);
        this.eOpen();
    }
    render() {
        const ButtonBack = this.state.ButtonBack.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
        })
        const opacityTabView = this.state.opacityTabView.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0.5]
        })
        return (
            this.state && this.state.isOpen ?
                <Animated.View style={[
                    {
                        width: DRIVER.width,
                        position: "absolute",
                        height: DRIVER.height + 10,
                    }]}
                >
                    <Animated.View style={[{ backgroundColor: 'black', opacity: opacityTabView, height: DRIVER.height }]}>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.eOpen()} />
                    </Animated.View>
                    <Animated.View style={{
                        backgroundColor: 'white',
                        bottom: 0,
                        position: 'absolute',
                        width: DRIVER.width,
                        borderRadius: 10,
                        transform: [{ translateY: this.state.TabViewOffsetY }]
                    }}>
                        <View style={{ flexDirection: 'column', bottom: 0, borderRadius: 10, width: DRIVER.width, height: DRIVER.height - 100 }}>
                            <View style={{ flexDirection: 'column', paddingBottom: 10, paddingTop: 20, justifyContent: 'flex-end' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <View>
                                        <Text style={{ fontSize: 18 }}>
                                            Nội dung công việc
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <Divider />
                            <View style={{ flex: 1, padding: 10 }}>
                                <ScrollView>
                                    <TouchableHighlight >
                                        <HTML source={{ html: this.state.data }} contentWidth={DRIVER.width} />
                                    </TouchableHighlight >
                                </ScrollView>
                            </View>
                        </View>
                    </Animated.View >
                </Animated.View >
                : null
        );
    }
    ClickCategoryTheme = (val) => {
        if (val === "Color") {
            this.setState({ ClickColor: true, ClickImage: false });
        } else {
            this.setState({ ClickImage: true, ClickColor: false });
        }
    }

    PageTheme = () => {
        this.state.height = 50 * 1 + 30;
        Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: this.state.height,
                duration: TimeOut,
                useNativeDriver: true,
            }
        ).start();
        Animated.sequence([this.OnTabTheme]).start();
        Animated.sequence([this.ButtonBack]).start();
    }

    PageSort = () => {
        this.state.height = 30;
        Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: this.state.height,
                duration: TimeOut,
                useNativeDriver: true,
            }
        ).start();
        Animated.sequence([this.OnTabSort]).start();
        Animated.sequence([this.ButtonBack]).start();
    }

    BackTab = () => {
        Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        ).start();
        Animated.sequence([this.OffButtonBack]).start();
        Animated.sequence([this.OffTabTheme]).start();
        Animated.sequence([this.OffTabSort]).start();
    }
    openActionTheme = () => {
        if (!this.state.isOpenTheme) {

        } else {

        }
    }

    //#region SkillAnimated
    SkillAnimated = () => {

        //#region Tab chính

        //#region bật tab
        this.OpenTabView = Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region đóng tab
        this.OffTabView = Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: DRIVER.height,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region  mờ tab
        this.opacityTabView = Animated.timing(
            this.state.opacityTabView,
            {
                toValue: 1,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region  mờ tab
        this.OffopacityTabView = Animated.timing(
            this.state.opacityTabView,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#endregion

        //#region Tab con

        //#region mở tap theme
        this.OnTabTheme = Animated.timing(
            this.state.Theme_offsetX,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region đóng tab Theme
        this.OffTabTheme = Animated.timing(
            this.state.Theme_offsetX,
            {
                toValue: DRIVER.width,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region mở tap Sort
        this.OnTabSort = Animated.timing(
            this.state.Sort_offsetX,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region đóng tab Sort
        this.OffTabSort = Animated.timing(
            this.state.Sort_offsetX,
            {
                toValue: DRIVER.width,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#endregion

        //#region Nut chuyển hướng
        this.ButtonBack = Animated.timing(
            this.state.ButtonBack,
            {
                toValue: 1,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        this.OffButtonBack = Animated.timing(
            this.state.ButtonBack,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

    }

    //#endregion
}
export default ViewDetail_TaskContent_Coponent;