import React, { Component } from 'react';
import { FlatList, Keyboard, RefreshControl, Text, TextInput, StyleSheet, View, Dimensions, TouchableOpacity, Alert, ScrollView, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Divider, Icon, ListItem, } from 'react-native-elements';
import { API_TM_TASKS, API_TM_CALENDAR, API } from '@network';
import TabBar from '../../component/TabBar';
import { AppStyles, AppColors } from '@theme';
import FormSearch from '../../component/FormSearch';
import SearchDay from '../../component/SearchDay';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import * as Progress from 'react-native-progress';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import { FuncCommon } from '@utils';
import { DataCombobox,  } from './DataCombobox';
import { controller } from './controller';
import LoadingComponent from '../../component/LoadingComponent';
import ComboboxV2 from '../../component/ComboboxV2';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Kanban_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectItem: null,
            Count_Tab: 0,
            List_CG: [],
            List_CXL: [],
            List_DXL: [],
            List_HT: [],
            List_CVL: [],
            List_CNK: [],
            List_HL: [],
            List_QH: [],
            setSearch_CG: false,
            setSearch_CXL: false,
            setSearch_DXL: false,
            setSearch_HT: false,
            setSearch_CVL: false,
            setSearch_CNK: false,
            setSearch_HL: false,
            setSearch_QH: false,
            StatusSort_CG: false,
            StatusSort_CXL: false,
            StatusSort_DXL: false,
            StatusSort_HT: false,
            StatusSort_CVL: false,
            StatusSort_CNK: false,
            StatusSort_HL: false,
            StatusSort_QH: false,
        };
        this._search_Kanban = {
            Process: "X",
            Page_CG: 0,
            Search_CG: "",
            Page_CXL: 0,
            Search_CXL: "",
            Page_DXL: 0,
            Search_DXL: "",
            Page_HT: 0,
            Search_HT: "",
            Page_CVL: 0,
            Search_CVL: "",
            Page_CNK: 0,
            Search_CNK: "",
            Page_HL: 0,
            Search_HL: "",
            Page_QH: 0,
            Search_QH: "",

        };
        this.props.ReloadKanban(this._reloadData);
    }
    componentDidMount() {
        this.setState({ loading: true });
        this.ListTask_Kanban(true)
    }
    _reloadData = () => {
        this.setState({ loading: true });
        this.ListTask_Kanban(true)
    }
    //#region Lấy danh sách kanban
    Search_Kanban = (search) => {
        this.setState({ loading: true });
        switch (this.state.Count_Tab) {
            case 0:
                this._search_Kanban.Search_CG = search; this.ListTask_Kanban(true);
                break;
            case 1:
                this._search_Kanban.Search_CXL = search; this.ListTask_Kanban(true);
                break;
            case 2:
                this._search_Kanban.Search_DXL = search; this.ListTask_Kanban(true);
                break;
            case 3:
                this._search_Kanban.Search_HT = search; this.ListTask_Kanban(true);
                break;
            case 4:
                this._search_Kanban.Search_CVL = search; this.ListTask_Kanban(true);
                break;
            case 5:
                this._search_Kanban.Search_CNK = search; this.ListTask_Kanban(true);
                break;
            case 6:
                this._search_Kanban.Search_HL = search; this.ListTask_Kanban(true);
                break;
            case 7:
                this._search_Kanban.Search_QH = search; this.ListTask_Kanban(true);
                break;
            default:
                break;
        }
    };
    tabcheck = (val) => {
        switch (this.state.Count_Tab) {
            case 0:
                if (val >= 15 * this._search_Kanban.Page_CG) { this.ListTask_Kanban(false); }
                break;
            case 1:
                if (val >= 15 * this._search_Kanban.Page_CXL) { this.ListTask_Kanban(false); }
                break;
            case 2:
                if (val >= 15 * this._search_Kanban.Page_DXL) { this.ListTask_Kanban(false); }
                break;
            case 3:
                if (val >= 15 * this._search_Kanban.Page_HT) { this.ListTask_Kanban(false); }
                break;
            case 4:
                if (val >= 15 * this._search_Kanban.Page_CVL) { this.ListTask_Kanban(false); }
                break;
            case 5:
                if (val >= 15 * this._search_Kanban.Page_CNK) { this.ListTask_Kanban(false); }
                break;
            case 6:
                if (val >= 15 * this._search_Kanban.Page_HL) { this.ListTask_Kanban(false); }
                break;
            case 7:
                if (val >= 15 * this._search_Kanban.Page_QH) { this.ListTask_Kanban(false); }
                break;
            default:
                break;
        }
    }
    ListTask_Kanban(status) {
        var obj = null;
        switch (this.state.Count_Tab) {
            case 0:
                if (status === true) { this.setState({ List_CG: [] }) };
                this._search_Kanban.Page_CG = status === true ? 1 : this._search_Kanban.Page_CG++;
                obj = { Page: this._search_Kanban.Page_CG, Search: this._search_Kanban.Search_CG, Process: "X" };
                break;
            case 1:
                if (status === true) { this.setState({ List_CXL: [] }) };
                this._search_Kanban.Page_CXL = status === true ? 1 : this._search_Kanban.Page_CXL++;
                obj = { Page: this._search_Kanban.Page_CXL, Search: this._search_Kanban.Search_CXL, Process: "N" };
                break;
            case 2:
                if (status === true) { this.setState({ List_DXL: [] }) };
                this._search_Kanban.Page_DXL = status === true ? 1 : this._search_Kanban.Page_DXL++;
                obj = { Page: this._search_Kanban.Page_DXL, Search: this._search_Kanban.Search_DXL, Process: "P" };
                break;
            case 3:
                if (status === true) { this.setState({ List_DHT: [] }) };
                this._search_Kanban.Page_HT = status === true ? 1 : this._search_Kanban.Page_HT++;
                obj = { Page: this._search_Kanban.Page_HT, Search: this._search_Kanban.Search_HT, Process: "C" };
                break;
            case 4:
                if (status === true) { this.setState({ List_CVL: [] }) };
                this._search_Kanban.Page_CVL = status === true ? 1 : this._search_Kanban.Page_CVL++;
                obj = { Page: this._search_Kanban.Page_CVL, Search: this._search_Kanban.Search_CVL, Process: "S" };
                break;
            case 5:
                if (status === true) { this.setState({ List_CNK: [] }) };
                this._search_Kanban.Page_CNK = status === true ? 1 : this._search_Kanban.Page_CNK++;
                obj = { Page: this._search_Kanban.Page_CNK, Search: this._search_Kanban.Search_CNK, Process: "W" };
                break;
            case 6:
                if (status === true) { this.setState({ List_HL: [] }) };
                this._search_Kanban.Page_HL = status === true ? 1 : this._search_Kanban.Page_HL++;
                obj = { Page: this._search_Kanban.Page_HL, Search: this._search_Kanban.Search_HL, Process: "D" };
                break;
            case 7:
                if (status === true) { this.setState({ List_QH: [] }) };
                this._search_Kanban.Page_QH = status === true ? 1 : this._search_Kanban.Page_QH++;
                obj = { Page: this._search_Kanban.Page_QH, Search: this._search_Kanban.Search_QH, Process: "E" };
                break;

            default:
                break;
        }
        controller.ListTask_Kanban(obj, rs => {
            switch (this.state.Count_Tab) {
                case 0:
                    for (let i = 0; i < rs.length; i++) { this.state.List_CG.push(rs[i]); } this.setState({ List_CG: this.state.List_CG });
                    break;
                case 1:
                    for (let i = 0; i < rs.length; i++) { this.state.List_CXL.push(rs[i]); } this.setState({ List_CXL: this.state.List_CXL });
                    break;
                case 2:
                    for (let i = 0; i < rs.length; i++) { this.state.List_DXL.push(rs[i]); } this.setState({ List_DXL: this.state.List_DXL });
                    break;
                case 3:
                    for (let i = 0; i < rs.length; i++) { this.state.List_HT.push(rs[i]); } this.setState({ List_HT: this.state.List_HT });
                    break;
                case 4:
                    for (let i = 0; i < rs.length; i++) { this.state.List_CVL.push(rs[i]); } this.setState({ List_CVL: this.state.List_CVL });
                    break;
                case 5:
                    for (let i = 0; i < rs.length; i++) { this.state.List_CNK.push(rs[i]); } this.setState({ List_CNK: this.state.List_CNK });
                    break;
                case 6:
                    for (let i = 0; i < rs.length; i++) { this.state.List_HL.push(rs[i]); } this.setState({ List_HL: this.state.List_HL });
                    break;
                case 7:
                    for (let i = 0; i < rs.length; i++) { this.state.List_QH.push(rs[i]); } this.setState({ List_QH: this.state.List_QH });
                    break;
                default:
                    break;
            }
            this.setState({ loading: false });
        });
    };
    OnScrollTo(event, para) {
        var y = event.nativeEvent.contentOffset.x;
        if (y >= ((this.state.Count_Tab * DRIVER.width) + (DRIVER.width / 3))) {
            this.state.Count_Tab = this.state.Count_Tab + 1
            this.myScroll.scrollTo({
                x: (
                    (DRIVER.width * para) - ((para - this.state.Count_Tab) * DRIVER.width)
                )
            });
        } else if (y < ((this.state.Count_Tab * DRIVER.width) - (DRIVER.width / 3))) {
            if (this.state.Count_Tab !== 0) {
                this.state.Count_Tab = this.state.Count_Tab - 1
                this.myScroll.scrollTo({
                    x: (
                        (DRIVER.width * para) - ((para - this.state.Count_Tab) * DRIVER.width)
                    )
                });
            }
        } else {
            this.myScroll.scrollTo({
                x: (
                    (DRIVER.width * para) - ((para - this.state.Count_Tab) * DRIVER.width)
                )
            });
        }
        switch (this.state.Count_Tab) {
            case 0:
                if (this.state.List_CG.length == 0) { this.setState({ loading: true }); this.ListTask_Kanban(true); }
                break;
            case 1:
                if (this.state.List_CXL.length == 0) { this.setState({ loading: true }); this.ListTask_Kanban(true); }
                break;
            case 2:
                if (this.state.List_DXL.length == 0) { this.setState({ loading: true }); this.ListTask_Kanban(true); }
                break;
            case 3:
                if (this.state.List_HT.length == 0) { this.setState({ loading: true }); this.ListTask_Kanban(true); }
                break;
            case 4:
                if (this.state.List_CVL.length == 0) { this.setState({ loading: true }); this.ListTask_Kanban(true); }
                break;
            case 5:
                if (this.state.List_CNK.length == 0) { this.setState({ loading: true }); this.ListTask_Kanban(true); }
                break;
            case 6:
                if (this.state.List_HL.length == 0) { this.setState({ loading: true }); this.ListTask_Kanban(true); }
                break;
            case 7:
                if (this.state.List_QH.length == 0) { this.setState({ loading: true }); this.ListTask_Kanban(true); }
                break;
            default:
                break;
        }
    };
    sortSubject = (para) => {
        controller.sortSubject(para, this.state.KanbanView, rs => {
            this.setState({ KanbanView: rs });
        })
    };
    //#endregion
    //======================================================================VIEW========================================================================
    //#region View Kanban
    render() {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView ref={(ref) => { this.myScroll = ref; }} onScrollEndDrag={(event) => this.OnScrollTo(event, 8)} horizontal={true} showsHorizontalScrollIndicator={false} style={{ top: 0 }}>
                    {/* Chờ giao */}
                    <View style={styles.style_View_Top}>
                        <TouchableOpacity style={[styles.style_TouchableOpacity_Top, { backgroundColor: AppColors.StatusTask_CG }]}>
                            <View style={styles.style_View_Title}>
                                <Text style={[AppStyles.h4, { color: '#fff' }]}>Chờ giao</Text>
                                {/* <Text style={[AppStyles.Textsmall, { color: '#fff' }]}>{item.Value.length} công việc</Text> */}
                            </View>
                            <View style={styles.style_View_Search}>
                                {this.state.setSearch_CG ? <TextInput style={[AppStyles.FormInput, { backgroundColor: '#fff' }]} underlineColorAndroid="transparent" placeholder={'Từ khoá'} autoCapitalize="none" onChangeText={text => this.Search_Kanban(text)} /> : null}
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.setState({ setSearch_CG: !this.state.setSearch_CG })}>
                                    <Icon name="search1" type="antdesign" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.sortSubject()}>
                                    <Icon name="sort-amount-desc" type="font-awesome" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.OpenFormAdd()}>
                                    <Icon name={'plus'} type={'entypo'} size={30} color={'#fff'} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                        {/* Body */}
                        <View style={{ height: DRIVER.height - 150, flexDirection: 'column', justifyContent: 'center' }}>
                            {this.state.loading !== true ?
                                this.state.List_CG.length > 0 ?
                                    this.Kanban_Body(this.state.List_CG, AppColors.StatusTask_CG)
                                    : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không có dữ liệu</Text></View>
                                : <LoadingComponent backgroundColor={'#fff'} />}
                        </View>
                    </View>
                    {/* chờ xử lý */}
                    <View style={styles.style_View_Top}>
                        <TouchableOpacity style={[styles.style_TouchableOpacity_Top, { backgroundColor: AppColors.StatusTask_CXL }]}>
                            <View style={styles.style_View_Title}>
                                <Text style={[AppStyles.h4, { color: '#fff' }]}>Chờ xử lý</Text>
                                {/* <Text style={[AppStyles.Textsmall, { color: '#fff' }]}>{item.Value.length} công việc</Text> */}
                            </View>
                            <View style={styles.style_View_Search}>
                                {this.state.setSearch_CXL ? <TextInput style={[AppStyles.FormInput, { backgroundColor: '#fff' }]} underlineColorAndroid="transparent" placeholder={'Từ khoá'} autoCapitalize="none" onChangeText={text => this.Search_Kanban(text)} /> : null}
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.setState({ setSearch_CXL: !this.state.setSearch_CXL })}>
                                    <Icon name="search1" type="antdesign" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.sortSubject()}>
                                    <Icon name="sort-amount-desc" type="font-awesome" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.OpenFormAdd()}>
                                    <Icon name={'plus'} type={'entypo'} size={30} color={'#fff'} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                        {/* Body */}
                        <View style={{ height: DRIVER.height - 150, flexDirection: 'column', justifyContent: 'center' }}>
                            {this.state.loading !== true ? this.state.List_CXL.length > 0 ? this.Kanban_Body(this.state.List_CXL, AppColors.StatusTask_CXL) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không có dữ liệu</Text></View> : <LoadingComponent backgroundColor={'#fff'} />}
                        </View>
                    </View>
                    {/* Đang xử lý */}
                    <View style={styles.style_View_Top}>
                        <TouchableOpacity style={[styles.style_TouchableOpacity_Top, { backgroundColor: AppColors.StatusTask_DTH }]}>
                            <View style={styles.style_View_Title}>
                                <Text style={[AppStyles.h4, { color: '#fff' }]}>Đang thực hiện</Text>
                                {/* <Text style={[AppStyles.Textsmall, { color: '#fff' }]}>{item.Value.length} công việc</Text> */}
                            </View>
                            <View style={styles.style_View_Search}>
                                {this.state.setSearch_DXL ? <TextInput style={[AppStyles.FormInput, { backgroundColor: '#fff' }]} underlineColorAndroid="transparent" placeholder={'Từ khoá'} autoCapitalize="none" onChangeText={text => this.Search_Kanban(text)} /> : null}
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.setState({ setSearch_DXL: !this.state.setSearch_DXL })}>
                                    <Icon name="search1" type="antdesign" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.sortSubject()}>
                                    <Icon name="sort-amount-desc" type="font-awesome" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.OpenFormAdd()}>
                                    <Icon name={'plus'} type={'entypo'} size={30} color={'#fff'} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                        {/* Body */}
                        <View style={{ height: DRIVER.height - 150, flexDirection: 'column', justifyContent: 'center' }}>
                            {this.state.loading !== true ? this.state.List_DXL.length > 0 ? this.Kanban_Body(this.state.List_DXL, AppColors.StatusTask_DTH) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không có dữ liệu</Text></View> : <LoadingComponent backgroundColor={'#fff'} />}
                        </View>
                    </View>
                    {/* Hoàn thành */}
                    <View style={styles.style_View_Top}>
                        <TouchableOpacity style={[styles.style_TouchableOpacity_Top, { backgroundColor: AppColors.StatusTask_HT }]}>
                            <View style={styles.style_View_Title}>
                                <Text style={[AppStyles.h4, { color: '#fff' }]}>Hoàn thành</Text>
                                {/* <Text style={[AppStyles.Textsmall, { color: '#fff' }]}>{item.Value.length} công việc</Text> */}
                            </View>
                            <View style={styles.style_View_Search}>
                                {this.state.setSearch_HT ? <TextInput style={[AppStyles.FormInput, { backgroundColor: '#fff' }]} underlineColorAndroid="transparent" placeholder={'Từ khoá'} autoCapitalize="none" onChangeText={text => this.Search_Kanban(text)} /> : null}
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.setState({ setSearch_HT: !this.state.setSearch_HT })}>
                                    <Icon name="search1" type="antdesign" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.sortSubject()}>
                                    <Icon name="sort-amount-desc" type="font-awesome" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.OpenFormAdd()}>
                                    <Icon name={'plus'} type={'entypo'} size={30} color={'#fff'} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                        {/* Body */}
                        <View style={{ height: DRIVER.height - 150, flexDirection: 'column', justifyContent: 'center' }}>
                            {this.state.loading !== true ? this.state.List_HT.length > 0 ? this.Kanban_Body(this.state.List_HT, AppColors.StatusTask_HT) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không có dữ liệu</Text></View> : <LoadingComponent backgroundColor={'#fff'} />}
                        </View>
                    </View>
                    {/* Công việc lưu */}
                    <View style={styles.style_View_Top}>
                        <TouchableOpacity style={[styles.style_TouchableOpacity_Top, { backgroundColor: AppColors.StatusTask_CG }]}>
                            <View style={styles.style_View_Title}>
                                <Text style={[AppStyles.h4, { color: '#fff' }]}>Công việc lưu</Text>
                                {/* <Text style={[AppStyles.Textsmall, { color: '#fff' }]}>{item.Value.length} công việc</Text> */}
                            </View>
                            <View style={styles.style_View_Search}>
                                {this.state.setSearch_CVL ? <TextInput style={[AppStyles.FormInput, { backgroundColor: '#fff' }]} underlineColorAndroid="transparent" placeholder={'Từ khoá'} autoCapitalize="none" onChangeText={text => this.Search_Kanban(text)} /> : null}
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.setState({ setSearch_CVL: !this.state.setSearch_CVL })}>
                                    <Icon name="search1" type="antdesign" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.sortSubject()}>
                                    <Icon name="sort-amount-desc" type="font-awesome" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.OpenFormAdd()}>
                                    <Icon name={'plus'} type={'entypo'} size={30} color={'#fff'} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                        {/* Body */}
                        <View style={{ height: DRIVER.height - 150, flexDirection: 'column', justifyContent: 'center' }}>
                            {this.state.loading !== true ? this.state.List_CVL.length > 0 ? this.Kanban_Body(this.state.List_CVL, AppColors.StatusTask_CG) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không có dữ liệu</Text></View> : <LoadingComponent backgroundColor={'#fff'} />}
                        </View>
                    </View>
                    {/* Chờ người khác */}
                    <View style={styles.style_View_Top}>
                        <TouchableOpacity style={[styles.style_TouchableOpacity_Top, { backgroundColor: AppColors.StatusTask_CNK }]}>
                            <View style={styles.style_View_Title}>
                                <Text style={[AppStyles.h4, { color: '#fff' }]}>Chờ người khác</Text>
                                {/* <Text style={[AppStyles.Textsmall, { color: '#fff' }]}>{item.Value.length} công việc</Text> */}
                            </View>
                            <View style={styles.style_View_Search}>
                                {this.state.setSearch_CNK ? <TextInput style={[AppStyles.FormInput, { backgroundColor: '#fff' }]} underlineColorAndroid="transparent" placeholder={'Từ khoá'} autoCapitalize="none" onChangeText={text => this.Search_Kanban(text)} /> : null}
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.setState({ setSearch_CNK: !this.state.setSearch_CNK })}>
                                    <Icon name="search1" type="antdesign" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.sortSubject()}>
                                    <Icon name="sort-amount-desc" type="font-awesome" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.OpenFormAdd()}>
                                    <Icon name={'plus'} type={'entypo'} size={30} color={'#fff'} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                        {/* Body */}
                        <View style={{ height: DRIVER.height - 150, flexDirection: 'column', justifyContent: 'center' }}>
                            {this.state.loading !== true ? this.state.List_CNK.length > 0 ? this.Kanban_Body(this.state.List_CNK, AppColors.StatusTask_CNK) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không có dữ liệu</Text></View> : <LoadingComponent backgroundColor={'#fff'} />}
                        </View>
                    </View>
                    {/* Hoãn lại */}
                    <View style={styles.style_View_Top}>
                        <TouchableOpacity style={[styles.style_TouchableOpacity_Top, { backgroundColor: AppColors.StatusTask_HL }]}>
                            <View style={styles.style_View_Title}>
                                <Text style={[AppStyles.h4, { color: '#fff' }]}>Hoãn lại</Text>
                                {/* <Text style={[AppStyles.Textsmall, { color: '#fff' }]}>{item.Value.length} công việc</Text> */}
                            </View>
                            <View style={styles.style_View_Search}>
                                {this.state.setSearch_HL ? <TextInput style={[AppStyles.FormInput, { backgroundColor: '#fff' }]} underlineColorAndroid="transparent" placeholder={'Từ khoá'} autoCapitalize="none" onChangeText={text => this.Search_Kanban(text)} /> : null}
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.setState({ setSearch_HL: !this.state.setSearch_HL })}>
                                    <Icon name="search1" type="antdesign" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.sortSubject()}>
                                    <Icon name="sort-amount-desc" type="font-awesome" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.OpenFormAdd()}>
                                    <Icon name={'plus'} type={'entypo'} size={30} color={'#fff'} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                        {/* Body */}
                        <View style={{ height: DRIVER.height - 150, flexDirection: 'column', justifyContent: 'center' }}>
                            {this.state.loading !== true ? this.state.List_HL.length > 0 ? this.Kanban_Body(this.state.List_HL, AppColors.StatusTask_HL) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không có dữ liệu</Text></View> : <LoadingComponent backgroundColor={'#fff'} />}
                        </View>
                    </View>
                    {/* Quá hạn */}
                    <View style={styles.style_View_Top}>
                        <TouchableOpacity style={[styles.style_TouchableOpacity_Top, { backgroundColor: AppColors.StatusTask_QH }]}>
                            <View style={styles.style_View_Title}>
                                <Text style={[AppStyles.h4, { color: '#fff' }]}>Quá hạn</Text>
                                {/* <Text style={[AppStyles.Textsmall, { color: '#fff' }]}>{item.Value.length} công việc</Text> */}
                            </View>
                            <View style={styles.style_View_Search}>
                                {this.state.setSearch_QH ? <TextInput style={[AppStyles.FormInput, { backgroundColor: '#fff' }]} underlineColorAndroid="transparent" placeholder={'Từ khoá'} autoCapitalize="none" onChangeText={text => this.Search_Kanban(text)} /> : null}
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.setState({ setSearch_QH: !this.state.setSearch_QH })}>
                                    <Icon name="search1" type="antdesign" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.sortSubject()}>
                                    <Icon name="sort-amount-desc" type="font-awesome" size={20} color={'#fff'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => this.OpenFormAdd()}>
                                    <Icon name={'plus'} type={'entypo'} size={30} color={'#fff'} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                        {/* Body */}
                        <View style={{ height: DRIVER.height - 150, flexDirection: 'column', justifyContent: 'center' }}>
                            {this.state.loading !== true ? this.state.List_QH.length > 0 ? this.Kanban_Body(this.state.List_QH, AppColors.StatusTask_QH) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không có dữ liệu</Text></View> : <LoadingComponent backgroundColor={'#fff'} />}
                        </View>
                    </View>
                </ScrollView>
                <MenuActionCompoment
                    callback={this.actionMenuCallback}
                    data={DataCombobox.DataMenuBottom}
                    eOpen={this.openMenu}
                    position={'bottom'}
                />
                <MenuActionCompoment
                    callback={this.actionMenuCallbackKanban}
                    data={DataCombobox.DataMenuBottomKanban}
                    nameMenu={'Cập nhật tiến độ công việc'}
                    eOpen={this.openMenuKanban}
                    position={'bottom'}
                />
            </View>
        )
    };
    Kanban_Body = (item, colorKb) => (
        <FlatList
            data={item}
            renderItem={({ item, index }) => (
                <ListItem
                    subtitle={() => {
                        return (
                            <View style={{ flexDirection: 'column', marginTop: -20, borderLeftWidth: 3, borderRadius: 5, paddingLeft: 5, borderLeftColor: colorKb }}>
                                <View style={{ flex: 5, flexDirection: 'column' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={{ flex: 4 }} onPress={() => Actions.taskDetail({ data: { TaskGuid: item.TaskGuid } })}>
                                            <Text style={AppStyles.Titledefault}>{item.Subject}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flex: 0.5, alignItems: 'flex-end', justifyContent: 'center' }} onPress={() => this.onActionKanban(item)}>
                                            <Icon
                                                name="calendar-check-o"
                                                type="font-awesome"
                                                size={15}
                                                color={AppColors.gray} />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flex: 0.5, alignItems: 'flex-end', justifyContent: 'center' }} onPress={() => this.onAction(item)}>
                                            <Icon
                                                name="dots-three-horizontal"
                                                type="entypo"
                                                size={15}
                                                color={AppColors.gray} />
                                        </TouchableOpacity>
                                    </View>
                                    {/* <View style={{ height: 40 }}>
                                        <WebView
                                            style={[AppStyles.Textdefault, { color: AppColors.gray }]}
                                            originWhitelist={['*']}
                                            source={{ html: '<Text style="font-size:250%; color:"#9B9B9B"">' + item.TaskContent.substring(0, 100) + '</Text>' }}
                                        />
                                    </View> */}
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Progress.Bar
                                            style={{ marginBottom: 0, marginRight: 0 }}
                                            progress={(item.PercentCompleteTaskOfUser / 100)}
                                            height={2}
                                            width={250}
                                            color={colorKb}
                                        />
                                        <Text style={[AppStyles.Textdefault, { color: colorKb }]}> {item.PercentCompleteTaskOfUser} %</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flexDirection: 'row', flex: 5 }}>
                                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }} onPress={() => this.onViewItem(item.TaskGuid)}>
                                                    <Icon
                                                        name="list"
                                                        type="entypo"
                                                        size={15}
                                                        color={AppColors.gray} />
                                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.CountCLDone == null ? 0 : item.CountCLDone}/{item.CheckLists.length}</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}
                                                    onPress={() => Actions.taskComment({
                                                        data: {
                                                            TaskGuid: item.TaskGuid,
                                                            TaskName: item.Subject
                                                        }
                                                    })}>
                                                    <Icon
                                                        name="comments-o"
                                                        type="font-awesome"
                                                        size={15}
                                                        color={AppColors.gray} />
                                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.CountComment}</Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}
                                                    onPress={() => Actions.attachmentComponent({
                                                        ModuleId: '18',
                                                        RecordGuid: item.TaskGuid
                                                    })}>
                                                    <Icon
                                                        name="attachment"
                                                        type="entypo"
                                                        size={15}
                                                        color={AppColors.gray} />
                                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.CountAtt}</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }} onPress={() => this.UpdateFollow(item, index)}>
                                                    <Icon
                                                        name="flag"
                                                        type="antdesign"
                                                        size={15}
                                                        color={item.IsFollow == true ? AppColors.Maincolor : AppColors.gray} />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                                <Text style={AppStyles.Textdefault}>{FuncCommon.ConDate(item.EndDate, 0)}</Text>
                                            </View>
                                        </View>
                                        {item.CheckChild > 0 ?
                                            <TouchableOpacity style={{ alignItems: 'flex-end', justifyContent: 'center', flex: 1 }} onPress={() => { item.OpenparantId ? this.onClearParentId_Kanban(item) : this.onViewParentId_Kanban(item) }}>
                                                <Icon
                                                    name={item.OpenparantId ? "angle-double-down" : "angle-double-right"}
                                                    type="font-awesome"
                                                    size={15}
                                                    color={AppColors.ColorAdd} />
                                            </TouchableOpacity>
                                            :
                                            <View style={{ alignItems: 'flex-end', justifyContent: 'center', flex: 1 }} />
                                        }
                                    </View>
                                    {item.OpenparantId && item.DataChild.length > 0 ? this.ChildTask(item.DataChild, colorKb) : null}
                                </View>
                            </View>
                        );
                    }}
                    bottomDivider
                />
            )}
            onEndReached={() => this.tabcheck(item.length)}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['black']}
                />
            }
        />

    );
    ChildTask = (item) => (
        <FlatList
            data={item}
            renderItem={({ item, index }) => (
                <ListItem
                    subtitle={() => {
                        return (
                            <View style={{ flexDirection: 'column', marginTop: -20, borderLeftWidth: 3, borderRadius: 5, paddingLeft: 5, borderLeftColor: item.StatusCompletedTaskOfUser == 'X' ? AppColors.StatusTask_CG : item.StatusCompletedTaskOfUser == 'N' ? AppColors.StatusTask_CXL : item.StatusCompletedTaskOfUser == 'P' ? AppColors.StatusTask_DTH : item.StatusCompletedTaskOfUser == 'D' ? AppColors.StatusTask_HL : item.StatusCompletedTaskOfUser == 'C' ? AppColors.StatusTask_HT : item.StatusCompletedTaskOfUser == 'W' ? AppColors.StatusTask_CNK : item.StatusCompletedTaskOfUser == 'F' ? AppColors.StatusTask_TD : AppColors.gray }}>
                                <View style={{ flex: 5, flexDirection: 'column' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}>
                                            <Text style={AppStyles.Titledefault}>{item.Subject}</Text>
                                        </View>

                                    </View>
                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>{item.TaskContent.substring(0, 100)}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Progress.Bar
                                            style={{ marginBottom: 0, marginRight: 0 }}
                                            progress={(item.PercentCompleteTaskOfUser / 100)}
                                            height={2}
                                            width={200}
                                            color={item.StatusCompletedTaskOfUser == 'X' ? AppColors.StatusTask_CG : item.StatusCompletedTaskOfUser == 'N' ? AppColors.StatusTask_CXL : item.StatusCompletedTaskOfUser == 'P' ? AppColors.StatusTask_DTH : item.StatusCompletedTaskOfUser == 'D' ? AppColors.StatusTask_HL : item.StatusCompletedTaskOfUser == 'C' ? AppColors.StatusTask_HT : item.StatusCompletedTaskOfUser == 'W' ? AppColors.StatusTask_CNK : item.StatusCompletedTaskOfUser == 'F' ? AppColors.StatusTask_TD : AppColors.gray}
                                        />
                                        <Text style={[AppStyles.Textdefault, { color: item.StatusCompletedTaskOfUser == 'X' ? AppColors.StatusTask_CG : item.StatusCompletedTaskOfUser == 'N' ? AppColors.StatusTask_CXL : item.StatusCompletedTaskOfUser == 'P' ? AppColors.StatusTask_DTH : item.StatusCompletedTaskOfUser == 'D' ? AppColors.StatusTask_HL : item.StatusCompletedTaskOfUser == 'C' ? AppColors.StatusTask_HT : item.StatusCompletedTaskOfUser == 'W' ? AppColors.StatusTask_CNK : item.StatusCompletedTaskOfUser == 'F' ? AppColors.StatusTask_TD : AppColors.gray }]}> {item.PercentCompleteTaskOfUser} %</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flexDirection: 'row', flex: 5 }}>
                                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }} onPress={() => this.onViewItem(item.TaskGuid)}>
                                                    <Icon
                                                        name="list"
                                                        type="entypo"
                                                        size={15}
                                                        color={AppColors.gray} />
                                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.CountCLDone == null ? 0 : item.CountCLDone}/{item.CheckLists.length}</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}
                                                    onPress={() => Actions.taskComment({
                                                        data: {
                                                            TaskGuid: item.TaskGuid,
                                                            TaskName: item.Subject
                                                        }
                                                    })}>
                                                    <Icon
                                                        name="comments-o"
                                                        type="font-awesome"
                                                        size={15}
                                                        color={AppColors.gray} />
                                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.CountComment}</Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}
                                                    onPress={() => Actions.attachmentComponent({
                                                        ModuleId: '18',
                                                        RecordGuid: item.TaskGuid
                                                    })}>
                                                    <Icon
                                                        name="attachment"
                                                        type="entypo"
                                                        size={15}
                                                        color={AppColors.gray} />
                                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}> {item.CountAtt}</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }} onPress={() => this.UpdateFollow(item)}>
                                                    <Icon
                                                        name="flag"
                                                        type="antdesign"
                                                        size={15}
                                                        color={item.IsFollow == true ? AppColors.Maincolor : AppColors.gray} />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                                <Text style={AppStyles.Textdefault}>{FuncCommon.ConDate(item.EndDate, 0)}</Text>
                                            </View>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', justifyContent: 'center', flex: 1 }}>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        );
                    }}
                    bottomDivider
                    onPress={() => null}
                />
            )}
            onEndReached={() => item.length >= 100 * this._search_Kanban.Page ? this.ListTask_Kanban() : null}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['black']}
                />
            }
        />
    );
    //#endregion
    //============================================================ Chức năng của tab kanban ============================================================
    //#region
    //#region Bật chế độ ghim theo dõi
    UpdateFollow = (item, index) => {
        controller.UpdateFollow(item, rs => {
            switch (this.state.Count_Tab) {
                case 0:
                    this.state.List_CG[index].IsFollow = !this.state.List_CG[index].IsFollow;
                    this.setState({ List_CG: this.state.List_CG });
                    break;
                case 1:
                    this.state.List_CXL[index].IsFollow = !this.state.List_CXL[index].IsFollow;
                    this.setState({ List_CXL: this.state.List_CXL });
                    break;
                case 2:
                    this.state.List_DXL[index].IsFollow = !this.state.List_DXL[index].IsFollow;
                    this.setState({ List_DXL: this.state.List_DXL });
                    break;
                case 3:
                    this.state.List_HT[index].IsFollow = !this.state.List_HT[index].IsFollow;
                    this.setState({ List_HT: this.state.List_HT });
                    break;
                case 4:
                    this.state.List_CVL[index].IsFollow = !this.state.List_CVL[index].IsFollow;
                    this.setState({ List_CVL: this.state.List_CVL });
                    break;
                case 5:
                    this.state.List_CNK[index].IsFollow = !this.state.List_CNK[index].IsFollow;
                    this.setState({ List_CNK: this.state.List_CNK });
                    break;
                case 6:
                    this.state.List_HL[index].IsFollow = !this.state.List_HL[index].IsFollow;
                    this.setState({ List_HL: this.state.List_HL });
                    break;
                case 7:
                    this.state.List_QH[index].IsFollow = !this.state.List_QH[index].IsFollow;
                    this.setState({ List_QH: this.state.List_QH });
                    break;
                default:
                    break;
            }
            // var _list = this.state.ListTask;
            // for (let i = 0; i < _list.length; i++) {
            //     if (_list[i].TaskGuid == item.TaskGuid) {
            //         _list[i].IsFollow = !_list[i].IsFollow
            //     }
            // }
            // this.setState({ ListTask: _list });
        });
    }
    //#endregion
    //#region Mở dropdow
    _openMenu() { }
    openMenu = (d) => {
        this._openMenu = d;
    }
    onAction(item) {
        this.setState({
            selectItem: item
        })
        this._openMenu();
    }
    //#endregion
    //#region Chức năng trong menu Task
    actionMenuCallback = (d) => {
        switch (d) {
            case 'details':
                Actions.taskDetail({ data: { TaskGuid: this.state.selectItem.TaskGuid } })
                break;
            case 'edit':
                Actions.taskAdd({ TypeForm: 'edit', data: { TaskGuid: this.state.selectItem.TaskGuid } })
                break;
            case 'delete':
                Alert.alert(
                    'Xác nhận thông tin',
                    'Bạn có muốn xóa ' + this.state.selectItem.Subject + ' không?',
                    [
                        {
                            text: 'Xác nhận',
                            onPress: () => this.DeleteTask(this.state.selectItem)
                        },
                        { text: 'Huỷ', onPress: () => console.log('No Pressed') },
                    ],
                    { cancelable: true }
                );
                break;
            case 'comments':
                Actions.taskComment({
                    data: {
                        TaskGuid: this.state.selectItem.TaskGuid,
                        TaskName: this.state.selectItem.Subject
                    }
                })
                break;
            case 'attachment':
                var obj = {
                    ModuleId: '18',
                    RecordGuid: this.state.selectItem.TaskGuid
                }
                Actions.attachmentComponent(obj);
                break;
            case 'Calendar':
                this.InsertToCalenderOfUser(this.state.selectItem)
                break;
            case 'UpdateProcess':
                Actions.taskUpdateProcess({ data: { TaskGuid: this.state.selectItem.TaskGuid } });
                break;
            case "Recall":
                Alert.alert(
                    'Xác nhận thông tin',
                    'Yêu cầu thu hồi công việc [ ' + this.state.selectItem.Subject + ' ]',
                    [
                        {
                            text: 'Bắt đầu thu hồi',
                            onPress: () => this.RecallTask(this.state.selectItem)
                        },
                        { text: 'Bỏ qua', onPress: () => console.log('No Pressed') },
                    ],
                    { cancelable: true }
                );
                break;
            default:
                break;
        }
    };
    //#endregion
    //#region thu hồi 1 bản ghi task
    RecallTask = (item) => {
        controller.RecallTask(item, rs => {
            this.updateSearch('');
        });
    };
    //#endregion
    //#region Xoá 1 bản ghi task
    DeleteTask = (item) => {
        controller.DeleteTask(item, rs => {
            this.ListTask_Kanban(true);
        });
    };
    //#endregion
    //#region thêm mới vào Calendar
    InsertToCalenderOfUser = (item) => {
        controller.InsertToCalenderOfUser(item);
    };
    //#endregion
    //#region Mở dropdow
    _openMenuKanban() { }
    openMenuKanban = (d) => {
        this._openMenuKanban = d;
    }
    onActionKanban(item) {
        this.setState({
            selectItem: item,
        })
        this._openMenuKanban();
    }
    //#endregion
    //#region update tiến độ
    actionMenuCallbackKanban = (d) => {
        switch (d) {
            case 'waiting':
                controller.Submit_Kanban(this.state.selectItem, "X", 0);
                break;
            case 'notstart':
                controller.Submit_Kanban(this.state.selectItem, "N", 0);
                break;
            case 'process':
                controller.Submit_Kanban(this.state.selectItem, "P", 50);
                break;
            case 'complete':
                controller.Submit_Kanban(this.state.selectItem, "C", 100);
                break;
            case 'waitsomeone':
                controller.Submit_Kanban(this.state.selectItem, "W", 0);
                break;
            case 'expiry':
                controller.Submit_Kanban(this.state.selectItem, "E", 0);
                break;
            case 'deferred':
                controller.Submit_Kanban(this.state.selectItem, "D", 0);
                break;
            default:
                break;
        };
        this.ListTask_Kanban(true)
    }
    //#endregion
    //#region hiển thị công viêc con
    onViewParentId_Kanban(item) {
        controller.onViewParentId_Kanban(item, this.DataMenuKanban, rs => {
            this.DataMenuKanban = rs;
            this.setState({ KanbanView: rs });
        });
    };
    onClearParentId_Kanban(item) {
        controller.onClearParentId_Kanban(item, this.DataMenuKanban, rs => {
            this.DataMenuKanban = rs;
            this.setState({ KanbanView: rs });
        });
    }
    //#endregion
    //#region Sắp xếp theo tên công việc
    sortSubject = () => {
        switch (this.state.Count_Tab) {
            case 0:
                controller.sortSubject(this.state.StatusSort_CG, this.state.List_CG, rs => {
                    this.setState({ List_CG: rs.data, StatusSort_CG: rs.status });
                })
                break;
            case 1:
                controller.sortSubject(this.state.StatusSort_CXL, this.state.List_CXL, rs => {
                    this.setState({ List_CXL: rs.data, StatusSort_CXL: rs.status });
                })
                break;
            case 2:
                controller.sortSubject(this.state.StatusSort_DXL, this.state.List_DXL, rs => {
                    this.setState({ List_DXL: rs.data, StatusSort_DXL: rs.status });
                })
                break;
            case 3:
                controller.sortSubject(this.state.StatusSort_HT, this.state.List_HT, rs => {
                    this.setState({ List_HT: rs.data, StatusSort_HT: rs.status });
                })
                break;
            case 4:
                controller.sortSubject(this.state.StatusSort_CVL, this.state.List_CVL, rs => {
                    this.setState({ List_CVL: rs.data, StatusSort_CVL: rs.status });
                })
                break;
            case 5:
                controller.sortSubject(this.state.StatusSort_CNK, this.state.List_CNK, rs => {
                    this.setState({ List_CNK: rs.data, StatusSort_CNK: rs.status });
                })
                break;
            case 6:
                controller.sortSubject(this.state.StatusSort_HL, this.state.List_HL, rs => {
                    this.setState({ List_HL: rs.data, StatusSort_HL: rs.status });
                })
                break;
            case 7:
                controller.sortSubject(this.state.StatusSort_QH, this.state.List_QH, rs => {
                    this.setState({ List_QH: rs.data, StatusSort_QH: rs.status });
                })
                break;
            default:
                break;
        }
    };
    //#endregion
    //#region đến form add
    OpenFormAdd = () => {
        switch (this.state.Count_Tab) {
            case 0:
                Actions.taskAdd({ TypeForm: 'add', StatusCompleted: "X" });
                break;
            case 1:
                Actions.taskAdd({ TypeForm: 'add', StatusCompleted: "N" });
                break;
            case 2:
                Actions.taskAdd({ TypeForm: 'add', StatusCompleted: "P" });
                break;
            case 3:
                Actions.taskAdd({ TypeForm: 'add', StatusCompleted: "C" });
                break;
            case 4:
                Actions.taskAdd({ TypeForm: 'add', StatusCompleted: "S" });
                break;
            case 5:
                Actions.taskAdd({ TypeForm: 'add', StatusCompleted: "W" });
                break;
            case 6:
                Actions.taskAdd({ TypeForm: 'add', StatusCompleted: "D" });
                break;
            case 7:
                Actions.taskAdd({ TypeForm: 'add', StatusCompleted: "E" });
                break;
            default:
                break;
        }
    }
    //#endregion
    //#endregion
}
const styles = StyleSheet.create({
    style_TouchableOpacity_Top: {
        flexDirection: 'row',
        padding: 10,
        borderBottomColor: AppColors.Maincolor,
        borderBottomWidth: 2,
        width: DRIVER.width
    },
    style_View_Top: {
        flexDirection: 'column',
        flex: 1,
        width: DRIVER.width,
        height: DRIVER.height,
        borderWidth: 0.5,
        borderColor: AppColors.gray
    },
    style_View_Title: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    style_View_Search: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'center'
    }
});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Kanban_Component);
