import React, { Component } from 'react';
import {
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View,
    TouchableOpacity,
    Dimensions,
    Alert,
    Image,
    TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Divider, CheckBox, Icon } from 'react-native-elements';
import { API_TM_TASKS, API_HR } from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import { AppStyles, AppColors, AppSizes } from '@theme';
import _ from 'lodash';
import * as Progress from 'react-native-progress';
import styles from '../TasksStyle';
import { ScrollView } from 'react-native-gesture-handler';
import { FuncCommon } from '../../../utils';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import LoadingComponent from '../../component/LoadingComponent';
import ViewDetail_TaskContent_Coponent from './ViewDetail_TaskContent_Coponent';
import { controller } from './controller';
import HTML from "react-native-render-html";
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Task_Detail_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            TaskGuid: props.RecordGuid,
            DataView: null,
            ListAttachment: [],
            ListUserApproval: [],
            ListUserProcess: [],
            ListUserFollow: [],
            colorStt: '',
            ListTree: [],
            listtabbarBotom: [],
            TabbarBottom_Sussess: [],
        };
        this.Tabbar_AcceptTask = [
            {
                Title: "Xác nhận",
                Icon: "edit",
                Type: "antdesign",
                Value: "AcceptTask",
                OffEditcolor: true,
                Checkbox: false,
            }
        ];
    }
    componentDidMount() {
        this.setState({ loading: true });
        this.GetTreeDataCOT();

    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.moduleId == "Taskadd") {
            this.setState({ TaskGuid: nextProps.Data.TaskGuid })
            this.GetData();
        }
    }
    //Form detail
    GetData = () => {
        controller.GetData(this.state.TaskGuid, rs => {
            var obj = this.state.ListTree.find(x => x.value === rs.DataView.CategoryOfTaskGuid);
            if (obj !== undefined) {
                rs.DataView.Title = obj.text;
            } else {
                rs.DataView.Title = "";
            }
            this.setState({
                DataView: rs.DataView,
                ListUserApproval: rs.ListUserApproval,
                ListUserProcess: rs.ListUserProcess,
                ListUserFollow: rs.ListUserFollow,
                loading: false
            })
        });
    }
    //#region lấy dữ liệu nhóm công việc
    GetTreeDataCOT = () => {
        controller.GetTreeDataCOT(null, rs => {
            this.setState({ ListTree: rs }, () => {
                this.GetPemission();
            });

        });
    };
    //#endregion
    GetPemission = () => {
        controller.CheckPermission(this.state.TaskGuid, res => {
            var rs = res.Permission;
            if (rs !== null) {
                var listtabbarBotom = [
                    rs.IsUpdate !== true ? null : {
                        Title: "Cập nhật",
                        Icon: "edit",
                        Type: "antdesign",
                        Value: "edit",
                        OffEditcolor: true,
                        Checkbox: false,
                    },
                    rs.IsView !== true ? null : {
                        Title: "Đính kèm",
                        Icon: "attachment",
                        Type: "entypo",
                        Value: "attachment",
                        OffEditcolor: true,
                        Checkbox: false,
                    },
                    rs.IsUpdate !== true ? null : {
                        Title: "Hoàn thành",
                        Icon: "Safety",
                        Type: "antdesign",
                        Value: "success",
                        OffEditcolor: true,
                        Checkbox: false,
                    },
                    rs.IsComment !== true ? null : {
                        Title: "Ý kiến",
                        Icon: "message1",
                        Type: "antdesign",
                        Value: "comment",
                        OffEditcolor: true,
                        Checkbox: false,
                    },
                    rs.IsDelete !== true ? null : {
                        Title: "Xoá",
                        Icon: "delete",
                        Type: "antdesign",
                        Value: "delete",
                        OffEditcolor: true,
                        Checkbox: false,
                    }
                ];
                var TabbarBottom_Sussess = [
                    rs.IsUpdate !== true ? null : {
                        Title: "Cập nhật",
                        Icon: "edit",
                        Type: "antdesign",
                        Value: "edit",
                        OffEditcolor: true,
                        Checkbox: false,
                    },
                    rs.IsView !== true ? null : {
                        Title: "Đính kèm",
                        Icon: "attachment",
                        Type: "entypo",
                        Value: "attachment",
                        OffEditcolor: true,
                        Checkbox: false,
                    },
                    rs.IsComment !== true ? null : {
                        Title: "Ý kiến",
                        Icon: "message1",
                        Type: "antdesign",
                        Value: "comment",
                        OffEditcolor: true,
                        Checkbox: false,
                    },
                    rs.IsDelete !== true ? null : {
                        Title: "Xoá",
                        Icon: "delete",
                        Type: "antdesign",
                        Value: "delete",
                        OffEditcolor: true,
                        Checkbox: false,
                    }
                ];

                var list = [];
                var list_S = [];
                for (let i = 0; i < listtabbarBotom.length; i++) {
                    if (listtabbarBotom[i] !== null) {
                        list.push(listtabbarBotom[i]);
                    }
                }
                for (let i = 0; i < TabbarBottom_Sussess.length; i++) {
                    if (TabbarBottom_Sussess[i] !== null) {
                        list_S.push(TabbarBottom_Sussess[i]);
                    }
                }
                this.setState({
                    listtabbarBotom: list,
                    TabbarBottom_Sussess: list_S
                }, () => {
                    this.GetData();
                });
            }
        })
    }
    //
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <View style={{ height: DRIVER.height, marginBottom: 10 }}>
                            <TabBar_Title
                                title={this.state.DataView.Subject}
                                callBack={() => this.callBackList()} />
                            <Divider />
                            {this.state.DataView !== null ? this.CustomFormDetail(this.state.DataView) : null}
                        </View>
                        {/* <View style={{ bottom: 0 }}> */}
                        {this.state.DataView !== null ?
                            <TabBarBottomCustom
                                ListData={((this.state.DataView.IsAcceptTask === "W" || this.state.DataView.IsAcceptTask === "D") && this.state.DataView.CreatedBy.split("#")[0] !== global.__appSIGNALR.SIGNALR_object.USER.LoginName) ? this.Tabbar_AcceptTask : this.state.DataView.StatusCompleted == "C" ? this.state.TabbarBottom_Sussess : this.state.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                            : null
                        }
                        {/* </View> */}
                        {this.state.DataView !== null ?
                            <ViewDetail_TaskContent_Coponent
                                callback={this.TabViewChange}
                                nameMenu={'Tuỳ chọn danh sách'}
                                eOpen={this.openTabView}
                                data={this.state.DataView.TaskContent} />
                            : null
                        }
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    CustomFormDetail = (para) => (
        <View style={{ flex: 1, flexDirection: 'column', padding: 5, backgroundColor: AppColors.white }}>
            <View style={{ flexDirection: 'column', }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <Text style={AppStyles.Labeldefault}>Tên công việc : </Text>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Text style={AppStyles.Textdefault}>{para.Subject}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                        <Text style={AppStyles.Labeldefault}>Nhóm công việc : </Text>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Text style={AppStyles.Textdefault}>{para.Title}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <Text style={AppStyles.Labeldefault}>Bắt đầu: </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={AppStyles.Textdefault}>{FuncCommon.ConDate(para.StartDate, 0)}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <Text style={AppStyles.Labeldefault}>Đến hạn: </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={AppStyles.Textdefault}>{FuncCommon.ConDate(para.EndDate, 0)}</Text>
                        </View>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <View style={{ flex: 2 }}>
                            <Text style={AppStyles.Labeldefault}>Ưu tiên:</Text>
                        </View>
                        <View style={{ flex: 3 }}>
                            <Text style={AppStyles.Textdefault}>{this.ConvertPriority(para.Priority)}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <Text style={AppStyles.Labeldefault}>Trạng thái:</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={[AppStyles.Textdefault, { color: para.StatusCompletedTaskOfUser == 'X' ? AppColors.StatusTask_CG : para.StatusCompletedTaskOfUser == 'N' ? AppColors.StatusTask_CXL : para.StatusCompletedTaskOfUser == 'P' ? AppColors.StatusTask_DTH : para.StatusCompletedTaskOfUser == 'D' ? AppColors.StatusTask_HL : para.StatusCompletedTaskOfUser == 'C' ? AppColors.StatusTask_HT : para.StatusCompletedTaskOfUser == 'W' ? AppColors.StatusTask_CNK : para.StatusCompletedTaskOfUser == 'F' ? AppColors.StatusTask_TD : (FuncCommon.ConDate(para.EndDate, 0) < FuncCommon.ConDate(new Date(), 0) && para.StatusCompletedTaskOfUser !== "C") ? AppColors.StatusTask_QH : AppColors.gray }]}>{this.ConvertStatus(para.StatusCompletedTaskOfUser)}</Text>
                        </View>
                    </View>
                </View>
            </View>
            <ScrollView>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    {this.state.ListUserApproval.length > 0 ?
                        <View style={{ flexDirection: 'row', flex: 2 }}>
                            <View style={{ flex: 1 }}>
                                <Text style={AppStyles.Labeldefault}>Người chủ trì : </Text>
                            </View>
                            <View style={{ flex: 2 }}>
                                {this.state.ListUserApproval.map((item, i) => (
                                    <TouchableOpacity key={i} style={{ flexDirection: 'row', padding: 5, borderLeftWidth: 0.5 }}>
                                        <View style={[AppStyles.containerCentered, { flex: 2 }]}>
                                            <Image
                                                style={{ width: 40, height: 40, borderRadius: 50 }}
                                                source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                                            />
                                        </View>
                                        <View style={{ flex: 6, justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 14 }}>{item.FullName}</Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Progress.Bar
                                                    style={{ marginBottom: 0, marginRight: 0 }}
                                                    progress={(item.PercentComplete / 100)}
                                                    height={2}
                                                    width={DRIVER.width / 3}
                                                    color={item.PercentComplete <= 10 ? "red" : (item.PercentComplete > 10 && item.PercentComplete <= 50) ? AppColors.StatusTask_DTH : AppColors.StatusTask_HT}
                                                />
                                                <Text style={[AppStyles.Textdefault, {
                                                    color: item.PercentComplete <= 10 ? "red" : (item.PercentComplete > 10 && item.PercentComplete <= 50) ? AppColors.StatusTask_DTH : AppColors.StatusTask_HT
                                                }]}> {item.PercentComplete} %</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </View>
                        : null
                    }
                </View>
                {this.state.ListUserProcess.length > 0 ?
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', flex: 2 }}>
                            <View style={{ flex: 1 }}>
                                <Text style={AppStyles.Labeldefault}>Người phối hợp : </Text>
                            </View>
                            <View style={{ flex: 2 }}>
                                {this.state.ListUserProcess.map((item, i) => (
                                    <TouchableOpacity key={i} style={{ flexDirection: 'row', padding: 5, borderLeftWidth: 0.5 }}>
                                        <View style={[AppStyles.containerCentered, { flex: 2 }]}>
                                            <Image
                                                style={{ width: 40, height: 40, borderRadius: 50 }}
                                                source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                                            />
                                        </View>
                                        <View style={{ flex: 6, justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 14 }}>{item.FullName}</Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Progress.Bar
                                                    style={{ marginBottom: 0, marginRight: 0 }}
                                                    progress={(item.PercentComplete / 100)}
                                                    height={2}
                                                    width={DRIVER.width / 3}
                                                    color={item.PercentComplete <= 10 ? "red" : (item.PercentComplete > 10 && item.PercentComplete <= 50) ? AppColors.StatusTask_DTH : AppColors.StatusTask_HT}
                                                />
                                                <Text style={[AppStyles.Textdefault, {
                                                    color: item.PercentComplete <= 10 ? "red" : (item.PercentComplete > 10 && item.PercentComplete <= 50) ? AppColors.StatusTask_DTH : AppColors.StatusTask_HT
                                                }]}> {item.PercentComplete} %</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </View>
                    </View>
                    : null
                }
                {this.state.ListUserFollow.length > 0 ?
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', flex: 2 }}>
                            <View style={{ flex: 1 }}>
                                <Text style={AppStyles.Labeldefault}>Người theo dõi : </Text>
                            </View>
                            <View style={{ flex: 2 }}>
                                {this.state.ListUserFollow.map((item, i) => (
                                    <TouchableOpacity key={i} style={{ flexDirection: 'row', padding: 5, borderLeftWidth: 0.5 }}>
                                        <View style={[AppStyles.containerCentered, { flex: 2 }]}>
                                            <Image
                                                style={{ width: 40, height: 40, borderRadius: 50 }}
                                                source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                                            />
                                        </View>
                                        <View style={{ flex: 6, justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 14 }}>{item.FullName}</Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Progress.Bar
                                                    style={{ marginBottom: 0, marginRight: 0 }}
                                                    progress={(item.PercentComplete / 100)}
                                                    height={2}
                                                    width={DRIVER.width / 3}
                                                    color={item.PercentComplete <= 10 ? "red" : (item.PercentComplete > 10 && item.PercentComplete <= 50) ? AppColors.StatusTask_DTH : AppColors.StatusTask_HT}
                                                />
                                                <Text style={[AppStyles.Textdefault, {
                                                    color: item.PercentComplete <= 10 ? "red" : (item.PercentComplete > 10 && item.PercentComplete <= 50) ? AppColors.StatusTask_DTH : AppColors.StatusTask_HT
                                                }]}> {item.PercentComplete} %</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </View>
                    </View>
                    : null
                }
                <View style={{ flexDirection: 'column', marginTop: 10 }}>
                    <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.ViewContent()}>
                        <View style={{ flex: 1, marginBottom: 5 }} onPress={() => this.ViewContent()}>
                            <Text style={AppStyles.Labeldefault}>Nội dung</Text>
                        </View>
                        <View>
                            <Text style={[AppStyles.TextLink, { marginLeft: 10 }]}>Chi tiết >></Text>
                        </View>
                    </TouchableOpacity>

                    <Divider />
                    <ScrollView >
                        <TouchableHighlight >
                            <HTML source={{ html: para.CalenderContent }} contentWidth={DRIVER.width} />
                        </TouchableHighlight >
                    </ScrollView>
                </View>
                {/* hạng mục */}
                <View style={{ flexDirection: 'column', marginTop: 10 }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={AppStyles.Labeldefault}>Hạng mục công việc</Text>
                        <Text style={{ color: 'red' }}> </Text>
                    </View>
                    <Divider />
                    {this.state.DataView.CheckList.length > 0 ?
                        this.state.DataView.CheckList.map((item, i) => (
                            <TouchableOpacity key={i} style={[{ flexDirection: 'row', padding: 10, alignItems: 'center' }]} onPress={() => this.UpdateCheckList(i)}>
                                {item.Status ?
                                    <Icon
                                        name={"check"}
                                        type="entypo"
                                        color={AppColors.ColorEdit}
                                        size={20} />
                                    :
                                    <Icon
                                        name={"square-o"}
                                        type="font-awesome"
                                        color={AppColors.IconcolorTabbar}
                                        size={20} />
                                }
                                <Text style={[AppStyles.Textdefault, { marginLeft: 10 }]}>{item.CheckListTitle}</Text>

                            </TouchableOpacity>
                        ))
                        :
                        <View style={AppStyles.centerAligned}>
                            <Text>Không có dữ liệu</Text>
                        </View>}
                </View>

            </ScrollView>
        </View>
    );
    callBackList() {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'back', ActionTime: (new Date()).getTime() });
    }
    ConvertPriority = (item) => {
        switch (item) {
            case "H":
                return 'Cao';
                break;
            case "N":
                return 'Trung bình'
                break;
            case "M":
                return 'Thấp'
                break;
            default:
                break;
        }

    }
    ConvertStatus = (item) => {
        switch (item) {
            case "X":

                return 'Chờ giao';
                break;
            case "N":

                return 'Chờ xử lý'
                break;
            case "P":

                return 'Đang thực hiện'
                break;
            case "D":

                return 'Hoãn lại'
                break;
            case "C":

                return 'Hoàn thành'
                break;
            case "W":

                return 'Chờ người khác'
                break;
            case "F":

                return 'Theo dõi'
                break;
            case "E":

                return 'Quá hạn'
                break;
            default:
                break;
        }
    }
    CallbackValueBottom = (value) => {
        switch (value) {
            case 'edit':
                Actions.taskAdd({ TypeForm: 'edit', data: { TaskGuid: this.state.DataView.TaskGuid } })
                break;
            case 'attachment':
                var obj = {
                    ModuleId: '18',
                    RecordGuid: this.state.DataView.TaskGuid
                }
                Actions.attachmentComponent(obj);
                break;
            case 'delete':
                Alert.alert(
                    'Thông báo',
                    'Bạn có muốn xóa ' + this.state.DataView.Subject + ' không?',
                    [
                        {
                            text: 'Xác nhận',
                            onPress: () => this.DeleteTask(this.state.DataView)
                        },
                        { text: 'Huỷ', onPress: () => console.log('No Pressed') },
                    ],
                    { cancelable: true }
                );
                break;
            case 'success':
                Alert.alert(
                    'Xác nhận hoàn thành công việc',
                    'Bạn đã chắc chắn là đã hoàn thành ' + this.state.DataView.Subject + '?',
                    [
                        {
                            text: 'Xác nhận',
                            onPress: () => this.UpdateProcessSuccess(this.state.DataView)
                        },
                        { text: 'Huỷ', onPress: () => console.log('No Pressed') },
                    ],
                    { cancelable: true }
                );
                break;
                break;
            case 'comment':
                Actions.taskComment({
                    data: {
                        TaskGuid: this.state.DataView.TaskGuid,
                        TaskName: this.state.DataView.Subject
                    }
                })
                break;
            case 'AcceptTask':
                this.ChangeIsAcceptTask(this.state.DataView);
            default:
                break;
        }
    }
    DeleteTask = (item) => {
        controller.DeleteTask(item, rs => {
            this.callBackList();
        });
    };
    UpdateProcessSuccess = (item) => {
        let obj = {
            IdS: [
                item.TaskGuid
            ]
        }
        API_TM_TASKS.UpdateProcessSuccess(obj).then(rs => {
            Alert.alert(
                'Thông báo',
                'Công việc đã hoàn thành',
                [
                    { text: 'Đóng', onPress: () => this.GetData() },
                ],
                { cancelable: true }
            );
        }).catch(error => {
            this.setState({ loading: false });
            console.log('Error when call API Mobile.');
        });
    }
    //#region Hiện thị tab tuỳ chỉnh
    _openTabView() { }
    openTabView = (d) => {
        this._openTabView = d;
    }
    ViewContent() {
        this._openTabView();
    }
    TabViewChange = (rs) => {
        alert(rs)
    }
    //#endregion

    //#region UpdateCheckList
    UpdateCheckList = (index) => {
        controller.UpdateCheckList(this.state.DataView.CheckList, index, rs => {
            this.state.DataView.CheckList = rs;
            this.setState({ DataView: this.state.DataView });
        });
    }
    //#endregion

    //#region mở form xác nhận
    ChangeIsAcceptTask(item) {
        Alert.alert(
            'Chờ xác nhận',
            'Xác nhận công việc được giao: [' + item.Subject + ']',
            [
                {
                    text: 'Nhận việc',
                    onPress: () => this.UpdateIsAcceptTask(item, "Y"),
                    style: { color: 'red' }
                },
                {
                    text: 'Không nhận',
                    onPress: () => this.UpdateIsAcceptTask(item, "N"),
                    style: 'destructive'
                },
                {
                    text: 'Để sau',
                    onPress: () => this.UpdateIsAcceptTask(item, "D"),
                    style: "cancel"
                }
            ],
            { cancelable: true }
        );
    }

    UpdateIsAcceptTask = (item, stt) => {
        var obj = { IdS: [item.TaskGuid, stt] };
        controller.UpdateAccept(obj, rs => {
            this.componentDidMount();
        });
    };
    //#endregion
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Task_Detail_Component);
