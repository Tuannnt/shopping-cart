import { StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppSizes } from '@theme';
export default StyleSheet.create({
    container: {
        flex: 1,
    },
    containerContent: {
        flex: 1,
        marginBottom:50,
        backgroundColor: 'white',
        marginLeft:0
    },
    boxSearch: {
        flexDirection: "row"
    },
    line:{
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        marginBottom: 3,
        marginTop: 3,
    },
    boxComment: {
        padding: 3,  
        justifyContent: "center",
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: '#99CFD0',
        margin:10,
        paddingLeft:10,
        paddingRight:10,
        backgroundColor: '#CEF6F5',
    },
    boxComment_user: {
        padding: 3,  
        justifyContent: "center",
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: '#99CFD0',
        margin:10,
        paddingLeft:10,
        paddingRight:10,
        backgroundColor: '#fff',
    },
    boxBottom_Full:{
        flex: 1,
        width: '100%',
        position: "absolute",
        bottom: 0,
        borderTopColor: '#eff0f1',
        borderTopWidth: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    boxBottom:{
        flex: 1,
        width: '100%',
        bottom: 0,
        flexDirection: 'row'
    },
    boxBottomIcon:{
        width: '100%',
        height:250,
        bottom: 0,
        borderTopColor: '#eff0f1',
        borderTopWidth: 1,
    },
    //Begin trongtq@esvn.com.vn
    PaddingText:{
        padding:3
    },
    //End trongtq@esvn.com.vn
});
