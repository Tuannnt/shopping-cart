import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    FlatList,
    Keyboard,
    RefreshControl,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
    Dimensions,
    TouchableOpacity,
    StyleSheet,
    Alert,
    ScrollView,
    ImageBackground,
    Animatedm,
    Image
} from 'react-native';
import { AppStyles, AppColors } from '@theme';
import FormSearch from '../../component/FormSearch';
import { Icon, ListItem } from 'react-native-elements';
import TabBar from '../../component/TabBar';
import { API_EMAIL } from '@network';
import { FuncCommon } from '../../../utils';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import LoadingComponent from '../../component/LoadingComponent';
import { values } from 'lodash';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Mail_index_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListEmail: [],
            EvenFromSearch: false,
            loading: false,
            Page: 0,
            SearchText: "",
            EmailUser: "",
            SendFilter: { folder: "inbox" },
            TypeMail_MS: "inbox"
        };
        this.listtabbarBotom = [
            {
                Title: "Thư",
                Icon: "mail",
                Type: "feather",
                Value: "inbox",
                Checkbox: true,
                Badge: 0
            },
            {
                Title: "Đã gửi",
                Icon: "Safety",
                Type: "antdesign",
                Value: "sent items",
                Checkbox: false,
                Badge: 0
            },
            {
                Title: "Theo dõi",
                Icon: "eye",
                Type: "feather",
                Value: "follow",
                Checkbox: false,
                Badge: 0
            },
            {
                Title: "Nháp",
                Icon: "filetext1",
                Type: "antdesign",
                Value: "drafts",
                Checkbox: false,
                Badge: 0
            },
            {
                Title: "Thùng rác",
                Icon: "delete",
                Type: "antdesign",
                Value: "deleted items",
                Checkbox: false,
                Badge: 0
            }
        ];
    }
    componentDidMount() {
        this.setState({ loading: true });
        this.Information();
    };
    componentWillReceiveProps = (nextProps) => {
        this.setState({ loading: true });
        if (nextProps.moduleId === "Back") {
            this.setState({
                ListEmail: [],
                EvenFromSearch: false,
            });
            this.Information();
        }
    };
    Information() {
        API_EMAIL.LoadMailConfig().then(rs => {
            if (rs.data.Object !== null) {
                this.setState({
                    EmailUser: rs.data.Object.Email,
                    loading: true,
                });
                this.GetEmails(false);
            } else {
                Alert.alert("Thông báo", "Bạn chưa có email.")
                this.setState({ loading: false });
            }
        }).catch(error => {
            this.setState({ loading: false });
            Alert.alert("Thông báo", "Có lỗi khi lấy Email của bạn")
        });
    }
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    updateSearch = (text) => {
        this.setState({ SearchText: text });
        this.GetEmails(false);
    }
    loadMoreData() {
        this.GetEmails(true);
    }
    GetEmails(s) {
        var ListView = [];
        if (s === true) {
            this.setState({ Page: ++this.state.Page });
            ListView = this.state.ListEmail;
        } else {
            this.setState({ Page: 0 });
        }
        try {
            var obj = {
                Count: -1,
                Page: this.state.Page,
                RowPage: 30,
                SendFilter: this.state.SendFilter,
                Skip: 0,
                Type: "A",
                TypeMail_MS: this.state.TypeMail_MS,
                SearchText: this.state.SearchText
            };
            API_EMAIL.GetEmails(obj).then(rs => {
                if (rs.data.Error === false) {
                    if (rs.data.Object !== "") {
                        var data = JSON.parse(rs.data.Object);
                        for (let i = 0; i < data.results.length; i++) {
                            ListView.push({
                                hasAttachments: data.results[i].hasAttachments !== undefined ? data.results[i].hasAttachments : false,
                                fromAddress: data.results[i].fromAddress,
                                colorBackGroud: this.getRandomColor(),
                                Id: data.results[i].uid,
                                Title: data.results[i].subject !== undefined ? data.results[i].subject : "",
                                DateSent: FuncCommon.ConDate(data.results[i].dateSent, 0),
                                folder: data.results[i].folder,
                                isSeen: data.results[i].isSeen,
                                isFlagged: data.results[i].isFlagged === undefined ? false : data.results[i].isFlagged
                            })
                        };
                    }
                    this.setState({ loading: false, ListEmail: ListView })
                } else {
                    alert("Có lỗi khi lấy ra danh sách Email")
                    this.setState({ loading: false })
                }
            });
        } catch (error) {
            this.setState({ loading: false })
            console.log('Có lỗi khi lấy dữ liệu');
        }
    }

    //#region View chính
    render() {
        return (
            <TouchableWithoutFeedback
                style={{ flex: 1 }}
                onPress={() => {
                    Keyboard.dismiss();
                }}>
                {this.state.loading === true ?
                    <LoadingComponent backgroundColor={'#fff'} />
                    :
                    <View style={AppStyles.container}>
                        <TabBar
                            title={'Email'}
                            BackModuleByCode={'TM'}
                            FormSearch={true}
                            CallbackFormSearch={(callback) => this.setState({ EvenFromSearch: callback })}
                        />
                        {this.state.EvenFromSearch == true ?
                            <FormSearch CallbackSearch={(callback) => this.updateSearch(callback)} />
                            : null}
                        {this.state.ListEmail.length > 0 ?
                            < TouchableOpacity style={styles.styleMessage} onPress={() => this.NextPage()}>
                                <Icon name="edit-3" type="feather" size={20} color={"#C00104"} />
                                <Text style={{ color: '#C00104' }}> Soạn tin</Text>
                            </TouchableOpacity> : null}
                        {this.state.ListEmail.length > 0 ? this.ViewList(this.state.ListEmail) :
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../../../images/Mail/5964.jpg')} style={[AppStyles.centerAligned, { width: DRIVER.width, height: 300 }]} />
                                < TouchableOpacity style={{ flexDirection: 'row', backgroundColor: AppColors.AcceptColor, padding: 10, borderRadius: 15, alignItems: 'center', justifyContent: 'center', width: "50%" }} onPress={() => this.NextPage()}>
                                    <Icon name="edit-3" type="feather" size={20} color={"#fff"} />
                                    <Text style={[AppStyles.Textdefault, { color: '#fff' }]}> Soạn tin</Text>
                                </TouchableOpacity>
                            </View>
                        }
                        <View style={AppStyles.StyleTabvarBottom}>
                            <TabBarBottomCustom
                                ListData={this.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                        </View>
                    </View>
                }
            </TouchableWithoutFeedback >
        );
    }
    ViewList = (item) => {
        return (
            <FlatList
                data={item}
                renderItem={({ item, index }) => (
                    <ListItem
                        leftAvatar={
                            <TouchableOpacity onPress={() => this.mailOpen(item)} style={{ backgroundColor: item.colorBackGroud, width: 35, height: 35, borderRadius: 50, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: 'rgba(231, 232, 232,0.8)', fontSize: 20 }}>{item.fromAddress.substring(0, 1).toUpperCase()}</Text>
                            </TouchableOpacity>
                        }
                        title={
                            <TouchableOpacity onPress={() => this.mailOpen(item)} style={{ flexDirection: 'row' }}>
                                <View style={[{ flex: 1 }]}>
                                    <Text style={[item.isSeen === true ? { color: "#808080" } : { color: "#222222" }, { fontSize: 16 }]}>{this.CustomTitle(item.Title)}</Text>
                                </View>
                                <View style={{}}>
                                    <Text style={AppStyles.Textsmall}>{this.CustomDate(item.DateSent)}</Text>
                                </View>
                            </TouchableOpacity>
                        }
                        subtitle={
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity style={{ flex: 1 }} onPress={() => this.mailOpen(item)}>
                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>. . .</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{}}>
                                    {item.hasAttachments === true ?
                                        <Icon name={"ios-attach"} type={"ionicon"} size={16} iconStyle={{ color: AppColors.gray }} />
                                        : null
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity style={{ paddingLeft: 10 }} onPress={() => this.UpdateFollow(item,index)}>
                                    <Icon name={item.isFlagged === true ? "star" : "star-o"} type={"font-awesome"} size={16} iconStyle={{ color: item.isFlagged === true ? AppColors.Maincolor : AppColors.gray }} />
                                </TouchableOpacity>
                            </View>
                        }

                    />
                )}
                onEndReached={() => this.state.ListEmail.length >= 30 ? this.loadMoreData() : null}
                keyExtractor={(item, index) => index.toString()
                }
                refreshControl={
                    < RefreshControl
                        refreshing={false}
                        onRefresh={this._onRefresh}
                        tintColor="#f5821f"
                        titleColor="#fff"
                        colors={['black']}
                    />
                }
            />
        )
    }
    CustomDate = (date) => {
        if (date !== null) {
            var _date = date.split("/");
            var _Year = new Date().getFullYear();
            if (_date[2] === _Year.toString()) {
                return _date[0] + " thg " + _date[1]
            } else {
                return date
            }
        }
        else {
            return "";
        }
    };
    CustomTitle = (title) => {
        if (title != "") {
            if (title.length > 30) {
                return title.substring(0, 30) + '...';
            } else {
                return title
            }
        } else {
            return "";
        }
    };
    NextPage() {
        Actions.mailMessage();
    }
    mailOpen = (val) => {
        try {
            if (val.isSeen === true) {
                Actions.mailOpen({ Data: { Id: val.Id, Folder: val.folder } });
            } else {
                var obj = {
                    Type: "A",
                    EmailId: val.Id,
                    Read: !val.isSeen,
                    Status: 1,
                    EmailFolderId: val.folder,
                    OwnerEmailAddress: this.state.EmailUser
                };
                API_EMAIL.UpdateReaded(obj).then(rs => {
                    if (rs.data.Error === false) {
                        Actions.mailOpen({ Data: { Id: val.Id, Folder: val.folder } });
                    } else {
                        alert("Có lỗi khi cập nhật trạng thái đọc mail")
                    }
                });
            }
        } catch (error) {
            console.log('Có lỗi khi cập nhật trạng thái đọc mail');
        }
    }
    UpdateFollow = (item, index) => {
        var obj = {
            EmailFolderId: item.folder,
            EmailId: item.Id,
            Flag: !item.isFlagged,
            OwnerEmailAddress: this.state.EmailUser,
            Status: 1,
            Type: "A",
        };
        this.state.ListEmail[index].isFlagged = !this.state.ListEmail[index].isFlagged;
        this.setState({ ListEmail: this.state.ListEmail });
        API_EMAIL.UpdateFollowFlag(obj).then(rs => {
            if (rs.data.Error !== false) {
                alert("Có lỗi khi cập nhật trạng thái đọc mail")
            }
        });
    }
    CallbackValueBottom = (callback) => {
        switch (callback) {
            case "inbox":
                this.setState({
                    SendFilter: { folder: "inbox" },
                    TypeMail_MS: callback
                });
                break;
            case "sent items":
                this.setState({
                    SendFilter: { folder: "sent items" },
                    TypeMail_MS: "inbox"
                })
                break;
            case "drafts":
                this.setState({
                    SendFilter: { folder: "drafts" },
                    TypeMail_MS: "inbox"
                })
                break;
            case "deleted items":
                this.setState({
                    SendFilter: { folder: "deleted items" },
                    TypeMail_MS: "inbox"
                })
                break;
            case "follow":
                this.setState({
                    SendFilter: { folder: "inbox", searchFlags: { 4: true }, sortType: 5 },
                    TypeMail_MS: "inbox"
                })
                break;
            default:
                break;
        };
        setTimeout(() => {
            this.GetEmails(false);
        }, 10);
    }
}
const styles = StyleSheet.create({
    styleMessage: {
        flexDirection: 'row',
        borderRadius: 20,
        position: 'absolute',
        padding: 10,
        bottom: 80,
        right: 20,
        backgroundColor: '#fff',
        zIndex: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.8,
        shadowRadius: 18.97,
        elevation: 5,
    }
});
const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Mail_index_Component);
