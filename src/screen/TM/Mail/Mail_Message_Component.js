import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  KeyboardAvoidingView,
  Keyboard,
  Image,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  Alert,
  ScrollView,
  Animated,
} from 'react-native';
import {AppColors, AppStyles, AppSizes, getStatusBarHeight} from '@theme';
import {Icon, ListItem, Divider, Header} from 'react-native-elements';
import {API, API_EMAIL} from '@network';
import {Actions} from 'react-native-router-flux';
import LoadingComponent from '../../component/LoadingComponent';
import ComboboxV2 from '../../component/ComboboxV2';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import configApp from '../../../configApp';
import RNFetchBlob from 'rn-fetch-blob';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
const options = {
  title: 'Chọn ảnh',
  cancelButtonTitle: 'Huỷ bỏ',
  takePhotoButtonTitle: 'Chụp ảnh',
  chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
  chooseWhichLibraryTitle: 'Chọn thư viện',
};
class Mail_Message_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      EmailUser: null,
      Addressee: '',
      Content: '',
      Subject: '',
      Reply: false,
      loading: true,
      lstFiles: [],
    };
    this.ListComboboxAtt = [
      {
        value: 'img',
        text: 'Chọn ảnh từ thư viện',
      },
      {
        value: 'file',
        text: 'Chọn file',
      },
      {
        value: 'camera',
        text: 'Chụp ảnh',
      },
    ];
    this.state.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    if (this.props.Data !== undefined) {
      this.setState({
        Subject: this.props.Data.subject,
        Addressee: this.props.Data.fromAddress,
        Reply: this.props.Data.Reply,
      });
    }
    this.Information();
  }
  Information() {
    API_EMAIL.LoadMailConfig()
      .then(rs => {
        this.setState({
          EmailUser: rs.data.Object.Email,
          loading: false,
        });
      })
      .catch(error => {
        console.log('error:', error);
        this.setState({loading: false});
        Alert.alert('Thông báo', 'Có lỗi khi lấy Email của bạn');
      });
  }
  //#region View chính
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={AppStyles.container}>
            {this.Header()}
            {this.Content()}
            <ComboboxV2
              callback={this.ChoiceAtt}
              data={this.ListComboboxAtt}
              eOpen={this.openCombobox_Att}
            />
            <OpenPhotoLibrary
              callback={this.callbackLibarary}
              openLibrary={this.openLibrary}
            />
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  //#region Header
  Header = () => {
    if (Platform.OS === 'ios') {
      return (
        <Header
          containerStyle={{
            backgroundColor: '#fff',
          }}
          placement="left"
          leftComponent={this.renderLeftComponent()}
          rightComponent={this.renderRightComponent()}
        />
      );
    } else {
      return (
        <View
          style={[
            styles.squareContainer,
            styles.background,
            {flexDirection: 'row'},
          ]}>
          {this.renderLeftComponent()}
          {this.renderRightComponent()}
        </View>
      );
    }
  };
  renderLeftComponent = () => {
    return (
      <TouchableOpacity
        style={[
          {alignItems: 'center', justifyContent: 'center', paddingLeft: 10},
        ]}
        onPress={() => this.callBackList()}>
        <Icon
          name={'close'}
          type={'antdesign'}
          color={AppColors.gray}
          size={20}
        />
      </TouchableOpacity>
    );
  };
  renderRightComponent = () => {
    return (
      <View
        style={[
          {
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
          },
        ]}>
        <TouchableOpacity
          style={[{paddingRight: 20}]}
          onPress={() => this.openAttachment()}>
          <Icon
            name={'attachment'}
            type={'entypo'}
            color={AppColors.gray}
            size={20}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[{paddingRight: 20}]}
          onPress={() => {
            this.state.Addressee.length > 0 ? this.Submit() : null;
          }}>
          <Icon
            name={this.state.Addressee.length > 0 ? 'send' : 'send-o'}
            type={'font-awesome'}
            color={
              this.state.Addressee.length > 0
                ? AppColors.Maincolor
                : AppColors.gray
            }
            size={20}
          />
        </TouchableOpacity>
        {/* <TouchableOpacity style={[{ paddingRight: 20 }]}>
                    <Icon name={'dots-three-horizontal'} type={'entypo'} color={AppColors.gray} size={20} />
                </TouchableOpacity> */}
      </View>
    );
  };
  //#endregion

  //#region Content
  Content = () => {
    return (
      <ScrollView style={{flex: 1, flexDirection: 'column', padding: 10}}>
        <TextInput
          // onFocus={() => this.OffAllForm()}
          // underlineColorAndroid="transparent"
          placeholder="Đến"
          style={{fontSize: 16, padding: 10}}
          autoCapitalize="none"
          value={this.state.Addressee}
          onChangeText={text => this.setState({Addressee: text})}
        />
        <Divider />
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={{color: AppColors.gray, fontSize: 16, padding: 10}}>
            Từ
          </Text>
          <TextInput
            style={{fontSize: 16, padding: 10}}
            autoCapitalize="none"
            value={this.state.EmailUser !== null ? this.state.EmailUser : ''}
          />
        </View>
        <Divider />
        <TextInput
          placeholder="Chủ đề"
          style={{fontSize: 16, padding: 10}}
          autoCapitalize="none"
          value={this.state.Subject}
          onChangeText={text => this.setState({Subject: text})}
        />
        <Divider />
        {this.state.lstFiles.length > 0 ? (
          <View style={{padding: 5}}>
            {this.state.lstFiles.map((para, i) => (
              <View key={i} style={{flex: 1, flexDirection: 'row', padding: 5}}>
                <View style={[AppStyles.containerCentered, {marginRight: 10}]}>
                  <Image
                    style={{width: 30, height: 30, borderRadius: 5}}
                    source={{
                      uri: this.state.FileAttackments.renderImage(para.name),
                    }}
                  />
                </View>
                <TouchableOpacity style={{flex: 1}}>
                  <Text style={[{color: '#0a87eb', fontSize: 12}]}>
                    {para.name}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{}}
                  onPress={() => this.deleteAtt(para)}>
                  <Icon
                    name={'close'}
                    type={'antdesign'}
                    color={'red'}
                    size={15}
                  />
                </TouchableOpacity>
              </View>
            ))}
          </View>
        ) : null}
        {this.state.lstFiles.length > 0 ? <Divider /> : null}

        <TextInput
          placeholder="Soạn tin"
          style={{fontSize: 16, padding: 10}}
          autoCapitalize="none"
          multiline={true}
          value={this.state.Content}
          onChangeText={text => this.setState({Content: text})}
        />
      </ScrollView>
    );
  };
  //#endregion

  //#region Submit
  Submit = () => {
    var obj = {
      Type: 'A',
      Address: this.state.EmailUser,
      Subject: this.state.Subject,
      Content: this.state.Content,
      SendToUser: [this.state.Addressee],
      CarbonCopy: [],
      BlindCarbonCopy: [],
      ReplyTo: [],
    };
    var _data = new FormData();
    _data.append('insert', JSON.stringify(obj));
    for (var i = 0; i < this.state.lstFiles.length; i++) {
      _data.append(this.state.lstFiles[i].name, this.state.lstFiles[i]);
    }

    API_EMAIL.SendEmail(_data)
      .then(res => {
        Alert.alert('Thông báo', 'Gửi thành công');
        this.callBackList();
      })
      .catch(error => {
        console.log('error:', error);
        Alert.alert('Thông báo', 'Có lỗi khi lấy Email của bạn');
      });
  };
  //#endregion

  //#region  quay trở lại
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  //#endregion

  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#endregion

  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.lstFiles.push(rs[i]);
    }
    this.setState({lstFiles: this.state.lstFiles});
  };
  //#endregion

  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.lstFiles;

      for (let index = 0; index < res.length; index++) {
        _liFiles.push(res[index]);
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].name = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({
        lstFiles: _liFiles,
      });
    } catch (err) {
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.lstFiles;
      await ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            name:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          lstFiles: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion

  //#region deleteAtt
  deleteAtt = item => {
    var data = [];
    for (let i = 0; i < this.state.lstFiles.length; i++) {
      if (this.state.lstFiles[i].name !== item.name) {
        data.push(this.state.lstFiles[i]);
      }
    }
    this.setState({lstFiles: data});
  };
  //#endregion
}
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const navHeight = Platform.OS === 'ios' ? 100 : 100 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
const styles = StyleSheet.create({
  styleMessage: {
    flexDirection: 'row',
    borderWidth: 0.5,
    borderRadius: 20,
    position: 'absolute',
    padding: 10,
    bottom: 50,
    right: 20,
    backgroundColor: '#fff',
    zIndex: 10,
  },
  squareContainer: {
    height: toolbarHeight,
  },
  background: {
    backgroundColor: '#fff',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Mail_Message_Component);
