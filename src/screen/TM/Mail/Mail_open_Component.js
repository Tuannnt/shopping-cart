import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    FlatList,
    Keyboard,
    RefreshControl,
    Text,
    Image,
    TouchableWithoutFeedback,
    View,
    Dimensions,
    TouchableOpacity,
    StyleSheet,
    Alert,
    ScrollView,
    ImageBackground,
    Animated
} from 'react-native';
import { WebView } from 'react-native-webview';
import { AppStyles, AppColors } from '@theme';
import { Icon, ListItem, Header, Divider } from 'react-native-elements';
import { API_EMAIL } from '@network';
import { Actions } from 'react-native-router-flux';
import { FuncCommon } from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import configApp from '../../../configApp';
import RNFetchBlob from 'rn-fetch-blob';
class Mail_open_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Item: null,
            loading: true,
            EmailId: props.Data.Id,
            Folder: props.Data.Folder,
            EmailUser: "",
            ViewToAddress: false,
            OpenAtt: false,
        };
        this.listtabbarBotom = [
            {
                Title: "Chưa đọc",
                Icon: "mail",
                Type: "antdesign",
                Value: "seen",
                OffEditcolor: true,
                Checkbox: false,
            },
            {
                Title: "Đính kèm",
                Icon: "attachment",
                Type: "entypo",
                Value: "attachment",
                OffEditcolor: true,
                Checkbox: false,
            },
            {
                Title: "Trả lời",
                Icon: "corner-up-left",
                Type: "feather",
                Value: "reply",
                OffEditcolor: true,
                Checkbox: false,
            },
            {
                Title: "Xoá",
                Icon: "delete",
                Type: "antdesign",
                Value: "delete",
                OffEditcolor: true,
                Checkbox: false,
            }
        ];
        this.state.FileAttackments = {
            renderImage(check) {
                if (check !== "") {
                    var _check = check.split(".");
                    if (_check.length > 1) {
                        return configApp.url_icon_chat + configApp.link_type_icon + _check[_check.length - 1] + '-icon.png';
                    } else {
                        return configApp.url_icon_chat + configApp.link_type_icon + 'default' + '-icon.png';
                    }
                }
            }
        }
    }
    componentDidMount() {
        this.GetEmails();
        this.Information();
    };
    GetEmails() {
        try {
            this.setState({ loading: true });
            var obj = {
                EmailFolderId: this.state.Folder,
                EmailId: this.state.EmailId,
                Type: "A"
            };
            API_EMAIL.GetItemEmail(obj).then(rs => {
                if (rs.data.Error === false) {
                    var data = JSON.parse(rs.data.Object);
                    this.listtabbarBotom[1].Badge = data.messageData.attachments.length;
                    this.setState({
                        loading: false,
                        Item: data.messageData

                    })
                } else {
                    alert("Có lỗi khi lấy Email")
                }
            });
        } catch (error) {
            console.log('Có lỗi khi lấy dữ liệu');
        }
    }
    Information() {
        API_EMAIL.LoadMailConfig().then(rs => {
            this.setState({
                EmailUser: rs.data.Object.Email
            });
        }).catch(error => {
            console.log("error:", error);
            Alert.alert("Thông báo", "Có lỗi khi lấy Email của bạn")
        });
    }
    //#region View chính
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={AppStyles.container}>
                        <Header
                            containerStyle={{ backgroundColor: '#fff' }}
                            placement="left"
                            leftComponent={this.renderLeftComponent()}
                            //rightComponent={this.renderRightComponent()}
                        />
                        {this.state.Item !== null ? this.ViewItem(this.state.Item) : null}
                        <View style={[AppStyles.StyleTabvarBottom, { backgroundColor: AppColors.white }]}>
                            <TabBarBottomCustom
                                ListData={this.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                :
                <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    ViewItem = (item) => {
        return (
            <ScrollView style={{ flex: 1, padding: 5 }}>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <View style={[{ flex: 1 }]}>
                        <Text style={AppStyles.h1}>{item.subject}</Text>
                        <View style={[{ backgroundColor: "#f0f0f0", padding: 5, borderRadius: 5, width: 100, alignItems: "center", justifyContent: 'center', margin: 5 }]}>
                            <Text style={[AppStyles.Textdefault]}>Hộp thư đến</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={[{ paddingRight: 10 }]} onPress={() => this.UpdateFollow()}>
                        <Icon name={item.isFlagged === true ? "star" : "star-o"} type={"font-awesome"} color={item.isFlagged === true ? AppColors.Maincolor : AppColors.gray} size={20} />
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <View style={[AppStyles.containerCentered, { backgroundColor: "#1E88E1", borderRadius: 40, width: 40, height: 40 }]}>
                        <Text style={[AppStyles.Textdefault, { color: "#fff" }]}>{item.fromAddress.email.substring(0, 1).toUpperCase()}</Text>
                    </View>
                    <View style={[{ flex: 1, marginLeft: 10, justifyContent: 'center', }]}>
                        <View style={[{ flex: 1, flexDirection: 'row', alignItems: 'center' }]}>
                            <Text style={AppStyles.Textdefault}>{item.fromAddress.name}</Text>
                            <Text style={[AppStyles.Textsmall, { color: AppColors.gray, marginLeft: 5 }]}>{this.CustomDate(item.date)}</Text>
                        </View>
                        <TouchableOpacity style={[{ flex: 1, flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.setState({ ViewToAddress: !this.state.ViewToAddress })}>
                            <Text style={[AppStyles.Textsmall, { color: AppColors.gray }]}>tới</Text>
                            <Text style={[AppStyles.Textsmall, { color: AppColors.gray, marginLeft: 5 }]}>{this.checkNameMail(item.toAddresses[0].email)}</Text>
                            <Icon iconStyle={{ marginLeft: 10 }} name={this.state.ViewToAddress === true ? "chevron-up" : "chevron-down"} type={"feather"} color={AppColors.gray} size={15} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => this.OpenFormReply()}>
                        <Icon name={"corner-up-left"} type={"feather"} color={AppColors.black} size={25} />
                    </TouchableOpacity>
                </View>
                {this.state.ViewToAddress === true ?
                    <View style={[AppStyles.containerCentered, { flexDirection: 'column', borderWidth: 0.5, borderRadius: 10, margin: 10, borderColor: AppColors.gray, }]}>
                        <View style={[AppStyles.containerCentered, { flexDirection: 'row', padding: 10 }]}>
                            <View style={{ flex: 1 }}>
                                <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>Từ</Text>
                            </View>
                            <View style={{ flex: 6, flexDirection: 'row', }}>
                                <Text style={AppStyles.Textdefault}>{item.fromAddress.name}</Text>
                                <Text style={[AppStyles.Textdefault, { color: AppColors.gray, marginLeft: 5 }]}>{item.fromAddress.email}</Text>
                            </View>
                        </View>
                        <View style={[AppStyles.containerCentered, { flexDirection: 'row', padding: 10 }]}>
                            <View style={{ flex: 1 }}>
                                <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>Đến</Text>
                            </View>
                            {item.toAddresses.map((para, i) => (
                                <View key={i} style={{ flex: 6, flexDirection: 'row', }}>
                                    <Text style={AppStyles.Textdefault}>{para.name}</Text>
                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray, marginLeft: 5 }]}>{para.email}</Text>
                                </View>
                            ))}
                        </View>
                        <View style={[AppStyles.containerCentered, { flexDirection: 'row', padding: 10 }]}>
                            <View style={{ flex: 1 }}>
                                <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>Ngày</Text>
                            </View>
                            <View style={{ flex: 6 }}>
                                <Text>{FuncCommon.ConDate(item.date, 1)}</Text>
                            </View>
                        </View>
                    </View>
                    : null
                }
                {this.state.OpenAtt === true ?
                    <View style={{ padding: 5 }} >
                        <Text style={{ fontWeight: 'bold' }}>Đính kèm</Text>
                        <Divider />
                        <View style={{ padding: 5 }} >
                            {item.attachments.map((para, i) => (
                                <TouchableOpacity key={i} style={{ flex: 1, flexDirection: "row", padding: 5 }} onPress={() => this.OnDownload(para)}>
                                    <View style={[AppStyles.containerCentered, { marginRight: 10 }]}>
                                        <Image
                                            style={{ width: 50, height: 50, borderRadius: 10 }}
                                            source={{ uri: this.state.FileAttackments.renderImage(para.filename) }}
                                        />
                                    </View>
                                    <View key={i} style={{ flex: 1 }}>
                                        <Text style={AppStyles.Textdefault, { color: '#1769ff' }}>{para.filename}</Text>
                                        <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>size:{para.size / 1024} MB</Text>
                                    </View>
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>
                    : null}
                <Divider />

                <TouchableOpacity style={{ flex: 1 }}>
                    <WebView
                        style={[{ height: 1000 }]}
                        originWhitelist={['*']}
                        source={{ html: '<Text style="font-size:250%;">' + item.messagePlainText + '</Text>' }}
                    />
                </TouchableOpacity>
            </ScrollView>
        )
    }
    //#region header
    renderLeftComponent = () => {
        return (
            <TouchableOpacity style={[{ flexDirection: 'row', flex: 3, paddingLeft: 5, alignItems: 'center' }]} onPress={() => this.Back()}>
                <Icon name={'chevron-left'} type={'feather'} size={25} />
            </TouchableOpacity>
        );
    }
    renderRightComponent = () => {
        return (
            <View style={[{ flexDirection: 'row', flex: 1, alignItems: 'center' }]}>
                <TouchableOpacity style={[{ paddingRight: 10, paddingLeft: 10, justifyContent: 'flex-end' }]} >
                    <Icon name={'more-horizontal'} type={'feather'} size={25} />
                </TouchableOpacity>
            </View>
        );
    }
    //#endregion

    Back = () => {
        Actions.pop();
        Actions.refresh({ moduleId: 'Back', ActionTime: (new Date()).getTime() });
    };
    Delete = () => {
        var obj = {
            EmailFolderId: this.state.Item.folder,
            OwnerEmailAddress: this.state.EmailUser,
            ListID: [this.state.Item.uid],
            Type: "A"
        };
        API_EMAIL.DeleteEmail(obj).then(rs => {
            if (rs.data.Error === false) {
                this.Back();
            } else {
                alert("Có lỗi khi lấy Email")
            }
        });
    }
    CustomDate = (date) => {
        if (date !== null) {
            var fDate = FuncCommon.ConDate(date, 0);
            var _date = fDate.split("/");
            var _Year = new Date().getFullYear();
            if (_date[2] === _Year.toString()) {
                return _date[0] + " thg " + _date[1]
            } else {
                return date
            }
        }
        else {
            return "";
        }
    }
    checkNameMail = (item) => {
        var name = item;
        if (item === this.state.EmailUser) {
            name = 'tôi'
        }
        return name
    };
    OpenFormReply = () => {
        var obj = {
            subject: 'Reply:' + this.state.Item.subject,
            fromAddress: this.state.Item.fromAddress.email,
            fromAddressName: this.state.Item.fromAddress.name,
            Reply: true,
        }
        Actions.mailMessage({ Data: obj });
    }
    CallbackValueBottom = (callback) => {
        switch (callback) {
            case "attachment":
                this.setState({ OpenAtt: !this.state.OpenAtt })
                break;
            case "reply":
                this.OpenFormReply();
                break;
            case "seen":
                var obj = {
                    Type: "A",
                    EmailId: this.state.Item.uid,
                    Read: false,
                    Status: 1,
                    EmailFolderId: this.state.Item.folder,
                    OwnerEmailAddress: this.state.EmailUser
                };
                API_EMAIL.UpdateReaded(obj).then(rs => {
                    if (rs.data.Error === false) {
                        this.Back();
                    } else {
                        alert("Có lỗi khi cập nhật trạng thái đọc mail")
                    }
                });
                break;
            case "delete":
                this.Delete();
                break;
            default:
                break;
        }
    }
    UpdateFollow = () => {
        var obj = {
            EmailFolderId: this.state.Item.folder,
            EmailId: this.state.Item.uid,
            Flag: !this.state.Item.isFlagged,
            OwnerEmailAddress: this.state.EmailUser,
            Status: 1,
            Type: "A",
        };
        API_EMAIL.UpdateFollowFlag(obj).then(rs => {
            if (rs.data.Error === false) {
                this.state.Item.isFlagged = !this.state.Item.isFlagged;
                this.setState({ Item: this.state.Item });
            } else {
                alert("Có lỗi khi cập nhật trạng thái đọc mail")
            }
        });
    }

    //#region DownloadFile
    OnDownload(attachObject) {
        Alert.alert(
            //title
            'Thông báo',
            //body
            'Bạn muốn tải file đính kèm về máy',
            [
                { text: 'Tải về', onPress: () => this.DownloadFile(attachObject) },
                { text: 'Huỷ', onPress: () => console.log('No Pressed') },
            ],
            { cancelable: true }
        );
    }
    DownloadFile = (attachObject) => {
        let dirs = RNFetchBlob.fs.dirs;
        if (Platform.OS !== "ios") {
            RNFetchBlob
                .config({
                    addAndroidDownloads: {
                        useDownloadManager: true,
                        notification: true,
                        mime: '/',
                        description: 'File downloaded by download manager.',
                        path: "file://" + dirs.DownloadDir + '/' + attachObject.filename, //using for android
                    }
                })
                .fetch('GET', configApp.url_Download_mail + attachObject.link, {})
                .then((resp) => {
                    Alert.alert(
                        //title
                        'Thông báo',
                        //body
                        'Tải thành công',
                        [
                            { text: 'Đóng', onPress: () => console.log('No Pressed') },
                        ],
                        { cancelable: true }
                    )
                    resp.path()
                })
        }
        else {
            RNFetchBlob
                .config({
                    path: dirs.DocumentDir + '/' + attachObject.filename,
                    addAndroidDownloads: {
                        useDownloadManager: true,
                        notification: true,
                        IOSDownloadTask: true,
                        mime: '/',
                        description: 'File downloaded by download manager.',
                        title: new Date().toLocaleString() + '-' + attachObject.filename,
                        path: dirs.DocumentDir + "/" + new Date().toLocaleString() + '-' + attachObject.filename,
                    }

                })
                .fetch('GET', configApp.url_Download_mail + attachObject.link, {})
                .then((resp) => {
                    RNFetchBlob.ios.openDocument(resp.data);
                })
        }
    }
    //#endregion
}
const styles = StyleSheet.create({
    styleMessage: {
        flexDirection: 'row',
        borderWidth: 0.5,
        borderRadius: 20,
        position: 'absolute',
        padding: 10,
        bottom: 50,
        right: 20,
        backgroundColor: '#fff',
        zIndex: 10
    }
});
const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Mail_open_Component);
