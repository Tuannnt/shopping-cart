import React, { Component, useRef, useEffect } from 'react';
import { connect } from "react-redux";
import { Divider, Icon, ListItem, SearchBar, CheckBox, } from "react-native-elements";
import { Keyboard, ActivityIndicator, FlatList, RefreshControl, Dimensions, StyleSheet, Text, View, Animated, TouchableOpacity, Image, ScrollView, TextInput, Button, PermissionsAndroid, Alert } from "react-native";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import RNPickerSelect from 'react-native-picker-select';
import { FlatGrid } from 'react-native-super-grid';
import configApp from '../../../configApp';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary'
import { listCombobox,  } from './listCombobox'
import { controller } from './controller'
import { FuncCommon } from '@utils';
import { CustomView, Combobox } from '@Component';
import Toast from 'react-native-simple-toast';

class ListDocumentComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListCategory: [],
            staticParam: {
                Type: 'D',
                id: null,
                Keyword: "",
                CategoryGuid: ""
            },
            ChoiseFolder: [{ id: null, FileName: 'Thư mục gốc' }],
            CategoryName: "",
            list: [],

            isOpenAction: false,
            model: {},
            isFetching: false,
            selectItem: null,
            selected: undefined,
            openSearch: false,
            showList: true,
            showGrid: false,
            showModalListFile: false,
            NameMenu: '',
            ItemFile: {},
            Title: '',
            FolderName: '',
            lstFiles: [],
            EventBoxIcon: false,
            editName: '',
            checkBox: false,
            checked: false,
            DataCheck: [],
        };
        this.state.FileAttackments = {
            renderImage(check) {
                return configApp.url_icon_chat + configApp.link_type_icon + check + '-icon.png';
            }
        };
    };
    async componentDidMount() {
        await this.request_storage_runtime_permission();
        this.GetCategory();
    };
    //#region ===================chức năng lấy danh sách==========================
    GetCategory = () => {
        controller.GetCategory(rs => {
            this.setState({ ListCategory: rs });
        });
    };
    updateSearch = text => {
        this.state.staticParam.Keyword = text;
        this.setState({ staticParam: this.state.staticParam });
        if (this.state.checkcategory <= 6) {
            this.GetFiles();
        } else {
            this.GetAttachmentByFolder();
        }
    };
    GetFiles = () => {
        this.setState({ refreshing: true, list: [] }, () => {
            controller.GetFiles(this.state.staticParam, rs => {
                console.log(rs)
                this.setState({ list: rs, refreshing: false });
            });
        });
    };
    GetAttachmentByFolder = () => {
        this.setState({ refreshing: true, list: [] }, () => {
            controller.GetAttachmentByFolder(this.state.staticParam, rs => {
                this.setState({ list: rs, refreshing: false });
            });
        });
    };
    //#endregion
    //#region =============================View===================================
    render() {
        return (
            <View style={[AppStyles.container, { backgroundColor: '#fff' }]}>
                <TabBar
                    title={'Quản lý tài liệu'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'TM'}
                    addForm={'abc'}
                    CallbackFormAdd={() => this.onActionTop()}
                />
                <TouchableOpacity style={[AppStyles.FormInput, { margin: 5 }]} onPress={() => this.onActionComboboxCategory()}>
                    <Text style={[AppStyles.TextInput]}>{this.state.CategoryName.substring(0, 30)}</Text>
                </TouchableOpacity>
                {this.state.ChoiseFolder.length > 1 ?
                    <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, borderWidth: 0.5, flexDirection: 'row', height: 30, alignItems: 'center' }}>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            style={{}}
                            overScrollMode='always'>
                            {this.state.ChoiseFolder.map((item, index) => (
                                <TouchableOpacity
                                    style={{ paddingLeft: 5 }}
                                    onPress={() => this.BackFolder(item.AttachmentGuid, index)} >
                                    <Text>{item.FileName} > </Text>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                    </View>
                    : null
                }
                {this.state.openSearch == true ? <FormSearch CallbackSearch={(callback) => this.updateSearch(callback)} /> : null}
                <View style={{ flexDirection: 'row', backgroundColor: '#fff' }}>
                    <View style={{ flex: 5, paddingLeft: 10, paddingRight: 10 }}>
                        <RNPickerSelect
                            doneText="Xong"
                            onValueChange={(value) => this.ChangeDropDownOrder(value)}
                            items={listCombobox.DropdownOrder}
                            placeholder={{}}
                            doneText={'Chọn'}
                        />
                    </View>
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.ChangeOrder('list')}>
                        <Icon
                            color={this.state.showList === true ? AppColors.Maincolor : '#888888'}
                            name={'list'}
                            type='entypo'

                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.ChangeOrder('grid')}>
                        <Icon
                            color={this.state.showGrid === true ? AppColors.Maincolor : '#888888'}
                            name={'grid'}
                            type='entypo'
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => { this.state.checkBox === true ? this.setState({ checkBox: false }) : this.setState({ checkBox: true }) }}>
                        <Icon
                            color={this.state.checkBox === true ? AppColors.Maincolor : '#888888'}
                            name={'checksquareo'}
                            type='antdesign'
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                    {/* hiển thị dạng danh sách */}
                    {this.state.showList === true ?
                        this.CustomViewList(this.state.list)
                        // hiển thị dạng lưới
                        : this.state.showGrid === true ?
                            this.CustomViewGrid(this.state.list)
                            : null
                    }
                    {this.state.checkBox === true ?
                        <View style={AppStyles.StyleTabvarBottom}>
                            <TabBarBottomCustom
                                ListData={listCombobox.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.onClickBottom(callback)}
                            />
                        </View>
                        : null
                    }
                </View>
                {/* begin xem chi tiết */}
                <CustomView eOpen={this.openCustomView}>
                    <View style={{ height: DRIVER.height / 2.2, width: DRIVER.width - 50 }}>
                        <View style={{ padding: 10, backgroundColor: '#EEEEEE', justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                            <Text style={AppStyles.Labeldefault}>{this.state.ItemFile.FileName}</Text>
                        </View>
                        <View style={{ padding: 5 }}>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Ngày tạo :</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{FuncCommon.ConDate(this.state.ItemFile.CreatedDate, 1)}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Ngày sửa :</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{FuncCommon.ConDate(this.state.ItemFile.ModifiedDate, 1)}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Kích thước :</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>(Chưa xác định)</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Dung lượng :</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.ConFileSize(this.state.ItemFile.FileSize, 1)}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Từ khóa :</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>-</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Tiêu đề :</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>-</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Người tạo:</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.state.ItemFile.CreatedBy}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Chia sẻ :</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>-</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Trạng thái :</Text>
                                <Text style={[AppStyles.Textdefault, { flex: 2 }]}>-</Text>
                            </View>
                        </View>
                        <Divider />
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <TouchableOpacity style={[AppStyles.FormInput, { flex: 1, justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.DownFile(this.state.ItemFile)}>
                                <Text style={[AppStyles.Textdefault, { color: 'black' }]}>Tải xuống</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[AppStyles.FormInput, { flex: 1, justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.offView()}>
                                <Text style={[AppStyles.Textdefault, { color: 'black' }]}>Đóng</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </CustomView>
                {/* end xem chi tiết */}

                {/* begin đổi tên file */}
                <CustomView eOpen={this.openCustomView2}>
                    <View style={{ height: DRIVER.height / 4, width: DRIVER.width - 50 }}>
                        <View style={{ padding: 10, backgroundColor: '#EEEEEE', justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                            <Text style={AppStyles.Labeldefault}>{this.state.editName}</Text>
                        </View>
                        <View style={{ padding: 5 }}>
                            <TextInput
                                style={{ borderColor: '#BDBDBD', marginTop: 10 }}
                                underlineColorAndroid="transparent"
                                placeholderTextColor="#C0C0C0"
                                numberOfLines={1}
                                autoFocus={true}
                                multiline={true}
                                value={this.state.Title}
                                onChangeText={txt => this.setState({ Title: txt })}
                            />
                        </View>
                        <Divider />
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <TouchableOpacity style={[AppStyles.FormInput, { flex: 1, justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.UpdateFolderFile(this.state.selectItem)}>
                                <Text style={[AppStyles.Textdefault, { color: 'black' }]}>Xác nhận</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[AppStyles.FormInput, { flex: 1, justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.offFormeditTitle()}>
                                <Text style={[AppStyles.Textdefault, { color: 'black' }]}>Đóng</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </CustomView>
                {/* end đổi tên file */}

                {/* begin tạo thư mục */}
                <CustomView eOpen={this.openCustomView3}>
                    <View style={{ height: DRIVER.height / 4, width: DRIVER.width - 50 }}>
                        <View style={{ padding: 10, backgroundColor: '#EEEEEE', justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                            <Text style={AppStyles.Labeldefault}>Tạo thư mục</Text>
                        </View>
                        <View style={{ padding: 5 }}>
                            <TextInput
                                style={{ borderColor: '#BDBDBD', marginTop: 10 }}
                                underlineColorAndroid="transparent"
                                placeholderTextColor="#C0C0C0"
                                placeholder='Nhập tên thư mục'
                                autoFocus={true}
                                numberOfLines={1}
                                multiline={false}
                                value={this.state.FolderName}
                                onChangeText={txt => this.setState({ FolderName: txt })}
                            />
                        </View>
                        <Divider />
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <TouchableOpacity style={[AppStyles.FormInput, { flex: 1, justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.SubmitAddFolder()}>
                                <Text style={[AppStyles.Textdefault, { color: 'black' }]}>Tạo</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[AppStyles.FormInput, { flex: 1, justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.offFormAddFolder()}>
                                <Text style={[AppStyles.Textdefault, { color: 'black' }]}>Đóng</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </CustomView>
                {/* end tạo thư mục */}

                <MenuActionCompoment
                    callback={this.actionMenuCallback}
                    data={listCombobox.DataMenuFile}
                    name={this.state.NameMenu}
                    eOpen={this.openMenu} position={'bottom'}>
                </MenuActionCompoment>
                <MenuActionCompoment
                    callback={this.actionMenuTopCallback}
                    data={listCombobox.DataMenuTop}
                    name={''}
                    eOpen={this.openMenuTop}
                    position={'bottom'}></MenuActionCompoment>
                <OpenPhotoLibrary
                    callback={this.callbackLibarary}
                    openLibrary={this.openLibrary}
                />
                {this.state.ListCategory.length > 0 ?
                    <Combobox
                        value={this.state.ListCategory[0].value}
                        TypeSelect={"single"}
                        callback={this.ChangeCategory}
                        data={this.state.ListCategory}
                        nameMenu={'Chọn danh mục'}
                        eOpen={this.openComboboxCategory}
                        position={'bottom'}
                    />
                    :
                    null
                }
            </View>
        );
    };
    CustomViewList = (data) => {
        return (
            <FlatList
                style={{ height: '100%' }}
                refreshing={this.state.isFetching}
                ref={(ref) => {
                    this.ListView_Ref = ref;
                }}
                ListFooterComponent={this.renderFooter}
                ListEmptyComponent={this.ListEmpty}
                keyExtractor={item => item.AttachmentGuid}
                data={data}
                renderItem={({ item, index }) => (
                    <View>
                        <ListItem
                            subtitle={() => {
                                return (
                                    <View style={{ flexDirection: 'row', marginTop: -20 }}>
                                        {this.state.checkBox === true ?
                                            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.onCheckBox(item, index)}>
                                                <CheckBox
                                                    checked={this.state.list[index].checked}
                                                    onPress={() => this.onCheckBox(item, index)}
                                                />
                                            </TouchableOpacity>
                                            : null
                                        }
                                        <View style={{ flex: 1 }}>
                                            <Image
                                                style={{ width: '70%', height: 50 }}
                                                source={{ uri: this.state.FileAttackments.renderImage(item.FileExtension) }}
                                            />
                                        </View>
                                        <TouchableOpacity style={{ flex: 3 }} onPress={() => this.onClick(item)}>
                                            <Text style={AppStyles.Titledefault}>{item.FileName}</Text>
                                            <Text style={AppStyles.Textdefault}>{FuncCommon.ConDate(item.CreatedDate, 0)}</Text>
                                            {item.FileExtension !== "FOLDER" && (
                                                <Text style={AppStyles.Textdefault}>Kích thước file: {this.ConFileSize(item.FileSize)}</Text>
                                            )}
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.onAction(item)} >
                                            <Icon color='#888888' name={'ellipsis1'} type='antdesign' />
                                        </TouchableOpacity>
                                    </View>
                                )
                            }}
                            bottomDivider

                        />
                    </View>
                )}
                onEndReachedThreshold={0.5}
                onEndReached={({ distanceFromEnd }) => {
                    if (this.state.staticParam.CurrentPage !== 1) {
                        //this.nextPage();
                    }
                }}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                        tintColor="#f5821f"
                        titleColor="#fff"
                        colors={["red", "green", "blue"]}
                    />
                }
            />
        )
    };
    CustomViewGrid = (data) => {
        return (
            <FlatGrid
                itemDimension={130}
                items={this.state.list}
                style={{ paddingRight: 5 }}
                renderItem={({ item, index }) => (
                    <View style={{ borderRadius: 5, backgroundColor: '#fff' }}>
                        <View style={{ flexDirection: 'row' }}>
                            {this.state.checkBox === true ?
                                <TouchableOpacity style={{ flex: 1 }} onPress={() => this.onCheckBox(item, index)}>
                                    <CheckBox
                                        checked={this.state.list[index].checked}
                                        onPress={() => this.onCheckBox(item, index)}
                                    />
                                </TouchableOpacity>
                                : null
                            }
                            <TouchableOpacity style={{ alignItems: 'flex-end', position: 'relative', width: '100%', flex: 1 }} onPress={() => this.onAction(item)}>
                                <Icon color='#888888' name={'ellipsis1'} type='antdesign' onPress={() => this.onAction(item)} />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity style={[styles.itemContainerdetail]}
                            onPress={() => this.onClick(item)}
                        >
                            <Image
                                style={{ width: '45%', height: 80, }}
                                source={{ uri: this.state.FileAttackments.renderImage(item.FileExtension) }}
                            />
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 5 }}>
                                    <Text style={[AppStyles.Titledefault, { textAlign: 'center' }]}>{item.FileName.length > 12 ? item.FileName.substring(0, 10) + ' ...' : item.FileName}</Text>
                                </View>
                            </View>
                            <TouchableOpacity style={{}}
                                onPress={() => this.onClick(item)}
                            >
                                <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{FuncCommon.ConDate(item.CreatedDate, 0)}</Text>
                                <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{this.ConFileSize(item.FileSize)}</Text>
                            </TouchableOpacity>
                        </TouchableOpacity>
                    </View>
                )}
            />
        )
    };
    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };
    hideOnboarding() {
        if (this.state.move === -100) {
            this.setState({
                move: 0
            })
        } else {
            this.setState({
                move: -100
            })
        }
        Animated.timing(
            this.state.offsetY,
            { toValue: this.state.move }
        ).start();
    };
    //#endregion
    //#region ======================Chức năng combobox============================
    //#region Menu Item
    _openMenu() { }
    openMenu = (d) => {
        this._openMenu = d;
    }
    onAction(item) {
        this.setState({
            selectItem: item
        })
        var _option = {};
        if (item.FileExtension === 'FOLDER') {
            _option = {
                isChangeAction: true,
                data: listCombobox.DataMenuFolder,
                NameMenu: item.FileName,
            }
        }
        else {
            _option = {
                isChangeAction: true,
                data: listCombobox.DataMenuFile,
                NameMenu: item.FileName,
            }
        }
        this._openMenu(_option);
    }
    actionMenuCallback = (d) => {
        if (d === 'delete') {
            Alert.alert(
                'Thông báo',
                this.state.DataCheck.length > 0 ? 'Bạn có muốn xóa những file này không?' : 'Bạn có muốn xóa ' + this.state.selectItem.FileName + ' không?',
                [
                    { text: 'Xác nhận', onPress: () => this.deleteDocument(this.state.selectItem) },
                    { text: 'Đóng', onPress: () => console.log('No Pressed') },
                ],
                { cancelable: true }
            );
        }
        else if (d === 'details') {
            this.onClick(this.state.selectItem);
        }
        else if (d === 'edit') {
            this.setState({
                Title: this.state.selectItem.FileName
            })
            if (this.state.selectItem.FileExtension === 'FOLDER') {
                this.setState({ editName: 'Sửa thư mục' })
            }
            else {
                this.setState({ editName: 'Sửa tệp tin' })
            }
            this._openCustomView2();
        }
        else if (d === 'download') {
            this.DownloadFile(this.state.selectItem);
        }
        console.log('Event: ', d);
    };
    deleteDocument = (obj) => {
        if (this.state.DataCheck.length > 0) {
            for (let i = 0; i < this.state.DataCheck.length; i++) {
                controller.DeleteDocument(this.state.DataCheck[i].AttachmentGuid, this.state.staticParam.Type, rs => {

                });
            };
            Toast.showWithGravity("Xoá thành công", Toast.SHORT, Toast.CENTER);
            this.setState({
                list: [],
                DataCheck: []
            }, () => {
                if (this.state.checkcategory <= 6) {
                    this.GetFiles();
                } else {
                    this.GetAttachmentByFolder();
                }
            })
        }
        else {
            controller.DeleteDocument(obj.AttachmentGuid, this.state.staticParam.Type, rs => {
                this.setState({
                    list: [],
                    DataCheck: []
                }, () => {
                    if (this.state.checkcategory <= 6) {
                        this.GetFiles();
                    } else {
                        this.GetAttachmentByFolder();
                    }
                })
            });
        }



    }
    //#endregion
    //#region Menu Top
    _openMenuTop() { }
    openMenuTop = (d) => {
        this._openMenuTop = d;
    }
    onActionTop(item) {
        this._openMenuTop();
    }
    actionMenuTopCallback = (d) => {
        if (d === 'addfolder') {
            this._openCustomView3();
        }
        else if (d === 'upload') {
            this.choiseFile();
        }
        else if (d === 'uploadImg') {
            this.openPhotoLibrary();
        }
    };
    //#endregion
    //#region openFormAddFolder
    _openCustomView3() { }
    openCustomView3 = (d) => {
        this._openCustomView3 = d;
    }
    offFormAddFolder = () => {
        this.setState({ FolderName: '' }, () => {
            this._openCustomView3();
        })
    }
    //#region insert Folder
    SubmitAddFolder = () => {
        Keyboard.dismiss();
        controller.InsertFolder(this.state.staticParam, this.state.FolderName, rs => {
            Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
            this.setState({ FolderName: '' }, () => {
                this._openCustomView3();
                if (this.state.checkcategory <= 6) {
                    this.GetFiles();
                } else {
                    this.GetAttachmentByFolder();
                }
            })
        });
    }
    //#endregion
    //#endregion
    //#region openLibrary
    _open() { }
    openLibrary = (d) => {
        this._open = d;
    }
    openPhotoLibrary() {
        this._open();
    }
    callbackLibarary = (rs) => {
        var _liFiles = this.state.lstFiles;
        for (let index = 0; index < rs.length; index++) {
            _liFiles.push(rs[index])
        }
        this.setState(
            {
                lstFiles: _liFiles,
                EventBoxIcon: false,
            }
        );
        this.uploadFile();
    };
    //#endregion
    //#region CustomView
    _openCustomView() { }
    openCustomView = (d) => {
        this._openCustomView = d;
    }
    offView = () => {
        this._openCustomView();
    }
    DownFile = (val) => {
        this.DownloadFile(val);
        this._openCustomView();
    }
    //#endregion
    //#region openEditTitle
    _openCustomView2() { }
    openCustomView2 = (d) => {
        this._openCustomView2 = d;
    }
    offFormeditTitle = () => {
        this._openCustomView2();
    }
    //#region thay đổi tên file, folder
    UpdateFolderFile = (val) => {
        Keyboard.dismiss();
        controller.UpdateFolderFile(val, this.state.staticParam.Type, this.state.Title, rs => {
            for (let i = 0; i < this.state.list.length; i++) {
                if (this.state.list[i].AttachmentGuid === val.AttachmentGuid) {
                    this.state.list[i].FileName = this.state.Title;
                    break;
                }
            }
            this.setState({ list: this.state.list });
            this._openCustomView2();
        });
    }
    //#endregion
    //#endregion
    //#region  combobox Category
    _openComboboxCategory() { }
    openComboboxCategory = (d) => {
        this._openComboboxCategory = d;
    }
    onActionComboboxCategory() {
        this._openComboboxCategory();
    }
    ChangeCategory = (rs) => {
        if (rs.value !== null) {
            this.state.staticParam.CategoryGuid = rs.CategoryGuid === null ? "" : rs.CategoryGuid;
            this.state.staticParam.Type = rs.Type;
            this.setState({ staticParam: this.state.staticParam, CategoryName: rs.text, checkcategory: rs.value });
            if (rs.value <= 6) {
                this.GetFiles();
            } else {
                this.GetAttachmentByFolder();
            }
        }
    };
    //#endregion
    //#endregion
    //#region ===================Chức năng thêm================
    //Load lại
    onRefresh = () => {
        this.setState({
            refreshing: true,
            list: []
        });
        this.GetFiles();
    };
    setMenuTopRef = ref => {
        this._menuTop = ref;
    };
    hideMenuTop = () => {
        this._menuTop.hide();
    };
    showMenuTop = () => {
        this._menuTop.show();
    };

    ChangeDropDownOrder(value) {
        if (value === 'Name') {
            controller.sortTitle(this.state.list, rs => {
                this.setState({ list: rs });
            });
        }
        else if (value === 'Date') {
            this.setState({ list: [] });
            if (this.state.checkcategory <= 6) {
                this.GetFiles();
            } else {
                this.GetAttachmentByFolder();
            }
        }
    }
    onClick(obj) {
        if (obj.FileExtension === 'FOLDER') {
            this.openFolder(obj);
        }
        else {
            var newData = obj;
            if (obj.CreatedBy !== null && obj.CreatedBy !== undefined) {
                newData.CreatedBy = obj.CreatedBy.split('#')[1]
                this.setState()
            }
            this.setState({ ItemFile: newData });
            this._openCustomView();
        }
    }
    ChangeOrder(data) {
        if (data === 'list') {
            this.setState({
                showGrid: false,
                showList: true,
            })
        }
        else if (data === 'grid') {
            this.setState({
                showList: false,
                showGrid: true,
            })
        }
    }
    openFolder = (obj) => {
        this.state.staticParam.id = obj.AttachmentGuid;
        this.state.ChoiseFolder.push(obj);
        this.setState({ staticParam: this.state.staticParam })
        if (this.state.checkcategory <= 6) {
            this.GetFiles();
        } else {
            this.GetAttachmentByFolder();
        }
    }
    BackFolder = (id, position) => {
        this.state.list = [];
        this.state.staticParam.id = id;
        this.setState({
            staticParam: {
                Type: 'D',
                id: this.state.staticParam.id,
                Keyword: ''
            }
        })
        // for (let index = 0; index < this.state.ChoiseFolder.length; index++) {
        this.state.ChoiseFolder.splice(position + 1, (this.state.ChoiseFolder.length - 1));
        // }
        if (this.state.checkcategory <= 6) {
            this.GetFiles();
        } else {
            this.GetAttachmentByFolder();
        }
    }
    //#region  upload file
    async choiseFile() {
        try {
            const res = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.allFiles],

            });
            var _liFiles = this.state.lstFiles;
            //_liFiles.push(res);
            for (let index = 0; index < res.length; index++) {
                _liFiles.push(res[index])
            }
            this.setState(
                {
                    lstFiles: _liFiles,
                    EventBoxIcon: false,

                    //showModalListFile: true
                }
            );
            this.uploadFile();

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
        //this.setState({ showModalListFile: true });
    }
    removeAttactment(para) {
        var list = this.state.lstFiles;
        var listNewValue = [];
        for (let i = 0; i < list.length; i++) {
            if (list[i].name != para.name) {
                listNewValue.push(list[i]);
            }
        }
        this.setState({
            lstFiles: listNewValue
        })
    }
    uploadFile = () => {
        controller.InsertDocument(this.state.lstFiles, this.state.staticParam, this.state.staticParam.Type, rs => {
            this.setState({
                lstFiles: [],
                showModalListFile: false,
                list: [],
            });
            if (this.state.checkcategory <= 6) {
                this.GetFiles();
            } else {
                this.GetAttachmentByFolder();
            }
        });
    }

    //begin chức năng item
    //update

    //end chức năng item
    //tải file đính kèm về
    DownloadFile(attachObject) {
        if (this.state.DataCheck.length > 0) {
            for (let i = 0; i < this.state.DataCheck.length; i++) {
                var val = this.state.DataCheck[i];
                let dirs = RNFetchBlob.fs.dirs;
                if (Platform.OS !== "ios") {
                    RNFetchBlob
                        .config({
                            addAndroidDownloads: {
                                useDownloadManager: true, // <-- this is the only thing required
                                // Optional, override notification setting (default to true)
                                notification: true,
                                // Optional, but recommended since android DownloadManager will fail when
                                // the url does not contains a file extension, by default the mime type will be text/plain
                                mime: '/',
                                description: 'File downloaded by download manager.',
                                //title: new Date().toLocaleString() + ' - test.xlsx',
                                //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
                                path: "file://" + dirs.DownloadDir + '/' + val.FileName, //using for android
                            }
                        })
                        .fetch('GET', configApp.url_Document_Download + 'D' + '/' + val.AttachmentGuid, {})
                        .then((resp) => {
                            // the path of downloaded file
                            resp.path()
                        })
                }
                else {
                    RNFetchBlob
                        .config({
                            path: dirs.DocumentDir + '/' + val.FileName,
                            // path: "file://" + dirs.DownloadDir + '/' + val.FileName, //using for android
                            addAndroidDownloads: {
                                useDownloadManager: true, // <-- this is the only thing required
                                // Optional, override notification setting (default to true)
                                notification: true,
                                IOSDownloadTask: true,
                                // Optional, but recommended since android DownloadManager will fail when
                                // the url does not contains a file extension, by default the mime type will be text/plain
                                mime: '/',
                                description: 'File downloaded by download manager.',
                                path: dirs.DocumentDir + "/" + new Date().toLocaleString() + '-' + val.FileName, //using for ios
                            }
                        })
                        .fetch('GET', configApp.url_Document_Download + 'D' + '/' + val.AttachmentGuid, {})
                        .then((resp) => {
                            RNFetchBlob.ios.openDocument(resp.data);
                        })
                }
            }
            for (let j = 0; j < this.state.list.length; j++) {
                var val = this.state.list[j];
                if (val.checked === true) {
                    val.checked = false
                }
            }
            this.setState({
                DataCheck: [],
                //checkBox: false
                list: this.state.list
            })
        }
        else {
            let dirs = RNFetchBlob.fs.dirs;
            if (Platform.OS !== "ios") {
                RNFetchBlob
                    .config({
                        addAndroidDownloads: {
                            useDownloadManager: true, // <-- this is the only thing required
                            // Optional, override notification setting (default to true)
                            notification: true,
                            // Optional, but recommended since android DownloadManager will fail when
                            // the url does not contains a file extension, by default the mime type will be text/plain
                            mime: '/',
                            description: 'File downloaded by download manager.',
                            //title: new Date().toLocaleString() + ' - test.xlsx',
                            //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
                            path: "file://" + dirs.DownloadDir + '/' + attachObject.FileName, //using for android
                        }
                    })
                    .fetch('GET', configApp.url_Document_Download + 'D' + '/' + attachObject.AttachmentGuid, {})
                    .then((resp) => {
                        // the path of downloaded file
                        resp.path()
                    })
            }
            else {
                RNFetchBlob
                    .config({
                        path: dirs.DocumentDir + '/' + attachObject.FileName,
                        // path: "file://" + dirs.DownloadDir + '/' + attachObject.FileName, //using for android
                        addAndroidDownloads: {
                            useDownloadManager: true, // <-- this is the only thing required
                            // Optional, override notification setting (default to true)
                            notification: true,
                            IOSDownloadTask: true,
                            // Optional, but recommended since android DownloadManager will fail when
                            // the url does not contains a file extension, by default the mime type will be text/plain
                            mime: '/',
                            description: 'File downloaded by download manager.',
                            path: dirs.DocumentDir + "/" + new Date().toLocaleString() + '-' + attachObject.FileName, //using for ios

                        }
                    })
                    .fetch('GET', configApp.url_Document_Download + 'D' + '/' + attachObject.AttachmentGuid, {})
                    .then((resp) => {
                        console.log("88888888888:", resp.data)
                        RNFetchBlob.ios.openDocument(resp.data);
                    })
            }
        }
    }
    onCheckBox(para, index) {
        var _list = this.state.list;
        _list[index].checked = !_list[index].checked;
        var _dataCheck = [];
        for (let i = 0; i < _list.length; i++) {
            if (_list[i].checked === true) {
                _dataCheck.push(_list[i])
            }
        }
        this.setState({
            DataCheck: _dataCheck
        });
    }
    //Chức năng xóa nhiều, tải nhiều
    onClickBottom(data) {
        if (!this.state.DataCheck || this.state.DataCheck.length == 0) {
            // fix lỗi ko chọn file nào đã action
            Toast.showWithGravity(
                'Yêu cầu chọn file',
                Toast.SHORT,
                Toast.CENTER,
            );
            return
        }

        if (data == 'delete') {
            Alert.alert(
                'Thông báo',
                this.state.DataCheck.length > 0 ? 'Bạn có muốn xóa những file này không?' : 'Bạn có muốn xóa ' + this.state.selectItem.FileName + ' không?',
                [
                    { text: 'Xác nhận', onPress: () => this.deleteDocument(this.state.selectItem) },
                    { text: 'Huỷ', onPress: () => console.log('No Pressed') },
                ],
                { cancelable: true }
            );
        }
        else if (data == 'download') {
            this.DownloadFile();
        }
    }
    async request_storage_runtime_permission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    'title': 'ReactNativeCode Storage Permission',
                    'message': 'ReactNativeCode App needs access to your storage to download Photos.'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                console.log("Storage Permission Granted.");
            }
            else {

                console.log("Storage Permission Not Granted");

            }
        } catch (err) {
            console.warn(err)
        }
    }
    ConFileSize(d) {
        var _size = ' KB';
        try {
            if (d === 0 || d === null || d === undefined) {
                _size = '';
            } else {
                _size = parseFloat((d) / 1024).toFixed(2) + _size;
            }
        } catch (ex) {
            _size = '';
        }
        return _size;
    }
}
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
const styles = StyleSheet.create({
    itemContainerdetail: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 17,
        flexDirection: 'column'
    },
    button: {
        height: AppSizes.buttonHeight,
        paddingHorizontal: 25,
        borderColor: 'transparent',
        borderWidth: 0,
        borderRadius: AppSizes.borderRadius,
    },
    boxBottom_Full: {
        flex: 1,
        width: '100%',
        position: "absolute",
        bottom: 0,
        borderTopColor: '#eff0f1',
        borderTopWidth: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    hidden: {
        display: 'none'
    }
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListDocumentComponent);
