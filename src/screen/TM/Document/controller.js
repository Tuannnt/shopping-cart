import { API_TM_Document, API_TM_CALENDAR, API } from '@network';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { FuncCommon } from '@utils';
import Toast from 'react-native-simple-toast';
export default {
    GetFiles(val, callback) {
        var list = [];
        API_TM_Document.GetFiles(val).then(res => {
            if (res.data.data != "null") {
                var _data = JSON.parse(res.data.data)
                for (let i = 0; i < _data.length; i++) {
                    list.unshift(_data[i]);
                }
            }
            callback(list);
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi khi lấy danh sách");
            Actions.pop();
            console.log(error)
        });
    },
    GetAttachmentByFolder(val, callback) {
        var list = [];
        API_TM_Document.GetAttachmentByFolder(val).then(res => {
            if (res.data.data !== "[]") {
                var _data = JSON.parse(res.data.data)
                for (let i = 0; i < _data.length; i++) {
                    list.unshift(_data[i]);
                }
            }
            callback(list);
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi khi lấy danh sách");
            Actions.pop();
            console.log(error)
        });
    },
    InsertDocument(lstFiles, staticParam, type, callback) {
        let _data = new FormData();
        var obj = {
            Type: type,
            id: staticParam.id,
            CategoryGuid: staticParam.CategoryGuid,
            Guids: [],
            TypeExtend: "A"
        }
        _data.append('insert', JSON.stringify(obj));
        _data.append('modelCustom', JSON.stringify({ IsPublicAll: false }));

        for (var i = 0; i < lstFiles.length; i++) {
            _data.append(lstFiles[i].name, lstFiles[i]);
        }
        API_TM_Document.InsertDocument(_data).then(res => {
            callback(res);
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi thêm mới");
            Actions.pop();
            console.log(error)
        });
    },
    InsertFolder(staticParam, text, callback) {
        var obj = {
            Type: staticParam.Type,
            CategoryGuid: staticParam.CategoryGuid,
            Title: text,
            id: staticParam.id
        }
        API_TM_Document.InsertFolder(obj).then(res => {
            callback(res);
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi thêm mới folder");
            Actions.pop();
            console.log(error)
        });
    },
    DeleteDocument(Guid, type, callback) {
        var obj = {
            Type: type,
            FileId: Guid
        }
        API_TM_Document.DeleteDocument(obj).then(res => {
            callback(res);
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi khi xoá");
            Actions.pop();
            console.log(error)
        });
    },
    sortTitle(list, callback) {
        list.sort(function (a, b) {
            if (a.FileName < b.FileName) { return -1; }
            if (a.FileName > b.FileName) { return 1; }
            return 0;
        })
        callback(list);
    },
    UpdateFolderFile(val, type, title, callback) {
        var obj = {
            Title: title,
            Type: type,
            id: val.AttachmentGuid
        }
        API_TM_Document.UpdateFolderFile(obj).then(res => {
            if (res.data.errorCode === 200) {
                callback();
            }
            Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);

        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi khi đổi tên file");
            console.log(error)
        });
    },
    GetCategory(callback) {
        var list = [
            {
                value: 1,
                text: "Các tài liệu",
                CategoryGuid: null,
                Type: "D",
            },
            {
                value: 2,
                text: "Media",
                CategoryGuid: null,
                Type: "MEDIA",
            },
            {
                value: 3,
                text: "Tài liệu dự án",
                CategoryGuid: null,
                Type: "PROJECT",
            },
            {
                value: 4,
                text: "Hồ sơ công việc",
                CategoryGuid: null,
                Type: "TASK",
            },
            {
                value: 5,
                text: "Hợp đồng",
                CategoryGuid: null,
                Type: "CONTRACT",
            },
            {
                value: 6,
                text: "Tài liệu chia sẻ",
                CategoryGuid: null,
                Type: "SHARE",
            }
        ]
        API_TM_Document.GetCategory().then(res => {
            var data = JSON.parse(res.data.data);
            var stt = 6;
            for (let i = 0; i < data.length; i++) {
                list.push({
                    value: stt + 1,
                    text: data[i].Title,
                    CategoryGuid: data[i].CategoryGuid,
                    Type: "LABEL",
                });
                stt++
            }
            callback(list);
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi khi lấy danh sách loại dữ liệu");
            Actions.pop();
            console.log(error)
        });
    },
}