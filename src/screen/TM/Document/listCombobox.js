import { AppStyles, AppColors } from '@theme';
export default {
    listtabbarBotom: [{
        Title: "Xóa",
        Icon: "delete",
        Type: "antdesign",
        Value: "delete",
        Checkbox: false
    },
    {
        Title: "Download",
        Icon: "download",
        Type: "antdesign",
        Value: "download",
        Checkbox: false
    }],
    DropdownOrder: [{
        value: 'Date',
        label: 'Sắp xếp theo ngày'
    },
    {
        value: 'Name',
        label: 'Sắp xếp theo tên'
    }],
    DataMenuFile: [{
        key: 'delete',
        name: 'Xóa',
        icon: {
            name: 'delete',
            type: 'antdesign',
            color: AppColors.Maincolor
        }
    },
    {
        key: 'details',
        name: 'Chi tiết',
        icon: {
            name: 'infocirlceo',
            type: 'antdesign',
            color: AppColors.Maincolor
        }
    },
    {
        key: 'edit',
        name: 'Đổi tên',
        icon: {
            name: 'edit',
            type: 'antdesign',
            color: AppColors.Maincolor
        }
    },
    {
        key: 'download',
        name: 'Tải xuống',
        icon: {
            name: 'download',
            type: 'antdesign',
            color: AppColors.Maincolor
        }
    }],
    DataMenuFolder: [{
        key: 'delete',
        name: 'Xóa',
        icon: {
            name: 'delete',
            type: 'antdesign',
            color: AppColors.Maincolor
        }
    },
    {
        key: 'details',
        name: 'Chi tiết',
        icon: {
            name: 'infocirlceo',
            type: 'antdesign',
            color: AppColors.Maincolor
        }
    },
    {
        key: 'edit',
        name: 'Đổi tên',
        icon: {
            name: 'edit',
            type: 'antdesign',
            color: AppColors.Maincolor
        }
    }],
    DataMenuTop: [{
        key: 'upload',
        name: 'Tải lên file',
        icon: {
            name: 'upload',
            type: 'antdesign',
            color: '#f6b801'
        }
    },
    {
        key: 'uploadImg',
        name: 'Tải lên hình ảnh',
        icon: {
            name: 'picture',
            type: 'antdesign',
            color: '#f6b801'
        }
    },
    {
        key: 'addfolder',
        name: 'Tạo thư mục',
        icon: {
            name: 'addfolder',
            type: 'antdesign',
            color: '#f6b801'
        }
    }],
}