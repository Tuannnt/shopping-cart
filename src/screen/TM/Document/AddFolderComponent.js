
import React, { Component } from 'react';
import { View, Text, TextInput, TouchableWithoutFeedback, Keyboard, StyleSheet, ToastAndroid, TouchableOpacity, Alert } from 'react-native';
import { Header, Button, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import API_CheckRequests from "../../../network/BM/API_CheckRequests";
import { DatePicker, Container, Content, Item, Label, Picker } from 'native-base';
import { FuncCommon } from '../../../utils';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from "../../component/TabBar_Title";

class AddFolderComponent extends Component {
    constructor(props) {
        super(props);
        this.DataProcess = FuncCommon.DataProcess();
        this.state = {
            model: {},
            refreshing: false,
            FolderName: ''
        };
    }
    componentDidMount() {
        let id = this.props.RecordGuid;
    }
    render() {
        return (
            <Container>
                <TouchableWithoutFeedback style={{ flex: 30 }} onPress={() => {
                    Keyboard.dismiss()
                }}>
                    <View style={{ flex: 40 }}>
                        <TabBar_Title
                            title={'Tạo thư mục'}
                            callBack={() => this.onPressBack()} />
                        <View style={styles.containerContent}>
                            <Content padder>
                                <Text style={AppStyles.Labeldefault}>Tên thư mục</Text>
                                <TextInput style={styles.input}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="#C0C0C0"
                                    autoCapitalize="none"
                                    value={this.state.Name}
                                    onChangeText={FolderName => this.setFolderName(FolderName)} />
                            </Content>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <View>
                    <Button
                        title="Xác nhận"
                        onPress={() => this.onSubmit()}
                    />
                </View>
            </Container >
        )
    }
    onPressBack() {
        Actions.Document();
    }
    setQuantity(Quantity) {
        this.state.model.FolderName = FolderName;
        this.setState({ model: this.state.model });
    }
    setQuantityOK(QuantityOK) {
        this.state.model.QuantityOK = QuantityOK;
        this.setState({ model: this.state.model });
    }
    setQuantityNG(QuantityNG) {
        this.state.model.QuantityNG = QuantityNG;
        this.setState({ model: this.state.model });
        this.state.model.QuantityOK = parseFloat(this.state.model.Quantity - this.state.model.QuantityNG)
    }
    onSubmit() {
        if (this.state.model.Quantity < this.state.model.QuantityNG) {
            Alert.alert(
                "Thông báo", 'Số lượng NG không được lớn hơn số lượng đề nghị', [{ text: 'Đóng' },], { cancelable: true }
            );
        } else {
            this.state.model.NgreasonId = this.state.selected
            API_CheckRequests.Check(this.state.model)
                .then(res => {
                    Alert.alert(
                        "Thông báo", res.data.message, [{ text: 'Đóng' },], { cancelable: true }
                    );
                    Actions.CheckRequests();
                })
                .catch(error => {
                    Alert.alert(
                        "Thông báo", res.data.message, [{ text: 'Đóng' },], { cancelable: true }
                    );
                });
        }
    }
    //định dạng tiền tệ
    addPeriod(nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "," + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AddFolderComponent);

const styles = StyleSheet.create({
    containerContent: {
        flex: 20,
        flexDirection: "row",
        marginBottom: 50,
        backgroundColor: 'white',
        marginLeft: 0
    },
    input: {
        marginTop: 10,
        height: 40,
        borderColor: '#C0C0C0',
        borderWidth: 1
    }
})