import {AppStyles, AppColors} from '@theme';
export default {
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //     value: 'camera',
    //     text: "Chụp ảnh"
    // }
  ],
  ListCategories: [
    {
      key: 'Myday',
      icon: 'light-down',
      typeIcon: 'entypo',
      colorIcon: '#8B8378',
      Title: 'Ngày của tôi',
      CountContent: 0,
      count: 1,
    },
    {
      key: 'IsFollow',
      icon: 'star-o',
      typeIcon: 'font-awesome',
      colorIcon: '#FFD700',
      Title: 'Quan trọng',
      CountContent: 0,
      count: 2,
    },
    {
      key: 'Myday',
      icon: 'calendar',
      typeIcon: 'feather',
      colorIcon: '#008B00',
      Title: 'Đã lập kế hoạch',
      CountContent: 0,
      count: 3,
    },
    {
      key: 'Myday',
      icon: 'user',
      typeIcon: 'feather',
      colorIcon: '#B23AEE',
      Title: 'Đã giao cho bạn',
      CountContent: 0,
      count: 4,
    },
    {
      key: 'Myday',
      icon: 'shield',
      typeIcon: 'feather',
      colorIcon: '#4876FF',
      Title: 'Tác vụ',
      CountContent: 0,
      count: 5,
    },
  ],
  DataMenuBottom: [
    {
      key: '1',
      name: 'Hôm nay',
      icon: {
        name: 'back-in-time',
        type: 'entypo',
        color: 'black',
      },
    },
    {
      key: '2',
      name: 'Ngày mai',
      icon: {
        name: 'calendar',
        type: 'feather',
        color: 'black',
      },
    },
    {
      key: '3',
      name: 'Tuần tới',
      icon: {
        name: 'calendar',
        type: 'feather',
        color: 'black',
      },
    },
    {
      key: '4',
      name: 'Chọn ngày',
      icon: {
        name: 'calendar',
        type: 'feather',
        color: 'black',
      },
    },
  ],
  DataMenuBottomRepeat: [
    {
      key: '1',
      name: 'Hàng ngày',
      icon: {
        name: 'calendar-week',
        type: 'material-community',
        color: 'black',
      },
    },
    {
      key: '2',
      name: 'Hàng tuần',
      icon: {
        name: 'calendar-week-begin',
        type: 'material-community',
        color: 'black',
      },
    },
    {
      key: '3',
      name: 'Hàng tháng',
      icon: {
        name: 'calendar-today',
        type: 'material-community',
        color: 'black',
      },
    },
    {
      key: '4',
      name: 'Hàng năm',
      icon: {
        name: 'calendar-week-begin',
        type: 'material-community',
        color: 'black',
      },
    },
  ],
  DataMenuBottomTimeReminder: [
    {
      key: '1',
      name: 'Cuối ngày',
      icon: {
        name: 'calendar-week',
        type: 'material-community',
        color: 'black',
      },
    },
    {
      key: '2',
      name: 'Ngày mai',
      icon: {
        name: 'calendar-week-begin',
        type: 'material-community',
        color: 'black',
      },
    },
    {
      key: '3',
      name: 'Tuần tới',
      icon: {
        name: 'calendar-today',
        type: 'material-community',
        color: 'black',
      },
    },
    {
      key: '4',
      name: 'Tuỳ chọn',
      icon: {
        name: 'calendar-week-begin',
        type: 'material-community',
        color: 'black',
      },
    },
  ],
};
