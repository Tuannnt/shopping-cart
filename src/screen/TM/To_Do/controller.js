import { API_TM_TASKS, API_TODO, API } from '@network';
import configApp from '../../../configApp';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { FuncCommon } from '@utils';
export default {
    getProfile(data, callback) {
        API.getProfile().then(rs => {
            callback(rs.data);
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi khi lấy dữ liệu người đăng nhập" + error);
            Actions.pop();
        });
    },
    renderImage(check) {
        if (check !== "") {
            var _check = check.split(".");
            if (_check.length > 1) {
                return configApp.url_icon_chat + configApp.link_type_icon + _check[_check.length - 1] + '-icon.png';
            } else {
                return configApp.url_icon_chat + configApp.link_type_icon + 'default' + '-icon.png';
            }
        }
    },
    ListCategoryOfTasks(obj, list, callback) {
        API_TODO.ListCategoryOfTasks(obj).then(rs => {
            if (rs.data.errorCode === 200) {
                var data = JSON.parse(rs.data.data);
                list[0].CountContent = JSON.parse(rs.data.data_v2)[0].CountToday;
                list[1].CountContent = JSON.parse(rs.data.data_v2)[0].CountFollow;
                var count = 5;
                for (let i = 0; i < data.length; i++) {
                    list.push({
                        key: data[i].CategoryOfTaskGuid,
                        icon: 'list',
                        typeIcon: 'feather',
                        colorIcon: '#9fa1a3',
                        Title: data[i].Title,
                        CountContent: data[i].CountData,
                        count: count + 1
                    })
                    count = count + 1
                };
                callback(list);
            }
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi khi lấy danh sách nhóm");
            Actions.pop();
        });
    },
    ListTasks(obj, isShowDone, callback) {
        API_TODO.ListTasks(obj).then(rs => {
            if (rs.data.errorCode === 200) {
                var data = JSON.parse(rs.data.data).data;
                var _dataView = [];
                var viewTitleDone = true;
                for (let i = 0; i < data.length; i++) {
                    if (data[i].IsFollow == "True") {
                        data[i].IsFollow = true
                    } else {
                        data[i].IsFollow = false
                    }
                    if (data[i].StatusCompleted !== "C") {
                        data[i].isShow = true;
                        data[i].viewTitleDone = false;
                        _dataView.push(data[i]);
                    }
                }
                for (let i = 0; i < data.length; i++) {
                    if (data[i].StatusCompleted === "C") {
                        if (viewTitleDone === true) {
                            data[i].viewTitleDone = true;
                            viewTitleDone = false;
                        } else {
                            data[i].viewTitleDone = false;
                        }
                        data[i].isShow = isShowDone;
                        _dataView.push(data[i]);
                    }
                }
                callback(_dataView);
            }
        }).catch(error => {
            // Alert.alert("Thông báo", "Đã xảy ra lỗi khi lấy thông tin");
            console.log(error)
            // Actions.pop();
        });
    },
    ListTasks_AddMyDay(val, listCombobox, callback) {
        var obj = {
            txtSearch: "",
            NumberPage: val,
            Length: 50
        }
        API_TODO.ListTasks_AddMyDay(obj).then(rs => {
            if (rs.data.errorCode === 200) {
                var data = JSON.parse(rs.data.data).data;
                for (let i = 0; i < data.length; i++) {
                    listCombobox.push({
                        value: data[i].TaskGuid,
                        text: data[i].Subject,
                    })
                }
                callback(listCombobox);
            }
        }).catch(error => {
            Alert.alert("Thông báo", "Đã xảy ra lỗi khi lấy thông tin.");
            Actions.pop();
        });
    },
    Insert(txt, listData, categoryOfTaskGuid, callback) {
        var _activeMail = {
            IsSendApproval: false,
            IsSendProcess: false,
            IsSendFollow: false
        }
        var _insertTOU = {
            TaskGuid1: [],
            TaskGuid2: [],
            TaskGuid3: [],
        }
        let _obj = {
            Subject: txt,
            StartDate: FuncCommon.ConDate(new Date(), 2),
            EndDate: FuncCommon.ConDate(new Date(), 2),
            TaskContent: "",
            Priority: "N",
            StatusCompleted: "P",
            CategoryOfTaskGuid: categoryOfTaskGuid,
        };
        let _data = new FormData();
        _data.append('TaskInsertOption', JSON.stringify({ "Option": "ADD" }));
        _data.append('insertCL', JSON.stringify([]));
        _data.append('ActiveMail', JSON.stringify(_activeMail));
        _data.append('insertTOU', JSON.stringify(_insertTOU));
        _data.append('insert', JSON.stringify(_obj));
        _data.append('Recurrence', null);
        _data.append('Label', JSON.stringify([]));
        _data.append('lstTitlefile', JSON.stringify([]));
        _data.append('DeleteAttach', JSON.stringify([]));
        API_TM_TASKS.Insert(_data).then(rs => {
            var _rs = rs.data;
            if (_rs.Error) {
                console.log(_rs);
            } else {
                listData.unshift({
                    TaskGuid: JSON.parse(rs.data.data),
                    Subject: txt,
                    StatusCompleted: "P",
                    EndDate: new Date(),
                    viewTitleDone: false,
                    Content: "",
                    IsFollow: false,
                    isShow: true,
                })
                callback(listData);
            }
        }).catch(error => {
            Alert.alert("Thông báo", "Có lỗi khi thêm mới.");
            console.log(error)
        });
    },
    InsertCategoryOfTasks(obj, callback) {
        API_TODO.InsertCategoryOfTasks(obj).then(rs => {
            if (rs.data.errorCode === 200) {
                var data = JSON.parse(rs.data.data);
                var rs = {
                    Title: data.Title,
                    CategoryOfTaskGuid: data.CategoryOfTaskGuid,
                    DateTimeNow: null,
                    AddCategory: false,
                    IsFollow: null
                };
                callback(rs);
            }
        }).catch(error => {
            Alert.alert("Thông báo", "Có lỗi khi thêm mới nhóm");
            console.log('có lỗi khi thêm mới nhóm')
            console.log(error)
        });
    },
    UpdateCategoryOfTasks(obj, callback) {
        API_TODO.UpdateCategoryOfTasks(obj).then(rs => {
            if (rs.data.errorCode === 200) {
                var data = JSON.parse(rs.data.data);
                var rs = {
                    Title: data.Title,
                    CategoryOfTaskGuid: data.CategoryOfTaskGuid,
                    DateTimeNow: null,
                    UpdateTitle: false,
                    IsFollow: null
                };
                callback(rs);
            }
        }).catch(error => {
            Alert.alert("Thông báo", "Có lỗi khi thay đổi tên nhóm");
            console.log(error)
        });
    },
    GetData(taskGuid, callback) {
        var obj = {
            TaskGuid: taskGuid
        }
        API_TODO.GetItemTasks(obj).then(res => {
            var _data = JSON.parse(res.data.data);
            if (_data !== null) {
                var _obj = { IdS: [taskGuid] }
                for (let i = 0; i < _data.TaskOfUsers.length; i++) {
                    if (_data.TaskOfUsers[i].LoginName === global.__appSIGNALR.SIGNALR_object.USER.LoginName) {
                        if (FuncCommon.ConDate(_data.TaskOfUsers[i].ActualStartDate, 0) === FuncCommon.ConDate(new Date(), 0)) {
                            _data.StatusMyDay = true;
                        } else {
                            _data.StatusMyDay = false;
                        }
                    }
                }
                API_TODO.GetAttList(_obj).then(rs => {
                    var _Att = JSON.parse(rs.data.data);
                    var listatt = [];
                    if (_Att.length > 0) {
                        for (let i = 0; i < _Att.length; i++) {
                            listatt.push(
                                {
                                    AttachmentGuid: _Att[i].AttachmentGuid,
                                    name: _Att[i].Title.toLowerCase(),
                                });
                        }
                    }
                    var _callback = {
                        _data: _data,
                        listatt: listatt
                    }
                    callback(_callback);
                }).catch(error => {
                    // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
                    console.log('Error when call API Mobile. ' + error);
                    this.setState({
                        loading: false
                    });
                });
            } else {
                callback(null);
            }
        }).catch(error => {
            console.log('Error when call API Mobile. ' + error);
        });

    }
}