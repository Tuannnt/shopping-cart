import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {AppColors, AppStyles, AppSizes, getStatusBarHeight} from '@theme';
import {
  Keyboard,
  Platform,
  Text,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  TextInput,
  RefreshControl,
  FlatList,
  ImageBackground,
  Dimensions,
  StyleSheet,
  Animated,
  Alert,
} from 'react-native';
import {FuncCommon} from '../../../utils';
import {Header, Icon, Divider} from 'react-native-elements';
import HeaderToDo from './Component_ToDo/HeaderToDo';
import stylesToDo from './css/style_ToDo';
import colorsToDo from './css/colors_ToDo';
import TabView_ToDo_Component from './Component_ToDo/TabView_ToDo_Component';
import {API, API_TODO, API_TM_TASKS} from '@network';
import LoadingComponent from '../../component/LoadingComponent';
import Combobox from '../../component/Combobox';
import { listCombobox,  } from './listCombobox'
import { controller } from './controller'
class ListData_ToDo_Component extends Component {
  constructor(props) {
    super(props);
    //Hiệu ứng
    this.animated_DoneOn = new Animated.Value(0);
    this.state = {
      setoffDone: false,
      loading: false,
      isShowDone: true,
      TitleBack:
        this.props.TitleBack !== undefined ? this.props.TitleBack : 'Danh sách',
      ListData: [],
      Title: props.Data.Title,
      DateTimeNow: props.Data.DateTime,
      FormAdd: false,
      Text: '',
      handleInput: true,
      TextAdd: '',
      AddCategory:
        this.props.AddCategory !== undefined ? this.props.AddCategory : false,
      UpdateTitle: false,
      CategoryOfTaskGuid: props.Data.CategoryOfTaskGuid,
      ListCombobox: [],
      NumberPage: 1,
      CheckBox: false,
      ListCheck: [],
    };
    this.DataSelect = {
      txtSearch: '',
      Length: 100,
      NumberPage: 1,
      StartDate: null,
      EndDate: null,
      Query: 'ModifiedDate DESC',
      CategoryOfTaskGuid: props.Data.CategoryOfTaskGuid,
      DateTime: props.Data.DateTime,
      IsFollow: props.Data.IsFollow,
      UserInformation: null,
    };
    this.DoneOn = Animated.timing(this.animated_DoneOn, {
      toValue: 1,
      duration: 1000,
    });
    this.DoneOff = Animated.timing(this.animated_DoneOn, {
      toValue: 0,
      duration: 1000,
    });
  }
  componentDidMount() {
    if (this.props.AddCategory !== true) {
      this.SelectData();
    }
    if (this.props.Data.CategoryOfTaskGuid === null) {
      this.ListComboboxAddMyDay();
    }
    this.Information();
  }
  componentWillReceiveProps = nextProps => {
    if (this.state.AddCategory === false) {
      this.SelectData();
    }
    this.Information();
  };
  Information() {
    controller.getProfile(null, rs => {
      this.setState({UserInformation: rs});
    });
  }
  SelectData() {
    this.setState({loading: true});
    controller.ListTasks(this.DataSelect, this.state.isShowDone, rs => {
      this.setState({ListData: rs, loading: false});
      Animated.sequence([this.DoneOn]).start();
    });
  }
  ListComboboxAddMyDay = () => {
    controller.ListTasks(this.state.NumberPage, this.state.ListCombobox, rs => {
      this.setState({ListCombobox: rs});
    });
  };
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          {this.state.loading !== true ? (
            <View style={[stylesToDo.container_Full]}>
              <ImageBackground
                source={require('@images/TM/ImageToDo/ImageBackground/rungcay.jpg')}
                style={stylesToDo.FullSize}>
                {this.state.CheckBox === true ? (
                  <HeaderToDo
                    leftTitle={this.state.TitleBack}
                    Customcolor={colorsToDo.white}
                    Title="Ngày của tôi"
                    hideTitle={true}
                    hiderMenu={true}
                    hiderDelete={false}
                    hidelightbulb={
                      this.props.Data.DateTime !== null ? true : false
                    }
                    CallbackBack={() => this.CallbackBack()}
                    CallbackDeleteItem={() => this.CallbackDeleteItem()}
                    CallbackLightbulb={() => this.CallbackLightbulb()}
                  />
                ) : (
                  <HeaderToDo
                    leftTitle={this.state.TitleBack}
                    Customcolor={colorsToDo.white}
                    Title="Ngày của tôi"
                    hideTitle={true}
                    hiderMenu={false}
                    hiderDelete={true}
                    hidelightbulb={
                      this.props.Data.DateTime !== null ? true : false
                    }
                    CallbackBack={() => this.CallbackBack()}
                    CallbackThreeDots={() => this.CallbackThreeDots()}
                    CallbackLightbulb={() => this.CallbackLightbulb()}
                  />
                )}
                {this.state.AddCategory === true ? (
                  <View
                    style={[
                      stylesToDo.alignItems,
                      {
                        flexDirection: 'row',
                        padding: 10,
                        backgroundColor: 'rgba(83, 112, 89,0.9)',
                        borderRadius: 10,
                      },
                    ]}>
                    <TextInput
                      style={[{flex: 1, color: '#fff', fontSize: 20}]}
                      ref="x"
                      underlineColorAndroid="transparent"
                      autoCapitalize="none"
                      placeholder={'Thêm mới nhóm công việc'}
                      autoFocus={true}
                      onEndEditing={event => this.SubmitCategory(event)}
                    />
                  </View>
                ) : null}
                {this.state.AddCategory !== true ? (
                  <View style={[{padding: 10, marginLeft: 10}]}>
                    {this.state.CategoryOfTaskGuid !== null &&
                    this.state.UpdateTitle === true ? (
                      <View
                        style={[
                          stylesToDo.alignItems,
                          {
                            flexDirection: 'row',
                            padding: 10,
                            backgroundColor: 'rgba(83, 112, 89,0.9)',
                            borderRadius: 10,
                          },
                        ]}>
                        <TextInput
                          style={[{flex: 1, color: '#fff', fontSize: 20}]}
                          ref="x"
                          underlineColorAndroid="transparent"
                          autoCapitalize="none"
                          placeholder={'nhập tên nhóm công việc'}
                          value={this.state.Title}
                          onChangeText={text => this.setState({Title: text})}
                          autoFocus={true}
                          onEndEditing={event => this.UpdateCategory(event)}
                        />
                      </View>
                    ) : (
                      <TouchableOpacity
                        onPress={() => this.setState({UpdateTitle: true})}>
                        <Text
                          style={{
                            fontSize: 30,
                            color: colorsToDo.white,
                            fontWeight: 'bold',
                          }}>
                          {this.state.Title !== ''
                            ? this.state.Title
                            : 'Danh sách chưa đặt tên'}
                        </Text>
                      </TouchableOpacity>
                    )}
                    {this.state.DateTimeNow !== null ? (
                      <Text
                        style={[
                          stylesToDo.styleText,
                          {color: colorsToDo.white},
                        ]}>
                        {this.CustomDateTime(this.state.DateTimeNow)}
                      </Text>
                    ) : null}
                  </View>
                ) : null}
                {this.state.AddCategory !== true &&
                this.state.FormAdd === true ? (
                  <View style={styles.styleListView}>
                    <View
                      style={{
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                      }}>
                      <Icon
                        name={'circle'}
                        type={'feather'}
                        color={colorsToDo.gray}
                        size={22}
                      />
                    </View>
                    <TextInput
                      style={[stylesToDo.styleText, {flex: 1}]}
                      ref="x"
                      underlineColorAndroid="transparent"
                      autoCapitalize="none"
                      placeholder={'Nhập tên công việc'}
                      value={this.state.TextAdd}
                      autoFocus={true}
                      onChangeText={text => this.setState({TextAdd: text})}
                      onSubmitEditing={() => this.Enter()}
                      onEndEditing={() => this.Submit()}
                    />
                    {/* <View style={{ flex: 1 }} /> */}
                  </View>
                ) : null}
                {this.state.AddCategory !== true &&
                this.state.ListData.length > 0
                  ? this.ViewListData(this.state.ListData)
                  : null}
                {this.state.AddCategory !== true ? this.ViewFooter() : null}
              </ImageBackground>
              <TabView_ToDo_Component
                callback={this.TabViewChange}
                setoffDone={this.state.setoffDone}
                nameMenu={'Tuỳ chọn danh sách'}
                eOpen={this.openTabView}
              />
              {this.state.ListCombobox.length > 0 ? (
                <Combobox
                  TypeSelect={'multiple'} // single or multiple
                  callback={this.ChoiceAddMyDay}
                  data={this.state.ListCombobox}
                  nameMenu={'Đề xuất'}
                  eOpen={this.openCombobox}
                  position={'bottom'}
                  paging={true}
                  callback_Paging={callBack => this.nextpage(callBack)}
                  callback_SearchPaging={(callback, textsearch) =>
                    this.Searchpage(callback, textsearch)
                  }
                />
              ) : null}
            </View>
          ) : (
            <LoadingComponent backgroundColor={'#fff'} />
          )}
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }

  //#region List
  ViewListData = rs => {
    const rotateStart = this.animated_DoneOn.interpolate({
      inputRange: [0, 1],
      outputRange: ['270deg', '360deg'],
    });
    return (
      <FlatList
        data={rs}
        style={{flex: 1}}
        renderItem={({item, index}) => (
          <View>
            {this.state.setoffDone !== true &&
            item.viewTitleDone &&
            item.viewTitleDone === true ? (
              <TouchableOpacity
                style={[stylesToDo.alignItems, styles.viewTitleDone]}
                onPress={() => {
                  (this.state.isShowDone = !this.state.isShowDone),
                    this.SetFormDone();
                }}>
                <Animated.View
                  style={{
                    transform: [{rotate: rotateStart}, {perspective: 4000}],
                  }}>
                  <Icon
                    name={'chevron-down'}
                    type={'feather'}
                    size={20}
                    iconStyle={{color: colorsToDo.white}}
                  />
                </Animated.View>
                <Text style={[stylesToDo.styleText, {color: colorsToDo.white}]}>
                  Đã hoàn thành
                </Text>
              </TouchableOpacity>
            ) : null}

            {item.isShow === false ? null : (
              <View style={[{flexDirection: 'row'}]}>
                {this.state.CheckBox === false ? null : (
                  <TouchableOpacity
                    style={{
                      padding: 10,
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}
                    onPress={() => this.setcheckbox(item, index)}>
                    <Icon
                      name={item.CheckBox === true ? 'check' : 'square'}
                      type={item.CheckBox === true ? 'entypo' : 'feather'}
                      color={
                        item.CheckBox === true ? '#FCCB00' : colorsToDo.gray
                      }
                      size={22}
                    />
                  </TouchableOpacity>
                )}
                <View style={[styles.styleListView, {flex: 1}]}>
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}
                    onPress={() => {
                      item.StatusCompleted !== 'C'
                        ? this.UpdateProcessSuccess(item)
                        : null;
                    }}>
                    <Icon
                      name={
                        item.StatusCompleted === 'C' ? 'checkcircle' : 'circle'
                      }
                      type={
                        item.StatusCompleted === 'C' ? 'antdesign' : 'feather'
                      }
                      color={colorsToDo.gray}
                      size={22}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      stylesToDo.justifyContent,
                      {flex: 8, flexDirection: 'column'},
                    ]}
                    onPress={() => this.NextPage(item.TaskGuid)}>
                    <View>
                      <Text style={[stylesToDo.styleText]}>{item.Subject}</Text>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      {item.ActualStartDate !== '' &&
                      item.ActualStartDate !== null &&
                      item.ActualStartDate !== undefined ? (
                        <Icon
                          name={'light-down'}
                          type={'entypo'}
                          color={colorsToDo.gray}
                          size={12}
                          iconStyle={{marginRight: 5}}
                        />
                      ) : null}
                      {item.TimeReminder !== '' &&
                      item.TimeReminder !== null &&
                      item.TimeReminder !== undefined ? (
                        <Icon
                          name={'bell'}
                          type={'feather'}
                          color={colorsToDo.gray}
                          size={12}
                          iconStyle={{marginRight: 5}}
                        />
                      ) : null}
                      {item.CountAtt !== '0' &&
                      item.Type !== null &&
                      item.Type !== undefined ? (
                        <Icon
                          name={'attachment'}
                          type={'entypo'}
                          color={colorsToDo.gray}
                          size={12}
                          iconStyle={{marginRight: 5}}
                        />
                      ) : null}
                      {item.Type !== '' &&
                      item.Type !== null &&
                      item.Type !== undefined ? (
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <Icon
                            name={'repeat'}
                            type={'feather'}
                            color={colorsToDo.gray}
                            size={10}
                            iconStyle={{marginRight: 5}}
                          />
                          <Text style={[stylesToDo.styleContent]}>
                            {this.customType(item.Type)}
                          </Text>
                        </View>
                      ) : null}
                      {item.EndDate !== '' &&
                      item.EndDate !== null &&
                      item.EndDate !== undefined ? (
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <Icon
                            name={'calendar'}
                            type={'feather'}
                            color={colorsToDo.gray}
                            size={12}
                            iconStyle={{marginRight: 5}}
                          />
                          <Text style={[stylesToDo.styleContent]}>
                            {FuncCommon.ConDate(item.EndDate, 0)}
                          </Text>
                        </View>
                      ) : null}
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[stylesToDo.contentCenter, {flex: 1}]}
                    onPress={() => this.UpdateFollow(item)}>
                    <Icon
                      name={item.IsFollow === true ? 'star' : 'star-o'}
                      type={'font-awesome'}
                      color={colorsToDo.gray}
                      size={20}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
        //onEndReached={() => this.loadMoreData()}
        // refreshControl={
        //     < RefreshControl
        //         refreshing={false}
        //         onRefresh={() => this._onRefresh}
        //         tintColor="#f5821f"
        //         titleColor="#fff"
        //         colors={[colorsToDo.MyDayColor]}
        //     />
        // }
      />
    );
  };
  //#endregion

  //#region Footer
  ViewFooter = () => {
    return (
      <View
        style={[
          stylesToDo.alignItems,
          {
            flexDirection: 'row',
            padding: 10,
            margin: 10,
            backgroundColor: 'rgba(83, 112, 89,0.9)',
            borderRadius: 10,
          },
        ]}>
        <TouchableOpacity
          style={[stylesToDo.alignItems, {flexDirection: 'row', flex: 1}]}
          onPress={() => this.OpenFormAdd()}>
          <Icon
            name={'plus'}
            type={'feather'}
            color={colorsToDo.white}
            size={25}
            iconStyle={{paddingLeft: 5, paddingRight: 10}}
          />
          <Text style={[stylesToDo.styleText, {color: colorsToDo.white}]}>
            Thêm tác vụ
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  //#endregion

  //#region chức năng

  //NextPage
  NextPage(val) {
    Actions.openToDo({
      TitleHeader: this.state.Title,
      ColorMain: colorsToDo.MyDayColor,
      TaskGuid: val,
    });
  }
  //Back List
  CallbackBack = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  };
  //OpenFormAdd
  OpenFormAdd = () => {
    this.setState({
      FormAdd: !this.state.FormAdd,
    });
  };
  // Hiện thị tab tuỳ chỉnh
  _openTabView() {}
  openTabView = d => {
    this._openTabView = d;
  };
  CallbackThreeDots() {
    this._openTabView();
  }
  TabViewChange = rs => {
    switch (rs) {
      case 'deleteCategory':
        Alert.alert(
          'Xác nhận',
          'Bạn chắc chắn muốn xoá ' + this.state.Title + ' vĩnh viễn?',
          [
            {text: 'Đồng ý', onPress: () => this.DeleteCategory()},
            {text: 'Huỷ bỏ', onPress: () => console.log('No Pressed')},
          ],
          {cancelable: true},
        );
        break;
      case 'setoffDone':
        this.setState({setoffDone: !this.state.setoffDone});
        break;
      case 'checkbox':
        this.setState({CheckBox: true});
        break;
      case 'SortCreateDate':
        this.DataSelect.Query = 'CreateDate';
        this.SelectData();
        break;
      default:
        break;
    }
  };
  DeleteCategory = () => {
    if (this.props.Data.CategoryOfTaskGuid !== null) {
      var obj = {IdS: [this.props.Data.CategoryOfTaskGuid.toString()]};
      API_TODO.DeleteCategoryOfTasks(obj)
        .then(rs => {
          this.CallbackBack();
        })
        .catch(error => {
          alert('Có lỗi Xoá dữ liệu');
          console.log('Có lỗi khi cập nhật theo dõi', error);
        });
    }
  };

  //#region CustomDateTime
  CustomDateTime = date => {
    if (date !== null) {
      var _getDay = new Date(date).getDay();
      var returnGetDay = '';
      switch (_getDay) {
        case 1:
          returnGetDay = 'Thứ 2';
          break;
        case 2:
          returnGetDay = 'Thứ 3';
          break;
        case 3:
          returnGetDay = 'Thứ 4';
          break;
        case 4:
          returnGetDay = 'Thứ 5';
          break;
        case 5:
          returnGetDay = 'Thứ 6';
          break;
        case 6:
          returnGetDay = 'Thứ 7';
          break;
        default:
          returnGetDay = 'Chủ nhật';
          break;
      }
      var _getDate = new Date(date).getDate();
      var _getMonth = new Date(date).getMonth() + 1;
      return returnGetDay + ', ngày ' + _getDate + ' tháng ' + _getMonth;
    } else {
      return '';
    }
  };
  //#endregion

  //#region statusDone
  statusDone = item => {
    var data = this.state.ListData;
    try {
      for (let i = 0; i < data.length; i++) {
        if (data[i].key === item.key) {
          data[i].Done = !data[i].Done;
        }
      }
      this.setState({ListData: data});
      this.SelectData();
    } catch (error) {
      alert('Có lỗi khi thay đổi dữ liệu');
    }
  };
  //#endregion

  //#region UpdateFollow
  UpdateFollow = item => {
    var obj = {
      IdS: [item.TaskGuid.toString()],
    };
    API_TODO.UpdateFollow(obj)
      .then(rs => {
        if (rs.data.errorCode == 200) {
          var _list = this.state.ListData;
          if (this.props.Data.IsFollow !== 1) {
            for (let i = 0; i < _list.length; i++) {
              if (_list[i].TaskGuid == item.TaskGuid) {
                _list[i].IsFollow = !_list[i].IsFollow;
                break;
              }
            }
            this.setState({ListData: _list});
          } else {
            var data = [];
            for (let i = 0; i < _list.length; i++) {
              if (_list[i].TaskGuid == item.TaskGuid) {
                _list[i].IsFollow = !_list[i].IsFollow;
              }
              if (_list[i].IsFollow == true) {
                data.push(_list[i]);
              }
            }
            this.setState({ListData: data});
          }
        }
      })
      .catch(error => {
        alert('Có lỗi khi cập nhật theo dõi');
        console.log('Có lỗi khi cập nhật theo dõi', error);
      });
  };
  //#endregion

  //#region UpdateProcessSuccess
  UpdateProcessSuccess = item => {
    let obj = {
      IdS: [item.TaskGuid],
    };
    API_TODO.UpdateProcessSuccess(obj)
      .then(rs => {
        var data = this.state.ListData;
        var _dataView = [];
        var viewTitleDone = true;
        for (let i = 0; i < data.length; i++) {
          if (
            data[i].TaskGuid == item.TaskGuid &&
            data[i].StatusCompleted !== 'C'
          ) {
            data[i].StatusCompleted = 'C';
          } else if (
            data[i].TaskGuid == item.TaskGuid &&
            data[i].StatusCompleted === 'C'
          ) {
            data[i].StatusCompleted = 'P';
          }
          if (data[i].StatusCompleted !== 'C') {
            data[i].isShow = true;
            data[i].viewTitleDone = false;
            _dataView.push(data[i]);
          }
        }
        for (let i = 0; i < data.length; i++) {
          if (data[i].StatusCompleted === 'C') {
            if (viewTitleDone === true) {
              data[i].viewTitleDone = true;
              viewTitleDone = false;
            } else {
              data[i].viewTitleDone = false;
            }
            data[i].isShow = this.state.isShowDone;
            _dataView.push(data[i]);
          }
        }
        this.setState({ListData: _dataView});
      })
      .catch(error => {
        alert('Có lỗi khi cập nhật trạng thái hoàn thành công việc');
        console.log('Error when call API Mobile.', error);
      });
  };
  //#endregion

  //#region Load lại thu nhỏ
  SetFormDone = () => {
    var data = this.state.ListData;
    for (let i = 0; i < data.length; i++) {
      if (data[i].StatusCompleted === 'C') {
        data[i].isShow = this.state.isShowDone;
      }
    }
    this.setState({ListData: data});
    if (this.state.isShowDone === true) {
      Animated.sequence([this.DoneOn]).start();
    } else {
      Animated.sequence([this.DoneOff]).start();
    }
  };

  //#endregion

  //#region  Submit
  focusTextInput(node) {
    this.refs[node].focus();
  }
  Enter = () => {
    if (this.state.TextAdd !== '') {
      this.state.handleInput = false;
      var category = this.state.CategoryOfTaskGuid;
      controller.Insert(
        this.state.TextAdd,
        this.state.ListData,
        category,
        rs => {
          this.setState({ListData: rs, TextAdd: ''});
          setTimeout(() => {
            this.focusTextInput('x');
          }, 1);
        },
      );
    } else {
      this.state.handleInput = true;
      this.setState({FormAdd: !this.state.FormAdd, TextAdd: ''});
    }
  };
  Submit = () => {
    if (!this.state.handleInput) {
      this.state.handleInput = true;
      return;
    }
    if (this.state.TextAdd !== '') {
      this.state.handleInput = false;
      var category = this.state.CategoryOfTaskGuid;
      controller.Insert(
        this.state.TextAdd,
        this.state.ListData,
        category,
        rs => {
          this.setState({ListData: rs, TextAdd: ''});
        },
      );
    } else {
      this.setState({FormAdd: !this.state.FormAdd, TextAdd: ''});
    }
  };
  //#endregion

  //#region SubmitCategory
  SubmitCategory = event => {
    var obj = {
      Title: event.nativeEvent.text,
    };
    controller.InsertCategoryOfTasks(obj, rs => {
      this.setState({
        Title: rs.Title,
        CategoryOfTaskGuid: rs.CategoryOfTaskGuid,
        DateTimeNow: null,
        AddCategory: false,
        IsFollow: null,
      });
    });
  };
  //#endregion
  //#region UpdateCategory
  UpdateCategory = event => {
    var obj = {
      CategoryOfTaskGuid: this.state.CategoryOfTaskGuid,
      Title: this.state.Title,
    };
    controller.UpdateCategoryOfTasks(obj, rs => {
      this.setState({
        Title: rs.Title,
        CategoryOfTaskGuid: rs.CategoryOfTaskGuid,
        DateTimeNow: null,
        AddCategory: false,
        IsFollow: null,
        UpdateTitle: false,
      });
    });
  };
  //#endregion
  //#region customType
  customType = val => {
    var rs = '';
    switch (val) {
      case 'D':
        rs = 'Hàng ngày';
        break;
      case 'W':
        rs = 'Hàng tuần';
        break;
      case 'M':
        rs = 'Hàng tháng';
        break;
      case 'Y':
        rs = 'Hàng năm';
        break;
      default:
        break;
    }
    return rs;
  };

  //#endregion
  //#region CallbackLightbulb
  CallbackLightbulb = () => {
    this.onActionCombobox();
  };
  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCombobox() {
    this._openCombobox();
  }
  ChoiceAddMyDay = rs => {
    if (rs[0].value !== null && rs[0].value !== '') {
      for (let i = 0; i < rs.length; i++) {
        if (rs[i].value !== null && rs[i].value !== '') {
          var obj = {
            IdS: [rs[i].value],
            ActualStartDate: new Date(),
          };
          API_TODO.UpdateMyDate(obj)
            .then(rs => {
              if (rs.data.errorCode === 200) {
              }
            })
            .catch(error => {
              // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
              console.log('Xin lỗi. Đã xảy ra.');
            });
        }
      }
      this.SelectData();
    }
  };
  //#endregion
  nextpage = callback => {
    this.state.NumberPage++;
    this.setState({
      NumberPage: this.state.NumberPage,
    });
    var obj = {
      txtSearch: '',
      NumberPage: this.state.NumberPage,
      Length: 50,
    };
    API_TODO.ListTasks_AddMyDay(obj)
      .then(rs => {
        if (rs.data.errorCode === 200) {
          var data = JSON.parse(rs.data.data).data;
          var List = [];
          for (let i = 0; i < data.length; i++) {
            List.push({
              value: data[i].TaskGuid,
              text: data[i].Subject,
            });
          }
          callback(List);
        }
      })
      .catch(error => {
        console.log('error:', error);
        Alert.alert('Thông báo', 'Có lỗi khi lấy thông tin');
      });
  };
  Searchpage = (callback, textsearch) => {
    this.setState({
      NumberPage: 1,
    });
    var obj = {
      txtSearch: textsearch,
      NumberPage: this.state.NumberPage,
      Length: 50,
    };
    API_TODO.ListTasks_AddMyDay(obj)
      .then(rs => {
        if (rs.data.errorCode === 200) {
          var data = JSON.parse(rs.data.data).data;
          var List = [];
          for (let i = 0; i < data.length; i++) {
            List.push({
              value: data[i].TaskGuid,
              text: data[i].Subject,
            });
          }
          callback(List);
        }
      })
      .catch(error => {
        console.log('error:', error);
        Alert.alert('Thông báo', 'Có lỗi khi lấy thông tin');
      });
  };
  setcheckbox = (item, index) => {
    if (this.state.ListData[index].CheckBox !== true) {
      this.state.ListData[index].CheckBox = true;
    } else {
      this.state.ListData[index].CheckBox = false;
    }
    this.setState({
      ListData: this.state.ListData,
    });
  };
  CallbackDeleteItem = () => {
    Alert.alert(
      'Xác nhận',
      'Bạn chắc chắn muốn xoá các công việc đã chọn.',
      [
        {text: 'Đồng ý', onPress: () => this.DeleteItem()},
        {text: 'Huỷ bỏ', onPress: () => console.log('No Pressed')},
      ],
      {cancelable: true},
    );
  };
  DeleteItem = () => {};
  //#endregion
}
//#region Css
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const navHeight = Platform.OS === 'ios' ? 80 : 80 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
const styles = StyleSheet.create({
  styleListView: {
    flexDirection: 'row',
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: colorsToDo.white,
    marginBottom: 2,
    borderRadius: 10,
  },
  viewTitleDone: {
    flexDirection: 'row',
    padding: 6,
    margin: 10,
    backgroundColor: 'rgba(24, 32, 35,0.7)',
    borderRadius: 8,
    width: SCREEN_WIDTH / 2.5,
  },
});
//#endregion
//Redux
const mapStateToProps = state => ({});
//Using call para public all page
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListData_ToDo_Component);
