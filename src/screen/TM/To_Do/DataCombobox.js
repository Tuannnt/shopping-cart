
import { AppStyles, AppColors } from '@theme';
export default {
    ListCategories: [
        {
            key: 'Myday',
            icon: 'light-down',
            typeIcon: 'entypo',
            colorIcon: '#8B8378',
            Title: 'Ngày của tôi',
            CountContent: 0,
            count: 1
        },
        {
            key: 'IsFollow',
            icon: 'star-o',
            typeIcon: 'font-awesome',
            colorIcon: '#FFD700',
            Title: 'Quan trọng',
            CountContent: 0,
            count: 2
        },
        {
            key: 'Myday',
            icon: 'calendar',
            typeIcon: 'feather',
            colorIcon: '#008B00',
            Title: 'Đã lập kế hoạch',
            CountContent: 0,
            count: 3
        },
        {
            key: 'Myday',
            icon: 'user',
            typeIcon: 'feather',
            colorIcon: '#B23AEE',
            Title: 'Đã giao cho bạn',
            CountContent: 0,
            count: 4
        },
        {
            key: 'Myday',
            icon: 'shield',
            typeIcon: 'feather',
            colorIcon: '#4876FF',
            Title: 'Tác vụ',
            CountContent: 0,
            count: 5
        }
    ]
}