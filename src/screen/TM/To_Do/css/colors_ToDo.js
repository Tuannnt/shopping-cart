const colors = {
  primary: '#0066FF',
  gray: '#696969',
  black: '#000000',
  white: '#CFCFCF',
  red: '#D0021B',
  green: '#3BA753',
  blue: '#1769ff',

  MyDayColor: '#696969'
};
export default {
  ...colors,
};
