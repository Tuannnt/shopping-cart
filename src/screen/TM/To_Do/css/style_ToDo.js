import { StyleSheet, Dimensions } from 'react-native';
import { AppColors, AppStyles, AppSizes, getStatusBarHeight } from '@theme';
import colorsToDo from './colors_ToDo';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const navHeight = Platform.OS === 'ios' ? 100 : 100 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
export default StyleSheet.create({
    squareContainer: {
        // height: toolbarHeight,
    },
    background: {
        backgroundColor: null
    },
    container_Full: {
        flex: 1,
        flexDirection: 'column'
    },
    HeaderStyle: {
        position: 'absolute',
        zIndex: 1,
        width: SCREEN_WIDTH,
        backgroundColor: 'red',
        height: 100
    },
    contentCenter:{
        alignItems: 'center', 
        justifyContent: 'center'
    },
    styleText:{
        fontSize:16
    },
    styleContent:{
        fontSize:13,
        color:colorsToDo.gray,
        marginRight:10
    },
    styleTitle:{
        fontSize:18,
        fontWeight:"bold"
    },
    alignItems:{
        alignItems: 'center' 
    }, 
    justifyContent:{
        justifyContent: 'center'
    },
    FullSize:{
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT
    },
    fontSize6:{
        fontSize:6
    },
    fontSize8:{
        fontSize:8
    },
    fontSize10:{
        fontSize:10
    },
    fontSize12:{
        fontSize:12
    },
});
