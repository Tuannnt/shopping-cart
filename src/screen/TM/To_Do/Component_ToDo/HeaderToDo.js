import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, StatusBar, Platform, Text, Image } from 'react-native';
import { AppColors, AppStyles, AppSizes, getStatusBarHeight } from '@theme';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Header, Icon } from 'react-native-elements';
import stylesToDo from '../css/style_ToDo';
import colorsToDo from '../css/colors_ToDo';
class HeaderToDo extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.CallbackBack = this.props.CallbackBack;
        this.CallbackLightbulb = this.props.CallbackLightbulb;
        this.CallbackThreeDots = this.props.CallbackThreeDots;
        this.CallbackDeleteItem = this.props.CallbackDeleteItem;
    }
    render() {
        const {
            leftIcon,
            leftTitle,
            hideTitle,
            Customcolor,
            rightIcon,
            hideright,
            Title,
            hidelightbulb,
            hiderMenu,
            hiderDelete
        } = this.props;
        return (
            <View>
                <Header
                    containerStyle={[stylesToDo.squareContainer, stylesToDo.background, { borderBottomWidth: 0 }]}
                    placement="left"
                    leftComponent={this.renderLeftComponent(leftIcon, Customcolor, leftTitle)}
                    centerComponent={this.renderTitle(leftTitle, Customcolor, hideTitle)}
                    rightComponent={this.renderRightComponent(hideright, Customcolor, hidelightbulb, hiderMenu, hiderDelete)}
                />
            </View>
        )
        // if (Platform.OS === "ios") {
        //     return (
        //         <View>
        //             <Header
        //                 containerStyle={[stylesToDo.squareContainer, stylesToDo.background, { borderBottomWidth: 0 }]}
        //                 placement="left"
        //                 leftComponent={this.renderLeftComponent(leftIcon, Customcolor, leftTitle)}
        //                 centerComponent={this.renderTitle(leftTitle, Customcolor, hideTitle)}
        //                 rightComponent={this.renderRightComponent(hideright, Customcolor, hidelightbulb)}
        //             />
        //         </View>
        //     )
        // } else {
        //     return (
        //         <View style={[stylesToDo.squareContainer, stylesToDo.background, { flexDirection: 'row', borderBottomWidth: 0 }]}>
        //             {this.renderLeftComponent(leftIcon, Customcolor, leftTitle)}
        //             {this.renderTitle(Title, Customcolor, hideTitle)}
        //             {this.renderRightComponent(hideright, Customcolor,hidelightbulb)}
        //         </View>
        //     )
        // }
    }

    renderLeftComponent = (hideleft, Customcolor, leftTitle) => {
        return (
            <TouchableOpacity style={[stylesToDo.squareContainer, stylesToDo.alignItems, { flexDirection: 'row', flex: 3 }]} onPress={() => this.Back()}>
                <Icon name={'chevron-left'} type={'feather'} color={Customcolor} size={25} />
                <Text style={[{ fontSize: 15, color: Customcolor }]}>{leftTitle}</Text>
            </TouchableOpacity>
        );
    }
    renderTitle = (Title, Customcolor, hideTitle) => {
        return (
            hideTitle ? null :
                <View style={[stylesToDo.contentCenter, { flex: 1 }]}>
                    <Text style={[{ fontWeight: 'bold', fontSize: 16, color: Customcolor }]}>{Title}</Text>
                </View>

        );
    }
    renderRightComponent = (hideright, Customcolor, hidelightbulb, hiderMenu, hiderDelete) => {
        return (
            hideright && hideright === true ?
                <View style={[stylesToDo.contentCenter, { flex: 3 }]} />
                :
                <View style={[stylesToDo.squareContainer, stylesToDo.alignItems, { flexDirection: 'row', flex: 1, paddingRight: 5, justifyContent: 'flex-end' }]}>

                    {hidelightbulb === true ?
                        <TouchableOpacity style={[stylesToDo.alignItems, { paddingRight: 20, justifyContent: 'flex-end' }]} onPress={() => this.Lightbulb()}>
                            <Icon name={'lightbulb-on-outline'} type={'material-community'} color={Customcolor} size={25} />
                        </TouchableOpacity>
                        : null
                    }
                    {hiderMenu === true ? null :
                        <TouchableOpacity style={[stylesToDo.alignItems, { paddingRight: 10, justifyContent: 'flex-end' }]} onPress={() => this.ThreeDots()}>
                            <Icon name={'more-horizontal'} type={'feather'} color={Customcolor} size={25} />
                        </TouchableOpacity>
                    }
                    {hiderDelete === true ? null :
                        <TouchableOpacity style={[stylesToDo.alignItems, { paddingRight: 10, justifyContent: 'flex-end' }]} onPress={() => this.DeleteItem()}>
                            <Icon name={'delete-forever'} type={'material-community'} color={'red'} size={25} />
                        </TouchableOpacity>
                    }
                </View>
        );
    }
    Back = () => {
        this.CallbackBack();
    }
    Lightbulb = () => {
        this.CallbackLightbulb();
    }
    ThreeDots = () => {
        this.CallbackThreeDots();
    }
    DeleteItem = () => {
        this.CallbackDeleteItem();
    }
}
export default HeaderToDo;
