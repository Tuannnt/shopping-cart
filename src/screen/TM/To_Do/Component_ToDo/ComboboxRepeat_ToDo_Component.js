import React, { Component, } from 'react';
import { Icon, Divider } from "react-native-elements";
import { Text, View, Animated, Dimensions, Easing, TouchableOpacity, StyleSheet, ScrollView, KeyboardAvoidingView, Image } from "react-native";
import { AppStyles, AppColors } from '@theme';
import colorsToDo from '../css/colors_ToDo';
import styleToDo from '../css/style_ToDo';
import { Item } from 'native-base';
import { request } from 'react-native-permissions';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
}
const TimeOut = 400
const ListImage = [
    {
        key: 'theme1',
        value: require('@images/TM/ImageToDo/ImageBackground/rungcay.jpg'),
    },
    {
        key: 'theme2',
        value: require('@images/TM/ImageToDo/ImageBackground/bienca.jpg'),
    },
    {
        key: 'theme3',
        value: require('@images/TM/ImageToDo/ImageBackground/nuico.jpg'),
    },
    {
        key: 'theme4',
        value: require('@images/TM/ImageToDo/ImageBackground/troisao.jpg'),
    },
    {
        key: 'theme5',
        value: require('@images/TM/ImageToDo/ImageBackground/camtrai.jpg'),
    },
    {
        key: 'theme6',
        value: require('@images/TM/ImageToDo/ImageBackground/hoa.jpg'),
    },
    {
        key: 'theme7',
        value: require('@images/TM/ImageToDo/ImageBackground/caycothu.jpg'),
    },
    {
        key: 'theme8',
        value: require('@images/TM/ImageToDo/ImageBackground/thanhpho.jpg'),
    },
    {
        key: 'theme9',
        value: require('@images/TM/ImageToDo/ImageBackground/chantrau.jpg'),
    },
    {
        key: 'theme10',
        value: require('@images/TM/ImageToDo/ImageBackground/chuyendi.jpg'),
    }
];
const ListColor = [
    {
        key: "Color1",
        value: "#226dce"
    },
    {
        key: "Color2",
        value: "#fccb00"
    },
    {
        key: "Color3",
        value: "#f47373"
    },
    {
        key: "Color4",
        value: "#37d67a"
    },
    {
        key: "Color5",
        value: "#fa28ff"
    },
    {
        key: "Color6",
        value: "#fb9e00"
    },
    {
        key: "Color7",
        value: "#808900"
    },
    {
        key: "Color8",
        value: "#808080"
    },
    {
        key: "Color9",
        value: "#795548"
    },
    {
        key: "Color10",
        value: "#607d8b"
    }
];
const ListSort = [
    {
        key: '1',
        title: 'Tâm quan trọng',
        icon: 'star-o',
        type: 'font-awesome',
        size: 20
    },
    {
        key: '2',
        title: 'Theo thứ tự bảng chữ cái',
        icon: 'sort-alpha-asc',
        type: 'font-awesome',
        size: 20
    },
    {
        key: '3',
        title: 'Ngày đến hạn',
        icon: 'calendar',
        type: 'font-awesome',
        size: 20
    },
    {
        key: '4',
        title: 'Ngày tạo',
        icon: 'calendar-plus-o',
        type: 'font-awesome',
        size: 20
    }
]
class ComboboxRepeat_ToDo_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TabViewOffsetY: new Animated.Value(1000),
            opacityTabView: new Animated.Value(0),
            DateTime_offsetX: new Animated.Value(DRIVER.width),
            ButtonBack: new Animated.Value(1),
            height: DRIVER.height,
            isOpen: false,
            isOpenTheme: false,
            dataSubmit: null,
            NameMenu: props.nameMenu ? props.nameMenu : 'Tuỳ chỉnh',
            CheckAllValue: false,
            ClickColor: true,
            ClickImage: false,
            ListDate: [],
        };
        props.eOpen(this.eOpen);
        this.actionClick = props.callback;
    }
    actionClick() { }
    eOpen = (option) => {
        Animated.spring(
            this.state.ButtonBack,
            {
                toValue: 0,
                duration: 0,
                useNativeDriver: true
            }
        ).start();
        if (!this.state.isOpen) {
            Animated.sequence([this.opacityTabView]).start();
            Animated.sequence([this.OpenTabView]).start();
        } else {
            Animated.sequence([this.OffopacityTabView]).start();
            Animated.sequence([this.OffTabView]).start();

        }
        if (option) {
            if (option.isChangeAction) {
                this.setState({
                    data: option.data,
                })
            }
            if (option.NameMenu !== undefined && option.NameMenu !== null) {
                this.setState({
                    NameMenu: option.NameMenu,
                })
            }
        }
        if (!this.state.isOpen) {
            this.setState({
                isOpen: true,
            })
        } else {
            setTimeout(() => {
                this.setState({
                    isOpen: false,
                })
            }, 500);
        }

    }
    componentDidMount() {
        this.SkillAnimated();
        this.GetDate(new Date().getFullYear(),new Date().getMonth() + 1,true);
    }

    GetDate = (yyyy,MM,s) => {
        var _listday = this.state.ListDate;
        // số ngày trong 1 tháng
        var _nday = new Date(yyyy, MM, 0).getDate();
        for (let i = 1; i <= _nday; i++) {
            var _MM = MM < 10 ? '0' + MM : MM
            var _i = i < 10 ? '0' + i : i
            _listday.push({
                Key: '' + yyyy + '' + _MM + '' + _i + '',
                dd: _i,
                MM: MM,
                yyyy: yyyy,
                CheckBox: false,
            })
        }
        // kiểm tra ngày đầu tiên của tháng nằm vào thứ mấy trong tuần
        var _1st = new Date(yyyy, MM - 1, 1).getDay();
        if (_1st > 1 || _1st === 0) {
            var lastMM = MM - 1;
            var _LastDate = new Date(yyyy, lastMM, 0).getDate();
            for (let i = _LastDate; i >= 1; i--) {
                var _lastMM = lastMM < 10 ? '0' + lastMM : lastMM
                var _i = i < 10 ? '0' + i : i
                var _Dayofweek = new Date(yyyy, lastMM - 1, i).getDay()
                if (_Dayofweek === 1) {
                    _listday.unshift({
                        Key: '' + yyyy + '' + _lastMM + '' + _i + '',
                        dd: _i,
                        MM: lastMM,
                        CheckBox: false,
                        lastMM: true,
                    })
                    break;
                } else {
                    _listday.unshift({
                        Key: '' + yyyy + '' + _lastMM + '' + _i + '',
                        dd: _i,
                        MM: lastMM,
                        CheckBox: false,
                        lastMM: true,
                    })
                }

            }
        }
        //kiểm tra ngày cuối cùng của tháng vào thứ mấy trong tuần
        var _endst = new Date(yyyy, MM - 1, _nday).getDay();
        if (_endst > 1 || _endst === 0) {
            var NextMM = MM + 1;
            var NextDate = new Date(yyyy, NextMM, 0).getDate();
            for (let i = 1; i <= NextDate; i++) {
                var _NextMM = NextMM < 10 ? '0' + NextMM : NextMM
                var _i = i < 10 ? '0' + i : i
                var _Dayofweek = new Date(yyyy, NextMM - 1, i).getDay()
                if (_Dayofweek === 0) {
                    _listday.push({
                        Key: '' + yyyy + '' + _NextMM + '' + _i + '',
                        dd: _i,
                        MM: NextMM,
                        CheckBox: false,
                        NextMM: true,
                    })
                    break;
                } else {
                    _listday.push({
                        Key: '' + yyyy + '' + _NextMM + '' + _i + '',
                        dd: _i,
                        MM: NextMM,
                        CheckBox: false,
                        NextMM: true,
                    })
                }

            }
        }
    }
    callBack = (d) => {
        this.actionClick(d);
        this.eOpen();
    }
    OnScrollTo = (event) => {
        var x = event.nativeEvent.contentOffset.x;
        console.log(x)
        // if (x>= ((this.state.Count_Tab * DRIVER.width) + (DRIVER.width / 3))) {
        //     this.state.Count_Tab = this.state.Count_Tab + 1
        //     this.myScroll.scrollTo({
        //         x: (
        //             (DRIVER.width) - ((para.length - this.state.Count_Tab) * DRIVER.width)
        //         )
        //     });
        // } else if (x < ((this.state.Count_Tab * DRIVER.width) - (DRIVER.width / 3))) {
        //     if (this.state.Count_Tab !== 0) {
        //         this.state.Count_Tab = this.state.Count_Tab - 1
        //         this.myScroll.scrollTo({
        //             x: (
        //                 (DRIVER.width * para.length) - ((para.length - this.state.Count_Tab) * DRIVER.width)
        //             )
        //         });
        //     }
        // } else {
        //     this.myScroll.scrollTo({
        //         x: (
        //             (DRIVER.width * para.length) - ((para.length - this.state.Count_Tab) * DRIVER.width)
        //         )
        //     });
        // }
    }
    render() {
        const ButtonBack = this.state.ButtonBack.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
        })
        const opacityTabView = this.state.opacityTabView.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0.5]
        })
        return (
            this.state && this.state.isOpen ?
                <Animated.View style={[
                    {
                        zIndex: 10,
                        width: DRIVER.width,
                        position: "absolute",
                        height: DRIVER.height,
                    }]}
                >
                    <Animated.View style={[{ backgroundColor: 'black', opacity: opacityTabView, height: DRIVER.height }]}>
                        {/* <TouchableOpacity style={{ flex: 1 }} onPress={() => this.eOpen()} /> */}
                    </Animated.View>
                    <Animated.View style={{
                        backgroundColor: 'white',
                        bottom: 0,
                        position: 'absolute',
                        width: DRIVER.width,
                        borderTopLeftRadius: 10, borderTopRightRadius: 10,
                        transform: [{ translateY: this.state.TabViewOffsetY }]
                    }}>
                        {/* Button dùng chung */}
                        <Animated.View style={{ opacity: ButtonBack, position: 'absolute', left: 10, top: 20, zIndex: 1000 }}>
                            <TouchableOpacity style={{}} onPress={() => this.BackTab()} >
                                <Icon color={colorsToDo.blue} name={'chevron-left'} type={'feather'} />
                            </TouchableOpacity>
                        </Animated.View>
                        <TouchableOpacity style={[styleToDo.contentCenter, { marginTop: 5, position: 'absolute', zIndex: 1000, width: DRIVER.width, }]} >
                            <View style={[{ backgroundColor: colorsToDo.gray, height: 4, width: 30, borderRadius: 20 }]} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ right: 10, position: 'absolute', top: 20, zIndex: 10 }} onPress={() => this.eOpen()}>
                            <Icon color={AppColors.gray} name={'close'} type='antdesign' />
                        </TouchableOpacity>
                        {/* the end Button dùng chung */}
                        <View style={{ flexDirection: 'column' }}>
                            {/* Begin tiêu đề */}
                            <View style={{ flexDirection: 'column', paddingBottom: 10, paddingTop: 20, justifyContent: 'flex-end' }}>
                                <View style={[styleToDo.alignItems]}>
                                    <View>
                                        <Text style={[styleToDo.styleTitle]}>
                                            {this.state.NameMenu.length > 30 ? (this.state.NameMenu).substring(0, 30) + ' ...' : this.state.NameMenu}
                                        </Text>
                                    </View>
                                    <Animated.View style={[styleToDo.alignItems, { transform: [{ translateX: this.state.DateTime_offsetX }], position: 'absolute', width: DRIVER.width, backgroundColor: '#fff' }]}>
                                        <Text style={[styleToDo.styleTitle]}>
                                            ngày tháng hiện tại
                                        </Text>
                                    </Animated.View>
                                </View>
                            </View>
                            {/* the end tiêu đề */}
                            <Divider />
                            <View style={{ flexDirection: 'column', bottom: 0 }}>
                                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }}>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'back-in-time'} type={'entypo'} size={20} />
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={styleToDo.styleText}>Cuối ngày</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }}>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'calendar'} type={'feather'} size={20} />
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={styleToDo.styleText}>Ngày mai</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }}>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'calendar'} type={'feather'} size={20} />
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={styleToDo.styleText}>Tuần tới</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => this.OffsetCustom()}>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'calendar'} type={'feather'} size={20} />
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={styleToDo.styleText}>Chọn ngày và giờ</Text>
                                    </View>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'chevron-right'} type={'feather'} size={20} />
                                    </View>
                                </TouchableOpacity>
                                {/* Begin tab theme */}
                                {this.FormCustom()}
                                {/* the end tab isOpenTheme */}
                            </View>
                        </View>
                    </Animated.View >
                </Animated.View >
                : null
        );
    }
    //#region giao diện Form ngày tháng tuỳ chọn
    FormCustom = () => {
        return (
            <Animated.View style={{ transform: [{ translateX: this.state.DateTime_offsetX }], position: 'absolute', width: DRIVER.width, backgroundColor: '#fff' }}>
                <View style={{ flexDirection: 'column' }}>
                    <View style={[styleToDo.contentCenter, { flexDirection: 'row',flexWrap: 'wrap',  padding: 5, marginLeft: 10, marginRight: 10, marginBottom: 5, marginTop: 5  }]}>
                        <View style={[styleToDo.contentCenter, { flex: 1 }]}><Text style={[styleToDo.fontSize12, { fontWeight: 'bold' }]}>Th 2</Text></View>
                        <View style={[styleToDo.contentCenter, { flex: 1 }]}><Text style={[styleToDo.fontSize12, { fontWeight: 'bold' }]}>Th 3</Text></View>
                        <View style={[styleToDo.contentCenter, { flex: 1 }]}><Text style={[styleToDo.fontSize12, { fontWeight: 'bold' }]}>Th 4</Text></View>
                        <View style={[styleToDo.contentCenter, { flex: 1 }]}><Text style={[styleToDo.fontSize12, { fontWeight: 'bold' }]}>Th 5</Text></View>
                        <View style={[styleToDo.contentCenter, { flex: 1 }]}><Text style={[styleToDo.fontSize12, { fontWeight: 'bold' }]}>Th 6</Text></View>
                        <View style={[styleToDo.contentCenter, { flex: 1 }]}><Text style={[styleToDo.fontSize12, { fontWeight: 'bold' }]}>Th 7</Text></View>
                        <View style={[styleToDo.contentCenter, { flex: 1 }]}><Text style={[styleToDo.fontSize12, { fontWeight: 'bold' }]}>CN</Text></View>
                    </View>
                    <Divider />
                    <ScrollView ref={(ref) => { this.myScroll = ref }} onScrollEndDrag={(event) => this.OnScrollTo(event)} horizontal={true} showsHorizontalScrollIndicator={false} >
                        <View style={[styleToDo.contentCenter, {
                            width: DRIVER.width,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                        }]}>
                            {this.state.ListDate.length > 0 ?
                                this.state.ListDate.map((item, i) => (
                                    <TouchableOpacity key={i} style={[{ padding: 5, marginLeft: 10, marginRight: 15, marginBottom: 5, marginTop: 5 }]} >
                                        <Text style={[item.lastMM === true || item.NextMM == true ? { color: '#C0C0C0' } : {}, styleToDo.fontSize12]}>{item.dd}</Text>
                                    </TouchableOpacity>
                                ))
                                : null
                            }
                        </View>
                        <View style={[styleToDo.contentCenter, {
                            width: DRIVER.width,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                        }]}>
                            {this.state.ListDate.length > 0 ?
                                this.state.ListDate.map((item, i) => (
                                    <TouchableOpacity key={i} style={[{ padding: 5, marginLeft: 10, marginRight: 15, marginBottom: 5, marginTop: 5 }]} >
                                        <Text style={[item.lastMM === true || item.NextMM == true ? { color: '#C0C0C0' } : {}, styleToDo.fontSize12]}>{item.dd}</Text>
                                    </TouchableOpacity>
                                ))
                                : null
                            }
                        </View>
                        <View style={[styleToDo.contentCenter, {
                            width: DRIVER.width,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                        }]}>
                            {this.state.ListDate.length > 0 ?
                                this.state.ListDate.map((item, i) => (
                                    <TouchableOpacity key={i} style={[{ padding: 5, marginLeft: 10, marginRight: 15, marginBottom: 5, marginTop: 5 }]} >
                                        <Text style={[item.lastMM === true || item.NextMM == true ? { color: '#C0C0C0' } : {}, styleToDo.fontSize12]}>{item.dd}</Text>
                                    </TouchableOpacity>
                                ))
                                : null
                            }
                        </View>
                    </ScrollView>
                    <Divider />
                    <TouchableOpacity style={{ flexDirection: 'row', padding: 5, marginTop: 10, marginBottom: 30 }}>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Text style={styleToDo.styleText}>Chọn thời gian</Text>
                        </View>
                        <View style={[styleToDo.contentCenter, { width: 40 }]}>
                            <Icon color={colorsToDo.black} name={'chevron-right'} type={'feather'} size={20} />
                        </View>
                    </TouchableOpacity>
                </View>
            </Animated.View>
        )
    }
    //#endregion

    OffsetCustom = () => {
        Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: -100,
                duration: TimeOut,
                useNativeDriver: true,
            }
        ).start();
        Animated.sequence([this.OnTabDateTime]).start();
        Animated.sequence([this.ButtonBack]).start();
    }

    BackTab = () => {
        Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        ).start();
        Animated.sequence([this.OffButtonBack]).start();
        Animated.sequence([this.OffTabDateTime]).start();
    }
    openActionTheme = () => {
        if (!this.state.isOpenTheme) {

        } else {

        }
    }

    //#region SkillAnimated
    SkillAnimated = () => {

        //#region bật tab
        this.OpenTabView = Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region đóng tab
        this.OffTabView = Animated.timing(
            this.state.TabViewOffsetY,
            {
                toValue: this.state.height,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region  mờ tab
        this.opacityTabView = Animated.timing(
            this.state.opacityTabView,
            {
                toValue: 1,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region  mờ tab
        this.OffopacityTabView = Animated.timing(
            this.state.opacityTabView,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region mở tap dateCustom
        this.OnTabDateTime = Animated.timing(
            this.state.DateTime_offsetX,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region đóng tab dateCustom
        this.OffTabDateTime = Animated.timing(
            this.state.DateTime_offsetX,
            {
                toValue: DRIVER.width,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion

        //#region Nut chuyển hướng
        this.ButtonBack = Animated.timing(
            this.state.ButtonBack,
            {
                toValue: 1,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        this.OffButtonBack = Animated.timing(
            this.state.ButtonBack,
            {
                toValue: 0,
                duration: TimeOut,
                useNativeDriver: true,
            }
        );
        //#endregion
    }

    //#endregion
}
export default ComboboxRepeat_ToDo_Component;