import React, {Component} from 'react';
import {Icon, Divider} from 'react-native-elements';
import {
  Text,
  View,
  Animated,
  Dimensions,
  Easing,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Image,
} from 'react-native';
import {AppStyles, AppColors} from '@theme';
import colorsToDo from '../css/colors_ToDo';
import styleToDo from '../css/style_ToDo';
import {Item} from 'native-base';
import {request} from 'react-native-permissions';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const TimeOut = 400;
const ListImage = [
  {
    key: 'theme1',
    value: require('@images/TM/ImageToDo/ImageBackground/rungcay.jpg'),
  },
  {
    key: 'theme2',
    value: require('@images/TM/ImageToDo/ImageBackground/bienca.jpg'),
  },
  {
    key: 'theme3',
    value: require('@images/TM/ImageToDo/ImageBackground/nuico.jpg'),
  },
  {
    key: 'theme4',
    value: require('@images/TM/ImageToDo/ImageBackground/troisao.jpg'),
  },
  {
    key: 'theme5',
    value: require('@images/TM/ImageToDo/ImageBackground/camtrai.jpg'),
  },
  {
    key: 'theme6',
    value: require('@images/TM/ImageToDo/ImageBackground/hoa.jpg'),
  },
  {
    key: 'theme7',
    value: require('@images/TM/ImageToDo/ImageBackground/caycothu.jpg'),
  },
  {
    key: 'theme8',
    value: require('@images/TM/ImageToDo/ImageBackground/thanhpho.jpg'),
  },
  {
    key: 'theme9',
    value: require('@images/TM/ImageToDo/ImageBackground/chantrau.jpg'),
  },
  {
    key: 'theme10',
    value: require('@images/TM/ImageToDo/ImageBackground/chuyendi.jpg'),
  },
];
const ListColor = [
  {
    key: 'Color1',
    value: '#226dce',
  },
  {
    key: 'Color2',
    value: '#fccb00',
  },
  {
    key: 'Color3',
    value: '#f47373',
  },
  {
    key: 'Color4',
    value: '#37d67a',
  },
  {
    key: 'Color5',
    value: '#fa28ff',
  },
  {
    key: 'Color6',
    value: '#fb9e00',
  },
  {
    key: 'Color7',
    value: '#808900',
  },
  {
    key: 'Color8',
    value: '#808080',
  },
  {
    key: 'Color9',
    value: '#795548',
  },
  {
    key: 'Color10',
    value: '#607d8b',
  },
];
const ListSort = [
  {
    key: 'SortABC',
    title: 'Theo thứ tự bảng chữ cái',
    icon: 'sort-alpha-asc',
    type: 'font-awesome',
    size: 20,
  },
  {
    key: 'SortEndDate',
    title: 'Ngày đến hạn',
    icon: 'calendar',
    type: 'font-awesome',
    size: 20,
  },
  {
    key: 'SortCreateDate',
    title: 'Ngày tạo',
    icon: 'calendar-plus-o',
    type: 'font-awesome',
    size: 20,
  },
];
class TabView_ToDo_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TabViewOffsetY: new Animated.Value(1000),
      opacityTabView: new Animated.Value(0),
      Sort_offsetX: new Animated.Value(DRIVER.width),
      Theme_offsetX: new Animated.Value(DRIVER.width),
      ButtonBack: new Animated.Value(1),
      height: 50 * 7 + 30,
      isOpen: false,
      isOpenTheme: false,
      dataSubmit: null,
      NameMenu: props.nameMenu ? props.nameMenu : 'Tuỳ chỉnh',
      CheckAllValue: false,
      ClickColor: true,
      ClickImage: false,
    };
    props.eOpen(this.eOpen);
    this.actionClick = props.callback;
  }
  actionClick() {}
  eOpen = option => {
    Animated.spring(this.state.ButtonBack, {
      toValue: 0,
      duration: 0,
      useNativeDriver: true,
    }).start();
    if (!this.state.isOpen) {
      Animated.sequence([this.opacityTabView]).start();
      Animated.sequence([this.OpenTabView]).start();
    } else {
      Animated.sequence([this.OffopacityTabView]).start();
      Animated.sequence([this.OffTabView]).start();
    }
    if (option) {
      if (option.isChangeAction) {
        this.setState({
          data: option.data,
        });
      }
      if (option.NameMenu !== undefined && option.NameMenu !== null) {
        this.setState({
          NameMenu: option.NameMenu,
        });
      }
    }
    if (!this.state.isOpen) {
      this.setState({
        isOpen: true,
      });
    } else {
      setTimeout(() => {
        this.setState({
          isOpen: false,
        });
      }, 500);
    }
  };
  componentDidMount() {
    this.SkillAnimated();
  }
  oncallback = val => {
    this.actionClick(val);
    this.eOpen();
  };
  render() {
    const ButtonBack = this.state.ButtonBack.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });
    const opacityTabView = this.state.opacityTabView.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 0.5],
    });
    return this.state && this.state.isOpen ? (
      <Animated.View
        style={[
          {
            width: DRIVER.width,
            position: 'absolute',
            height: DRIVER.height + 10,
          },
        ]}>
        <Animated.View
          style={[
            {
              backgroundColor: 'black',
              opacity: opacityTabView,
              height: DRIVER.height,
            },
          ]}>
          <TouchableOpacity style={{flex: 1}} onPress={() => this.eOpen()} />
        </Animated.View>
        <Animated.View
          style={{
            backgroundColor: 'white',
            bottom: 0,
            position: 'absolute',
            width: DRIVER.width,
            borderRadius: 10,
            transform: [{translateY: this.state.TabViewOffsetY}],
          }}>
          {/* Button dùng chung */}
          <Animated.View
            style={{
              opacity: ButtonBack,
              position: 'absolute',
              left: 10,
              top: 20,
              zIndex: 1000,
            }}>
            <TouchableOpacity style={{}} onPress={() => this.BackTab()}>
              <Icon
                color={colorsToDo.blue}
                name={'chevron-left'}
                type={'feather'}
              />
            </TouchableOpacity>
          </Animated.View>
          <TouchableOpacity
            style={[
              styleToDo.contentCenter,
              {
                marginTop: 5,
                position: 'absolute',
                zIndex: 1000,
                width: DRIVER.width,
              },
            ]}>
            <View
              style={[
                {
                  backgroundColor: colorsToDo.gray,
                  height: 4,
                  width: 30,
                  borderRadius: 20,
                },
              ]}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{right: 10, position: 'absolute', top: 20, zIndex: 10}}
            onPress={() => this.eOpen()}>
            <Icon color={AppColors.gray} name={'close'} type="antdesign" />
          </TouchableOpacity>
          {/* the end Button dùng chung */}
          <View
            style={{
              flexDirection: 'column',
              bottom: 0,
              borderRadius: 10,
              width: DRIVER.width,
            }}>
            {/* Begin tiêu đề */}
            <View
              style={{
                flexDirection: 'column',
                paddingBottom: 10,
                paddingTop: 20,
                justifyContent: 'flex-end',
              }}>
              <View style={[styleToDo.alignItems]}>
                <View>
                  <Text style={[styleToDo.styleTitle]}>
                    {this.state.NameMenu.length > 30
                      ? this.state.NameMenu.substring(0, 30) + ' ...'
                      : this.state.NameMenu}
                  </Text>
                </View>
                <Animated.View
                  style={[
                    styleToDo.alignItems,
                    {
                      transform: [{translateX: this.state.Theme_offsetX}],
                      position: 'absolute',
                      width: DRIVER.width,
                      backgroundColor: '#fff',
                    },
                  ]}>
                  <Text style={[styleToDo.styleTitle]}>Chọn chủ đề</Text>
                </Animated.View>
                <Animated.View
                  style={[
                    styleToDo.alignItems,
                    {
                      transform: [{translateX: this.state.Sort_offsetX}],
                      position: 'absolute',
                      width: DRIVER.width,
                      backgroundColor: '#fff',
                    },
                  ]}>
                  <Text style={[styleToDo.styleTitle]}>Sắp xếp</Text>
                </Animated.View>
              </View>
            </View>
            {/* the end tiêu đề */}
            <Divider />
            <View style={{flexDirection: 'column', bottom: 0}}>
              {/* <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => this.oncallback('checkbox')}>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'edit-3'} type={'feather'} size={20} />
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={styleToDo.styleText}>Chỉnh sửa</Text>
                                    </View>
                                </TouchableOpacity> */}
              <TouchableOpacity
                style={{flexDirection: 'row', padding: 10}}
                onPress={() => this.PageSort()}>
                <View style={[styleToDo.contentCenter, {width: 40}]}>
                  <Icon
                    color={colorsToDo.black}
                    name={'format-line-spacing'}
                    type={'material'}
                    size={20}
                  />
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={styleToDo.styleText}>Sắp xếp</Text>
                </View>
                <View style={[styleToDo.contentCenter, {width: 40}]}>
                  <Icon
                    color={colorsToDo.black}
                    name={'chevron-right'}
                    type={'feather'}
                    size={20}
                  />
                </View>
              </TouchableOpacity>
              {/* <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => this.PageTheme()}>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'magic'} type={'font-awesome'} size={20} />
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={styleToDo.styleText}>Thay đổi chủ đề</Text>
                                    </View>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'chevron-right'} type={'feather'} size={20} />
                                    </View>
                                </TouchableOpacity> */}
              {/* <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }}>
                                    <View style={[styleToDo.contentCenter, { width: 40 }]}>
                                        <Icon color={colorsToDo.black} name={'printer'} type={'feather'} size={20} />
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={styleToDo.styleText}>In danh sách</Text>
                                    </View>
                                </TouchableOpacity> */}
              <TouchableOpacity
                style={{flexDirection: 'row', padding: 10}}
                onPress={() => this.oncallback('setoffDone')}>
                <View style={[styleToDo.contentCenter, {width: 40}]}>
                  <Icon
                    color={colorsToDo.black}
                    name={'checkcircleo'}
                    type={'antdesign'}
                    size={20}
                  />
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={styleToDo.styleText}>
                    {this.props.setoffDone == false
                      ? 'Ẩn các tác vụ hoàn thành'
                      : 'Hiện các tác vụ hoàn thành'}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{flexDirection: 'row', padding: 10}}
                onPress={() => this.oncallback('deleteCategory')}>
                <View style={[styleToDo.contentCenter, {width: 40}]}>
                  <Icon
                    color={colorsToDo.red}
                    name={'delete'}
                    type={'antdesign'}
                    size={20}
                  />
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={[styleToDo.styleText, {color: colorsToDo.red}]}>
                    Xoá danh sách
                  </Text>
                </View>
              </TouchableOpacity>
              {/* Begin tab theme */}
              {this.FormTheme()}
              {/* the end tab isOpenTheme */}
              {this.FormSort()}
            </View>
          </View>
        </Animated.View>
      </Animated.View>
    ) : null;
  }
  //#region giao diện Form Theme
  FormTheme = () => {
    return (
      <Animated.View
        style={{
          transform: [{translateX: this.state.Theme_offsetX}],
          position: 'absolute',
          width: DRIVER.width,
          backgroundColor: '#fff',
        }}>
        <View style={{flexDirection: 'column', height: DRIVER.height}}>
          <View style={{flexDirection: 'row'}}>
            <View style={[styleToDo.contentCenter, {padding: 15}]}>
              <TouchableOpacity
                style={[
                  styleToDo.contentCenter,
                  {
                    backgroundColor:
                      this.state.ClickColor === true
                        ? colorsToDo.gray
                        : '#e9e9e9',
                    borderRadius: 20,
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 5,
                    paddingBottom: 5,
                  },
                ]}
                onPress={() => this.ClickCategoryTheme('Color')}>
                <Text
                  style={[
                    styleToDo.fontSize12,
                    {
                      color:
                        this.state.ClickColor === true
                          ? '#fff'
                          : colorsToDo.gray,
                    },
                  ]}>
                  Màu
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={[styleToDo.contentCenter, {padding: 15, paddingLeft: 0}]}>
              <TouchableOpacity
                style={[
                  styleToDo.contentCenter,
                  {
                    backgroundColor:
                      this.state.ClickImage === true
                        ? colorsToDo.gray
                        : '#e9e9e9',
                    borderRadius: 20,
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 5,
                    paddingBottom: 5,
                  },
                ]}
                onPress={() => this.ClickCategoryTheme('Image')}>
                <Text
                  style={[
                    styleToDo.fontSize12,
                    {
                      color:
                        this.state.ClickImage === true
                          ? '#fff'
                          : colorsToDo.gray,
                    },
                  ]}>
                  Ảnh
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {this.state.ClickImage === true
              ? ListImage.map((item, i) => (
                  <TouchableOpacity style={{width: 40, height: 40, margin: 5}}>
                    <Image
                      style={{width: '100%', height: '100%', borderRadius: 20}}
                      source={item.value}
                    />
                  </TouchableOpacity>
                ))
              : ListColor.map((item, i) => (
                  <TouchableOpacity style={{width: 40, height: 40, margin: 5}}>
                    <View
                      style={{
                        width: '100%',
                        height: '100%',
                        borderRadius: 20,
                        backgroundColor: item.value,
                      }}
                    />
                  </TouchableOpacity>
                ))}
          </ScrollView>
        </View>
      </Animated.View>
    );
  };
  //#endregion

  //#region  giao diện form sort (sắp xếp)
  FormSort = () => {
    return (
      <Animated.View
        style={{
          transform: [{translateX: this.state.Sort_offsetX}],
          position: 'absolute',
          width: DRIVER.width,
          backgroundColor: '#fff',
        }}>
        <View style={{flexDirection: 'column', height: DRIVER.height}}>
          <ScrollView>
            {ListSort.map((item, i) => (
              <TouchableOpacity
                style={{flexDirection: 'row', padding: 10}}
                onPress={() => this.oncallback(item.key)}>
                <View style={[styleToDo.contentCenter, {width: 40}]}>
                  <Icon
                    color={colorsToDo.black}
                    name={item.icon}
                    type={item.type}
                    size={item.size}
                  />
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={styleToDo.styleText}>{item.title}</Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </Animated.View>
    );
  };
  //#endregion
  ClickCategoryTheme = val => {
    if (val === 'Color') {
      this.setState({ClickColor: true, ClickImage: false});
    } else {
      this.setState({ClickImage: true, ClickColor: false});
    }
  };

  PageTheme = () => {
    this.state.height = 50 * 1 + 30;
    Animated.timing(this.state.TabViewOffsetY, {
      toValue: this.state.height,
      duration: TimeOut,
      useNativeDriver: true,
    }).start();
    Animated.sequence([this.OnTabTheme]).start();
    Animated.sequence([this.ButtonBack]).start();
  };

  PageSort = () => {
    this.state.height = 0;
    Animated.timing(this.state.TabViewOffsetY, {
      toValue: this.state.height,
      duration: TimeOut,
      useNativeDriver: true,
    }).start();
    Animated.sequence([this.OnTabSort]).start();
    Animated.sequence([this.ButtonBack]).start();
  };

  BackTab = () => {
    Animated.timing(this.state.TabViewOffsetY, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    }).start();
    Animated.sequence([this.OffButtonBack]).start();
    Animated.sequence([this.OffTabTheme]).start();
    Animated.sequence([this.OffTabSort]).start();
  };
  openActionTheme = () => {
    if (!this.state.isOpenTheme) {
    } else {
    }
  };

  //#region SkillAnimated
  SkillAnimated = () => {
    //#region Tab chính

    //#region bật tab
    this.OpenTabView = Animated.timing(this.state.TabViewOffsetY, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#region đóng tab
    this.OffTabView = Animated.timing(this.state.TabViewOffsetY, {
      toValue: this.state.height,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#region  mờ tab
    this.opacityTabView = Animated.timing(this.state.opacityTabView, {
      toValue: 1,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#region  mờ tab
    this.OffopacityTabView = Animated.timing(this.state.opacityTabView, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#endregion

    //#region Tab con

    //#region mở tap theme
    this.OnTabTheme = Animated.timing(this.state.Theme_offsetX, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#region đóng tab Theme
    this.OffTabTheme = Animated.timing(this.state.Theme_offsetX, {
      toValue: DRIVER.width,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#region mở tap Sort
    this.OnTabSort = Animated.timing(this.state.Sort_offsetX, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#region đóng tab Sort
    this.OffTabSort = Animated.timing(this.state.Sort_offsetX, {
      toValue: DRIVER.width,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion

    //#endregion

    //#region Nut chuyển hướng
    this.ButtonBack = Animated.timing(this.state.ButtonBack, {
      toValue: 1,
      duration: TimeOut,
      useNativeDriver: true,
    });
    this.OffButtonBack = Animated.timing(this.state.ButtonBack, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //#endregion
  };

  //#endregion
}
export default TabView_ToDo_Component;
