import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { AppColors, AppStyles, AppSizes, getStatusBarHeight } from '@theme';
import {
    Keyboard,
    Platform,
    Text,
    TouchableOpacity,
    View,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Image,
    RefreshControl,
    FlatList,
    ImageBackground,
    Dimensions,
    StyleSheet,
    Animated
} from 'react-native';
import { Header, Icon, Divider } from 'react-native-elements';
import HeaderToDo from './Component_ToDo/HeaderToDo';
import stylesToDo from './css/style_ToDo';
import colorsToDo from './css/colors_ToDo';
import TabView_ToDo_Component from './Component_ToDo/TabView_ToDo_Component';
class MyDay_ToDo_Component extends Component {
    constructor(props) {
        super(props);
        //Hiệu ứng
        this.animated_DoneOn = new Animated.Value(0);
        this.state = {
            isShowDone: true,
            TitleBack: this.props.TitleBack !== undefined ? this.props.TitleBack : 'Danh sách',
            ListData: [
                {
                    key: "0",
                    Title: "Kế hoạch tuần",
                    Content: "Cải thiện mobile",
                    Important: true,
                    Done: true,
                    isShow: true,
                },
                {
                    key: "1",
                    Title: "Quảng bá thương hiệu",
                    Content: "đẩy mạnh truyền thông",
                    Important: false,
                    Done: false,
                    isShow: true,
                },
                {
                    key: "2",
                    Title: "Phát triển bản thân",
                    Content: "Thói quen tạo nên số phận",
                    Important: false,
                    Done: false,
                    isShow: true,
                },
                {
                    key: "3",
                    Title: "Kế hoạch tuần",
                    Content: "Cải thiện mobile",
                    Important: true,
                    Done: true,
                    isShow: true,
                },
                {
                    key: "4",
                    Title: "Quảng bá thương hiệu",
                    Content: "đẩy mạnh truyền thông",
                    Important: false,
                    Done: false,
                    isShow: true,
                },
                {
                    key: "5",
                    Title: "Phát triển bản thân",
                    Content: "Thói quen tạo nên số phận",
                    Important: false,
                    Done: false,
                    isShow: true,
                },
                {
                    key: "6",
                    Title: "Kế hoạch tuần",
                    Content: "Cải thiện mobile",
                    Important: true,
                    Done: true,
                    isShow: true,
                },
                {
                    key: "7",
                    Title: "Quảng bá thương hiệu",
                    Content: "đẩy mạnh truyền thông",
                    Important: false,
                    Done: false,
                    isShow: true,
                },
                {
                    key: "8",
                    Title: "Phát triển bản thân",
                    Content: "Thói quen tạo nên số phận",
                    Important: false,
                    Done: false,
                    isShow: true,
                },

            ]


        };
        this.DoneOn = Animated.timing(this.animated_DoneOn,
            {
                toValue: 1, // from value 0 to 100 
                duration: 500, // Thời gian thay đổi
            });
        this.DoneOff = Animated.timing(this.animated_DoneOn,
            {
                toValue: 0, // from value 0 to 100 
                duration: 500, // Thời gian thay đổi
            });
    }
    async componentDidMount() {
        this.SelectData();
    }
    SelectData() {
        var data = this.state.ListData;
        var viewTitleDone = true;
        var _dataView = [];
        try {
            for (let i = 0; i < data.length; i++) {
                if (data[i].Done !== true) {
                    data[i].isShow = true;
                    data[i].viewTitleDone = false;
                    _dataView.push(data[i]);
                }
            }
            for (let i = 0; i < data.length; i++) {
                if (data[i].Done === true) {
                    if (viewTitleDone === true) {
                        data[i].viewTitleDone = true;
                        viewTitleDone = false;
                    } else {
                        data[i].viewTitleDone = false;
                    }
                    data[i].isShow = this.state.isShowDone;
                    _dataView.push(data[i]);
                }
            }
            this.setState({ ListData: _dataView });
            if (this.state.isShowDone === true) {
                Animated.sequence([this.DoneOn]).start();
            } else {
                Animated.sequence([this.DoneOff]).start();
            }

        } catch (error) {
            alert("có lỗi khi chọn lọc dữ liệu")
        }
    }
    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1 }}
            >
                <TouchableWithoutFeedback
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[stylesToDo.container_Full]} >
                        <ImageBackground source={require('../../../images/TM/ImageToDo/ImageBackground/rungcay.jpg')} style={stylesToDo.FullSize}>
                            <HeaderToDo
                                leftTitle={this.state.TitleBack}
                                Customcolor={colorsToDo.white}
                                Title='Ngày của tôi'
                                hideTitle={true}
                                hideright={false}
                                CallbackBack={() => this.CallbackBack()}
                                CallbackThreeDots={() => this.CallbackThreeDots()}
                            />
                            <View style={[{ padding: 10, marginLeft: 10 }]}>
                                <Text style={{ fontSize: 30, color: colorsToDo.white, fontWeight: 'bold' }}>Ngày của tôi</Text>
                                <Text style={[stylesToDo.styleText, { color: colorsToDo.white }]}>{this.CustomDateTime(new Date())}</Text>
                            </View>
                            {this.state.ListData.length > 0 ? this.ViewListData(this.state.ListData) : null}
                            {this.ViewFooter()}
                        </ImageBackground>
                        <TabView_ToDo_Component
                            callback={this.TabViewChange}
                            nameMenu={'Tuỳ chọn danh sách'}
                            eOpen={this.openTabView} />
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

    //#region List 
    ViewListData = (rs) => {
        const rotateStart = this.animated_DoneOn.interpolate({
            inputRange: [0, 1],
            outputRange: ['270deg', '360deg']
        })
        return (
            <FlatList
                data={rs}
                style={{ flex: 1 }}
                renderItem={({ item }) => (
                    <View>
                        {item.viewTitleDone && item.viewTitleDone === true ?
                            <TouchableOpacity style={[stylesToDo.alignItems, styles.viewTitleDone]} onPress={() => { this.state.isShowDone = !this.state.isShowDone, this.SelectData() }}>
                                <Animated.View style={{transform: [{ rotate: rotateStart },{ perspective: 1000 }]}}>
                                    <Icon name={'chevron-down'} type={'feather'} size={20} iconStyle={{ color: colorsToDo.white }} />
                                </Animated.View>
                                <Text style={[stylesToDo.styleText, { color: colorsToDo.white }]}>Đã hoàn thành</Text>
                            </TouchableOpacity>
                            : null
                        }
                        {item.isShow === true ?
                            <View style={styles.styleListView}>
                                <TouchableOpacity style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center' }} onPress={() => this.statusDone(item)}>
                                    <Icon name={item.Done === true ? "checkcircle" : "circle"} type={item.Done === true ? "antdesign" : "feather"} color={colorsToDo.gray} size={22} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[stylesToDo.justifyContent, { flex: 8 }]} onPress={() => this.NextPage()}>
                                    <Text style={[stylesToDo.styleText]}>{item.Title}</Text>
                                    <Text style={[stylesToDo.styleContent]}>{item.Content}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[stylesToDo.contentCenter, { flex: 1 }]} onPress={() => this.editImportant(item)}>
                                    <Icon name={item.Important === true ? "star" : "star-o"} type={"font-awesome"} color={colorsToDo.gray} size={20} />
                                </TouchableOpacity>
                            </View>
                            : null
                        }
                    </View>

                )
                }
                keyExtractor={(item, index) => index.toString()}
                //onEndReached={() => this.loadMoreData()}
                refreshControl={
                    < RefreshControl
                        refreshing={false}
                        onRefresh={() => this._onRefresh}
                        tintColor="#f5821f"
                        titleColor="#fff"
                        colors={[colorsToDo.MyDayColor]}
                    />
                }

            />
        )
    }
    //#endregion

    //#region Footer
    ViewFooter = () => {
        return (
            <View style={[stylesToDo.alignItems,
            {
                flexDirection: 'row',
                padding: 10,
                margin: 10,
                backgroundColor: 'rgba(83, 112, 89,0.9)',
                borderRadius: 10
            }]}>
                <TouchableOpacity style={[stylesToDo.alignItems, { flexDirection: 'row', flex: 1 }]}>
                    <Icon name={'plus'} type={'feather'} color={colorsToDo.white} size={25} iconStyle={{ paddingLeft: 5, paddingRight: 10 }} />
                    <Text style={[stylesToDo.styleText, { color: colorsToDo.white }]} >Thêm tác vụ</Text>
                </TouchableOpacity>
            </View>
        )
    }
    //#endregion

    //#region chức năng

    //#region NextPage
    NextPage() {
        Actions.openToDo({ TitleHeader: "Ngày của tôi", ColorMain: colorsToDo.MyDayColor });
    }
    //#endregion

    //#region Back List
    CallbackBack = () => {
        Actions.pop();
    }
    //#endregion

    //#region Hiện thị tab tuỳ chỉnh
    _openTabView() { }
    openTabView = (d) => {
        this._openTabView = d;
    }
    CallbackThreeDots() {
        this._openTabView();
    }
    TabViewChange = (rs) => {
        alert(rs)
    }
    //#endregion

    //#region CustomDateTime
    CustomDateTime = (date) => {
        if (date !== null) {
            var _getDay = new Date(date).getDay();
            var returnGetDay = "";
            switch (_getDay) {
                case 1:
                    returnGetDay = 'Thứ 2'
                    break;
                case 2:
                    returnGetDay = 'Thứ 3'
                    break;
                case 3:
                    returnGetDay = 'Thứ 4'
                    break;
                case 4:
                    returnGetDay = 'Thứ 5'
                    break;
                case 5:
                    returnGetDay = 'Thứ 6'
                    break;
                case 6:
                    returnGetDay = 'Thứ 7'
                    break;
                default:
                    returnGetDay = 'Chủ nhật'
                    break;
            }
            var _getDate = new Date(date).getDate();
            var _getMonth = new Date(date).getMonth() + 1;
            return returnGetDay + ', ngày ' + _getDate + ' tháng ' + _getMonth;
        } else {
            return "";
        }
    }
    //#endregion

    //#region statusDone
    statusDone = (item) => {
        var data = this.state.ListData;
        try {
            for (let i = 0; i < data.length; i++) {
                if (data[i].key === item.key) {
                    data[i].Done = !data[i].Done;
                }
            }
            this.setState({ ListData: data });
            this.SelectData();
        } catch (error) {
            alert("Có lỗi khi thay đổi dữ liệu")
        }
    }
    //#endregion

    //#region EditImportant
    editImportant = (item) => {
        var data = this.state.ListData;
        try {
            for (let i = 0; i < data.length; i++) {
                if (data[i].key === item.key) {
                    data[i].Important = !data[i].Important;
                }
            }
            this.setState({ ListData: data });
        } catch (error) {
            alert("Có lỗi khi thay đổi dữ liệu")
        }
    }
    //#endregion
    //#endregion

}
//#region Css
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const navHeight = Platform.OS === 'ios' ? 80 : 80 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
const styles = StyleSheet.create({
    styleListView: {
        flexDirection: 'row',
        padding: 10,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor:
            colorsToDo.white,
        marginBottom: 2,
        borderRadius: 10
    },
    viewTitleDone: {
        flexDirection: 'row',
        padding: 6,
        margin: 10,
        backgroundColor: 'rgba(24, 32, 35,0.7)',
        borderRadius: 8,
        width: SCREEN_WIDTH / 2.5
    }
});
//#endregion
//Redux
const mapStateToProps = state => ({
})
//Using call para public all page
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(MyDay_ToDo_Component);
