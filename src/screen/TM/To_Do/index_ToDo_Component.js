import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import {
    Keyboard,
    Platform,
    Text,
    TouchableOpacity,
    View,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Image,
    RefreshControl,
    FlatList
} from 'react-native';
import { Header, Icon, Divider } from 'react-native-elements';
import FormSearch from '../../component/FormSearch';
import stylesToDo from './css/style_ToDo';
import colorsToDo from './css/colors_ToDo';
import { API, API_TODO } from '@network';
import LoadingComponent from '../../component/LoadingComponent';
import { listCombobox,  } from './listCombobox'
import { controller } from './controller'
class index_ToDo_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            DataCount: [],
            key: '',
            EventSearch: false,
            ListCategories: [
                {
                    key: 'Myday',
                    icon: 'light-down',
                    typeIcon: 'entypo',
                    colorIcon: '#8B8378',
                    Title: 'Ngày của tôi',
                    CountContent: 0,
                    count: 1
                },
                {
                    key: 'IsFollow',
                    icon: 'star-o',
                    typeIcon: 'font-awesome',
                    colorIcon: '#FFD700',
                    Title: 'Quan trọng',
                    CountContent: 0,
                    count: 2
                },
                {
                    key: 'Myday',
                    icon: 'calendar',
                    typeIcon: 'feather',
                    colorIcon: '#008B00',
                    Title: 'Đã lập kế hoạch',
                    CountContent: 0,
                    count: 3
                },
                {
                    key: 'Myday',
                    icon: 'user',
                    typeIcon: 'feather',
                    colorIcon: '#B23AEE',
                    Title: 'Đã giao cho bạn',
                    CountContent: 0,
                    count: 4
                },
                {
                    key: 'Myday',
                    icon: 'shield',
                    typeIcon: 'feather',
                    colorIcon: '#4876FF',
                    Title: 'Tác vụ',
                    CountContent: 0,
                    count: 5
                }
            ]
        };
    }
    componentDidMount() {
        this.updateSearch("");
    };
    componentWillReceiveProps = (nextProps) => {
        if (nextProps.moduleId === "Back") {
            this.setState({ ListCategories: [] });
            listCombobox.ListCategories = [
                {
                    key: 'Myday',
                    icon: 'light-down',
                    typeIcon: 'entypo',
                    colorIcon: '#8B8378',
                    Title: 'Ngày của tôi',
                    CountContent: 0,
                    count: 1
                },
                {
                    key: 'IsFollow',
                    icon: 'star-o',
                    typeIcon: 'font-awesome',
                    colorIcon: '#FFD700',
                    Title: 'Quan trọng',
                    CountContent: 0,
                    count: 2
                },
                {
                    key: 'Myday',
                    icon: 'calendar',
                    typeIcon: 'feather',
                    colorIcon: '#008B00',
                    Title: 'Đã lập kế hoạch',
                    CountContent: 0,
                    count: 3
                },
                {
                    key: 'Myday',
                    icon: 'user',
                    typeIcon: 'feather',
                    colorIcon: '#B23AEE',
                    Title: 'Đã giao cho bạn',
                    CountContent: 0,
                    count: 4
                },
                {
                    key: 'Myday',
                    icon: 'shield',
                    typeIcon: 'feather',
                    colorIcon: '#4876FF',
                    Title: 'Tác vụ',
                    CountContent: 0,
                    count: 5
                }
            ]
            this.updateSearch("");
        };
    }
    updateSearch = (t) => {
        this.GetAllCategory(true, t);
    }
    GetAllCategory(s, key) {
        this.setState({ loading: true });
        var ListView = [];
        if (s === true) {
            ListView = listCombobox.ListCategories;
        } else {
            ListView = this.state.ListCategories;
        }
        controller.ListCategoryOfTasks({ CategoryOfTaskGuid: null, Keyword: key }, ListView, rs => {
            this.setState({ loading: false, ListCategories: rs });
        });
    }
    render() {
        return (
            <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null} style={{ flex: 1 }}>
                <TouchableWithoutFeedback
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[stylesToDo.container_Full, { backgroundColor: '#fff' }]}>
                        {this.Header()}
                        {this.state.EventSearch === true ? <FormSearch CallbackSearch={(callback) => this.updateSearch(callback)} /> : null}
                        <View style={{ flex: 1 }}>
                            {this.ViewListCategory(this.state.ListCategories)}
                        </View>
                        {this.ViewFooter()}
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }
    //#region Header
    Header = () => {
        return (
            <Header
                containerStyle={[stylesToDo.squareContainer, stylesToDo.background]}
                placement="left"
                leftComponent={this.renderLeftComponent()}
                rightComponent={this.renderRightComponent()}
            />
        )
    }
    renderLeftComponent = () => {
        return (
            <View style={[{ flex: 1, flexDirection: 'row', alignItems: 'center' }]}>
                <TouchableOpacity style={[stylesToDo.contentCenter, { marginRight: 10 }]} onPress={() => Actions.pop({ BackModuleByCode: 'TM' })}>
                    <Icon name={'chevron-thin-left'} type='entypo' size={20} color={colorsToDo.black} />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{global.__appSIGNALR.SIGNALR_object.USER.FullName}</Text>
            </View>
        );
    }
    renderRightComponent = () => {
        return (
            <TouchableOpacity style={[stylesToDo.contentCenter, { marginRight: 10 }]} onPress={() => this.setState({ EventSearch: !this.state.EventSearch })}>
                <Icon name={'ios-search'} type={'ionicon'} color={colorsToDo.primary} size={25} />
            </TouchableOpacity>
        );
    }

    //#endregion
    //#region List
    ViewListCategory = (rs) => {
        return (
            this.state.loading !== true ?
                <FlatList
                    containerStyle={{ flex: 1 }}
                    data={rs}
                    renderItem={({ item, i }) => (
                        <View key={i} style={{ padding: 10, flexDirection: 'column' }}>
                            <TouchableOpacity style={{ flexDirection: 'row', paddingRight: 5, paddingLeft: 5 }} onPress={() => this.NextPage(item.key, item.Title)}>
                                <View style={{ flex: 1, alignItems: 'flex-start' }}>
                                    <Icon name={item.icon} type={item.typeIcon} color={item.colorIcon} size={22} />
                                </View>
                                <View style={[stylesToDo.justifyContent, { flex: 7 }]}>
                                    <Text style={[stylesToDo.styleText]}>{item.Title === "" ? "Dách sách chưa đặt tên" : item.Title}</Text>
                                </View>
                                <View style={[stylesToDo.justifyContent, { flex: 1, alignItems: 'flex-end' }]}>
                                    <Text style={[stylesToDo.styleText, { color: colorsToDo.gray }]}>{item.CountContent === 0 ? '' : item.CountContent}</Text>
                                </View>
                            </TouchableOpacity>
                            {item.count === 5 ? <View style={{ borderWidth: 0.5, borderColor: '#E8E8E8', marginTop: 20 }} /> : null}
                        </View>
                    )
                    }
                    keyExtractor={(item, index) => index.toString()}
                    //onEndReached={() => this.loadMoreData()}
                    refreshControl={
                        < RefreshControl
                            refreshing={false}
                            onRefresh={() => this._onRefresh}
                            tintColor="#f5821f"
                            titleColor="#fff"
                            colors={['red']}
                        />
                    }
                />
                :
                <LoadingComponent backgroundColor={'#fff'} />
        )
    }
    //#endregion
    //#region Footer
    ViewFooter = () => {
        return (
            <View style={[stylesToDo.alignItems, { flexDirection: 'row', padding: 10, bottom: 0, backgroundColor: '#fff' }]}>
                <TouchableOpacity style={{ flexDirection: 'row', flex: 5 }} onPress={() => this.AddCategory()}>
                    <Icon name={'plus'} type={'feather'} color={colorsToDo.primary} size={25} iconStyle={{ paddingLeft: 5, paddingRight: 10 }} />
                    <Text style={[stylesToDo.styleText, { color: colorsToDo.primary }]} >Danh sách mới</Text>
                </TouchableOpacity>
            </View>
        )
    }
    //#endregion

    //#region NextPage
    NextPage = (key, title) => {
        //dữ liệu để chuyển tab chi tiết
        var _CategoryOfTaskGuid = null;
        var _DateTime = null;
        var _IsFollow = null;
        switch (key) {
            case "Myday":
                _DateTime = new Date();
                break;
            case "IsFollow":
                _IsFollow = 1;
                break;
            default:
                _CategoryOfTaskGuid = key;
                break;
        }
        Actions.listDataToDo({
            Data: { CategoryOfTaskGuid: _CategoryOfTaskGuid, DateTime: _DateTime, IsFollow: _IsFollow, Title: title }
        });
    }
    AddCategory = () => {
        Actions.listDataToDo({
            Data: {
                CategoryOfTaskGuid: null,
                DateTime: null,
                IsFollow: null,
                Title: ""
            },
            AddCategory: true
        })
    }
    //#endregion
}
//Redux
const mapStateToProps = state => ({
})
//Using call para public all page
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(index_ToDo_Component);
