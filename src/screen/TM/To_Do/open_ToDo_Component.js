import configApp from '../../../configApp';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {getStatusBarHeight} from '@theme';
import {
  Keyboard,
  Platform,
  Text,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Alert,
  Dimensions,
  StyleSheet,
  Animated,
  Image,
  ScrollView,
  TextInput,
} from 'react-native';
import {Icon, Divider} from 'react-native-elements';
import RNFetchBlob from 'rn-fetch-blob';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-date-picker';
import {API, API_TODO} from '@network';
import {FuncCommon} from '@utils';
import {LoadingComponent} from '@Component';
import { listCombobox,  } from './listCombobox'
import { controller } from './controller'
import HeaderToDo from './Component_ToDo/HeaderToDo';
import stylesToDo from './css/style_ToDo';
import colorsToDo from './css/colors_ToDo';
import ComboboxRepeat_ToDo_Component from './Component_ToDo/ComboboxRepeat_ToDo_Component';
import ComboboxV2 from '../../component/ComboboxV2';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import MenuActionCompoment from '../../component/MenuActionCompoment';
class open_ToDo_Component extends Component {
  constructor(props) {
    super(props);
    this.animated = new Animated.Value(0);
    this.state = {
      isShowDone: true,
      TitleBack:
        this.props.TitleHeader !== undefined
          ? this.props.TitleHeader.length < 20
            ? this.props.TitleHeader
            : this.props.TitleHeader.substring(0, 20) + '...'
          : 'Quay lại',
      ColorMain:
        this.props.ColorMain !== undefined ? this.props.ColorMain : 'black',
      TaskGuid: this.props.TaskGuid,
      DataView: null,
      Description: null,
      OpenFormInput: false,
      TextCheckList: '',
      CheckList: [],
      handleInput: true,
      CheckListold: [],
      lstFiles: [],
      EndDate: new Date(),
      TimeReminder: new Date(),
      offsetDatePicker: false,
      offsetDateTimePicker: false,
      EditSubject: false,
      txtSubject: '',
    };
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    this.GetData();
  }
  //#region file
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = [];

      for (let index = 0; index < res.length; index++) {
        _liFiles.push(res[index]);
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].name = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      // this.setState(
      //     {
      //         lstFiles: _liFiles
      //     }
      // );
      this.InsertAtt(_liFiles);
    } catch (err) {
      console.log('Error from pick file');
    }
  }
  //#endregion
  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = [];
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            name:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        // this.setState(
        //     {
        //         lstFiles: _liFiles,
        //     }
        // );
        this.InsertAtt(_liFiles);
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion
  //#endregion

  //#region lấy thông tin chi tiết
  GetData() {
    this.setState({loading: true});
    controller.GetData(this.props.TaskGuid, rs => {
      this.setState({
        DataView: rs._data,
        Description: rs._data.Description,
        CheckList: rs._data.CheckList,
        CheckListold: rs._data.CheckList,
        lstFiles: rs.listatt,
        loading: false,
        txtSubject: rs._data.Subject,
      });
    });
    // var obj = {
    //     TaskGuid: this.props.TaskGuid
    // }

    // API_TODO.GetItemTasks(obj).then(res => {
    //     var _data = JSON.parse(res.data.data);
    //     if (_data !== null) {
    //         // for (let i = 0; i < _data.CheckList.TaskCheckLists.length; i++) {
    //         //     _data.CheckList.TaskCheckLists[i].Id = i + 1
    //         // }
    //         var _obj = {
    //             IdS: [this.props.TaskGuid]
    //         }
    //         if (FuncCommon.ConDate(_data.MyDay, 0) === FuncCommon.ConDate(new Date(), 0)) {
    //             _data.StatusMyDay = true;
    //         } else {
    //             _data.StatusMyDay = false;
    //         }
    //         API_TODO.GetAttList(_obj).then(rs => {
    //             var listatt = [];
    //             var _Att = JSON.parse(rs.data.data);
    //             if (_Att.length > 0) {
    //                 for (let i = 0; i < _Att.length; i++) {
    //                     listatt.push({
    //                         AttachmentGuid: _Att[i].AttachmentGuid,
    //                         name: _Att[i].Title.toLowerCase(),
    //                     });
    //                 }
    //             }

    //             this.setState({
    //                 DataView: _data,
    //                 Description: _data.Description,
    //                 CheckList: _data.CheckList.TaskCheckLists,
    //                 CheckListold: JSON.parse(JSON.stringify(_data.CheckList.TaskCheckLists)),
    //                 lstFiles: listatt,
    //                 loading: false,
    //                 txtSubject: _data.Subject
    //             });
    //         }).catch(error => {
    //             // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
    //             console.log('Error when call API Mobile.');
    //             this.setState({
    //                 loading: false
    //             });
    //         });
    //     } else {
    //         // alert("dữ liệu không có");
    //         this.setState({
    //             loading: false
    //         });
    //     }
    // }).catch(error => {
    //     // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
    //     console.log('Error when call API Mobile.');
    //     this.setState({
    //         loading: false
    //     });
    // });
  }
  //#endregion

  startanimated() {
    Animated.timing(this.animated, {
      toValue: 1, // from value 0 to 100
      duration: 1000, // Thời gian thay đổi
      //easing: Easing.linear
    }).start();
  }
  render() {
    const spin = this.animated.interpolate({
      inputRange: [0, 1],
      outputRange: ['270deg', '360deg'],
    });
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          {this.state.loading !== true ? (
            <View
              style={[
                stylesToDo.container_Full,
                {backgroundColor: '#fff', paddingBottom: 10},
              ]}>
              <HeaderToDo
                Customcolor={colorsToDo.blue}
                leftTitle={this.state.TitleBack}
                // Title='Ngày của tôi'
                hideTitle={true}
                hideright={true}
                CallbackBack={() => this.CallbackBack()}
              />
              {this.state.DataView !== null ? (
                <View style={[stylesToDo.container_Full, {padding: 10}]}>
                  <View style={{flexDirection: 'row', paddingBottom: 15}}>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                      onPress={() =>
                        this.state.DataView.StatusCompleted === 'C'
                          ? null
                          : this.UpdateProcessSuccess(this.state.DataView)
                      }>
                      <Icon
                        name={
                          this.state.DataView.StatusCompleted === 'C'
                            ? 'checkcircle'
                            : 'circle'
                        }
                        type={
                          this.state.DataView.StatusCompleted === 'C'
                            ? 'antdesign'
                            : 'feather'
                        }
                        color={this.state.ColorMain}
                        size={30}
                      />
                    </TouchableOpacity>
                    {this.state.EditSubject === false ? (
                      <TouchableOpacity
                        style={[stylesToDo.justifyContent, {flex: 6}]}
                        onPress={() => this.setState({EditSubject: true})}>
                        <Text style={[stylesToDo.styleTitle]}>
                          {this.state.txtSubject}
                        </Text>
                      </TouchableOpacity>
                    ) : (
                      <View style={[stylesToDo.justifyContent, {flex: 6}]}>
                        <TextInput
                          style={[stylesToDo.styleTitle]}
                          ref="x"
                          underlineColorAndroid="transparent"
                          autoCapitalize="none"
                          value={this.state.txtSubject}
                          onChangeText={text =>
                            this.setState({txtSubject: text})
                          }
                          autoFocus={true}
                          onEndEditing={() => {
                            this.APIEditSubject(),
                              this.setState({EditSubject: false});
                          }}
                        />
                      </View>
                    )}
                    <TouchableOpacity
                      style={[stylesToDo.contentCenter, {flex: 1}]}
                      onPress={() => this.UpdateFollow(this.state.DataView)}>
                      <Icon
                        name={
                          this.state.DataView.IsFollow === true
                            ? 'star'
                            : 'star-o'
                        }
                        type={'font-awesome'}
                        color={this.state.ColorMain}
                        size={20}
                      />
                    </TouchableOpacity>
                  </View>
                  <ScrollView>
                    {/* hạng mục công việc */}
                    {this.state.CheckList.length === 0
                      ? null
                      : this.state.CheckList.map((item, i) => (
                          <View style={{flexDirection: 'row'}}>
                            <TouchableOpacity
                              style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}
                              onPress={() => this.updateStatusCL(item, i)}>
                              <Icon
                                name={
                                  item.Status === true
                                    ? 'checkcircle'
                                    : 'circle'
                                }
                                type={
                                  item.Status === true ? 'antdesign' : 'feather'
                                }
                                color={colorsToDo.gray}
                                size={20}
                              />
                            </TouchableOpacity>
                            <TextInput
                              style={[
                                stylesToDo.styleText,
                                {padding: 10, flex: 6},
                              ]}
                              ref
                              underlineColorAndroid="transparent"
                              autoCapitalize="none"
                              value={item.CheckListTitle}
                              placeholder={'Nhập hạng mục công việc'}
                              onChangeText={text =>
                                this.setItemCheckList(text, item.Id)
                              }
                              onSubmitEditing={() => this.Enter()}
                              onEndEditing={() => this.cleanForm()}
                            />
                            <TouchableOpacity
                              style={[stylesToDo.contentCenter, {flex: 1}]}
                              onPress={() => this.ClearItemCheckList(item, i)}>
                              <Icon
                                name={'close'}
                                type={'antdesign'}
                                color={colorsToDo.gray}
                                size={12}
                              />
                            </TouchableOpacity>
                          </View>
                        ))}

                    {this.state.OpenFormInput === false ? (
                      <TouchableOpacity
                        style={{flexDirection: 'row'}}
                        onPress={() => this.OpenFormInput()}>
                        <View
                          style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icon
                            name={'plus'}
                            type={'feather'}
                            color={colorsToDo.blue}
                            size={25}
                          />
                        </View>
                        <View
                          style={[
                            stylesToDo.justifyContent,
                            {padding: 10, flex: 6},
                          ]}>
                          <Text
                            style={[
                              stylesToDo.styleText,
                              {color: colorsToDo.blue},
                            ]}>
                            {this.state.CheckList.length > 0
                              ? 'Bước tiếp theo'
                              : 'Thêm bước'}
                          </Text>
                        </View>
                        <TouchableOpacity style={{flex: 1}} />
                      </TouchableOpacity>
                    ) : (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icon
                            name={'circle'}
                            type={'feather'}
                            color={colorsToDo.gray}
                            size={20}
                          />
                        </TouchableOpacity>
                        <TextInput
                          style={[stylesToDo.styleText, {padding: 10, flex: 6}]}
                          ref="x"
                          underlineColorAndroid="transparent"
                          autoCapitalize="none"
                          value={this.state.TextCheckList}
                          placeholder={'Nhập hạng mục công việc'}
                          onChangeText={text => this.setCheckList(text)}
                          autoFocus={true}
                          onSubmitEditing={() => this.Enter()}
                          onEndEditing={() => this.cleanForm()}
                        />
                        <TouchableOpacity
                          style={[stylesToDo.contentCenter, {flex: 1}]}>
                          <Icon
                            name={'close'}
                            type={'antdesign'}
                            color={colorsToDo.gray}
                            size={12}
                          />
                        </TouchableOpacity>
                      </View>
                    )}
                    <Divider />

                    {/* thêm vào ngày của tôi */}
                    <TouchableOpacity
                      style={{
                        flexDirection: 'row',
                        paddingBottom: 15,
                        paddingTop: 15,
                      }}
                      onPress={() => this.addMyDay()}>
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Icon
                          name={'light-down'}
                          type={'entypo'}
                          color={
                            this.state.DataView.StatusMyDay === true
                              ? colorsToDo.blue
                              : colorsToDo.gray
                          }
                          size={25}
                        />
                      </View>
                      <View style={[stylesToDo.justifyContent, {flex: 7}]}>
                        <Text
                          style={[
                            stylesToDo.styleText,
                            {
                              color:
                                this.state.DataView.StatusMyDay === true
                                  ? colorsToDo.blue
                                  : colorsToDo.gray,
                            },
                          ]}>
                          {this.state.DataView.StatusMyDay === true
                            ? 'Đã thêm vào ngày của tôi'
                            : 'Thêm vào ngày của tôi'}
                        </Text>
                      </View>
                      {this.state.DataView.StatusMyDay === true ? (
                        <TouchableOpacity
                          style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                          onPress={() => this.addMyDay()}>
                          <Icon
                            name={'close'}
                            type={'antdesign'}
                            color={colorsToDo.gray}
                            size={15}
                          />
                        </TouchableOpacity>
                      ) : null}
                    </TouchableOpacity>
                    <Divider />

                    {/* Nhắc tôi */}
                    <TouchableOpacity
                      style={{
                        flexDirection: 'row',
                        paddingBottom: 15,
                        paddingTop: 15,
                      }}
                      onPress={() => this.onActionTimeReminder()}>
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Icon
                          name={'bell'}
                          type={'feather'}
                          color={
                            this.state.DataView.TimeReminder !== '' &&
                            this.state.DataView.TimeReminder !== null
                              ? FuncCommon.ConDate(
                                  this.state.DataView.TimeReminder,
                                  5,
                                ) >= FuncCommon.ConDate(new Date(), 5)
                                ? colorsToDo.blue
                                : colorsToDo.red
                              : colorsToDo.gray
                          }
                          size={20}
                        />
                      </View>
                      <View style={[stylesToDo.justifyContent, {flex: 7}]}>
                        {this.state.DataView.TimeReminder !== '' &&
                        this.state.DataView.TimeReminder !== null ? (
                          FuncCommon.ConDate(
                            this.state.DataView.TimeReminder,
                            5,
                          ) >= FuncCommon.ConDate(new Date(), 5) ? (
                            <Text
                              style={[
                                stylesToDo.styleText,
                                {color: colorsToDo.blue},
                              ]}>
                              {'Nhắc tôi: ' +
                                FuncCommon.ConDate(
                                  this.state.DataView.TimeReminder,
                                  1,
                                )}
                            </Text>
                          ) : (
                            <Text
                              style={[
                                stylesToDo.styleText,
                                {color: colorsToDo.red},
                              ]}>
                              {'Nhắc tôi: ' +
                                FuncCommon.ConDate(
                                  this.state.DataView.TimeReminder,
                                  1,
                                )}
                            </Text>
                          )
                        ) : (
                          <Text
                            style={[
                              stylesToDo.styleText,
                              {color: colorsToDo.gray},
                            ]}>
                            Nhắc tôi
                          </Text>
                        )}
                      </View>
                      {this.state.DataView.TimeReminder !== '' &&
                      this.state.DataView.TimeReminder !== null ? (
                        <TouchableOpacity
                          style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                          onPress={() => this.APITimeReminder(null)}>
                          <Icon
                            name={'close'}
                            type={'antdesign'}
                            color={colorsToDo.gray}
                            size={15}
                          />
                        </TouchableOpacity>
                      ) : null}
                    </TouchableOpacity>

                    {/* Đến hạn */}
                    <TouchableOpacity
                      style={{
                        flexDirection: 'row',
                        paddingBottom: 15,
                        paddingTop: 15,
                      }}
                      onPress={() => this.onAction()}>
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Icon
                          name={'calendar'}
                          type={'feather'}
                          color={
                            this.state.DataView.EndDate !== '' &&
                            this.state.DataView.EndDate !== null
                              ? FuncCommon.ConDate(
                                  this.state.DataView.EndDate,
                                  5,
                                ) >= FuncCommon.ConDate(new Date(), 5)
                                ? colorsToDo.blue
                                : colorsToDo.red
                              : colorsToDo.gray
                          }
                          size={20}
                        />
                      </View>
                      <View style={[stylesToDo.justifyContent, {flex: 7}]}>
                        {this.state.DataView.EndDate !== '' &&
                        this.state.DataView.EndDate !== null ? (
                          FuncCommon.ConDate(this.state.DataView.EndDate, 5) >=
                          FuncCommon.ConDate(new Date(), 5) ? (
                            <Text
                              style={[
                                stylesToDo.styleText,
                                {color: colorsToDo.blue},
                              ]}>
                              {'Đến hạn ' +
                                FuncCommon.ConDate(
                                  this.state.DataView.EndDate,
                                  0,
                                )}
                            </Text>
                          ) : (
                            <Text
                              style={[
                                stylesToDo.styleText,
                                {color: colorsToDo.red},
                              ]}>
                              {'Đến hạn ' +
                                FuncCommon.ConDate(
                                  this.state.DataView.EndDate,
                                  0,
                                )}
                            </Text>
                          )
                        ) : (
                          <Text
                            style={[
                              stylesToDo.styleText,
                              {color: colorsToDo.gray},
                            ]}>
                            Thêm ngày đến hạn
                          </Text>
                        )}
                      </View>
                      {this.state.DataView.EndDate !== '' &&
                      this.state.DataView.EndDate !== null ? (
                        <TouchableOpacity
                          style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                          onPress={() => this.APIEndDate(null)}>
                          <Icon
                            name={'close'}
                            type={'antdesign'}
                            color={colorsToDo.gray}
                            size={15}
                          />
                        </TouchableOpacity>
                      ) : null}
                    </TouchableOpacity>

                    {/* Lặp lại */}
                    <TouchableOpacity
                      style={{
                        flexDirection: 'row',
                        paddingBottom: 15,
                        paddingTop: 15,
                      }}
                      onPress={() => this.onActionRepeat()}>
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Icon
                          name={'repeat'}
                          type={'feather'}
                          color={
                            this.state.DataView.Recurrences !== '' &&
                            this.state.DataView.Recurrences !== null
                              ? colorsToDo.blue
                              : colorsToDo.gray
                          }
                          size={20}
                        />
                      </View>
                      <View style={[stylesToDo.justifyContent, {flex: 7}]}>
                        <Text
                          style={[
                            stylesToDo.styleText,
                            {
                              color:
                                this.state.DataView.Recurrences !== '' &&
                                this.state.DataView.Recurrences !== null
                                  ? colorsToDo.blue
                                  : colorsToDo.gray,
                            },
                          ]}>
                          {this.state.DataView.Recurrences !== '' &&
                          this.state.DataView.Recurrences !== null
                            ? this.CustomRepeat(this.state.DataView.Recurrences)
                            : 'Lặp lại'}
                        </Text>
                      </View>
                      {this.state.DataView.Recurrences !== '' &&
                      this.state.DataView.Recurrences !== null ? (
                        <TouchableOpacity
                          style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                          onPress={() => this.APIRepeat(null)}>
                          <Icon
                            name={'close'}
                            type={'antdesign'}
                            color={colorsToDo.gray}
                            size={15}
                          />
                        </TouchableOpacity>
                      ) : null}
                    </TouchableOpacity>
                    <Divider />

                    {/* tệp */}
                    {this.state.lstFiles.length > 0 ? (
                      <View style={{padding: 5}}>
                        {this.state.lstFiles.map((para, i) => (
                          <View
                            key={i}
                            style={{flex: 1, flexDirection: 'row', padding: 5}}>
                            <View
                              style={[
                                stylesToDo.contentCenter,
                                {marginRight: 10},
                              ]}>
                              <Image
                                style={{width: 30, height: 30, borderRadius: 5}}
                                source={{
                                  uri: controller.renderImage(para.name),
                                }}
                              />
                            </View>
                            <TouchableOpacity
                              style={{flex: 1}}
                              onPress={() => this.DownloadFile(para)}>
                              <Text
                                style={[
                                  stylesToDo.fontSize12,
                                  {color: '#0a87eb'},
                                ]}>
                                {para.name}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={{}}
                              onPress={() => this.deleteAtt(para)}>
                              <Icon
                                name={'close'}
                                type={'antdesign'}
                                color={colorsToDo.red}
                                size={15}
                              />
                            </TouchableOpacity>
                          </View>
                        ))}
                      </View>
                    ) : null}
                    <TouchableOpacity
                      style={{
                        flexDirection: 'row',
                        paddingBottom: 15,
                        paddingTop: 15,
                      }}
                      onPress={() => this.openAttachment()}>
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Icon
                          name={'attachment'}
                          type={'entypo'}
                          color={colorsToDo.gray}
                          size={20}
                        />
                      </View>
                      <View style={[stylesToDo.justifyContent, {flex: 7}]}>
                        <Text
                          style={[
                            stylesToDo.styleText,
                            {color: colorsToDo.gray},
                          ]}>
                          Thêm tệp
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <Divider />

                    {/* Ghi chú */}
                    <View>
                      <TextInput
                        style={[stylesToDo.styleText, {padding: 10}]}
                        ref
                        underlineColorAndroid="transparent"
                        autoCapitalize="none"
                        value={this.state.Description}
                        multiline={true}
                        placeholder={'Thêm ghi chú'}
                        onChangeText={text =>
                          this.setState({Description: text})
                        }
                        onEndEditing={() => this.UpdateDescription()}
                      />
                    </View>
                  </ScrollView>
                </View>
              ) : null}
              {this.state.DataView !== null ? (
                <View
                  style={[
                    stylesToDo.justifyContent,
                    {flexDirection: 'row', paddingBottom: 5, paddingTop: 15},
                  ]}>
                  <Text
                    style={[stylesToDo.styleContent, {color: colorsToDo.gray}]}>
                    Đã tạo{' '}
                    {FuncCommon.ConDate(this.state.DataView.CreatedDate, 0)}
                  </Text>
                </View>
              ) : null}
              <TouchableOpacity
                style={{position: 'absolute', bottom: 10, right: 25}}
                onPress={() => this.Delete()}>
                <Icon
                  name={'delete'}
                  type={'antdesign'}
                  color={colorsToDo.gray}
                  size={20}
                />
              </TouchableOpacity>
              <ComboboxV2
                callback={this.ChoiceAtt}
                data={listCombobox.ListComboboxAtt}
                eOpen={this.openCombobox_Att}
              />
              <MenuActionCompoment
                callback={this.actionMenuCallback}
                data={listCombobox.DataMenuBottom}
                eOpen={this.openMenu}
                position={'bottom'}
                nameMenu={'Đến hạn'}
              />
              <MenuActionCompoment
                callback={this.actionMenuCallbackRepeat}
                data={listCombobox.DataMenuBottomRepeat}
                eOpen={this.openMenuRepeat}
                position={'bottom'}
                nameMenu={'Lặp lại'}
              />
              <MenuActionCompoment
                callback={this.actionMenuCallbackTimeReminder}
                data={listCombobox.DataMenuBottomTimeReminder}
                eOpen={this.openMenuTimeReminder}
                position={'bottom'}
                nameMenu={'Nhắc tôi'}
              />
              <OpenPhotoLibrary
                callback={this.callbackLibarary}
                openLibrary={this.openLibrary}
              />
              {this.state.offsetDatePicker === true ? (
                <View
                  style={{
                    backgroundColor: '#fff',
                    flexDirection: 'column',
                    position: 'absolute',
                    bottom: 0,
                    zIndex: 100,
                    width: SCREEN_WIDTH,
                  }}>
                  <TouchableOpacity
                    style={{
                      borderWidth: 0.5,
                      borderColor: colorsToDo.gray,
                      padding: 10,
                    }}
                    onPress={() => {
                      this.setState({offsetDatePicker: false}),
                        this.APIEndDate(
                          FuncCommon.ConDate(this.state.EndDate, 4),
                        );
                    }}>
                    <Text style={{textAlign: 'right'}}>Xong</Text>
                  </TouchableOpacity>
                  <DatePicker
                    locale="vie"
                    locale="vie"
                    date={this.state.EndDate}
                    mode="date"
                    style={{width: SCREEN_WIDTH}}
                    onDateChange={setDate => this.setdate(setDate)}
                  />
                </View>
              ) : null}
              {this.state.offsetDateTimePicker === true ? (
                <View
                  style={{
                    backgroundColor: '#fff',
                    flexDirection: 'column',
                    position: 'absolute',
                    bottom: 0,
                    zIndex: 100,
                    width: SCREEN_WIDTH,
                  }}>
                  <TouchableOpacity
                    style={{
                      borderWidth: 0.5,
                      borderColor: colorsToDo.gray,
                      padding: 10,
                    }}
                    onPress={() => {
                      this.setState({offsetDateTimePicker: false}),
                        this.APITimeReminder(this.state.TimeReminder);
                    }}>
                    <Text style={{textAlign: 'right'}}>Xong</Text>
                  </TouchableOpacity>
                  <DatePicker
                    locale="vie"
                    locale="vie"
                    date={this.state.EndDate}
                    mode="datetime"
                    style={{width: SCREEN_WIDTH}}
                    onDateChange={setDate => this.setdatetime(setDate)}
                  />
                </View>
              ) : null}
            </View>
          ) : (
            <LoadingComponent backgroundColor={'#fff'} />
          )}
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  //#region Back List
  CallbackBack = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  };
  //#endregion

  //#region Hiện thị tab tuỳ chỉnh
  _openMenuTimeReminder() {}
  openMenuTimeReminder = d => {
    this._openMenuTimeReminder = d;
  };
  onActionTimeReminder() {
    this._openMenuTimeReminder();
  }
  actionMenuCallbackTimeReminder = d => {
    switch (d) {
      case '1':
        var date =
          new Date().getFullYear() +
          '-' +
          (new Date().getMonth() + 1) +
          '-' +
          new Date().getDate() +
          ' 18:00:00';
        this.APITimeReminder(date);
        break;
      case '2':
        // var date = new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate()+" 9:00:00";
        var date =
          new Date().getFullYear() +
          '-' +
          (new Date().getMonth() + 1) +
          '-' +
          (new Date().getDate() + 1) +
          ' 9:00:00';
        this.APITimeReminder(date);
        break;
      case '3':
        var curr = new Date(); // get current date
        var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        var last = first + 8; // last day is the first day + 6
        var lastday = new Date(curr.setDate(last)).toUTCString();
        var cusday =
          FuncCommon.ConDate(lastday, 1).split(' ')[0] + ' ' + '9:00:00';
        this.APITimeReminder(cusday);
        break;
      case '4':
        this.setState({offsetDateTimePicker: true});
        break;
      default:
        break;
    }
  };
  APITimeReminder = s => {
    var obj = {
      IdS: [this.props.TaskGuid],
      TimeReminder: s === null ? null : FuncCommon.ConDate(s, 2),
    };
    API_TODO.UpdateTimeReminder(obj)
      .then(rs => {
        this.state.DataView.TimeReminder = s === null ? '' : s;
        this.setState({DataView: this.state.DataView});
        // this.Notification();
      })
      .catch(error => {
        console.log('Xin lỗi. Đã xảy ra.');
      });
  };
  Notification = () => {
    var userName = [];
    for (let i = 0; i < this.state.DataView.TaskOfUsers.length; i++) {
      userName.push(this.state.DataView.TaskOfUsers[i].LoginName);
    }
    var _item = {
      RecordGuid: this.state.DataView.TaskGuid, //guid
      ModuleId: 'ES_TM_TODO', //thông báo trên web
      Data: [global.__appSIGNALR.SIGNALR_object.USER.LoginName], //danh sách người nhận
      Head: 'TODO',
      Content: 'Nhắc nhở: ' + this.state.DataView.Subject,
    };
    API.SendNotiCommon(_item)
      .then(rs => {
        console.log(rs);
      })
      .catch(error => {
        console.log('Lỗi lỗi====>', error);
      });
  };

  setdatetime = s => {
    this.state.DataView.TimeReminder = s;
    this.setState({DataView: this.state.DataView, TimeReminder: s});
  };

  //#endregion

  //#region UpdateFollow
  UpdateFollow = item => {
    var obj = {IdS: [item.TaskGuid.toString()]};
    API_TODO.UpdateFollow(obj)
      .then(rs => {
        if (rs.data.errorCode == 200) {
          this.state.DataView.IsFollow = !this.state.DataView.IsFollow;
        }
        this.setState({
          DataView: this.state.DataView,
        });
      })
      .catch(error => {
        alert('Có lỗi khi cập nhật theo dõi');
        console.log('Có lỗi khi cập nhật theo dõi', error);
      });
  };
  //#endregion

  //#region UpdateProcessSuccess
  UpdateProcessSuccess = item => {
    let obj = {IdS: [item.TaskGuid.toString()]};
    API_TODO.UpdateProcessSuccess(obj)
      .then(rs => {
        if (this.state.DataView.StatusCompleted !== 'C') {
          this.state.DataView.StatusCompleted = 'C';
        } else if (this.state.DataView.StatusCompleted === 'C') {
          this.state.DataView.StatusCompleted = 'P';
        }
        this.setState({
          DataView: this.state.DataView,
        });
      })
      .catch(error => {
        alert('Có lỗi khi cập nhật trạng thái hoàn thành công việc');
        console.log('Error when call API Mobile.', error);
      });
  };
  //#endregion

  //#region UpdateDescription
  UpdateDescription = () => {
    let obj = {
      Description: this.state.Description,
      IdS: [this.state.DataView.TaskGuid.toString()],
    };
    API_TODO.UpdateDescription(obj)
      .then(rs => {})
      .catch(error => {
        alert('Có lỗi khi cập nhật trạng thái hoàn thành công việc');
        console.log('Error when call API Mobile.', error);
      });
  };
  //#endregion

  //#region Delete
  Delete = () => {
    Alert.alert(
      'Thông báo',
      'Bạn chắc chắn muốn xóa ' + this.state.DataView.Subject + ' không?',
      [
        {
          text: 'Xác nhận',
          onPress: () => this.DeleteTask(this.state.DataView),
        },
        {text: 'Huỷ', onPress: () => console.log('No Pressed')},
      ],
      {cancelable: true},
    );
  };
  DeleteTask = item => {
    let obj = {
      IdS: [item.TaskGuid],
    };
    API_TODO.Delete(obj)
      .then(rs => {
        if (rs.data.Error) {
          Alert.alert(
            'Thông báo',
            rs.data.Title,
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
        } else {
          Actions.pop();
          Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
        }
      })
      .catch(error => {
        console.log('Error when call API Mobile.');
      });
  };
  //#endregion

  //#region OpenFormInput
  OpenFormInput = () => {
    this.setState({
      OpenFormInput: !this.state.OpenFormInput,
    });
  };
  //#endregion

  //#region setItemCheckList
  setItemCheckList = (val, id) => {
    this.state.CheckList[id - 1].CheckListTitle = val;
    this.setState({CheckList: this.state.CheckList});
  };
  //#endregion

  //#region setCheckList
  setCheckList = text => {
    this.setState({TextCheckList: text});
  };
  focusTextInput(node) {
    this.refs[node].focus();
  }
  Enter = () => {
    this.state.handleInput = false;
    this.state.CheckList.push({
      Id: this.state.CheckList.length + 1,
      CheckListTitle: this.state.TextCheckList,
      CheckListGuid: this.newGuid(),
      Status: false,
    });
    this.setState({CheckList: this.state.CheckList, TextCheckList: ''});
    setTimeout(() => {
      this.focusTextInput('x');
    }, 1);
  };
  cleanForm = () => {
    if (!this.state.handleInput) {
      this.state.handleInput = true;
      return;
    }
    this.state.CheckList.push({
      Id: this.state.CheckList.length + 1,
      CheckListTitle: this.state.TextCheckList,
      CheckListGuid: this.newGuid(),
      Status: false,
    });
    this.APIChecklist();
    this.OpenFormInput();
  };
  //#endregion
  newGuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c == 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  };
  //#region  API Checklist
  APIChecklist = () => {
    var listCL = [];
    for (let i = 0; i < this.state.CheckList.length; i++) {
      listCL.push({
        CheckListGuid: this.state.CheckList[i].CheckListGuid,
        CheckListTitle: this.state.CheckList[i].CheckListTitle,
        Status: false,
      });
    }
    var obj = {TaskGuid: this.props.TaskGuid, listChecklist: listCL};
    API_TODO.InsertCheckList(obj)
      .then(rs => {
        if (rs.data.Error === true) {
          alert('Có lỗi khi cập nhật hạng mục công việc.');
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  };
  updateStatusCL = (item, index) => {
    var obj = {IdS: [item.CheckListGuid]};
    API_TODO.UpdateCheckList(obj)
      .then(rs => {
        if (rs.data.Error === true) {
          alert('Có lỗi khi cập nhật hạng mục công việc.');
        } else {
          this.state.CheckList[index].Status = !this.state.CheckList[index]
            .Status;
          this.setState({CheckList: this.state.CheckList});
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  };
  ClearItemCheckList = (item, index) => {
    var obj = {IdS: [item.CheckListGuid]};
    API_TODO.DeleteCheckList(obj)
      .then(rs => {
        if (rs.data.Error === true) {
          alert('Có lỗi khi cập nhật hạng mục công việc.');
        } else {
          var list = [];
          for (let i = 0; i < this.state.CheckList.length; i++) {
            if (this.state.CheckList[i].CheckListGuid !== item.CheckListGuid) {
              list.push(this.state.CheckList[i]);
            }
          }
          this.setState({CheckList: list});
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  };
  //#endregion
  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };

  //#endregion

  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    this.InsertAtt(rs);
  };
  //#endregion

  //#region deleteAtt
  deleteAtt = item => {
    var obj = [item.AttachmentGuid];
    var _data = new FormData();
    _data.append('DeleteAttach', JSON.stringify(obj));
    API_TODO.DeleteAtt(_data)
      .then(res => {
        var data = [];
        for (let i = 0; i < this.state.lstFiles.length; i++) {
          if (this.state.lstFiles[i].name !== item.name) {
            data.push(this.state.lstFiles[i]);
          }
        }
        this.setState({lstFiles: data});
      })
      .catch(error => {
        // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
        console.log('Xảy ra lỗi khi thêm mới đính kèm');
      });
  };
  //#endregion

  //#region InsertAtt
  InsertAtt = val => {
    var obj = {
      IdS: [this.state.TaskGuid],
    };
    var _data = new FormData();
    _data.append('insert', JSON.stringify(obj));
    for (var i = 0; i < val.length; i++) {
      if (val.AttachmentGuid === undefined) {
        _data.append(val[i].name, val[i]);
      }
    }
    var _listAtt = this.state.lstFiles;
    API_TODO.AttInsert(_data)
      .then(res => {
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
          _listAtt.push({
            AttachmentGuid: data[i].AttachmentGuid,
            name: data[i].Title.toLowerCase(),
          });
        }
        this.setState({lstFiles: _listAtt});
      })
      .catch(error => {
        // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
        console.log('Xảy ra lỗi khi thêm mới đính kèm');
      });
  };
  //#endregion

  //#region DownloadFile
  DownloadFile(attachObject) {
    Alert.alert(
      //title
      'Thông báo',
      //body
      'Bạn muốn tải file đính kèm về máy',
      [
        {text: 'Tải về', onPress: () => this.DownloadAtt(attachObject)},
        {text: 'Huỷ', onPress: () => console.log('No Pressed')},
      ],
      {cancelable: true},
    );
  }
  //tair file đính kèm về
  DownloadAtt(attachObject) {
    let dirs = RNFetchBlob.fs.dirs;
    if (Platform.OS !== 'ios') {
      RNFetchBlob.config({
        addAndroidDownloads: {
          useDownloadManager: true, // <-- this is the only thing required
          // Optional, override notification setting (default to true)
          notification: true,
          // Optional, but recommended since android DownloadManager will fail when
          // the url does not contains a file extension, by default the mime type will be text/plain
          mime: '/',
          description: 'File downloaded by download manager.',
          //title: new Date().toLocaleString() + ' - test.xlsx',
          //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
          path: 'file://' + dirs.DownloadDir + '/' + attachObject.name, //using for android
        },
      })
        .fetch(
          'GET',
          configApp.url_Task_Downloadfile + attachObject.AttachmentGuid,
          {},
        )
        .then(resp => {
          Alert.alert(
            //title
            'Thông báo',
            //body
            'Tải thành công',
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
          // the path of downloaded file
          resp.path();
        });
    } else {
      RNFetchBlob.config({
        path: dirs.DocumentDir + '/' + attachObject.name,
        addAndroidDownloads: {
          useDownloadManager: true, // <-- this is the only thing required
          // Optional, override notification setting (default to true)
          notification: true,
          IOSDownloadTask: true,
          // Optional, but recommended since android DownloadManager will fail when
          // the url does not contains a file extension, by default the mime type will be text/plain
          mime: '/',
          description: 'File downloaded by download manager.',
          title: new Date().toLocaleString() + '-' + attachObject.name,
          path:
            dirs.DocumentDir +
            '/' +
            new Date().toLocaleString() +
            '-' +
            attachObject.name, //using for ios
          //using for android
        },
      })
        .fetch(
          'GET',
          configApp.url_Task_Downloadfile + attachObject.AttachmentGuid,
          {},
        )
        .then(resp => {
          console.log('88888888888:', resp.data);
          RNFetchBlob.ios.openDocument(resp.data);
          // the path of downloaded file
          // resp.path()
          // CameraRoll.saveToCameraRoll(dirs.DocumentDir + '/' + attachObject.Title)
          //     .then(
          //         Alert.alert(
          //             //title
          //             'Thông báo',
          //             //body
          //             'Tải thành công',
          //             [
          //                 { text: 'Đóng', onPress: () => console.log('No Pressed') },
          //             ],
          //             { cancelable: true }
          //         )
          //         // alert('Tải đính kèm thành công', 'Photo added to camera roll!')
          //     )
          //     .catch(err => console.log('err:', err))
          // alert('Image Downloaded Successfully.');
        });
    }
  }
  //#endregion

  //#region đến hạn
  _openMenu() {}
  openMenu = d => {
    this._openMenu = d;
  };
  onAction() {
    this._openMenu();
  }
  actionMenuCallback = d => {
    switch (d) {
      case '1':
        this.APIEndDate(FuncCommon.ConDate(new Date(), 4));
        break;
      case '2':
        var date = new Date();
        date.setDate(date.getDate() + 1);
        this.APIEndDate(FuncCommon.ConDate(date, 4));
        break;
      case '3':
        var curr = new Date(); // get current date
        var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        var last = first + 8; // last day is the first day + 6
        var lastday = new Date(curr.setDate(last)).toUTCString();
        this.APIEndDate(FuncCommon.ConDate(lastday, 4));
        break;
      case '4':
        this.setState({offsetDatePicker: true});
        break;
      default:
        break;
    }
  };
  APIEndDate = s => {
    var obj = {
      IdS: [this.props.TaskGuid],
      EndDate: s === null ? null : FuncCommon.ConDate(s, 2),
    };
    API_TODO.UpdateDate(obj)
      .then(rs => {
        this.state.DataView.EndDate = s === null ? '' : s;
        this.setState({DataView: this.state.DataView});
      })
      .catch(error => {
        // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
        console.log('Xin lỗi. Đã xảy ra.');
      });
  };
  setdate = s => {
    this.state.DataView.EndDate = s;
    this.setState({DataView: this.state.DataView, EndDate: s});
  };
  //#endregion

  //#region Lặp lại
  _openMenuRepeat() {}
  openMenuRepeat = d => {
    this._openMenuRepeat = d;
  };
  onActionRepeat() {
    this._openMenuRepeat();
  }
  actionMenuCallbackRepeat = d => {
    switch (d) {
      case '1':
        //hàng ngày
        var Recurrence = {
          Type: 'D',
          TableConfigs: [
            {
              Option: 'DAY',
              Day: 1,
            },
          ],
          StartDate: new Date(),
          EndDate: new Date(),
          DayRegenerate: 1,
          OptionMore: 'A',
          Postion: new Date(),
        };
        this.APIRepeat(Recurrence);
        break;
      case '2':
        // Hàng tuần
        var Recurrence = {
          Type: 'W',
          TableConfigs: [
            {
              Option: 'WED',
              Week: 1,
              WeekChoice: (new Date().getDay() + 1).toString(),
            },
          ],
          StartDate: new Date(),
          EndDate: new Date(),
          DayRegenerate: 1,
          OptionMore: 'A',
          Postion: new Date(),
        };
        this.APIRepeat(Recurrence);
        break;
      case '3':
        //hàng tháng
        var Recurrence = {
          Type: 'M',
          TableConfigs: [
            {
              Option: 'MDAY',
              Day: 1,
              Month: 1,
            },
          ],
          StartDate: new Date(),
          EndDate: new Date(),
          DayRegenerate: 1,
          OptionMore: 'A',
          Postion: new Date(),
        };
        this.APIRepeat(Recurrence);
        break;
      case '4':
        var Recurrence = {
          Type: 'Y',
          TableConfigs: [{}],
          StartDate: new Date(),
          EndDate: new Date(),
          DayRegenerate: 1,
          OptionMore: 'A',
          Postion: new Date(),
        };
        this.APIRepeat(Recurrence);
        break;
      default:
        break;
    }
  };
  APIRepeat = s => {
    if (s !== null) {
      this.state.DataView.Recurrences = s.Type;
    } else {
      this.state.DataView.Recurrences = null;
    }
    this.setState({DataView: this.state.DataView});
    var obj = {
      TaskGuid: this.props.TaskGuid,
    };
    var _data = new FormData();
    _data.append('update', JSON.stringify(obj));
    _data.append('Recurrence', JSON.stringify(s));
    API_TODO.UpdateRecurrence(_data)
      .then(res => {})
      .catch(error => {
        // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
        console.log('Xảy ra lỗi khi thêm mới đính kèm');
      });
  };
  //#region CustomRepeat
  CustomRepeat = val => {
    if (val !== null) {
      if (val === 'D') {
        return 'Hàng ngày';
      } else if (val === 'W') {
        return 'Hàng tuần';
      } else if (val === 'M') {
        return 'Hàng tháng';
      } else if (val === 'Y') {
        return 'Hàng năm';
      } else {
        return '';
      }
    }
  };
  //#endregion

  //#endregion

  //#region add My Day
  addMyDay = () => {
    var s = null;
    if (this.state.DataView.StatusMyDay !== true) {
      s = new Date();
    }
    var obj = {
      IdS: [this.props.TaskGuid],
      ActualStartDate: s === null ? null : FuncCommon.ConDate(s, 2),
    };
    API_TODO.UpdateMyDate(obj)
      .then(rs => {
        if (rs.data.errorCode === 200) {
          this.state.DataView.StatusMyDay = s === null ? false : true;
          this.setState({DataView: this.state.DataView});
        }
      })
      .catch(error => {
        // alert("Xin lỗi. Đã xảy ra lỗi khi lấy dữ liệu")
        console.log('Xin lỗi. Đã xảy ra.');
      });
  };
  //#endregion

  //#region sửa subject
  APIEditSubject = () => {
    let obj = {
      Subject: this.state.txtSubject,
      IdS: [this.state.DataView.TaskGuid.toString()],
    };
    API_TODO.UpdateSubject(obj)
      .then(rs => {})
      .catch(error => {
        alert('Có lỗi khi cập nhật tên công việc công việc');
        console.log('Error when call API Mobile.', error);
      });
  };
  //#endregion
}
//#region Css
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const navHeight = Platform.OS === 'ios' ? 80 : 80 - getStatusBarHeight();
//#endregion

//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(open_ToDo_Component);
