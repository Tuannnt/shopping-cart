import React, { Component } from 'react';
import {
  Button,
  FlatList,
  Keyboard,
  RefreshControl,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView,
  ImageBackground,
  PermissionsAndroid,
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Header } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { AppStyles, AppColors } from '@theme';
import Geolocation from '@react-native-community/geolocation';
import styles from '../TasksStyle';
import { isBuffer, reduce } from 'lodash';
import TabBar from '../../component/TabBar';
import { FuncCommon } from '../../../utils';
import { API_TM_TASKS } from '../../../network';
import Modal, {
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
  SlideAnimation,
  ScaleAnimation
} from 'react-native-modals';
import LoadingComponent from '../../component/LoadingComponent';
// import MapView, { Marker } from 'react-native-maps';

class CheckInComponent extends Component {
  constructor(props) {
    super();
    this.state = {
      coordinate: null,
      initialRegion: null,
      showChechin: false,
      dataCheckin: {},
    };
    this.serverLoca = null;
    this.localLoca = null;
    this.AboutPostion = 0;
  }
  async componentDidMount() {
    this._setCheckin();
    this.time = setInterval(() => {
      this.requestLocationPermission()
      if (this.state.showChechin && !this.state.showModal) {
        this.setState({
          showModal: true,
        }, () => {
          clearInterval(this.time);
        });
      }
    }, 3000);
  }
  componentWillUnmount = () => {
    this.setState({ showModal: false })
    clearInterval(this.time);
  };
  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Quyền vị trí',
          message: 'Bạn cần cấp quyền cho vị trí',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Geolocation.getCurrentPosition(d => this._currenLocation(d));
      } else {
        Alert.alert(
          'Thông báo',
          'Không có quyền truy cập vị trí.',
          [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
          { cancelable: false },
        );
      }
    } catch (err) {
      console.log(err);
    }
  }
  _setCheckin() {
    API_TM_TASKS.GetLocationCheckin({})
      .then(rs => {
        if (rs.data.Status) {
          if (
            rs.data.Data !== undefined &&
            rs.data.Data != null &&
            rs.data.Data !== '' &&
            rs.data.Data !== '[]'
          ) {
            var _d = JSON.parse(JSON.parse(rs.data.Data))[0];
            this.serverLoca = _d;
            this.AboutPostion = _d.AboutPostion;
            this.setState(
              {
                dataCheckin: {
                  dataCheckin_location: {
                    latitude: _d.latitude,
                    longitude: _d.longitude,
                  },
                  dataCheckin_title: _d.Title,
                  dataCheckin_description: _d.Description,
                },
              },
              () => {
                this.requestLocationPermission();
              },
            );
          }
        } else {
          Alert.alert(
            'Thông báo',
            'Không lấy được vị trí chấm công.',
            [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
            { cancelable: false },
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  }
  distance = (lat1, lon1, lat2, lon2) => {
    var p = 0.017453292519943295;    // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 +
      c(lat1 * p) * c(lat2 * p) *
      (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  }
  __showButtonCheckin() {
    if (this.localLoca && this.serverLoca) {
      let distance = (this.distance(this.serverLoca.latitude, this.serverLoca.longitude, this.localLoca.latitude, this.localLoca.longitude) * 1000)
      if (distance <= Number(this.AboutPostion)) {
        this.setState({
          showChechin: true,
        });
      }
    }
  }
  _currenLocation(d) {
    this.localLoca = d.coords;
    this.setState(
      {
        initialRegion: {
          latitude: d.coords.latitude,
          longitude: d.coords.longitude,
          longitudeDelta: 0.1,
          latitudeDelta: 0.1,
        },
      },
      () => {
        this.__showButtonCheckin();
      },
    );
  }
  _actionCheckin() {
    var _o = {
      DateTimeRecord: FuncCommon.ConDate(new Date(), 5),
      DateOnlyRecord: FuncCommon.ConDate(new Date(), 0),
      TimeOnlyRecord: FuncCommon.ConDate(new Date(), 6),
      CheckTime: new Date().getHours() >= 12 ? 'PM' : 'AM',
      Latitude: this.state.initialRegion.latitude,
      Longitude: this.state.initialRegion.longitude,
      DeviceId: this.serverLoca.DeviceID
    };
    API_TM_TASKS.InsertTimeKeepingsLog(_o)
      .then(rs => {
        this.setState({ loading: false })
        var _err = '';
        if (rs.data.Status) {
          clearInterval(this.time);
          _err = 'Chấm công thành công.';
          Alert.alert(
            'Thông báo',
            _err,
            [{ text: 'OK', onPress: () => Actions.TimeKeepingByMe() }],
            { cancelable: false },
          );
        } else {
          _err = 'Chấm công không thành công.';
          Alert.alert(
            'Thông báo',
            _err,
            [{ text: 'OK', onPress: () => Keyboard.dismiss() }],
            { cancelable: false },
          );
        }
      })
      .catch(error => {
        this.setState({ loading: false })
        console.log('Error when call API Mobile.');
      });
  }
  onClickBack() {
    clearInterval(this.time);
    Actions.pop();
  }
  close = () => {
    this.setState({ showModal: false, dataItem: {} });
  };
  onRegionChange(d1) { }
  //#region View Tổng phân biệt các View
  handleCheckin = () => {
    this.setState({ showModal: false, }, () => {
      this._actionCheckin()
    });
  };
  render() {
    return (
      this.state.loading ? (
        <LoadingComponent />
      ) :
        <View style={[styles.container, { backgroundColor: 'white' }]}>
          <TabBar title={'Chấm công'} BackModuleByCode={'MyProfile'} />
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              height: 400,
              backgroundColor: 'rgb(104 173 79 / 47%)',
            }}>
           
          </View>
          <Modal
            width={0.8}
            visible={this.state.showModal}
            onTouchOutside={this.close}
            onHardwareBackPress={this.close}
            modalTitle={<ModalTitle title={'Thông báo'} align="left" />}
            modalAnimation={
              new ScaleAnimation({
                initialValue: 0.1, // optional
                useNativeDriver: true, // optional
              })
            }
            footer={
              <ModalFooter style={{ flexDirection: 'row' }}>
                <ModalButton
                  text="Gửi"
                  style={{ flex: 1 }}
                  onPress={() => {
                    this.handleCheckin();
                  }}
                  textStyle={{ fontSize: 16, color: AppColors.ColorButtonSubmit }}
                />
                <ModalButton
                  text="Đóng"
                  style={{ flex: 1 }}
                  onPress={() => {
                    this.close();
                  }}
                  textStyle={{ fontSize: 16, color: 'red' }}
                />
              </ModalFooter>
            }>
            <ModalContent>
              <View style={{ padding: 10 }}>
                <Text
                  style={
                    (AppStyles.Labeldefault, { alignItems: 'center', fontSize: 16 })
                  }>
                  Mời bạn xác nhận chấm công
              </Text>
              </View>
            </ModalContent>
          </Modal>
        </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(CheckInComponent);
