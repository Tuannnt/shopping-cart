import {API_TM_TASKS, API_TM_CALENDAR, API} from '@network';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '@utils';
import Toast from 'react-native-simple-toast';
export default {
  Calendar_GetItem(value, callback) {
    var obj = {
      Id: value,
    };
    API_TM_CALENDAR.Calendar_GetItem(obj)
      .then(res => {
        var _data = JSON.parse(res.data.data);
        callback(_data);
      })
      .catch(error => {
        console.log(error);
        Actions.pop();
      });
  },
  GetAttachments(value, callback) {
    var obj = {
      RecordGuid: value,
    };
    API_TM_CALENDAR.GetAttachments(obj)
      .then(res => {
        var _data = JSON.parse(res.data.data);
        callback(_data);
      })
      .catch(error => {
        console.log(error);
        Actions.pop();
      });
  },
  Calendar_Insert(DataSave, Attachment, callback) {
    var _insert = {
      IsLocked: DataSave.IsLocked,
      StartTime: FuncCommon.ConDate(DataSave.StartTime, 11),
      EndTime: FuncCommon.ConDate(DataSave.EndTime, 11),
      TimeNotify: DataSave.CheckNotify
        ? FuncCommon.ConDate(DataSave.TimeNotify, 11)
        : null,
      CalenderType: DataSave.CalenderType,
      CalenderRange: DataSave.CalenderRange,
      IsActive: DataSave.IsActive,
      CalenderContent: DataSave.CalenderContent,
      CheckNotify: DataSave.CheckNotify,
      Title: DataSave.Title,
      IsDeleted: DataSave.IsDeleted,
    };
    var _lstTitlefile = [];
    for (let x = 0; x < Attachment.length; x++) {
      _lstTitlefile.push({
        STT: x,
        Title: Attachment[x].FileName,
      });
    }
    let _data = new FormData();
    _data.append('insert', JSON.stringify(_insert));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    _data.append('DeleteAttach', JSON.stringify([]));
    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_TM_CALENDAR.Calendar_Insert(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          console.log(_rs);
        } else {
          callback(_rs);
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
  Calendar_Update(DataSave, Attachment, DeleteAttachment, callback) {
    DataSave.StartTime = FuncCommon.ConDate(DataSave.StartTime, 2);
    DataSave.EndTime = FuncCommon.ConDate(DataSave.EndTime, 2);
    DataSave.TimeNotify = DataSave.CheckNotify
      ? FuncCommon.ConDate(DataSave.TimeNotify, 2)
      : null;
    var _lstTitlefile = [];
    var _stt = 0;
    for (let x = 0; x < Attachment.length; x++) {
      if (
        Attachment[x].AttachmentGuid === null ||
        Attachment[x].AttachmentGuid === undefined
      ) {
        _lstTitlefile.push({
          STT: _stt,
          Title: Attachment[x].FileName,
        });
        _stt = _stt + 1;
      }
    }
    let _data = new FormData();
    _data.append('update', JSON.stringify(DataSave));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    _data.append('DeleteAttach', JSON.stringify(DeleteAttachment));
    for (var i = 0; i < Attachment.length; i++) {
      if (
        Attachment[i].AttachmentGuid === null ||
        Attachment[i].AttachmentGuid === undefined
      ) {
        _data.append(Attachment[i].FileName, {
          name: Attachment[i].FileName,
          size: Attachment[i].size,
          type: Attachment[i].type,
          uri: Attachment[i].uri,
        });
      }
    }
    API_TM_CALENDAR.Calendar_Update(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          console.log(_rs);
        } else {
          callback(_rs);
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
};
