import React, { Component } from 'react';
import {
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View,
    TouchableOpacity,
    PermissionsAndroid,
    KeyboardAvoidingView,
    TextInput,
    Alert,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { Actions, Drawer } from 'react-native-router-flux';
import { Divider, Icon, Input, CheckBox } from 'react-native-elements';
import { API_TM_TASKS, API_TM_CALENDAR } from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import { AppStyles, AppColors, AppSizes } from '@theme';
import _ from 'lodash';
import styles from '../TasksStyle';
import { ScrollView } from 'react-native-gesture-handler';
import { FuncCommon } from '../../../utils';
import DatePicker from 'react-native-date-picker';
import ComboboxV2 from '../../component/ComboboxV2';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import Combobox from '../../component/Combobox';
import { DataCombobox,  } from './DataCombobox';
import { controller } from './controller';
import LoadingComponent from '../../component/LoadingComponent';
import Toast from 'react-native-simple-toast';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
class Calendar_Add_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TypeForm: props.TypeForm,
            loading: false,
            setEventStartTime: false,
            setEventEndTime: false,
            setEventTimeNotify: false,
            Attachment: [],
            DeleteAttachment: [],
            CalenderTypeName: "",
            CalenderRangeName: "",
            DataSave: {
                CalendarGuid: null,
                IsLocked: false,
                StartTime: new Date(),
                EndTime: new Date(),
                TimeNotify: new Date(),
                CalenderContent: "",
                Title: "",
                IsActive: true,
                IsDeleted: false,
                CalenderType: "B",
                CalenderRange: "G",
                CheckNotify: false
            },
        };
    }
    async componentDidMount() {
        await this.request_storage_runtime_permission();
        if (this.props.TypeForm == 'edit') {
            this.setState({ loading: true });
            this.GetDataEdit();
        }
    }
    //#region  edit
    GetDataEdit = () => {
        controller.Calendar_GetItem(this.props.data.CalendarGuid, rs => {
            this.setState({
                DataSave: rs,
                StartTime: new Date(rs.StartTime),
                EndTime: new Date(rs.EndTime),
                TimeNotify: rs.TimeNotify !== null ? new Date(rs.TimeNotify) : new Date()
            })
        });
        this.GetAttachments();
    };
    GetAttachments = () => {
        controller.GetAttachments(this.props.data.CalendarGuid, rs => {
            this.setState({
                Attachment: rs,
                loading: false
            })
        });
    };
    //#endregion
    render() {
        return (
            this.state.loading !== true ?
                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : null}
                    style={{ flex: 1 }}
                >
                    <TouchableWithoutFeedback
                        style={{ flex: 1 }}
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <View style={[AppStyles.container]}>
                            {this.props.TypeForm == 'add' ?
                                <TabBar_Title
                                    title={'Thêm mới lịch'}
                                    callBack={() => this.callBackList()} />
                                : (this.props.TypeForm === 'edit' && this.state.loading === false) ?
                                    <TabBar_Title
                                        title={this.state.DataSave.Title}
                                        callBack={() => this.callBackList()} />
                                    : null
                            }
                            <Divider />
                            {this.state.loading === false ? this.CustomForm(this.state.DataSave) : null}
                            {this.state.setEventStartTime === true ?
                                (
                                    (<DateTimePickerModal
                                        locale="vi-VN"
                                        cancelTextIOS="Huỷ"
                                        confirmTextIOS="Chọn"
                                        headerTextIOS="Chọn thời gian"
                                        isVisible={this.state.setEventStartTime}
                                        mode="datetime"
                                        onConfirm={this.handleConfirmStartTime}
                                        onCancel={this.hideDatePicker}
                                    />)
                                )
                                : null
                            }
                            {this.state.setEventEndTime === true ?
                                (<DateTimePickerModal
                                    locale="vi-VN"
                                    cancelTextIOS="Huỷ"
                                    confirmTextIOS="Chọn"
                                    headerTextIOS="Chọn thời gian"
                                    isVisible={this.state.setEventEndTime}
                                    mode="datetime"
                                    onConfirm={this.handleConfirmEndTime}
                                    onCancel={this.hideDatePicker}
                                />)
                                : null
                            }
                            {this.state.setEventTimeNotify === true ?
                                (<DateTimePickerModal
                                    locale="vi-VN"
                                    cancelTextIOS="Huỷ"
                                    confirmTextIOS="Chọn"
                                    headerTextIOS="Chọn thời gian"
                                    isVisible={this.state.setEventTimeNotify}
                                    mode="datetime"
                                    onConfirm={this.handleConfirmTimeNotify}
                                    onCancel={this.hideDatePicker}
                                />)
                                : null
                            }
                            <Combobox
                                value={this.state.DataSave.CalenderType}
                                TypeSelect={"single"}// single or multiple
                                callback={this.ChoiceEmployeeCalenderType}
                                data={DataCombobox.ListCalenderType}
                                nameMenu={'Chọn loại lịch'}
                                eOpen={this.openCombobox_CalenderType}
                                position={'bottom'}
                            />
                            <Combobox
                                value={this.state.DataSave.CalenderRange}
                                TypeSelect={"single"}// single or multiple
                                callback={this.ChoiceEmployeeCalenderRange}
                                data={DataCombobox.ListCalenderRange}
                                nameMenu={'Chọn phạm vi'}
                                eOpen={this.openCombobox_CalenderRange}
                                position={'bottom'}
                            />
                            <ComboboxV2
                                callback={this.ChoiceAtt}
                                data={DataCombobox.ListComboboxAtt}
                                eOpen={this.openCombobox_Att}
                            />
                            <OpenPhotoLibrary
                                callback={this.callbackLibarary}
                                openLibrary={this.openLibrary}
                            />
                        </View>
                    </TouchableWithoutFeedback >
                </KeyboardAvoidingView>
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    CustomForm = (para) => (
        <ScrollView horizontal={false} style={{ flexDirection: 'column', padding: 5 }}>
            {/* Sự kiện */}
            <View style={{ padding: 10, paddingBottom: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Tiêu đề</Text>
                    <Text style={{ color: 'red' }}> *</Text>
                </View>
                <TextInput style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    placeholder={'Nhập tiêu đề'}
                    autoCapitalize="none"
                    // multiline={true}
                    maxLength={255}
                    value={para.Title}
                    onChangeText={text => this.setTitle(text)} />
            </View>

            {/* Thời gian bắt đầu */}
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu</Text>
                    <Text style={{ color: 'red' }}></Text>
                </View>
                <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'flex-end' }]} onPress={() => this.setState({ setEventStartTime: true })}>
                    <Text>{moment(this.state.DataSave.StartTime).format('DD/MM/YYYY HH:mm:ss')}</Text>
                </TouchableOpacity>

            </View>

            {/* ngày hết hạn */}
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Text style={AppStyles.Labeldefault}>Thời gian kết thúc</Text>
                    <Text style={{ color: 'red' }}></Text>
                </View>
                <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'flex-end' }]} onPress={() => this.setState({ setEventEndTime: true })}>
                    <Text>{moment(this.state.DataSave.EndTime).format('DD/MM/YYYY HH:mm:ss')}</Text>
                </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', padding: 10, paddingBottom: 0 }}>
                <View style={{ flex: 1, marginRight: 2 }}>
                    <View style={{ marginBottom: 5 }}>
                        <Text style={AppStyles.Labeldefault}>Loại công việc</Text>
                    </View>
                    <TouchableOpacity style={[AppStyles.FormInput]} onPress={() => this.onActionCombobox_CalenderType()}>
                        <Text style={[AppStyles.TextInput, { color: AppColors.black }]}>{this.state.CalenderTypeName}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, marginLeft: 2 }}>
                    <View style={{ marginBottom: 5 }}>
                        <Text style={AppStyles.Labeldefault}>Phạm vi lịch</Text>
                    </View>
                    <TouchableOpacity style={[AppStyles.FormInput]} onPress={() => this.onActionCombobox_CalenderRange()}>
                        <Text style={[AppStyles.TextInput, { color: AppColors.black }]}>{this.state.CalenderRangeName}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Text style={AppStyles.Labeldefault}>Thông báo</Text>
                    <Text style={{ color: 'red' }}></Text>
                </View>
                <CheckBox
                    checked={this.state.DataSave.CheckNotify}
                    onPress={() => this.oncheck()}
                />
            </View>
            {this.state.DataSave.CheckNotify ?
                <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                        <Text style={AppStyles.Labeldefault}>Thời gian gửi thông báo</Text>
                        <Text style={{ color: 'red' }}></Text>
                    </View>
                    <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'flex-end' }]} onPress={() => this.setState({ setEventTimeNotify: true })}>
                        <Text>{moment(this.state.DataSave.TimeNotify).format('DD/MM/YYYY HH:mm:ss')}</Text>
                    </TouchableOpacity>
                </View>
                : null}

            {/* Nội dung */}
            <View style={{ padding: 10, paddingBottom: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Nội dung lịch làm việc</Text>
                    <Text style={{ color: 'red' }}></Text>
                </View>
                <TextInput
                    style={[AppStyles.FormInput, { maxHeight: 100 }]}
                    underlineColorAndroid="transparent"
                    value={this.state.DataSave.CalenderContent}
                    numberOfLines={5}
                    multiline={true}
                    onChangeText={text => this.setCalenderContent(text)}
                />
            </View>

            {/* đính kèm */}
            <View style={{ flexDirection: 'column', padding: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                        <Text style={{ color: 'red' }}> </Text>
                    </View>
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }} onPress={() => this.openAttachment()}>
                        <Icon
                            name="picture"
                            type="antdesign"
                            size={15}
                            color={AppColors.ColorAdd} />
                        <Text style={[AppStyles.Labeldefault, { color: AppColors.ColorAdd }]}> Chọn file</Text>
                    </TouchableOpacity>
                </View>
                <Divider />
                {this.state.Attachment.length > 0 ?
                    this.state.Attachment.map((item, index) => (
                        <View style={[{ flexDirection: 'row', marginBottom: 3 }]}>
                            <View style={{ flex: 6, justifyContent: 'center', marginLeft: 5 }}>
                                <Text style={AppStyles.textInput}>{item.FileName}</Text>
                            </View>
                            <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1, padding: 10 }]} onPress={() => this.removeAttactment(index)}>
                                <Icon
                                    name="close"
                                    type="antdesign"
                                    size={20}
                                    color={AppColors.ColorDelete} />
                            </TouchableOpacity>
                        </View>
                    ))
                    : null}
            </View>

            <View style={{ flexDirection: 'row', padding: 10, marginBottom: 20 }}>
                {/* cancel*/}
                <TouchableOpacity style={[
                    AppStyles.containerCentered,
                    {
                        flex: 1,
                        padding: 10,
                        borderRadius: 10,
                        borderColor: AppColors.ColorPrimary,
                        borderWidth: 0.5,
                        marginRight: 5
                    }
                ]} onPress={() => this.Cancel()}>
                    <Text style={[AppStyles.Titledefault, { color: AppColors.ColorPrimary }]} >Huỷ bỏ</Text>
                </TouchableOpacity>
                {/* Update*/}
                <TouchableOpacity style={[
                    AppStyles.containerCentered,
                    {
                        flex: 1,
                        padding: 10,
                        borderRadius: 10,
                        borderColor: AppColors.ColorEdit,
                        borderWidth: 0.5,
                        marginLeft: 5
                    }
                ]} onPress={() => { this.props.TypeForm == 'edit' ? this.Update() : this.Submit() }}>
                    <Text style={[AppStyles.Titledefault, { color: AppColors.ColorEdit }]} >Lưu</Text>
                </TouchableOpacity>
            </View>
        </ScrollView >
    );
    oncheck = () => {
        this.state.DataSave.CheckNotify = !this.state.DataSave.CheckNotify;
        this.setState({ DataSave: this.state.DataSave });
    }
    setTitle = (value) => {
        this.state.DataSave.Title = value;
        this.setState({ DataSave: this.state.DataSave });
    }
    setCalenderContent = (value) => {
        this.state.DataSave.CalenderContent = value;
        this.setState({ DataSave: this.state.DataSave });
    }
    setStartTime = (val) => {
        this.state.DataSave.StartTime = val;
        this.setState({ DataSave: this.state.DataSave });
    };
    setEndTime = (val) => {
        this.state.DataSave.EndTime = val;
        this.setState({ DataSave: this.state.DataSave });
    };
    setTimeNotify = (val) => {
        this.state.DataSave.TimeNotify = val;
        this.setState({ DataSave: this.state.DataSave });
    };
    handleConfirmEndTime = (val) => {
        this.state.DataSave.EndTime = val;
        this.setState({ DataSave: this.state.DataSave });
        this.hideDatePicker()
    }
    handleConfirmTimeNotify = (val) => {
        this.state.DataSave.TimeNotify = val;
        this.setState({ DataSave: this.state.DataSave });
        this.hideDatePicker()
    }
    handleConfirmStartTime = (val) => {
        this.state.DataSave.StartTime = val;
        this.setState({ DataSave: this.state.DataSave });
        this.hideDatePicker()
    }
    hideDatePicker = () => {
        this.setState({
            setEventTimeNotify: false,
            setEventEndTime: false,
            setEventStartTime: false
        });
    };
    //#region  combobox loại lịch
    _openCombobox_CalenderType() { }
    openCombobox_CalenderType = (d) => {
        this._openCombobox_CalenderType = d;
    }
    onActionCombobox_CalenderType() {
        this._openCombobox_CalenderType();
    }
    ChoiceEmployeeCalenderType = (rs) => {
        this.state.DataSave.CalenderType = rs.value;
        this.setState({ DataSave: this.state.DataSave, CalenderTypeName: rs.text });
    }
    //#endregion
    //#region  combobox phạm vi lịch
    _openCombobox_CalenderRange() { }
    openCombobox_CalenderRange = (d) => {
        this._openCombobox_CalenderRange = d;
    }
    onActionCombobox_CalenderRange() {
        this._openCombobox_CalenderRange();
    }
    ChoiceEmployeeCalenderRange = (rs) => {
        this.state.DataSave.CalenderRange = rs.value;
        this.setState({ DataSave: this.state.DataSave, CalenderRangeName: rs.text });
    }
    //#endregion

    callBackList() {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Calendar_Back', Data: { CalendarGuid: this.state.DataSave.CalendarGuid }, ActionTime: (new Date()).getTime() });
    }

    //#region openAttachment
    _openCombobox_Att() { }
    openCombobox_Att = (d) => {
        this._openCombobox_Att = d;
    }
    openAttachment() {
        this._openCombobox_Att();
    }
    ChoiceAtt = (rs) => {
        if (rs.value !== null) {
            switch (rs.value) {
                case 'img':
                    this.openPhotoLibrary();
                    break;
                case 'file':
                    this.choiceFile();
                    break;
                case 'camera':
                    this.ChoicePicture();
                    break;
                default:
                    break;
            }
        }
    }

    //#region open img
    _open() { }
    openLibrary = (d) => {
        this._open = d;
    }
    openPhotoLibrary() {
        this._open();
    }
    callbackLibarary = (rs) => {
        for (let i = 0; i < rs.length; i++) {
            this.state.Attachment.push(
                {
                    FileName: rs[i].name.toLowerCase(),
                    size: rs[i].size,
                    type: rs[i].type.toLowerCase(),
                    uri: rs[i].uri
                });
        };
        this.setState({ Attachment: this.state.Attachment });
    };
    //#endregion

    //#region trỏ đến thư mục file trong máy
    async choiceFile() {
        try {
            const dirs = RNFetchBlob.fs.dirs

            RNFetchBlob.fs.ls(dirs.PictureDir)
                .then((stats) => {
                    console.log("dirs:..........", stats)

                })
                .catch((err) => {
                    console.log("dirs:..........", err)

                })
            const res = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.allFiles],
            });
            var _liFiles = this.state.Attachment;
            for (let index = 0; index < res.length; index++) {
                _liFiles.push({
                    FileName: res[index].name.toLowerCase(),
                    size: res[index].size,
                    type: res[index].type.toLowerCase(),
                    uri: res[index].uri
                })
                await RNFetchBlob.fs.stat(res[index].uri)
                    .then((s) => {
                        _liFiles[_liFiles.length - 1].FileName = s.filename;
                        // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
                    })
                    .catch((err) => {
                        console.log('Error get infor file')
                    })
            }
            this.setState({ Attachment: this.state.Attachment });
        } catch (err) {
            this.setState({ loading: false });
            console.log('Error from pick file')
        }
    }
    //#endregion

    //#region trỏ đến camera
    async request_storage_runtime_permission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    'title': 'ReactNativeCode Storage Permission',
                    'message': 'ReactNativeCode App needs access to your storage to download Photos.'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                console.log("Storage Permission Granted.");
            }
            else {

                console.log("Storage Permission Not Granted");

            }
        } catch (err) {
            console.warn(err)
        }
    }

    async ChoicePicture() {
        try {
            var _liFiles = this.state.Attachment;
            await ImagePicker.launchCamera(DataCombobox.options, (response) => {
                if (response.didCancel) {
                    // console.log('User cancelled image picker');
                } else if (response.error) {
                    //  console.log('ImagePicker Error: ', response.error);
                } else {
                    var _type = response.type.split("/")[1].toLowerCase()
                    _liFiles.push(
                        {
                            FileName: response.fileName == null ? _type + "." + _type : response.fileName.toLowerCase(),
                            size: response.fileSize,
                            type: response.type.toLowerCase(),
                            uri: response.uri
                        });

                }
                this.setState(
                    {
                        Attachment: _liFiles,
                    }
                );
            })
        } catch (err) {
            if (ImagePicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }
    //#endregion

    //#endregion

    removeAttactment(para) {
        var list = this.state.Attachment;
        var listNewValue = [];
        for (let i = 0; i < list.length; i++) {
            if (i === para) {
                if (list[i].AttachmentGuid !== null && list[i].AttachmentGuid !== undefined) {
                    this.state.DeleteAttachment.push(list[i].AttachmentGuid)
                }
            } else {
                listNewValue.push(list[i]);
            }
        }
        this.setState({
            Attachment: listNewValue,
            DeleteAttachment: this.state.DeleteAttachment
        });
    }
    Submit = () => {
        if (this.state.DataSave.Title === "") {
            Toast.showWithGravity('Yêu cầu nhập tiêu đề.', Toast.LONG, Toast.CENTER);
            return;
        }
        var _start = this.state.DataSave.StartTime.getTime();
        var _end = this.state.DataSave.EndTime.getTime();
        if (_start >= _end) {
            Toast.showWithGravity('Yêu cầu thời gian bắt đầu nhỏ hơn thời gian đến hạn.', Toast.LONG, Toast.CENTER);
            return;
        }
        this.setState({ loading: true }, () => {
            controller.Calendar_Insert(this.state.DataSave, this.state.Attachment, rs => {
                Toast.showWithGravity(rs.message, Toast.LONG, Toast.CENTER);
                if (rs.errorCode === 200) {
                    this.callBackList();
                } else {
                    this.setState({ loading: false })
                }
            });
        })
    }
    Update = () => {
        if (this.state.DataSave.Title === "") {
            Toast.showWithGravity('Yêu cầu nhập tiêu đề.', Toast.LONG, Toast.CENTER);
            return;
        }
        var _start = FuncCommon.ConDate(this.state.DataSave.StartTime, 2);
        var _end = FuncCommon.ConDate(this.state.DataSave.EndTime, 2);
        if (_start >= _end) {
            Toast.showWithGravity('Yêu cầu thời gian bắt đầu nhỏ hơn thời gian đến hạn.', Toast.LONG, Toast.CENTER);
            return;
        }
        this.setState({ loading: true }, () => {
            controller.Calendar_Update(this.state.DataSave, this.state.Attachment, this.state.DeleteAttachment, rs => {
                Toast.showWithGravity(rs.message, Toast.LONG, Toast.CENTER);
                if (rs.errorCode === 200) {
                    this.callBackList();
                } else {
                    this.setState({ loading: false })
                }
            });
        })
    }
    Cancel() {
        Alert.alert(
            'Thông báo',
            'Bạn có chắc chắn huỷ bỏ không?',
            [
                {
                    text: 'Xác nhận',
                    onPress: () => this.callBackList()
                },
                { text: 'Huỷ', onPress: () => console.log('No Pressed') },
            ],
            { cancelable: true }
        );
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Calendar_Add_Component);
