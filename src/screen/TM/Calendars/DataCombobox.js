export default {
  ListCalenderType: [
    {
      value: 'A',
      text: 'Lịch họp',
    },
    {
      value: 'B',
      text: 'Lịch công việc',
    },
    {
      value: 'C',
      text: 'Lịch công tác',
    },
    {
      value: 'D',
      text: 'Lịch khác',
    },
  ],
  ListCalenderRange: [
    {
      value: 'E',
      text: 'Lịch cơ quan',
    },
    {
      value: 'F',
      text: 'Lịch cá nhân',
    },
    {
      value: 'G',
      text: 'Lịch phòng',
    },
  ],
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
  ],
  ListtabbarBotomDetail: [
    // {
    //     Title: "Cập nhật",
    //     Icon: "edit",
    //     Type: "antdesign",
    //     Value: "edit",
    //     OffEditcolor: true,
    //     Checkbox: false,
    // },
    {
      Title: 'Đính kèm',
      Icon: 'attachment',
      Type: 'entypo',
      Value: 'attachment',
      OffEditcolor: true,
      Checkbox: false,
    },
    {
      Title: 'Xoá',
      Icon: 'delete',
      Type: 'antdesign',
      Value: 'delete',
      OffEditcolor: true,
      Checkbox: false,
    },
  ],
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
};
