import React, { Component } from 'react';
import {
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View,
    TouchableHighlight,
    Alert,
    Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Divider, CheckBox, Icon } from 'react-native-elements';
import { API_TM_CALENDAR, API_HR } from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import { AppStyles, AppColors, AppSizes } from '@theme';
import _ from 'lodash';
import { ScrollView } from 'react-native-gesture-handler';
import { FuncCommon } from '../../../utils';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import LoadingComponent from '../../component/LoadingComponent';
import HTML from "react-native-render-html";
import { DataCombobox,  } from './DataCombobox';
import { controller } from './controller';
import moment from 'moment';

const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Calendar_Detail_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            CalendarGuid: props.data.CalendarGuid,
            DataView: null,
            ListAttachment: [],
            ListUserApproval: [],
            ListUserProcess: [],
            ListUserFollow: [],
        };
    }
    componentDidMount() {
        this.GetData();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.moduleId == "Calendar_Back") {
            this.setState({ CalendarGuid: nextProps.Data.CalendarGuid })
            this.GetData();
        }
    }
    //Form detail
    GetData() {
        controller.Calendar_GetItem(this.props.data.CalendarGuid, rs => {
            this.setState({ DataView: rs });
        });
    }
    //
    render() {
        return (
            this.state.DataView === null ? <LoadingComponent backgroundColor={'#fff'} /> :
                <View style={[AppStyles.container]}>
                    {this.state.DataView !== null ?
                        <TabBar_Title
                            title={FuncCommon.ConMiniTitle(this.state.DataView.Title, 25)}
                            callBack={() => this.callBackList()} />
                        : null
                    }
                    <Divider />
                    <View style={{ flex: 1 }}>
                        {this.CustomFormDetail(this.state.DataView)}
                    </View>
                    <View style={[AppStyles.StyleTabvarBottom, { backgroundColor: AppColors.white }]}>
                        <TabBarBottomCustom
                            ListData={DataCombobox.ListtabbarBotomDetail}
                            onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                        />
                    </View>
                </View>

        );
    }
    CustomFormDetail = (para) => (
        <View style={{ padding: 10 }}>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                    <Text style={AppStyles.Labeldefault}>Tên công việc : </Text>
                </View>
                <View style={{ flex: 2 }}>
                    <Text style={AppStyles.Textdefault}>{para.Title}</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                    <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu: </Text>
                </View>
                <View style={{ flex: 2 }}>
                    <Text style={AppStyles.Textdefault}>{moment(para.StartTime).format('DD/MM/YYYY HH:mm:ss')}</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                    <Text style={AppStyles.Labeldefault}>Thời gian kết thúc: </Text>
                </View>
                <View style={{ flex: 2 }}>
                    <Text style={AppStyles.Textdefault}>{moment(para.EndTime).format('DD/MM/YYYY HH:mm:ss')}</Text>
                </View>
            </View>
            {para.TimeNotify ?
                <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                    <View style={{ flex: 1 }}>
                        <Text style={AppStyles.Labeldefault}>Thông báo: </Text>
                    </View>
                    <View style={{ flex: 2, flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[AppStyles.Textdefault, { marginRight: 10 }]}>{moment(para.TimeNotify).format('DD/MM/YYYY HH:mm:ss')}</Text>
                        <Icon name={"check-square"} type={"feather"} color={AppColors.Maincolor} />
                    </View>
                </View>
                : null}
            <View style={{ marginTop: 10 }}>
                <Text style={AppStyles.Labeldefault}>Nội dung:</Text>
            </View>
            <ScrollView >
                <TouchableHighlight >
                    <HTML source={{ html: para.CalenderContent }} contentWidth={DRIVER.width} />
                </TouchableHighlight >
            </ScrollView>
        </View>
    );
    callBackList() {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Calendar_Back', Data: { Calendar: true }, ActionTime: (new Date()).getTime() });
    }
    ConvertStatus = (item) => {
        if (item == 'H') {
            return 'Cao'
        } else if (item == 'N') {
            return 'Trung bình'
        } else {
            'Thấp'
        }
    }
    CallbackValueBottom = (value) => {
        switch (value) {
            case 'edit':
                Actions.calendarAdd({ TypeForm: 'edit', data: { CalendarGuid: this.state.DataView.CalendarGuid } })
                break;
            case 'attachment':
                var obj = {
                    ModuleId: '20',
                    RecordGuid: this.state.DataView.CalendarGuid
                }
                Actions.attachmentComponent(obj);
                break;
            case 'delete':
                Alert.alert(
                    'Thông báo',
                    'Bạn có muốn xóa ' + this.state.DataView.Title + ' không?',
                    [
                        {
                            text: 'Xác nhận',
                            onPress: () => this.DeleteCalendar(this.state.DataView.CalendarGuid)
                        },
                        { text: 'Huỷ', onPress: () => console.log('No Pressed') },
                    ],
                    { cancelable: true }
                );
                break;
            case 'success':

                break;
            case 'comment':
                Actions.taskComment({
                    data: {
                        CalendarGuid: this.state.DataView.CalendarGuid,
                        TaskName: this.state.DataView.Title
                    }
                })
                break;
            default:
                break;
        }
    }
    DeleteCalendar = (item) => {
        let obj = {
            Id: item
        }
        API_TM_CALENDAR.Calendar_Delete(obj).then(rs => {
            this.callBackList()
        }).catch(error => {
            console.log(error);
        });
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Calendar_Detail_Component);
