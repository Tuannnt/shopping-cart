import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Divider, Icon, ListItem} from 'react-native-elements';
import {API_TM_TASKS, API_TM_CALENDAR, API} from '@network';
import TabBar from '../../component/TabBar';
import {AppStyles, AppColors} from '@theme';
import FormSearch from '../../component/FormSearch';
import SearchDay from '../../component/SearchDay';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import * as Progress from 'react-native-progress';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import {FuncCommon} from '@utils';
import { DataCombobox,  } from '../Task/DataCombobox';
import { controller } from '../Task/controller';
import LoadingComponent from '../../component/LoadingComponent';
import ComboboxV2 from '../../component/ComboboxV2';
import Kanban_Component from '../Task/Kanban_Component';
import moment from 'moment';

const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const DataMenuBottomCalendar = [
  {
    key: 'details',
    name: 'Xem',
    icon: {
      name: 'eye-outline',
      type: 'material-community',
      color: AppColors.Maincolor,
    },
  },
  // {
  //   key: 'edit',
  //   name: 'Sửa',
  //   icon: {
  //     name: 'edit',
  //     type: 'antdesign',
  //     color: AppColors.Maincolor,
  //   },
  // },
  {
    key: 'delete',
    name: 'Xóa',
    icon: {
      name: 'delete',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'attachment',
    name: 'Đính kèm',
    icon: {
      name: 'attachment',
      type: 'entypo',
      color: AppColors.Maincolor,
    },
  },
];
class Task_ListAll_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      loadingSearch: false,
      InforUserLogin: global.__appSIGNALR.SIGNALR_object.USER,
      EvenCalendar: true,
      EvenKanban: false,
      EvenFromSearch: false,
      ListtabbarBotom: [],
      StartDate: FuncCommon.ConDate(new Date(), 2),
      EndDate: FuncCommon.ConDate(new Date(), 2),
      Process: '',
      selectItem: null,
      KanbanView: [],
      Count_Tab: 0,
      SearchKanban: false,
      ListCalendar: [],
      selectItemCalendar: null,

      offload: false,
      NameMenuCalendar: '',
      refreshing: false,
      ChoiceDate: '',
      ListTask: [],
      OffSearchDay: false,
      ListTask_ParentId: [],
    };
    this.jtableTasks = {
      txtSearch: '',
      Page: 0,
      StartDate: FuncCommon.ConDate(new Date(), 2),
      EndDate: FuncCommon.ConDate(new Date(), 2),
      Process: null,
    };
    this.jtableCalendar = {
      Search: '',
      StartDateTime: null,
      EndDateTime: null,
    };
    this._search_Kanban = {
      Page: 0,
      Search: '',
    };
  }
  componentDidMount() {
    this.setState({loading: true});
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Taskadd') {
      if (nextProps.Data !== null) {
        this.Calendar_GetAll();
      }
      if (this.state.EvenKanban === true) {
        this._ReloadKanban();
      }
    } else if (nextProps.moduleId === 'Calendar_Back') {
      this.setState({EvenCalendar: true});
      this.Calendar_GetAll();
    }
    this.nextPage(true);
  }
  _ReloadKanban() {}
  ReloadKanban = d => {
    this._ReloadKanban = d;
  };
  //#region Lọc ngày
  Search_day(value) {
    this.setState({OffSearchDay: false, loadingSearch: true});
    controller.Searchday(value, rs => {
      this.state.StartDate = rs.StartDate.split(' ')[0];
      this.state.EndDate = rs.EndDate.split(' ')[0];
      this.setState({
        StartDate: this.state.StartDate,
        EndDate: this.state.EndDate,
      });
      this.jtableCalendar.StartDateTime = rs.StartDate;
      this.jtableCalendar.EndDateTime = rs.EndDate;
      this.jtableTasks.StartDate = rs.StartDate.split(' ')[0];
      this.jtableTasks.EndDate = rs.EndDate.split(' ')[0];
      if (this.state.EvenCalendar == true) {
        this.Calendar_GetAll();
      } else {
        this.nextPage(true);
      }
    });
  }
  //#endregion
  //#region Search
  updateSearch = search => {
    if (this.state.EvenKanban == true) {
    } else if (this.state.EvenCalendar) {
      this.jtableCalendar.Search = search;
      this.Calendar_GetAll();
    } else {
      this.jtableTasks.txtSearch = search;
      this.nextPage(true);
    }
  };
  //#endregion
  //#region Lấy danh sách Task
  nextPage(status) {
    var list = [];
    if (status === true) {
      this.jtableTasks.Page = 1;
      this.setState({loadingSearch: true, ListTask: []});
    } else {
      this.jtableTasks.Page++;
      list = this.state.ListTask;
    }
    controller.GetTasks(this.jtableTasks, rs => {
      for (let i = 0; i < rs.length; i++) {
        list.push(rs[i]);
      }
      this.setState({
        ListTask: list,
        refreshing: false,
        loading: false,
        loadingSearch: false,
      });
    });
  }
  //#endregion
  //#region Lấy danh sách Calendar
  Calendar_GetAll() {
    this.setState({ListCalendar: []});
    controller.Calendar_GetAll(this.jtableCalendar, rs => {
      this.setState({
        ListCalendar: rs,
        refreshing: false,
        loading: false,
        loadingSearch: false,
      });
    });
  }
  //#endregion
  //======================================================================VIEW========================================================================
  //#region View Tổng phân biệt các View
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={AppStyles.container}>
          <TabBar
            title={'Lịch làm việc'}
            BackModuleByCode={'TM'}
            FormSearch={true}
            addForm={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            CallbackFormAdd={() => this.CallbackFormAdd()}
          />
          <Divider />
          <SearchDay CallBackValue={value => this.Search_day(value)} />
          {this.state.EvenFromSearch == true ? (
            <FormSearch
              CallbackSearch={callback => this.updateSearch(callback)}
            />
          ) : null}
          {this.state.loading === false ? (
            <View style={{flex: 1}}>
              {/* view lịch */}
              {this.state.EvenCalendar ? (
                this.state.ListCalendar.length > 0 ? (
                  this.CustomCalendar(this.state.ListCalendar)
                ) : (
                  <View style={[AppStyles.centerAligned]}>
                    <ImageBackground
                      source={require('../../../images/TM/CalendarNull.jpg')}
                      style={[
                        AppStyles.centerAligned,
                        {width: '100%', height: '70%'},
                      ]}
                    />
                    <Text style={[AppStyles.Titledefault]}>
                      Không tìm thấy lịch làm việc
                    </Text>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {textAlign: 'center', padding: 10},
                      ]}>
                      Hãy tạo lịch làm việc mới để sắp xếp thời gian khoa học
                      cho mình nhé.
                    </Text>
                    <TouchableOpacity
                      style={[
                        AppStyles.centerAligned,
                        {
                          marginTop: 20,
                          backgroundColor: AppColors.ColorAdd,
                          width: '60%',
                          padding: 10,
                          borderRadius: 10,
                        },
                      ]}
                      onPress={() => this.CallbackFormAdd()}>
                      <Text style={[AppStyles.Titledefault, {color: '#fff'}]}>
                        Thêm mới
                      </Text>
                    </TouchableOpacity>
                  </View>
                )
              ) : (
                <LoadingComponent backgroundColor={'#fff'} />
              )}
            </View>
          ) : (
            <LoadingComponent backgroundColor={'#fff'} />
          )}
          <MenuActionCompoment
            callback={this.actionMenuCallbackCalendar}
            data={DataMenuBottomCalendar}
            eOpen={this.openMenuCalendar}
            position={'bottom'}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  //#endregion
  //#region View Calendar
  CustomCalendar = item => (
    <FlatList
      data={item}
      renderItem={({item, index}) => (
        <TouchableOpacity
          style={{
            padding: 10,
            borderBottomColor: AppColors.gray,
            borderBottomWidth: 0.5,
          }}
          onPress={() => this.CalendarViewItem(item.CalendarGuid)}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <Text style={AppStyles.Titledefault}>
                {moment(item.StartTime).format('HH:mm')}
              </Text>
              <Text style={AppStyles.Textdefault}>
                {moment(item.EndTime).format('HH:mm')}
              </Text>
            </View>
            <View
              style={{
                flex: 5,
                flexDirection: 'column',
                borderLeftWidth: 2,
                paddingLeft: 10,
                borderLeftColor: AppColors.StatusTask_CXL,
              }}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 5}}>
                  <Text style={AppStyles.Titledefault}>
                    {FuncCommon.ConMiniTitle(item.Title, 25)}
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginRight: 10,
                  }}>
                  <Icon
                    name="attachment"
                    type="entypo"
                    size={15}
                    color={AppColors.gray}
                  />
                  <Text
                    style={[AppStyles.Textdefault, {color: AppColors.gray}]}>
                    {' '}
                    {item.count}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flex: 0.5,
                    alignItems: 'flex-end',
                    justifyContent: 'center',
                  }}
                  onPress={() => this.onActionCalendar(item)}>
                  <Icon
                    name="dots-three-horizontal"
                    type="entypo"
                    size={15}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', flex: 3}}>
                <Text style={[AppStyles.Textdefault, {color: AppColors.gray}]}>
                  {this.ViewType(item.Type)}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )}
      // onEndReached={() => this.loadMoreData()}
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['black']}
        />
      }
    />
  );
  //#endregion
  //============================================================= Chức năng dùng chung ===============================================================
  //#region
  //#region Dẫn đến form add của các module
  CallbackFormAdd() {
    this.state.EvenCalendar
      ? Actions.calendarAdd({TypeForm: 'add'})
      : Actions.taskAdd({TypeForm: 'add'});
  }
  //#endregion
  //#region Độ dài văn bản
  checkText(str, i) {
    try {
      if (str !== null && str !== '') {
        var splitted = str.split(' ');
        var strreturn = '';
        var j = 0;
        for (var a = 0; a < splitted.length; a++) {
          j = j + 1;
          if (j == i) {
            strreturn = strreturn + ' ' + splitted[a] + '...';
            break;
          } else {
            strreturn = strreturn + ' ' + splitted[a];
          }
        }
        return strreturn.trim();
      } else {
        return 'Không có nội dung';
      }
    } catch (error) {
      return 'Không có nội dung';
    }
  }
  //#endregion
  //#region  Tuỳ chỉnh thời gian
  customdateTime(DataTime) {
    if (DataTime != null) {
      var date = FuncCommon.ConDate(DataTime, 1);
      var time = date.split(' ');
      return time[1];
    } else {
      return '';
    }
  }
  //#endregion
  //#endregion

  //=========================================================== Chức năng của tab Calendar ===========================================================
  //#region
  //#region Bật tắt form Calendar
  CallbackFormcalendar = value => {
    this.setState({EvenCalendar: value});
    if (value == true) {
      this.Calendar_GetAll();
    } else {
      this.nextPage(true);
    }
  };
  //#endregion
  //#region Dẫn đến view chi tiết Calendar
  CalendarViewItem = value => {
    Actions.calendarDetail({data: {CalendarGuid: value}});
  };
  //#endregion
  //#region Menu chọn chức năng Calendar
  _openMenuCalendar() {}
  openMenuCalendar = d => {
    this._openMenuCalendar = d;
  };
  onActionCalendar(item) {
    this.state.NameMenuCalendar = item.Title;
    this.setState({
      selectItemCalendar: item,
      NameMenuCalendar: item.Title,
    });
    this._openMenuCalendar();
  }
  //#endregion
  //#region Chức năng trong menu Calendar
  actionMenuCallbackCalendar = d => {
    switch (d) {
      case 'details':
        Actions.calendarDetail({
          data: {CalendarGuid: this.state.selectItemCalendar.CalendarGuid},
        });
        break;
      case 'edit':
        Actions.calendarAdd({
          TypeForm: 'edit',
          data: {CalendarGuid: this.state.selectItemCalendar.CalendarGuid},
        });
        break;
      case 'delete':
        Alert.alert(
          'Thông báo',
          'Bạn có muốn xóa ' + this.state.selectItemCalendar.Title + ' không?',
          [
            {
              text: 'Xác nhận',
              onPress: () =>
                this.DeleteCalendar(this.state.selectItemCalendar.CalendarGuid),
            },
            {text: 'Huỷ', onPress: () => console.log('No Pressed')},
          ],
          {cancelable: true},
        );
        break;
      case 'attachment':
        var obj = {
          ModuleId: '20',
          RecordGuid: this.state.selectItemCalendar.CalendarGuid,
        };
        Actions.attachmentComponent(obj);
        break;
      default:
        break;
    }
  };
  //#endregion
  //#region Xoá 1 bản ghi calendar
  DeleteCalendar = item => {
    var obj = {
      Id: item,
    };
    API_TM_CALENDAR.Calendar_Delete(obj)
      .then(rs => {
        this.Calendar_GetAll();
      })
      .catch(error => {
        console.log('Error when call API Mobile.');
      });
  };
  //#endregion
  //#endregion
  ViewType = val => {
    return val === 'A'
      ? 'Lịch họp'
      : val === 'B'
      ? 'Lịch công việc'
      : val === 'C'
      ? 'Lịch công tác'
      : val === 'D'
      ? 'Lịch khác'
      : '';
  };
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Task_ListAll_Component);
