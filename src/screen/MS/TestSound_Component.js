import React, {Component, useRef, useEffect} from 'react';
import {connect} from 'react-redux';
import {
  Text,
  View,
  Animated,
  Dimensions,
  Easing,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import CustomView from '../component/CustomView';
import QRCode from 'react-native-qrcode-svg';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class TestSound_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.ListComboboxAtt = [
      {
        value: '1',
        text: 'Chọn từ thư viện',
      },
      {
        value: '2',
        text: 'Chọn file',
      },
      // {
      //     value: '3',
      //     text: "Chụp ảnh"
      // }
    ];
  }

  componentDidMount() {}

  render() {
    let base64Logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAA..';
    return (
      <TouchableOpacity
        style={[
          {
            backgroundColor: '#fff',
            flex: 1,
            marginBottom: 100,
            alignItems: 'center',
            justifyContent: 'center',
          },
        ]}>
        {/* <QRCode
                    value="esvn.vn"
                    logo={require('@images/logo/esvnQR.png')}
                    logoSize={20}
                    // logoBackgroundColor='transparent'
                /> */}
        <TouchableOpacity
          style={{
            backgroundColor: 'red',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
            borderRadius: 10,
          }}
          onPress={() => this.onActionopenQR()}>
          <Text style={{color: '#fff'}}>test chức năng</Text>
        </TouchableOpacity>
        <CustomView eOpen={this.openQR}>
          <View style={{height: 100, width: DRIVER.width - 50}}>
            <Text>
              {' '}
              Đây là cái tab em tạo. Em muốn viết nội dung ở đây để sang bên tab
              kia nó hiển thị nội dung tuỳ chỉnh ở đây thì làm như nào hả anh
            </Text>
          </View>
        </CustomView>
      </TouchableOpacity>
    );
  }
  _openQR() {}
  openQR = d => {
    this._openQR = d;
  };
  onActionopenQR() {
    this._openQR();
  }
}
const styles = StyleSheet.create({
  positionTop: {
    top: 80,
  },
  positionBottom: {
    bottom: -10,
  },
});
export default TestSound_Component;
