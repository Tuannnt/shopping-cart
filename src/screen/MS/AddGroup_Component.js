import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  ScrollView,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Image,
  //CheckBox,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, Icon, ListItem, CheckBox} from 'react-native-elements';
import {API, API_HR, API_Message} from '../../network';
import Fonts from '../../theme/fonts';
import {FlatGrid} from 'react-native-super-grid';
import {Thumbnail} from 'native-base';
import TabBar_Title from '../component/TabBar_Title';
import LoadingComponent from '../component/LoadingComponent';
import { listCombobox } from './listCombobox';
import { controller } from './controller';
import Toast from 'react-native-simple-toast';
import {FuncCommon} from '@utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
    paddingLeft: 5,
    paddingRight: 5,
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
    color: '#AAAAAA',
  },
  subtitleStyle_v1: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
});
class AddGroup_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txtName: '',
      refreshing: false,
      loading: true,
      Data: null,
      txtSearch: '',
      test: '',
      Id_User: '',
      ListSearch: [],
      Countcheck: 0,
      DataInsert: [],
      DataView: [],
      moduleBack: '',
    };
  }
  componentDidMount() {
    if (
      this.props.ListAddAcount !== null &&
      this.props.ListAddAcount != undefined
    ) {
      this.setState({DataView: this.props.ListAddAcount, Countcheck: 1});
    }
    if (this.props.moduleId !== null && this.props.moduleId != undefined) {
      this.setState({moduleBack: this.props.moduleId});
    }
    API.getProfile()
      .then(rs => {
        this.setState({
          LoginName_User: rs.data.LoginName,
          FullName_User: rs.data.FullName,
          Id_User: rs.data.EmployeeGuid,
        });
      })
      .catch(error => {
        ErrorHandler.handle(error.data);
      });
    // this.nextPage();
    this.Search_people('');
  }

  updateNameGroup = txt => {
    this.setState({txtName: txt});
  };
  updateSearch = search => {
    this.setState({txtSearch: search});
    this.Search_people(search);
  };
  //Tìm kiếm người chat
  Search_people(search) {
    var _ListSearch = {
      txtSearch: search,
      DepartmentGuid: '',
    };
    API_Message.Messenger_Employees_Search(_ListSearch)
      .then(res => {
        var checklistAPI = JSON.parse(res.data.data);
        var checklist = this.state.DataView;
        var view = [];
        for (let i = 0; i < checklistAPI.length; i++) {
          if (this.state.LoginName_User !== checklistAPI[i].LoginName) {
            for (let j = 0; j < checklist.length; j++) {
              if (checklistAPI[i].LoginName == checklist[j].LoginName) {
                checklistAPI[i].check = true;
              }
            }
            view.push(checklistAPI[i]);
          }
        }
        this.setState({
          ListSearch: view,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        ErrorHandler.handle(error.data);
      });
  }
  loadMoreData() {
    if (this.state.ListSearch >= 30) {
      this.Search_people('');
    }
  }
  onCheckBox(para, index) {
    var _countcheck = 0;
    var _list = this.state.ListSearch;
    _list[index].check = !_list[index].check;
    this.setState({
      ListSearch: _list,
    });
    var _dataView = this.state.DataView;
    if (_list[index].check == true) {
      _dataView.push({
        LoginName: _list[index].LoginName,
        EmployeeGuid: _list[index].EmployeeGuid,
      });
    } else {
      for (let i = 0; i < _dataView.length; i++) {
        if (_dataView[i].LoginName === _list[index].LoginName) {
          _dataView.splice(i, 1);
        }
      }
    }
    _countcheck = _dataView.length;
    this.setState({
      Countcheck: _countcheck,
      DataView: _dataView,
    });
  }
  CustomeListSearch = item => (
    <FlatList
      data={item}
      style={{flex: 1}}
      renderItem={({item, index}) => (
        <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
          <ListItem
            subtitle={() => {
              return (
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: -10,
                    marginTop: -10,
                  }}>
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 2, marginRight: 10}}>
                      <Thumbnail
                        style={{}}
                        source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                      />
                    </View>
                  </View>
                  <View style={{flex: 4, flexDirection: 'row'}}>
                    <View style={{flex: 6, justifyContent: 'center'}}>
                      <Text style={{fontSize: 14}}>{item.FullName}</Text>
                    </View>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                      <CheckBox
                        checked={this.state.ListSearch[index].check}
                        onPress={() =>
                          this.onCheckBox(item.EmployeeGuid, index)
                        }
                      />
                    </View>
                  </View>
                </View>
              );
            }}
            onPress={() => this.onCheckBox(item.EmployeeGuid, index)}
          />
        </View>
      )}
      onEndReached={() => this.loadMoreData()}
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['black']}
        />
      }
    />
  );
  render() {
    return this.state.loading !== true ? (
      <View style={styles.container}>
        <TabBar_Title
          title={'Tạo nhóm mới'}
          callBack={() => this.backToList()}
        />
        <Divider />
        <View style={{flex: 1, marginTop: 5}}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 10,
              }}>
              <Icon name="group" type="font-awesome" size={40} />
            </View>
            <View style={{flex: 4}}>
              <TextInput
                style={{
                  height: 50,
                  fontSize: 16,
                  borderBottomColor: '#00CCFF',
                  borderBottomWidth: 1,
                }}
                onChangeText={txt => this.updateNameGroup(txt)}
                value={this.state.txtName}
                placeholder={'Đặt tên nhóm'}
              />
            </View>
            {/* <View style={{ flex: 0.5, alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Icon name="check" type="antdesign" iconStyle={{ color: "#00CCFF" }} size={25} />
                        </View> */}
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 15,
              marginBottom: 15,
              backgroundColor: '#CAE5E8',
              borderRadius: 15,
            }}>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <TouchableWithoutFeedback underlayColor="white">
                <Icon name="search1" type="antdesign" size={14} />
              </TouchableWithoutFeedback>
            </View>
            <View style={{flex: 8}}>
              <TextInput
                style={{height: 50}}
                onChangeText={search => this.updateSearch(search)}
                value={this.state.txtSearch}
                placeholder={'Tìm họ và tên hoặc tên tài khoản'}
              />
            </View>
            <View
              style={{
                flex: 1,
                width: 45,
                height: 50,
                textAlign: 'center',
                justifyContent: 'center',
              }}>
              {this.state.txtSearch.length > 0 ? (
                <TouchableWithoutFeedback
                  onPress={() => this.onclickreturnSearch()}
                  underlayColor="white">
                  <Icon name="close" type="antdesign" size={14} />
                </TouchableWithoutFeedback>
              ) : null}
            </View>
          </View>
          {this.state.ListSearch
            ? this.CustomeListSearch(this.state.ListSearch)
            : null}
          {this.state.Countcheck > 0 ? (
            <View
              style={{
                flex: 0.18,
                backgroundColor: '#F2F2F2',
                borderRadius: 10,
              }}>
              {this.state.DataView && this.state.DataView.length > 0
                ? this.state.DataView.map((item, index) => (
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 4}}>
                        <ScrollView
                          horizontal={true}
                          showsHorizontalScrollIndicator={false}>
                          <View style={{width: 820}}>
                            <FlatGrid
                              itemDimension={50}
                              items={this.state.DataView}
                              renderItem={({item, index}) => (
                                <Thumbnail
                                  style={{}}
                                  source={API_HR.GetPicApplyLeaves(
                                    item.EmployeeGuid,
                                  )}
                                />
                              )}
                            />
                          </View>
                        </ScrollView>
                      </View>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                        onPress={() => this.submit()}>
                        <Icon
                          name="right"
                          type="antdesign"
                          color={'#00CCFF'}
                          size={30}
                        />
                      </TouchableOpacity>
                    </View>
                  ))
                : null}
            </View>
          ) : null}
        </View>
      </View>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  backToList() {
    if (this.state.moduleBack == 'InformationGotoAddGroup') {
      Actions.pop();
      Actions.refresh({
        moduleId: 'BackInformation',
        Data: {
          RecordGuid: this.props.guid,
          Name: this.props.Name,
          IsGroup: this.props.IsGroup,
        },
        ActionTime: new Date().getTime(),
      });
    } else {
      Actions.pop();
    }
  }
  onclickreturnSearch() {
    this.state.Search = '';
    this.updateSearch('');
  }
  //Load lại
  _onRefresh = () => {
    this.Search_people('');
    this.setState({refreshing: true});
  };
  //thêm mới
  submit() {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        Toast.showWithGravity('Yêu cầu kết nối mạng', Toast.LONG, Toast.CENTER);
        return;
      }
    });
    var strEmp = [global.__appSIGNALR.SIGNALR_object.USER.LoginName];
    for (let i = 0; i < this.state.DataView.length; i++) {
      strEmp.push(this.state.DataView[i].LoginName);
    }
    let dataSubmit = {
      Name: this.state.txtName ? this.state.txtName : 'Nhóm chưa có tên',
      IsGroup: 1,
      detail: strEmp,
    };
    API_Message.Messenger_ChatboxGroup_Insert(dataSubmit)
      .then(res => {
        console.log(res);
        if (res.data.errorCode !== 200) {
          Toast.showWithGravity(
            'Có lỗi thêm mới nhóm chat',
            Toast.LONG,
            Toast.CENTER,
          );
        }
        var _text = {
          S: 'A',
          D: [
            {
              L: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
              F: global.__appSIGNALR.SIGNALR_object.USER.FullName,
            },
          ],
        };
        var objMessage = {
          ChatboxGroupGuid: JSON.parse(res.data.data).ChatboxGroupGuid,
          Messager: JSON.stringify(_text),
          Type: 'I',
          IsGroup: true,
          ParentGuid: null,
        };
        controller.sendComment(objMessage, [], strEmp, rs => {
          if (rs === '') {
          }
        });
        if (this.state.moduleBack == 'Information') {
          Actions.home({
            data: 'Viewchat',
            DatachatGroupNew: {
              RecordGuid: JSON.parse(res.data.data).ChatboxGroupGuid,
              Name: dataSubmit.Name,
              IsGroup: 'True',
            },
          });
        } else {
          Actions.pop();
          Actions.refresh({
            moduleId: this.state.moduleBack,
            Data: {
              RecordGuid: JSON.parse(res.data.data).ChatboxGroupGuid,
              Name: dataSubmit.Name,
              IsGroup: 'True',
            },
            ActionTime: new Date().getTime(),
          });
        }
      })
      .catch(error => {
        ErrorHandler.handle(error.data);
      });
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(AddGroup_Component);
