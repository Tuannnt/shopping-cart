import React, { Component } from 'react';
import {
    FlatList,
    Keyboard,
    RefreshControl,
    StyleSheet,
    ScrollView,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
    Image,
    CheckBox,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import { Divider, Icon, ListItem, } from 'react-native-elements';
import { API, API_HR, API_Message } from '../../network';
import Fonts from '../../theme/fonts';
import { FlatGrid } from 'react-native-super-grid';
import { Thumbnail } from 'native-base';
import TabBar_Title from "../component/TabBar_Title";
import { listCombobox } from './listCombobox';
import { controller } from './controller';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius: 1,
        backgroundColor: '#FFF',
        elevation: 2,
        paddingLeft: 5,
        paddingRight: 5
    },
    subtitleStyle: {
        fontSize: 12,
        fontFamily: 'Arial',
        color: '#AAAAAA'
    },
    subtitleStyle_v1: {
        fontSize: 12,
        fontFamily: 'Arial'
    }
});
class AddUserOfGroup_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            LoginName_User: "",
            OrganizationGuid: "",
            ListPeople: [],
            txtSearch: '',
            DataView: [],
            ListSearch: [],
            ListView: [],

            txtName: "",
            refreshing: false,
            Data: null,
            test: '',
            Id_User: '',
            Countcheck: 0,
            DataInsert: [],
            UserSend: ""
        };
    }
    componentDidMount() {
        var guid = { ChatboxGroupGuid: this.props.ChatboxGroupGuid }
        API_Message.Messenger_AllAccountOfGroup(guid).then(res => {
            var _data = JSON.parse(res.data.data);
            this.setState({
                ListPeople: _data
            })
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
        var obj_userSend = { Id: this.props.ChatboxGroupGuid }
        API_Message.Messenger_UserSend(obj_userSend).then(ress => {
            var _UserSend = JSON.parse(ress.data.data).UserSend.split(';');
            this.setState({ UserSend: _UserSend });
        }).catch(error => {
            this.callBack()
            ErrorHandler.handle(error.data);
        });
        API.getProfile().then(rs => {
            this.setState({
                LoginName_User: rs.data.LoginName,
                FullName_User: rs.data.FullName,
                Id_User: rs.data.EmployeeGuid
            });
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
        this.Search_people()
    }
    componentWillReceiveProps(nextProps) {
    }
    updateSearch = search => {
        this.setState({ txtSearch: search });
        this.Search_people()
    }
    //Tìm kiếm người chat
    Search_people() {
        var _ListSearch = {
            txtSearch: this.state.txtSearch,
            DepartmentGuid: ""
        }
        API_Message.Messenger_Employees_Search(_ListSearch)
            .then(res => {
                var checklistAPI = JSON.parse(res.data.data);
                var checklist = this.state.DataView;
                for (let i = 0; i < checklistAPI.length; i++) {
                    checklistAPI[i].exist = false;
                    for (let j = 0; j < checklist.length; j++) {
                        if (checklistAPI[i].LoginName == checklist[j].LoginName) {
                            checklistAPI[i].check = true;
                        }
                    }
                }
                this.setState({
                    ListSearch: checklistAPI,
                })
                this.CustomListView();
            })
            .catch(error => {
                ErrorHandler.handle(error.data);
            });
    }
    CustomListView() {
        var _dataSearch = this.state.ListSearch;
        var _dataPeople = this.state.ListPeople;
        var _listView = [];
        for (let i = 0; i < _dataSearch.length; i++) {
            for (let j = 0; j < _dataPeople.length; j++) {
                if (_dataSearch[i].LoginName == _dataPeople[j].LoginName) {
                    _dataSearch[i].exist = true;

                }
            }
            _listView.push(_dataSearch[i])
        }
        this.setState({
            ListView: _listView
        })
    }

    loadMoreData() {
        this.Search_people()
    }
    onCheckBox(para, index) {
        var _countcheck = 0;
        var _list = this.state.ListView;
        _list[index].check = !_list[index].check;
        this.setState({
            ListView: _list
        });
        var _dataView = this.state.DataView;
        if (_list[index].check == true) {
            _dataView.push(
                {
                    LoginName: _list[index].LoginName,
                    EmployeeGuid: _list[index].EmployeeGuid
                });
        } else {
            for (let i = 0; i < _dataView.length; i++) {
                if (_dataView[i].LoginName === _list[index].LoginName) {
                    _dataView.splice(i, 1);
                }
            }
        }
        _countcheck = _dataView.length;
        this.setState({
            Countcheck: _countcheck,
            DataView: _dataView,
        })

    }
    CustomeListView = (item) => (
        <FlatList
            data={item}
            style={{ flex: 1 }}
            renderItem={({ item, index }) => (
                item.exist == false ?
                    <View style={{ fontSize: 11, fontFamily: Fonts.base.family }}>
                        <ListItem
                            subtitle={() => {
                                return (
                                    <View style={{ flexDirection: 'row', marginBottom: -10, marginTop: -30 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', }}>
                                            <View style={{ flex: 2, marginRight: 10 }}>
                                                <Thumbnail style={{}} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
                                            </View>
                                        </View>
                                        <View style={{ flex: 4, flexDirection: 'row' }}>
                                            <View style={{ flex: 6, justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 14 }}>{item.FullName}</Text>
                                            </View>
                                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                                <CheckBox
                                                    value={this.state.ListView[index].check}
                                                    onValueChange={() => this.onCheckBox(item.EmployeeGuid, index)}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                );
                            }}
                            onPress={() => this.onCheckBox(item.EmployeeGuid, index)}
                        />
                    </View>

                    :
                    <View style={{ fontSize: 11, fontFamily: Fonts.base.family }}>
                        <ListItem
                            subtitle={() => {
                                return (
                                    <View style={{ flexDirection: 'row', marginBottom: -10, marginTop: -30, opacity: 0.5 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', }}>
                                            <View style={{ flex: 2, marginRight: 10 }}>
                                                <Thumbnail style={{}} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
                                            </View>
                                        </View>
                                        <View style={{ flex: 4, flexDirection: 'row' }}>
                                            <View style={{ flex: 6, justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 14 }}>{item.FullName}</Text>
                                            </View>
                                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                                <CheckBox value={true} disabled={true} />
                                            </View>
                                        </View>
                                    </View>
                                );
                            }}
                        />
                    </View>
            )}
            onEndReached={() => this.loadMoreData()}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['black']}
                />
            }
        />
    )
    render() {
        return (
            <View style={styles.container}>
                <TabBar_Title
                    title={"Thêm thành viên"}
                    callBack={() => this.backToList()}
                />
                <Divider />
                <View style={{ flex: 1, marginTop: 5 }}>
                    <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 15, backgroundColor: "#CAE5E8", borderRadius: 15 }}>
                        <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', }}>
                            <TouchableWithoutFeedback
                                underlayColor="white">
                                <Icon name="search1" type="antdesign" size={14} />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={{ flex: 8 }}>
                            <TextInput
                                style={{ height: 50 }}
                                onChangeText={search => this.updateSearch(search)}
                                value={this.state.txtSearch}
                                placeholder={'Tìm họ và tên hoặc tên tài khoản'}
                            />
                        </View>
                        <View style={{ flex: 1, width: 45, height: 50, textAlign: 'center', justifyContent: 'center' }}>
                            {this.state.txtSearch.length > 0 ?
                                <TouchableWithoutFeedback
                                    onPress={() => this.onclickreturnSearch()}
                                    underlayColor="white">
                                    <Icon name="close" type="antdesign" size={14} />
                                </TouchableWithoutFeedback> : null}
                        </View>
                    </View>
                    {this.state.ListView ? this.CustomeListView(this.state.ListView) : null}
                    {this.state.Countcheck > 0 ?
                        <View style={{ flex: 0.18, backgroundColor: "#F2F2F2", borderRadius: 10 }}>
                            {(this.state.DataView && this.state.DataView.length > 0) ? this.state.DataView.map((item, index) =>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 4 }}>
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                            <View style={{ width: 820 }}>
                                                <FlatGrid
                                                    itemDimension={50}
                                                    items={this.state.DataView}
                                                    renderItem={({ item, index }) => (
                                                        <Thumbnail style={{}} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
                                                    )}
                                                />
                                            </View>
                                        </ScrollView>
                                    </View>
                                    <TouchableOpacity style={{ flex: 1, alignItems: "center", justifyContent: "center" }} onPress={() => this.submit()}>
                                        <Icon name="right" type="antdesign" color={"#00CCFF"} size={30} />
                                    </TouchableOpacity>
                                </View>
                            ) : null}
                        </View> : null
                    }
                </View>
            </View>
        );
    }
    backToList() {
        Actions.pop();
        Actions.refresh({
            moduleId: 'back',
            Data: {
                Name: this.props.Name, guid: this.props.ChatboxGroupGuid
            },
            ActionTime: (new Date()).getTime()
        });
    }
    onclickreturnSearch() {
        this.state.Search = '';
        this.updateSearch('')
    }
    //Load lại
    _onRefresh = () => {
        this.Search_people()
        this.setState({ refreshing: true });
    };
    //thêm mới
    submit = () => {
        controller.Messenger_UserSend(this.props.ChatboxGroupGuid, rs => {
            this.setState({ UserSend: rs }, () => {
                this.update();
            });
        })
    }
    update = () => {
        var strEmp = [];
        for (let i = 0; i < this.state.DataView.length; i++) {
            strEmp.push(this.state.DataView[i].LoginName);
        }
        let dataEdit = {
            ChatboxGroupGuid: this.props.ChatboxGroupGuid,
            Name: "",
            IsGroup: 1,
            detail: strEmp,
            updateOwend: []
        }
        let _deleteUser = [];
        var _edit = new FormData();
        _edit.append('edit', JSON.stringify(dataEdit));
        _edit.append('DeleteUser', JSON.stringify(_deleteUser));
        API_Message.Messenger_ChatboxGroup_Update(_edit).then(res => {
            var _rs = res.data;
            if (_rs.errorCode === 200) {
                var user = JSON.parse(_rs.data_v2);
                var _user = [];
                for (let i = 1; i < user.length; i++) {
                    _user.push({
                        L: user[i].LoginName,
                        F: user[i].FullName
                    })
                }
                var _text = {
                    S: "I",
                    D: _user
                };
                var objMessage = {
                    ChatboxGroupGuid: this.props.ChatboxGroupGuid,
                    Messager: JSON.stringify(_text),
                    Type: "I",
                    IsGroup: true,
                    ParentGuid: null
                };
                controller.sendComment(objMessage, [], this.state.UserSend, rs => {
                    if (rs === "") {
                        Actions.pop();
                        Actions.refresh({
                            moduleId: 'back',
                            Data: {
                                Name: this.props.Name, guid: this.props.ChatboxGroupGuid
                            },
                            ActionTime: (new Date()).getTime()
                        });
                    }
                });
            }
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(AddUserOfGroup_Component);
