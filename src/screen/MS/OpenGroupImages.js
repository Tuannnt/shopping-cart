import React, {Component, useRef, useEffect} from 'react';
import {connect} from 'react-redux';
import {
  Header,
  Icon,
  ListItem,
  SearchBar,
  Badge,
  Divider,
} from 'react-native-elements';
import {
  Text,
  View,
  Animated,
  Dimensions,
  Easing,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Image,
} from 'react-native';
import {Directions} from 'react-native-gesture-handler';
import {AppStyles, AppColors} from '@theme';
import {Actions} from 'react-native-router-flux';
import _ from 'lodash';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class OpenGroupImages extends Component {
  constructor(props) {
    super(props);
    var _data = props.data;
    this.state = {
      TypeSelect:
        props.TypeSelect !== undefined && props.TypeSelect !== null
          ? props.TypeSelect
          : 'single', // kiểu chọn nhiều hay chọn 1
      valueSelect:
        props.value !== undefined && props.value !== null ? props.value : null, //giá trị mặc định
      position: props.position, // hiển thị ở đâu?
      offsetY:
        props.position === 'top'
          ? new Animated.Value(props.data.length * -50)
          : props.position === 'bottom'
          ? new Animated.Value(props.data.length * 50 + 30)
          : '',
      isOpen: false,
      height:
        props.position === 'top'
          ? props.data.length * -110
          : props.position === 'bottom'
          ? props.data.length * 50 + 50
          : '',
      dataSubmit: null,
      data: _data,
      dataView: _data,
      NameMenu: props.nameMenu ? props.nameMenu : 'Chức năng',
      CheckAllValue: false,
    };

    props.eOpen(this.eOpen);
    this.actionClick = props.callback;
  }
  checkPosition = position => {
    switch (position) {
      case 'top':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      case 'bottom':
        this.setState({
          offsetY: new Animated.Value(props.data.length * 50 + 30),
          height: props.data.length * 50 + 30,
        });
        break;

      case 'left':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      case 'right':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      default:
        Alert.alert('NUMBER NOT FOUND');
    }
  };
  actionClick() {}

  componentDidMount() {
    this.ae(this.props.data, this.state.TypeSelect, this.state.valueSelect);
  }
  componentDidUpdate = prevProps => {
    // Typical usage (don't forget to compare props):
    if (!_.isEqual(this.props.data, prevProps.data)) {
      this.ae(this.props.data, this.state.TypeSelect, this.state.valueSelect);
    }
  };
  //#region Xử lý dữ liệu truyền vào
  ae(d, type, value) {
    switch (type) {
      case 'multiple':
        var itemselect = [];
        if (value !== null) {
          if (value.length > 0) {
            for (var i = 0; i < d.length; i++) {
              for (let y = 0; y < value.length; y++) {
                if (value[y] == d[i].value) {
                  d[i].check = true;
                  itemselect.push(d[i]);
                  break;
                } else {
                  d[i].check = false;
                }
              }
            }
          } else {
            for (var i = 0; i < d.length; i++) {
              d[i].check = false;
            }
            itemselect.push({text: '', value: null});
          }
        } else {
          for (var i = 0; i < d.length; i++) {
            d[i].check = false;
          }
          itemselect.push({text: '', value: null});
        }
        this.setState({dataView: d, dataSubmit: itemselect});
        this.actionClick(itemselect);
        break;
      case 'single':
        var itemselect = null;
        if (value !== null) {
          for (var i = 0; i < d.length; i++) {
            if (value == d[i].value) {
              d[i].check = true;
              itemselect = d[i];
            } else {
              d[i].check = false;
            }
          }
        } else {
          for (var i = 0; i < d.length; i++) {
            d[i].check = false;
            itemselect = {text: '', value: null};
          }
        }
        this.setState({dataView: d, dataSubmit: itemselect});
        this.actionClick(itemselect);
        break;
      default:
        break;
    }
  }
  //#endregion

  //#region  đóng cửa sổ
  eOpen = option => {
    if (option) {
      if (option.isChangeAction) {
        this.ae(option.data);
        this.setState({
          data: option.data,
        });
      }
    }
    if (!this.state.isOpen) {
      this.setState({
        isOpen: true,
      });
    } else {
      setTimeout(() => {
        this.setState({
          isOpen: false,
        });
      }, 500);
    }
    this.setState({EvenFromSearch: false});
    this.openAction();
  };
  //#endregion

  //#region mở cửa sổ
  openAction = () => {
    if (!this.state.isOpen) {
      Animated.timing(this.state.offsetY, {
        toValue: 0,
        // duration: 1000,
        // easing: Easing.ease
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this.state.offsetY, {
        toValue: this.state.height,
        // duration: 1000,
        // easing: Easing.ease
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };
  //#endregion

  //#region chọn nhiều chọn 1
  callBack_multiple = () => {
    var _dataview = this.state.dataView;
    var _submit = [];
    for (let z = 0; z < _dataview.length; z++) {
      if (_dataview[z].check == true) {
        _submit.push(_dataview[z]);
      }
    }
    this.setState({dataSubmit: _submit});
    this.actionClick(_submit);
    this.eOpen();
  };
  SaveValues = item => {
    if (this.state.TypeSelect == 'single') {
      var _submit = null;
      var _data = this.state.dataView;
      for (var i = 0; i < _data.length; i++) {
        if (_data[i].value == item.value) {
          _data[i].check = true;
        } else _data[i].check = false;
      }
      for (let z = 0; z < _data.length; z++) {
        if (_data[z].check == true) {
          _submit = _data[z];
          break;
        } else {
          _submit = {text: '', value: null};
        }
      }
      this.setState({dataSubmit: _submit, dataView: _data});
      this.actionClick(_submit);
      this.eOpen();
    } else {
      var _data = this.state.dataView;
      for (var i = 0; i < _data.length; i++) {
        if (_data[i].value == item.value) {
          _data[i].check = !item.check;
          break;
        }
      }
      this.setState({
        dataView: _data,
      });
    }
  };
  //#endregion

  //#region chọn tất cả
  CheckAllValue = () => {
    var data = this.state.dataView;
    var dataNew = [];
    for (let i = 0; i < data.length; i++) {
      data[i].check = true;
      dataNew.push(data[i]);
    }
    this.setState({
      CheckAllValue: true,
      dataView: dataNew,
    });
  };
  ClearAllValue = () => {
    var data = this.state.dataView;
    var dataNew = [];
    for (let i = 0; i < data.length; i++) {
      data[i].check = false;
      dataNew.push(data[i]);
    }
    this.setState({
      CheckAllValue: false,
      dataView: dataNew,
      dataSubmit: [],
    });
  };
  //#endregion

  //#region sự kiện load đế cuối trang
  LoadDateEnd(event) {
    console.log(
      '=====',
      event.nativeEvent.contentOffset.y,
      'X',
      this.state.dataView.length * 40 - 310,
    );
    var y = event.nativeEvent.contentOffset.y;
    var heightView = this.state.dataView.length * 40 - 310;
    if (y >= heightView) {
      // if (this.state.paging == true) {
      //     this.callback_Paging(this.Paging)
      // }
    }
  }
  //#endregion

  render() {
    return this.state && this.state.isOpen ? (
      <TouchableOpacity
        style={[
          this.state.position === 'top'
            ? styles.positionTop
            : this.state.position === 'bottom'
            ? styles.positionBottom
            : '',
          {
            width: DRIVER.width,
            position: 'absolute',
            backgroundColor: 'rgba(52, 52, 52, 0.5)',
            height: DRIVER.height,
            zIndex: 9999,
          },
        ]}
        onPress={() => {
          this.eOpen();
        }}>
        <View
          style={[
            this.state.position === 'top'
              ? {top: 0}
              : this.state.position === 'bottom'
              ? {bottom: 0}
              : '',
            {position: 'absolute', width: DRIVER.width},
          ]}>
          <Animated.View
            style={{
              backgroundColor: 'rgba(52, 52, 52, 0.5)',
              transform: [{translateY: this.state.offsetY}],
              borderRadius: 10,
              bottom: 0,
            }}>
            <KeyboardAvoidingView
              behavior={Platform.OS === 'ios' ? 'padding' : null}
              style={{flex: 1}}>
              <View
                style={{
                  flexDirection: 'column',
                  backgroundColor: 'white',
                  borderTopColor: '#CED0CE',
                  borderTopWidth: 1,
                  bottom: 0,
                  maxHeight: DRIVER.height * 0.8,
                  borderRadius: 10,
                  opacity: 1,
                }}>
                <ScrollView
                  style={{padding: 10, marginBottom: 20}}
                  ref={ref => {
                    this.myScroll = ref;
                  }}
                  onMomentumScrollEnd={event => this.LoadDateEnd(event)}>
                  {this.state.dataView &&
                    this.state.dataView.length > 0 &&
                    this.state.dataView.map((item, i) => {
                      return (
                        <TouchableOpacity
                          key={i}
                          onPress={() => {
                            this.SaveValues(item);
                          }}
                          style={{
                            flexDirection: 'row',
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderBottomColor: '#eff0f1',
                            borderBottomWidth: 1,
                            height: 110,
                          }}>
                          <View style={{flex: 2, justifyContent: 'center'}}>
                            <Image
                              style={{width: 100, height: 100}}
                              source={{uri: item.image}}
                            />
                          </View>
                          <View style={{flex: 3, justifyContent: 'center'}}>
                            <Text style={AppStyles.h4}>{item.text}</Text>
                            <Text style={AppStyles.Textdefault}>
                              {item.count}
                            </Text>
                          </View>
                          <View style={{flex: 1, justifyContent: 'center'}}>
                            {item.check && item.check == true ? (
                              <Icon
                                color={AppColors.gray}
                                name="checkcircle"
                                type="antdesign"
                                color={AppColors.ColorEdit}
                                size={20}
                              />
                            ) : null}
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                </ScrollView>
              </View>
            </KeyboardAvoidingView>
          </Animated.View>
        </View>
      </TouchableOpacity>
    ) : null;
  }
}
const styles = StyleSheet.create({
  positionTop: {
    top: 80,
  },
  positionBottom: {
    bottom: -10,
  },
});
export default OpenGroupImages;
