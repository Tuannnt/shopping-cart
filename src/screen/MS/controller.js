import {API, API_HR, API_Message} from '@network';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '@utils';
import Toast from 'react-native-simple-toast';
const Sound = require('react-native-sound');
export default {
  getProfile(callback) {
    API.getProfile()
      .then(rs => {
        callback(rs.data);
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Đã xảy ra lỗi khi lấy dữ liệu người đăng nhập',
        );
        console.log(error);
      });
  },
  Messenger_RefreshList(list, Comment, callback) {
    var CommentString = JSON.parse(Comment);
    if (CommentString.Code == 'CHAT') {
      var obj = {
        ChatboxGroupGuid: CommentString.ChatboxGroupGuid,
      };
      API_Message.Messenger_RefreshList(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            var data = JSON.parse(res.data.data);
            if (
              data.ListEmpGroup &&
              data.ListEmpGroup != '' &&
              data.ListEmpGroup != null
            ) {
              data.ListEmpGroup = data.ListEmpGroup.split(';');
            }
            var view = [];
            view.push(data);
            for (let i = 0; i < list.length; i++) {
              if (list[i].ChatboxGroupGuid !== data.ChatboxGroupGuid) {
                view.push(list[i]);
              }
            }
            var whoosh = new Sound('huawei.mp3', Sound.MAIN_BUNDLE, error => {
              if (error) {
                console.log('failed to load the sound', error);
                return;
              }
              whoosh.play(success => {
                if (success) {
                } else {
                }
              });
            });
            callback(view);
          }
        })
        .catch(error => {
          Alert.alert('Thông báo', 'Đã xảy ra lỗi Messenger_RefreshList');
          console.log(error);
        });
    }
  },
  Messenger_UserOfChatbox_ListAll(staticparam, list, callback) {
    staticparam.NumberPage++;
    API_Message.Messenger_UserOfChatbox_ListAll(staticparam)
      .then(res => {
        console.log(res);
        var _listDB = JSON.parse(res.data.data).data;
        for (let i = 0; i < _listDB.length; i++) {
          if (
            _listDB[i].ListEmpGroup &&
            _listDB[i].ListEmpGroup != '' &&
            _listDB[i].ListEmpGroup != null
          ) {
            _listDB[i].ListEmpGroup = _listDB[i].ListEmpGroup.split(';');
          } else {
            _listDB[i].ListEmpGroup = null;
          }
          list.push(_listDB[i]);
        }
        callback(list);
      })
      .catch(error => {
        Alert.alert('Thông báo', 'Đã xảy ra lỗi lấy danh sách');
        console.log(error);
      });
  },
  Messenger_Search(search, callback) {
    var _ListSearch = {
      txtSearch: search,
      DepartmentGuid: '',
    };
    API_Message.Messenger_Search(_ListSearch)
      .then(res => {
        var _data = JSON.parse(res.data.data);
        for (let i = 0; i < _data.length; i++) {
          if (_data[i].ListEmpGroup !== '') {
            _data[i].ListEmpGroup = _data[i].ListEmpGroup.split(';');
          }
        }
        callback(_data);
      })
      .catch(error => {
        Alert.alert('Thông báo', 'Đã xảy ra lỗi lấy danh sách tìm kiếm');
        console.log(error);
      });
  },
  Messenger_FavoritesAction(val, guid, callback) {
    var obj = {
      LoginName: val,
      RecordGuid: guid,
    };
    API_Message.Messenger_FavoritesAction(obj)
      .then(res => {
        if (res.data.Error === false) {
          callback(res.data.Error);
        }
      })
      .catch(error => {
        Alert.alert('Thông báo', 'Đã xảy ra lỗi lấy danh sách tìm kiếm');
        console.log(error);
      });
  },
  Messenger_ActiveRead(guid) {
    var obj = {RecordGuid: guid};
    API_Message.Messenger_ActiveRead(obj)
      .then(res => {})
      .catch(error => {
        Alert.alert('Thông báo', 'Đã xảy ra lỗi Messenger_ActiveRead');
        console.log(error);
      });
  },
  onViewItem(para) {
    if (para.Message_Status == 'False') {
      var obj_Update = {
        ChatboxGroupGuid: para.ChatboxGroupGuid,
        Status: 0,
      };
      API_Message.Messenger_ChatboxMessages_Update(obj_Update)
        .then(res => {
          if (JSON.parse(res.data.errorCode) === 200) {
            if (
              para.Name == null ||
              para.Name == '' ||
              para.Name == undefined
            ) {
              Actions.viewItemMessange({
                RecordGuid: para.ChatboxGroupGuid,
                Name: '',
                FullName: para.FullName,
                IsGroup: para.IsGroup,
                LoginName: para.EmpLoginName,
              });
            } else {
              Actions.viewItemMessange({
                RecordGuid: para.ChatboxGroupGuid,
                Name: para.Name,
                IsGroup: para.IsGroup,
                LoginName: para.EmpLoginName,
              });
            }
          } else {
            alert(JSON.parse(res.data.message));
          }
        })
        .catch(error => {
          ErrorHandler.handle(error.data);
        });
    } else {
      if (para.Name == null || para.Name == '' || para.Name == undefined) {
        Actions.viewItemMessange({
          RecordGuid: para.ChatboxGroupGuid,
          Name: '',
          FullName: para.FullName,
          IsGroup: para.IsGroup,
          LoginName: para.EmpLoginName,
        });
      } else {
        Actions.viewItemMessange({
          RecordGuid: para.ChatboxGroupGuid,
          Name: para.Name,
          IsGroup: para.IsGroup,
          LoginName: para.EmpLoginName,
        });
      }
    }
  },
  oncheckItem(item) {
    if (item.Group === true) {
      Actions.viewItemMessange({
        RecordGuid: item.Guid,
        Name: item.Name,
        IsGroup: item.Group,
        LoginName: item.LoginName,
      });
    } else {
      var obj = {LoginName: item.LoginName};
      API_Message.Messenger_Check_UserOfChatbox(obj)
        .then(res => {
          if (JSON.parse(res.data.data) !== null) {
            if (JSON.parse(res.data.data).ChatboxGroupGuid !== undefined) {
              // this.ActiveRead(JSON.parse(res.data.data).ChatboxGroupGuid);
              var obj_Update = {
                ChatboxGroupGuid: JSON.parse(res.data.data).ChatboxGroupGuid,
                Status: 0,
              };
              API_Message.Messenger_ChatboxMessages_Update(obj_Update)
                .then(rs => {
                  if (JSON.parse(rs.data.errorCode) === 200) {
                    if (
                      JSON.parse(res.data.data).Name == null ||
                      JSON.parse(res.data.data).Name == '' ||
                      JSON.parse(res.data.data).Name == undefined
                    ) {
                      Actions.viewItemMessange({
                        RecordGuid: JSON.parse(res.data.data).ChatboxGroupGuid,
                        Name: '',
                        FullName: item.Name,
                        IsGroup: JSON.parse(res.data.data).IsGroup,
                        LoginName: item.LoginName,
                      });
                    } else {
                      Actions.viewItemMessange({
                        RecordGuid: JSON.parse(res.data.data).ChatboxGroupGuid,
                        Name: JSON.parse(res.data.data).Name,
                        IsGroup: JSON.parse(res.data.data).IsGroup,
                        LoginName: item.LoginName,
                      });
                    }
                  } else {
                    alert(JSON.parse(rs.data.message));
                  }
                })
                .catch(error => {
                  ErrorHandler.handle(error.data);
                });
            } else {
              Actions.viewItemMessange({
                LoginName: item.LoginName,
                Name: '',
                FullName: item.Name,
                IsGroup: 'False',
                LoginName: item.LoginName,
              });
            }
          } else {
            Actions.viewItemMessange({
              LoginName: item.LoginName,
              Name: '',
              FullName: item.Name,
              IsGroup: 'False',
              LoginName: item.LoginName,
            });
          }
        })
        .catch(error => {
          ErrorHandler.handle(error.data);
        });
    }
  },
  //#region custom
  customTime(DataTime) {
    try {
      if (DataTime != null && DataTime != '') {
        var SplitTime = String(DataTime).split(' ');
        var SplitDateNow =
          new Date().getMonth() +
          1 +
          '/' +
          new Date().getDate() +
          '/' +
          new Date().getFullYear();
        if (SplitTime[0] === SplitDateNow) {
          //định dạng giờ
          if (SplitTime[2] == 'PM') {
            var SplitTime_v2 = SplitTime[1].split(':');
            var HH = (parseInt(SplitTime_v2[0]) + 12).toString();
            if (HH == 24) {
              HH = '00';
            }
            var mm = '';
            if (SplitTime_v2[1].length < 2) {
              mm = '0' + SplitTime_v2[1];
            } else {
              mm = SplitTime_v2[1];
            }
            var ss = '';
            if (SplitTime_v2[2].length < 2) {
              ss = '0' + SplitTime_v2[2];
            } else {
              ss = SplitTime_v2[2];
            }
          } else {
            var SplitTime_v2 = SplitTime[1].split(':');
            var HH = SplitTime_v2[0];
            var mm = '';
            if (SplitTime_v2[1].length < 2) {
              mm = '0' + SplitTime_v2[1];
            } else {
              mm = SplitTime_v2[1];
            }
            var ss = '';
            if (SplitTime_v2[2].length < 2) {
              ss = '0' + SplitTime_v2[2];
            } else {
              ss = SplitTime_v2[2];
            }
          }

          var HH_Time = new Date().getHours().toString() - HH;
          var mm_Time = new Date().getMinutes().toString() - mm;
          var ss_Time = new Date().getSeconds().toString() - ss;
          if (HH_Time > 0) {
            return (HH_Time + ' giờ trước').toString();
          }
          if (mm_Time > 0) {
            return (mm_Time + ' phút trước').toString();
          }
          if (ss_Time > 0) {
            return (ss_Time + ' giây trước').toString();
          }
        } else {
          var date = SplitTime[0].split('/');
          var yyyy = new Date().getFullYear() - date[2];
          var MM = new Date().getMonth() + 1 - date[0];
          var dd = new Date().getDate() - date[1];
          if (yyyy > 0) {
            return (yyyy + ' năm trước').toString();
          }
          if (MM > 0) {
            return (MM + ' tháng trước').toString();
          }
          if (dd > 0) {
            if (dd == 1) {
              return 'Hôm qua'.toString();
            } else {
              return (dd + ' ngày trước').toString();
            }
          }
        }
      } else {
        return '';
      }
    } catch (error) {
      return '';
    }
  },
  checkMessage(type, str, i) {
    try {
      if (str !== null && str !== '') {
        if (type === 'A') {
          return 'đã gửi icon';
        } else if (type === 'L') {
          return 'đã gửi đường đẫn liên kết';
        } else if (type === 'P') {
          var splittext = JSON.parse(str);
          if (splittext.Module === 'TM') {
            return 'Tạo mới phiếu công việc';
          } else if (splittext.Module === 'ApplyOutsides') {
            return 'Tạo mới phiếu đi công tác';
          } else if (splittext.Module === 'RegisterCars') {
            return 'Tạo mới phiếu đăng ký xe';
          } else if (splittext.Module === 'Advances') {
            return 'Tạo mới phiếu tạm ứng';
          } else if (splittext.Module === 'ApplyLeaves') {
            return 'Tạo mới phiếu nghỉ phép';
          } else if (splittext.Module === 'TicketRequestsImport') {
            return 'Tạo mới đề nghị nhập';
          } else if (splittext.Module === 'TicketChecking') {
            return 'Tạo mới kiểm kê kho';
          } else {
            return 'Tạo mới một phiếu';
          }
        } else if (type === 'I') {
          return 'Cập nhật thông tin';
        } else {
          var splitted = str.split(' ');
          var strreturn = '';
          var j = 0;
          for (var a = 0; a < splitted.length; a++) {
            j = j + 1;
            if (j == i) {
              strreturn = strreturn + ' ' + splitted[a] + '...';
              break;
            } else {
              strreturn = strreturn + ' ' + splitted[a];
            }
          }
          return strreturn.trim();
        }
      } else {
        return 'Đã gửi file đính kèm';
      }
    } catch (error) {
      return 'Không có tin nhắn';
    }
  },
  //#endregion

  Loadata(isGroup, guid, loginName, page, list, callback) {
    FuncCommon.Data_Offline(async on => {
      if (on) {
        if (isGroup === true) {
          var data = {
            Keyword: '',
            RecordGuid: guid,
            Take: 20,
            Page: page,
          };
          API_Message.GetIdChatGroup(data)
            .then(res => {
              var _list_v1 = JSON.parse(res.data.data).reverse();
              for (let i = 0; i < _list_v1.length; i++) {
                list.push(_list_v1[i]);
              }
              callback(list);
            })
            .catch(error => {
              console.log(error);
            });
        } else {
          var data = {
            LoginName: loginName,
            Keyword: '',
            Page: page,
          };
          API_Message.GetIdChat(data)
            .then(res => {
              var _list_v1 = JSON.parse(res.data.data).reverse();
              if (page === 0) {
                for (let i = 0; i < _list_v1.length; i++) {
                  list.push(_list_v1[i]);
                }
              } else {
                for (let i = 0; i < _list_v1.length; i++) {
                  list.unshift(_list_v1[i]);
                }
              }
              callback(list);
            })
            .catch(error => {
              console.log(error);
            });
        }
      }
    });
  },
  Messenger_UserSend(data, callback) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        Toast.showWithGravity('Yêu cầu kết nối mạng', Toast.LONG, Toast.CENTER);
        return;
      }
    });
    var obj_userSend = {Id: data};
    API_Message.Messenger_UserSend(obj_userSend)
      .then(ress => {
        console.log(ress);
        var _UserSend = JSON.parse(ress.data.data).UserSend.split(';');
        callback(_UserSend);
      })
      .catch(error => {
        console.log(error);
      });
  },
  RecallMess(data, callback) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        Toast.showWithGravity('Yêu cầu kết nối mạng', Toast.LONG, Toast.CENTER);
        return;
      }
    });
    API_Message.RecallMess(data)
      .then(res => {
        callback(res);
      })
      .catch(error => {
        console.log(error);
      });
  },
  EventKeyCode(guid, callback) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        Toast.showWithGravity('Yêu cầu kết nối mạng', Toast.LONG, Toast.CENTER);
        return;
      }
    });
    var obj = {ChatboxGroupGuid: guid};
    API_Message.GetUserChatForTags(obj)
      .then(res => {
        if (res.data.Error === false) {
          callback(res.data.Object);
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
  SendNotifify(guid, comment, list) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        Toast.showWithGravity('Yêu cầu kết nối mạng', Toast.LONG, Toast.CENTER);
        return;
      }
    });
    var obj = {
      Head:
        'Bạn được nhắc đến bởi ' +
        global.__appSIGNALR.SIGNALR_object.USER.FullName,
      RecordGuid: guid,
      Subject: comment,
      Users: list,
    };
    API_Message.SendNotifify(obj)
      .then(rs => {
        if (rs.data.errorCode !== 200) {
          Toast.showWithGravity(
            'Có lỗi khi tag thành viên',
            Toast.LONG,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
  sendComment(objMessage, ListImg, UserSend, callback) {
    var fd = new FormData();
    fd.append('insert', JSON.stringify(objMessage));
    for (var i = 0; i < ListImg.length; i++) {
      fd.append(ListImg[i].name, ListImg[i]);
    }
    API_Message.Messenger_ChatboxMessages_Insert(fd)
      .then(res => {
        var _data = JSON.parse(JSON.parse(res.data.data));
        var _comment = {
          ..._data,
          Code: 'CHAT',
        };
        var obj = {};
        obj.LoginName = global.__appSIGNALR.SIGNALR_object.USER.LoginName;
        obj.OrganizationGuid =
          global.__appSIGNALR.SIGNALR_object.USER.OrganizationGuid;
        obj.ToUser = UserSend;
        obj.DataJson = JSON.stringify(_comment);
        global.__appSIGNALR.sendMess(obj);
        callback('');
      })
      .catch(error => {
        //Toast.showWithGravity("Có lỗi khi thêm mới tin nhắn", Toast.LONG, Toast.CENTER);
        console.log(error);
        Actions.pop();
        Actions.refresh({
          moduleId: 'back',
          Data: {
            RecordGuid: this.props.guid,
            Name: this.state.Name,
            IsGroup: this.state.IsGroup,
          },
          ActionTime: new Date().getTime(),
        });
      });
  },
};
