import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert,
    TextInput,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import { Icon, Badge, Header } from 'react-native-elements';
import { API_HR, API_Message, API } from '../../network';
import { Thumbnail } from 'native-base';
import { AppStyles, AppColors } from '@theme';
import TabBar_Title from "../component/TabBar_Title";
import { listCombobox } from './listCombobox';
import { controller } from './controller';
import LoadingComponent from '../component/LoadingComponent';
import Toast from 'react-native-simple-toast';
const styles = StyleSheet.create({
    containerfull: {
        flex: 1,
        elevation: 2,
        flexDirection: "column"
    },
    container: {
        borderTopWidth: 0.5,
        borderTopColor: '#CCCCCC',
        paddingLeft: 10,
        backgroundColor: '#FFF',
        flexDirection: 'column'
    },
    TouchableOpacityGroup: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomWidth: 0.5,
        borderBottomColor: '#CCCCCC'
    }
});
class Information_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            Image_EmpGuid: null,
            LoginName: "",
            IsGroup: false,
            ListAccount: [],
            CountDataAccount: 0,
            Name: '',
            EventEditName: false,
            LoginName_User: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
        };
    }
    componentDidMount() {
        this.setState({ Name: this.props.Name });
        var guid = { ChatboxGroupGuid: this.props.guid }
        API_Message.Messenger_Information_CheckGroup(guid).then(rs => {
            var dataJson = JSON.parse(rs.data.data);
            if (dataJson.IsGroup === true) {
                API_Message.Messenger_AllAccountOfGroup(guid).then(res => {
                    var _data = JSON.parse(res.data.data);
                    var _listAccount = [];
                    var _list = [];
                    for (let i = 0; i < _data.length; i++) {
                        if (_data[i].LoginName == _data[i].LoginNameOwned) {
                            _data[i].CheckOwned = true;
                            _listAccount.push(_data[i])
                        }
                    }
                    for (let j = 0; j < _data.length; j++) {
                        if (_data[j].LoginName != _data[j].LoginNameOwned) {
                            _data[j].CheckOwned = false;
                            _listAccount.push(_data[j])
                        }
                    }
                    for (let y = 0; y < _listAccount.length; y++) {
                        if (y < 4) {
                            _list.push(_listAccount[y])
                        }
                    }
                    this.setState({
                        IsGroup: dataJson.IsGroup,
                        ListAccount: _list,
                        CountDataAccount: _data.length,
                        loading: false
                    })
                }).catch(error => {
                    ErrorHandler.handle(error.data);
                });
            } else {
                this.setState({
                    Image_EmpGuid: dataJson.EmployeeGuid,
                    LoginName: dataJson.LoginName,
                    IsGroup: dataJson.IsGroup,
                    loading: false,
                })
            }
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.moduleId !== null) {
            this.setState({ loading: true });
            this.setState({ Name: nextProps.Data.Name });
            var guid = { ChatboxGroupGuid: nextProps.Data.guid }
            API_Message.Messenger_Information_CheckGroup(guid).then(rs => {
                var dataJson = JSON.parse(rs.data.data);
                if (dataJson.IsGroup === true) {
                    API_Message.Messenger_AllAccountOfGroup(guid).then(res => {
                        var _data = JSON.parse(res.data.data);
                        var _listAccount = [];
                        var _list = [];
                        for (let i = 0; i < _data.length; i++) {
                            if (_data[i].LoginName == _data[i].LoginNameOwned) {
                                _data[i].CheckOwned = true;
                                _listAccount.push(_data[i])
                            }
                        }
                        for (let j = 0; j < _data.length; j++) {
                            if (_data[j].LoginName != _data[j].LoginNameOwned) {
                                _data[j].CheckOwned = false;
                                _listAccount.push(_data[j])
                            }
                        }
                        for (let y = 0; y < _listAccount.length; y++) {
                            if (y < 4) {
                                _list.push(_listAccount[y])
                            }
                        }
                        this.setState({
                            IsGroup: dataJson.IsGroup,
                            ListAccount: _list,
                            CountDataAccount: _data.length,
                            loading: false,
                        })
                    }).catch(error => {
                        ErrorHandler.handle(error.data);
                    });
                } else {
                    this.setState({
                        Image_EmpGuid: dataJson.EmployeeGuid,
                        LoginName: dataJson.LoginName,
                        IsGroup: dataJson.IsGroup,
                        loading: false,
                    })
                }
            }).catch(error => {
                ErrorHandler.handle(error.data);
            });
        }
    }
    ViewItem = () => (
        <View>
            <View style={styles.container}>
                <TouchableOpacity style={{ flexDirection: 'row', marginBottom: 15 }} onPress={() => this.ToViewItemAcount(this.state.Image_EmpGuid)}>
                    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center', marginTop: 10 }}>
                        <Thumbnail source={API_HR.GetPicApplyLeaves(this.state.Image_EmpGuid)} />
                    </View>
                    <View style={{ flex: 6 }}>
                        <Text style={[AppStyles.headerStyle, { fontWeight: 'bold', marginTop: 10, fontSize: 16, color: 'black' }]}>{this.props.Name}</Text>
                        <Text style={[AppStyles.headerStyle, { marginTop: 3, fontSize: 12, color: '#808080' }]}>Thông tin cá nhân</Text>
                    </View>
                    <TouchableOpacity style={{ flex: 2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 10 }} onPress={() => this.ToViewItemAcount(this.state.Image_EmpGuid)}>
                        <Text style={{ fontSize: 12, color: '#3399FF' }}>Xem thêm </Text>
                        <Icon name={'chevron-thin-right'} type='entypo' color='#3399FF' size={10} />
                    </TouchableOpacity>
                </TouchableOpacity>
                <View>
                    <TouchableOpacity style={{ flexDirection: 'row', borderBottomWidth: 0.5, paddingVertical: 5 }} onPress={() => this.ToListImg()}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10 }}>
                            <Icon name={"image"} type={"feather"} size={30} color={AppColors.gray} />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Text style={{ paddingTop: 15, paddingBottom: 10 }}>Ảnh, link, file đã gửi</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', paddingRight: 10 }}>
                            <Icon name={"chevron-right"} type={"feather"} size={20} color={AppColors.gray} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.TouchableOpacityGroup} onPress={() => this.ToAddGroup(this.state.LoginName, this.state.Image_EmpGuid)}>
                        <Text style={{ fontSize: 14 }}>Tạo nhóm với {this.props.Name}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.TouchableOpacityGroup} onPress={() => this.ToViewGroupByLGN(this.state.LoginName)}>
                        <View style={{ flex: 4 }}>
                            <Text style={{ fontSize: 14 }}>Xem nhóm chung</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 10 }}>
                            <Icon name={'chevron-thin-right'} type='entypo' color='#CCCCCC' size={10} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            {/* <View style={{ height: 10 }}></View>
            <View style={styles.container}>
                <TouchableOpacity style={styles.TouchableOpacityGroup} onPress={() => this.ToViewItemAcount()}>
                    <View style={{ flex: 4 }}>
                        <Text style={{ fontSize: 14, color: 'red' }}>Xoá tin nhắn</Text>
                    </View>
                </TouchableOpacity>
            </View> */}
        </View>
    )
    ViewGroup = () => (
        <View>
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon name="group" iconStyle={{ color: "#888888" }} type="font-awesome" size={30} />
                    </View>
                    {this.state.EventEditName == false ?
                        <View style={{ flex: 7, flexDirection: 'row', marginBottom: 15, marginTop: 20 }}>
                            <View style={{ flex: 6, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                                <Text style={[AppStyles.headerStyle, { fontWeight: 'bold', fontSize: 16, color: 'black' }]}>{this.state.Name}</Text>
                            </View>
                            <TouchableOpacity style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }} onPress={() => this.setState({ EventEditName: true })}>
                                <Icon name="edit" type="antdesign" color={'blue'} size={20} />
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{ flex: 7, flexDirection: 'row', marginBottom: 3, marginTop: 3 }}>
                            <View style={{ flex: 6, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', borderBottomColor: "#00CCFF", borderBottomWidth: 1 }}>
                                <TextInput
                                    style={[{ fontWeight: 'bold', fontSize: 16, color: 'black', width: '100%' }]}
                                    onChangeText={txt => this.updateNameGroup(txt)}
                                    value={this.state.Name}
                                    placeholder={'Đặt tên nhóm'}
                                />
                            </View>
                            <TouchableOpacity style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }} onPress={() => this.EditnameGroup()}>
                                <Icon name="check" type="antdesign" color={'blue'} size={20} />
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            </View>
            <View style={{ height: 10 }}></View>
            <View style={styles.container}>
                <TouchableOpacity style={{ flexDirection: 'row', borderBottomWidth: 0.5, paddingVertical: 5 }} onPress={() => this.ToListImg()}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10 }}>
                        <Icon name={"image"} type={"feather"} size={30} color={AppColors.gray} />
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ paddingTop: 15, paddingBottom: 10 }}>Ảnh, link, file đã gửi</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', paddingRight: 10 }}>
                        <Icon name={"chevron-right"} type={"feather"} size={20} color={AppColors.gray} />
                    </View>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', borderBottomWidth: 0.5 }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ fontSize: 12, color: '#3399FF', paddingTop: 15, paddingBottom: 10 }}>Thành viên ({this.state.CountDataAccount})</Text>
                    </View>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }} onPress={() => this.ToAddUserOfGroup()}>
                        <Text style={{ fontSize: 12, color: '#808080', paddingTop: 15, paddingBottom: 10, marginRight: 10 }}>+ Thêm thành viên</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 20, paddingBottom: 20, borderBottomWidth: 0.5, borderBottomColor: '#CCCCCC' }}>
                    {(this.state.ListAccount && this.state.ListAccount.length > 0) ?
                        this.state.ListAccount.length == 2 ?
                            this.state.ListAccount.map((item, index) => (
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Thumbnail style={{}} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
                                    {item.CheckOwned == true ?
                                        <Badge
                                            badgeStyle={{ backgroundColor: '#888888', opacity: 0.8 }}
                                            value={<Icon iconStyle={{ color: '#FFFF00' }} size={11} name='key' type='simple-line-icon' />}
                                            containerStyle={{ position: 'absolute', right: 55, bottom: 18 }}
                                        /> : null}
                                    <Text style={{ fontSize: 10, marginTop: 5 }}>{item.FullName}</Text>

                                </View>
                            ))
                            :
                            this.state.ListAccount.length == 3 ?
                                this.state.ListAccount.map((item, index) => (
                                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <Thumbnail style={{}} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
                                        {item.CheckOwned == true ?
                                            <Badge
                                                badgeStyle={{ backgroundColor: '#888888', opacity: 0.8 }}
                                                value={<Icon iconStyle={{ color: '#FFFF00' }} size={11} name='key' type='simple-line-icon' />}
                                                containerStyle={{ position: 'absolute', right: 30, bottom: 18 }}
                                            /> : null}
                                        <Text style={{ fontSize: 10, marginTop: 5 }}>{item.FullName}</Text>

                                    </View>
                                ))
                                : this.state.ListAccount.length == 4 ?
                                    this.state.ListAccount.map((item, index) => (
                                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <Thumbnail style={{}} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
                                            {item.CheckOwned == true ?
                                                <Badge
                                                    badgeStyle={{ backgroundColor: '#888888', opacity: 0.8 }}
                                                    value={<Icon iconStyle={{ color: '#FFFF00' }} size={11} name='key' type='simple-line-icon' />}
                                                    containerStyle={{ position: 'absolute', right: 12, bottom: 18 }}
                                                /> : null}
                                            <Text style={{ fontSize: 10, marginTop: 5 }}>{item.FullName}</Text>

                                        </View>
                                    ))
                                    : null
                        : null
                    }
                </View>
                <TouchableOpacity style={{ flexDirection: 'row', height: 40, alignItems: 'center', justifyContent: 'flex-start' }} onPress={() => this.GotoViewDetailAllUser(this.props.guid, false)}>
                    <Text style={{ fontSize: 14 }}>Xem tất cả</Text>
                </TouchableOpacity>
            </View>
            <View style={{ height: 10 }}></View>
            <View style={styles.container}>
                <TouchableOpacity style={styles.TouchableOpacityGroup} onPress={() => this.RemoveUserName()}>
                    <Text style={{ fontSize: 14, color: 'red' }}>Rời nhóm</Text>
                </TouchableOpacity>
                {this.state.LoginName_User == this.state.ListAccount[0].LoginNameOwned ?
                    <TouchableOpacity style={styles.TouchableOpacityGroup} onPress={() => this.AlertRemoveGroup()}>
                        <Text style={{ fontSize: 14, color: 'red' }}>Xoá nhóm</Text>
                    </TouchableOpacity>
                    : null
                }
            </View>
        </View>
    )
    render() {
        return (
            this.state.loading !== true ?
                <View style={styles.containerfull}>
                    <TabBar_Title
                        title={'Tuỳ chọn'}
                        callBack={() => this.backToList()}
                    />
                    {this.state.IsGroup == false ? this.ViewItem() : this.ViewGroup()}
                </View>
                :
                <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    updateNameGroup = txt => {
        this.setState({ Name: txt });
    }
    backToList() {
        Actions.pop();
        Actions.refresh({
            moduleId: 'back',
            Data: {
                RecordGuid: this.props.guid,
                Name: this.state.Name,
                IsGroup: this.state.IsGroup
            },
            ActionTime: (new Date()).getTime()
        });
    }
    ToAddUserOfGroup() {
        Actions.addUserOfGroup({ ChatboxGroupGuid: this.props.guid, Name: this.state.Name });
    }
    ToListImg() {
        Actions.ListImage({ ChatboxGroupGuid: this.props.guid });
    }
    ToViewItemAcount(item) {
        Actions.empviewitem({ EmployeeGuid: item });
    }
    onPressTestMS() {
        Actions.TestMS();
    }
    ToAddGroup(loginname, employeeGuid) {
        let _lgn = [{ LoginName: loginname, EmployeeGuid: employeeGuid }];
        Actions.AddGroup({ ListAddAcount: _lgn, moduleId: 'Information' });
    }
    ToViewGroupByLGN(loginname) {
        Actions.viewAllGroupByLoginName({ LoginName: loginname })
    }
    GotoViewDetailAllUser(guid, check) {
        Actions.viewDetailAllUser({ Guid: guid, CheckRemove: check, Name: this.state.Name, IsGroup: this.state.IsGroup })
    }
    EditnameGroup() {
        this.setState({ EventEditName: false })
        let dataEdit = {
            ChatboxGroupGuid: this.props.guid,
            Name: this.state.Name,
            IsGroup: 1,
            detail: [],
            updateOwend: []
        }
        let _deleteUser = [];
        var _edit = new FormData();
        _edit.append('edit', JSON.stringify(dataEdit));
        _edit.append('DeleteUser', JSON.stringify(_deleteUser));
        API_Message.Messenger_ChatboxGroup_Update(_edit).then(res => {
            var _text = {
                S: "RN",
                D: [{
                    L: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
                    F: global.__appSIGNALR.SIGNALR_object.USER.FullName,
                    M: this.state.Name
                }]
            };
            this.InsertChatBox(_text);
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }
    RemoveUserName() {
        var _ListUserName = this.state.ListAccount;
        if (global.__appSIGNALR.SIGNALR_object.USER.LoginName == _ListUserName[0].LoginNameOwned) {
            Alert.alert(
                //title
                'Chuyển quyền quản trị',
                //body
                'Bạn là người quản trị nhóm.Nếu bạn muốn rời khỏi nhóm thì hãy trao quyền quản trị cho người khác.',
                [
                    { text: 'Chọn người', onPress: () => this.GotoViewDetailAllUser(this.props.guid, true) },
                    { text: 'Đóng', onPress: () => console.log('No Pressed') },
                ],
                { cancelable: true }
            );
        } else {
            this.setState({ EventEditName: false })
            Alert.alert(
                //title
                'Xác nhận',
                //body
                'Bạn chắc chắn muốn rời khỏi nhóm',
                [
                    { text: 'Đồng ý', onPress: () => this.delete() },
                    { text: 'Đóng', onPress: () => console.log('No Pressed') },
                ],
                { cancelable: true }
            );
        }
    }
    delete = () => {
        let dataEdit = {
            ChatboxGroupGuid: this.props.guid,
            Name: "",
            IsGroup: 1,
            detail: [],
            updateOwend: []
        }
        let _deleteUser = [global.__appSIGNALR.SIGNALR_object.USER.LoginName];
        var _edit = new FormData();
        _edit.append('edit', JSON.stringify(dataEdit));
        _edit.append('DeleteUser', JSON.stringify(_deleteUser));
        API_Message.Messenger_ChatboxGroup_Update(_edit).then(res => {
            var _text = {
                S: "L",
                D: [{
                    L: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
                    F: global.__appSIGNALR.SIGNALR_object.USER.FullName
                }]
            };

            this.InsertChatBox(_text);
            Actions.home({ data: "chat" })
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }

    AlertRemoveGroup() {
        Alert.alert(
            //title
            'Thông báo',
            //body
            'Bạn có chắc chắn xoá nhóm chat ' + this.state.Name + ' không',
            [
                { text: 'Xoá', onPress: () => this.RemoveGroup() },
                { text: 'Huỷ', onPress: () => console.log('No Pressed') },
            ],
            { cancelable: true }
        );
    }
    RemoveGroup() {
        let obj = { ChatboxGroupGuid: this.props.guid }
        API_Message.Messenger_ChatBoxGroup_Delete(obj).then(res => {
            Toast.showWithGravity("Bạn đã xoá nhóm chat thành công", Toast.LONG, Toast.CENTER);
            Actions.home({ data: "chat" })
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }

    //#region thêm mới vào tin nhắn
    InsertChatBox = (val) => {
        controller.Messenger_UserSend(this.props.guid, rs => {
            var _UserSend = rs;
            var objMessage = {
                ChatboxGroupGuid: this.props.guid,
                Messager: JSON.stringify(val),
                Type: "I",
                IsGroup: this.state.IsGroup,
                ParentGuid: null
            };
            controller.sendComment(objMessage, [], _UserSend, rs => {
                if (rs === "") {
                    Toast.showWithGravity("Cập nhật thành công", Toast.LONG, Toast.CENTER);
                }
            })
        })

    }
    //#endregion
}
const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Information_Component);
