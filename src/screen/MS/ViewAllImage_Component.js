import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert,
    Dimensions,
    Modal
} from 'react-native';
import CameraRoll from "@react-native-camera-roll/camera-roll";
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Divider, Icon, Header } from 'react-native-elements';
import styles from './StyleMS';
import { API_Message } from '../../network';
import configApp from '../../configApp';
import RNFetchBlob from 'rn-fetch-blob';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import ImageViewer from 'react-native-image-zoom-viewer';
import LoadingComponent from '../component/LoadingComponent';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class ViewAllImage_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            ListdataView: [],
            ViewImage: this.props.ItemImage,
            Count_Tab: 0,
            Title: this.props.ItemImage.Title,
        };
        this.state.FileAttackments = {
            renderFileAttackments: function (item) {
                return configApp.url_icon_chat + configApp.link_type_icon + item + '-icon.png';
            },
            renderImage(guid) {
                return configApp.url_Image_chat + guid;
            },
            renderViewImage(guid) {
                return configApp.url_View_Image + guid;
            },
        }
    }
    componentDidMount() {
        this.ListDataTabImage();
    }
    componentWillReceiveProps(nextProps) {
    }
    ListDataTabImage() {
        var guid = { ChatboxGroupGuid: this.props.ChatboxGroupGuid };
        API_Message.Messenger_ViewAll_Image(guid)
            .then(res => {
                var _list = this.state.ListdataView;
                var _listDB = JSON.parse(res.data.data);
                for (let i = 0; i < _listDB.length; i++) {
                    _list.unshift({
                        ..._listDB[i],
                        url: this.state.FileAttackments.renderViewImage(_listDB[i].AttachmentGuid),
                        props: {
                            // headers: ...
                        }
                    })
                }
                this.setState({
                    ListdataView: _list,
                    loading: false
                });
                var obj = _list.find(x => x.AttachmentGuid.toUpperCase() === this.state.ViewImage.AttachmentGuid.toUpperCase());
                if (obj !== undefined) {
                    setTimeout(() => {
                        this.EventViewImage(obj, _list.length - obj.id);
                    }, 50);
                }
            })
            .catch(error => {
                ErrorHandler.handle(error.data);
            });
    }
    EventViewImage = (item, index) => {
        this.myScroll.scrollTo({
            x: (
                (DRIVER.width * this.state.ListdataView.length) - ((this.state.ListdataView.length - index) * DRIVER.width)
            )
        });
        this.setState({
            ViewImage: item,
            Title: item.Title,
            Count_Tab: index
        })

    }
    OnScrollTo = (event, para) => {
        var x = event.nativeEvent.contentOffset.x;
        if (x >= ((this.state.Count_Tab * DRIVER.width) + (DRIVER.width / 3))) {
            this.state.ViewImage = this.state.ListdataView[this.state.Count_Tab + 1];

            this.state.Count_Tab = this.state.Count_Tab + 1;
            this.myScroll.scrollTo({
                x: (
                    (DRIVER.width * para.length) - ((para.length - this.state.Count_Tab) * DRIVER.width)
                )
            });
        } else if (x < ((this.state.Count_Tab * DRIVER.width) - (DRIVER.width / 3))) {
            if (this.state.Count_Tab !== 0) {
                this.state.ViewImage = this.state.ListdataView[this.state.Count_Tab - 1]
                this.state.Count_Tab = this.state.Count_Tab - 1
                this.myScroll.scrollTo({
                    x: (
                        (DRIVER.width * para.length) - ((para.length - this.state.Count_Tab) * DRIVER.width)
                    )
                });
            }
        } else {
            this.state.ViewImage = this.state.ListdataView[this.state.Count_Tab]
            this.myScroll.scrollTo({
                x: (
                    (DRIVER.width * para.length) - ((para.length - this.state.Count_Tab) * DRIVER.width)
                )
            });
        }
        this.SetTitle();
    }
    SetTitle = () => {
        this.setState({ Title: this.state.ViewImage.Title })
    }
    CustomViewImage = (item) => (
        // <Modal visible={true} transparent={true}>

        //     <ImageViewer imageUrls={item} />
        // </Modal>
        <ScrollView ref={(ref) => { this.myScroll = ref; }}
            onScrollEndDrag={(event) => this.OnScrollTo(event, item)}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={AppStyles.centerComponent}>
            {item.map((obj, i) => (
                <TouchableOpacity key={i} style={[{ justifyContent: 'center', alignItems: 'center', width: DRIVER.width }]} >
                    {(obj.FileExtension == "png" || obj.FileExtension == "jpg" || obj.FileExtension == "jpeg") ?
                        // <Modal visible={true} transparent={true}>
                        //     <ImageViewer imageUrls={images} />
                        // </Modal>
                        <Image
                            style={{ width: DRIVER.width, height: "50%" }}
                            source={{ uri: this.state.FileAttackments.renderViewImage(obj.AttachmentGuid) }}
                        />
                        // <ImageViewer imageUrls={item} style={{ width: "100%", height: "100%" }} />
                        :
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Image
                                style={{ width: 100, height: 100 }}
                                source={{ uri: this.state.FileAttackments.renderFileAttackments(obj.FileExtension) }}
                            />
                            <Text style={{ color: '#fff', marginTop: 10 }}>{obj.Title}</Text>
                            <Text style={{ color: '#fff' }}>Đây là file {obj.FileExtension}. Không thể hiển thị</Text>
                            <TouchableOpacity style={{ flexDirection: 'row', width: '100%', height: 30, marginTop: 10 }} onPress={() => this.DownloadFile(this.state.ViewImage)}>
                                <Icon name="download" type="antdesign" color={'#0066FF'} size={16} />
                                <Text style={{ color: '#0066FF' }}>Tải về</Text>
                            </TouchableOpacity>
                        </View>
                    }
                </TouchableOpacity>
            ))}
        </ScrollView>
    )
    CustomTabbarImage = (item) => (
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {item.map((obj, index) => (
                <TouchableOpacity style={{ height: 50, width: 50, margin: 10, borderColor: '#fff' }} onPress={() => this.EventViewImage(obj, index)}>
                    {this.state.ViewImage.AttachmentGuid == obj.AttachmentGuid ?
                        (obj.FileExtension == "png" || obj.FileExtension == "jpg" || obj.FileExtension == "jpeg") ?
                            <Image
                                style={{ width: 50, height: 50, borderRadius: 10, borderWidth: 1, borderColor: AppColors.Maincolor }}
                                source={{ uri: this.state.FileAttackments.renderImage(obj.AttachmentGuid) }}
                            />
                            :
                            <View style={{ backgroundColor: "#fff", borderRadius: 10 }}>
                                <Image
                                    style={{ width: 50, height: 50 }}
                                    source={{ uri: this.state.FileAttackments.renderFileAttackments(obj.FileExtension) }}
                                />
                            </View>
                        :
                        (obj.FileExtension == "png" || obj.FileExtension == "jpg" || obj.FileExtension == "jpeg") ?

                            <Image
                                style={{ width: 50, height: 50, borderRadius: 10 }}
                                source={{ uri: this.state.FileAttackments.renderImage(obj.AttachmentGuid) }}
                            />
                            :
                            <View style={{ backgroundColor: "#fff", borderRadius: 10, opacity: 0.5 }}>
                                <Image
                                    style={{ width: 50, height: 50 }}
                                    source={{ uri: this.state.FileAttackments.renderFileAttackments(obj.FileExtension) }}
                                />
                            </View>
                    }
                </TouchableOpacity>
            ))}
        </ScrollView>
    )
    render() {
        return (
            this.state.loading !== true ?
                <View style={styles.container_VAI}>
                    <Header
                        statusBarProps={AppStyles.statusbarProps}
                        {...AppStyles.headerImage}
                        leftComponent={
                            <TouchableOpacity style={{ flex: 1, width: 45, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.backToList()}>
                                <Icon name="close" type="antdesign" color={"#fff"} size={16} />
                            </TouchableOpacity>
                        }
                        centerComponent={<View style={{ flex: 8, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', color: '#fff' }}>{this.state.Title}</Text></View>}
                        rightComponent={
                            <TouchableOpacity style={{ flex: 1, width: 45, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.DownloadFile(this.state.ViewImage)}>
                                <Icon name="download" type="antdesign" color={"#fff"} size={16} />
                            </TouchableOpacity>
                        }
                    />
                    <Divider />
                    <View style={{ flex: 1, marginTop: 5 }}>
                        <View style={{ flex: 5 }}>
                            {this.state.ListdataView != null ? this.CustomViewImage(this.state.ListdataView) : null}
                        </View>
                        <View style={styles.TabImage_VAI}>
                            {this.state.ListdataView && this.state.ListdataView.length > 0 ? this.CustomTabbarImage(this.state.ListdataView) : null}
                        </View>

                    </View>
                </View>
                :
                <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    OnDownload(attachObject) {
        Alert.alert(
            //title
            'Thông báo',
            //body
            'Bạn muốn tải file đính kèm về máy',
            [
                { text: 'Tải về', onPress: () => this.DownloadFile(attachObject) },
                { text: 'Huỷ', onPress: () => console.log('No Pressed') },
            ],
            { cancelable: true }
        );
    }
    //tair file đính kèm về
    DownloadFile(attachObject) {
        let dirs = RNFetchBlob.fs.dirs;
        if (Platform.OS !== "ios") {
            RNFetchBlob
                .config({
                    addAndroidDownloads: {
                        useDownloadManager: true, // <-- this is the only thing required
                        // Optional, override notification setting (default to true)
                        notification: true,
                        // Optional, but recommended since android DownloadManager will fail when
                        // the url does not contains a file extension, by default the mime type will be text/plain
                        mime: '/',
                        description: 'File downloaded by download manager.',
                        //title: new Date().toLocaleString() + ' - test.xlsx',
                        //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
                        path: "file://" + dirs.DownloadDir + '/' + attachObject.Title, //using for android
                    }
                })
                .fetch('GET', configApp.url_View_Image + attachObject.AttachmentGuid, {})
                .then((resp) => {
                    Alert.alert(
                        //title
                        'Thông báo',
                        //body
                        'Tải thành công',
                        [
                            { text: 'Đóng', onPress: () => console.log('No Pressed') },
                        ],
                        { cancelable: true }
                    )
                    // the path of downloaded file
                    resp.path()
                })
        }
        else {
            RNFetchBlob
                .config({
                    path: dirs.DocumentDir + '/' + attachObject.Title,
                    addAndroidDownloads: {
                        useDownloadManager: true, // <-- this is the only thing required
                        // Optional, override notification setting (default to true)
                        notification: true,
                        IOSDownloadTask: true,
                        // Optional, but recommended since android DownloadManager will fail when
                        // the url does not contains a file extension, by default the mime type will be text/plain
                        mime: '/',
                        description: 'File downloaded by download manager.',
                        title: new Date().toLocaleString() + '-' + attachObject.Title,
                        path: dirs.DocumentDir + "/" + new Date().toLocaleString() + '-' + attachObject.Title, //using for ios
                        //using for android
                    }

                })
                .fetch('GET', configApp.url_View_Image + attachObject.AttachmentGuid, {})
                .then((resp) => {
                    console.log("88888888888:", resp.data)
                    RNFetchBlob.ios.openDocument(resp.data);
                    // the path of downloaded file
                    // resp.path()
                    // CameraRoll.saveToCameraRoll(dirs.DocumentDir + '/' + attachObject.Title)
                    //     .then(
                    //         Alert.alert(
                    //             //title
                    //             'Thông báo',
                    //             //body
                    //             'Tải thành công',
                    //             [
                    //                 { text: 'Đóng', onPress: () => console.log('No Pressed') },
                    //             ],
                    //             { cancelable: true }
                    //         )
                    //         // alert('Tải đính kèm thành công', 'Photo added to camera roll!')
                    //     )
                    //     .catch(err => console.log('err:', err))
                    // alert('Image Downloaded Successfully.');
                })
        }
    }
    backToList() {
        Actions.pop();
    }
}

const mapStateToProps = state => ({
});
const mapDispatchToProps = {
};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(ViewAllImage_Component);
