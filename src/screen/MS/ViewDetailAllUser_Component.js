import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import { Divider, Icon, ListItem, Badge } from 'react-native-elements';
import { API, API_HR, API_Message } from '../../network';
import Fonts from '../../theme/fonts';
import { Thumbnail } from 'native-base';
import { listCombobox } from './listCombobox';
import { controller } from './controller';
import { AppStyles } from '@theme';
import TabBar_Title from "../component/TabBar_Title";
import LoadingComponent from '../component/LoadingComponent';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius: 1,
        backgroundColor: '#FFF',
        elevation: 2,
        paddingLeft: 5,
        paddingRight: 5
    },
    subtitleStyle: {
        fontSize: 12,
        fontFamily: 'Arial',
        color: '#AAAAAA'
    },
    subtitleStyle_v1: {
        fontSize: 12,
        fontFamily: 'Arial'
    }
});
class ViewDetailAllUser_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            ListPeople: []
        };
    }
    componentDidMount() {
        this.List_people();
    }
    componentWillReceiveProps(nextProps) {
    }
    //Tìm kiếm người chat
    List_people() {
        var guid = { ChatboxGroupGuid: this.props.Guid }
        API_Message.Messenger_AllAccountOfGroup(guid).then(res => {
            var _data = JSON.parse(res.data.data);
            var _listAccount = [];
            for (let i = 0; i < _data.length; i++) {
                if (_data[i].LoginName == _data[i].LoginNameOwned) {
                    _data[i].CheckOwned = true;
                    _listAccount.push(_data[i])
                }
            }
            for (let j = 0; j < _data.length; j++) {
                if (_data[j].LoginName != _data[j].LoginNameOwned) {
                    _data[j].CheckOwned = false;
                    _listAccount.push(_data[j])
                }
            }
            this.setState({
                ListPeople: _listAccount,
                loading: false
            })
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }
    CustomeListPeople = (item) => (
        <ScrollView>
            {this.state.ListPeople.map((item, index) => (
                <View style={{ fontSize: 11, fontFamily: Fonts.base.family }}>
                    {this.props.CheckRemove == false ?
                        <ListItem
                            subtitle={() => {
                                return (
                                    <View style={{ flexDirection: 'row', marginBottom: -10, marginTop: -30 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', }}>
                                            <View style={{ flex: 2, marginRight: 10 }}>
                                                <Thumbnail style={{}} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
                                                {item.CheckOwned == true ?
                                                    <Badge
                                                        badgeStyle={{ backgroundColor: '#888888', opacity: 0.8 }}
                                                        value={<Icon iconStyle={{ color: '#FFFF00' }} size={11} name='key' type='simple-line-icon' />}
                                                        containerStyle={{ position: 'absolute', right: -5, bottom: 0 }}
                                                    /> : null}
                                            </View>
                                        </View>
                                        <View style={{ flex: 4, flexDirection: 'row' }}>
                                            <View style={{ flex: 6, justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 14, fontWeight: 'bold', }}>{item.FullName}</Text>
                                            </View>
                                            {item.LoginName === global.__appSIGNALR.SIGNALR_object.USER.LoginName ? null :
                                                <TouchableOpacity style={{ flex: 3, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} onPress={() => this.ToViewItemAcount(item.EmployeeGuid)}>
                                                    <Text style={{ fontSize: 10, color: '#3399FF' }}>Xem thông tin</Text>
                                                </TouchableOpacity>
                                            }
                                        </View>
                                    </View>
                                );
                            }}
                            onPress={() => this.ToViewItemAcount(item.EmployeeGuid)}
                        />
                        :
                        <ListItem
                            subtitle={() => {
                                return (
                                    <View style={{ flexDirection: 'row', marginBottom: -10, marginTop: -30 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', }}>
                                            <View style={{ flex: 2, marginRight: 10 }}>
                                                <Thumbnail style={{}} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
                                                {item.CheckOwned == true ?
                                                    <Badge
                                                        badgeStyle={{ backgroundColor: '#888888', opacity: 0.8 }}
                                                        value={<Icon iconStyle={{ color: '#FFFF00' }} size={11} name='key' type='simple-line-icon' />}
                                                        containerStyle={{ position: 'absolute', right: -5, bottom: 0 }}
                                                    /> : null}
                                            </View>
                                        </View>
                                        <View style={{ flex: 4, flexDirection: 'row' }}>
                                            <View style={{ flex: 6, justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 14, fontWeight: 'bold', }}>{item.FullName}</Text>
                                            </View>
                                        </View>
                                    </View>
                                );
                            }}
                            onPress={() => this.OnCheckRemove(item.LoginName, item.FullName)}
                        />
                    }
                </View>
            ))}
        </ScrollView>
    )
    render() {
        return (
            this.state.loading !== true ?
                <View style={styles.container}>
                    <TabBar_Title
                        title={'Thành viên trong nhóm'}
                        callBack={() => this.backToList()} />
                    <Divider />
                    <View style={{ flex: 1, marginTop: 5 }}>
                        {this.state.ListPeople ? this.CustomeListPeople(this.state.ListPeople) : null}
                    </View>
                </View>
                :
                <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    backToList() {
        Actions.pop();
        Actions.refresh({ moduleId: 'back', Data: { guid: this.props.Guid, Name: this.props.Name, IsGroup: this.props.IsGroup }, ActionTime: (new Date()).getTime() });
    }
    ToViewItemAcount(item) {
        Actions.empviewitem({ EmployeeGuid: item });
    }
    ToAddUserOfGroup() {
        Actions.addUserOfGroup({ ChatboxGroupGuid: this.props.Guid });
    }
    OnCheckRemove(EmpGuid, fullname) {
        var _updateOwend = [];
        _updateOwend.push(EmpGuid);
        let dataEdit = {
            ChatboxGroupGuid: this.props.Guid,
            Name: "",
            IsGroup: 1,
            detail: [],
            updateOwend: _updateOwend
        }
        let _deleteUser = [];
        var _edit = new FormData();
        _edit.append('edit', JSON.stringify(dataEdit));
        _edit.append('DeleteUser', JSON.stringify(_deleteUser));
        API_Message.Messenger_ChatboxGroup_Update(_edit).then(res => {
            var obj_userSend = { Id: this.props.Guid }
            API_Message.Messenger_UserSend(obj_userSend).then(ress => {
                var _UserSend = JSON.parse(ress.data.data).UserSend.split(';');
                var _text = {
                    S: "S",
                    D: [{
                        L: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
                        F: global.__appSIGNALR.SIGNALR_object.USER.FullName
                    }]
                };
                var objMessage = {
                    ChatboxGroupGuid: this.props.Guid,
                    Messager: JSON.stringify(_text),
                    Type: "I",
                    ParentGuid: null,
                    IsGroup: this.props.IsGroup
                };
                controller.sendComment(objMessage, [], _UserSend, rs => {
                    if (rs === "") {
                        this.backToList();
                    }
                })
            }).catch(error => {
                this.callBack()
                ErrorHandler.handle(error.data);
            });











        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
    //GetUserName: state.user.getProfile
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(ViewDetailAllUser_Component);
