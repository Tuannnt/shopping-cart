import React, { Component } from 'react';
import { FlatList, StyleSheet, Keyboard, Dimensions, RefreshControl, Text, TouchableOpacity, TouchableWithoutFeedback, View, Image, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import { Header, Icon, ListItem, Badge } from 'react-native-elements';
import { API, API_HR, API_Message } from '../../network';
import Fonts from '../../theme/fonts';
import { Thumbnail } from 'native-base';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { LoadingComponent, FormSearch, TabBar } from '@Component';
import { FuncCommon } from '@utils';
import { listCombobox } from './listCombobox';
import { controller } from './controller';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const Sound = require('react-native-sound')
class ListAll_Messange_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            Data: null,
            loading: false,
            loadingSearch: true,
            test: '',
            ListAll: [],
            ListSearch: [],
            checkSearch: 1
        };
        this.staticparam = {
            Length: 50,
            NumberPage: 0,
            txtSearch: "",
            IsFavourite: 0
        };
        this.props.ReloadMessage(this._reloadData);
        if (this.props.DatachatGroupNew && this.props.DatachatGroupNew != undefined && this.props.DatachatGroupNew != null) {
            this.openViewChat()
        }
        global.__appSIGNALR.addReviceMess(this.RefreshList);
    }
    componentDidMount() {

        this.setState({ loading: true });

        this.nextPage();
    }
    componentWillReceiveProps = (nextProps) => {

        this.setState({ loading: true });
        this.ReloadData();
    }
    _reloadData = () => {
        this.ReloadData();
    }
    ReloadData() {
        this.setState({
            refreshing: false,
            Data: null,
            loading: false,
            test: '',
            ListAll: [],
            ListSearch: []
        })
        this.staticparam = {
            Length: 50,
            NumberPage: 0,
            txtSearch: "",
            IsFavourite: 0
        };
        this.nextPage(true);
    }
    //RefreshList
    RefreshList = (LoginName, Comment) => {
        var CommentString = JSON.parse(Comment);
        if (CommentString.Code == "CHAT") {
            var check = this.state.ListAll.find(x => x.ChatboxGroupGuid === CommentString.ChatboxGroupGuid);
            var list = [];
            if (check !== undefined) {
                for (let i = 0; i < this.state.ListAll.length; i++) {
                    if (this.state.ListAll[i].ChatboxGroupGuid === CommentString.ChatboxGroupGuid) {
                        list.unshift({
                            ...this.state.ListAll[i],
                            LastTime: FuncCommon.ConDate(CommentString.Time, 15),
                            Message: CommentString.Message,
                            Message_Employee: CommentString.EmployeeGuid,
                            NotRead: parseInt(this.state.ListAll[i].NotRead) + 1,
                            Type: CommentString.Type,
                        })
                    } else {
                        list.push(this.state.ListAll[i])
                    }
                }
                this.setState({ ListAll: list });
            } else {
                controller.Messenger_RefreshList(this.state.ListAll, Comment, rs => {
                    this.setState({ ListAll: rs });
                });
            }
        }

    };
    //hàm lấy danh sách

    onclicksearch = (val) => {
        this.staticparam.txtSearch = "";
        this.setState({ checkSearch: val });
        setTimeout(() => {
            this.nextPage(true);
        }, 100);
    };
    nextPage(s) {
        this.setState({ loadingSearch: true });
        var list = [];
        if (s !== true) {
            list = this.state.ListAll;
        } else {
            this.staticparam.NumberPage = 0;
        }
        if (this.state.checkSearch === 1) {
            this.staticparam.IsFavourite = 0;
            controller.Messenger_UserOfChatbox_ListAll(this.staticparam, list, rs => {
                this.setState({ ListAll: rs, refreshing: false, loading: false, loadingSearch: false });
            });
        } else if (this.state.checkSearch === 3) {
            this.staticparam.IsFavourite = 1;
            controller.Messenger_UserOfChatbox_ListAll(this.staticparam, list, rs => {
                this.setState({ ListAll: rs, refreshing: false, loading: false, loadingSearch: false });
            });
        } else {
            controller.Messenger_Search(this.staticparam.txtSearch, rs => {
                this.setState({ ListSearch: rs, refreshing: false, loading: false, loadingSearch: false });
            })
        }
    };
    updateSearch = search => {
        this.staticparam.txtSearch = search;
        this.nextPage(true);
    };
    updateFavorites = (val, guid, i) => {
        controller.Messenger_FavoritesAction(val, guid, rs => {
            this.state.ListAll[i].IsFavourite = !this.state.ListAll[i].IsFavourite;
            this.setState({ ListAll: this.state.ListAll });
        });
    };
    render() {
        return (
            <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null} style={{ flex: 1 }}>
                <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => { Keyboard.dismiss() }}>
                    <View style={AppStyles.container}>
                        {this.Header()}
                        <FormSearch CallbackSearch={(callback) => this.updateSearch(callback)} />
                        <View style={{ flex: 1 }}>
                            {this.state.loading !== true ?
                                this.state.checkSearch !== 2 ?
                                    this.state.loadingSearch == true ? null : this.state.ListAll.length > 0 ? this.CustomeListAll(this.state.ListAll) : <View style={{ alignItems: 'center' }}><Text>Không có dữ liệu</Text></View>
                                    : this.state.loadingSearch == true ? null : this.state.ListSearch.length > 0 ? this.CustomeListSearch(this.state.ListSearch) : <View style={{ alignItems: 'center' }}><Text>Không có dữ liệu</Text></View>
                                :
                                <LoadingComponent backgroundColor={'#fff'} />
                            }
                            <TouchableOpacity style={{ right: 25, bottom: 30, width: 40, height: 40, backgroundColor: '#0693e3', borderRadius: 50, position: 'absolute', justifyContent: 'center', alignItems: 'center' }} onPress={() => this.onClickAddGroup()}>
                                <Icon name={'plus'} type='feather' size={30} color={"#fff"} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    };
    //#region View Top
    Header = () => {
        return (
            <Header
                statusBarProps={AppStyles.statusbarProps}
                {...AppStyles.headerProps}
                leftComponent={this.renderLeft()}
                centerComponent={this.renderCenter()}
                rightComponent={this.renderRight()}
            />
        )
    }
    renderLeft = () => {
        return (
            <TouchableOpacity style={[_styles.Style_Header]} onPress={() => this.onclicksearch(1)}>
                <Icon name={'message-circle'} type='feather' size={20} color={this.state.checkSearch == 1 ? AppColors.Maincolor : AppColors.gray} />
                <Text style={[AppStyles.Textdefault, { color: this.state.checkSearch == 1 ? AppColors.Maincolor : AppColors.gray }]}>Tin nhắn</Text>
            </TouchableOpacity>
        );
    }
    renderCenter = () => {
        return (
            <TouchableOpacity style={[_styles.Style_Header]} onPress={() => this.onclicksearch(2)}>
                <Icon name={'users'} type='feather' size={20} color={this.state.checkSearch == 2 ? AppColors.Maincolor : AppColors.gray} />
                <Text style={[AppStyles.Textdefault, { color: this.state.checkSearch == 2 ? AppColors.Maincolor : AppColors.gray }]}>Nhân viên</Text>
            </TouchableOpacity>
        );
    }
    renderRight = () => {
        return (
            <TouchableOpacity style={[_styles.Style_Header]} onPress={() => this.onclicksearch(3)}>
                <Icon name={'star'} type='feather' size={20} color={this.state.checkSearch == 3 ? AppColors.Maincolor : AppColors.gray} />
                <Text style={[AppStyles.Textdefault, { color: this.state.checkSearch == 3 ? AppColors.Maincolor : AppColors.gray }]}>Yêu thích</Text>
            </TouchableOpacity>
        );
    }
    //#endregion

    CustomeListAll = (item) => (
        <FlatList
            data={item}
            style={{ flex: 1 }}
            renderItem={({ item, index }) => (
                <View style={{ fontSize: 11, borderBottomWidth: 0.5, borderBottomColor: "#C0C0C0" }}>
                    <ListItem
                        subtitle={() => {
                            return (
                                <View style={{ flexDirection: 'row', marginTop: -20 }} >
                                    <TouchableOpacity style={{ flex: 1, flexDirection: 'row', }} onPress={() => { controller.onViewItem(item), item.NotRead > 0 ? controller.Messenger_ActiveRead(item.ChatboxGroupGuid) : null }}>
                                        <View style={{ flex: 2, marginRight: 10 }}>
                                            {item.IsGroup == "False" ?
                                                <Thumbnail source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} /> :
                                                this.CustomViewIconGroup(item.ListEmpGroup)
                                            }
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{ flex: 4, flexDirection: 'column', justifyContent: 'center' }}>
                                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { controller.onViewItem(item), item.NotRead > 0 ? controller.Messenger_ActiveRead(item.ChatboxGroupGuid) : null }}>
                                            <View style={{ flex: 3 }}>
                                                {item.IsGroup == "False" ?
                                                    <Text style={[AppStyles.TitleMSS, { color: AppColors.TextColorMSS }]}>{item.FullName}</Text> :
                                                    <Text style={[AppStyles.TitleMSS, { color: AppColors.TextColorMSS }]}>{item.Name}</Text>
                                                }
                                            </View>
                                            <View style={{ flex: 2, alignItems: 'flex-end' }}>
                                                <Text style={AppStyles.Textdefault}>{controller.customTime(item.LastTime)}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <View style={{ marginTop: 2, flexDirection: 'row' }}>
                                            <TouchableOpacity style={{ marginRight: 30 }}>
                                                {item.Message_Employee === global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid ?
                                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>Bạn: {controller.checkMessage(item.Type, item.Message, 10)}</Text>
                                                    :
                                                    //  item.Message_Status == "False" ?
                                                    //     <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>{this.checkMessage(item.Message, 10)}</Text>
                                                    //     :
                                                    <Text style={[AppStyles.Textdefault, { color: AppColors.gray }]}>{controller.checkMessage(item.Type, item.Message, 10)}</Text>
                                                }
                                            </TouchableOpacity>
                                            {item.NotRead > 0 ?
                                                <View style={{ right: 25, width: 20, height: 20, backgroundColor: '#0693e3', borderRadius: 5, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 12, color: '#fff' }}>{item.NotRead}</Text>
                                                </View>
                                                : null
                                            }
                                            {item.IsGroup === "True" ? null :
                                                <TouchableOpacity style={{ right: 0, position: 'absolute', justifyContent: 'center', alignItems: 'center' }} onPress={() => this.updateFavorites(item.EmpLoginName, item.ChatboxGroupGuid, index)}>
                                                    <Icon name={'star'} type='feather' size={20} color={item.IsFavourite === "True" ? AppColors.Maincolor : AppColors.gray} />
                                                </TouchableOpacity>
                                            }
                                        </View>
                                    </View>
                                </View>
                            );
                        }}
                    //chevron
                    />
                </View>
            )}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={() => { this.staticparam.NumberPage * this.staticparam.Length <= this.state.ListAll.length ? this.nextPage(false) : null }}
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={() => this._onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['black']}
                />
            }

        />
    )
    CustomeListSearch = (item) => (
        <FlatList
            data={item}
            style={{ flex: 1 }}
            renderItem={({ item, index }) => (
                <View style={{ fontSize: 11, fontFamily: Fonts.base.family }}>
                    <ListItem
                        subtitle={() => {
                            return (
                                <TouchableOpacity style={{ flexDirection: 'row', marginTop: -20 }} onPress={() => controller.oncheckItem(item)}>
                                    <View style={{ flex: 3, flexDirection: 'row' }}>
                                        <View style={{ flex: 2, marginRight: 10 }}>
                                            {item.Group === false ?
                                                <Thumbnail source={API_HR.GetPicApplyLeaves(item.Guid)} />
                                                :
                                                this.CustomViewIconGroup(item.ListEmpGroup)
                                            }
                                        </View>
                                        <View style={{ flex: 7, justifyContent: 'center' }}>
                                            <Text style={AppStyles.TitleMSS, { color: AppColors.TextColorMSS }}>{item.Name}</Text>
                                            {/* <Text style={AppStyles.Textdefault}> Chức vụ: {item.JobTitleName}</Text> */}
                                        </View>
                                    </View>
                                    {/* <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={[AppStyles.Textdefault, { textAlign: 'right', }]}>
                                            {item.HomePhone}
                                        </Text>
                                    </View> */}
                                </TouchableOpacity>
                            );
                        }}
                        bottomDivider
                        onPress={() => controller.oncheckItem(item)}
                    />
                </View>
            )}

            keyExtractor={(item, index) => index.toString()}
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['black']}
                />
            }
        />
    )
    CustomViewIconGroup = (para) => (
        para.length == 2 ?
            <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', }}>
                    <Image
                        style={{ width: 30, height: 30, borderRadius: 50 }}
                        source={API_HR.GetPicApplyLeaves(para[0])}
                    />
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Image
                        style={{ width: 30, height: 30, borderRadius: 50 }}
                        source={API_HR.GetPicApplyLeaves(para[1])}
                    />
                </View>
            </View>
            :
            para.length == 3 ?
                <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image
                            style={{ width: 30, height: 30, borderRadius: 50 }}
                            source={API_HR.GetPicApplyLeaves(para[0])}
                        />
                        <Image
                            style={{ width: 30, height: 30, borderRadius: 50 }}
                            source={API_HR.GetPicApplyLeaves(para[1])}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Image
                            style={{ width: 30, height: 30, borderRadius: 50 }}
                            source={API_HR.GetPicApplyLeaves(para[2])}
                        />
                    </View>
                </View>
                :
                para.length > 3 ?
                    <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image
                                style={{ width: 30, height: 30, borderRadius: 50 }}
                                source={API_HR.GetPicApplyLeaves(para[0])}
                            />
                            <Image
                                style={{ width: 30, height: 30, borderRadius: 50 }}
                                source={API_HR.GetPicApplyLeaves(para[1])}
                            />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Image
                                style={{ width: 30, height: 30, borderRadius: 50 }}
                                source={API_HR.GetPicApplyLeaves(para[2])}
                            />
                            <Badge
                                badgeStyle={{ backgroundColor: '#888888', opacity: 0.8, width: 30, height: 30, borderRadius: 50 }}
                                value={'+' + (para.length - 3).toString() + ''}
                                containerStyle={{}}
                            />
                        </View>
                    </View>
                    : null

    )


    onSwipeClose(item) {
        this.setState({
            model: {},
        });
    }
    onclickreturnSearch() {
        this.state.Search = '';
        this.updateSearch('')
    } 79
    onClickAddGroup() {
        Actions.AddGroup({ moduleId: 'ListAllMSS' });
    }
    openViewChat = () => {
        setTimeout(() => {
            Actions.viewItemMessange({ RecordGuid: this.props.DatachatGroupNew.RecordGuid, Name: this.props.DatachatGroupNew.Name, IsGroup: this.props.DatachatGroupNew.IsGroup, LoginName: null });
        }, 1000);
    }
    //Load lại
    _onRefresh = () => {
        this.setState({ ListAll: [] })
        this.nextPage(true);
        this.setState({ refreshing: true });
    };
}
const _styles = StyleSheet.create({
    Style_Header: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: SCREEN_WIDTH / 3
    },
});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(ListAll_Messange_Component);
