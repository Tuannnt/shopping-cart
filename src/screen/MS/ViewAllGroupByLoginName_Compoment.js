import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import { Icon, ListItem } from 'react-native-elements';
import { API_Message } from '../../network';
import { AppStyles } from '@theme';
import Fonts from '../../theme/fonts';
import TabBar_Title from "../component/TabBar_Title";
import LoadingComponent from '../component/LoadingComponent';
const styles = StyleSheet.create({
    containerfull: {
        flex: 1,
        elevation: 2,
        flexDirection: "column"
    },
    container: {
        borderTopWidth: 0.5,
        borderTopColor: '#CCCCCC',
        paddingLeft: 10,
        backgroundColor: '#FFF',
    },
    TouchableOpacityGroup: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomWidth: 0.5,
        borderBottomColor: '#CCCCCC'
    }
});
class ViewAllGroupByLoginName_Compoment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListData: [],
            loading: true
        };
    }
    componentDidMount(): void {
        var data = { LoginName: this.props.LoginName }
        API_Message.Messenger_ViewAllGroup_ByLoginName(data).then(rs => {
            this.setState({
                ListData: JSON.parse(rs.data.data),
                loading: false
            })
        }).catch(error => {
            ErrorHandler.handle(error.data);
        });
    }
    componentWillReceiveProps(nextProps) {
    }
    render() {
        return (
            this.state.loading !== true ?
                <View style={AppStyles.container}>
                    <TabBar_Title
                        title={'Nhóm chung'}
                        callBack={() => this.backToList()}
                    />
                    <View>
                        {this.state.ListData && this.state.ListData.length > 0 ?
                            this.state.ListData.map((item, index) => (
                                <View style={{ fontSize: 12, fontFamily: Fonts.base.family }}>
                                    <ListItem
                                        subtitle={() => {
                                            return (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ flex: 1, flexDirection: 'row', }}>
                                                        <View style={{ flex: 2, marginRight: 10 }}>
                                                            <Icon name="group" type="font-awesome" color={"#888888"} size={30} />
                                                        </View>
                                                    </View>
                                                    <View style={{ flex: 4, borderBottomWidth: 0.5, borderBottomColor: "#C0C0C0" }}>
                                                        <View style={{ flexDirection: 'row', }}>
                                                            <View style={{ flex: 3, justifyContent: 'center' }}>
                                                                {item.Name == "" ?
                                                                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#2E77FF' }}>Chưa đặt tên nhóm</Text> :
                                                                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#2E77FF' }}>{item.Name}</Text>
                                                                }
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                            );
                                        }}
                                        //chevron
                                        onPress={() => this.onViewItem(item.ChatboxGroupGuid, item.Name, item.IsGroup)}
                                    />
                                </View>
                            ))
                            :
                            <View style={AppStyles.centerAligned}>
                                <Text style={AppStyles.Textdefault}>Không có dữ liệu</Text>
                            </View>
                        }
                    </View>
                </View>
                :
                <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    backToList() {
        Actions.pop();
    }
    //view item
    onViewItem(guid, name, isGroup) {
        if (name == null || name == "" || name == undefined) {
            Actions.viewItemMessange({ RecordGuid: guid, Name: "", FullName: "Chưa đặt tên nhóm", IsGroup: isGroup });
        } else {
            Actions.viewItemMessange({ RecordGuid: guid, Name: name, IsGroup: isGroup });

        }
    }
}
const mapStateToProps = state => ({
});
const mapDispatchToProps = {
};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(ViewAllGroupByLoginName_Compoment);
