import React, { Component } from 'react'
import { View, Button, Image, Text, Animated, Easing,StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { width } from 'react-native-dimension';
const styles =StyleSheet.create({
  messageContainerStyle: {
    width: width(100),
    flexDirection: 'row',
    backgroundColor: '#c8e6c9',
    height: 70,
    paddingTop: 15,
    paddingLeft: width(100) * 0.024,
    paddingRight: width(100) * 0.114
  },

  avatarStyle: {
    width: 40,
    height: 40,
    borderWidth: 0, borderRadius: 50
  },

  textContainerStyle: {
    marginTop: -1,
    marginLeft: width(100) * 0.016
  },
});
class Notification extends Component {
  constructor(props) {
    super(props)
    this.state = { offset: new Animated.Value(props.offset) }
  }

  componentDidMount() {
    Animated.sequence([
      Animated.delay(this.props.delay),
      Animated.spring(this.state.offset, {
        tension: -5,
        toValue: 0
      }),
      Animated.timing(this.state.offset, {
        duration: 1000,
        toValue: -80
      })
    ]).start(() => this.props.removeNotification(this.props.id))
  }

  render() {
    const { message, avatar, FullName } = this.props

    return (
      <Animated.View
        style={[styles.messageContainerStyle, {
          transform: [{
            translateY: this.state.offset
          }]
        }]}
      >
        <Image
          style={styles.avatarStyle}
          source={avatar}
        />
        <View style={styles.textContainerStyle}>
          <Text style={{ fontWeight: 'bold', color: 'white' }}>{FullName}</Text>
          <Text style={{ color: 'white' }}>{message}</Text>
        </View>
      </Animated.View>
    )
  }
}
const mapStateToProps = state => ({
});
const mapDispatchToProps = {
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(Notification);