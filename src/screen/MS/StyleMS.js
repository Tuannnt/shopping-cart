import { StyleSheet, Dimensions } from 'react-native';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column'
    },
    boxSearch: {
        flexDirection: "row"
    },
    boxComment: {
        padding: 10,
        justifyContent: "center",
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: '#778899',
        marginRight: 10,
        marginTop: 3,
        backgroundColor: '#CEF6F5',
    },
    boxComment_R: {
        padding: 10,
        justifyContent: "center",
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: '#778899',
        marginRight: 10,
        marginTop: 3,
        backgroundColor: '#ECECEC',
    },
    boxComment_user_R: {
        padding: 10,
        justifyContent: "center",
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: '#778899',
        marginLeft: 5,
        marginBottom: 3,
        backgroundColor: '#ECECEC',
    },
    boxImage: {
        marginRight: 10,
        marginTop: 3
    },
    boxComment_user: {
        padding: 10,
        justifyContent: "center",
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: '#778899',
        marginLeft: 5,
        marginBottom: 3,
        backgroundColor: '#ECECEC',
    },
    boxImage_user: {
        marginLeft: 5,
        marginBottom: 3
    },
    boxBottom_Full: {
        // flex: 1,
        bottom: 0,
        borderTopColor: '#eff0f1',
        borderTopWidth: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
        zIndex: 1000
    },
    boxBottom: {
        flex: 1,
        bottom: 0,
        flexDirection: 'row'
    },
    boxBottomIcon: {
        width: SCREEN_WIDTH,
    },
    boxBottom_Full_Event: {
        flex: 3,
        width: '100%',
        bottom: 0,
        borderTopColor: '#eff0f1',
        borderTopWidth: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    boxBottom_content_Event: {
        flex: 1,
        width: '100%',
        bottom: 0,
        flexDirection: 'row'
    },
    boxBottom_content: {

        bottom: 0,
        flexDirection: 'row',
        borderWidth: 0.5,
        borderColor: '#eff0f1',
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height:1,
        // },
        // shadowOpacity: 0.1,
        // elevation:0.5,
        height: 50
    },
    boxBottom_contentIOS: {
        backgroundColor: '#fff',
        zIndex: 100,
        height: 50,
        width: '100%',
        bottom: 0,
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10
    },
    boxBottomIconTab: {
        width: SCREEN_WIDTH,
        bottom: 0,
        borderTopColor: '#eff0f1',
        borderTopWidth: 0.5,
        flexDirection: 'row',
        backgroundColor: '#fff',
        height: 50
    },
    ClickDown: {
        bottom: 10,
        right: 15,
        backgroundColor: '#CCCCCC',
        borderRadius: 50,
        position: 'absolute',
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    styleDateMess: {
        color: "#fff",
        backgroundColor: '#CCCCCC',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 4,
        paddingBottom: 4,
        borderRadius: 20,
        position: 'absolute',
        zIndex: 100,
    },
    TextViewTime: {
        fontSize: 10,
        color: "#A4A4A4"
    },
    //---------------------------------------------View All Image VAI ---------------------------------------
    container_VAI: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    TabImage_VAI: {
        flex: 1,
        bottom: 0,
        borderColor: '#fff',
        borderTopWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerImage: {
        backgroundColor: 'rgba(52, 52, 52,0.8)'
    }
});
