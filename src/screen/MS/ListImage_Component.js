import React, { Component } from 'react';
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import { Text, TouchableOpacity, View, ScrollView, StyleSheet, Dimensions, Image, Linking } from "react-native";
import { Actions } from "react-native-router-flux";
import { AppStyles, AppColors } from '@theme';
import { API_HR, API_Message, API } from '../../network';
import { Combobox, TabBar_Title, LoadingComponent } from '@Component';
import configApp from '../../configApp';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const _styles = StyleSheet.create({
    imageGrid: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start'
    },
    image: {
        width: SCREEN_WIDTH / 3.03,
        height: SCREEN_WIDTH / 3.03,
        margin: 0.5,
    },
});
class ListImage_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            checkbox: 'jpg',
            ListViewIMG: [],
            ListViewFILE: [],
            ListViewLINK: [],
        };
        this.FileAttackments = {
            renderImage(_check, guid) {
                var check = _check.toLowerCase();
                if (check === "png" || check == "jpg" || check == "jpeg") {
                    return configApp.url_Image_chat + guid;
                } else {
                    return configApp.url_icon_chat + configApp.link_type_icon + check + '-icon.png';
                }
            },
        }
    }
    componentDidMount = () => {
        this.GetLiFile();
        this.GetLiLink();
    };
    GetLiFile = () => {
        var obj = {
            RecordGuid: this.props.ChatboxGroupGuid,
            EmployeeGuid: this.props.ChatboxGroupGuid,
        }
        API_Message.GetLiFile(obj).then(rs => {
            if (rs.data.errorCode === 200) {
                var listimg = this.state.ListViewIMG;
                var listfile = this.state.ListViewFILE;
                var data = JSON.parse(rs.data.data);
                for (let i = 0; i < data.length; i++) {
                    if (data[i].FileExtension === "png" || data[i].FileExtension == "jpg" || data[i].FileExtension == "jpeg") {
                        listimg.push(data[i]);
                    } else {
                        listfile.push(data[i]);
                    }
                }
                this.setState({ ListViewIMG: listimg, ListViewFILE: listfile });
            }
        }).catch(error => {
            console.log(error);
        });
    }
    GetLiLink = () => {
        var obj = {
            RecordGuid: this.props.ChatboxGroupGuid,
        }
        API_Message.GetLinkChat(obj).then(rs => {
            if (rs.data.errorCode === 200) {
                var listLink = this.state.ListViewLINK;
                var data = JSON.parse(rs.data.data);
                for (let i = 0; i < data.length; i++) {
                    listLink.push(data[i]);
                }
                this.setState({ ListViewLINK: listLink });
            }
        }).catch(error => {
            console.log(error);
        });
    }
    render() {
        return (
            this.state.loading === true ? <LoadingComponent backgroundColor={'#fff'} /> :
                <View style={AppStyles.container}>
                    <TabBar_Title
                        title={'Ảnh - file - link'}
                        callBack={() => this.backToList()}
                    />
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity style={{ flex: 1, padding: 10, justifyContent: "center", alignItems: 'center', borderWidth: 0.5, borderColor: AppColors.gray }} onPress={() => this.setState({ checkbox: 'jpg' })}>
                            <Text style={{ color: this.state.checkbox === "jpg" ? AppColors.back : AppColors.gray }} >ẢNH</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, padding: 10, justifyContent: "center", alignItems: 'center', borderWidth: 0.5, borderColor: AppColors.gray }} onPress={() => this.setState({ checkbox: 'file' })}>
                            <Text style={{ color: this.state.checkbox === "file" ? AppColors.back : AppColors.gray }} >FILE</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, padding: 10, justifyContent: "center", alignItems: 'center', borderWidth: 0.5, borderColor: AppColors.gray }} onPress={() => this.setState({ checkbox: 'link' })}>
                            <Text style={{ color: this.state.checkbox === "link" ? AppColors.back : AppColors.gray }} >LINK</Text>
                        </TouchableOpacity>
                    </View>
                    {this.state.checkbox === 'jpg' ?
                        this.ViewIMG()
                        : this.state.checkbox === 'file' ?
                            this.ViewFILE()
                            : this.ViewLINK()
                    }

                </View >
        );
    };
    ViewIMG = () => {
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={_styles.imageGrid}>
                    {this.state.ListViewIMG.length > 0 ? this.state.ListViewIMG.map((item, i) => {
                        return (
                            <View>
                                <TouchableOpacity onPress={() => this.onViewImage(item)}>
                                    <Image style={_styles.image} source={{ uri: this.FileAttackments.renderImage(item.FileExtension, item.AttachmentGuid) }} />
                                </TouchableOpacity >
                            </View>
                        );
                    })
                        :
                        <View style={{ alignItems: "center", flex: 1 }}><Text>Không có dữ liệu</Text></View>
                    }
                </View>
            </ScrollView>
        )
    }
    ViewFILE = () => {
        return (
            this.state.ListViewFILE.length > 0 ?
                <ScrollView style={{ flex: 1 }}>
                    {this.state.ListViewFILE.map((item, i) => {
                        return (
                            <View style={_styles.imageGrid}>
                                <View>
                                    <TouchableOpacity onPress={() => this.onViewImage(item)}>
                                        <Image style={_styles.image} source={{ uri: this.FileAttackments.renderImage(item.FileExtension, item.AttachmentGuid) }} />
                                    </TouchableOpacity >
                                </View>
                            </View>
                        )
                    })}
                </ScrollView>
                :
                <View style={{ alignItems: "center", flex: 1 }}><Text>Không có dữ liệu</Text></View>
        )
    }
    ViewLINK = () => {
        return (
            <ScrollView style={{ flex: 1 }}>
                {this.state.ListViewLINK.length > 0 ? this.state.ListViewLINK.map((item, i) => {
                    return (
                        <TouchableOpacity style={{ padding: 10, borderBottomWidth: 0.5, borderBottomColor: AppColors.gray }}>
                            <Text style={[AppStyles.TextMessage, { color: AppColors.Maincolor }]} onPress={() => Linking.openURL(item.Message)}>{item.Message.substring(0, 40) + "..."}</Text>
                        </TouchableOpacity >
                    );
                })
                    :
                    <View style={{ alignItems: "center", flex: 1 }}><Text>Không có dữ liệu</Text></View>
                }
            </ScrollView>
        )
    }
    backToList() {
        Actions.pop();
    }
    onViewImage(obj) {
        Actions.viewAllImage({ ChatboxGroupGuid: this.props.ChatboxGroupGuid, ItemImage: obj });
    }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps,)(ListImage_Component);
