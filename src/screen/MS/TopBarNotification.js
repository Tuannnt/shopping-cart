import React, { Component } from 'react'
import { View, Button } from 'react-native';
import { connect } from 'react-redux';
import Notification from './Notification';
import { width } from 'react-native-dimension';
const styles = {
  notificationsContainerStyle: {
    width:'100%'
  },
}
class TopBarNotification extends Component {
  render() {
    return (
      <View style={styles.notificationsContainerStyle}>
        {this.props.Listview.map((notification, index) => {
          return (
            <Notification
              key={notification.id}
              offset={-(80 * (index + 1))}
              delay={index === 0 ? 80 : (index + 1) * 20}
              removeNotification={id => this.props.removeNotification(id)}
              {...notification}
             />
          )
        })}
      </View>
    )
  }
}
const mapStateToProps = state => ({
});
const mapDispatchToProps = {
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(TopBarNotification);