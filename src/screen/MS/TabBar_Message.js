import React, { Component } from 'react';
import { Keyboard, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { AppStyles, getStatusBarHeight } from '@theme';
import { Actions } from 'react-native-router-flux';
import { Header, Icon } from 'react-native-elements';
import { connect } from 'react-redux'

class TabBar_Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            callBack: this.props.callBack
        };

    }
    render() {
        const {
            hideLeft,
            title,
        } = this.props;

        return (
            //view tabbar
            <View>
                <Header
                    statusBarProps={AppStyles.statusbarProps}
                    {...AppStyles.headerProps}
                    leftComponent={this.renderLeftComponent(hideLeft)}
                    centerComponent={this.renderTitle(title)}
                    rightComponent={this.renderRightComponent(title)}
                />
            </View>
        );
    }

    renderLeftComponent = (hideLeft, leftIcon, leftIconSet) => {
        return (
            <TouchableOpacity style={[styles.squareContainer, { alignItems: 'flex-start', justifyContent: 'center', width: "100%", height: 50, paddingLeft: 10 }]}
                onPress={() => this.onPressback()}>
                {!hideLeft && <Icon name={'chevron-thin-left'} type='entypo' size={25} color='black' />}
            </TouchableOpacity>
        );
    }
    renderTitle = (title, subTitle) => {
        return (
            <View style={[styles.squareContainer, { alignItems: 'flex-start', justifyContent: 'center', width: "100%", height: 50 }]}>
                <Text style={[AppStyles.headerStyle, { fontWeight: 'bold', marginLeft: 0, fontSize: 18, color: 'black' }]}>{title}</Text>
            </View>
        );
    }
    renderRightComponent = (title, rightIcon, rightIconSet) => {
        if (!rightIcon) {
            rightIconSet = 'feather'
        }
        return (
            <TouchableOpacity style={[styles.squareContainer, { alignItems: 'flex-end', justifyContent: 'center', width: "100%", height: 50, paddingRight: 10 }]} onPress={() => this.onPressInformation(title, this.props.ChatboxGroupGuid)}>

                <Icon name='more-horizontal' type={rightIconSet} size={30} />
            </TouchableOpacity>
        );
    }
    onPressback() {
        this.state.callBack();
    }
    onPressInformation(title, para) {
        Keyboard.dismiss();
        Actions.Information({ Name: title, guid: para });
    }
}

// CSS
const navHeight = Platform.OS === 'ios' ? 80 : 80 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
const styles = StyleSheet.create({
    squareContainer: {
        height: toolbarHeight,
        width: toolbarHeight,
    }
});
//Redux
const mapStateToProps = state => ({
})
//Using call para public all page
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(TabBar_Message);
