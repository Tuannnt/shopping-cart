import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, ScrollView, StyleSheet, FlatList, Dimensions } from 'react-native';
import { ListItem, Text, Body, } from 'native-base';
import { Header, SearchBar, Icon, Badge, Divider } from 'react-native-elements';
import { FlatGrid } from 'react-native-super-grid';
import { Actions } from 'react-native-router-flux';
import API_Orders from "../../../network/SM/API_Orders";
import API_HomeSM from "../../../network/SM/API_HomeSM";
import API_ReportCRM from "../../../network/SM/API_ReportCRM";
import TabBar from '../../component/TabBar';
import { ECharts } from "react-native-echarts-wrapper";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Combobox from '../../component/Combobox';
import { FuncCommon } from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';

var width = Dimensions.get('window').width; //full width
class Dashboard_KPI extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staticParam: {
                ModuleId: 'Customers',
                Type: '1',
                OptionDate: '30NT',
                StartDate: '',
                EndDate: '',
                DepartmentId: 'SFAAS2',
                CompaignId: 'CD018'
            },
            listDepartment: [],
            list: [],
            IsTab: 'Customers',
            listMenu: [],
            //số lượng khách hàng theo mối quan hệ
            optionPie: {
                // title: {
                //     text: 'Số lượng khách hàng theo mối quan hệ',
                //     left: 'center'
                // },
                tooltip: {
                    trigger: 'item',
                    formatter: '{b}: {c} ({d}%)'
                },
                legend: {
                    orient: 'horizontal',
                    left: 'center',
                    bottom: 0,
                    data: []
                },
                series: [
                    {
                        name: 'Số lượng',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '30%'],
                        data: [],
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            },
            //Biến động doanh số
            optionDepartments: {
                title: {
                    text: 'Biến động doanh số',
                    left: 'center'
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    containLabel: true
                },
                color: ['#FF6600', '#FCD202', '#B0DE09'],
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                legend: {
                    bottom: 0,
                    data: ['Doanh số', 'Doanh thu', 'Đã thu']
                },
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center'
                },
                xAxis: [
                    {
                        type: 'category',
                        axisTick: { show: false },
                        data: []
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: 'Doanh số',
                        type: 'bar',
                        barGap: 0,
                        data: []
                    },
                    {
                        name: 'Doanh thu',
                        type: 'bar',
                        data: []
                    },
                    {
                        name: 'Đã thu',
                        type: 'bar',
                        data: []
                    }
                ]
            },
            //Biến động doanh số trong chiến dịch
            optionLineCompaigns: {
                title: {
                    text: 'Biến động doanh số trong chiến dịch',
                    left: 'center'
                },
                grid: {
                    left: '10%',
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c}'
                },
                // legend: {
                //     left: 'left',
                //     data: ['Tổng số lượng']
                // },
                xAxis: {
                    type: 'category',
                    data: []
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    //name: 'Tổng số lượng',
                    data: [],
                    type: 'line',
                    smooth: true
                }]
            },
            //Thống kê cơ hội
            optionFunnelCompaigns: {
                title: {
                    text: 'Thống kê cơ hội',
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c}"
                },
                legend: {
                    bottom: 10,
                    data: []
                },

                series: [
                    {
                        name: 'Số lượng cơ hội:',
                        type: 'funnel',
                        min: 0,
                        max: 100,
                        left: '10%',
                        right: '10%',
                        minSize: '0%',
                        maxSize: '100%',
                        width: '80%',
                        //sort: 'descending',
                        gap: 2,
                        label: {
                            show: true,
                            position: 'inside'
                        },
                        // labelLine: {
                        //     length: 10,
                        //     lineStyle: {
                        //         width: 1,
                        //         type: 'solid'
                        //     }
                        // },
                        // itemStyle: {
                        //     borderColor: '#fff',
                        //     borderWidth: 1
                        // },
                        // emphasis: {
                        //     label: {
                        //         fontSize: 20
                        //     }
                        // },
                        data: []
                    }
                ]
            },
            //top 5 doanh thu sản phẩm
            optionPieDTofItems: {
                title: {
                    text: 'Top 5 doanh thu sản phẩm',
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{b}: {c} ({d}%)'
                },
                legend: {
                    orient: 'horizontal',
                    left: 'center',
                    bottom: 0,
                    data: []
                },
                series: [
                    {
                        name: 'Số lượng',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '30%'],
                        data: [],
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            },
            Dropdown: [
                {
                    value: '1',
                    text: 'Năm hiện tại'
                },
                {
                    value: '3',
                    text: 'Tháng hiện tại'
                },
                {
                    value: '30NT',
                    text: '30 ngày trước'
                },
                {
                    value: '4',
                    text: 'Tuần hiện tại'
                }
            ],
            loading: true,
        };
        this.listtabbarBotom = [
            {
                Title: "Khách hàng",
                Icon: "account",
                Type: "material-community",
                Value: "Customers",
                Checkbox: true
            },
            {
                Title: "Phòng ban",
                Icon: "fork",
                Type: "antdesign",
                Value: "Departments",
                Checkbox: false
            },
            {
                Title: "Chiến dịch",
                Icon: "calendar-month-outline",
                Type: "material-community",
                Value: "Compaigns",
                Checkbox: false
            },
            {
                Title: "Sản phẩm",
                Icon: "pie-chart",
                Type: "entypo",
                Value: "Items",
                Checkbox: false
            }
            // {
            //     Title: "",
            //     Icon: "dots-three-horizontal",
            //     Type: "entypo",
            //     Value: "combobox",
            //     Checkbox: false
            // }
        ]
    }

    loadMoreData() {
        this.nextPage();
    }
    componentDidMount() {
        //this.GetAllReport({ ModuleId: 'Customers', Type: '1', StartDate: this.state.staticParam.StartDate, EndDate: this.state.staticParam.EndDate });
    }
    //định dạng tiền tệ
    addPeriod(nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    onRefDashboard = (ref) => {
        if (ref) {
            this.chartDashboard = ref;
        }
    };
    onRefDepartments = (ref) => {
        if (ref) {
            this.chartDepartments = ref;
        }
    };
    onRefLineCompaigns = (ref) => {
        if (ref) {
            this.chartLineCompaigns = ref;
        }
    };
    onRefFunnelCompaigns = (ref) => {
        if (ref) {
            this.chartFunnelCompaigns = ref;
        }
    };
    onRefDTofItems = (ref) => {
        if (ref) {
            this.chartItems = ref;
        }
    };
    GetAllReport = (obj) => {
        API_ReportCRM.GetAllReport(obj).then(res => {
            if (obj.ModuleId === 'Customers' && obj.Type === '1') {
                var data = JSON.parse(res.data.data);
                var lstMenu = [];
                for (let i = 0; i < data.length; i++) {
                    lstMenu.push({
                        Title: data[i].StatusName,
                        Icon: data[i].IconMobile,
                        type: data[i].TypeIconMobile,
                        count: data[i].Value,
                        code: data[i].Id,
                    })
                }
                this.setState({ listMenu: lstMenu })

                //dữ liệu biểu đồ khách hàng
                var _option = this.state.optionPie;
                var data = JSON.parse(res.data.data);
                var seriesData = []
                for (let i = 0; i < data.length; i++) {
                    seriesData.push({ value: data[i].Value, name: data[i].StatusName })
                    _option.legend.data.push(data[i].StatusName)
                }
                _option.series[0].data = seriesData;
                this.setState({ optionPie: _option })
                this.chartDashboard.setOption(this.state.optionPie);
            }
            else if (obj.ModuleId === 'Departments') {
                //dữ liệu biểu đồ phòng ban
                var _option = this.state.optionDepartments;
                var data = JSON.parse(res.data.data);
                var xAxisdata = []
                var Data1 = []
                var Data2 = []
                var Data3 = []
                for (let i = 0; i < data.length; i++) {
                    xAxisdata.push(data[i].Name);
                    Data1.push(data[i].Value1);
                    Data2.push(data[i].Value2);
                    Data3.push(data[i].Value3);
                }
                _option.xAxis[0].data = xAxisdata;
                _option.series[0].data = Data1;
                _option.series[1].data = Data2;
                _option.series[2].data = Data3;
                this.state.optionDepartments = _option;
                this.setState({ optionDepartments: this.state.optionDepartments })
                this.chartDepartments.setOption(this.state.optionDepartments);
            }
            else if (obj.ModuleId === 'Compaigns' && obj.Type === '1') {
                //dữ liệu biểu đồ đường chiến dịch
                var _optionLine = this.state.optionLineCompaigns;
                var data = JSON.parse(res.data.data);
                var xAxisdata = []
                var Data1 = []
                for (let i = 0; i < data.length; i++) {
                    xAxisdata.push(data[i].Name);
                    Data1.push(data[i].Value);
                }
                _optionLine.xAxis.data = xAxisdata;
                _optionLine.series[0].data = Data1;
                this.state.optionLineCompaigns = _optionLine;
                this.setState({ optionLineCompaigns: this.state.optionLineCompaigns })
                this.chartLineCompaigns.setOption(this.state.optionLineCompaigns);
            }
            else if (obj.ModuleId === 'Compaigns' && obj.Type === '5') {
                //dữ liệu biểu đồ thống kê cơ hội
                var _optionFunnel = this.state.optionFunnelCompaigns;
                var data = JSON.parse(res.data.data);
                var legend = []
                var Seriesdata = []
                for (let i = 0; i < data.length; i++) {
                    legend.push(data[i].Name);
                    Seriesdata.push({ value: data[i].Value, name: data[i].Name });
                }
                _optionFunnel.legend.data = legend;
                _optionFunnel.series[0].data = Seriesdata;
                this.state.optionFunnelCompaigns = _optionFunnel;
                this.setState({ optionFunnelCompaigns: this.state.optionFunnelCompaigns })
                this.chartFunnelCompaigns.setOption(this.state.optionFunnelCompaigns);

            }
            else if (obj.ModuleId === 'Items' && obj.Type === '1') {
                //dữ liệu biểu đồ khách hàng
                var _option = this.state.optionPieDTofItems;
                var data = JSON.parse(res.data.data);
                var seriesData = []
                for (let i = 0; i < data.length; i++) {
                    seriesData.push({ value: data[i].Value, name: data[i].Name })
                    _option.legend.data.push(data[i].Name)
                }
                _option.series[0].data = seriesData;
                this.setState({ optionPieDTofItems: _option })
                this.chartItems.setOption(this.state.optionPieDTofItems);
            }
            this.setState({ loading: false });
        })
            .catch(error => {
                this.setState({ loading: false });
                console.log(error);
            });
    }

    clearOptionLineCompaigns = () => {
        if (this.chartLineCompaigns)
            this.chartLineCompaigns.clear();
    };
    clearOptionDepartments = () => {
        if (this.chartDepartments)
            this.chartDepartments.clear();
    };
    onClick(data) {
        this.setState({ IsTab: data })
        if (data == 'Customers') {
            this.state.listMenu = [];
            this.state.staticParam.ModuleId = data;
            this.state.staticParam.Type = '1';
            this.setState({
                staticParam: this.state.staticParam,
                loading: true
            });
            this.GetAllReport(this.state.staticParam);
        }
        else if (data == 'Departments') {
            //this.clearOptionLineCompaigns();
            this.state.staticParam.ModuleId = data;
            this.state.staticParam.Type = '1';
            this.setState({
                staticParam: this.state.staticParam,
                loading: true
            });
            this.GetAllReport(this.state.staticParam);
        }
        else if (data == 'Compaigns') {
            //this.clearOptionDepartments();
            this.state.staticParam.ModuleId = data;
            this.state.staticParam.Type = '1';
            this.setState({
                staticParam: this.state.staticParam,
                loading: true
            });
            this.GetAllReport({
                ModuleId: 'Compaigns',
                Type: '1',
                DepartmentId: this.state.staticParam.DepartmentId,
                CompaignId: this.state.staticParam.CompaignId,
                OptionDate: this.state.staticParam.OptionDate,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate
            });
            this.GetAllReport({
                ModuleId: 'Compaigns',
                Type: '5',
                DepartmentId: this.state.staticParam.DepartmentId,
                CompaignId: this.state.staticParam.CompaignId,
                OptionDate: this.state.staticParam.OptionDate,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate
            });
        }
        else if (data == 'Items') {
            this.state.staticParam.ModuleId = data;
            this.state.staticParam.Type = '1';
            this.setState({
                staticParam: this.state.staticParam,
                loading: true
            });
            this.GetAllReport(this.state.staticParam);
        }
        else if (data == 'combobox') {
            this.onActionCB();
        }
        else if (data === 'home') {
            Actions.app();
        }
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null && strDate != "" && strDate != undefined) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "-" + mm + "-" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    render() {
        return (
            <View style={[AppStyles.container]}>
                <TabBar
                    title={'Báo cáo KPI'}
                    BackModuleByCode={'SM'}
                />
                <TouchableOpacity style={[AppStyles.FormInput, { flexDirection: 'row', width: width / 2, marginLeft: 10 }]} onPress={() => this.onActionCombobox()}>
                    <Text style={[AppStyles.TextInput, this.state.dropdownName == "" ? { color: AppColors.gray, flex: 3 } : { flex: 3 }]}>{this.state.dropdownName !== "" ? this.state.dropdownName : "Chọn..."}</Text>
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Icon color='#888888'
                            name={'chevron-thin-down'} type='entypo'
                            size={20} />
                    </View>
                </TouchableOpacity>
                <Divider style={{ marginTop: 10 }} />
                {this.state.loading === true ?
                    <LoadingComponent backgroundColor={'#fff'} />
                    :
                    this.state.IsTab === 'Customers' ?
                        <ScrollView style={{ flexDirection: 'column' }}>
                            <FlatGrid
                                itemDimension={120}
                                items={this.state.listMenu}
                                style={styles.gridView}
                                renderItem={({ item, index }) => (
                                    <View style={{ borderRadius: 10, backgroundColor: '#FFFFFF', height: 70, borderColor: '#f6b801', borderWidth: 1 }}>
                                        <View style={[styles.itemContainerdetail]}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 1 }}>
                                                    <Icon
                                                        name={item.Icon}
                                                        type={item.type}
                                                        size={20}
                                                        color={AppColors.Maincolor}
                                                    />
                                                </View>
                                                <View style={{ flex: 1 }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 17, textAlign: 'right', paddingRight: 5 }}>{item.count}</Text>
                                                </View>
                                            </View>
                                            <View>
                                                <Text style={AppStyles.Textdefault}>{item.Title}</Text>
                                            </View>
                                        </View>
                                    </View>
                                )}
                            />
                            <Divider style={{ marginBottom: 10 }} />
                            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                                <Text style={[AppStyles.Titledefault, { textAlign: 'center' }]}>Số lượng khách hàng theo mối quan hệ</Text>
                                <View style={{ height: 500, width: width, paddingBottom: 20 }}>
                                    {
                                        (this.state.optionPie && this.state.optionPie.legend.data.length > 0 && this.state.optionPie.series[0].data.length > 0) ?
                                            <ECharts
                                                option={this.state.optionPie}
                                                ref={this.onRefDashboard}
                                            >
                                            </ECharts>
                                            : null
                                    }
                                </View>
                            </View>
                        </ScrollView>
                        : this.state.IsTab === 'Departments' ?
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ backgroundColor: '#fff', flex: 1 }}>
                                <View style={{ height: 450, width: width }}>
                                    {
                                        (this.state.optionDepartments && this.state.optionDepartments.series[0].data.length > 0) ?
                                            <ECharts
                                                option={this.state.optionDepartments}
                                                ref={this.onRefDepartments}
                                            >
                                            </ECharts>
                                            : null
                                    }
                                </View>
                            </ScrollView>
                            : this.state.IsTab === 'Compaigns' ?
                                <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={{ backgroundColor: '#fff', flex: 1 }}>
                                    <View style={{ height: 450, width: width }}>
                                        {
                                            (this.state.optionLineCompaigns && this.state.optionLineCompaigns.series[0].data.length > 0) ?
                                                <ECharts
                                                    option={this.state.optionLineCompaigns}
                                                    ref={this.onRefLineCompaigns}
                                                >
                                                </ECharts>
                                                : null
                                        }
                                    </View>
                                    <View style={{ height: 400, width: width }}>
                                        {
                                            // (this.state.optionFunnelCompaigns && this.state.optionFunnelCompaigns.series[0].data.length > 0) ?
                                            <ECharts
                                                option={this.state.optionFunnelCompaigns}
                                                ref={this.onRefFunnelCompaigns}
                                            >
                                            </ECharts>
                                            // : null
                                        }
                                    </View>
                                </ScrollView>
                                : this.state.IsTab === 'Items' ?
                                    <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={{ backgroundColor: '#fff', flex: 1 }}>
                                        <View style={{ height: 500, width: width, paddingBottom: 20 }}>
                                            {
                                                (this.state.optionPieDTofItems && this.state.optionPieDTofItems.legend.data.length > 0 && this.state.optionPieDTofItems.series[0].data.length > 0) ?
                                                    <ECharts
                                                        option={this.state.optionPieDTofItems}
                                                        ref={this.onRefDTofItems}
                                                    >
                                                    </ECharts>
                                                    : null
                                            }
                                        </View>
                                    </ScrollView>

                                    : null
                }
                <Combobox
                    value={this.state.staticParam.OptionDate}
                    TypeSelect={"single"}// single or multiple
                    callback={this.ChangeDropDown}
                    data={this.state.Dropdown}
                    nameMenu={'Chọn'}
                    eOpen={this.openCombobox}
                    position={'top'}
                />
                <View style={[AppStyles.StyleTabvarBottom]}>
                    <TabBarBottomCustom
                        ListData={this.listtabbarBotom}
                        onCallbackValueBottom={(callback) => this.onClick(callback)}
                    />
                </View>
            </View>
        )
    }
    ChangeDropDown = (value) => {
        var date = new Date();
        this.state.staticParam.OptionDate = value.value;
        if (value.value === '30NT') {
            this.state.staticParam.StartDate = FuncCommon.ConDate(new Date(date.getFullYear(), date.getMonth(), date.getDate() - 30), 2);
            this.state.staticParam.EndDate = FuncCommon.ConDate(date, 2);
            this.setState({
                dropdownName: value.text,
                staticParam: this.state.staticParam
            });
        }
        else if (value.value === '1') {
            this.state.staticParam.StartDate = FuncCommon.ConDate(new Date(date.getFullYear(), 0, 1), 2);
            this.state.staticParam.EndDate = FuncCommon.ConDate(new Date(date.getFullYear(), 11, 31), 2);
            this.setState({
                dropdownName: value.text,
                staticParam: this.state.staticParam
            });
        }
        else if (value.value === '3') {
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            this.state.staticParam.StartDate = FuncCommon.ConDate(new Date(firstDay), 2);
            this.state.staticParam.EndDate = FuncCommon.ConDate(new Date(lastDay), 2);
            this.setState({
                dropdownName: value.text,
                staticParam: this.state.staticParam
            });
        }
        else if (value.value === '4') {
            var first = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1)
            var last = new Date(date.getFullYear(), date.getMonth(), date.getDate() + (7 - date.getDay()))
            this.state.staticParam.StartDate = FuncCommon.ConDate(new Date(first), 2);
            this.state.staticParam.EndDate = FuncCommon.ConDate(new Date(last), 2);
            this.setState({
                dropdownName: value.text,
                staticParam: this.state.staticParam
            });
        }
        this.onClick(this.state.IsTab);
    }
    _openCombobox() { }
    openCombobox = (d) => {
        this._openCombobox = d;
    }
    onActionCombobox() {
        this._openCombobox();
    }
}

const styles = StyleSheet.create({
    itemContainerdetail: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 17,
        flexDirection: 'column'
    },
    FormALl: {
        fontSize: 14,
        marginLeft: 10,
        alignItems: 'flex-end',
        flex: 1
    },
    hidden: { display: 'none' }
});
//Redux
const mapStateToProps = state => ({
})
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard_KPI);