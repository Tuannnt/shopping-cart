import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  FlatList,
  Keyboard,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Button, Icon, Divider} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Container} from 'native-base';
import {FuncCommon} from '../../../utils';
import DatePicker from 'react-native-datepicker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import _ from 'lodash';
import {AppStyles, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RequiredText from '../../component/RequiredText';
import RNPickerSelect from 'react-native-picker-select';
import LoadingComponent from '../../component/LoadingComponent';
import {API_QuotationRequest, API_WorkShiftChange} from '@network';
import controller from './controller';
import listCombobox from './listCombobox';
import Combobox from '../../component/Combobox';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';
import ComboboxV2 from '../../component/ComboboxV2';
import Toast from 'react-native-simple-toast';

const headerTable = listCombobox.headerTable;
const ListProductGroupID = listCombobox.ListProductGroupID;
class AddQuotationRequestComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [{Origin: 'VIET NAM'}],
      Type: 'A',
      ConfirmBy: '',
      RequireDate: new Date(),
      Quantity: 0,
      search: '',
      loading: false,
      ListEmployee: [],
      workShiftData: [],
      ListObjectKH: [],
      CustomerName: '',
      ProductGroupId: 2,
      listOrder: [],
      ListUnits: [],
      Attachment: [],
    };
    this.setDate = this.setDate.bind(this);
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  componentDidMount = () => {
    Promise.all([
      this.getPeople_Quotation(),
      this.getAllObjectKH(),
      this.getAllOrder(),
      this.GetUnitsAllJExcel(),
    ]);
    const {itemData, rowData = []} = this.props;
    if (itemData) {
      this.setState({
        RequireDate: FuncCommon.ConDate(new Date(itemData.RequireDate), 0),
        TimeRequired: itemData.TimeRequired
          ? FuncCommon.ConDate(new Date(itemData.TimeRequired), 0)
          : undefined,
        Title: itemData.Title,
        CustomerId: itemData.CustomerId,
        FullName: itemData.EmployeeName,
        ProductGroupId: itemData.ProductGroupId,
        EmployeeId: itemData.EmployeeId,
        Address: itemData.Address,
        CustomerName: itemData.CustomerName,
        rows: [...rowData].map(row => {
          if (!row.Quantity || row.Quantity === 'null') {
            row.Quantity = '0';
          } else {
            row.Quantity += '';
          }
          if (!row.AmountOc || row.AmountOc === 'null') {
            row.AmountOc = '0';
          } else {
            row.AmountOc += '';
          }
          if (!row.UnitPriceNet || row.UnitPriceNet === 'null') {
            row.UnitPriceNet = '0';
          } else {
            row.UnitPriceNet += '';
          }
          return row;
        }),
        isEdit: false,
        QuotationRequestNo: itemData.QuotationRequestNo,
      });
    } else {
      this.getNumberAuto();
      this.setState({
        RequireDate: FuncCommon.ConDate(new Date(), 0),
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        EmployeeId: global.__appSIGNALR.SIGNALR_object.USER.EmployeeId,
      });
    }
  };
  componentWillUnmount = () => {
    clearTimeout(this.time);
  };
  GetUnitsAllJExcel = () => {
    let data = [];
    controller.GetUnitsAllJExcel(res => {
      data = res.map(item => ({
        value: item.UnitId,
        label: item.UnitName,
      }));
      data.unshift({
        value: null,
        label: 'Chọn',
      });
      this.setState({ListUnits: data});
    });
  };
  getAllOrder = (callback, search = '') => {
    clearTimeout(this.time);
    // debounce call api
    this.time = setTimeout(() => {
      controller.getAllOrder(search, rs => {
        console.log(rs);
        let data = JSON.parse(rs).map(item => ({
          value: item.id,
          text: item.name,
        }));
        data.unshift({
          value: null,
          text: 'Chọn sản phẩm',
        });
        if (callback) {
          callback(data);
        } else {
          this.setState({listOrder: data});
        }
      });
    }, 300);
  };
  getAllObjectKH = () => {
    controller.getAllObjectKH('', rs => {
      if (rs !== null) {
        this.setState({ListObjectKH: rs, loading: false});
      }
    });
  };
  getPeople_Quotation = () => {
    controller.getPeople_Quotation(rs => {
      if (!rs) {
        return;
      }
      let data = rs.map(item => ({
        value: item.value,
        label: item.text,
      }));
      data.unshift({
        value: null,
        label: 'Chọn nhân viên KD',
      });
      this.setState({ListEmployee: data});
    });
  };
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_YCBG',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      this.setState({isEdit: data.IsEdit, QuotationRequestNo: data.Value});
    });
  };
  onChangeType(value) {
    this.setState({
      Type: value,
    });
  }
  setNote(Title) {
    this.setState({Title});
  }
  setQuantity(Quantity) {
    this.setState({Quantity});
  }
  onClickBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  Submit() {
    const {Attachment} = this.state;
    let lstDetails = [...this.state.rows].map(row => {
      return {
        ...row,
        ParentGuid: null,
        Quantity: +row.Quantity,
        UnitPriceNet: +row.UnitPriceNet,
        AmountOc: +row.AmountOc,
      };
    });
    if (!this.state.QuotationRequestNo) {
      Toast.showWithGravity(
        'Bạn chưa điền mã phiếu',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.CustomerId) {
      Toast.showWithGravity(
        'Bạn chưa chọn mã khách hàng',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!this.state.CustomerName) {
      Toast.showWithGravity(
        'Bạn chưa điền tên khách hàng',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let auto = {
      Value: this.state.QuotationRequestNo,
      IsEdit: this.state.isEdit,
    };
    if (this.props.itemData) {
      this.Update();
      return;
    }
    let _obj = {
      FileAttachments: [],
      CustomerId: this.state.CustomerId,
      ProductGroupId: this.state.ProductGroupId,
      EmployeeId: this.state.EmployeeId,
      RequireDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.RequireDate, 99),
        2,
      ),
      TimeRequired: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.TimeRequired, 99),
        2,
      ),
      IsVat: 1,
      CurrencyId: 'VND',
      EmployeeName: this.state.EmployeeName,
      WorkPhone: this.state.WorkPhone,
      CustomerGuid: this.state.CustomerGuid,
      CustomerName: this.state.CustomerName,
      Address: this.state.Address,
      TaxCode: this.state.TaxCode,
      CurrencyRate: 1,
      Status: 0,
      QuotationRequestNo: this.state.QuotationRequestNo,
      Title: this.state.Title,
    };
    let _data = new FormData();
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    _data.append('Insert', JSON.stringify(_obj));
    _data.append('Details', JSON.stringify(lstDetails));
    _data.append('auto', JSON.stringify(auto));
    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_QuotationRequest.Insert(_data)
      .then(rs => {
        console.log(rs);
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Toast.showWithGravity(_rs.message, Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Thêm mới thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onClickBack();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }
  setDate(Date) {
    this.setState({RequireDate: Date});
  }
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  Update = () => {
    let auto = {
      Value: this.state.QuotationRequestNo,
      IsEdit: this.state.isEdit,
    };
    let _obj = {
      ...this.props.itemData,
      FileAttachments: [],
      CustomerId: this.state.CustomerId,
      ProductGroupId: this.state.ProductGroupId,
      EmployeeId: this.state.EmployeeId,
      RequireDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.RequireDate, 99),
        2,
      ),
      TimeRequired: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.TimeRequired, 99),
        2,
      ),
      EmployeeName: this.state.EmployeeName,
      CustomerGuid: this.state.CustomerGuid,
      CustomerName: this.state.CustomerName,
      Address: this.state.Address,
      TaxCode: this.state.TaxCode,
      QuotationRequestNo: this.state.QuotationRequestNo,
      Title: this.state.Title,
    };
    let _rows = _.cloneDeep(this.state.rows).map(x => {
      x.AmountOc = x.AmountOc ? x.AmountOc?.replace(/\./g, '') : 0;
      x.UnitPriceNet = x.UnitPriceNet ? x.UnitPriceNet?.replace(/\./g, '') : 0;
      x.Quantity = x.Quantity ? x.Quantity?.replace(/\./g, '') : 0;
      x.Amount = !x.Amount || x.Amount === 'null' ? 0 : x.Amount;
      return x;
    });
    let _data = new FormData();
    _data.append('Insert', JSON.stringify(_obj));
    _data.append('Details', JSON.stringify(_rows));
    _data.append('DeleteAttach', JSON.stringify([]));
    _data.append('ListDetailDelete', JSON.stringify([]));
    _data.append('auto', JSON.stringify(auto));
    API_QuotationRequest.Update(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Toast.showWithGravity('Có lỗi xảy ra', Toast.SHORT, Toast.CENTER);
        } else {
          Toast.showWithGravity(
            'Chỉnh sửa thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onClickBack();
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{Origin: 'VIET NAM'}];
    } else {
      data.push({Origin: 'VIET NAM'});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({
      rows: res,
      typeTime: null,
      currentIndex: null,
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
    });
  };
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  _openComboboxOrder() {}
  openComboboxOrder = d => {
    this._openComboboxOrder = d;
  };
  openOrders(index) {
    this.setState({currentIndexItem: index}, () => {
      this._openComboboxOrder();
    });
  }
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: 200}]}
          onPress={() => {
            if (this.props.seeDetail) {
              return;
            }
            this.openOrders(index);
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.ItemId,
                {color: 'black', fontSize: 14},
              ]}>
              {row.ItemId ? row.ItemId : ''}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <Icon
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.ItemName}
            autoCapitalize="none"
            onChangeText={ItemName => {
              this.handleChangeRows(ItemName + '', index, 'ItemName');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Origin}
            autoCapitalize="none"
            onChangeText={Origin => {
              this.handleChangeRows(Origin + '', index, 'Origin');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <RNPickerSelect
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'UnitId');
            }}
            items={this.state.ListUnits}
            value={row.UnitId}
            placeholder={{}}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Quantity}
            autoCapitalize="none"
            onChangeText={Quantity => {
              this.handleChangeRows(Quantity + '', index, 'Quantity');
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td_custom, {width: 80}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    const {
      isEdit,
      QuotationRequestNo,
      CustomerGuid,
      ListObjectKH,
      CustomerId,
    } = this.state;
    if (this.state.loading) {
      return <LoadingComponent />;
    }
    return (
      <Container>
        <View style={{flex: 30}}>
          <View style={{flex: 40}}>
            {this.props.itemData ? (
              <TabBar_Title
                title={'Sửa yêu cầu dự toán'}
                callBack={() => {
                  this.onClickBack();
                }}
              />
            ) : (
              <TabBar_Title
                title={'Thêm yêu cầu dự toán'}
                callBack={() => {
                  this.onClickBack();
                }}
              />
            )}
            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Mã phiếu <RequiredText />{' '}
                    </Text>
                  </View>
                  {isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={QuotationRequestNo}
                      autoCapitalize="none"
                      onChangeText={Title =>
                        this.onChangeState('QuotationRequestNo', Title)
                      }
                    />
                  )}
                  {!isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                        {QuotationRequestNo}
                      </Text>
                    </View>
                  )}
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>Ngày yêu cầu</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.RequireDate}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setDate(date)}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>Hạn yêu cầu</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.TimeRequired}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date =>
                          this.onChangeState('TimeRequired', date)
                        }
                      />
                    </View>
                  </View>
                </View>
                {/* <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người tạo phiếu</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View> */}
                <View style={{padding: 10}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Mã khách hàng <RequiredText />
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={[AppStyles.FormInput, {flexDirection: 'row'}]}
                    onPress={() => this.onActionCustomer()}>
                    <Text
                      style={[
                        AppStyles.TextInput,
                        CustomerId == ''
                          ? {color: AppColors.gray, flex: 3}
                          : {flex: 3},
                      ]}>
                      {CustomerId !== '' ? CustomerId : 'Chọn đối tượng...'}
                    </Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Icon
                        color="#888888"
                        name={'chevron-thin-down'}
                        type="entypo"
                        size={20}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Tên Khách hàng <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.CustomerName}
                    onChangeText={CustomerName =>
                      this.onChangeState('CustomerName', CustomerName)
                    }
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Địa chỉ</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.Address}
                    onChangeText={Address =>
                      this.onChangeState('Address', Address)
                    }
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>Nhân viên KD</Text>
                  <View
                    style={{
                      ...AppStyles.FormInputPicker,
                      justifyContent: 'center',
                    }}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value =>
                        this.onChangeState('EmployeeId', value)
                      }
                      items={this.state.ListEmployee || []}
                      placeholder={{}}
                      value={this.state.EmployeeId}
                      style={picker}
                      useNativeAndroidPickerStyle={false}
                    />
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>Loại sản phẩm</Text>
                  <View
                    style={{
                      ...AppStyles.FormInputPicker,
                      justifyContent: 'center',
                    }}>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value =>
                        this.onChangeState('ProductGroupId', value)
                      }
                      items={ListProductGroupID || []}
                      placeholder={{}}
                      value={this.state.ProductGroupId}
                      style={picker}
                      useNativeAndroidPickerStyle={false}
                    />
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Nội dung</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.Title}
                    onChangeText={Title => this.setNote(Title)}
                  />
                </View>

                {/* Table */}
                {/* <View style={{marginTop: 5}}>
                  <View style={{width: 150, marginLeft: 10}}>
                    <Button
                      title="Thêm dòng"
                      type="outline"
                      size={15}
                      onPress={() => this.addRows()}
                      buttonStyle={{padding: 5, marginBottom: 5}}
                      titleStyle={{marginLeft: 5}}
                      icon={
                        <Icon
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>

                  <ScrollView horizontal={true}>
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        {headerTable.map(item => (
                          <TouchableOpacity
                            key={item.title}
                            style={[AppStyles.table_th, {width: item.width}]}>
                            <Text style={AppStyles.Labeldefault}>
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        ))}
                      </View>
                      <View>
                        {this.state.rows.length > 0 ? (
                          <View>
                            <FlatList
                              data={this.state.rows}
                              renderItem={({item, index}) => {
                                return this.itemFlatList(item, index);
                              }}
                              keyExtractor={(rs, index) => index.toString()}
                            />
                            {this.rowTotal()}
                          </View>
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'left'},
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View> */}
              </View>
            </ScrollView>
            {this.state.modalPickerTimeVisibleDate && (
              <DateTimePickerModal
                locale="vi-VN"
                cancelTextIOS="Huỷ"
                confirmTextIOS="Chọn"
                headerTextIOS="Chọn thời gian"
                isVisible={this.state.modalPickerTimeVisibleDate}
                mode="date"
                onConfirm={this.handleConfirmTime}
                onCancel={this.hideDatePicker}
              />
            )}
          </View>
        </View>
        <View>
          {/* attachment */}
          {!this.props.itemData && (
            <View
              style={{
                flexDirection: 'column',
                padding: 10,
                marginBottom: 15,
              }}>
              <View style={{flexDirection: 'row', marginBottom: 5}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                  <Text style={{color: 'red'}}> </Text>
                </View>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    flexDirection: 'row',
                  }}
                  onPress={() => this.openAttachment()}>
                  <Icon
                    name="attachment"
                    type="entypo"
                    size={15}
                    color={AppColors.ColorAdd}
                  />
                  <Text
                    style={[
                      AppStyles.Labeldefault,
                      {color: AppColors.ColorAdd},
                    ]}>
                    {' '}
                    Chọn file
                  </Text>
                </TouchableOpacity>
              </View>
              <Divider style={{marginBottom: 10}} />
              {this.state.Attachment.length > 0
                ? this.state.Attachment.map((para, i) => (
                    <View
                      key={i}
                      style={[
                        styles.StyleViewInput,
                        {flexDirection: 'row', marginBottom: 3},
                      ]}>
                      <View
                        style={[AppStyles.containerCentered, {padding: 10}]}>
                        <Image
                          style={{
                            width: 30,
                            height: 30,
                            borderRadius: 10,
                          }}
                          source={{
                            uri: this.FileAttackments.renderImage(
                              para.FileName,
                            ),
                          }}
                        />
                      </View>
                      <TouchableOpacity
                        key={i}
                        style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={AppStyles.Textdefault}>
                          {para.FileName}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.containerCentered, {padding: 10}]}
                        onPress={() => this.removeAttactment(para)}>
                        <Icon
                          name="close"
                          type="antdesign"
                          size={20}
                          color={AppColors.ColorDelete}
                        />
                      </TouchableOpacity>
                    </View>
                  ))
                : null}
            </View>
          )}
        </View>
        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
        <View>
          <OpenPhotoLibrary
            callback={this.callbackLibarary}
            openLibrary={this.openLibrary}
          />
          <ComboboxV2
            callback={this.ChoiceAtt}
            data={listCombobox.ListComboboxAtt}
            eOpen={this.openCombobox_Att}
          />
        </View>
        {ListObjectKH.length === 0 ? null : (
          <Combobox
            value={CustomerGuid}
            TypeSelect={'single'}
            callback={this.ChangeCustomer}
            data={ListObjectKH}
            nameMenu={'Chọn khách hàng'}
            eOpen={this.openComboboxCustomer}
            position={'bottom'}
          />
        )}
        {/* {this.state.listOrder.length > 0 && (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeOrder}
            data={this.state.listOrder}
            nameMenu={'Chọn'}
            eOpen={this.openComboboxOrder}
            position={'bottom'}
            value={null}
            callback_SearchPaging={(callback, textsearch) =>
              this.getAllOrder(callback, textsearch)
            }
          />
        )} */}
      </Container>
    );
  }
  rowTotal = () => {
    const {rows} = this.state;
    const {list} = listCombobox;
    return (
      <View style={{flexDirection: 'row'}}>
        {list.map((item, index) => {
          if (item.id === 'quantity') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text style={[AppStyles.Textdefault, styles.totalText]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.Quantity || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          return (
            <TouchableOpacity
              key={index}
              style={[
                AppStyles.table_td_custom,
                {width: item.width, backgroundColor: '#f5f2f2'},
              ]}
            />
          );
        })}
      </View>
    );
  };
  ChangeOrder = async rs => {
    // const { currentIndexItem, rows } = this.state;
    // let { value } = rs;
    // let data = [];
    // await API_QuotationRequest.GetItemsByItemId({ search: value || '' })
    //   .then(res => {
    //     let obj = {};
    //     const rs = JSON.parse(res.data.data);
    //     obj = { ItemName: rs.VenderName, Quantity: rs.Quantity + '' };
    //     data = rows.map((row, index) => {
    //       if (currentIndexItem === index) {
    //         row.ItemId = value;
    //         row.ItemName = obj.ItemName || '';
    //         row.Quantity = obj.Quantity || '0';
    //       }
    //       return row;
    //     });
    //   })
    //   .catch(error => {
    //     this.setState({ loading: false });
    //     console.log(error.data.data);
    //   });
    // this.setState({ rows: data, currentIndexItem: null });
  };
  _openComboboxCustomer() {}
  openComboboxCustomer = d => {
    this._openComboboxCustomer = d;
  };
  onActionCustomer() {
    this._openComboboxCustomer();
  }
  ChangeCustomer = rs => {
    if (rs.value) {
      controller.getKHbyCustomerId(rs.value, res => {
        this.setState({
          CustomerGuid: res.CustomerGuid,
          CustomerId: rs.value,
          Address: res.Address,
          TaxCode: res.TaxCode,
          DebtDay: res.DebtDay,
          Email: res.Email,
          CustomerName: res.CustomerName,
          loading: false,
        });
      });
    }
  };
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'arrowleft'}
          type="antdesign"
          size={30}
          onPress={() => this.onPressBack()}
        />
      </View>
    );
  }
  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  onPressBack() {
    Actions.Registereats();
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddQuotationRequestComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
const picker = {
  inputIOS: {
    padding: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    padding: 10,
    color: 'black',
    fontSize: 14,
    paddingRight: 15, // to ensure the text is never behind the icon
    borderRadius: 2,
  },
};
