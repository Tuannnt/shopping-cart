import {AppStyles, AppColors} from '@theme';
export default {
  OrderStatusData: [
    {
      value: '0',
      text: 'Nháp',
    },
    {
      value: '1',
      text: 'Chờ phản hồi',
    },
    {
      value: '2',
      text: 'Đã phản hồi',
    },
    {
      value: '3',
      text: 'Đặt hàng',
    },
    {
      value: '4',
      text: 'Không đặt hàng',
    },
    {
      value: '5',
      text: 'Chờ xử lý',
    },
    {
      value: '6',
      text: 'Đã xử lý',
    },
  ],
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Mã sản phẩm', width: 150},
    {title: 'Tên sản phẩm', width: 250},
    {title: 'Xuât xứ', width: 150},
    {title: 'ĐVT', width: 100},
    {title: 'S.Lượng SO', width: 100},
    {title: 'Giá COST', width: 100},
    {title: '% LN mong muốn', width: 100},
    {title: '% CP bán hàng', width: 100},
    {title: '% CP tài chính', width: 100},
    {title: '% QLP', width: 100},
    {title: '% CSKH', width: 100},
    {title: 'Chi phí khác', width: 100},
    {title: 'Đơn giá bán', width: 100},
    {title: 'Thành tiền trước thuế', width: 150},
    {title: '% VAT', width: 100},
    {title: 'VAT', width: 100},
    {title: 'Thành tiền sau VAT', width: 150},
    {title: 'Ngày cần hàng', width: 150},
    {title: 'Ghi chú', width: 200},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
};
