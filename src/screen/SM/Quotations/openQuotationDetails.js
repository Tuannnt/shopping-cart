import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Text,
  Keyboard,
  ScrollView,
  Alert,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import { Icon, Header } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_Orders from '../../../network/SM/API_Orders';
import API from '../../../network/API';
import { DatePicker, Container, Item, Label, Picker } from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import { WebView } from 'react-native-webview';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import { FuncCommon } from '../../../utils';
class openQuotationDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: props.item,
    };
  }
  render() {
    const TotalCosts =
      +this.state.model.UnitPriceNet +
      +this.state.model.SellExpenses +
      +this.state.model.FinancialExpenses +
      +this.state.model.ManagementFee +
      +this.state.model.CSKH +
      +this.state.model.OtherCosts +
      this.state.model.Profit;
    return (
      <View
        style={{
          flex: 1,
          fontSize: 11,
          fontFamily: Fonts.base.family,
          backgroundColor: 'white',
        }}>
        <TabBar_Title
          title={'Chi tiết sản phẩm'}
          callBack={() => Actions.pop()}
        />
        <ScrollView>
          <View style={{ flex: 1 }}>
            <View style={{ padding: 20 }}>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Mã SPKH :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.ItemIdByProvider}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Tên SPKH :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.ItemNameByProvider}
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Mã sản phẩm :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.ItemId}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Tên sản phẩm :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.ItemName}
                  </Text>
                </View>
              </TouchableOpacity>
              {/* <TouchableOpacity style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{flex: 2}}>
                  <Text style={AppStyles.Labeldefault}>Tên tiếng việt :</Text>
                </View>
                <View style={{flex: 3}}>
                  <Text style={[AppStyles.Textdefault, {textAlign: 'right'}]}>
                    {this.state.model.ItemNameVN}
                  </Text>
                </View>
              </TouchableOpacity> */}
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Quy cách :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.Specification}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Tên máy :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.MachineId}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Mã bản vẽ :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.NoDrawing}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Xuất xứ:</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.Origin}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>ĐVT :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.UnitName}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Số lượng :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.Quantity)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Vật liệu :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.Material}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Giá Cost :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.UnitPriceCost)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    % Độ phức tạp và rủi ro :
                  </Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.RiskRate)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    Chi phí độ phức tạp và rủi ro :
                  </Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.Risk)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>%CP quản lý :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.CostsRate)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Chi phí quản lý :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.ManagementCosts)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    %Chi phí lập quy trình công nghệ :
                  </Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.ProcedureRate)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    Chi phí Lập quy trình công nghệ :
                  </Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.Procedures)}
                  </Text>
                </View>
              </TouchableOpacity>
              {/* <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Giá COST :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.UnitPriceNet)}
                  </Text>
                </View>
              </TouchableOpacity> */}
              {/* <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Số ĐH kinh doanh :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.OrderNumber}
                  </Text>
                </View>
              </TouchableOpacity> */}
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>% LN Mong muốn :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.ProfitPercent)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>LN Mong muốn :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.Profit)}
                  </Text>
                </View>
              </TouchableOpacity>
              {/* <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>% CP bán hàng :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.SellExpensesPercent)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>CP bán hàng :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.SellExpenses)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>% CP tài chính :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(
                      this.state.model.FinancialExpensesPercent,
                    )}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>CP tài chính :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.FinancialExpenses)}
                  </Text>
                </View>
              </TouchableOpacity> */}
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>CP vận chuyển :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.Cost)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>CP đóng gói :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.Pack)}
                  </Text>
                </View>
              </TouchableOpacity>
              {/* <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>%CPKH :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.CustomerPercent)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>CPKH :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.CustomerPrice)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>% QLP :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(
                      this.state.model.ManagementFeePercent,
                    )}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Quản lý phí :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.ManagementFee)}
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>% CSKH :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.CSKHPercent)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>CSKH :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.CSKH)}
                  </Text>
                </View>
              </TouchableOpacity> */}
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Chi phí khác :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.OtherCosts)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Đơn giá dự kiến :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.UnitPriceExpect)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>%NVKD :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.SaleRate)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>CP NVKD :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.Sale)}
                  </Text>
                </View>
              </TouchableOpacity>
              {/* <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Tổng giá :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(TotalCosts)}
                  </Text>
                </View>
              </TouchableOpacity> */}
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Đơn giá bán :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.UnitPrice)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Đơn giá ngoại tệ :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.UnitPriceOc)}
                  </Text>
                </View>
              </TouchableOpacity>
              {/* <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>LN Thực tế :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.ProfitActual)}
                  </Text>
                </View>
              </TouchableOpacity> */}
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>TT trước thuế :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.AmountNoVat)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    Thành tiền quy đổi trước VAT :
                  </Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.AmountNoVATOc)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>% VAT :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.Vatpercent)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>VAT :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.VatamountOc)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>% Chiết khấu :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.DiscountPercent)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    Số tiền chiết khấu :
                  </Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.DiscountOc)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>TT sau VAT :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.AmountVat)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Giá chênh :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.PriceDifference)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>% Giữ lại :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.KeepPercent)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    Thành tiền trả khách :
                  </Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.addPeriod(this.state.model.AmountCustomer)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày cần hàng :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {FuncCommon.ConDate(this.state.model.DeliveryDate, 0)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Labeldefault}>Ghi chú :</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                    {this.state.model.Note}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  onPressBack() {
    Actions.openQuotations();
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  closeComment() {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const mapStateToProps = state => ({ Loginname: state.user.account });

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(openQuotationDetails);
