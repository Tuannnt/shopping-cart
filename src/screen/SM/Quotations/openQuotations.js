import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Text,
  Keyboard,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  Dimensions,
  TouchableHighlight,
} from 'react-native';
import { Icon, Header, Divider } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_Orders from '../../../network/SM/API_Orders';
import API from '../../../network/API';
import { DatePicker, Container, ListItem, Label, Picker } from 'native-base';
import TabBarBottom from '../../component/TabBarBottom';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import { FuncCommon } from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import Toast from 'react-native-simple-toast';
import { Combobox, CustomView } from '@Component';

const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const ListApprovalStatus = [
  {
    value: 0,
    text: 'Nháp',
  },
  {
    value: 1,
    text: 'Chờ phản hồi',
  },
  {
    value: 2,
    text: 'Đã phản hồi',
  },
  {
    value: 3,
    text: 'Đặt hàng',
  },
  {
    value: 4,
    text: 'Không đặt hàng',
  },
  {
    value: 5,
    text: 'Chờ xử lý',
  },
  {
    value: 6,
    text: 'Đã xử lý',
  },
];
class openQuotations extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      staticParam: {
        QuotationGuid: '',
      },
      QuotationGuid: null,
      Data: [],
      listWofflowData: [],
      loading: false,
      isModalVisible: false,
      txtComment: '',
      checkInLogin: '',
      LoginName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
      Date: null,
      StartTime: null,
      EndTime: null,
      selected2: [],
      Reciever: [],
      Licars: [
        {
          CarName: 'Chọn xe',
          CarId: '',
        },
      ],
      loadingComponent: true,
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
      // ApprovalStatus: '0',
    };
  }
  componentDidMount(): void {
    this.Skill();
    this.setViewOpen('ViewTotal');
    (this.state.QuotationGuid = this.props.RecordGuid || this.props.RowGuid),
      this.getItem();
  }
  GetAll = () => {
    this.state.staticParam.QuotationGuid = this.state.QuotationGuid;
    API_Orders.QuotationDetails_ListDetail(this.state.staticParam)
      .then(res => {
        this.state.list = JSON.parse(res.data.data);
        this.setState({
          list: this.state.list,
          loadingComponent: false,
        });
      })
      .catch(error => {
        this.setState({
          loadingComponent: false,
        });
        console.log('===========> error');
        console.log(error);
      });
  };
  submit = () => {
    this.changeStatusQuotation();
    setTimeout(() => {
      let obj = {
        QuotationGuid: this.props.RecordGuid || this.props.RowGuid,
        Status: this.state.ApprovalStatus + '',
      };
      API_Orders.UpdateStatusQuotation(obj).then(res => {
        Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
        if (res.data.errorCode === 200) {
          this.getItem();
        }
      });
    }, 300);
  };
  getItem() {
    let id = {
      Id: this.props.RecordGuid || this.props.RowGuid || this.props.RowGuid,
    };
    API_Orders.Quotations_Items(id)
      .then(res => {
        var rs = JSON.parse(res.data.data);
        let checkin = {
          QuotationGuid: id.Id,
          WorkFlowGuid: rs.model.WorkFlowGuid,
        };
        API_Orders.CheckLoginQuotation(checkin)
          .then(_res => {
            this.setState({
              checkInLogin: _res.data.data,
              Data: rs.model,
              list: rs.detail,
              loadingComponent: false,
            });
          })
          .catch(error => {
            console.log(error.data);
          });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  keyExtractor = (item, index) => index.toString();
  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 1, duration: 333 },
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 0, duration: 333 },
    );
  };
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ ViewAll: !this.state.ViewAll });
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ ViewDetail: !this.state.ViewDetail });
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ ViewDetail: true, ViewAll: true });
    }
  };
  _openComboboxApprovalStatus() { }
  openComboboxApprovalStatus = d => {
    this._openComboboxApprovalStatus = d;
  };
  onActionApprovalStatus() {
    this._openComboboxApprovalStatus();
  }
  ChangeApprovalStatus = rs => {
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.state.ApprovalStatus = rs.value;
    this.setState({ ApprovalStatus: rs.value, ApprovalStatusName: rs.text });
  };
  changeStatusQuotation = () => {
    console.log('open');
    this._openViewisApproval();
  };
  _openViewisApproval() { }
  openViewisApproval = d => {
    this._openViewisApproval = d;
  };
  CustomView = () => {
    return (
      <CustomView eOpen={this.openViewisApproval}>
        <View style={{ width: DRIVER.width - 50 }}>
          <View style={{ alignItems: 'center', padding: 10 }}>
            <Text style={[AppStyles.Titledefault]}>Sửa tình trạng</Text>
          </View>
          <Divider />
          <View style={{ marginBottom: 10 }}>
            <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>Tình trạng</Text>
              </View>
              <TouchableHighlight
                style={[AppStyles.FormInput]}
                onPress={() => this.onActionApprovalStatus()}>
                <Text style={[AppStyles.TextInput, { color: 'black' }]}>
                  {this.state.ApprovalStatusName
                    ? this.state.ApprovalStatusName
                    : this.checkstatusBG(this.state.Data.QuotationStatus)}
                </Text>
              </TouchableHighlight>
            </View>
          </View>
          <Divider />
          <TouchableHighlight
            style={[
              AppStyles.FormInput,
              {
                backgroundColor: AppColors.ColorButtonSubmit,
                margin: 10,
                justifyContent: 'center',
              },
            ]}
            onPress={() => this.submit()}>
            <Text style={[AppStyles.TextInput, { color: '#fff' }]}>Lưu</Text>
          </TouchableHighlight>
        </View>
      </CustomView>
    );
  };
  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <View style={{ flex: 1 }}>
          <TabBar_Title
            title={'Chi tiết báo giá'}
            callBack={() => this.onPressBack()}
          // IconSubmit={true}
          // CallbackIconSubmit={() => this.changeStatusQuotation()}
          // FormAttachment={true}
          // CallbackFormAttachment={() => this.openAttachments()}
          />
          <ScrollView>
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  backgroundColor: '#bed9f6',
                }}
                onPress={() => this.setViewOpen('ViewAll')}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
                </View>
                <Animated.View
                  style={{
                    transform: [
                      { rotate: rotateStart_ViewAll },
                      { perspective: 4000 },
                    ],
                    paddingRight: 10,
                    paddingLeft: 10,
                  }}>
                  <Icon type={'feather'} name={'chevron-up'} />
                </Animated.View>
              </TouchableOpacity>
              <Divider />
              {this.state.ViewAll !== true ? null : (
                <View style={{ padding: 20 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>Số báo giá :</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.QuotationNo}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>
                        Mã khách hàng :
                      </Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.CustomerId}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>
                        Tên khách hàng :
                      </Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.CustomerName}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>Địa chỉ :</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.Address}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>
                        Người báo giá :
                      </Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.EmployeeName}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>Ngày báo giá :</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {FuncCommon.ConDate(this.state.Data.QuotationDate, 0)}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>Hạn báo giá :</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {FuncCommon.ConDate(this.state.Data.RequireDate, 0)}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>
                        Loại sản phẩm :
                      </Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.GroupName}
                      </Text>
                    </View>
                  </View>
                  {/* <View style={{flexDirection: 'row', marginTop: 10}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Loại TMC :</Text>
                    </View>
                    <View style={{flex: 2}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.checkTypeItem(this.state.Data.TypeItem)}
                      </Text>
                    </View>
                  </View> */}
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>Loại tiền :</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.CurrencyId +
                          ' - Tỷ giá : ' +
                          FuncCommon.addPeriod(this.state.Data.CurrencyRate)}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>VAT :</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.IsVat == 1
                          ? 'Bao gồm VAT'
                          : this.state.Data.IsVat == 0
                            ? 'Không gồm VAT'
                            : ''}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>Xử lý :</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.StatusWf}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>
                        Trạng thái báo giá :
                      </Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.checkstatusBG(this.state.Data.QuotationStatus)}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                      <Text style={AppStyles.Labeldefault}>Ghi chú :</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.Data.Note}
                      </Text>
                    </View>
                  </View>
                </View>
              )}
              <Divider />
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  backgroundColor: '#bed9f6',
                }}
                onPress={() => this.setViewOpen('ViewDetail')}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
                </View>
                <Animated.View
                  style={{
                    transform: [
                      { rotate: rotateStart_ViewDetail },
                      { perspective: 4000 },
                    ],
                    paddingRight: 10,
                    paddingLeft: 10,
                  }}>
                  <Icon type={'feather'} name={'chevron-up'} />
                </Animated.View>
              </TouchableOpacity>
              <Divider />
              {this.state.ViewDetail !== true ? (
                <View style={{ flex: 1 }} />
              ) : (
                <View style={{ flex: 1 }}>
                  <FlatList
                    style={{ marginBottom: 10, height: '100%' }}
                    refreshing={this.state.isFetching}
                    ref={ref => {
                      this.ListView_Ref = ref;
                    }}
                    ListEmptyComponent={this.ListEmpty}
                    //ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                    keyExtractor={item => item.BankGuid}
                    data={this.state.list}
                    renderItem={({ item, index }) => (
                      <ScrollView style={{}}>
                        <View>
                          <ListItem onPress={() => this.openView(item)}>
                            <View style={{ flex: 1 }}>
                              <Text style={[AppStyles.Titledefault]}>
                                {item.ItemName}
                              </Text>
                              <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                  <Text style={[AppStyles.Textdefault]}>
                                    Đơn vị tính: {item.UnitName}
                                  </Text>
                                  <Text style={[AppStyles.Textdefault]}>
                                    Số lượng SO: {item.Quantity}
                                  </Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                  <Text style={[AppStyles.Textdefault]}>
                                    LNTT:{' '}
                                    {FuncCommon.addPeriod(item.ProfitActual)}
                                  </Text>
                                  <Text style={[AppStyles.Textdefault]}>
                                    TT sau VAT:{' '}
                                    {FuncCommon.addPeriod(item.AmountVat)}
                                  </Text>
                                </View>
                              </View>
                            </View>
                          </ListItem>
                        </View>
                      </ScrollView>
                    )}
                    onEndReachedThreshold={0.5}
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                        tintColor="#f5821f"
                        titleColor="#fff"
                        colors={['red', 'green', 'blue']}
                      />
                    }
                  />
                </View>
              )}
            </View>
          </ScrollView>
        </View>
        <View style={[AppStyles.StyleTabvarBottom]}>
          <TabBarBottom
            onDelete={() => this.onDelete()}
            onAttachment={() => this.openAttachments()}
            keyCommentWF={{
              ModuleId: 88,
              RecordGuid: this.props.RecordGuid || this.props.RowGuid,
              Title: this.state.Data.QuotationNo,
              Type: 3,
            }}
            onSubmitWF={(callback, CommentWF) =>
              this.submitWorkFlow(callback, CommentWF)
            }
            Title={this.state.Data.QuotationNo}
            Permisstion={this.state.Data.Permisstion}
            checkInLogin={this.state.checkInLogin}
          />
        </View>
        <Combobox
          value={this.state.ApprovalStatus}
          TypeSelect={'single'}
          callback={this.ChangeApprovalStatus}
          data={ListApprovalStatus}
          nameMenu={'Chọn trạng thái'}
          eOpen={this.openComboboxApprovalStatus}
          position={'bottom'}
        />
        {this.CustomView()}
      </View>
    );
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'Quotation', ActionTime: new Date().getTime() });
  }
  onDelete = () => {
    let obj = { QuotationGuid: this.props.RecordGuid || this.props.RowGuid };
    this.setState({ loading: true });
    API_Orders.Quotations_Delete(obj)
      .then(response => {
        if (!response.data.Error) {
          this.setState({
            loading: false,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  };
  openView(item) {
    Actions.openQuotationDetails({ item: item });
  }
  openAttachments() {
    var obj = {
      ModuleId: '14',
      RecordGuid: this.props.RecordGuid || this.props.RowGuid,
      notEdit: this.state.checkInLogin === '1' ? false : true,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  onClick(data) {
    this.setState({ selectedTab: data, check: false });
    if (data == 'home') {
      Actions.app();
    } else if (data == 'List') {
      Actions.indexRegistercar();
    } else if (data == 'ok' || data == 'no') {
      this.setState({ isModalVisible: !this.state.isModalVisible });
    } else if (data == 'comment') {
      Actions.commentRegistercar({ QuotationGuid: this.props.QuotationGuid });
    }
  }
  handleEmail(data) {
    this.setState({ CommentWF: data });
  }
  submitWorkFlow(callback, CommentWF) {
    let obj = {
      QuotationGuid: this.state.Data.QuotationGuid,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
    };
    if (callback == 'D') {
      API_Orders.Approve_Quotations(obj)
        .then(rs => {
          console.log('====Lỗi');
          console.log(rs);
          if (rs.data.errorCode === 200) {
            this.closeComment();
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({ loading: false });
          console.log(error.data.data);
        });
    } else {
      API_Orders.NotApprove_Quotations(obj)
        .then(rs => {
          console.log('====Lỗi');
          console.log(rs);
          if (rs.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          this.setState({ loading: false });
          console.log(error.data.data);
        });
    }
  }
  closeComment() {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <Text style={{ color: '#000' }} />;
  };
  checkstatusBG = Status => {
    if (Status == 0) {
      return 'Nháp';
    } else if (Status == 1) {
      return 'Chờ phản hồi';
    } else if (Status == 2) {
      return 'Đã phản hồi';
    } else if (Status == 3) {
      return 'Đặt hàng';
    } else if (Status == 4) {
      return 'Không đặt hàng';
    } else if (Status == 5) {
      return 'Chờ xử lý';
    } else if (Status == 6) {
      return 'Đã xử lý';
    } else {
      return '';
    }
  };
  checkTypeItem = TypeItem => {
    if (TypeItem == 'K') {
      return 'Thường';
    } else if (TypeItem == 'T') {
      return 'Danh sách TMC.';
    } else if (TypeItem == 'U') {
      return 'Vỏ tủ điện';
    } else if (TypeItem == 'R') {
      return 'Danh sách tủ Rack';
    } else {
      return '';
    }
  };
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  FormALl: {
    fontSize: 14,
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
});
const mapStateToProps = state => ({ Loginname: state.user.account });

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(openQuotations);
