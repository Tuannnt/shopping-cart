import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {ListItem, SearchBar, Icon, Badge, Divider} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import API_Orders from '../../../network/SM/API_Orders';
import {AppStyles, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Combobox from '../../component/Combobox';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '../../../utils';
import {listCombobox} from './listCombobox';
import {controller} from './controller';
const SCREEN_WIDTH = Dimensions.get('window').width;
class indexQuotations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      name: '',
      refreshing: false,
      list: [],
      ListCountNoti: [],
      selectedTab: 'profile',
      isFetching: false,
      EvenFromSearch: false,
      StatusName: '',
    };
    this.staticParam = {
      EndDate: new Date(),
      StartDate: new Date(),
      CustomerGuid: null,
      QuotationStatus: ['0', '1', '2', '3', '4', '5', '6'],
      CurrentPage: 1,
      Length: 15,
      search: {
        value: '',
      },
      IsApproved: 1,
      ObjectId: null,
      QueryOrderBy: 'QuotationDate DESC',
      Status: 'W',
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
    this.onEndReachedCalledDuringMomentum = true;
  }
  loadMoreData() {
    if (this.Total > this.state.list.length) {
      this.nextPage();
    }
  }
  render() {
    return (
      <TouchableWithoutFeedback style={{flex: 1}}>
        <View style={styles.container}>
          <TabBar
            title={'Quản lý báo giá'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            BackModuleByCode={'SM'}
            // addForm={'quotations_Form'}
            // CallbackFormAdd={() => Actions.quotations_Form()}
          />
          <Divider />
          <View>
            {this.state.EvenFromSearch == true ? (
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventStartDate: true})}>
                      <Text>
                        {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventEndDate: true})}>
                      <Text>
                        {FuncCommon.ConDate(this.staticParam.EndDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'column', padding: 5}}>
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        {textAlign: 'center'},
                        styles.timeHeader,
                      ]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput, {marginHorizontal: 10}]}
                  onPress={() => this.onActionStatus()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.state.StatusName
                        ? {color: 'black'}
                        : {color: AppColors.gray},
                    ]}>
                    {this.state.StatusName
                      ? this.state.StatusName
                      : 'Chọn tình trạng...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      ncolor={AppColors.gray}
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={text => this.updateSearch(text)}
                  value={this.staticParam.search.value}
                />
              </View>
            ) : null}
          </View>
          <View style={{flex: 1}}>
            {this.state.list && this.state.list.length > 0 && (
              <FlatList
                style={{marginBottom: 10}}
                ref={ref => {
                  this.ListView_Ref = ref;
                }}
                keyExtractor={(item, index) => index.toString()}
                data={this.state.list}
                renderItem={({item, index}) => (
                  <ListItem
                    title={() => (
                      <View
                        style={[
                          AppStyles.ListItemTitle,
                          {
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          },
                        ]}>
                        <Text style={AppStyles.Labeldefault}>
                          {item.QuotationNo}
                        </Text>
                        {item.StatusWF == 'Kết thúc' ? (
                          <Text
                            style={[
                              {
                                justifyContent: 'center',
                                color: AppColors.AcceptColor,
                              },
                              AppStyles.Textdefault,
                            ]}>
                            {item.StatusWF}
                          </Text>
                        ) : (
                          <Text
                            style={[
                              {
                                justifyContent: 'center',
                                color: AppColors.PendingColor,
                              },
                              AppStyles.Textdefault,
                            ]}>
                            {item.StatusWF}
                          </Text>
                        )}
                      </View>
                    )}
                    subtitle={() => {
                      return (
                        <View style={{flexDirection: 'row'}}>
                          <View style={{flex: 5}}>
                            <Text style={[AppStyles.Textdefault]}>
                              Khách hàng: {item.CustomerName}
                            </Text>
                            <Text style={[AppStyles.Textdefault]}>
                              Lần: {item.VersionNo}
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 3,
                              alignItems: 'flex-end',
                              justifyContent: 'flex-start',
                            }}>
                            <Text style={[AppStyles.Textdefault]}>
                              {this.customDate(item.QuotationDate)}
                            </Text>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {
                                  fontWeight: 'bold',
                                  textAlign: 'right',
                                },
                              ]}>
                              {this.addPeriod(item.TotalNoVAT)}
                            </Text>
                          </View>
                        </View>
                      );
                    }}
                    bottomDivider
                    titleStyle={AppStyles.Titledefault}
                    onPress={() => this.openView(item.QuotationGuid)}
                  />
                )}
                onEndReachedThreshold={0.5}
                onMomentumScrollBegin={() => {
                  this.onEndReachedCalledDuringMomentum = false;
                }}
                onEndReached={() => this.loadMoreData()}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['red', 'green', 'blue']}
                  />
                }
              />
            )}
          </View>
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback => this.onClick(callback)}
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          <Combobox
            value={this.staticParam.QuotationStatus}
            TypeSelect={'multiple'}
            callback={this.ChangeStatus}
            data={listCombobox.OrderStatusData}
            nameMenu={'Chọn tình trạng'}
            eOpen={this.openComboboxStatus}
            position={'bottom'}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  _openComboboxStatus() {}
  openComboboxStatus = d => {
    this._openComboboxStatus = d;
  };
  onActionStatus() {
    this._openComboboxStatus();
  }
  ChangeStatus = rs => {
    if (rs.length === 0) {
      this.staticParam.QuotationStatus = [];
      this.setState({StatusName: ''}, () => {
        this.updateSearch('');
      });
      return;
    }
    var value = [];
    var list = [];
    for (let i = 0; i < rs.length; i++) {
      if (rs[i].value !== null) {
        list.push(rs[i]);
        value.push(rs[i].value);
      }
    }
    this.staticParam.QuotationStatus = value;
    var name =
      list.length !== 0 && list.length > 1
        ? list.length + ' lựa chọn'
        : list[0].text;
    this.setState({StatusName: name}, () => {
      this.updateSearch('');
    });
  };

  //Load lại
  onRefresh = () => {
    this.updateSearch('');
  };
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({EvenFromSearch: true});
    } else {
      this.setState({EvenFromSearch: false});
    }
  }
  renderFooter = () => {
    if (!this.state.isFetching) {
      return null;
    }
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState(
        {
          ValueSearchDate: callback.value,
        },
        () => {
          this.updateSearch('');
        },
      );
    }
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  updateSearch = text => {
    this.staticParam.search.value = text;
    this.staticParam.CurrentPage = 1;
    this.setState(
      {
        list: [],
      },
      () => {
        this.GetAll();
      },
    );
  };
  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetAll();
  }
  GetAll = () => {
    this.setState({refreshing: true});
    var obj = {
      ...this.staticParam,
      EndDate: FuncCommon.ConDate(this.staticParam.EndDate, 2),
      StartDate: FuncCommon.ConDate(this.staticParam.StartDate, 2),
    };
    API_Orders.QuotationsAM_ListAll(obj)
      .then(res => {
        var list = [];
        let _data = JSON.parse(res.data.data);
        if (this.staticParam.CurrentPage === 1) {
          list = JSON.parse(res.data.data).data;
        } else {
          list = this.state.list.concat(JSON.parse(res.data.data).data);
        }
        this.Total = _data.recordsTotal;
        if (this.staticParam.Status === 'W') {
          this.listtabbarBotom[0].Badge = _data.recordsTotal;
        }
        this.setState({list, refreshing: false, notInit: true});
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
        this.setState({refreshing: false});
      });
  };
  openView(id) {
    Actions.openQuotations({RecordGuid: id});
    console.log('===================chuyen Id:' + id);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Quotation') {
      this.updateSearch('');
    }
  }
  clickItem(item) {
    console.log('================> ' + JSON.stringify(item));
    switch (item) {
      case 'Back':
        Actions.app();
        break;
      case 'Open':
        Actions.openQuotations();
        break;
      default:
        break;
    }
  }
  onClick(data) {
    if (data == 'checkcircleo') {
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 15;
      this.staticParam.Status = 'Y';
      this.updateSearch('');
    } else {
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 15;
      this.staticParam.Status = 'W';
      this.updateSearch('');
    }
    // else if (data == 'export2') {
    //     this.state.list = [];
    //     this.staticParam.CurrentPage = 1;
    //     this.staticParam.Length = 15;
    //     this.staticParam.Status = "M";
    //     this.updateSearch('');
    // }
  }
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
  timeHeader: {
    marginBottom: 3,
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(indexQuotations);
