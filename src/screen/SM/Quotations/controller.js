import {API_Quotations, API_ApplyOverTimes} from '@network';
import configApp from '../../../configApp';
import Toast from 'react-native-simple-toast';
export default {
  getNumberAuto(callback) {
    let val = {AliasId: 'QTDSCT_BG', Prefixs: null, VoucherType: ''};
    API_ApplyOverTimes.GetNumberAuto(val)
      .then(rs => {
        var _dataSM = JSON.parse(rs.data.data);
        let dataSM = {Value: _dataSM.Value, IsEdits: _dataSM.IsEdit};
        callback(dataSM);
      })
      .catch(error => {
        alert(error);
      });
  },
  getAllObjectKH(callback) {
    API_Quotations.getAllObjectKH()
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        for (let i = 0; i < _data.length; i++) {
          list.push({
            value: _data[i].CustomerId,
            text: '[' + _data[i].CustomerId + ']' + ' ' + _data[i].CustomerName,
          });
        }
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  getKHbyCustomerId(data, callback) {
    var obj = {IdS: [data]};
    API_Quotations.getKHbyCustomerId(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  getPeople_Quotation(callback) {
    API_Quotations.getPeople_Quotation()
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetAllCurrency(callback) {
    API_Quotations.GetAllCurrency()
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        _data.forEach(value => {
          list.push({
            value: value.CurrencyId,
            CurrencyId: value.CurrencyId,
            text: '[' + value.CurrencyId + '] ' + value.Title,
          });
        });
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetRateByCurrencies(data, callback) {
    API_Quotations.GetRateByCurrencies(data)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetItems(data, callback) {
    var obj = {search: data, Type: 'T'};
    API_Quotations.GetItems(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        _data.forEach(val => {
          list.push({
            value: val.id,
            text: val.name,
          });
        });
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetItemsByItemId(data, callback) {
    API_Quotations.GetItemsByItemId(data)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetUnitsAllJExcel(callback) {
    API_Quotations.GetUnitsAllJExcel()
      .then(res => {
        var para = JSON.parse(res.data.data);
        var list = [];
        for (let i = 0; i < para.length; i++) {
          list.push({
            value: para[i].UnitId,
            text: para[i].UnitName,
          });
        }
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetAllTerms(callback) {
    API_Quotations.GetAllTerms()
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        _data.forEach(val => {
          list.push({
            value: val.TermId,
            text: val.TermName,
            TermContent: val.TermContent,
          });
        });
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  renderImage(check) {
    if (check !== '') {
      var _check = check.split('.');
      if (_check.length > 1) {
        return (
          configApp.url_icon_chat +
          configApp.link_type_icon +
          _check[_check.length - 1] +
          '-icon.png'
        );
      } else {
        return (
          configApp.url_icon_chat +
          configApp.link_type_icon +
          'default' +
          '-icon.png'
        );
      }
    }
  },
  removeAttactment(data, list, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },
  GetUnitPriceOfProduct(data, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },

  Insert(state, callback) {
    if (state.TermId === '') {
      Toast.showWithGravity(
        'Yêu cầu chọn điều khoản báo giá',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    var objchung = {
      CustomerId: state.CustomerId,
      CustomerName: state.CustomerName,
      Address: state.Address,
      TaxCode: state.TaxCode,
      Title: state.Title,
      ContactName: state.ContactName,
      Email: null,
      EmployeeId: state.EmployeeId,
      EmployeeName: state.EmployeeName,
      OrderTypeId: null,
      GroupItemId: null,
      WorkPhone: state.WorkPhone,
      Note: null,
      CurrencyId: state.CurrencyId,
      CurrencyRate: state.CurrencyRate,
      Description: null,
      IsVersion: null,
      TermId: state.TermId,
      TermContent: state.TermContent,
      TotalNoVAT: null,
      TotalWithVAT: null,
      QuotationNo: state.QuotationNo,
      QuotationDate: state.QuotationDate,
      ExpiredDate: null,
      Status: 0,
      IsVat: 1,
      ValidDay: null,
      RequireDate: null,
      DebtDay: state.DebtDay,
      PaymentExpiredDate: state.PaymentExpiredDate,
      DeliveryType: state.DeliveryType,
    };
    var lstDetail = state.rows;
    var list = [];
    for (var i = 0; i < lstDetail.length; i++) {
      lstDetail[i].DeliveryDate =
        lstDetail[i].DeliveryDate !== '' ? lstDetail[i].DeliveryDate : null;
      lstDetail[i].ConfirmDate =
        lstDetail[i].ConfirmDate !== '' ? lstDetail[i].ConfirmDate : null;
      var ob = {
        ItemId: lstDetail[i].ProductId ?? null,
        ItemName: lstDetail[i].ProductName ?? null,
        ItemNameVN: lstDetail[i].ItemNameVN ?? null,
        Specification: lstDetail[i].Specification ?? null,
        UnitId: lstDetail[i].UnitId ?? null,
        Quantity: parseFloat(lstDetail[i].Quantity) ?? 0,
        UnitPriceNET: parseFloat(lstDetail[i].UnitPriceNET) ?? 0,
        UnitPrice: parseFloat(lstDetail[i].UnitPrice) ?? 0,
        AmountOc:
          parseFloat(lstDetail[i].Quantity) ??
          0 * parseFloat(lstDetail[i].UnitPrice) ??
          0,
        Amount:
          parseFloat(lstDetail[i].Quantity) ??
          0 * parseFloat(lstDetail[i].UnitPrice) ??
          0 * parseFloat(state.CurrencyRate) ??
          0,
        AmountNoVAT: parseFloat(lstDetail[i].AmountNoVAT) ?? 0,
        AmountVAT: parseFloat(lstDetail[i].AmountVAT) ?? 0,
        VATPercent: parseFloat(lstDetail[i].VATPercent) ?? 0,
        VATAmountOC: parseFloat(lstDetail[i].VATAmountOC) ?? 0,
        DiscountPercent: 0,
        DiscountOc: 0,
        Discount: 0,
        Profit: parseFloat(lstDetail[i].Profit) ?? 0,
        ProfitPercent: parseFloat(lstDetail[i].ProfitPercent) ?? 0,
        ManagementFeePercent:
          parseFloat(lstDetail[i].ManagementFeePercent) ?? 0,
        ManagementFee: parseFloat(lstDetail[i].ManagementFee) ?? 0,
        CSKH: parseFloat(lstDetail[i].CSKH) ?? 0,
        CSKHPercent: parseFloat(lstDetail[i].CSKHPercent) ?? 0,
        ProfitActual: parseFloat(lstDetail[i].ProfitActual) ?? 0,
        ProfitActualPercent: parseFloat(lstDetail[i].ProfitActualPercent) ?? 0,
        IsPrice: lstDetail[i].IsPrice ?? null,
        DeliveryDate: lstDetail[i].DeliveryDate ?? null,
        ConfirmDate: lstDetail[i].ConfirmDate ?? null,
        Note: lstDetail[i].Note ?? null,
        SortOrder: parseFloat(lstDetail[i].SortOrder) ?? 0,
        ItemIDByProvider: lstDetail[i].ProductId ?? null,
      };
      list.push(ob);
    }
    var _lstTitlefile = [];
    for (var i = 0; i < state.Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: state.Attachment[i].FileName,
      });
    }
    var fd = new FormData();
    fd.append('Insert', JSON.stringify(objchung));
    fd.append('Detail', JSON.stringify(list));
    fd.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    fd.append('auto', JSON.stringify(state.NumberAuto));
    for (var i = 0; i < state.Attachment.length; i++) {
      _data.append(state.Attachment[i].FileName, {
        name: state.Attachment[i].FileName,
        size: state.Attachment[i].size,
        type: state.Attachment[i].type,
        uri: state.Attachment[i].uri,
      });
    }
    API_Quotations.Insert(fd)
      .then(rs => {
        Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
        if (rs.data.errorCode === 200) {
          callback(rs.data);
        }
      })
      .catch(error => {
        console.log('Đã xảy ra lỗi khi thêm mới.');
        console.log(error);
      });
  },
};
