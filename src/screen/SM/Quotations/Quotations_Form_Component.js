import React, {Component} from 'react';
import {
  FlatList,
  KeyboardAvoidingView,
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Animated,
  TextInput,
  ScrollView,
  Image,
  Dimensions,
  PermissionsAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Button, Divider, Icon} from 'react-native-elements';
import _ from 'lodash';
import {AppStyles, AppColors} from '@theme';
import {
  LoadingComponent,
  TabBar_Title,
  RequiredText,
  Combobox,
  ComboboxV2,
  OpenPhotoLibrary,
} from '@Component';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '@utils';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import {listCombobox} from './listCombobox';
import {controller} from './controller';
import Toast from 'react-native-simple-toast';
const styles = StyleSheet.create({});
const ListTypeItem = [
  {
    value: 'K',
    text: 'Thường',
  },
  {
    value: 'T',
    text: 'Danh sách TMC.',
  },
  {
    value: 'U',
    text: 'Vỏ tủ',
  },
  {
    value: 'R',
    text: 'Danh sách tủ Rack',
  },
];
class Quotations_Form_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      //hiển thị thông tin chung
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
      //Hiển thị điều khoản báo giá
      ViewBG: false,
      transIcon_ViewViewBG: new Animated.Value(0),
      NumberAuto: null,
      setQuotationDate: false,
      setRequireDate: false,
      ListObjectKH: [],
      ListObjectNV: [],
      ListCurrency: [],
      ListProduct: [],
      ListUnit: [],
      ListTerms: [],
      Attachment: [],
      rows: [{}],
      rowchange: 0,
      // các giá trị model
      QuotationNo: '',
      QuotationDate: new Date(),
      CustomerGuid: '',
      CustomerId: '',
      CustomerName: '',
      Address: '',
      ContactName: '',
      EmployeeId: '',
      EmployeeName: '',
      TypeItem: 'T',
      TypeItemName: '',
      WorkPhone: '',
      CurrencyId: '',
      CurrencyRate: '',
      DebtDay: '',
      RequireDate: null,
      DeliveryType: '',
      Title: '',
      TermId: '',
      TermName: '',
      TermContent: '',
    };
    this.lstdataDetail = [];
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    this.Skill();
    this.setViewOpen('ViewDetail');
    controller.getNumberAuto(rs => {
      this.state.QuotationNo = rs.Value;
      this.setState({NumberAuto: rs, QuotationNo: rs.Value});
      this.getAllObjectKH();
    });
  }
  getAllObjectKH = () => {
    // controller.getAllObjectKH(rs => {
    //   if (rs !== null) {
    //     this.setState({ ListObjectKH: rs });
    //   }
    // });
    this.getPeople_Quotation();
  };
  getPeople_Quotation = () => {
    controller.getPeople_Quotation(rs => {
      if (rs !== null) {
        var obj = rs.find(
          x => x.value === global.__appSIGNALR.SIGNALR_object.USER.EmployeeId,
        );
        if (obj !== undefined) {
          this.settxt('EmployeeId', obj.value);
          this.settxt('EmployeeName', obj.text);
        }
      }
      this.setState({ListObjectNV: rs});
      this.GetAllCurrency();
    });
  };
  GetAllCurrency = () => {
    controller.GetAllCurrency(rs => {
      this.setState({ListCurrency: rs});
      this.GetAllTerms();
    });
  };
  GetAllTerms = () => {
    controller.GetAllTerms(rs => {
      this.setState({ListTerms: rs});
      this.GetItems('');
    });
  };
  GetItems = txt => {
    controller.GetItems(txt, rs => {
      this.setState({ListProduct: rs});
      this.GetUnitsAllJExcel();
    });
  };
  GetUnitsAllJExcel = () => {
    controller.GetUnitsAllJExcel(rs => {
      this.setState({ListUnit: rs, loading: false});
    });
  };

  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewViewBG = this.state.transIcon_ViewViewBG.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const {
      setQuotationDate,
      setRequireDate,
      ListObjectKH,
      ListObjectNV,
      ListCurrency,
      ListProduct,
      ListUnit,
      ListTerms,
      QuotationNo,
      QuotationDate,
      CustomerId,
      CustomerGuid,
      CustomerName,
      ContactName,
      EmployeeId,
      EmployeeName,
      TypeItem,
      TypeItemName,
      WorkPhone,
      CurrencyId,
      CurrencyRate,
      DebtDay,
      RequireDate,
      DeliveryType,
      Title,
      TermId,
      TermName,
      TermContent,
    } = this.state;
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={[AppStyles.container]}>
            <TabBar_Title
              title={'Thêm mới báo giá'}
              callBack={() => this.BackList()}
            />
            <ScrollView>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  backgroundColor: '#bed9f6',
                }}
                onPress={() => this.setViewOpen('ViewAll')}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
                </View>
                <Animated.View
                  style={{
                    transform: [
                      {rotate: rotateStart_ViewAll},
                      {perspective: 4000},
                    ],
                    paddingRight: 10,
                    paddingLeft: 10,
                  }}>
                  <Icon type={'feather'} name={'chevron-up'} />
                </Animated.View>
              </TouchableOpacity>
              <Divider />
              {this.state.ViewAll !== true ? null : (
                <View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>
                        Số báo giá <RequiredText />
                      </Text>
                    </View>
                    <TextInput
                      style={[AppStyles.FormInput, {color: 'black'}]}
                      underlineColorAndroid="transparent"
                      autoCapitalize="none"
                      value={QuotationNo}
                      onChangeText={txt => this.settxt('QuotationNo', txt)}
                    />
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Mã khách hàng</Text>
                    </View>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionCustomer()}>
                      <Text
                        style={[
                          AppStyles.TextInput,
                          {
                            color:
                              CustomerId !== ''
                                ? AppColors.black
                                : AppColors.gray,
                          },
                        ]}>
                        {CustomerId === '' ? 'Chọn mã khách hàng' : CustomerId}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Khách hàng</Text>
                    </View>
                    <TextInput
                      style={[AppStyles.FormInput, {color: 'black'}]}
                      underlineColorAndroid="transparent"
                      placeholder="Nhập tên khách hàng"
                      autoCapitalize="none"
                      value={CustomerName}
                      onChangeText={txt => this.settxt('CustomerName', txt)}
                    />
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Người báo giá</Text>
                    </View>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionEmployee()}>
                      <Text
                        style={[
                          AppStyles.TextInput,
                          {
                            color:
                              EmployeeId !== ''
                                ? AppColors.black
                                : AppColors.gray,
                          },
                        ]}>
                        {EmployeeName === ''
                          ? 'Chọn người báo giá'
                          : EmployeeName}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  {/* ngày báo giá - hạn báo giá  */}
                  <View
                    style={{
                      padding: 10,
                      paddingBottom: 0,
                      flexDirection: 'row',
                    }}>
                    <View style={{flex: 1, marginRight: 5}}>
                      <View>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày báo giá <RequiredText />
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.setState({setQuotationDate: true})}>
                        <Text
                          style={[
                            AppStyles.TextInput,
                            {color: AppColors.black},
                          ]}>
                          {FuncCommon.ConDate(QuotationDate, 0)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, marginLeft: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>
                          Hạn báo giá <RequiredText />
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.setState({setRequireDate: true})}>
                        <Text
                          style={[
                            AppStyles.TextInput,
                            {
                              color:
                                RequireDate !== null
                                  ? AppColors.black
                                  : AppColors.gray,
                            },
                          ]}>
                          {RequireDate !== null
                            ? FuncCommon.ConDate(RequireDate, 0)
                            : 'dd/MM/yyyy'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={{
                      padding: 10,
                      paddingBottom: 0,
                      flexDirection: 'row',
                    }}>
                    <View style={{flex: 1, marginRight: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>Loại TMC</Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionTypeItem()}>
                        <Text style={[AppStyles.TextInput]}>
                          {TypeItemName}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, marginLeft: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>Loại tiền</Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionCurrency()}>
                        <Text
                          style={[
                            AppStyles.TextInput,
                            {
                              color:
                                CurrencyId !== ''
                                  ? AppColors.black
                                  : AppColors.gray,
                            },
                          ]}>
                          {CurrencyId === '' ? 'Chọn loại tiền' : CurrencyId}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={{
                      padding: 10,
                      paddingBottom: 0,
                    }}>
                    <View>
                      <Text style={AppStyles.Labeldefault}>Tỷ giá</Text>
                    </View>
                    <TextInput
                      style={[
                        AppStyles.FormInput,
                        {width: '100%', textAlign: 'right'},
                      ]}
                      underlineColorAndroid="transparent"
                      autoCapitalize="none"
                      keyboardType={'numeric'}
                      value={CurrencyRate}
                      onChangeText={txt => this.settxt('CurrencyRate', txt)}
                    />
                  </View>

                  <View style={{padding: 10, paddingBottom: 0}} />
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Nội dung</Text>
                    </View>
                    <TextInput
                      style={[AppStyles.FormInput, {color: 'black'}]}
                      underlineColorAndroid="transparent"
                      placeholder="Nhập nội dung"
                      autoCapitalize="none"
                      value={Title}
                      onChangeText={txt => this.settxt('Title', txt)}
                    />
                  </View>
                  <View style={{flexDirection: 'column', padding: 10}}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          flexDirection: 'row',
                        }}
                        onPress={() => this.openAttachment()}>
                        <Icon
                          name="attachment"
                          type="entypo"
                          size={15}
                          color={AppColors.ColorAdd}
                        />
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {color: AppColors.ColorAdd},
                          ]}>
                          {' '}
                          Chọn file
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <Divider />
                    {this.state.Attachment.length > 0 ? (
                      this.state.Attachment.map((para, i) => (
                        <View
                          key={i}
                          style={[
                            styles.StyleViewInput,
                            {flexDirection: 'row', marginBottom: 3},
                          ]}>
                          <View
                            style={[
                              AppStyles.containerCentered,
                              {padding: 10},
                            ]}>
                            <Image
                              style={{
                                width: 30,
                                height: 30,
                                borderRadius: 10,
                              }}
                              source={{
                                uri: controller.renderImage(para.FileName),
                              }}
                            />
                          </View>
                          <TouchableOpacity
                            key={i}
                            style={{flex: 1, justifyContent: 'center'}}>
                            <Text style={AppStyles.Textdefault}>
                              {para.FileName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.containerCentered, {padding: 10}]}
                            onPress={() => this.removeAttactment(para)}>
                            <Icon
                              name="close"
                              type="antdesign"
                              size={20}
                              color={AppColors.ColorDelete}
                            />
                          </TouchableOpacity>
                        </View>
                      ))
                    ) : (
                      <View style={[{marginBottom: 50}]} />
                    )}
                  </View>
                </View>
              )}
              <Divider />
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  backgroundColor: '#bed9f6',
                }}
                onPress={() => this.setViewOpen('ViewDetail')}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
                </View>
                <Animated.View
                  style={{
                    transform: [
                      {rotate: rotateStart_ViewDetail},
                      {perspective: 4000},
                    ],
                    paddingRight: 10,
                    paddingLeft: 10,
                  }}>
                  <Icon type={'feather'} name={'chevron-up'} />
                </Animated.View>
              </TouchableOpacity>
              <Divider />
              {this.state.ViewDetail !== true ? (
                <View style={{flex: 1}} />
              ) : (
                <View style={{flex: 1}}>{this.customViewDetail()}</View>
              )}
              <Divider />
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  backgroundColor: '#bed9f6',
                }}
                onPress={() => this.setViewOpen('ViewBG')}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={AppStyles.Labeldefault}>ĐIỀU KHOẢN BÁO GIÁ</Text>
                </View>
                <Animated.View
                  style={{
                    transform: [
                      {rotate: rotateStart_ViewViewBG},
                      {perspective: 4000},
                    ],
                    paddingRight: 10,
                    paddingLeft: 10,
                  }}>
                  <Icon type={'feather'} name={'chevron-up'} />
                </Animated.View>
              </TouchableOpacity>
              <Divider />
              {this.state.ViewBG !== true ? (
                <View style={{flex: 1}} />
              ) : (
                <View style={{flex: 1}}>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>
                        Điều khoản báo giá
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionTerms()}>
                      <Text
                        style={[
                          AppStyles.TextInput,
                          {
                            color:
                              TermId !== '' ? AppColors.black : AppColors.gray,
                          },
                        ]}>
                        {TermId === '' ? 'Chọn diều khoản' : TermName}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  marginBottom: 20,
                }}>
                <TouchableOpacity
                  style={[
                    AppStyles.containerCentered,
                    {
                      flex: 1,
                      padding: 10,
                      borderRadius: 10,
                      marginLeft: 5,
                      backgroundColor: AppColors.ColorButtonSubmit,
                    },
                  ]}
                  onPress={() => this.Insert()}>
                  <Text style={[AppStyles.Titledefault, {color: '#fff'}]}>
                    Lưu
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>

            {ListObjectKH && (
              <Combobox
                value={CustomerGuid}
                TypeSelect={'single'}
                callback={this.ChangeCustomer}
                data={ListObjectKH}
                nameMenu={'Chọn khách hàng'}
                eOpen={this.openComboboxCustomer}
                position={'bottom'}
              />
            )}
            {ListObjectNV && (
              <Combobox
                value={EmployeeId}
                TypeSelect={'single'}
                callback={this.ChangeEmployee}
                data={ListObjectNV}
                nameMenu={'Chọn nhân viên báo giá'}
                eOpen={this.openComboboxEmployee}
                position={'bottom'}
              />
            )}
            {ListCurrency && (
              <Combobox
                value={CurrencyId}
                TypeSelect={'single'}
                callback={this.ChangeCurrency}
                data={ListCurrency}
                nameMenu={'Chọn loại tiền'}
                eOpen={this.openComboboxCurrency}
                position={'bottom'}
              />
            )}
            <Combobox
              value={TypeItem}
              TypeSelect={'single'}
              callback={this.ChangeTypeItem}
              data={ListTypeItem}
              nameMenu={'Chọn loại TMC'}
              eOpen={this.openComboboxTypeItem}
              position={'bottom'}
            />
            <ComboboxV2
              callback={this.ChoiceAtt}
              data={listCombobox.ListComboboxAtt}
              eOpen={this.openCombobox_Att}
            />
            <OpenPhotoLibrary
              callback={this.callbackLibarary}
              openLibrary={this.openLibrary}
            />
            {setQuotationDate === true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 100,
                  width: DRIVER.width,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({setQuotationDate: false});
                  }}>
                  <Text style={{textAlign: 'right'}}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  locale="vie"
                  date={QuotationDate}
                  mode="date"
                  style={{width: DRIVER.width}}
                  onDateChange={setDate =>
                    this.settxt('QuotationDate', setDate)
                  }
                />
              </View>
            ) : null}
            {setRequireDate === true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 100,
                  width: DRIVER.width,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({setRequireDate: false});
                  }}>
                  <Text style={{textAlign: 'right'}}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  locale="vie"
                  date={RequireDate !== null ? RequireDate : new Date()}
                  mode="date"
                  style={{width: DRIVER.width}}
                  onDateChange={setDate => this.settxt('RequireDate', setDate)}
                />
              </View>
            ) : null}
            {ListProduct && (
              <Combobox
                value={''}
                TypeSelect={'single'}
                callback={this.ChangeProduct}
                data={ListProduct}
                nameMenu={'Chọn mã sản phẩm'}
                eOpen={this.openComboboxProduct}
                position={'bottom'}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.searchItems(callback, textsearch)
                }
              />
            )}
            {ListUnit && (
              <Combobox
                value={''}
                TypeSelect={'single'}
                callback={this.ChangeUnit}
                data={ListUnit}
                nameMenu={'Chọn đơn vị tính'}
                eOpen={this.openComboboxUnit}
                position={'bottom'}
              />
            )}
            {ListTerms && (
              <Combobox
                value={TermId}
                TypeSelect={'single'}
                callback={this.ChangeTerms}
                data={ListTerms}
                nameMenu={'Chọn điều khoản'}
                eOpen={this.openComboboxTerms}
                position={'bottom'}
              />
            )}
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  customViewDetail = () => {
    return (
      <View style={{marginTop: 5}}>
        <View style={{width: 150, marginLeft: 10}}>
          <Button
            title="Thêm dòng"
            type="outline"
            size={15}
            onPress={() => this.addRows()}
            buttonStyle={{padding: 5, marginBottom: 5}}
            titleStyle={{marginLeft: 5}}
            icon={
              <Icon
                name="pluscircleo"
                type="antdesign"
                color={AppColors.blue}
                size={14}
              />
            }
          />
        </View>
        <ScrollView horizontal={true}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              {listCombobox.headerTable.map((item, i) => (
                <TouchableOpacity
                  key={i}
                  style={[AppStyles.table_th, {width: item.width}]}>
                  <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                </TouchableOpacity>
              ))}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <FlatList
                  data={this.state.rows}
                  renderItem={({item, index}) => {
                    return this.itemFlatList(item, index);
                  }}
                  keyExtractor={(rs, index) => index.toString()}
                />
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        {/* STT  */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(0)}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        {/* Mã sản phẩm */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(1)}]}>
          <TouchableOpacity onPress={() => this.onActionProduct(index)}>
            <Text
              style={[
                AppStyles.TextInput,
                {
                  color:
                    row.ProductId !== undefined
                      ? AppColors.black
                      : AppColors.gray,
                },
              ]}>
              {row.ProductId === '' || row.ProductId === undefined
                ? 'Chọn mã sản phẩm'
                : row.ProductId}
            </Text>
          </TouchableOpacity>
        </TouchableOpacity>
        {/* Tên sản phẩm */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(2)}]}>
          <TextInput
            underlineColorAndroid="transparent"
            value={row.ProductName}
            autoCapitalize="none"
            onChangeText={txt =>
              this.handleChangeRows(txt + '', index, 'ProductName')
            }
          />
        </TouchableOpacity>
        {/* Xuât xứ */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(3)}]}>
          <TextInput
            underlineColorAndroid="transparent"
            value={row.Origin}
            autoCapitalize="none"
            onChangeText={txt =>
              this.handleChangeRows(txt + '', index, 'Origin')
            }
          />
        </TouchableOpacity>
        {/* Đơn vị tính */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(4)}]}>
          <TouchableOpacity onPress={() => this.onActionUnit(index)}>
            <Text
              style={[
                AppStyles.TextInput,
                {
                  color:
                    row.UnitId !== undefined ? AppColors.black : AppColors.gray,
                },
              ]}>
              {row.UnitId === '' || row.UnitId === undefined
                ? 'Chọn ĐVT'
                : row.UnitName}
            </Text>
          </TouchableOpacity>
        </TouchableOpacity>
        {/* Số lương SO */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(5)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.Quantity ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'Quantity');
            }}
          />
        </TouchableOpacity>
        {/* Đơn giá COST */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(6)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.UnitPriceNET ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'UnitPriceNET');
            }}
          />
        </TouchableOpacity>
        {/* % LN mong muốn */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(7)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.ProfitPercent ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'ProfitPercent');
            }}
          />
        </TouchableOpacity>
        {/* % CP bán hàng */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(8)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.SellExpensesPercent ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'SellExpensesPercent');
            }}
          />
        </TouchableOpacity>
        {/* % CP tài chính */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(9)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.FinancialExpensesPercent ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(
                txt + '',
                index,
                'FinancialExpensesPercent',
              );
            }}
          />
        </TouchableOpacity>
        {/* % QLP */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(10)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.ManagementFeePercent ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'ManagementFeePercent');
            }}
          />
        </TouchableOpacity>
        {/* % CSKH */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(11)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.CSKHPercent ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'CSKHPercent');
            }}
          />
        </TouchableOpacity>
        {/* Chi phí khác */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(12)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.OtherCosts ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'OtherCosts');
            }}
          />
        </TouchableOpacity>
        {/* Đơn giá bán */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(13)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.UnitPrice ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'UnitPrice');
            }}
          />
        </TouchableOpacity>
        {/*Thành tiền trước thuế */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(14)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.AmountNoVAT ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'AmountNoVAT');
            }}
          />
        </TouchableOpacity>
        {/*% VAT */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(15)}]}
          onPress={() => this.onActionProduct(index)}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                color:
                  row.ProductId !== undefined
                    ? AppColors.black
                    : AppColors.gray,
              },
            ]}>
            {row.VATPercent === '' || row.VATPercent === undefined
              ? 'Chọn % thuế GTGT'
              : row.VATPercent}
          </Text>
        </TouchableOpacity>
        {/* VAT */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(16)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.VATAmountOC ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'VATAmountOC');
            }}
          />
        </TouchableOpacity>
        {/* Thành tiền sau VAT */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(17)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={row.AmountVAT ?? '0'}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(txt + '', index, 'AmountVAT');
            }}
          />
        </TouchableOpacity>
        {/*% ngày cần hàng */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(18)}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                color:
                  row.ProductId !== undefined
                    ? AppColors.black
                    : AppColors.gray,
              },
            ]}>
            {row.DeliveryDate === '' || row.DeliveryDate === undefined
              ? 'Chọn % thuế GTGT'
              : row.DeliveryDate}
          </Text>
        </TouchableOpacity>
        {/* Ghi chú */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(19)}]}>
          <TextInput
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={txt => this.handleChangeRows(txt + '', index, 'Note')}
          />
        </TouchableOpacity>
        {/* Hành động */}
        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td, {width: this.widthTable(20)}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  widthTable = i => {
    return listCombobox.headerTable[i].width;
  };
  //#region thông tin chung
  //
  settxt = (key, txt) => {
    this.setState({[key]: txt});
  };
  //
  _openComboboxCustomer() {}
  openComboboxCustomer = d => {
    this._openComboboxCustomer = d;
  };
  onActionCustomer() {
    this._openComboboxCustomer();
  }
  ChangeCustomer = rs => {
    if (rs !== null) {
      this.setState({loading: true});
      controller.getKHbyCustomerId(rs.value, res => {
        this.setState({
          CustomerGuid: res.CustomerGuid,
          CustomerId: rs.value,
          Address: res.Address,
          TaxCode: res.TaxCode,
          DebtDay: res.DebtDay,
          Email: res.Email,
          CustomerName: res.CustomerName,
          loading: false,
        });
      });
    }
  };
  //
  _openComboboxEmployee() {}
  openComboboxEmployee = d => {
    this._openComboboxEmployee = d;
  };
  onActionEmployee() {
    this._openComboboxEmployee();
  }
  ChangeEmployee = rs => {
    if (rs !== null) {
      this.settxt('EmployeeId', rs.value);
      this.settxt('EmployeeName', rs.text);
    }
  };
  //
  _openComboboxTypeItem() {}
  openComboboxTypeItem = d => {
    this._openComboboxTypeItem = d;
  };
  onActionTypeItem() {
    this._openComboboxTypeItem();
  }
  ChangeTypeItem = rs => {
    if (rs !== null) {
      this.settxt('TypeItem', rs.value);
      this.settxt('TypeItemName', rs.text);
    }
  };
  //
  _openComboboxCurrency() {}
  openComboboxCurrency = d => {
    this._openComboboxCurrency = d;
  };
  onActionCurrency() {
    this._openComboboxCurrency();
  }
  ChangeCurrency = rs => {
    console.log(rs);
    if (rs !== null) {
      this.settxt('CurrencyId', rs.value);
      controller.GetRateByCurrencies(rs.value, para => {
        if (!para) {
          return;
        }
        this.setState({
          CurrencyRate: para.Rate.toString(),
        });
      });
    }
  };
  //#endregion

  //#region thông tin chi tiết
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);
    this.setState({rows: res});
  };
  roundNumber = number => {
    if (number > 0) {
      return number;
    } else return 0;
  };
  handleChangeRows = async (val, row, columnName) => {
    var rowindex = parseInt(row); // dòng muốn thay đổi
    let ListData = _.cloneDeep(this.state.rows);
    this.state.CurrencyRate =
      parseInt(this.state.CurrencyRate) > 0 ? this.state.CurrencyRate : 1;
    await this.settxt('CurrencyRate', this.state.CurrencyRate);
    if (columnName === 'ProductId') {
      //nếu bằng cell hiện tại bằng cell muốn thay đổi thì đổi giá trị
      await this.ChangeRows(val, rowindex, 'ProductId');
      var UnitId = '';
      var ItemName = '';
      var ProductId = '';
      var Quantity = '';
      var ItemNameVN = '';
      var Specification = '';
      var obj = {
        Searchs: val,
        Type: this.state.TypeItem,
      };
      await controller.GetItemsByItemId(obj, rs => {
        if (rs != null) {
          ItemName = rs.VenderName !== null ? rs.VenderName : '';
          Quantity = rs.Quantity !== null ? rs.Quantity : 0;
          UnitId = rs.UnitId !== null ? rs.UnitId : 0;
          var UnitPrice = '';
          var Origin = '';
          UnitPrice = rs.UnitPrice !== null ? rs.UnitPrice : 0;
          Origin = rs.Origin !== null ? rs.Origin : '';
          this.ChangeRows(UnitPrice, rowindex, 'UnitPriceNET');
          this.ChangeRows(Origin, rowindex, 'Origin');
          this.ChangeRows(UnitId, rowindex, 'UnitId');
          this.ChangeRows(ItemName, rowindex, 'ProductName');
          this.ChangeRows(Quantity, rowindex, 'Quantity');
          var stemp = false;
          for (let i = 0; i < ListData.length; i++) {
            if (
              ListData[i].ProductId == ListData[rowindex].ProductId &&
              rowindex != i
            ) {
              stemp = true;
            }
          }
          if (stemp == true) {
            this.ChangeRows('', rowindex, 'ProductId');
            this.ChangeRows('', rowindex, 'ProductName');
            this.ChangeRows('', rowindex, 'Origin');
            this.ChangeRows('0', rowindex, 'Quantity');
            Toast.showWithGravity(
              'Mã sản phẩm đã tồn tại, xin kiểm tra lại',
              Toast.SHORT,
              Toast.CENTER,
            );
            return;
          }
          if (this.state.TypeItem == 'K') {
            controller.GetUnitPriceOfProduct(
              {IdS: [ListData[rowindex].ProductId]},
              rs => {
                if (rs && rs.length > 0) {
                  for (let i = 0; i < rs.length; i++) {
                    var stam = false;
                    if (rs[i].ParentID == null || rs.length == 1) {
                      var number = parseFloat(rs[i].UnitPrice);
                      this.ChangeRows(number, rowindex, 'UnitPriceNET');
                      this.ChangeRows(number, rowindex, 'UnitPrice');
                      this.ChangeRows(rs[i].UnitId, rowindex, 'UnitId');
                    }
                    if (rs[i].ParentID != null) {
                      rs[i].Vatpercent =
                        ListData[rowindex].VATPercent != null
                          ? FuncCommon.roundNumber(
                              parseFloat(ListData[rowindex].VATPercent),
                            )
                          : 0;
                      rs[i].Quantity =
                        rs[i].Quantity != null && rs[i].Quantity != undefined
                          ? rs[i].Quantity
                          : 0;
                      var obj = [
                        null,
                        rs[i].ProductId,
                        rs[i].ProductName,
                        rs[i].ItemNameVN,
                        rs[i].Specification,
                        rs[i].Origin,
                        rs[i].UnitId,
                        rs[i].Quantity,
                        FuncCommon.roundNumber(rs[i].UnitPrice),
                        rs[i].ProfitPercent,
                        rs[i].Profit,
                        rs[i].SellExpensesPercent,
                        rs[i].SellExpenses,
                        rs[i].FinancialExpensesPercent,
                        rs[i].FinancialExpenses,
                        rs[i].ManagementFeePercent,
                        rs[i].ManagementFee,
                        rs[i].CSKHPercent,
                        rs[i].CSKH,
                        rs[i].OtherCosts,
                        FuncCommon.roundNumber(rs[i].UnitPrice),
                        0,
                        rs[i].ProfitActual,
                        rs[i].ProfitActualPercent != null
                          ? rs[i].ProfitActualPercent
                          : 0,
                        rs[i].AmountNoVat,
                        rs[i].Vatpercent,
                        rs[i].VatamountOc,
                        rs[i].AmountVAT,
                        null,
                        rs[i].Note,
                        key,
                        rs[i].ItemId,
                        rs[i].ParentID,
                      ];
                      if (this.lstdataDetail && this.lstdataDetail.length > 0) {
                        for (let j = 0; j < this.lstdataDetail.length; j++) {
                          if (this.lstdataDetail[j].ProductId == value1[1]) {
                            stam = true;
                          }
                        }
                      }

                      if (stam == false) {
                        this.lstdataDetail.push(obj);
                      }
                    }
                  }
                  var lstProductId = [];
                  for (let i = 0; i < ListData.length; i++) {
                    lstProductId.push(ListData[i].ProductId.toUpperCase());
                  }
                  for (var i = 0; i < this.lstdataDetail.length; i++) {
                    if (
                      lstProductId.includes(
                        this.lstdataDetail[i][32].toUpperCase(),
                      ) === false
                    ) {
                      this.lstdataDetail.splice(i, 1);
                      i--;
                    }
                  }
                  if (this.lstdataDetail.length == 0) {
                    var obj = [
                      '',
                      '',
                      '',
                      '',
                      '',
                      'VIET NAM',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '10',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                      '',
                    ];
                    this.lstdataDetail.push(obj);
                  }
                  var ListData2 = this.lstdataDetail;
                  this.ListData.forEach((value, key) => {
                    var rowindex = key;
                    var row = key;
                    var Quantity = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].Quantity),
                    );
                    var UnitPriceNET = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].UnitPriceNET),
                    );
                    var ManagementFeePercent = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].ManagementFeePercent),
                    );
                    var ProfitPercent = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].ProfitPercent),
                    );
                    var OtherCosts = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].OtherCosts),
                    );
                    var CSKHPercent = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].CSKHPercent),
                    );
                    var VATPercent = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].VATPercent),
                    );
                    var SellExpensesPercent = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].SellExpensesPercent),
                    );
                    var FinancialExpensesPercent = FuncCommon.roundNumber(
                      parseFloat(ListData2[rowindex].FinancialExpensesPercent),
                    );
                    //  Chi phí bán hàng
                    var ValuaSellExpensesPercent =
                      (SellExpensesPercent * UnitPriceNET) / 100;
                    ValuaSellExpensesPercent =
                      ValuaSellExpensesPercent > 0
                        ? FuncCommon.roundNumber(ValuaSellExpensesPercent)
                        : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaSellExpensesPercent),
                      row,
                      'SellExpenses',
                    );
                    //  Chi phí tài chính
                    var ValuaFinancialExpensesPercent =
                      (FinancialExpensesPercent * UnitPriceNET) / 100;
                    ValuaFinancialExpensesPercent =
                      ValuaFinancialExpensesPercent > 0
                        ? FuncCommon.roundNumber(ValuaFinancialExpensesPercent)
                        : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaFinancialExpensesPercent),
                      row,
                      'FinancialExpenses',
                    );
                    // tính tổng quản lý phí
                    var ValuaQLP = (ManagementFeePercent * UnitPriceNET) / 100;
                    ValuaQLP =
                      ValuaQLP > 0 ? FuncCommon.roundNumber(ValuaQLP) : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaQLP),
                      row,
                      'ManagementFee',
                    );
                    // tính LN mong muốn
                    var ValuaProfit = (ProfitPercent * UnitPriceNET) / 100;
                    ValuaProfit =
                      ValuaProfit > 0 ? FuncCommon.roundNumber(ValuaProfit) : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaProfit),
                      row,
                      'Profit',
                    );
                    // tính CSKH
                    var ValuaCSKH = (CSKHPercent * UnitPriceNET) / 100;
                    ValuaCSKH = FuncCommon.roundNumber(ValuaCSKH);
                    this.ChangeRows(
                      FuncCommon.roundNumber(ValuaCSKH),
                      row,
                      'CSKH',
                    );
                    // tính đơn giá bán
                    var ValuaUnitPrice =
                      UnitPriceNET +
                      ValuaSellExpensesPercent +
                      ValuaFinancialExpensesPercent +
                      ValuaQLP +
                      ValuaCSKH +
                      OtherCosts;
                    ValuaUnitPrice =
                      ValuaUnitPrice > 0
                        ? FuncCommon.roundNumber(ValuaUnitPrice)
                        : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaUnitPrice),
                      row,
                      'UnitPrice',
                    );
                    // tính đơn giá bán ngoại tệ
                    var UnitPriceOc = ValuaUnitPrice / this.state.CurrencyRate;
                    UnitPriceOc =
                      UnitPriceOc > 0
                        ? parseFloat(UnitPriceOc.toString()).toFixed(2)
                        : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(UnitPriceOc),
                      row,
                      'UnitPriceOc',
                    );
                    // tính LN thực tế
                    var ValuaProfitActual = ValuaProfit;
                    ValuaProfitActual =
                      ValuaProfitActual > 0
                        ? FuncCommon.roundNumber(ValuaProfitActual)
                        : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaProfitActual),
                      row,
                      'ProfitActual',
                    );
                    // tính tiền NET
                    var ValuaProfitActualPercent =
                      (ValuaProfitActual * 100) / ValuaUnitPrice;
                    ValuaProfitActualPercent =
                      ValuaProfitActualPercent > 0
                        ? ValuaProfitActualPercent
                        : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaProfitActualPercent),
                      row,
                      'ProfitActualPercent',
                    );
                    // tính thành tiền bán trước thuế
                    var ValuaAmountNoVAT = Quantity * ValuaUnitPrice;
                    ValuaAmountNoVAT =
                      ValuaAmountNoVAT > 0
                        ? FuncCommon.roundNumber(ValuaAmountNoVAT)
                        : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaAmountNoVAT),
                      row,
                      'AmountNoVAT',
                    );
                    // tính VAT
                    var ValuaVAT = (VATPercent * ValuaAmountNoVAT) / 100;
                    ValuaVAT =
                      ValuaVAT > 0 ? FuncCommon.roundNumber(ValuaVAT) : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaVAT),
                      row,
                      'VATAmountOC',
                    );
                    // tính thành tiền bán sau VAT
                    var ValuaAmountVAT = ValuaVAT + ValuaAmountNoVAT;
                    ValuaAmountVAT =
                      ValuaAmountVAT > 0
                        ? FuncCommon.roundNumber(ValuaAmountVAT)
                        : 0;
                    this.ChangeRowsV2(
                      FuncCommon.roundNumber(ValuaAmountVAT),
                      row,
                      'AmountVAT',
                    );
                  });
                }
              },
            );
          } else {
            var number = Math.round(parseFloat(UnitPrice));
            this.ChangeRows(number, rowindex, 'UnitPriceNET');
            this.ChangeRows(number, rowindex, 'UnitPrice');
            this.ChangeRows(rs.UnitId, rowindex, 'UnitId');
            this.ChangeRows(rs.Origin, rowindex, 'Origin');
          }
          var Quantity = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].Quantity),
          );
          var UnitPriceNET = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].UnitPriceNET),
          );
          var ManagementFeePercent = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].ManagementFeePercent),
          );
          var ProfitPercent = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].ProfitPercent),
          );
          var OtherCosts = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].OtherCosts),
          );
          var CSKHPercent = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].CSKHPercent),
          );
          var VATPercent = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].VATPercent),
          );
          var SellExpensesPercent = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].SellExpensesPercent),
          );
          var FinancialExpensesPercent = FuncCommon.roundNumber(
            parseFloat(ListData[rowindex].FinancialExpensesPercent),
          );
          //  Chi phí bán hàng
          var ValuaSellExpensesPercent =
            (SellExpensesPercent * UnitPriceNET) / 100;
          ValuaSellExpensesPercent =
            ValuaSellExpensesPercent > 0
              ? FuncCommon.roundNumber(ValuaSellExpensesPercent)
              : 0;
          this.ChangeRows(
            FuncCommon.roundNumber(ValuaSellExpensesPercent),
            row,
            'SellExpenses',
          );
          //  Chi phí tài chính
          var ValuaFinancialExpensesPercent =
            (FinancialExpensesPercent * UnitPriceNET) / 100;
          ValuaFinancialExpensesPercent =
            ValuaFinancialExpensesPercent > 0
              ? FuncCommon.roundNumber(ValuaFinancialExpensesPercent)
              : 0;
          this.ChangeRows(
            FuncCommon.roundNumber(ValuaFinancialExpensesPercent),
            row,
            'FinancialExpenses',
          );
          // tính tổng quản lý phí
          var ValuaQLP = (ManagementFeePercent * UnitPriceNET) / 100;
          ValuaQLP = ValuaQLP > 0 ? FuncCommon.roundNumber(ValuaQLP) : 0;
          this.ChangeRows(
            FuncCommon.roundNumber(ValuaQLP),
            row,
            'ManagementFee',
          );
          // tính LN mong muốn
          var ValuaProfit = (ProfitPercent * UnitPriceNET) / 100;
          ValuaProfit =
            ValuaProfit > 0 ? FuncCommon.roundNumber(ValuaProfit) : 0;
          this.ChangeRows(FuncCommon.roundNumber(ValuaProfit), row, 'Profit');
          // tính CSKH
          var ValuaCSKH = (CSKHPercent * UnitPriceNET) / 100;
          ValuaCSKH = FuncCommon.roundNumber(ValuaCSKH);
          this.ChangeRows(FuncCommon.roundNumber(ValuaCSKH), row, 'CSKH');
          // tính đơn giá bán
          var ValuaUnitPrice =
            UnitPriceNET +
            ValuaSellExpensesPercent +
            ValuaFinancialExpensesPercent +
            ValuaQLP +
            ValuaCSKH +
            OtherCosts;
          ValuaUnitPrice =
            ValuaUnitPrice > 0 ? FuncCommon.roundNumber(ValuaUnitPrice) : 0;
          this.ChangeRows(
            FuncCommon.roundNumber(ValuaUnitPrice),
            row,
            'UnitPrice',
          );
          // tính đơn giá bán ngoại tệ
          var UnitPriceOc = ValuaUnitPrice / this.state.CurrencyRate;
          UnitPriceOc =
            UnitPriceOc > 0 ? parseFloat(UnitPriceOc.toString()).toFixed(2) : 0;
          this.ChangeRows(UnitPriceOc, row, 'UnitPriceOc');
          // tính LN thực tế
          var ValuaProfitActual = ValuaProfit;
          ValuaProfitActual =
            ValuaProfitActual > 0
              ? FuncCommon.roundNumber(ValuaProfitActual)
              : 0;
          this.ChangeRows(
            FuncCommon.roundNumber(ValuaProfitActual),
            row,
            'ProfitActual',
          );
          // tính tiền NET
          var ValuaProfitActualPercent =
            (ValuaProfitActual * 100) / ValuaUnitPrice;
          ValuaProfitActualPercent =
            ValuaProfitActualPercent > 0 ? ValuaProfitActualPercent : 0;
          this.ChangeRows(
            FuncCommon.roundNumber(ValuaProfitActualPercent),
            row,
            'ProfitActualPercent',
          );
          // tính thành tiền bán trước thuế
          var ValuaAmountNoVAT = Quantity * ValuaUnitPrice;
          ValuaAmountNoVAT =
            ValuaAmountNoVAT > 0 ? FuncCommon.roundNumber(ValuaAmountNoVAT) : 0;
          this.ChangeRows(
            FuncCommon.roundNumber(ValuaAmountNoVAT),
            row,
            'AmountNoVAT',
          );

          // tính VAT
          var ValuaVAT = (VATPercent * ValuaAmountNoVAT) / 100;
          ValuaVAT = ValuaVAT > 0 ? FuncCommon.roundNumber(ValuaVAT) : 0;
          this.ChangeRows(FuncCommon.roundNumber(ValuaVAT), row, 'VATAmountOC');
          // tính thành tiền bán sau VAT
          var ValuaAmountVAT = ValuaVAT + ValuaAmountNoVAT;
          ValuaAmountVAT =
            ValuaAmountVAT > 0 ? FuncCommon.roundNumber(ValuaAmountVAT) : 0;
          this.ChangeRows(
            FuncCommon.roundNumber(ValuaAmountVAT),
            row,
            'AmountVAT',
          );
        }
      });
    }
    // Thay đổi cột mã hàng
    if (
      columnName === 'Quantity' ||
      columnName === 'UnitPriceNET' ||
      columnName === 'Profit' ||
      columnName === 'SellExpensesPercent' ||
      columnName === 'FinancialExpensesPercent' ||
      columnName === 'ManagementFeePercent' ||
      columnName === 'ProfitPercent' ||
      columnName === 'CSKHPercent' ||
      columnName === 'VATPercent' ||
      columnName === 'OtherCosts'
    ) {
      var Quantity = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].Quantity),
      );
      var UnitPriceNET = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].UnitPriceNET),
      );
      var ManagementFeePercent = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].ManagementFeePercent),
      );
      var ProfitPercent = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].ProfitPercent),
      );
      var OtherCosts = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].OtherCosts),
      );
      var CSKHPercent = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].CSKHPercent),
      );
      var VATPercent = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].VATPercent),
      );
      var SellExpensesPercent = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].SellExpensesPercent),
      );
      var FinancialExpensesPercent = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].FinancialExpensesPercent),
      );
      //  Chi phí bán hàng
      var ValuaSellExpensesPercent = (SellExpensesPercent * UnitPriceNET) / 100;
      ValuaSellExpensesPercent =
        ValuaSellExpensesPercent > 0
          ? FuncCommon.roundNumber(ValuaSellExpensesPercent)
          : 0;
      this.ChangeRows(
        FuncCommon.roundNumber(ValuaSellExpensesPercent),
        row,
        'SellExpenses',
      );
      //  Chi phí tài chính
      var ValuaFinancialExpensesPercent =
        (FinancialExpensesPercent * UnitPriceNET) / 100;
      ValuaFinancialExpensesPercent =
        ValuaFinancialExpensesPercent > 0
          ? FuncCommon.roundNumber(ValuaFinancialExpensesPercent)
          : 0;
      this.ChangeRows(
        FuncCommon.roundNumber(ValuaFinancialExpensesPercent),
        row,
        'FinancialExpenses',
      );
      // tính tổng quản lý phí
      var ValuaQLP = (ManagementFeePercent * UnitPriceNET) / 100;
      ValuaQLP = ValuaQLP > 0 ? FuncCommon.roundNumber(ValuaQLP) : 0;
      this.ChangeRows(FuncCommon.roundNumber(ValuaQLP), row, 'ManagementFee');
      // tính LN mong muốn
      var ValuaProfit = (ProfitPercent * UnitPriceNET) / 100;
      ValuaProfit = ValuaProfit > 0 ? FuncCommon.roundNumber(ValuaProfit) : 0;
      this.ChangeRows(FuncCommon.roundNumber(ValuaProfit), row, 'Profit');
      // tính CSKH
      var ValuaCSKH = (CSKHPercent * UnitPriceNET) / 100;
      ValuaCSKH = FuncCommon.roundNumber(ValuaCSKH);
      this.ChangeRows(FuncCommon.roundNumber(ValuaCSKH), row, 'CSKH');
      // tính đơn giá bán
      var ValuaUnitPrice =
        UnitPriceNET +
        ValuaSellExpensesPercent +
        ValuaFinancialExpensesPercent +
        ValuaQLP +
        ValuaCSKH +
        OtherCosts;
      ValuaUnitPrice =
        ValuaUnitPrice > 0 ? FuncCommon.roundNumber(ValuaUnitPrice) : 0;
      this.ChangeRows(FuncCommon.roundNumber(ValuaUnitPrice), row, 'UnitPrice');
      // tính đơn giá bán ngoại tệ
      var UnitPriceOc = ValuaUnitPrice / this.state.CurrencyRate;
      UnitPriceOc =
        UnitPriceOc > 0 ? parseFloat(UnitPriceOc.toString()).toFixed(2) : 0;
      this.ChangeRows(UnitPriceOc, row, 'UnitPriceOc');
      // tính LN thực tế
      var ValuaProfitActual = ValuaProfit;
      ValuaProfitActual =
        ValuaProfitActual > 0 ? FuncCommon.roundNumber(ValuaProfitActual) : 0;
      this.ChangeRows(
        FuncCommon.roundNumber(ValuaProfitActual),
        row,
        'ProfitActual',
      );
      // tính tiền NET
      var ValuaProfitActualPercent = (ValuaProfitActual * 100) / ValuaUnitPrice;
      ValuaProfitActualPercent =
        ValuaProfitActualPercent > 0 ? ValuaProfitActualPercent : 0;
      this.ChangeRows(
        FuncCommon.roundNumber(ValuaProfitActualPercent),
        row,
        'ProfitActualPercent',
      );
      // tính thành tiền bán trước thuế
      var ValuaAmountNoVAT = Quantity * ValuaUnitPrice;
      ValuaAmountNoVAT =
        ValuaAmountNoVAT > 0 ? FuncCommon.roundNumber(ValuaAmountNoVAT) : 0;
      this.ChangeRows(
        FuncCommon.roundNumber(ValuaAmountNoVAT),
        row,
        'AmountNoVAT',
      );

      // tính VAT
      var ValuaVAT = (VATPercent * ValuaAmountNoVAT) / 100;
      ValuaVAT = ValuaVAT > 0 ? FuncCommon.roundNumber(ValuaVAT) : 0;
      this.ChangeRows(FuncCommon.roundNumber(ValuaVAT), row, 'VATAmountOC');
      // tính thành tiền bán sau VAT
      var ValuaAmountVAT = ValuaVAT + ValuaAmountNoVAT;
      ValuaAmountVAT =
        ValuaAmountVAT > 0 ? FuncCommon.roundNumber(ValuaAmountVAT) : 0;
      this.ChangeRows(FuncCommon.roundNumber(ValuaAmountVAT), row, 'AmountVAT');
    }
    if (columnName === 'Quantity') {
      var Quantity = FuncCommon.roundNumber(
        parseFloat(ListData[rowindex].Quantity),
      );
      controller.GetUnitPriceOfProduct(
        {IdS: [ListData[rowindex].ProductId]},
        rs => {
          if (rs && rs.length > 0) {
            var ListData2 = this.lstdataDetail;
            ListData2.forEach((val, key) => {
              if (val.ParentGuid == ListData[rowindex].ProductId) {
                var obj = rs.find(x => x.ProductId === val.ProductId);
                if (obj != undefined) {
                  var slQuantity = obj.Quantity * Quantity;
                  this.ChangeRowsV2(slQuantity, key, 'Quantity');
                }
              }
            });
          }
        },
      );
    }
    if (
      columnName === 'ManagementFeePercent' ||
      columnName === 'SellExpensesPercent' ||
      columnName === 'FinancialExpensesPercent' ||
      columnName === 'ProfitPercent' ||
      columnName === 'CSKHPercent' ||
      columnName === 'ProfitActualPercent'
    ) {
      controller.GetUnitPriceOfProduct(
        {IdS: [ListData[rowindex].ProductId]},
        rs => {
          if (rs.length > 0) {
            var ListData2 = this.lstdataDetail;
            ListData2.forEach((val, key) => {
              if (val.ParentGuid == ListData[rowindex].ProductId) {
                var obj = rs.find(x => x.ProductId === val.ProductId);
                if (obj != undefined) {
                  var ManagementFeePercent =
                    ListData[rowindex].ManagementFeePercent > 0
                      ? parseFloat(ListData[rowindex].ManagementFeePercent)
                      : 0;
                  var ProfitPercent =
                    ListData[rowindex].ProfitPercent > 0
                      ? parseFloat(ListData[rowindex].ProfitPercent)
                      : 0;
                  var CSKHPercent =
                    ListData[rowindex].CSKHPercent > 0
                      ? parseFloat(ListData[rowindex].CSKHPercent)
                      : 0;
                  var ProfitActualPercent =
                    ListData[rowindex].ProfitActualPercent > 0
                      ? parseFloat(ListData[rowindex].ProfitActualPercent)
                      : 0;
                  var row = key;
                  this.ChangeRowsV2(
                    ManagementFeePercent,
                    key,
                    'ManagementFeePercent',
                  );
                  this.ChangeRowsV2(ProfitPercent, key, 'ProfitPercent');
                  this.ChangeRowsV2(CSKHPercent, key, 'CSKHPercent');
                  this.ChangeRowsV2(
                    ProfitActualPercent,
                    key,
                    'ProfitActualPercent',
                  );
                  var ListData2 = this.lstdataDetail;
                  var Quantity = parseFloat(ListData2[row].Quantity);
                  var UnitPriceNET = parseFloat(ListData2[row].UnitPriceNET);
                  var ManagementFeePercent = parseFloat(
                    ListData2[row].ManagementFeePercent,
                  );
                  var ProfitPercent = parseFloat(ListData2[row].ProfitPercent);
                  var OtherCosts = parseFloat(ListData2[row].OtherCosts);
                  var CSKHPercent =
                    ListData2[row].CSKHPercent > 0
                      ? parseFloat(ListData2[row].CSKHPercent)
                      : 0;
                  var VATPercent =
                    ListData2[row].VATPercent > 0
                      ? parseFloat(ListData2[row].VATPercent)
                      : 0;
                  var ProfitActualPercent =
                    ListData2[row].ProfitActualPercent > 0
                      ? parseFloat(ListData2[row].ProfitActualPercent)
                      : 0;
                  var SellExpensesPercent =
                    ListData2[row].SellExpensesPercent > 0
                      ? parseFloat(ListData2[row].SellExpensesPercent)
                      : 0;
                  var FinancialExpensesPercent =
                    ListData2[row].FinancialExpensesPercent > 0
                      ? parseFloat(ListData2[row].FinancialExpensesPercent)
                      : 0;

                  //  Chi phí bán hàng
                  var ValuaSellExpensesPercent =
                    (SellExpensesPercent * UnitPriceNET) / 100;
                  ValuaSellExpensesPercent =
                    ValuaSellExpensesPercent > 0
                      ? FuncCommon.roundNumber(ValuaSellExpensesPercent)
                      : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaSellExpensesPercent),
                    row,
                    'SellExpenses',
                  );
                  //  Chi phí tài chính
                  var ValuaFinancialExpensesPercent =
                    (FinancialExpensesPercent * UnitPriceNET) / 100;
                  ValuaFinancialExpensesPercent =
                    ValuaFinancialExpensesPercent > 0
                      ? FuncCommon.roundNumber(ValuaFinancialExpensesPercent)
                      : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaFinancialExpensesPercent),
                    row,
                    'FinancialExpenses',
                  );
                  // tính tổng quản lý phí
                  var ValuaQLP = (ManagementFeePercent * UnitPriceNET) / 100;
                  ValuaQLP =
                    ValuaQLP > 0 ? FuncCommon.roundNumber(ValuaQLP) : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaQLP),
                    row,
                    'ManagementFee',
                  );
                  // tính LN mong muốn
                  var ValuaProfit = (ProfitPercent * UnitPriceNET) / 100;
                  ValuaProfit =
                    ValuaProfit > 0 ? FuncCommon.roundNumber(ValuaProfit) : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaProfit),
                    row,
                    'Profit',
                  );
                  // tính CSKH
                  var ValuaCSKH = (CSKHPercent * UnitPriceNET) / 100;
                  ValuaCSKH = FuncCommon.roundNumber(ValuaCSKH);
                  this.ChangeRows(
                    FuncCommon.roundNumber(ValuaCSKH),
                    row,
                    'CSKH',
                  );
                  // tính đơn giá bán
                  var ValuaUnitPrice =
                    UnitPriceNET +
                    ValuaSellExpensesPercent +
                    ValuaFinancialExpensesPercent +
                    ValuaQLP +
                    ValuaCSKH +
                    OtherCosts;
                  ValuaUnitPrice =
                    ValuaUnitPrice > 0
                      ? FuncCommon.roundNumber(ValuaUnitPrice)
                      : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaUnitPrice),
                    row,
                    'UnitPrice',
                  );
                  // tính đơn giá bán ngoại tệ
                  var UnitPriceOc = ValuaUnitPrice / this.state.CurrencyRate;
                  UnitPriceOc =
                    UnitPriceOc > 0
                      ? parseFloat(UnitPriceOc.toString()).toFixed(2)
                      : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(UnitPriceOc),
                    row,
                    'UnitPriceOc',
                  );
                  // tính LN thực tế
                  var ValuaProfitActual = ValuaProfit;
                  ValuaProfitActual =
                    ValuaProfitActual > 0
                      ? FuncCommon.roundNumber(ValuaProfitActual)
                      : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaProfitActual),
                    row,
                    'ProfitActual',
                  );
                  // tính tiền NET
                  var ValuaProfitActualPercent =
                    (ValuaProfitActual * 100) / ValuaUnitPrice;
                  ValuaProfitActualPercent =
                    ValuaProfitActualPercent > 0 ? ValuaProfitActualPercent : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaProfitActualPercent),
                    row,
                    'ProfitActualPercent',
                  );
                  // tính thành tiền bán trước thuế
                  var ValuaAmountNoVAT = Quantity * ValuaUnitPrice;
                  ValuaAmountNoVAT =
                    ValuaAmountNoVAT > 0
                      ? FuncCommon.roundNumber(ValuaAmountNoVAT)
                      : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaAmountNoVAT),
                    row,
                    'AmountNoVAT',
                  );
                  // tính VAT
                  var ValuaVAT = (VATPercent * ValuaAmountNoVAT) / 100;
                  ValuaVAT =
                    ValuaVAT > 0 ? FuncCommon.roundNumber(ValuaVAT) : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaVAT),
                    row,
                    'VATAmountOC',
                  );
                  // tính thành tiền bán sau VAT
                  var ValuaAmountVAT = ValuaVAT + ValuaAmountNoVAT;
                  ValuaAmountVAT =
                    ValuaAmountVAT > 0
                      ? FuncCommon.roundNumber(ValuaAmountVAT)
                      : 0;
                  this.ChangeRowsV2(
                    FuncCommon.roundNumber(ValuaAmountVAT),
                    row,
                    'AmountVAT',
                  );
                }
              }
            });
          }
        },
      );
    }
  };
  ChangeRows = (val, rowindex, columnName) => {
    let res = this.state.rows;
    res[rowindex][columnName] = val.toString();
    this.setState({rows: res});
  };
  ChangeRowsV2 = (val, rowindex, columnName) => {
    this.lstdataDetail[rowindex][columnName] = val.toString();
  };
  //
  _openComboboxProduct() {}
  openComboboxProduct = d => {
    this._openComboboxProduct = d;
  };
  onActionProduct(i) {
    this.setState({rowchange: i});
    this._openComboboxProduct();
  }
  ChangeProduct = rs => {
    if (rs !== null) {
      this.handleChangeRows(rs.value, this.state.rowchange, 'ProductId');
    }
  };
  searchItems = (callback, textsearch) => {
    controller.GetItems(textsearch, rs => {
      callback(rs);
    });
  };
  //
  _openComboboxUnit() {}
  openComboboxUnit = d => {
    this._openComboboxUnit = d;
  };
  onActionUnit(i) {
    this.setState({rowchange: i});
    this._openComboboxUnit();
  }
  ChangeUnit = rs => {
    if (rs !== null) {
      this.handleChangeRows(rs.value, this.state.rowchange, 'UnitId');
      this.handleChangeRows(rs.text, this.state.rowchange, 'UnitName');
    }
  };
  //#endregion
  //#region điều khoản báo giá
  //
  _openComboboxTerms() {}
  openComboboxTerms = d => {
    this._openComboboxTerms = d;
  };
  onActionTerms(i) {
    this.setState({rowchange: i});
    this._openComboboxTerms();
  }
  ChangeTerms = rs => {
    if (rs !== null) {
      this.settxt('TermId', rs.value);
      this.settxt('TermName', rs.text);
      this.settxt('TermContent', rs.TermContent);
    }
  };
  //#endregion

  Insert = () => {
    controller.Insert(this.state, rs => {
      this.BackList();
    });
  };
  BackList() {
    Actions.pop();
    Actions.refresh({moduleId: 'back', ActionTime: new Date().getTime()});
  }

  //#region openAttachment
  openAttachment() {}
  openCombobox_Att = d => {
    this.openAttachment = d;
  };
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  //#region open img
  openPhotoLibrary() {}
  openLibrary = d => {
    this.openPhotoLibrary = d;
  };
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion

  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
        } else if (response.error) {
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion

  //#endregion

  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      {toValue: 0, duration: 333},
    );
    this.Animated_on_ViewBG = Animated.timing(this.state.transIcon_ViewViewBG, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewBG = Animated.timing(
      this.state.transIcon_ViewViewBG,
      {toValue: 0, duration: 333},
    );
  };
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ViewAll: !this.state.ViewAll});
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ViewDetail: !this.state.ViewDetail});
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
        this.Animated_on_ViewBG,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ViewDetail: true, ViewAll: true, ViewBG: true});
    } else if (val === 'ViewBG') {
      if (this.state.ViewBG === false) {
        Animated.sequence([this.Animated_on_ViewBG]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewBG]).start();
      }
      this.setState({ViewBG: !this.state.ViewBG});
    }
  };
  //#endregion
}
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Quotations_Form_Component);
