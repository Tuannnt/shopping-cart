import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TextInput,
  TouchableWithoutFeedback,
  PermissionsAndroid,
} from 'react-native';
import { ListView } from 'deprecated-react-native-listview'
import { connect } from 'react-redux'
import { Icon, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Tabbar from 'react-native-tabbar-bottom'
import DocumentPicker from 'react-native-document-picker';
import styles from '../../TM/TasksStyle';
import { WebView } from 'react-native-webview';
import { Button } from 'native-base';
import API_Orders from "../../../network/SM/API_Orders";
import { FuncCommon } from '../../../utils';
import RNFetchBlob from 'rn-fetch-blob'
import TabNavigator from "react-native-tab-navigator";
import { createOnShouldStartLoadWithRequest } from "react-native-webview/lib/WebViewShared";
import Swipeout from "react-native-swipeout";
import Fonts from "../../../theme/fonts";
import configApp from "../../../configApp";

const stylesX = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingRight: 2,
    paddingBottom: 5,
    paddingLeft: 10,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  title: {
    fontSize: 14,
    color: '#000',
    textAlign: 'left',
    flex: 8
  },
  date: {
    fontSize: 9,
    minWidth: 10,
    flex: 2,
    fontStyle: 'italic',
  },
  container_text: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  description: {
    fontSize: 10,
    fontStyle: 'italic',
    paddingBottom: 3
  },
  photo: {
    height: 50,
    width: 50,
  },
  statusNew: {
    marginTop: 5,
    width: 8,
    height: 8,
    borderRadius: 4,
    borderWidth: 0.1,
    borderColor: 'white',
    backgroundColor: '#4baf57',
  },

});
function convertMiniTitle(_title, _length) {
  if (_title === null) {
    _title = '';
  }
  if (_title !== '') {
    _title = _title.substring(0, _length) + '...';
  }
  return _title;
}
class OrdersAttachmentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AttachmentGuid: null,
      ListAttachments: [],
      LiFiles: [],
      model: {}
    }
  }
  // Lấy danh sách đính kèm
  loadAttachment(item) {
    var _obj = {
      DBNameDocument:configApp.DBNameDocument_AM,
      AttachmentGuid: this.state.AttachmentGuid,
      Scheme: "Sale",
      TableName: "OrderAttachments"
    };
    this.setState(
      {
        ListAttachments: [],
      });
    API_Orders.GetAttList(_obj).then(rs => {
      if (JSON.parse(rs.data.data).length > 0) {
        let _ListAttachments = JSON.parse(rs.data.data);
        for (var i = 0; i < _ListAttachments.length; i++) {
          this.state.ListAttachments.push(
            {
              attGuid: _ListAttachments[i].AttachmentGuid,
              title: _ListAttachments[i].Title,
              description: _ListAttachments[i].FileName,
              data: ['Pizza', 'Burger', 'Risotto'],
              image_url: require("@images/TypeFiles/file.png"),
              createdDate: FuncCommon.ConDate(_ListAttachments[i].CreatedDate, 1)
            });
        }
        this.setState(
          {
            ListAttachments: this.state.ListAttachments,
          });
      }
    })
      .catch(error => {
        console.log('Error when call API Mobile.');
      });
  }
  //Thêm mới đính kèm
  submit = () => {
    let _obj = {
      DBNameDocument:configApp.DBNameDocument_AM,
      AttachmentGuid: this.props.AttachmentGuid,
      Scheme: "Sale",
      TableName: "OrderAttachments"
    };
    let fd = new FormData();
    for (var i = 0; i < this.state.LiFiles.length; i++) {
      fd.append(this.state.LiFiles[i].name, this.state.LiFiles[i]);
    }
    fd.append('insert', JSON.stringify(_obj));
    API_Orders.InsertAttach(fd).then(rs => {
      if (rs.Error) {
      } else {
        this.loadAttachment();
      }

    }).catch(error => {
    });
  }
  //Xóa
  onDelete(item) {
    let _obj = {
      DBNameDocument:configApp.DBNameDocument_AM,
      AttachmentGuid: item.attGuid,
      Scheme: "Sale",
      TableName: "OrderAttachments"
    };
    API_Orders.DeleteAttach(_obj)
      .then(res => {
        Actions.pop();
      })
      .catch(error => { });
  }
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          'title': 'ReactNativeCode Storage Permission',
          'message': 'ReactNativeCode App needs access to your storage to download Photos.'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Storage Permission Granted.");
      }
      else {
        console.log("Storage Permission Not Granted");
      }
    } catch (err) {
      console.warn(err)
    }
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    this.setState({
      AttachmentGuid: this.props.AttachmentGuid
    })
    this.loadAttachment(this.props.AttachmentGuid)
  }
  componentWillReceiveProps(nextProps) {
    console.log(this.props.getItem);
  }
  onDownload(attachObject) {
    let dirs = RNFetchBlob.fs.dirs;
    console.log(dirs.DownloadDir);
    RNFetchBlob
      .config({
        addAndroidDownloads: {
          useDownloadManager: true, // <-- this is the only thing required
          // Optional, override notification setting (default to true)
          notification: true,
          // Optional, but recommended since android DownloadManager will fail when
          // the url does not contains a file extension, by default the mime type will be text/plain
          mime: '/',
          description: 'File downloaded by download manager.',
          //title: new Date().toLocaleString() + ' - test.xlsx',
          //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
          path: "file://" + dirs.DownloadDir + '/' + attachObject.title, //using for android
        }
      })
      .fetch('POST', 'http://mbapi.essolution.net/api/Attachments/Download', {  DBNameDocument:configApp.DBNameDocument_AM, AttachmentGuid: attachObject.attGuid, Scheme: "Sale", TableName: "OrderAttachments" })
      .then((resp) => {
        // the path of downloaded file
        resp.path()
      })
  }
  onClickBack() {
    Actions.pop();
  }
  async uploadFile() {
    // Pick a single file
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.LiFiles;
      _liFiles.push(res);
      this.setState(
        {
          LiFiles: _liFiles
        }
      );
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }
  onSwipeOpen(item) {
    this.setState({
      model: item,
    });
  }
  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }
  render() {
    const SwipeoutSetting = {
      autoClose: true,
      right: [
        {
          component: (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
                backgroundColor: '#fff',
              }}>
              <Icon
                name="delete"
                size={20}
                color="red"
                onPress={() => this.onDelete(this.state.model)}
              />
            </View>
          ),
        },
      ],
      buttonWidth: 40,
      rowId: this.props.index,
      sectionId: 1,
    };
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={this.onClickBack} underlayColor="white">
          <View style={styles.boxSearch}>
            <View style={{ flex: 1, width: 45, height: 50, textAlign: 'center', justifyContent: "center", }} onPress={() => this.onClickBack()}>
              <Icon
                name='left'
                type='antdesign'
              />
            </View>
            <View style={{ flex: 8, height: 50, textAlign: 'left', justifyContent: "center", }}>
              <Text style={{ height: 50, textAlign: 'left', textAlignVertical: "center", }}>
                Danh sách đính kèm
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.container}>
          <FlatList
            data={this.state.ListAttachments}
            renderItem={({ item, index }) => (
              <Swipeout
                {...SwipeoutSetting}
                onOpen={() => this.onSwipeOpen(item)}
                onClose={() => this.onSwipeClose(item)}>
                <TouchableWithoutFeedback onPress={() => this.onDownload(item)} underlayColor="white">
                  <View style={stylesX.container}>
                    <View style={{ width: 50, height: 50 }}>
                      <Image
                        style={{ width: 50, height: 50 }}
                        source={item.image_url}
                      />
                    </View>
                    <View style={stylesX.container_text}>
                      <View style={{ flex: 1, flexDirection: 'row', width: "100%" }}>
                        <Text style={stylesX.title}>
                          {convertMiniTitle(item.title, 30)}
                        </Text>
                        <Text style={stylesX.date}>
                          {item.createdDate}
                        </Text>
                      </View>
                      <Text style={stylesX.description}>
                        {convertMiniTitle(item.description, 130)}
                      </Text>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </Swipeout>
            )}
          />
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Button block info style={{ flex: 1 }} onPress={() => this.uploadFile()}>
            <Text style={{ color: 'white', fontSize: 15 }}>Tải lên</Text>
          </Button>
          <Button block info style={{ flex: 1 }} onPress={() => this.submit()}>
            <Text style={{ color: 'white', fontSize: 15 }}>Lưu</Text>
          </Button>
        </View>
      </View>
    );
  }
}
//Redux
const mapStateToProps = state => ({
  getItem: state.TM.paraTM
})
const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(OrdersAttachmentComponent);