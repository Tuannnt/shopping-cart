import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Alert,
  TouchableOpacity,
  View,
  FlatList,
  Text,
  Animated,
  Dimensions,
  TouchableHighlight,
  ScrollView,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import { Header, Icon, Divider, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_Orders from '../../../network/SM/API_Orders';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import {
  CustomView,
  TabBarBottomCustom,
  LoadingComponent,
  TabBar_Title,
  Combobox,
} from '@Component';
import controller from './controller';
import listCombobox from './listCombobox';
import { FuncCommon } from '../../../utils';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class ViewOrderComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: {
        OrderNumber: '',
        QuotationNo: '',
        CustomerId: '',
        CreatedDate: '',
        RequireDate: '',
        Status: '',
        TotalMoney: '0',
      },
      List: [],
      isFetching: false,
      loading: true,
      isApproval: false,
      //hiển thị thông tin chung
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
      ApprovalStatus: 'W',
      ApprovalStatusName: '',
      OrderStatus: '0',
      OrderStatusName: '',
      ListQuotations: null,
    };
    this.listtabbarBotom = [
      // {
      //   Title: 'Ý kiến',
      //   Icon: 'message1',
      //   Type: 'antdesign',
      //   Value: 'cmt',
      //   Checkbox: false,
      //   OffEditcolor: true,
      // },
      // {
      //   Title: 'Đề nghị sản xuất',
      //   Icon: 'checksquareo',
      //   Type: 'antdesign',
      //   Value: 'check',
      //   Checkbox: false,
      //   OffEditcolor: true,
      // },
      {
        Title: 'Đính kèm',
        Icon: 'attachment',
        Type: 'entypo',
        Value: 'att',
        Checkbox: false,
        OffEditcolor: true,
      },
      // {
      //   Title: 'Xoá',
      //   Icon: 'delete',
      //   Type: 'antdesign',
      //   Value: 'delete',
      //   Checkbox: false,
      //   OffEditcolor: true,
      // },
    ];
    this.listtabbarBotomV2 = [
      // {
      //   Title: 'Ý kiến',
      //   Icon: 'message1',
      //   Type: 'antdesign',
      //   Value: 'cmt',
      //   Checkbox: false,
      //   OffEditcolor: true,
      // },
      {
        Title: 'Đính kèm',
        Icon: 'attachment',
        Type: 'entypo',
        Value: 'att',
        Checkbox: false,
        OffEditcolor: true,
      },
      {
        Title: 'Xoá',
        Icon: 'delete',
        Type: 'antdesign',
        Value: 'delete',
        Checkbox: false,
        OffEditcolor: true,
      },
    ];
  }
  componentDidMount = () => {
    this.Skill();
    this.setViewOpen('ViewDetail');
    // controller.isApproval(this.props.OrderGuid, rs => {
    //   if (rs === 200) {
    //     this.setState({isApproval: true});
    //   }
    // });
    this.GetOrderById();
    this.GetOrderDetailsAll();
  };
  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <TabBar_Title
          title={'Chi tiết đơn hàng'}
          callBack={() => this.callBackList()}
        />
        <ScrollView style={{ flex: 1 }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewAll')}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
            </View>
            <Animated.View
              style={{
                transform: [{ rotate: rotateStart_ViewAll }, { perspective: 4000 }],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewAll !== true ? null : (
            <View style={[{ padding: 10 }]}>
              <View style={{ marginTop: 5, marginBottom: 5 }}>
                <Text style={[AppStyles.Labeldefault, { fontSize: 16 }]}>
                  {this.state.model.ObjectName}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Số ĐH :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.OrderNumber}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Số đơn hàng KD :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.SaleOrderNo}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Số báo giá :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.QuotationNo}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Loại :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.IsInvoice === 'Y'
                      ? 'Có XHĐ'
                      : 'Không XHĐ'}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày đặt :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.ConDate(this.state.model.OrderDate, 0)}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày Yêu cầu :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.ConDate(this.state.model.RequiredDate, 0)}
                  </Text>
                </View>
              </View>
              {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Hạn thanh toán :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.ConDate(this.state.model.DueDate, 0)}
                  </Text>
                </View>
              </View> */}
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Loại tiền :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.CurrencyId}
                  </Text>
                </View>
              </View>
              {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Số ngày được nợ :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.DueDay}
                  </Text>
                </View>
              </View> */}
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>
                    Địa chỉ khách hàng :
                  </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.ObjectAddress}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Cấp độ :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.Levels}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>
                    Địa chỉ giao hàng :
                  </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.ShipAddress}
                  </Text>
                </View>
              </View>
              {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Quản lý phí :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.ManagementFee}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Chiết khấu :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.Discount}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Ghi chú :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.Note}
                  </Text>
                </View>
              </View> */}
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.Description}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {
                        color: controller.CustomStatus(
                          this.state.model.OrderStatus,
                          this.state.model.ApprovalStatus,
                          1,
                        ),
                      },
                    ]}>
                    {controller.CustomStatus(
                      this.state.model.OrderStatus,
                      this.state.model.ApprovalStatus,
                      0,
                    )}
                  </Text>
                </View>
              </View>
              {/* <View style={{flexDirection: 'row', marginTop: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>CPVC :</Text>
                </View>
                <View style={{flex: 2, alignItems: 'flex-end'}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.convertMoneyDecimal(this.state.model.ShipCosts)}
                  </Text>
                </View>
              </View> */}
              {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Tổng tiền :</Text>
                </View>
                <View style={{ flex: 2, alignItems: 'flex-end' }}>
                  <Text style={[AppStyles.Labeldefault]}>
                    {FuncCommon.convertMoneyDecimal(
                      this.state.model.TotalOfMoney,
                    )}
                  </Text>
                </View>
              </View> */}
            </View>
          )}
          <Divider />
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewDetail')}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  { rotate: rotateStart_ViewDetail },
                  { perspective: 4000 },
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewDetail !== true ? (
            <View style={{ flex: 1 }} />
          ) : (
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.state.List}
                style={{ flex: 1 }}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, i }) => (
                  <TouchableOpacity
                    key={i}
                    style={{
                      flex: 1,
                      padding: 10,
                      borderBottomColor: AppColors.gray,
                      borderBottomWidth: 0.5,
                    }}
                    onPress={() => Actions.listOrderDetails({ DataView: item })}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row' }}>
                          <Text
                            style={[
                              AppStyles.Titledefault,
                              { width: '100%', flex: 10 },
                            ]}>
                            {FuncCommon.handleLongText(item.ItemName, 35)}
                          </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{ flexDirection: 'row', flex: 1 }}>
                            <Text style={[AppStyles.Textdefault]}>
                              Số lượng:{' '}
                            </Text>
                            <View
                              style={{
                                flex: 1,
                                marginRight: 10,
                              }}>
                              <Text style={[AppStyles.Textdefault]}>
                                {FuncCommon.addPeriod(item.Quantity)}
                              </Text>
                            </View>
                          </View>
                          <Text style={[AppStyles.Textdefault, { flex: 1 }]}>
                            ĐVT: {item.UnitName}
                          </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{ flexDirection: 'row', flex: 1 }}>
                            <Text style={[AppStyles.Textdefault]}>
                              Đơn Giá:{' '}
                            </Text>
                            <View
                              style={{
                                flex: 1,
                                marginRight: 10,
                              }}>
                              <Text style={[AppStyles.Textdefault]}>
                                {FuncCommon.addPeriod(item.UnitPrice)}
                              </Text>
                            </View>
                          </View>
                          <View style={{ flexDirection: 'row', flex: 1 }}>
                            <Text style={[AppStyles.Textdefault]}>
                              Tổng Tiền:{' '}
                            </Text>
                            <View
                              style={{
                                flex: 1,
                                marginRight: 10,
                              }}>
                              <Text style={[AppStyles.Labeldefault]}>
                                {FuncCommon.addPeriod(item.Amount)}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                      <View style={{ justifyContent: 'center' }}>
                        <Icon
                          color="#888888"
                          name={'chevron-thin-right'}
                          type="entypo"
                          size={20}
                          onPress={() =>
                            Actions.listOrderDetails({ DataView: item })
                          }
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          )}
        </ScrollView>
        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={
              this.state.model.Type === 'TM'
                ? this.listtabbarBotom
                : this.listtabbarBotomV2
            }
            onCallbackValueBottom={callback => this.onConfirm(callback)}
          />
        </View>
        <Combobox
          value={this.state.ApprovalStatus}
          TypeSelect={'single'}
          callback={this.ChangeApprovalStatus}
          data={listCombobox.ListApprovalStatus}
          nameMenu={'Chọn trạng thái'}
          eOpen={this.openComboboxApprovalStatus}
          position={'bottom'}
        />
        <Combobox
          value={this.state.OrderStatus}
          TypeSelect={'single'}
          callback={this.ChangeOrderStatus}
          data={listCombobox.ListOrderStatus}
          nameMenu={'Chọn trạng thái đơn hàng'}
          eOpen={this.openComboboxOrderStatus}
          position={'bottom'}
        />
        {this.CustomView()}
      </View>
    );
  }
  CustomView = () => {
    return (
      <CustomView eOpen={this.openViewisApproval}>
        <View style={{ height: 250, width: DRIVER.width - 50 }}>
          <View style={{ alignItems: 'center', padding: 10 }}>
            <Text style={[AppStyles.Titledefault]}>Thông báo</Text>
          </View>
          <Divider />
          <View
            style={{
              marginBottom: 10,
              justifyContent: 'space-between',
              flex: 1,
            }}>
            <View>
              <Text style={{ fontSize: 24, textAlign: 'center', marginTop: 15 }}>
                Xác nhận thông tin
              </Text>
              <Text
                style={{
                  color: 'gray',
                  fontSize: 15,
                  textAlign: 'center',
                  marginTop: 15,
                }}>
                {`Bạn có chắc chắn muốn yêu cầu sản xuất [${this.state.model.OrderNumber
                  }]`}
              </Text>
            </View>
            <View>
              <Divider />
              <TouchableHighlight
                style={[
                  AppStyles.FormInput,
                  {
                    backgroundColor: AppColors.ColorButtonSubmit,
                    margin: 10,
                    justifyContent: 'center',
                  },
                ]}
                onPress={() => this.submit()}>
                <Text style={[AppStyles.TextInput, { color: '#fff' }]}>
                  Xác nhận
                </Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </CustomView>
    );
  };
  callBackList() {
    Actions.pop();
    Actions.refresh({ moduleId: 'Back', ActionTime: new Date().getTime() });
  }
  submit = () => {
    this.openAcctionViewisApproval();
    var obj = {
      OrderGuid: this.props.OrderGuid,
    };
    controller.UpdateStatus(obj.OrderGuid, rs => {
      Alert.alert('Thông báo', rs.message);
      if (rs.errorCode === 200) {
        this.componentDidMount();
      }
    });
  };
  //
  _openViewisApproval() { }
  openViewisApproval = d => {
    this._openViewisApproval = d;
  };
  openAcctionViewisApproval = () => {
    this._openViewisApproval();
  };
  //
  _openComboboxApprovalStatus() { }
  openComboboxApprovalStatus = d => {
    this._openComboboxApprovalStatus = d;
  };
  onActionApprovalStatus() {
    this._openComboboxApprovalStatus();
  }
  ChangeApprovalStatus = rs => {
    this.state.ApprovalStatus = rs.value;
    this.setState({ ApprovalStatus: rs.value, ApprovalStatusName: rs.text });
  };
  //
  _openComboboxOrderStatus() { }
  openComboboxOrderStatus = d => {
    this._openComboboxOrderStatus = d;
  };
  onActionOrderStatus() {
    this._openComboboxOrderStatus();
  }
  ChangeOrderStatus = rs => {
    this.state.OrderStatus = rs.value;
    this.setState({ OrderStatus: rs.value, OrderStatusName: rs.text });
  };
  onConfirm = key => {
    switch (key) {
      case 'cmt':
        Actions.commentComponent({
          ModuleId: '28',
          ModuleIdDocument: '29',
          RecordGuid: this.props.OrderGuid,
        });
        break;
      case 'att':
        var obj = {
          ModuleId: '1',
          RecordGuid: this.props.OrderGuid,
        };
        Actions.attachmentComponent(obj);
        break;
      case 'check':
        this.openAcctionViewisApproval();
        break;
      case 'delete':
        Alert.alert(
          'Xác nhận thông tin',
          'bạn chắc chắn muốn xoá ' + this.state.model.OrderNumber + ' ?',
          [{ text: 'Xác nhận', onPress: () => this.APIDelete() }, { text: 'Đóng' }],
        );
        break;
      default:
        break;
    }
  };
  APIDelete = () => {
    controller.IsDeleted(this.state.model.OrderGuid, rs => {
      Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
      if (rs.data.errorCode === 200) {
        this.callBackList();
      }
    });
  };
  //Lấy thông tin đơn hàng
  GetOrderById = () => {
    FuncCommon.Data_Offline(async on => {
      if (on) {
        API_Orders.GetOrderById(this.props.OrderGuid)
          .then(res => {
            this.setState({
              model: JSON.parse(res.data.data),
            });
            FuncCommon.Data_Offline_Set(
              'API_Orders_GetOrderById' + this.props.OrderGuid,
              JSON.parse(res.data.data),
            );
            // this.GetOrderAndQuotations();
          })
          .catch(error => { });
      } else {
        var of = await FuncCommon.Data_Offline_Get(
          'API_Orders_GetOrderById' + this.props.OrderGuid,
        );
        this.setState({
          model: of,
        });
      }
    });
    this.setState({
      loading: false,
    });
  };
  //lấy danh sách các báo giá của đơn hàng
  GetOrderAndQuotations = () => {
    FuncCommon.Data_Offline(async on => {
      if (on) {
        API_Orders.GetOrderAndQuotations({ guid: this.props.OrderGuid })
          .then(res => {
            var data = JSON.parse(res.data.data);
            var list = this.state.ListQuotations;
            for (let i = 0; i < data.length; i++) {
              if (list !== null) {
                list = list + ', ' + data[i].QuotationNo;
              } else {
                list = data[i].QuotationNo;
              }
            }
            this.setState({
              ListQuotations: list,
            });
            FuncCommon.Data_Offline_Set(
              'API_Orders_GetOrderAndQuotations' + this.props.OrderGuid,
              list,
            );
            this.GetOrderDetailsAll();
          })
          .catch(error => { });
      } else {
        var of = await FuncCommon.Data_Offline_Get(
          'API_Orders_GetOrderAndQuotations' + this.props.OrderGuid,
        );
        this.setState({
          ListQuotations: of,
        });
      }
    });
  };
  //Lấy danh sách chi tiết đơn hàng
  GetOrderDetailsAll = () => {
    this.setState({ refreshing: true });
    FuncCommon.Data_Offline(async on => {
      if (on) {
        API_Orders.GetOrderDetailsAll({ OrderGuid: this.props.OrderGuid })
          .then(res => {
            var list = JSON.parse(res.data.data);
            this.setState({
              List: list,
              refreshing: false,
              loading: false,
            });
            FuncCommon.Data_Offline_Set(
              'API_Orders_GetOrderDetailsAll' + this.props.OrderGuid,
              JSON.parse(res.data.data),
            );
          })
          .catch(error => {
            this.setState({
              refreshing: false,
              loading: false,
            });
          });
      } else {
        var of = await FuncCommon.Data_Offline_Get(
          'API_Orders_GetOrderDetailsAll' + this.props.OrderGuid,
        );
        this.setState({
          List: of,
          refreshing: false,
          loading: false,
        });
      }
    });
  };
  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 1, duration: 333 },
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 0, duration: 333 },
    );
  };
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ ViewAll: !this.state.ViewAll });
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ ViewDetail: !this.state.ViewDetail });
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ ViewDetail: true, ViewAll: true });
    }
  };
  //#endregion
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ViewOrderComponent);
