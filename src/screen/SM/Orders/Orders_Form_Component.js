import React, {Component} from 'react';
import {
  FlatList,
  KeyboardAvoidingView,
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Animated,
  TextInput,
  ScrollView,
  Image,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Button, Divider, Icon} from 'react-native-elements';
import _ from 'lodash';
import {AppStyles, AppColors} from '@theme';
import {
  LoadingComponent,
  TabBar_Title,
  RequiredText,
  Combobox,
  ComboboxV2,
  OpenPhotoLibrary,
} from '@Component';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '@utils';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';
import {
  listCombobox as listComboboxQ,
} from '../Quotations/listCombobox';
import {
  controller as controllerQ,
} from '../Quotations/controller';
import { listCombobox  } from './listCombobox'
import { controller } from './controller'
import {Alert} from 'react-native';
import {PermissionsAndroid} from 'react-native';
const styles = StyleSheet.create({});
class Orders_Form_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      //hiển thị thông tin chung
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
      NumberAuto: null,
      ListQuotationNo: [],
      setOrderDate: false,
      setRequiredDate: false,
      setPaymentExpiredDate: false,
      Attachment: [],
      ListObjectKH: [],
      ListShippingUnit: [],
      ListWarehouses: [],
      ListProduct: [],
      ListUnit: [],
      rows: [{}],
      rowchange: 0,
      // các giá trị model
      OrderNumber: '',
      ListQuotationGuid: [],
      ListAllQuotationName: '',
      ObjectId: '',
      ObjectName: '',
      ObjectAddress: '',
      ShipAddress: '',
      OrderDate: new Date(),
      RequiredDate: null,
      DebtDay: '',
      DeliveryType: '',
      PaymentExpiredDate: null,
      StatusPB: '',
      StatusPBName: '',
      AmountCost: '0',
      Note: '',
      Description: '',
      IsInvoice: 'Y',
      StatusQuotation: 'TB',
      OrderStatus: '0',
      CurrencyId: 'VND',
      Type: 'TM',
      ApprovalStatus: 'W',
      CustomerGuid: null,
    };
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    this.Skill();
    this.setViewOpen('ViewAll');
    controller.getNumberAuto(rs => {
      this.setState({NumberAuto: rs, OrderNumber: rs.Value});
      this.GetCustomersAll('');
    });
  }
  GetCustomersAll = txt => {
    controller.GetCustomersAll(txt, rs => {
      if (rs !== null) {
        this.setState({ListObjectKH: rs, ObjectId: rs[0].value});
      }
      this.GetQuotationsAll();
    });
  };
  GetQuotationsAll = () => {
    controller.GetQuotationsAll(this.state.CustomerGuid, rs => {
      if (rs !== null) {
        this.setState({ListQuotationNo: rs});
      }
      this.GetItemsAllJExcel('');
    });
  };
  GetItemsAllJExcel = txt => {
    controller.GetItemsAllJExcel(txt, rs => {
      if (rs.length > 0) {
        this.setState({ListProduct: rs});
      }
      this.GetUnitByItemIdJExcel('', '');
    });
  };
  GetUnitByItemIdJExcel = (txt, ItemId) => {
    controller.GetUnitByItemIdJExcel(txt, ItemId, rs => {
      this.setState({ListUnit: rs});
    });
    this.GetObjectProvidersAll('');
  };
  GetObjectProvidersAll = txt => {
    controller.GetObjectProvidersAll(txt, rs => {
      this.setState({ListShippingUnit: rs});
    });
    this.GetWarehousesAllJExcel('');
  };
  GetWarehousesAllJExcel = txt => {
    controller.GetWarehousesAllJExcel(txt, rs => {
      this.setState({ListWarehouses: rs, loading: false});
    });
  };

  GetListPOVoucherDetails = list => {
    controller.GetListPOVoucherDetails(list, rs => {
      if (rs.length > 0) {
        this.setState({rows: rs});
      }
    });
  };
  GetQuotationsByQuotationGuid = list => {
    controller.GetQuotationsByQuotationGuid(
      list[0] !== 'null' ? list[0] : list[1],
      rs => {
        if (rs !== null) {
          this.setState({
            ObjectGuid: rs.CustomerGuid,
            ObjectId: rs.CustomerId,
            ObjectName: rs.CustomerName,
            ShipAddress: rs.Address,
            OrderTypeId: rs.OrderTypeId,
            OrderTypeName: rs.OrderTypeName,
            GroupItemGuid: rs.GroupItemGuid,
            GroupItemId: rs.GroupItemId,
            GroupName: rs.GroupName,
            Note: rs.Note,
            DebtDay: rs.DebtDay,
            PaymentExpiredDate:
              rs.PaymentExpiredDate != null
                ? $rootScope.convertToJSONDate(r.PaymentExpiredDate)
                : null,
            DeliveryType: rs.DeliveryType,
          });
        }
      },
    );
  };
  GetCustomerContacts = () => {
    controller.GetCustomerContacts(this.state.ObjectGuid, rs => {});
  };

  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const {
      Attachment,
      NumberAuto,
      OrderNumber,
      ListQuotationGuid,
      ListAllQuotationName,
      ObjectId,
      ObjectName,
      ObjectAddress,
      ShipAddress,
      OrderDate,
      RequiredDate,
      DebtDay,
      DeliveryType,
      StatusPB,
      StatusPBName,
      PaymentExpiredDate,
      AmountCost,
      Note,
      Description,
      setOrderDate,
      setRequiredDate,
      setPaymentExpiredDate,
      ListProduct,
      ListUnit,
      ListObjectKH,
      ListQuotationNo,
      ListShippingUnit,
      ListWarehouses,
    } = this.state;
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={[AppStyles.container]}>
            <TabBar_Title
              title={'Thêm mới đơn hàng'}
              callBack={() => this.BackList()}
            />
            <ScrollView>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  backgroundColor: '#bed9f6',
                }}
                onPress={() => this.setViewOpen('ViewAll')}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
                </View>
                <Animated.View
                  style={{
                    transform: [
                      {rotate: rotateStart_ViewAll},
                      {perspective: 4000},
                    ],
                    paddingRight: 10,
                    paddingLeft: 10,
                  }}>
                  <Icon type={'feather'} name={'chevron-up'} />
                </Animated.View>
              </TouchableOpacity>
              <Divider />
              {this.state.ViewAll !== true ? null : (
                <View>
                  <View
                    style={{
                      padding: 10,
                      paddingBottom: 0,
                      flexDirection: 'row',
                    }}>
                    <View style={{flex: 1, marginRight: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>
                          Số đơn hàng <RequiredText />
                        </Text>
                      </View>
                      {NumberAuto.IsEdits === true ? (
                        <TextInput
                          style={[AppStyles.FormInput, {color: 'black'}]}
                          underlineColorAndroid="transparent"
                          autoCapitalize="none"
                          value={OrderNumber}
                          onChangeText={txt => this.settxt('QuotationNo', txt)}
                        />
                      ) : (
                        <View style={[AppStyles.FormInput]}>
                          <Text
                            style={[
                              AppStyles.TextInput,
                              {color: AppColors.black},
                            ]}>
                            {OrderNumber}
                          </Text>
                        </View>
                      )}
                    </View>
                    <View style={{flex: 1, marginLeft: 5}}>
                      <View>
                        <Text style={AppStyles.Labeldefault}>
                          Số báo giá <RequiredText />
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionQuotationNo()}>
                        <Text
                          style={[
                            AppStyles.TextInput,
                            {color: AppColors.black},
                          ]}>
                          {ListAllQuotationName}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>
                        Tên khách hàng <RequiredText />
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionCustomer()}>
                      <Text
                        style={[
                          AppStyles.TextInput,
                          {
                            color:
                              ObjectId !== ''
                                ? AppColors.black
                                : AppColors.gray,
                          },
                        ]}>
                        {ObjectId === '' ? 'Chọn khách hàng' : ObjectName}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>
                        Địa chỉ khách hàng
                      </Text>
                    </View>
                    <TouchableOpacity style={[AppStyles.FormInput]}>
                      <Text
                        style={[
                          AppStyles.TextInput,
                          {
                            color:
                              ObjectId !== ''
                                ? AppColors.black
                                : AppColors.gray,
                          },
                        ]}>
                        {ObjectAddress}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>
                        Địa chỉ giao hàng
                      </Text>
                    </View>
                    <TextInput
                      style={[AppStyles.FormInput, {color: 'black'}]}
                      underlineColorAndroid="transparent"
                      placeholder="Nhập địa chỉ giao hàng"
                      autoCapitalize="none"
                      value={ShipAddress}
                      onChangeText={txt => this.settxt('ShipAddress', txt)}
                    />
                  </View>
                  <View
                    style={{
                      padding: 10,
                      paddingBottom: 0,
                      flexDirection: 'row',
                    }}>
                    <View style={{flex: 1, marginRight: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày đặt hàng <RequiredText />
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.setState({setOrderDate: true})}>
                        <Text
                          style={[
                            AppStyles.TextInput,
                            {color: AppColors.black},
                          ]}>
                          {FuncCommon.ConDate(OrderDate, 0)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, marginLeft: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>
                          Ngày yêu cầu giao hàng
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.setState({setRequiredDate: true})}>
                        <Text
                          style={[
                            AppStyles.TextInput,
                            {
                              color:
                                RequiredDate !== null
                                  ? AppColors.black
                                  : AppColors.gray,
                            },
                          ]}>
                          {RequiredDate !== null
                            ? FuncCommon.ConDate(RequiredDate, 0)
                            : 'dd/MM/yyyy'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={{
                      padding: 10,
                      paddingBottom: 0,
                      flexDirection: 'row',
                    }}>
                    <View style={{flex: 1, marginRight: 5}}>
                      <View>
                        <Text style={AppStyles.Labeldefault}>
                          Số ngày được nợ
                        </Text>
                      </View>
                      <TextInput
                        style={[AppStyles.FormInput, {color: 'black'}]}
                        underlineColorAndroid="transparent"
                        placeholder="Nhập số ngày"
                        autoCapitalize="none"
                        keyboardType={'numeric'}
                        value={DebtDay}
                        onChangeText={txt => this.settxt('DebtDay', txt)}
                      />
                    </View>
                    <View style={{flex: 1, marginLeft: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>
                          Hình thức giao hàng
                        </Text>
                      </View>
                      <TextInput
                        style={[AppStyles.FormInput, {color: 'black'}]}
                        underlineColorAndroid="transparent"
                        placeholder="Nhập hình thức giao hàng"
                        autoCapitalize="none"
                        value={DeliveryType}
                        onChangeText={txt => this.settxt('DeliveryType', txt)}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      padding: 10,
                      paddingBottom: 0,
                      flexDirection: 'row',
                    }}>
                    <View style={{flex: 1, marginRight: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>
                          Hạn thanh toán{' '}
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() =>
                          this.setState({setPaymentExpiredDate: true})
                        }>
                        <Text
                          style={[
                            AppStyles.TextInput,
                            {
                              color:
                                PaymentExpiredDate !== null
                                  ? AppColors.black
                                  : AppColors.gray,
                            },
                          ]}>
                          {PaymentExpiredDate !== null
                            ? FuncCommon.ConDate(PaymentExpiredDate, 0)
                            : 'dd/MM/yyyy'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, marginLeft: 5}}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>Phân bổ</Text>
                      </View>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionStatusPB()}>
                        <Text
                          style={[
                            AppStyles.TextInput,
                            {
                              color:
                                StatusPB !== ''
                                  ? AppColors.black
                                  : AppColors.gray,
                            },
                          ]}>
                          {StatusPB === '' ? 'Chọn hình thức' : StatusPBName}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Tổng CPVC</Text>
                    </View>
                    <TextInput
                      style={[
                        AppStyles.FormInput,
                        {color: 'black', textAlign: 'right'},
                      ]}
                      underlineColorAndroid="transparent"
                      autoCapitalize="none"
                      keyboardType={'numeric'}
                      value={FuncCommon.addPeriodTextInput(AmountCost)}
                      onChangeText={txt => this.setNumber('AmountCost', txt)}
                    />
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                    </View>
                    <TextInput
                      style={[AppStyles.FormInput, {color: 'black'}]}
                      underlineColorAndroid="transparent"
                      placeholder="Nhập ghi chú"
                      autoCapitalize="none"
                      value={Note}
                      onChangeText={txt => this.settxt('Note', txt)}
                    />
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Nội dung</Text>
                    </View>
                    <TextInput
                      style={[AppStyles.FormInput, {color: 'black'}]}
                      underlineColorAndroid="transparent"
                      placeholder="Nhập nội dung"
                      autoCapitalize="none"
                      value={Description}
                      onChangeText={txt => this.settxt('Description', txt)}
                    />
                  </View>
                  <View style={{flexDirection: 'column', padding: 10}}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          flexDirection: 'row',
                        }}
                        onPress={() => this.openAttachment()}>
                        <Icon
                          name="attachment"
                          type="entypo"
                          size={15}
                          color={AppColors.ColorAdd}
                        />
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {color: AppColors.ColorAdd},
                          ]}>
                          {' '}
                          Chọn file
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <Divider />
                    {Attachment.length > 0 ? (
                      Attachment.map((para, i) => (
                        <View
                          key={i}
                          style={[
                            styles.StyleViewInput,
                            {flexDirection: 'row', marginBottom: 3},
                          ]}>
                          <View
                            style={[
                              AppStyles.containerCentered,
                              {padding: 10},
                            ]}>
                            <Image
                              style={{width: 30, height: 30, borderRadius: 10}}
                              source={{
                                uri: controllerQ.renderImage(para.FileName),
                              }}
                            />
                          </View>
                          <TouchableOpacity
                            key={i}
                            style={{flex: 1, justifyContent: 'center'}}>
                            <Text style={AppStyles.Textdefault}>
                              {para.FileName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.containerCentered, {padding: 10}]}
                            onPress={() => this.removeAttactment(para)}>
                            <Icon
                              name="close"
                              type="antdesign"
                              size={20}
                              color={AppColors.ColorDelete}
                            />
                          </TouchableOpacity>
                        </View>
                      ))
                    ) : (
                      <View style={[{marginBottom: 50}]} />
                    )}
                  </View>
                </View>
              )}
              <Divider />
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  backgroundColor: '#bed9f6',
                }}
                onPress={() => this.setViewOpen('ViewDetail')}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
                </View>
                <Animated.View
                  style={{
                    transform: [
                      {rotate: rotateStart_ViewDetail},
                      {perspective: 4000},
                    ],
                    paddingRight: 10,
                    paddingLeft: 10,
                  }}>
                  <Icon type={'feather'} name={'chevron-up'} />
                </Animated.View>
              </TouchableOpacity>
              <Divider />
              {this.state.ViewDetail !== true ? (
                <View style={{flex: 1}} />
              ) : (
                <View style={{flex: 1}}>{this.customViewDetail()}</View>
              )}
              <View
                style={{flexDirection: 'row', padding: 10, marginBottom: 20}}>
                <TouchableOpacity
                  style={[
                    AppStyles.containerCentered,
                    {
                      flex: 1,
                      padding: 10,
                      borderRadius: 10,
                      marginLeft: 5,
                      backgroundColor: AppColors.ColorButtonSubmit,
                    },
                  ]}
                  onPress={() => this.submit()}>
                  <Text style={[AppStyles.Titledefault, {color: '#fff'}]}>
                    Lưu
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>

            {ListObjectKH.length === 0 ? null : (
              <Combobox
                value={ObjectId === '' ? ListObjectKH[0].value : ObjectId}
                TypeSelect={'single'}
                callback={this.ChangeCustomer}
                data={ListObjectKH}
                nameMenu={'Chọn khách hàng'}
                eOpen={this.openComboboxCustomer}
                position={'bottom'}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.searchCustomer(callback, textsearch)
                }
              />
            )}
            {ListQuotationNo.length === 0 ? null : (
              <Combobox
                value={ListQuotationGuid}
                TypeSelect={'multiple'}
                callback={this.ChangeQuotationNo}
                data={ListQuotationNo}
                nameMenu={'Chọn số báo giá'}
                eOpen={this.openComboboxQuotationNo}
                position={'bottom'}
              />
            )}
            <Combobox
              value={StatusPB}
              TypeSelect={'single'}
              callback={this.ChangeStatusPB}
              data={listCombobox.StatusPB}
              nameMenu={'Chọn hình thức phân bổ'}
              eOpen={this.openComboboxStatusPB}
              position={'bottom'}
            />
            <ComboboxV2
              callback={this.ChoiceAtt}
              data={listComboboxQ.ListComboboxAtt}
              eOpen={this.openCombobox_Att}
            />
            <OpenPhotoLibrary
              callback={this.callbackLibarary}
              openLibrary={this.openLibrary}
            />
            {setOrderDate !== true ? null : (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 100,
                  width: DRIVER.width,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({setOrderDate: false});
                  }}>
                  <Text style={{textAlign: 'right'}}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={OrderDate}
                  mode="date"
                  style={{width: DRIVER.width}}
                  onDateChange={setDate => this.settxt('OrderDate', setDate)}
                />
              </View>
            )}
            {setRequiredDate !== true ? null : (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 100,
                  width: DRIVER.width,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({setRequiredDate: false});
                  }}>
                  <Text style={{textAlign: 'right'}}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={RequiredDate ?? new Date()}
                  mode="date"
                  style={{width: DRIVER.width}}
                  onDateChange={setDate => this.settxt('RequiredDate', setDate)}
                />
              </View>
            )}
            {setPaymentExpiredDate !== true ? null : (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 100,
                  width: DRIVER.width,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.setState({setPaymentExpiredDate: false});
                  }}>
                  <Text style={{textAlign: 'right'}}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={PaymentExpiredDate ?? new Date()}
                  mode="date"
                  style={{width: DRIVER.width}}
                  onDateChange={setDate =>
                    this.settxt('PaymentExpiredDate', setDate)
                  }
                />
              </View>
            )}
            {ListProduct.length === 0 ? null : (
              <Combobox
                value={''}
                TypeSelect={'single'}
                callback={this.ChangeProduct}
                data={ListProduct}
                nameMenu={'Chọn mã sản phẩm'}
                eOpen={this.openComboboxProduct}
                position={'bottom'}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.searchItems(callback, textsearch)
                }
              />
            )}
            {ListUnit.length === 0 ? null : (
              <Combobox
                value={''}
                TypeSelect={'single'}
                callback={this.ChangeUnit}
                data={ListUnit}
                nameMenu={'Chọn đơn vị tính'}
                eOpen={this.openComboboxUnit}
                position={'bottom'}
              />
            )}
            {ListShippingUnit.length === 0 ? null : (
              <Combobox
                value={''}
                TypeSelect={'single'}
                callback={this.ChangeShippingUnit}
                data={ListShippingUnit}
                nameMenu={'Chọn đơn vị vân chuyển'}
                eOpen={this.openComboboxShippingUnit}
                position={'bottom'}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.searchShippingUnit(callback, textsearch)
                }
              />
            )}
            {ListWarehouses.length === 0 ? null : (
              <Combobox
                value={''}
                TypeSelect={'single'}
                callback={this.ChangeWarehouses}
                data={ListWarehouses}
                nameMenu={'Chọn Kho'}
                eOpen={this.openComboboxWarehouses}
                position={'bottom'}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.searchWarehouses(callback, textsearch)
                }
              />
            )}
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  customViewDetail = () => {
    return (
      <View style={{marginTop: 5}}>
        <View style={{width: 150, marginLeft: 10}}>
          <Button
            title="Thêm dòng"
            type="outline"
            size={15}
            onPress={() => this.addRows()}
            buttonStyle={{padding: 5, marginBottom: 5}}
            titleStyle={{marginLeft: 5}}
            icon={
              <Icon
                name="pluscircleo"
                type="antdesign"
                color={AppColors.blue}
                size={14}
              />
            }
          />
        </View>
        <ScrollView horizontal={true}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              {listCombobox.headerTable.map((item, i) => (
                <TouchableOpacity
                  key={i}
                  style={[AppStyles.table_th, {width: item.width}]}>
                  <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                </TouchableOpacity>
              ))}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <FlatList
                  data={this.state.rows}
                  renderItem={({item, index}) => {
                    return this.itemFlatList(item, index);
                  }}
                  keyExtractor={(rs, index) => index.toString()}
                />
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(0)}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        {/* Mã sản phẩm */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(1)}]}
          onPress={() => this.onActionProduct(index)}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                color:
                  row.ItemId !== undefined ? AppColors.black : AppColors.gray,
              },
            ]}>
            {row.ItemId === '' || row.ItemId === undefined
              ? 'Chọn mã sản phẩm'
              : row.ItemId}
          </Text>
        </TouchableOpacity>
        {/* Tên sản phẩm */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(2)}]}>
          <TextInput
            underlineColorAndroid="transparent"
            value={row.ItemName}
            autoCapitalize="none"
            onChangeText={txt =>
              this.ChangeRows('ItemName', index, txt + '', row)
            }
          />
        </TouchableOpacity>
        {/* Cơ chế */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(3)}]}>
          <TextInput
            underlineColorAndroid="transparent"
            value={row.Specification}
            autoCapitalize="none"
            onChangeText={txt =>
              this.ChangeRows('Specification', index, txt + '', row)
            }
          />
        </TouchableOpacity>
        {/* Đơn vị tính */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(4)}]}
          onPress={() => this.onActionUnit(index)}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                color:
                  row.UnitId !== undefined ? AppColors.black : AppColors.gray,
              },
            ]}>
            {row.UnitId === '' || row.UnitId === undefined
              ? 'Chọn ĐVT'
              : row.UnitName}
          </Text>
        </TouchableOpacity>
        {/* Số lượng tồn TT */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(5)}]}>
          <Text style={[AppStyles.TextInput, {textAlign: 'right'}]}>
            {FuncCommon.addPeriodTextInput(row.InventoryNumber)}
          </Text>
        </TouchableOpacity>
        {/* Số lượng */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(6)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={FuncCommon.addPeriodTextInput(row.Quantity)}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows('Quantity', index, txt + '', row);
            }}
          />
        </TouchableOpacity>
        {/* Đơn giá */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(7)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={FuncCommon.addPeriodTextInput(row.UnitPrice)}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows('UnitPrice', index, txt + '', row);
            }}
          />
        </TouchableOpacity>
        {/* Chi phí vận chuyển */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(8)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={FuncCommon.addPeriodTextInput(row.OtherCosts)}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.ChangeRows(
                'OtherCosts',
                index,
                FuncCommon.CVNumber(txt) + '',
                row,
              );
            }}
          />
        </TouchableOpacity>
        {/* Thành tiền trước thuế*/}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(9)}]}>
          <Text style={[AppStyles.TextInput, {textAlign: 'right'}]}>
            {FuncCommon.addPeriodTextInput(row.AmountNoVat)}
          </Text>
        </TouchableOpacity>
        {/*VAT(%)*/}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(10)}]}>
          <TextInput
            style={{width: '100%', textAlign: 'right'}}
            underlineColorAndroid="transparent"
            value={FuncCommon.addPeriodTextInput(row.Vatrate)}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows('Vatrate', index, txt + '', row);
            }}
          />
        </TouchableOpacity>
        {/*Thành tiền sau VAT(VND)*/}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(11)}]}>
          <Text style={[AppStyles.TextInput, {textAlign: 'right'}]}>
            {FuncCommon.addPeriodTextInput(row.Amount)}
          </Text>
        </TouchableOpacity>
        {/* Kho xuất*/}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(12)}]}
          onPress={() => this.onActionWarehouses(index)}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                color:
                  row.CustomerId !== undefined
                    ? AppColors.black
                    : AppColors.gray,
              },
            ]}>
            {row.CustomerId === '' || row.CustomerId === undefined
              ? 'Chọn kho'
              : row.CustomerName}
          </Text>
        </TouchableOpacity>
        {/* Số lô*/}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(13)}]}>
          <TextInput
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={txt => {
              this.ChangeRows('Note', index, txt + '', row);
            }}
          />
        </TouchableOpacity>
        {/*Đơn vị vận chuyển*/}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.WidthRowTable(14)}]}
          onPress={() => this.onActionShippingUnit(index)}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                color:
                  row.ShippingUnit !== undefined
                    ? AppColors.black
                    : AppColors.gray,
              },
            ]}>
            {row.ShippingUnit === '' || row.ShippingUnit === undefined
              ? 'Chọn ĐVVC'
              : row.ShippingUnitName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td, {width: this.WidthRowTable(15)}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={15}
          />
        </TouchableOpacity>
      </View>
    );
  };

  //#region thông tin chung
  //
  settxt = (key, txt) => {
    this.setState({[key]: txt});
  };
  setNumber = (key, number) => {
    this.setState({[key]: FuncCommon.CVNumber(number)});
  };
  //
  _openComboboxCustomer() {}
  openComboboxCustomer = d => {
    this._openComboboxCustomer = d;
  };
  onActionCustomer() {
    this._openComboboxCustomer();
  }
  ChangeCustomer = rs => {
    if (rs !== null) {
      this.setState({
        ObjectId: rs.value,
        ObjectName: rs.text,
        CustomerGuid: rs.CustomerGuid,
        CustomerName: rs.CustomerName,
        Address: rs.Address,
        ObjectGuid: rs.CustomerGuid,
        ObjectAddress: rs.Address,
        ObjectTaxCode: rs.TaxCode,
        DebtDay: rs.DebtDay,
        ListQuotationNo: [],
      });
      this.GetQuotationsAll();
    }
  };
  searchCustomer = (callback, textsearch) => {
    controller.GetCustomersAll(textsearch, rs => {
      callback(rs);
    });
  };
  //
  _openComboboxQuotationNo() {}
  openComboboxQuotationNo = d => {
    this._openComboboxQuotationNo = d;
  };
  onActionQuotationNo() {
    this._openComboboxQuotationNo();
  }
  ChangeQuotationNo = rs => {
    if (rs.length > 0 && rs[0].value !== null) {
      var list = [];
      var _string = rs.length + 'Lựa chọn';
      for (let i = 0; i < rs.length; i++) {
        list.push(rs[i].value);
      }
      this.settxt('ListAllQuotationName', _string);
      this.settxt('ListQuotationGuid', list);
      this.GetListPOVoucherDetails(list);
      this.GetQuotationsByQuotationGuid(list);
      this.GetCustomerContacts();
    }
  };
  //
  _openComboboxStatusPB() {}
  openComboboxStatusPB = d => {
    this._openComboboxStatusPB = d;
  };
  onActionStatusPB() {
    this._openComboboxStatusPB();
  }
  ChangeStatusPB = rs => {
    if (rs !== null) {
      this.settxt('StatusPB', rs.value);
      this.settxt('StatusPBName', rs.text);
    }
  };
  //#endregion

  //#region thông tin chi tiết
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  WidthRowTable = i => {
    var w = listCombobox.headerTable[i].width;
    return w;
  };
  handleChangeRows = (columnName, row, val, obj) => {
    if (columnName === 'ItemId') {
      this.ChangeRows(columnName, row, val, obj);
      var itemId = FuncCommon.removeLine(val.trim());
      if (this.state.rows.filter(x => x.ItemId === itemId).length > 1) {
        Alert.alert(
          'Thông báo',
          'Dòng thứ ' + (parseInt(row) + 1) + ': Mã sản phẩm đã bị trùng.',
        );
        this.ChangeRows(columnName, row, '', obj);
        return;
      } else {
        controller.GetItemsByItemId(itemId, this.state.ObjectId, rs => {
          if (rs.length > 0) {
            this.ChangeRows('ItemId', row, rs[0].ItemId, obj);
            this.ChangeRows('ItemGuid', row, rs[0].ItemGuid, obj);
            this.ChangeRows(
              'ItemName',
              row,
              rs[0].ItemName !== null ? rs[0].ItemName : '',
              obj,
            );
            this.ChangeRows(
              'UnitGuid',
              row,
              rs[0].UnitGuid !== null ? rs[0].UnitGuid : '',
              obj,
            );
            this.ChangeRows(
              'UnitId',
              row,
              rs[0].UnitId !== null ? rs[0].UnitId : '',
              obj,
            );
            this.ChangeRows(
              'UnitName',
              row,
              rs[0].UnitName !== null ? rs[0].UnitName : '',
              obj,
            );
            this.ChangeRows(
              'Specification',
              row,
              rs[0].Specification !== null ? rs[0].Specification : '',
              obj,
            );
            this.ChangeRows(
              'ItemNameVN',
              row,
              rs[0].ItemNameVN !== null ? rs[0].ItemNameVN : '',
              obj,
            );
          }
        });
        //Kiểm tra tồn
        controller.GetInventoryByItemId(itemId, rs => {
          this.ChangeRows(
            'InventoryNumber',
            row,
            rs.InventoryNumber.toString() ?? '0',
            obj,
          );
          this.ChangeRows(
            'Inventory',
            row,
            rs.QuantityReamin.toString() ?? '0',
            obj,
          );
        });
      }
    }
    if (columnName === 'UnitId') {
      this.ChangeRows(columnName, row, val, obj);
      controller.GetUnitsByUnitId(val, rs => {
        if (rs.length > 0) {
          this.ChangeRows(
            'UnitGuid',
            row,
            rs[0].UnitGuid !== null ? rs[0].UnitGuid : '',
          );
          this.ChangeRows(
            'UnitId',
            row,
            rs[0].UnitId !== null ? rs[0].UnitId : '',
          );
          this.ChangeRows(
            'UnitName',
            row,
            rs[0].UnitName !== null ? rs[0].UnitName : '',
          );
        }
      });
    }
    //Thành tiền bán trước = Đơn giá * SL
    if (columnName === 'UnitPrice' || columnName === 'Quantity') {
      this.ChangeRows(columnName, row, FuncCommon.CVNumber(val), obj);
      var unitPrice = FuncCommon.CVNumber(obj.UnitPrice);
      var quantity = FuncCommon.CVNumber(obj.Quantity);
      var amountNoVat = unitPrice * quantity;
      this.ChangeRows('AmountNoVat', row, amountNoVat, obj);
    }
    //Thành tiền sau bán hàng = Thành tiền bán*VAT
    if (
      columnName === 'AmountSale' ||
      columnName === 'Vatrate' ||
      columnName === 'Quantity' ||
      columnName === 'UnitPrice'
    ) {
      this.ChangeRows(columnName, row, FuncCommon.CVNumber(val), obj);
      var amount =
        (FuncCommon.CVNumber(obj.AmountNoVat) *
          FuncCommon.CVNumber(obj.Vatrate)) /
          100 +
        FuncCommon.CVNumber(obj.AmountNoVat);
      this.ChangeRows('Amount', row, amount, obj);
    }
    if (this.state.RequiredDate != undefined) {
      this.ChangeRows(columnName, row, FuncCommon.CVNumber(val), obj);
      this.ChangeRows(
        'RequireDate',
        row,
        FuncCommon.ConDate(this.state.RequiredDate, 2),
        obj,
      );
    }
    var Amount = 0;
    var ManagementFee = 0;
    this.state.rows.forEach(val => {
      ManagementFee += FuncCommon.CVNumber(val.ManagementFee);
      Amount += FuncCommon.CVNumber(val.Amount);
    });
    this.setState({
      //Tổng tiền chiết khấu
      Discount: 0,
      //Tổng giá net
      MoneyNet: 0,
      //Phí quản lý
      ManagementFee: ManagementFee,
      //Thành tiền
      TotalOfMoney: Amount,
    });
  };
  ChangeRows = (columnName, row, val, obj) => {
    let res = this.state.rows;
    res[row][columnName] = val.toString();
    this.setState({rows: res});
  };
  //
  _openComboboxProduct() {}
  openComboboxProduct = d => {
    this._openComboboxProduct = d;
  };
  onActionProduct(i) {
    this.setState({rowchange: i});
    this._openComboboxProduct();
  }
  ChangeProduct = rs => {
    if (rs !== null) {
      this.handleChangeRows(
        'ItemId',
        this.state.rowchange,
        rs.value,
        this.state.rows[this.state.rowchange],
      );
      this.handleChangeRows(
        'ItemName',
        this.state.rowchange,
        rs.text,
        this.state.rows[this.state.rowchange],
      );
      this.handleChangeRows(
        'Vatrate',
        this.state.rowchange,
        '10',
        this.state.rows[this.state.rowchange],
      );
    }
  };
  searchItems = (callback, textsearch) => {
    controller.GetItemsAllJExcel(textsearch, rs => {
      callback(rs);
    });
  };
  //
  _openComboboxUnit() {}
  openComboboxUnit = d => {
    this._openComboboxUnit = d;
  };
  onActionUnit(i) {
    this.setState({rowchange: i});
    this._openComboboxUnit();
  }
  ChangeUnit = rs => {
    if (rs !== null) {
      this.handleChangeRows(
        'UnitId',
        this.state.rowchange,
        rs.value,
        this.state.rows[this.state.rowchange],
      );
      this.handleChangeRows(
        'UnitName',
        this.state.rowchange,
        rs.text,
        this.state.rows[this.state.rowchange],
      );
    }
  };
  //
  _openComboboxShippingUnit() {}
  openComboboxShippingUnit = d => {
    this._openComboboxShippingUnit = d;
  };
  onActionShippingUnit(i) {
    this.setState({rowchange: i});
    this._openComboboxShippingUnit();
  }
  ChangeShippingUnit = rs => {
    if (rs !== null) {
      this.ChangeRows(
        'ShippingUnit',
        this.state.rowchange,
        rs.value,
        this.state.rows[this.state.rowchange],
      );
      this.ChangeRows(
        'ShippingUnitName',
        this.state.rowchange,
        rs.text,
        this.state.rows[this.state.rowchange],
      );
    }
  };
  searchShippingUnit = (callback, textsearch) => {
    controller.GetObjectProvidersAll(textsearch, rs => {
      callback(rs);
    });
  };
  //
  _openComboboxWarehouses() {}
  openComboboxWarehouses = d => {
    this._openComboboxWarehouses = d;
  };
  onActionWarehouses(i) {
    this.setState({rowchange: i});
    this._openComboboxWarehouses();
  }
  ChangeWarehouses = rs => {
    if (rs !== null) {
      this.ChangeRows(
        'CustomerId',
        this.state.rowchange,
        rs.value,
        this.state.rows[this.state.rowchange],
      );
      this.ChangeRows(
        'CustomerName',
        this.state.rowchange,
        rs.text,
        this.state.rows[this.state.rowchange],
      );
    }
  };
  searchWarehouses = (callback, textsearch) => {
    controller.GetWarehousesAllJExcel(textsearch, rs => {
      callback(rs);
    });
  };
  //#endregion
  //#region điều khoản báo giá
  //
  _openComboboxTerms() {}
  openComboboxTerms = d => {
    this._openComboboxTerms = d;
  };
  onActionTerms(i) {
    this.setState({rowchange: i});
    this._openComboboxTerms();
  }
  ChangeTerms = rs => {
    if (rs !== null) {
      this.settxt('TermId', rs.value);
      this.settxt('TermName', rs.text);
      this.settxt('TermContent', rs.TermContent);
    }
  };
  //#endregion

  submit = () => {
    controller.submit(this.state, this.state.rows, rs => {
      this.BackList();
    });
  };
  BackList() {
    Actions.pop();
    Actions.refresh({moduleId: 'back', ActionTime: new Date().getTime()});
  }

  //#region openAttachment
  openAttachment() {}
  openCombobox_Att = d => {
    this.openAttachment = d;
  };
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  removeAttactment(para) {
    controllerQ.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  //#region open img
  openPhotoLibrary() {}
  openLibrary = d => {
    this.openPhotoLibrary = d;
  };
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion

  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
        } else if (response.error) {
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion

  //#endregion

  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      {toValue: 0, duration: 333},
    );
  };
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ViewAll: !this.state.ViewAll});
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ViewDetail: !this.state.ViewDetail});
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ViewDetail: true, ViewAll: true});
    }
  };
  //#endregion
}
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Orders_Form_Component);
