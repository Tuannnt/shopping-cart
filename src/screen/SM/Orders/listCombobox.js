import { AppStyles, AppColors } from '@theme';
export default {
    ListApprovalStatus: [
        {
            value: 'W',
            text: "Chờ duyệt"
        },
        {
            value: 'Y',
            text: "Đã duyệt"
        },
        {
            value: 'N',
            text: "Không duyệt"
        }
    ],
    ListOrderStatus: [
        {
            value: '0',
            text: "Chưa thực hiện"
        }, {
            value: '1',
            text: "Đang thực hiện"
        }, {
            value: '2',
            text: "Hoàn thành"
        }, {
            value: '3',
            text: "Đã hủy bỏ"
        }
    ],
    StatusPB: [
        {
            value: 'S',
            text: "Theo số lượng"
        },
        {
            value: 'G',
            text: "Tự nhập giá trị"
        }
    ],
    headerTable: [
        { title: 'STT', width: 40 },
        { title: 'Mã sản phẩm', width: 150 },
        { title: 'Tên sản phẩm', width: 250 },
        { title: 'Cơ chế', width: 100 },
        { title: 'ĐVT', width: 100 },
        { title: 'S.L Tồn TT', width: 120 },
        { title: 'Số lượng', width: 100 },
        { title: 'Đơn giá', width: 100 },
        { title: 'Chi phí vận chuyển', width: 150 },
        { title: 'Thành tiền trước thuế', width: 150 },
        { title: 'VAT(%)', width: 100 },
        { title: 'Thành tiền sau VAT(VND)', width: 200 },
        { title: 'Kho xuất', width: 100 },
        { title: 'Số lô', width: 150 },
        { title: 'Đợn vị vận chuyển', width: 150 },
        { title: 'Hành động', width: 80, hideInDetail: true },
    ],
}