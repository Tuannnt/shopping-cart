import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { API_ApplyOverTimes } from '@network';
import API_Orders from '../../../network/SM/API_Orders';
import { Alert } from 'react-native';
import { FuncCommon } from '@utils';
export default {
  CustomStatus(val, approvalStatus, s) {
    if (s === 0) {
      if (val === 0) {
        return 'Chưa thực hiện';
      } else if (val === 1) {
        return 'Đang thực hiện';
      } else if (val === 2) {
        return 'Hoàn thành';
      } else if (val === 3) {
        return 'Đã hủy bỏ';
      } else if (val === 4) {
        return 'Tất cả';
      } else {
        return '';
      }
    } else {
      if (approvalStatus !== 'W') {
        return AppColors.black;
      } else {
        if (val === 0) {
          return AppColors.PendingColor;
        } else if (val === 1) {
          return AppColors.StatusTask_DTH;
        } else if (val === 2) {
          return AppColors.StatusTask_HT;
        } else if (val === 3) {
          return AppColors.StatusTask_HL;
        } else if (val === 4) {
          return AppColors.Maincolor;
        } else {
          return '';
        }
      }
    }
  },
  isApproval(data, callback) {
    API_Orders.getIsApproval({ OrderGuid: data })
      .then(rs => {
        callback(rs.data.errorCode);
      })
      .catch(error => {
        alert(error);
      });
  },
  Approval(data, callback) {
    API_Orders.Approval(data)
      .then(rs => {
        callback(rs.data);
      })
      .catch(error => {
        alert(error);
      });
  },
  UpdateStatus(id, callback) {
    API_Orders.UpdateStatus(id)
      .then(rs => {
        callback(rs.data);
      })
      .catch(error => {
        alert(error);
      });
  },
  getNumberAuto(callback) {
    let val = { AliasId: 'QTDSCT_DDH', Prefixs: null, VoucherType: '' };
    API_ApplyOverTimes.GetNumberAuto(val)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var data = { Value: _data.Value, IsEdits: _data.IsEdit };
        callback(data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetCustomersAll(txt, callback) {
    var obj = { Search: txt };
    API_Orders.GetCustomersAll(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        for (let i = 0; i < _data.length; i++) {
          list.push({
            value: _data[i].CustomerId,
            text: '[' + _data[i].CustomerId + ']' + ' ' + _data[i].CustomerName,
            Address: _data[i].Address,
            CustomerGuid: _data[i].CustomerGuid,
            DebtDay: _data[i].DebtDay,
            TaxCode: _data[i].TaxCode,
          });
        }
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetQuotationsAll(data, callback) {
    var obj = { guid: data };
    API_Orders.GetQuotationsAll(obj)
      .then(rs => {
        var list = [];
        var _data = JSON.parse(rs.data.data);
        for (let i = 0; i < _data.length; i++) {
          list.push({
            value: _data[i].QuotationGuid,
            text: _data[i].QuotationNoName,
            QuotationNo: _data[i].QuotationNo,
          });
        }
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetListPOVoucherDetails(data, callback) {
    var para = { list: data };
    API_Orders.GetListPOVoucherDetails(para)
      .then(rs => {
        var list = [];
        var _data = JSON.parse(rs.data.data);
        if (_data.length > 0) {
          _data.forEach((val, key) => {
            key++;
            list.push([
              '',
              val.QuotationGuid, //Gui báo giá
              val.QuotationNo, //Số báo giá
              val.ItemIdbyProvider, //Mã SP KH
              val.ItemGuid, //Gui sản phẩm,
              val.ItemId, // Mã sản phẩm
              val.ItemName, // Tên sản phẩm,
              val.ItemNameVN, // Tên tiếng việt
              val.Specification, // Cơ chế
              val.UnitGuid, // Gui đơn vị
              val.UnitId, // Mã đơn vị
              val.UnitName, //Tên đơn vị
              val.InventoryNumber, //Số lượng thực tế
              val.Inventory, //Số lượng tồn đáp ứng
              val.Quantity, //Số lượng
              val.ProductionPrice, //Chi phí sản xuất
              val.AmountOc, // Tổng chi phí sản xuất
              val.UnitPriceNet, // Đơn giá cost
              val.UnitPrice, //Đơn giá bán
              val.ManagementFeeRate, //  % quản lý phí
              val.ManagementFee, //  Quản lý phí
              val.ProfitPercent, //%Lợi nhuận mong muốn
              val.Profit, //Lợi nhuận mong muốn
              val.UnitPricePO, // Đơn giá
              val.FeedBackRate, //% CSKH
              val.FeedBack, //COM
              val.AmountCOST, //Thành tiền COST
              val.AmountCSKH, //Thành tiền CSKH
              val.AmountCPQL, //Thành tiền CPQL
              val.AmountSale, //Thành tiền BAN
              val.ProfitActual, //Lợi nhuận thực tế
              val.ProfitActualPercent, //%Lợi nhuận thực tế
              val.TotalUnitPriceNet, // Tổng giá net
              val.DiscountPO, // Tổng chiết khấu
              val.OtherCosts, // Chi phí vận chuyển
              val.AmountNoVat, // Thành tiền bán trước thuế
              val.DiscountRate, //% chiết khấu
              val.Discount, //Tiền chiết khấu
              val.Vatrate, // % VAT
              val.Amount, // Thành tiền
              val.RequiredDate != null
                ? moment(val.RequiredDate).format('YYYY-MM-DD')
                : '', // Ngày yêu cầu
              val.ConfirmDate != null
                ? moment(val.ConfirmDate).format('YYYY-MM-DD')
                : '', // Ngày xác nhận
              val.DeliveryDate != null
                ? moment(val.DeliveryDate).format('YYYY-MM-DD')
                : '', // Ngày thực tế
              '0', //Trạng thái
              '', //Nhà máy
              val.Note, //Số lô,
              val.ShippingUnit,
              val.QuotationDetailGuid, //Guid chi tiết báo giá
              key.toString(), //Key
            ]);
          });
        }
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },

  GetQuotationsByQuotationGuid(data, callback) {
    var obj = { guid: data };
    API_Orders.GetQuotationsByQuotationGuid(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetCustomerContacts(data, callback) {
    var obj = { guid: data };
    API_Orders.GetCustomerContacts(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetItemsAllJExcel(data, callback) {
    var obj = { Search: data };
    API_Orders.GetItemsAllJExcel(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        _data.forEach(val => {
          list.push({
            value: val.ItemID,
            text: val.ItemName,
          });
        });
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetItemsByItemId(itemId, ObjectId, callback) {
    var obj = { ItemId: itemId, ObjectId: ObjectId };
    API_Orders.GetItemsByItemId(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetInventoryByItemId(itemId, callback) {
    var obj = { ItemId: itemId };
    API_Orders.GetInventoryByItemId(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetUnitsByUnitId(txt, callback) {
    var obj = { Search: txt };
    API_Orders.GetUnitsByUnitId(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetUnitByItemIdJExcel(txt, itemId, callback) {
    var obj = { UnitId: txt, ItemId: itemId };
    API_Orders.GetUnitByItemIdJExcel(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        _data.forEach(val => {
          list.push({
            value: val.id,
            text: val.name,
          });
        });
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetObjectProvidersAll(txt, callback) {
    var obj = { Search: txt };
    API_Orders.GetObjectProvidersAll(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        _data.forEach(val => {
          list.push({
            value: val.id,
            text: val.name,
          });
        });
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  GetWarehousesAllJExcel(txt, callback) {
    var obj = { Search: txt };
    API_Orders.GetWarehousesAllJExcel(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        _data.forEach(val => {
          list.push({
            value: val.id,
            text: val.name,
          });
        });
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  //Chức nằng thêm mới đơn hàng
  submit(model, detail, callback) {
    if (
      model.NumberAuto.Value.toUpperCase() === model.OrderNumber.toUpperCase()
    ) {
      model.NumberAuto.IsEdit = false;
    } else {
      model.NumberAuto.IsEdit = true;
    }
    if (model.OrderDate !== undefined && model.RequiredDate !== undefined) {
      if (
        FuncCommon.ConDate(model.OrderDate, 5) >
        FuncCommon.ConDate(model.RequiredDate, 5)
      ) {
        Alert.alert(
          'Thông báo',
          'Ngày yêu cầu giao hàng phải lớn hơn hoặc bằng ngày đặt hàng.',
        );
        return;
      }
    } else if (model.OrderDate == undefined || model.OrderDate == '') {
      Alert.alert('Thông báo', 'Yêu cầu chọn ngày đặt.');
      return;
    } else if (/^[a-zA-Z0-9-_ ]+$/.test(model.SaleOrderNo) == false) {
      Alert.alert('Thông báo', 'Số đơn hàng KD không hợp lệ.');
      return;
    }
    for (var i = 0; i < detail.length; i++) {
      if (detail[i].ItemId !== '' && detail[i].ItemId !== null) {
        if (detail[i].UnitId === '') {
          Alert.alert(
            'Thông báo',
            'Dòng thứ ' + (i + 1) + ': Yêu cầu chọn đơn vị tính.',
          );
          return;
        } else if (detail[i].ItemId === '') {
          Alert.alert(
            'Thông báo',
            'Dòng thứ ' + (i + 1) + ':Yêu cầu thêm mới sản phẩm.',
          );
          return;
        } else if (detail[i].UnitPrice === '0' || detail[i].UnitPrice === '') {
          Alert.alert(
            'Thông báo',
            'Dòng thứ ' + (i + 1) + ':Yêu cầu nhập giá bán.',
          );
          return;
        } else if (parseFloat(detail[i].Quantity) < 0) {
          Alert.alert(
            'Thông báo',
            'Dòng thứ ' + (i + 1) + ':Yêu cầu số lượng >0.',
          );
          return;
        } else if (parseFloat(detail[i].UnitPrice) < 0) {
          Alert.alert(
            'Thông báo',
            'Dòng thứ ' + (i + 1) + ': Yêu cầu giá bán >0.',
          );
          return;
        }
      }
    }
    if (
      detail.filter(x => x.ItemIdbyProvider !== '' || x.ItemId !== '')
        .length === 0
    ) {
      Alert.alert('Thông báo', 'Yêu cầu nhập thông tin chi tiết.');
      return;
    }
    var ListOrderDetails = [];
    var OtherCosts = 0;
    var modelSubmit = {
      OrderNumber: model.OrderNumber,
      ObjectId: model.ObjectId,
      ObjectName: model.ObjectName,
      ObjectAddress: model.ObjectAddress,
      ShipAddress: model.ShipAddress,
      OrderDate: model.OrderDate,
      RequiredDate: model.RequiredDate,
      DebtDay: model.DebtDay,
      DeliveryType: model.DeliveryType,
      PaymentExpiredDate: model.PaymentExpiredDate,
      StatusPB: model.StatusPB,
      AmountCost: model.AmountCost,
      Note: model.Note,
      Description: model.Description,
      IsInvoice: model.IsInvoice,
      StatusQuotation: model.StatusQuotation,
      OrderStatus: model.OrderStatus,
      CurrencyId: model.CurrencyId,
      Type: model.Type,
      ApprovalStatus: model.ApprovalStatus,
      CustomerGuid: model.CustomerGuid,
    };
    for (var i = 0; i < detail.length; i++) {
      var val = detail[i];
      val.ItemId = val.ItemId;
      val.ItemIdbyProvider = val.ItemIdbyProvider;
      if (ListOrderDetails.filter(x => x.ItemId === val.ItemId).length >= 1) {
        Alert.alert(
          'Thông báo',
          'Dòng thứ ' + (parseInt(i) + 1) + ': Mã SP nội bộ đã bị trùng.',
        );
        return;
      } else if (
        (val.ItemIdbyProvider !== '' && val.ItemIdbyProvider !== null) ||
        (val.ItemId !== '' && val.ItemId !== null)
      ) {
        OtherCosts += parseFloat(val.OtherCosts);
        ListOrderDetails.push({
          QuotationGuid: val.QuotationGuid === '' ? null : val.QuotationGuid, //Gui báo giá
          QuotationNo: val.QuotationNo, //Số báo giá
          ItemIdbyProvider: val.ItemIdbyProvider, //Mã SP KH
          ItemGuid: val.ItemGuid !== '' ? val.ItemGuid : null, //Gui sản phẩm,
          ItemId: val.ItemId !== '' ? val.ItemId : null, // Mã sản phẩm
          ItemName: val.ItemName, // Tên sản phẩm,
          ItemNameVN: val.ItemNameVN, // Tên tiếng việt,
          Specification: val.Specification, // Cơ chế
          UnitGuid: val.UnitGuid !== '' ? val.UnitGuid : null, // Gui đơn vị
          UnitId: val.UnitId, // Mã đơn vị
          UnitName: val.UnitName, //Tên đơn vị
          Quantity: val.Quantity, //Số lượng
          ProductionPrice: val.ProductionPrice, //Chi phí sản xuất
          AmountOc: val.AmountOc, // Tổng chi phí sản xuất
          UnitPriceNet: val.UnitPriceNet, // Đơn giá net
          ManagementFeeRate: val.ManagementFeeRate, //  % quản lý phí
          ManagementFee: val.ManagementFee, //  Quản lý phí
          ProfitPercent: val.ProfitPercent, //% LN mong muốn
          Profit: val.Profit, //LN mong muốn
          UnitPrice: val.UnitPrice, // Đơn giá bán
          FeedBackRate: val.FeedBackRate, //% CSKH
          FeedBack: val.FeedBack, //COM
          ProfitActual: val.ProfitActual, //LN thực tế
          ProfitActualPercent: val.ProfitActualPercent, //% LN thực tế
          TotalUnitPriceNet: val.TotalUnitPriceNet, // Tổng giá net
          DiscountPO: val.DiscountPO, // Tổng chiết khấu
          OtherCosts: val.OtherCosts, // Chi phí vận chuyển
          PricePO: val.PricePO, // Tổng giá PO
          AmountNoVat: val.AmountNoVat, //Thành tiền bán trước thuế
          DiscountRate: val.DiscountRate, //% chiết khấu
          Discount: val.Discount, // Tiền chiết khấu
          Vatrate: val.Vatrate, // % VAT
          Amount: val.Amount, // Thành tiền
          RequireDate: model.RequiredDate, // Ngày yêu cầu
          ConfirmDate: val.ConfirmDate, // Ngày xác nhận
          DeliveryDate: val.DeliveryDate, // Ngày thực tế
          IsCancel: val.IsCancel === '1' ? true : false, //Trạng thái
          AmountNoVat: val.AmountNoVat,
          Specification: val.Specification, //Cơ chế
          CustomerId: val.CustomerId, //Nhà máy
          Note: val.Note, //Số lô
          ShippingUnit: val.ShippingUnit, //Đơn vị vận chuyển
          QuotationDetailGuid: val.QuotationDetailGuid, //Guid chi tiết báo giá
          GroupItemId: model.GroupItemId, //Loại sản phẩm
          OrderTypeId: model.OrderTypeId, //Hình thức
          SortOrder: val.SortOrder, //SortOrder
          Type: model.Type, //Loại đơn hàng
          SaleOrderNo: model.SaleOrderNo, //Số đơn hàng KD
          ObjectId: model.ObjectId, //Mã khách hàng
          //IsConfirm: val.IsConfirm,
          //ConfirmDate: val.ConfirmDate,
          //StatusYCSX: val.StatusYCSX
        });
      }
    }
    if (OtherCosts > parseFloat(model.AmountCost)) {
      Alert.alert(
        'Thông báo',
        'Chi phí vận chuyển không được lớn hơn tổng chi phí vận chuyển.',
      );
      return;
    }
    // var datatab2 = $scope.jexcelmodel.JexcelTab2.jexcel('getData');
    // $scope.ListOrderPayments = [];
    // angular.forEach(datatab2, function (val, key) {
    //     if (val.PlanDate !== "") {
    //         $scope.ListOrderPayments.push({
    //             PlanDate: val.PlanDate,//Ngày dự kiến
    //             PaymentType: val.PaymentType,//Loại thanh toán
    //             Status: val.EnterPaymentType === "1" ? true : false, //Loại nhập thanh toán
    //             IsInvoice: val.IsInvoice,//Có xuất đơn hàng,
    //             Rate: val.Rate,//%
    //             PlanTotal: val.TotalReally,// Tổng tiền
    //             Discount: val.Discount,// Chiết khấu
    //             Note: val.Note,// Số lô
    //             IsActives: true,//Trạng thái
    //         });
    //     }
    // })
    // var datatab3 = $scope.jexcelmodel.JexcelTab3.jexcel('getData');
    // $scope.ListProfitShare = [];
    // angular.forEach(datatab3, function (val, key) {
    //     if (val.LoginName !== "") {
    //         $scope.ListProfitShare.push({
    //             RowGuid: (val.RowGuid == "" ? null : val.RowGuid),
    //             LoginName: val.LoginName,
    //             Rate: val.Rate,
    //             Amount: val.Amount,
    //             Description: val.Description
    //         });
    //     }
    // })
    // var datatab4 = $scope.jexcelmodel.JexcelTab4.jexcel('getData');
    // $scope.ListOrderCom = [];
    // angular.forEach(datatab4, function (val, key) {
    //     if (val.EmployeeName !== "") {
    //         $scope.ListOrderCom.push({
    //             OrderComGuid: (val.OrderComGuid == "" ? null : val.OrderComGuid),
    //             EmployeeName: val.EmployeeName,
    //             ItemGuid: val.ItemGuid,
    //             ItemId: val.ItemId,
    //             ItemName: val.ItemName,
    //             UnitId: val.UnitId,
    //             UnitName: val.UnitName,
    //             Quantity: val.Quantity,
    //             UnitPrice: val.UnitPrice,
    //             Amount: val.Amount,
    //             FeedBack: val.FeedBack,
    //             UnitPriceNet: val.UnitPriceNet,
    //             RateRetained: val.RateRetained,
    //             AmountRetained: val.AmountRetained,
    //             TotalReally: val.TotalReally,
    //             IsActives: true
    //         });
    //     }
    // })
    var ListQuotation = [];
    for (let i = 0; i < model.ListQuotationGuid.length; i++) {
      var checkQuotation = model.ListQuotationNo.find(
        x => x.value === model.ListQuotationGuid[i],
      );
      if (checkQuotation !== undefined) {
        ListQuotation.push({
          QuotationGuid: checkQuotation.value,
          QuotationNo: checkQuotation.QuotationNo,
        });
      }
    }
    var fd = new FormData();
    if (model.OrderStatus != '' && model.OrderStatus != undefined) {
      model.OrderStatus = parseInt(model.OrderStatus);
    }
    fd.append('insert', JSON.stringify(modelSubmit));
    fd.append('OrderDetails', JSON.stringify(ListOrderDetails));
    fd.append('OrderPayments', JSON.stringify([]));
    fd.append('OrderAndQuotations', JSON.stringify(ListQuotation));
    fd.append('OrderDeliveryDates', JSON.stringify([]));
    fd.append('ProfitShare', JSON.stringify([]));
    fd.append('OrderCom', JSON.stringify([]));
    fd.append('auto', JSON.stringify(model.NumberAuto));
    for (var i = 0; i < model.Attachment.length; i++) {
      _data.append(model.Attachment[i].FileName, {
        name: model.Attachment[i].FileName,
        size: model.Attachment[i].size,
        type: model.Attachment[i].type,
        uri: model.Attachment[i].uri,
      });
    }
    API_Orders.Insert(fd)
      .then(rs => {
        Alert.alert('Thông báo', rs.data.message);
        if (rs.data.errorCode === 200) {
          callback(rs.data);
        }
      })
      .catch(error => {
        console.log('Đã xảy ra lỗi khi thêm mới.');
        console.log(error);
      });
  },
  IsDeleted(id, callback) {
    var obj = { guid: id };
    API_Orders.IsDeleted(obj).then(rs => {
      callback(rs);
    }).catch(error => {
      console.log(error)
    });
  }
};
