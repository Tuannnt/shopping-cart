import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Dimensions,
  TouchableOpacity,
  Image,
  View,
  TextInput,
  Text,
  StyleSheet,
  StatusBar,
  FlatList,
  ScrollView,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {
  Header,
  Divider,
  Button,
  Input,
  ListItem,
  SearchBar,
  Icon,
} from 'react-native-elements';
//import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_Orders from '../../../network/SM/API_Orders';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import Combobox from '../../component/Combobox';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '../../../utils';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import controller from './controller';

const SCREEN_WIDTH = Dimensions.get('window').width;
class ListOrdersComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      model: {},
      List: [],
      isFetching: false,
    };
    this.staticParam = {
      EndDate: new Date(),
      StartDate: new Date(),
      OrderStatus: ['0'],
      Status: '',
      CurrentPage: 0,
      Length: 15,
      Search: '',
      QueryOrderBy: 'OrderDate DESC',
      CustomerId: null,
      Type: null,
      OrderTypeID: null,
    };
    this.listtabbarBotom = [
      {
        Title: 'Chưa thực hiện',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Badge: 0,
        Checkbox: true,
      },
      {
        Title: 'Đang thực hiện',
        Icon: 'filetext1',
        Type: 'antdesign',
        Value: 'filetext1',
        Badge: 0,
        Checkbox: false,
      },
      {
        Title: 'Hoàn thành',
        Icon: 'checksquareo',
        Type: 'antdesign',
        Value: 'checksquareo',
        Badge: 0,
        Checkbox: false,
      },
      {
        Title: 'Hủy bỏ',
        Icon: 'minuscircleo',
        Type: 'antdesign',
        Value: 'minuscircleo',
        Badge: 0,
        Checkbox: false,
      },
    ];
    this.onEndReachedCalledDuringMomentum = true;
    this.Total = 0;
  }
  componentDidMount = () => {
    this.nextPage();
  };
  componentWillReceiveProps = nextProps => {
    this.updateSearch('');
  };
  //Scroll to top
  upButtonHandler = () => {
    //OnCLick of Up button we scrolled the list to top
    this.ListView_Ref.scrollToOffset({offset: 0, animated: true});
  };
  //Scroll to end
  downButtonHandler = () => {
    //OnCLick of down button we scrolled the list to bottom
    this.ListView_Ref.scrollToEnd({animated: true});
  };
  //Scroll to Index
  goIndex = () => {
    this.ListView_Ref.scrollToIndex({animated: true, index: 5});
  };
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };

  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  //Message
  ListEmpty = () => {
    if (this.state.List.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Danh sách đơn hàng'}
          FormSearch={true}
          CallbackFormSearch={callback =>
            this.setState({EvenFromSearch: callback})
          }
          BackModuleByCode={'SM'}
          // addForm={'orders_Form'}
          // CallbackFormAdd={() => Actions.orders_Form()}
        />
        <Divider />
        <View>
          {this.state.EvenFromSearch == true ? (
            <View style={{flexDirection: 'column'}}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    padding: 5,
                  }}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian bắt đầu
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventStartDate: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian kết thúc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventEndDate: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.staticParam.EndDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column', padding: 5}}>
                  <Text
                    style={[
                      AppStyles.Labeldefault,
                      {textAlign: 'center'},
                      styles.timeHeader,
                    ]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <SearchBar
                placeholder="Tìm kiếm..."
                lightTheme
                round
                inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                containerStyle={AppStyles.FormSearchBar}
                onChangeText={text => this.updateSearch(text)}
                value={this.staticParam.Search}
              />
            </View>
          ) : null}
        </View>
        <View style={{flex: 1}}>
          <FlatList
            style={{backgroundColor: '#fff'}}
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListEmptyComponent={this.ListEmpty}
            ListHeaderComponent={this.renderHeader}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.List}
            renderItem={({item, index}) => (
              <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
                <ListItem
                  title={
                    <View style={{flexDirection: 'row', flex: 1}}>
                      <Text style={[AppStyles.Labeldefault, {fontSize: 16}]}>
                        {item.OrderNumber}
                      </Text>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {
                              color: controller.CustomStatus(
                                item.OrderStatus,
                                item.ApprovalStatus,
                                1,
                              ),
                            },
                          ]}>
                          {controller.CustomStatus(
                            item.OrderStatus,
                            item.ApprovalStatus,
                            0,
                          )}
                        </Text>
                      </View>
                    </View>
                  }
                  subtitle={() => {
                    return (
                      <View>
                        <View style={{flex: 1}}>
                          <Text style={AppStyles.Textdefault}>
                            {FuncCommon.handleLongText(item.CustomerName, 45)}
                          </Text>
                        </View>
                        <View style={{flexDirection: 'row', flex: 1}}>
                          <View style={{flexDirection: 'row', flex: 1}}>
                            <Text style={AppStyles.Textdefault}>
                              Ngày đặt:{' '}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              {item.OrderDate}
                            </Text>
                          </View>
                        </View>
                        <View style={{flexDirection: 'row', flex: 1}}>
                          <View style={{flexDirection: 'row', flex: 1}}>
                            <Text style={AppStyles.Textdefault}>Ngày YC: </Text>
                            <Text style={AppStyles.Textdefault}>
                              {item.RequiredDate}
                            </Text>
                          </View>
                          <View style={{flex: 1, alignItems: 'flex-end'}}>
                            <Text style={[AppStyles.Labeldefault]}>
                              {this.addPeriod(item.TotalOfMoney)}
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  }}
                  bottomDivider
                  titleStyle={AppStyles.Titledefault}
                  onPress={() => this.onView(item)}
                />
              </View>
            )}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              this.state.List.length >=
              this.staticParam.CurrentPage * this.staticParam.Length
                ? this.nextPage()
                : null;
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
        </View>
        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onClick(callback)}
          />
        </View>
        {this.state.setEventStartDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearch(''),
                  this.setState({setEventStartDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.staticParam.StartDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setStartDate(setDate)}
            />
          </View>
        ) : null}
        {this.state.setEventEndDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearch(''), this.setState({setEventEndDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.staticParam.EndDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setEndDate(setDate)}
            />
          </View>
        ) : null}
        <MenuSearchDate
          value={this.state.ValueSearchDate}
          callback={this.CallbackSearchDate}
          eOpen={this.openMenuSearchDate}
        />
      </View>
    );
  }
  //Lấy dữ liệu
  GetListAll = () => {
    this.setState({refreshing: true});
    API_Orders.GetOrderAll(this.staticParam)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        this.state.List = this.state.List.concat(_data.data);
        this.Total = _data.recordsTotal;
        this.listtabbarBotom[0].Badge = _data.OrderStatus_0;
        this.listtabbarBotom[1].Badge = _data.OrderStatus_1;
        this.listtabbarBotom[2].Badge = _data.OrderStatus_2;
        this.listtabbarBotom[3].Badge = _data.OrderStatus_3;
        this.setState({List: this.state.List, refreshing: false});
      })
      .catch(error => {
        this.setState({refreshing: false});
      });
  };
  onClick(data) {
    if (data == 'profile') {
      this.staticParam.OrderStatus = ['0'];
      this.updateSearch('');
    }
    if (data == 'filetext1') {
      this.staticParam.OrderStatus = ['1'];
      this.updateSearch('');
    }
    if (data == 'checksquareo') {
      this.staticParam.OrderStatus = ['2'];
      this.updateSearch('');
    }
    if (data == 'minuscircleo') {
      this.staticParam.OrderStatus = ['3'];
      this.updateSearch('');
    }
  }
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  updateSearch = text => {
    this.staticParam.Search = text;
    this.staticParam.CurrentPage = 1;
    this.setState({
      List: [],
    });
    this.GetListAll();
  };
  //Sửa
  onView(item) {
    Actions.viewOrder({OrderGuid: item.OrderGuid});
  }
  //Sửa
  onEdit(item) {
    Actions.listOrderDetails(item);
  }
  //Xóa
  onDelete(item) {
    API_Orders.delete(item.OrderGuid)
      .then(res => {
        Actions.pop();
      })
      .catch(error => {});
  }
  //Load lại
  onRefresh = () => {
    this.setState({
      List: [],
    });
    this.updateSearch('');
  };
  //Tìm kiếm
  search = text => {
    this.setState({
      List: [],
      CurrentPage: 1,
      Search: text,
    });
    this.GetListAll();
  };
  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetListAll();
  }
  onPressHome() {
    Actions.home();
  }
}
const styles = StyleSheet.create({
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListOrdersComponent);
