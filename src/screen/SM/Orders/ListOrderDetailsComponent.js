import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header, Icon, Divider } from 'react-native-elements';
import {
  Text,
  View,
  StyleSheet,
  Animated,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import API_Orders from '../../../network/SM/API_Orders';
import Timeline from 'react-native-timeline-flatlist';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import controller from './controller';
import { FuncCommon } from '../../../utils';
import moment from 'moment';
const styles = StyleSheet.create({
  subtitleStyle: {
    fontSize: 13,
    fontFamily: 'Arial',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  textRight: {
    textAlign: 'right',
  },
  hidden: {
    display: 'none',
  },
});

class ListOrderDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: props.DataView,
      List: [],
      dataTimeline: [],
      //hiển thị thông tin chung
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
    };
  }
  componentDidMount = () => {
    // this.GetOrderDetailsById();
    this.GetOrderDeliveryDates();
    this.Skill();
    this.setViewOpen('ViewTotal');
  };
  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết đơn hàng'}
          callBack={() => this.onPressBack()}
        />
        <View style={{ flex: 1 }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewAll')}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
            </View>
            <Animated.View
              style={{
                transform: [{ rotate: rotateStart_ViewAll }, { perspective: 4000 }],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewAll !== true ? null : (
            <ScrollView style={[{ padding: 10 }]}>
              {/* <View
                style={{flexDirection: 'row', marginTop: 5, marginBottom: 5}}>
                <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                  {this.state.model.ItemName}
                </Text>
              </View> */}
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Mã SPKH:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.ItemIdByProvider}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Tên SPKH:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.ItemNameByProvider}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Mã SPNB:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.ItemId}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Tên SPNB:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.ItemName}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Quy cách:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.Specification}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Tên máy:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.MachineName}
                </Text>
              </View>
              {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Mã bản vẽ:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.NoDrawing}
                </Text>
              </View> */}
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Xuất xứ:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.Origin}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Đơn vị tính:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.UnitName}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Số lượng:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.Quantity)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Giá Cost:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.UnitPriceCost)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  % Độ phức tạp và rủi ro:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.RiskRate)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Chi phí độ phức tạp và rủi ro:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.Risk)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  %CP quản lý:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.CostsRate)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Chi phí quản lý:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.ManagementCosts)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  %Chi phí lập quy trình công nghệ:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.ProcedureRate)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Chi phí lập quy trình công nghệ:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.Procedures)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>% LNMM:</Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.ProfitPercent)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>LNMM:</Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.Profit)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Chi phí vận chuyển:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.Cost)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Chi phí kiểm tra đóng gói:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.Pack)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Chi phí khác:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.OtherCosts)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>%NVKD:</Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.SaleRate)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  CP NVKD:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.Sale)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Đơn giá dự kiến:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.UnitPriceExpect)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Đơn giá NET:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.UnitPriceNet)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Giá bán:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.UnitPrice)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Thành tiền trước VAT:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.AmountNoVat)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Thành tiền quy đổi trước VAT:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.AmountNoVatOc)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>VAT(%):</Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.Vatrate}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>VAT:</Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.VatamountOc)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  % Chiết khấu:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.DiscountRate)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Số tiền chiết khấu:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.Discount)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Thành tiền sau VAT:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.AmountVat)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Thành tiền QĐ sau VAT:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.AmountVatOc)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Giá chênh:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.PriceDifference)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  % Giữ lại:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.KeepPercent)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Thành tiền trả khách:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {FuncCommon.addPeriod(this.state.model.AmountCustomer)}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Ngày cần hàng:
                </Text>
                <Text
                  style={[AppStyles.Textdefault, styles.textRight, { flex: 3 }]}>
                  {this.state.model.RequireDate
                    ? moment(this.state.model.RequireDate).format('DD/MM/YYYY')
                    : ''}
                </Text>
              </View>

              <View
                style={{ flexDirection: 'row', marginTop: 5, marginBottom: 50 }}>
                <Text style={[AppStyles.Labeldefault, { flex: 2 }]}>
                  Ghi chú:
                </Text>
                <Text style={[AppStyles.Textdefault, { flex: 3 }]}>
                  {this.state.model.Note}
                </Text>
              </View>
            </ScrollView>
          )}
          {/* <Divider /> */}
          {/* <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewDetail')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>THÔNG TIN GIAO HÀNG</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewDetail},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity> */}
          {/* <Divider />
          {this.state.ViewDetail !== true ? null : (
            <View style={[{flex: 1, padding: 15}]}>
              {this.state.dataTimeline.length > 0 ? (
                <Timeline
                  data={this.state.dataTimeline}
                  circleSize={20}
                  innerCircle={'dot'}
                  circleColor="rgb(45,156,219)"
                  lineColor="rgb(45,156,219)"
                  timeContainerStyle={{minWidth: 52}}
                  timeStyle={{
                    textAlign: 'center',
                    backgroundColor: '#F5DA81',
                    color: 'black',
                    padding: 5,
                    borderRadius: 13,
                  }}
                  descriptionStyle={{color: 'black'}}
                  style={[
                    this.state.isHiddenDeliveryDates == true
                      ? styles.hidden
                      : '',
                    {},
                  ]}
                />
              ) : (
                <View style={AppStyles.centerAligned}>
                  <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
                </View>
              )}
            </View>
          )} */}
        </View>
      </View>
    );
  }

  // //Lấy thông tin chi tiết đơn hàng
  // GetOrderDetailsById = () => {
  //     API_Orders.GetOrderDetailsById(this.props.DataView.OrderDetailGuid)
  //         .then(res => {
  //             this.setState({
  //                 model: JSON.parse(res.data.data)
  //             })
  //         })
  //         .catch(error => {
  //         });
  // };
  //Lấy thông tin giao hàng
  GetOrderDeliveryDates = () => {
    API_Orders.GetOrderDeliveryDates(this.props.DataView.OrderDetailGuid)
      .then(res => {
        var _OrderDeliveryDates = JSON.parse(res.data.data);
        var _reOrderDeliveryDates = [];
        for (let i = 0; i < _OrderDeliveryDates.length; i++) {
          _reOrderDeliveryDates.push({
            time: FuncCommon.ConDate(_OrderDeliveryDates[i].RequireDate, 0),
            title: _OrderDeliveryDates[i].ItemId,
            description:
              'SL yêu cầu: ' +
              FuncCommon.addPeriod(_OrderDeliveryDates[i].QuantityRequire) +
              '\n' +
              'SL đã giao : ' +
              FuncCommon.addPeriod(_OrderDeliveryDates[i].Quantity) +
              '\n' +
              'SL còn lại : ' +
              FuncCommon.addPeriod(_OrderDeliveryDates[i].RemainQuantity) +
              '\n' +
              'Chú thích : ' +
              _OrderDeliveryDates[i].Description,
          });
        }
        this.setState({ dataTimeline: _reOrderDeliveryDates });
      })
      .catch(error => { });
  };
  onPressBack() {
    Actions.pop();
    Actions.refresh({ moduleId: 'Back', ActionTime: new Date().getTime() });
  }
  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 1, duration: 333 },
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 0, duration: 333 },
    );
  };
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ ViewAll: !this.state.ViewAll });
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ ViewDetail: !this.state.ViewDetail });
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ ViewDetail: true, ViewAll: true });
    }
  };
  //#endregion
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ListOrderDetailsComponent);
