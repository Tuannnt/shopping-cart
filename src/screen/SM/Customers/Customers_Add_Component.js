import React, {Component} from 'react';
import {
  PermissionsAndroid,
  KeyboardAvoidingView,
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Alert,
  TextInput,
  ScrollView,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Divider, Icon} from 'react-native-elements';
import _ from 'lodash';
import {AppStyles, AppColors} from '@theme';
import {
  LoadingComponent,
  TabBar_Title,
  RequiredText,
  Combobox,
  ComboboxV2,
  OpenPhotoLibrary,
} from '@Component';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import Toast from 'react-native-simple-toast';
import RNPickerSelect from 'react-native-picker-select';
import { listCombobox,  } from './listCombobox'
import { controller } from './controller'
const styleCustomIos =
  Platform.OS === 'ios'
    ? {
        inputIOS: {
          fontSize: 16,
          paddingVertical: 10,
          paddingHorizontal: 10,
          color: 'black',
          paddingRight: 10, // to ensure the text is never behind the icon
        },
      }
    : {};
const ListProvince = [
  {value: '0', label: 'Trong nước'},
  {value: '1', label: 'Ngoài nước'},
  ,
];
class Customers_Add_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      auto: false,
      GroupName: '',
      GroupGuid: null,
      ListGroup: [],
      ListResource: [],
      Attachment: [],
      IsProvince: '0',
      DataSubmit: {
        CustomerId: '',
        CustomerName: '',
        WorkEmail: '',
        Address: '',
        WorkMobile: '',
        TaxCode: null,
        SortTitle: '',
        ResourceId: null,
      },
    };
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    if (this.props.DataEdit) {
      this.setState({
        DataSubmit: this.props.DataEdit,
        GroupGuid: this.props.DataEdit.GroupGuid,
        ActivityFieldId: this.props.DataEdit.ActivityFieldId,
      });
    } else {
      this.getNumberAuto();
    }
    this.getTreeGroups();
  }
  getNumberAuto = () => {
    const prefix = this.state.IsProvince === '0' ? 'D' : 'E';
    controller.getNumberAuto(prefix, rs => {
      if (rs === 'null') {
        Actions.pop();
      }
      this.state.DataSubmit.CustomerId = rs.Value;
      this.setState({auto: rs, DataSubmit: this.state.DataSubmit});
    });
  };
  getTreeGroups = () => {
    var list = [{value: null, text: 'Bỏ chọn'}];
    controller.getTreeGroups(res => {
      res.forEach(val => {
        list.push({value: val.GroupGuid, text: val.Title});
      });
      this.setState({ListGroup: list});
      this.getAllCustomerResources();
    });
  };
  getAllCustomerResources = () => {
    var list = [];
    controller.getAllCustomerResources(res => {
      res.forEach(val => {
        list.push({value: val.ResourceId, text: val.ResourceName});
      });
      if (!this.props.DataEdit) {
        this.state.DataSubmit.ResourceId = list[0] ? list[0].value : null;
      }
      this.setState({
        ListResource: list,
        DataSubmit: this.state.DataSubmit,
        loading: false,
      });
    });
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={[AppStyles.container]}>
            <TabBar_Title
              title={
                this.props.DataEdit ? 'Sửa khách hàng' : 'Thêm mới khách hàng'
              }
              callBack={() => this.BackList()}
            />
            <ScrollView>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>
                    Mã khách hàng <RequiredText />
                  </Text>
                </View>
                <View style={[AppStyles.FormInput]}>
                  <Text style={[AppStyles.TextInput, {color: AppColors.black}]}>
                    {this.state.DataSubmit.CustomerId}
                  </Text>
                </View>
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <Text style={AppStyles.Labeldefault}>Loại khách hàng</Text>
                <View
                  style={{
                    ...AppStyles.FormInputPicker,
                    justifyContent: 'center',
                  }}>
                  <RNPickerSelect
                    doneText="Xong"
                    onValueChange={value => {
                      this.setState(
                        {
                          IsProvince: value,
                        },
                        () => {
                          if (this.props.DataEdit) {
                            return;
                          }
                          this.getNumberAuto();
                        },
                      );
                    }}
                    items={ListProvince}
                    placeholder={{}}
                    value={this.state.IsProvince}
                    style={styleCustomIos}
                  />
                </View>
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>
                    Tên khách hàng <RequiredText />
                  </Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  placeholder="Nhập tên khách hàng"
                  autoCapitalize="none"
                  value={this.state.DataSubmit.CustomerName}
                  onChangeText={txt => this.settxt('CustomerName', txt)}
                />
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>
                    Tên viết tắt <RequiredText />
                  </Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  placeholder="Nhập tên viết tắt"
                  autoCapitalize="none"
                  value={this.state.DataSubmit.SortTitle}
                  onChangeText={txt => this.settxt('SortTitle', txt)}
                />
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>
                    Số đăng ký kinh doanh <RequiredText />
                  </Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  placeholder="Nhập số ĐKKD "
                  autoCapitalize="none"
                  keyboardType={'numeric'}
                  value={this.state.DataSubmit.TaxCode}
                  onChangeText={txt => this.settxt('TaxCode', txt)}
                />
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Email</Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  placeholder="Nhập email khách hàng"
                  autoCapitalize="none"
                  value={this.state.DataSubmit.WorkEmail}
                  onChangeText={txt => this.settxt('WorkEmail', txt)}
                  onEndEditing={event => this.setWorkEmail(event)}
                />
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>
                    Địa chỉ <RequiredText />
                  </Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  placeholder="Nhập địa chỉ khách hàng"
                  autoCapitalize="none"
                  value={this.state.DataSubmit.Address}
                  onChangeText={txt => this.settxt('Address', txt)}
                />
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Di động</Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {color: 'black'}]}
                  underlineColorAndroid="transparent"
                  placeholder="Nhập số di động khách hàng"
                  autoCapitalize="none"
                  keyboardType={'numeric'}
                  value={this.state.DataSubmit.WorkMobile}
                  onChangeText={txt => this.settxt('WorkMobile', txt)}
                  onEndEditing={event => this.setWorkMobile(event)}
                />
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Nhóm khách hàng</Text>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionGroup()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.state.GroupGuid == null
                        ? {color: AppColors.gray}
                        : {color: 'black'},
                    ]}>
                    {this.state.GroupGuid !== null
                      ? this.state.GroupName
                      : 'Chọn nhóm khách hàng'}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{padding: 10, paddingBottom: 0}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Nguồn khách hàng</Text>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionResource()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.state.DataSubmit.ResourceId == null
                        ? {color: AppColors.gray}
                        : {color: 'black'},
                    ]}>
                    {this.state.DataSubmit.ResourceId !== null
                      ? this.state.ResourceName
                      : 'Chọn nhóm khách hàng'}
                  </Text>
                </TouchableOpacity>
              </View>
              {/* Đính kèm */}
              <View style={{flexDirection: 'column', padding: 10}}>
                {!this.props.DataEdit && (
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                    </View>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        flexDirection: 'row',
                      }}
                      onPress={() => this.openAttachment()}>
                      <Icon
                        name="attachment"
                        type="entypo"
                        size={15}
                        color={AppColors.ColorAdd}
                      />
                      <Text
                        style={[
                          AppStyles.Labeldefault,
                          {color: AppColors.ColorAdd},
                        ]}>
                        {' '}
                        Chọn file
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}

                <Divider />
                {this.state.Attachment.length > 0 ? (
                  this.state.Attachment.map((para, i) => (
                    <View
                      key={i}
                      style={[{flexDirection: 'row', marginBottom: 3}]}>
                      <View
                        style={[AppStyles.containerCentered, {padding: 10}]}>
                        <Image
                          style={{width: 30, height: 30, borderRadius: 10}}
                          source={{
                            uri: controller.renderImage(para.FileName),
                          }}
                        />
                      </View>
                      <TouchableOpacity
                        key={i}
                        style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={AppStyles.Textdefault}>
                          {para.FileName}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.containerCentered, {padding: 10}]}
                        onPress={() => this.removeAttactment(para)}>
                        <Icon
                          name="close"
                          type="antdesign"
                          size={20}
                          color={AppColors.ColorDelete}
                        />
                      </TouchableOpacity>
                    </View>
                  ))
                ) : (
                  <View style={[{marginBottom: 50}]} />
                )}
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  marginBottom: 20,
                }}>
                <TouchableOpacity
                  style={[
                    AppStyles.containerCentered,
                    {
                      flex: 1,
                      padding: 10,
                      borderRadius: 10,
                      marginLeft: 5,
                      backgroundColor: AppColors.ColorButtonSubmit,
                    },
                  ]}
                  onPress={() => this.Insert()}>
                  <Text style={[AppStyles.Titledefault, {color: '#fff'}]}>
                    Lưu
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>

            <Combobox
              value={this.state.GroupGuid}
              TypeSelect={'single'}
              callback={this.ChangeGroup}
              data={this.state.ListGroup}
              nameMenu={'Chọn nhóm khách hàng'}
              eOpen={this.openComboboxGroup}
              position={'bottom'}
            />
            {this.state.ListResource.length === 0 ? null : (
              <Combobox
                value={this.state.DataSubmit.ResourceId}
                TypeSelect={'single'}
                callback={this.ChangeResource}
                data={this.state.ListResource}
                nameMenu={'Chọn nguồn khách hàng'}
                eOpen={this.openComboboxResource}
                position={'bottom'}
              />
            )}
            <ComboboxV2
              callback={this.ChoiceAtt}
              data={listCombobox.ListComboboxAtt}
              eOpen={this.openCombobox_Att}
            />
            <OpenPhotoLibrary
              callback={this.callbackLibarary}
              openLibrary={this.openLibrary}
            />
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  settxt = (title, txt) => {
    switch (title) {
      case 'CustomerId':
        this.state.DataSubmit.CustomerId = txt;
        break;
      case 'CustomerName':
        this.state.DataSubmit.CustomerName = txt;
        break;
      case 'WorkEmail':
        this.state.DataSubmit.WorkEmail = txt;
        break;
      case 'Address':
        this.state.DataSubmit.Address = txt;
        break;
      case 'WorkMobile':
        this.state.DataSubmit.WorkMobile = txt;
        break;
      case 'SortTitle':
        this.state.DataSubmit.SortTitle = txt;
        break;
      case 'TaxCode':
        this.state.DataSubmit.TaxCode = txt;
      default:
        break;
    }
    this.setState({DataSubmit: this.state.DataSubmit});
  };
  setWorkMobile = val => {
    var regExp = /^(0[3789][0-9]{8}|1[89]00[0-9]{4})$/;
    if (regExp.test(val.nativeEvent.text)) {
      this.state.DataSubmit.WorkMobile = val.nativeEvent.text;
    } else {
      this.state.DataSubmit.WorkMobile = '';
      Toast.showWithGravity(
        'Số điện thoại không hợp lệ',
        Toast.SHORT,
        Toast.CENTER,
      );
    }
    this.setState({DataSubmit: this.state.DataSubmit});
  };
  setWorkEmail = val => {
    var regExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (regExp.test(val.nativeEvent.text)) {
      this.state.DataSubmit.WorkEmail = val.nativeEvent.text;
    } else {
      this.state.DataSubmit.WorkEmail = '';
      Toast.showWithGravity('Email không hợp lệ', Toast.SHORT, Toast.CENTER);
    }
    this.setState({DataSubmit: this.state.DataSubmit});
  };
  _openComboboxGroup() {}
  openComboboxGroup = d => {
    this._openComboboxGroup = d;
  };
  onActionGroup() {
    this._openComboboxGroup();
  }
  ChangeGroup = rs => {
    this.state.GroupGuid = rs.value;
    this.setState({GroupGuid: rs.value, GroupName: rs.text});
  };
  _openComboboxResource() {}
  openComboboxResource = d => {
    this._openComboboxResource = d;
  };
  onActionResource() {
    this._openComboboxResource();
  }
  ChangeResource = rs => {
    this.state.DataSubmit.ResourceId = rs.value;
    this.setState({DataSubmit: this.state.DataSubmit, ResourceName: rs.text});
  };
  Insert = () => {
    if (this.props.DataEdit) {
      controller.Update(
        this.state.auto,
        this.state.DataSubmit,
        this.state.GroupGuid,
        this.state.ActivityFieldId,
        this.state.Attachment,
        this.state.CustomersOfResult,
        rs => {
          this.BackList();
        },
      );
    } else {
      controller.Insert(
        this.state.auto,
        this.state.DataSubmit,
        this.state.GroupGuid,
        this.state.Attachment,
        rs => {
          this.BackList();
        },
      );
    }
  };
  BackList() {
    Actions.pop();
    Actions.refresh({moduleId: 'back', ActionTime: new Date().getTime()});
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  //#region openAttachment
  openAttachment() {}
  openCombobox_Att = d => {
    this.openAttachment = d;
  };
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };

  //#region open img
  openPhotoLibrary() {}
  openLibrary = d => {
    this.openPhotoLibrary = d;
  };
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      var check = this.state.Attachment.find(
        x => x.FileName.toLowerCase() === rs[i].name.toLowerCase(),
      );
      if (check === undefined) {
        this.state.Attachment.push({
          FileName: rs[i].name.toLowerCase(),
          size: rs[i].size,
          type: rs[i].type.toLowerCase(),
          uri: rs[i].uri,
        });
      }
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion

  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        var check = this.state.Attachment.find(
          x => x.FileName.toLowerCase() === res[index].name.toLowerCase(),
        );
        if (check === undefined) {
          _liFiles.push({
            FileName: res[index].name.toLowerCase(),
            size: res[index].size,
            type: res[index].type.toLowerCase(),
            uri: res[index].uri,
          });
        }
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
        } else if (response.error) {
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion

  //#endregion
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Customers_Add_Component);
