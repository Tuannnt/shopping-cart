import {API_Customers, API, API_Orders} from '@network';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '@utils';
import configApp from '../../../configApp';
import Toast from 'react-native-simple-toast';
export default {
  getNumberAuto(Prefixs, callback) {
    let val = {AliasId: 'QTDSCT_CU', Prefixs: Prefixs, VoucherType: ''};
    API.GetNumberAutoSM(val)
      .then(res => {
        var data = JSON.parse(res.data.data);
        console.log(data);
        callback(data);
      })
      .catch(error => {
        alert(error);
      });
  },
  getTreeGroups(callback) {
    API_Customers.GetTreeGroups()
      .then(res => {
        var data = res.data;
        callback(data);
      })
      .catch(error => {
        alert(error);
      });
  },
  removeAttactment(data, list, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },
  getAllStatusCustomers(callback) {
    API_Customers.GetAllStatusCustomers()
      .then(res => {
        var data = JSON.parse(res.data.data);
        callback(data);
      })
      .catch(error => {
        alert(error);
      });
  },
  renderImage(check) {
    if (check !== '') {
      var _check = check.split('.');
      if (_check.length > 1) {
        return (
          configApp.url_icon_chat +
          configApp.link_type_icon +
          _check[_check.length - 1] +
          '-icon.png'
        );
      } else {
        return (
          configApp.url_icon_chat +
          configApp.link_type_icon +
          'default' +
          '-icon.png'
        );
      }
    }
  },
  QuotationsAM_ListAll(staticParamQuotations, list, callback) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        var of = await FuncCommon.Data_Offline_Get(
          'API_Orders_QuotationsAM_ListAll',
        );
        callback(of);
      } else {
        API_Orders.QuotationsAM_ListAll(staticParamQuotations)
          .then(res => {
            list =
              res.data.data != 'null'
                ? list.concat(JSON.parse(res.data.data).data)
                : [];
            FuncCommon.Data_Offline_Set(
              'API_Orders_QuotationsAM_ListAll',
              list,
            );
            callback(list);
          })
          .catch(error => {
            console.log(error);
          });
      }
    });
  },
  GetOrderAll(staticParamOrders, list, callback) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        var of = await FuncCommon.Data_Offline_Get('API_Orders_GetOrderAll');
        callback(of);
      } else {
        API_Orders.GetOrderAll(staticParamOrders)
          .then(res => {
            if (res.data.errorCode == 200) {
              var data = JSON.parse(res.data.data).data;
              for (let i = 0; i < data.length; i++) {
                list.push(data[i]);
              }
              FuncCommon.Data_Offline_Set('API_Orders_GetOrderAll', list);
              callback(list);
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    });
  },
  GetCalendars(staticParamCalendars, list, callback) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        var of = await FuncCommon.Data_Offline_Get('API_Orders_GetCalendars');
        callback(of);
      } else {
        API_Customers.GetCalendars(staticParamCalendars)
          .then(res => {
            list =
              res.data.data != 'null'
                ? list.concat(JSON.parse(res.data.data))
                : [];
            FuncCommon.Data_Offline_Set('API_Orders_GetCalendars', list);
            callback(list);
          })
          .catch(error => {
            console.log(error);
          });
      }
    });
  },
  getAllCustomerResources(callback) {
    API_Customers.GetAllCustomerResources()
      .then(rs => {
        var data = JSON.parse(rs.data.data);
        callback(data);
      })
      .catch(error => {
        console.log(error);
      });
  },
  Update(
    auto,
    data,
    groupGuid,
    activityFieldId,
    att,
    CustomersOfResult,
    callback,
  ) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
    });
    if (data.CustomerName.trim() === '') {
      Toast.showWithGravity(
        'Yêu cầu nhập tên khách hàng',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (data.SortTitle.trim() === '') {
      Toast.showWithGravity(
        'Yêu cầu nhập tên viết tắt',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (data.SortTitle.trim().length > 4) {
      Toast.showWithGravity(
        'Yêu cầu tên viết tắt ít hơn 4 kí tự',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (data.ResourceId === null) {
      Toast.showWithGravity(
        'Yêu cầu chọn nguồn khách hàng',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    // if (data.ShipAddress.trim() === '') {
    //   Toast.showWithGravity(
    //     'Yêu cầu nhập địa chỉ giao hàng',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    // if (activityFieldId === null) {
    //   Toast.showWithGravity(
    //     'Yêu cầu chọn nhóm ngành nghề',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    // if (groupGuid === null) {
    //   Toast.showWithGravity(
    //     'Yêu cầu chọn nhóm khách hàng',
    //     Toast.SHORT,
    //     Toast.CENTER,
    //   );
    //   return;
    // }
    var _employees = {
      ...data,
    };

    var _groupGuid = {GroupGuid: groupGuid};
    let _data = new FormData();
    _data.append('employees', JSON.stringify(_employees));
    _data.append('CustomerGoup', JSON.stringify(_groupGuid));
    _data.append('CustomersOfResult', JSON.stringify(CustomersOfResult));
    API_Customers.Update(_data)
      .then(rs => {
        Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
        if (rs.data.errorCode === 200) {
          callback(rs.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
  Insert(auto, data, groupGuid, att, callback) {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
    });
    if (!data.CustomerName) {
      Toast.showWithGravity(
        'Yêu cầu nhập tên khách hàng',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!data.SortTitle) {
      Toast.showWithGravity(
        'Yêu cầu nhập tên viết tắt',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (data.SortTitle.trim().length > 4) {
      Toast.showWithGravity(
        'Yêu cầu tên viết tắt ít hơn 4 kí tự',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!data.TaxCode) {
      Toast.showWithGravity(
        'Yêu cầu nhập số đăng ký kinh doanh',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!data.ResourceId) {
      Toast.showWithGravity(
        'Yêu cầu chọn nguồn khách hàng',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (!data.Address) {
      Toast.showWithGravity('Yêu cầu nhập địa chỉ', Toast.SHORT, Toast.CENTER);
      return;
    }
    var _employees = {
      CustomerId: data.CustomerId,
      CustomerName: data.CustomerName,
      Address: data.Address,
      WorkEmail: data.WorkEmail,
      WorkMobile: data.WorkMobile,
      SortTitle: data.SortTitle.replace(/ /g, ''),
      TaxCode: data.TaxCode,
      IsBussiness: 1,
      Gender: 'M',
      MaritalStatus: 'M',
      HaveChildren: 0,
      RateByLevel: 'A',
      Status: 'A',
      IsActive: 'Y',
      ResourceName: 'Google+',
      ResourceId: data.ResourceId,
    };
    var _permission = [
      {
        EmployeeGuid: global.__appSIGNALR.SIGNALR_object.USER.EmployeeGuid,
        DepartmentGuid: global.__appSIGNALR.SIGNALR_object.USER.DepartmentGuid,
        DepartmentId: global.__appSIGNALR.SIGNALR_object.USER.DepartmentId,
        EmployeeId: global.__appSIGNALR.SIGNALR_object.USER.EmployeeId,
        LoginName: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
        EmployeeName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        Leader: 'N',
        StartDate: '',
        EndDate: '',
        EvaluateLevel: '',
        EvaluateScore: 0,
        SaleGroupGuid: null,
        SaleGroupName: null,
        GroupName: null,
        GroupGuid: groupGuid,
      },
    ];
    var bank = [
      {
        BankAccountGuid: null,
        BankAccountNo: '',
        BankGuid: null,
        Branch: '',
        ProvinceId: '',
      },
    ];
    var _groupGuid = {GroupGuid: groupGuid};
    let _data = new FormData();
    var _CustomersOfResult = [
      {
        ID: null,
        Date: '',
        Name: '',
        Origin: '',
        Supplier: '',
        Quantity: '0',
        Receiver: '',
        Result: '',
        Follow: '',
        Feedback: '',
        Note: '',
      },
    ];
    var _lstTitlefile = [];
    for (var i = 0; i < att.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: att[i].FileName,
      });
    }
    _data.append('employees', JSON.stringify(_employees));
    _data.append('Permission', JSON.stringify(_permission));
    _data.append('contact', JSON.stringify([]));
    _data.append('socialnetwork', JSON.stringify([]));
    _data.append('CustomerGoup', JSON.stringify(_groupGuid));
    _data.append('auto', JSON.stringify(auto));
    _data.append('CustomersOfResult', JSON.stringify(_CustomersOfResult));
    _data.append('BankAccounts', JSON.stringify(bank));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));
    for (var i = 0; i < att.length; i++) {
      _data.append(att[i].FileName, {
        name: att[i].FileName,
        size: att[i].size,
        type: att[i].type,
        uri: att[i].uri,
      });
    }
    API_Customers.Insert(_data)
      .then(rs => {
        if (rs.data.errorCode === 200) {
          Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
          callback(rs.data);
        } else {
          Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
          return;
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
};
