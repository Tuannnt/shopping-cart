import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  Animated,
  View,
  FlatList,
  Text,
  Dimensions,
  RefreshControl,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_Customers from '../../../network/SM/API_Customers';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import {
  CustomView,
  TabBarBottomCustom,
  LoadingComponent,
  TabBar_Title,
  Combobox,
  FormSearch,
  MenuSearchDate,
} from '@Component';
import Toast from 'react-native-simple-toast';
import {FuncCommon} from '../../../utils';
import controller from './controller';
import DatePicker from 'react-native-date-picker';
const SCREEN_WIDTH = Dimensions.get('window').width;
class ItemCustomersComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      Data: [],
      listEmployeeOfGroups: [],
      listSocialNetworks: [],
      listCustomerContacts: [],
      ListQuotations: [],
      ListOrders: [],
      ListCalendars: [],
      ValueSearchDate: '4', // 30 ngày trước
      staticParamOrders: {
        EndDate: null,
        StartDate: null,
        OrderStatus: [],
        CurrentPage: 1,
        Length: 15,
        Search: '',
        QueryOrderBy: 'CreatedDate DESC',
        CustomerId: null,
        Type: null,
        OrderTypeID: null,
        LoginName: null,
      },
      staticParamQuotations: {
        NumberPage: 1,
        Status: ['0', '1', '2', '3', '4', '5', '6'],
        QueryOrderBy: 'QuotationDate DESC',
        Length: 25,
        search: {
          value: '',
        },
        StartDate: null,
        EndDate: null,
        IsApproved: null,
        CompaignID: null,
        ObjectId: null,
      },
      staticParamCalendars: {
        CurrentPage: 1,
        Length: 25,
        CustomerGuid: null,
      },
      tab1: true,
      tab2: false,
      tab3: false,
      tab4: false,
      //hiển thị thông tin chung
      ViewCustomers: false,
      transIcon_ViewCustomers: new Animated.Value(0),
      //Hiển thị thông tin liên hệ
      ViewCustomerContacts: false,
      transIcon_ViewCustomerContacts: new Animated.Value(0),
      //Hiển thị mạng xã hôi
      ViewSocialNetworks: false,
      transIcon_ViewSocialNetworks: new Animated.Value(0),
      //Hiển thị người quản lý
      ViewEmployeeOfGroups: false,
      transIcon_ViewEmployeeOfGroups: new Animated.Value(0),
      loading: true,
    };
    this.listtabbarBotom = [
      {
        Title: 'Sửa khách hàng',
        Icon: 'edit',
        Type: 'antdesign',
        Value: 'edit',
        Checkbox: false,
        OffEditcolor: true,
      },
      {
        Title: 'Đính kèm',
        Icon: 'attachment',
        Type: 'entypo',
        Value: 'att',
        Checkbox: false,
        OffEditcolor: true,
      },
      {
        Title: 'Xoá khách hàng',
        Icon: 'delete',
        Type: 'antdesign',
        Value: 'delete',
        Checkbox: false,
        OffEditcolor: true,
      },
    ];
  }
  componentDidMount = () => {
    this.Skill();
    this.setViewOpen('ViewCustomers');
    this.getItem();
  };
  getItem = () => {
    let id = this.props.RecordGuid;
    let CustomerGuid = this.props.RecordGuid;
    FuncCommon.Data_Offline(async on => {
      if (on) {
        API_Customers.GetItem(id)
          .then(res => {
            this.setState({Data: JSON.parse(res.data.data)});
            FuncCommon.Data_Offline_Set(
              'API_Customers_GetItem' + this.props.RecordGuid,
              this.state.Data,
            );
            this.state.staticParamOrders.CustomerId = this.state.Data.CustomerId;
            this.state.staticParamQuotations.ObjectId = this.state.Data.CustomerId;
            this.state.staticParamCalendars.CustomerGuid = this.state.Data.CustomerGuid;
            this.setState({
              staticParamOrders: this.state.staticParamOrders,
              staticParamQuotations: this.state.staticParamQuotations,
              staticParamCalendars: this.state.staticParamCalendars,
              loading: false,
            });
          })
          .catch(error => {
            this.setState({loading: false});
            Toast.showWithGravity(
              'Không tìm thấy dữ liệu',
              Toast.SHORT,
              Toast.CENTER,
            );
            Actions.pop();
          });

        API_Customers.GetEmployeeOfGroups(CustomerGuid)
          .then(res => {
            FuncCommon.Data_Offline_Set(
              'API_Customers_GetEmployeeOfGroups' + this.props.RecordGuid,
              JSON.parse(res.data.data),
            );
            this.setState({listEmployeeOfGroups: JSON.parse(res.data.data)});
          })
          .catch(error => {
            console.log(error);
          });
        API_Customers.GetSocialNetworks(CustomerGuid)
          .then(res => {
            FuncCommon.Data_Offline_Set(
              'API_Customers_GetSocialNetworks' + this.props.RecordGuid,
              JSON.parse(res.data.data),
            );
            this.setState({listSocialNetworks: JSON.parse(res.data.data)});
          })
          .catch(error => {
            console.log(error);
          });
        API_Customers.GetCustomerContacts(CustomerGuid)
          .then(res => {
            FuncCommon.Data_Offline_Set(
              'API_Customers_GetCustomerContacts' + this.props.RecordGuid,
              JSON.parse(res.data.data),
            );
            this.setState({listCustomerContacts: JSON.parse(res.data.data)});
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        var of = await FuncCommon.Data_Offline_Get(
          'API_Customers_GetItem' + this.props.RecordGuid,
        );
        var of_GetEmployeeOfGroups = await FuncCommon.Data_Offline_Get(
          'API_Customers_GetEmployeeOfGroups' + this.props.RecordGuid,
        );
        var of_GetSocialNetworks = await FuncCommon.Data_Offline_Get(
          'API_Customers_GetSocialNetworks' + this.props.RecordGuid,
        );
        var of_GetCustomerContacts = await FuncCommon.Data_Offline_Get(
          'API_Customers_GetCustomerContacts' + this.props.RecordGuid,
        );
        this.state.staticParamOrders.CustomerId = of.CustomerId;
        this.state.staticParamQuotations.CustomerGuid = of.CustomerGuid;
        this.state.staticParamCalendars.CustomerGuid = of.CustomerGuid;
        this.setState({
          staticParamOrders: this.state.staticParamOrders,
          staticParamQuotations: this.state.staticParamQuotations,
          staticParamCalendars: this.state.staticParamCalendars,
          loading: false,
          listEmployeeOfGroups: of_GetEmployeeOfGroups,
          listSocialNetworks: of_GetSocialNetworks,
          listCustomerContacts: of_GetCustomerContacts,
        });
      }
    });
  };
  GetQuotations = () => {
    controller.QuotationsAM_ListAll(
      this.state.staticParamQuotations,
      this.state.ListQuotations,
      rs => {
        this.setState({ListQuotations: rs, refreshing: false});
      },
    );
  };
  updateSearchOrder = txt => {
    this.state.staticParamOrders.CurrentPage = 1;
    this.state.staticParamOrders.Search = txt;
    this.setState({
      staticParamOrders: this.state.staticParamOrders,
      ListOrders: [],
    });
    this.GetOrders();
  };
  GetOrders = () => {
    this.setState({refreshing: true});
    controller.GetOrderAll(
      this.state.staticParamOrders,
      this.state.ListOrders,
      rs => {
        this.setState({ListOrders: rs, refreshing: false});
      },
    );
  };
  GetCalendars = () => {
    controller.GetCalendars(
      this.state.staticParamCalendars,
      this.state.ListCalendars,
      rs => {
        this.setState({ListCalendars: rs, refreshing: false});
      },
    );
  };
  renderFooter = () => {
    if (!this.state.isFetching) {
      return null;
    }
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  nextPageQuotations = () => {
    this.state.staticParamQuotations.NumberPage++;
    this.setState(
      {
        staticParamQuotations: this.state.staticParamQuotations,
      },
      () => {
        this.GetQuotations();
      },
    );
  };
  nextPageOrders = () => {
    this.state.staticParamOrders.CurrentPage++;
    this.setState(
      {
        staticParamOrders: this.state.staticParamOrders,
      },
      () => {
        this.GetOrders();
      },
    );
  };
  nextPageCalendars = () => {
    this.state.staticParamCalendars.CurrentPage++;
    this.setState({
      staticParamCalendars: {
        CurrentPage: this.state.staticParamOrders.CurrentPage,
        Length: this.state.staticParamOrders.Length,
        CustomerGuid: this.state.staticParamQuotations.CustomerGuid,
      },
    });
    this.GetCalendars();
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết khách hàng'}
          callBack={() => this.onPressBack()}
        />
        {/* <View style={{ marginLeft: 10, marginRight: 10, flexDirection: 'row', padding: 10 }}>
            <TouchableOpacity onPress={() => this.OnChangeTab('1')} style={[styles.Tabar, styles.TabarPending]}>
              <Text style={this.state.tab1 === true ? { color: AppColors.Maincolor } : ''}>TT chung</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.OnChangeTab('2')} style={styles.Tabar}>
              <Text style={this.state.tab2 === true ? { color: AppColors.Maincolor } : ''}>Báo giá</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.OnChangeTab('3')} style={styles.Tabar}>
              <Text style={this.state.tab3 === true ? { color: AppColors.Maincolor } : ''}>Đơn hàng</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.OnChangeTab('4')} style={[styles.Tabar, styles.TabarApprove]}>
              <Text style={this.state.tab4 === true ? { color: AppColors.Maincolor } : ''}>Lịch</Text>
            </TouchableOpacity>
          </View> */}
        <View style={{flex: 1}}>
          {this.state.tab1 === true ? (
            this.CustomViewTTChung()
          ) : this.state.tab2 == true &&
            this.state.ListQuotations.length > 0 ? (
            this.CustomViewQuotations(this.state.ListQuotations)
          ) : this.state.tab3 == true ? (
            this.CustomViewOrders(this.state.ListOrders)
          ) : this.state.tab4 == true && this.state.ListCalendars.length > 0 ? (
            this.CustomViewCalendars(this.state.ListCalendars)
          ) : (
            <View style={styles.MainContainer}>
              <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
            </View>
          )}
        </View>
        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onConfirm(callback)}
          />
        </View>
        <MenuSearchDate
          value={this.state.ValueSearchDate}
          callback={this.CallbackSearchDate}
          eOpen={this.openMenuSearchDate}
        />
        {this.state.setEventStartDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearchOrder(''),
                  this.setState({setEventStartDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.state.staticParamOrders.StartDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setStartDate(setDate)}
            />
          </View>
        ) : null}
        {this.state.setEventEndDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearchOrder(''),
                  this.setState({setEventEndDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.state.staticParamOrders.EndDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setEndDate(setDate)}
            />
          </View>
        ) : null}
      </View>
    );
  }
  onConfirm = key => {
    switch (key) {
      case 'edit':
        var obj = {
          DataEdit: this.state.Data,
        };
        Actions.customers_Add(obj);
        break;
      case 'att':
        var obj = {
          ModuleId: '15',
          RecordGuid: this.state.Data.CustomerGuid,
        };
        Actions.attachmentComponent(obj);
        break;
      case 'delete':
        this.Delete();
        break;
      default:
        break;
    }
  };
  onPressBack() {
    Actions.pop();
    Actions.refresh({moduleId: 'back', ActionTime: new Date().getTime()});
  }
  //CustomView
  CustomViewTTChung = () => {
    const rotateStart_ViewCustomers = this.state.transIcon_ViewCustomers.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    const rotateStart_ViewCustomerContacts = this.state.transIcon_ViewCustomerContacts.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    const rotateStart_ViewSocialNetworks = this.state.transIcon_ViewSocialNetworks.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    const rotateStart_EmployeeOfGroups = this.state.transIcon_ViewEmployeeOfGroups.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <View style={{flex: 1}}>
        {/* Thông tin chung */}
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setViewOpen('ViewCustomers')}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewCustomers},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewCustomers !== true ? null : (
          <View style={[{padding: 10}]}>
            <View style={{paddingBottom: 5, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Tên khách hàng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {this.state.Data.CustomerName}
                </Text>
              </View>
            </View>
            <View style={{paddingBottom: 5, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Mã khách hàng :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {this.state.Data.CustomerId}
                </Text>
              </View>
            </View>
            <View style={{paddingBottom: 5, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Mã số thuế :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {this.state.Data.TaxCode}
                </Text>
              </View>
            </View>
            <View style={{paddingBottom: 5, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Email :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {this.state.Data.WorkEmail}
                </Text>
              </View>
            </View>
            <View style={{paddingBottom: 5, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>ĐT cố định :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {this.state.Data.WorkPhone}
                </Text>
              </View>
            </View>
            <View style={{paddingBottom: 5, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>ĐT di động :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {this.state.Data.WorkMobile}
                </Text>
              </View>
            </View>
            <View style={{paddingBottom: 5, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Địa chỉ :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {this.state.Data.Address}
                </Text>
              </View>
            </View>
          </View>
        )}
        {/* Thông tin liên hệ */}
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setViewOpen('ViewCustomerContacts')}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>THÔNG TIN LIÊN HỆ</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewCustomerContacts},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewCustomerContacts !== true ? null : (
          <ScrollView>
            {this.state.listCustomerContacts.length > 0 ? (
              this.state.listCustomerContacts.map((item, index) => (
                <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
                  <ListItem
                    title={item.FullName}
                    titleStyle={AppStyles.Titledefault}
                    subtitle={() => {
                      return (
                        <View>
                          <View style={{flexDirection: 'row'}}>
                            <View style={{width: '50%', paddingRight: 5}}>
                              <Text style={AppStyles.Textdefault}>
                                Chức vụ: {item.JobTitle}
                              </Text>
                              <Text style={AppStyles.Textdefault}>
                                Địa chỉ: {item.Address}
                              </Text>
                            </View>
                            <View style={{width: '50%', paddingLeft: 5}}>
                              <Text style={AppStyles.Textdefault}>
                                SĐT: {item.WorkPhone}
                              </Text>
                              <Text style={AppStyles.Textdefault}>
                                Email: {item.WorkEmail}
                              </Text>
                            </View>
                          </View>
                        </View>
                      );
                    }}
                  />
                </View>
              ))
            ) : (
              <View style={styles.MainContainer}>
                <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
              </View>
            )}
          </ScrollView>
        )}
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setViewOpen('ViewSocialNetworks')}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>MẠNG XÃ HỘI</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewSocialNetworks},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewSocialNetworks !== true ? null : (
          <ScrollView>
            {this.state.listSocialNetworks.length > 0 ? (
              this.state.listSocialNetworks.map((item, index) => (
                <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
                  <ListItem
                    title={item.Title}
                    titleStyle={AppStyles.Titledefault}
                    subtitle={() => {
                      return (
                        <View>
                          <View style={{flexDirection: 'row'}}>
                            <View style={{width: '50%'}}>
                              <Text style={AppStyles.Textdefault}>
                                Url: {item.Url}
                              </Text>
                            </View>
                            <View style={{width: '50%'}}>
                              <Text style={AppStyles.Textdefault}>
                                Mô tả: {item.Description}
                              </Text>
                            </View>
                          </View>
                        </View>
                      );
                    }}
                  />
                </View>
              ))
            ) : (
              <View style={styles.MainContainer}>
                <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
              </View>
            )}
          </ScrollView>
        )}
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setViewOpen('ViewEmployeeOfGroups')}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>NGƯỜI QUẢN LÝ</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_EmployeeOfGroups},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewEmployeeOfGroups !== true ? null : (
          <ScrollView>
            {this.state.listEmployeeOfGroups.length > 0 ? (
              this.state.listEmployeeOfGroups.map((item, index) => (
                <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
                  <ListItem
                    title={item.EmployeeName}
                    titleStyle={AppStyles.Titledefault}
                    subtitle={() => {
                      return (
                        <View>
                          <View style={{flexDirection: 'row'}}>
                            <View style={{width: '50%', paddingRight: 5}}>
                              <Text style={AppStyles.Textdefault}>
                                Chức vụ: {item.JobTitleName}
                              </Text>
                            </View>
                            <View style={{width: '50%', paddingLeft: 5}}>
                              <Text style={AppStyles.Textdefault}>
                                Bộ phận: {item.DepartmentName}
                              </Text>
                            </View>
                          </View>
                        </View>
                      );
                    }}
                  />
                </View>
              ))
            ) : (
              <View style={styles.MainContainer}>
                <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
              </View>
            )}
          </ScrollView>
        )}
      </View>
    );
  };
  CustomViewQuotations = DataQuotations => (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <FlatList
        refreshing={this.state.isFetching}
        ref={ref => {
          this.ListView_Ref = ref;
        }}
        ListFooterComponent={this.renderFooter}
        data={DataQuotations}
        renderItem={({item, index}) => (
          <ListItem
            title={() => {
              return (
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text style={[AppStyles.Titledefault]}>
                      {item.QuotationNo}
                    </Text>
                  </View>
                  <Text
                    style={{
                      justifyContent: 'center',
                      color: AppColors.AcceptColor,
                    }}>
                    {this.convertStatus(item.Status)}
                  </Text>
                </View>
              );
            }}
            subtitle={() => {
              return (
                <View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Textdefault}>
                      Khách hàng: {item.CustomerName}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[AppStyles.Textdefault, {width: '75%'}]}>
                      Ngày: {FuncCommon.ConDate(item.QuotationDate, 0)}
                    </Text>
                  </View>
                </View>
              );
            }}
            bottomDivider
            titleStyle={AppStyles.Textdefault}
            onPress={() => this.openViewQuotations(item.QuotationGuid)}
          />
        )}
        onEndReachedThreshold={0.5}
        onEndReached={() =>
          this.state.ListQuotations.length <
          this.state.staticParamQuotations.NumberPage *
            this.state.staticParamQuotations.Length
            ? null
            : this.nextPageQuotations()
        }
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
    </View>
  );
  CustomViewOrders = DataOrders => (
    <View>
      <View style={{flexDirection: 'column'}}>
        <View
          style={{
            flexDirection: 'row',
            paddingLeft: 10,
            paddingRight: 10,
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              padding: 5,
            }}>
            <Text style={[AppStyles.Labeldefault]}>Thời gian bắt đầu</Text>
            <TouchableOpacity
              style={[AppStyles.FormInput]}
              onPress={() => this.setState({setEventStartDate: true})}>
              <Text>
                {FuncCommon.ConDate(this.state.staticParamOrders.StartDate, 0)}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
            <Text style={[AppStyles.Labeldefault]}>Thời gian kết thúc</Text>
            <TouchableOpacity
              style={[AppStyles.FormInput]}
              onPress={() => this.setState({setEventEndDate: true})}>
              <Text>
                {FuncCommon.ConDate(this.state.staticParamOrders.EndDate, 0)}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'column', padding: 5}}>
            <Text
              style={[
                AppStyles.Labeldefault,
                {textAlign: 'center'},
                styles.timeHeader,
              ]}>
              Lọc
            </Text>
            <TouchableOpacity
              style={[AppStyles.FormInput]}
              onPress={() => this.onActionSearchDate()}>
              <Icon
                name={'down'}
                type={'antdesign'}
                size={18}
                color={AppColors.gray}
              />
            </TouchableOpacity>
          </View>
        </View>
        <FormSearch
          CallbackSearch={callback => this.updateSearchOrder(callback)}
        />
      </View>
      {DataOrders.length == 0 ? null : (
        <FlatList
          style={{backgroundColor: '#fff'}}
          refreshing={this.state.isFetching}
          ref={ref => {
            this.ListView_Ref = ref;
          }}
          ListFooterComponent={this.renderFooter}
          data={DataOrders}
          renderItem={({item, index}) => (
            <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
              <ListItem
                title={() => {
                  return (
                    <View style={{flexDirection: 'row'}}>
                      <Text style={[AppStyles.Titledefault, {width: '75%'}]}>
                        {item.OrderNumber}
                      </Text>
                      <Text style={[AppStyles.Textdefault, {width: '25%'}]}>
                        {FuncCommon.addPeriod(item.TotalOfMoney)}
                      </Text>
                    </View>
                  );
                }}
                subtitle={() => {
                  return (
                    <View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={AppStyles.Textdefault}>
                          Khách hàng: {item.CustomerName}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={[AppStyles.Textdefault, {width: '75%'}]}>
                          Trạng thái: {item.OrderStatusString}
                        </Text>
                        <Text style={[AppStyles.Textdefault, {width: '25%'}]}>
                          {item.OrderDateString}
                        </Text>
                      </View>
                    </View>
                  );
                }}
                bottomDivider
                titleStyle={AppStyles.Textdefault}
                //chevron
                onPress={() => this.openViewOrder(item.OrderGuid)}
              />
            </View>
          )}
          onEndReachedThreshold={0.5}
          onEndReached={() =>
            this.state.ListOrders.length <
            this.state.staticParamOrders.CurrentPage *
              this.state.staticParamOrders.Length
              ? null
              : this.nextPageOrders()
          }
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
              tintColor="#f5821f"
              titleColor="#fff"
              colors={['red', 'green', 'blue']}
            />
          }
        />
      )}
    </View>
  );
  CustomViewCalendars = DataCalendars => (
    <View>
      <FlatList
        style={{backgroundColor: '#fff'}}
        refreshing={this.state.isFetching}
        ref={ref => {
          this.ListView_Ref = ref;
        }}
        ListFooterComponent={this.renderFooter}
        data={DataCalendars}
        renderItem={({item, index}) => (
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              title={item.Title}
              subtitle={() => {
                return (
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '65%'}}>
                        <Text style={AppStyles.Textdefault}>
                          Bắt đầu: {FuncCommon.ConDate(item.StartTime, 0)}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          Người tạo: {item.CreatedBy.split('#')[1]}
                        </Text>
                      </View>
                      <View style={{width: '35%'}}>
                        <Text style={AppStyles.Textdefault}>
                          Kết thúc: {FuncCommon.ConDate(item.EndTime, 0)}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              }}
              bottomDivider
              titleStyle={AppStyles.Titledefault}
              //chevron
              onPress={() => this.onView(item)}
            />
          </View>
        )}
        onEndReachedThreshold={0.5}
        onEndReached={({distanceFromEnd}) => {
          if (this.state.staticParamCalendars.CurrentPage !== 1) {
            this.nextPageCalendars();
          }
        }}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
    </View>
  );
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.state.staticParamOrders.StartDate = new Date(callback.start);
      this.state.staticParamOrders.EndDate = new Date(callback.end);
      this.setState({
        staticParamOrders: this.state.staticParamOrders,
        ValueSearchDate: callback.value,
      });
      this.updateSearchOrder('');
    }
  };
  setStartDate = date => {
    this.state.staticParamOrders.StartDate = date;
    this.setState({
      staticParamOrders: this.state.staticParamOrders,
    });
    this.updateSearchOrder('');
  };
  setEndDate = date => {
    this.state.staticParamOrders.EndDate = date;
    this.setState({
      staticParamOrders: this.state.staticParamOrders,
    });
    this.updateSearchOrder('');
  };
  convertStatus = data => {
    if (data === '0') {
      return 'Nháp';
    }
    if (data === '1') {
      return 'Chờ phản hồi';
    }
    if (data === '2') {
      return 'Đã phản hồi';
    }
    if (data === '3') {
      return 'Đặt hàng';
    }
    if (data === '4') {
      return 'Không đặt hàng';
    }
    if (data === '5') {
      return 'Chờ xử lý';
    }
    if (data === '6') {
      return 'Đã xử lý';
    }
  };
  //Chuyển tab
  OnChangeTab(data) {
    if (data == '1') {
      this.setState({
        tab2: false,
        tab3: false,
        tab4: false,
        tab1: true,
      });
    } else if (data == '2') {
      this.setState(
        {
          tab1: false,
          tab3: false,
          tab4: false,
          tab2: true,
          ListQuotations: [],
        },
        () => {
          this.GetQuotations();
        },
      );
    } else if (data == '3') {
      this.setState({
        tab1: false,
        tab2: false,
        tab3: true,
        tab4: false,
      });
    } else if (data == '4') {
      this.setState(
        {
          tab1: false,
          tab2: false,
          tab3: false,
          tab4: true,
          ListCalendars: [],
        },
        () => {
          this.GetCalendars();
        },
      );
    }
  }
  openViewOrder(id) {
    Actions.viewOrder({OrderGuid: id});
  }
  openViewQuotations(id) {
    Actions.openQuotations({RecordGuid: id});
  }
  Skill = () => {
    this.Animated_on_ViewCustomers = Animated.timing(
      this.state.transIcon_ViewCustomers,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewCustomers = Animated.timing(
      this.state.transIcon_ViewCustomers,
      {toValue: 0, duration: 333},
    );
    this.Animated_on_ViewCustomerContacts = Animated.timing(
      this.state.transIcon_ViewCustomerContacts,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewCustomerContacts = Animated.timing(
      this.state.transIcon_ViewCustomerContacts,
      {toValue: 0, duration: 333},
    );
    this.Animated_on_ViewSocialNetworks = Animated.timing(
      this.state.transIcon_ViewSocialNetworks,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewSocialNetworks = Animated.timing(
      this.state.transIcon_ViewSocialNetworks,
      {toValue: 0, duration: 333},
    );
    this.Animated_on_ViewEmployeeOfGroups = Animated.timing(
      this.state.transIcon_ViewEmployeeOfGroups,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewEmployeeOfGroups = Animated.timing(
      this.state.transIcon_ViewEmployeeOfGroups,
      {toValue: 0, duration: 333},
    );
  };
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewCustomers') {
      if (this.state.ViewCustomers === false) {
        Animated.sequence([this.Animated_on_ViewCustomers]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewCustomers]).start();
      }
      this.setState({ViewCustomers: !this.state.ViewCustomers});
    } else if (val === 'ViewCustomerContacts') {
      if (this.state.ViewCustomerContacts === false) {
        Animated.sequence([this.Animated_on_ViewCustomerContacts]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewCustomerContacts]).start();
      }
      this.setState({ViewCustomerContacts: !this.state.ViewCustomerContacts});
    } else if (val === 'ViewSocialNetworks') {
      if (this.state.ViewSocialNetworks === false) {
        Animated.sequence([this.Animated_on_ViewSocialNetworks]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewSocialNetworks]).start();
      }
      this.setState({ViewSocialNetworks: !this.state.ViewSocialNetworks});
    } else if (val === 'ViewEmployeeOfGroups') {
      if (this.state.ViewEmployeeOfGroups === false) {
        Animated.sequence([this.Animated_on_ViewEmployeeOfGroups]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewEmployeeOfGroups]).start();
      }
      this.setState({ViewEmployeeOfGroups: !this.state.ViewEmployeeOfGroups});
    }
  };
  Delete = () => {
    Alert.alert(
      'Xác nhận thông tin',
      `Bạn có chắc chắn xóa ${this.state.Data.CustomerName}?`,
      [{text: 'Xác nhận', onPress: () => this.API_Delete()}, {text: 'Đóng'}],
    );
  };
  API_Delete = () => {
    FuncCommon.Data_Offline(async on => {
      if (!on) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
    });
    var obj = {
      CustomerGuid: this.state.Data.CustomerGuid,
    };
    API_Customers.DeletedKH(obj)
      .then(rs => {
        Toast.showWithGravity(rs.data.message, Toast.SHORT, Toast.CENTER);
        if (rs.data.errorCode === 200) {
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}
const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
  Tabar: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    padding: 10,
  },
  TabarPending: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
});
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemCustomersComponent);
