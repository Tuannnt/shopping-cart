import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, ListItem, SearchBar, Badge} from 'react-native-elements';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_Customers from '../../../network/SM/API_Customers';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import Combobox from '../../component/Combobox';
import { listCombobox,  } from './listCombobox'
import { controller } from './controller'
import {FuncCommon} from '@utils';
import LoadingComponent from '../../component/LoadingComponent';
class ListCustomersComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: {},
      list: [],
      isFetching: false,
      selected: undefined,
      openSearch: false,
      StatusCustomers: [],
      refreshing: true,
    };
    this.staticParam = {
      CurrentPage: 1,
      Length: 10,
      Search: '',
      IsBussiness: null,
      Status: null,
    };
  }
  GetAll = () => {
    API_Customers.ListAll(this.staticParam)
      .then(res => {
        this.state.list =
          res.data.data != 'null'
            ? this.state.list.concat(JSON.parse(res.data.data))
            : [];
        this.setState({list: this.state.list, refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };
  componentDidMount = () => {
    // this.GetAll();
    this.getAllStatusCustomers();
  };
  componentWillReceiveProps = nextProps => {
    this.updateSearch('');
  };
  getAllStatusCustomers = () => {
    controller.getAllStatusCustomers(rs => {
      var list = [
        {
          value: null,
          text: 'Tất cả',
        },
      ];
      rs.forEach(val => {
        list.push({value: val.StatusId, text: val.StatusName});
      });
      this.setState({StatusCustomers: list});
    });
  };
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  ChangeStatus = rs => {
    if (!rs) {
      return;
    }
    this.staticParam.Status = rs.value;
    this.staticParam.StatusName = rs.text;
    this.updateSearch('');
  };
  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCombobox() {
    this._openCombobox();
  }
  updateSearch = text => {
    this.staticParam.Search = text;
    this.staticParam.CurrentPage = 1;
    this.setState(
      {
        list: [],
      },
      () => {
        this.GetAll();
      },
    );
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Khách hàng'}
          FormSearch={true}
          addForm={true}
          CallbackFormAdd={() => Actions.customers_Add()}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'SM'}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity
              style={[AppStyles.FormInput, {marginHorizontal: 10}]}
              onPress={() => this.onActionCombobox()}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.staticParam.StatusName
                    ? {color: 'black'}
                    : {color: AppColors.gray},
                ]}>
                {this.staticParam.StatusName
                  ? this.staticParam.StatusName
                  : 'Chọn trạng thái khách hàng...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  ncolor={AppColors.gray}
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                />
              </View>
            </TouchableOpacity>
            <FormSearch
              CallbackSearch={callback => this.updateSearch(callback)}
            />
          </View>
        ) : null}
        <View style={{flex: 1}}>
          <FlatList
            data={this.state.list}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={{
                  padding: 10,
                  borderBottomWidth: 0.5,
                  borderBottomColor: AppColors.gray,
                }}
                onPress={() => this.openView(item.CustomerGuid)}>
                <View style={{flex: 1}}>
                  <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                    {item.CustomerName + ' (' + item.CustomerId + ')'}
                  </Text>
                </View>

                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Textdefault}>
                      Email: {item.WorkEmail}
                    </Text>
                  </View>
                  <Text style={AppStyles.Textdefault}>
                    Tổng tiền: {FuncCommon.addPeriod(item.Capital)}
                  </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text style={AppStyles.Textdefault}>
                      ĐT: {item.WorkPhone}
                    </Text>
                  </View>
                  <Text style={AppStyles.Textdefault}>
                    {FuncCommon.ConDate(item.CreatedDate, 0, 'iso')}
                  </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      backgroundColor: '#24c79f',
                      padding: 10,
                      borderRadius: 10,
                      marginRight: 5,
                    }}>
                    <Text style={[AppStyles.Textdefault, {color: '#fff'}]}>
                      Báo giá: {item.CountQuotation}
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: '#f0b71e',
                      padding: 10,
                      borderRadius: 10,
                    }}>
                    <Text style={[AppStyles.Textdefault, {color: '#fff'}]}>
                      Đơn hàng: {item.CountOrder}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            onEndReachedThreshold={0.5}
            // onEndReached={({ distanceFromEnd }) => {
            //     this.nextPage();
            // }}
            onEndReached={() => {
              this.state.list.length >=
              this.staticParam.CurrentPage * this.staticParam.Length
                ? this.nextPage(false)
                : null;
            }}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                colors={['red', 'green', 'blue']}
              />
            }
          />
        </View>
        {this.state.StatusCustomers.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeStatus}
            data={this.state.StatusCustomers}
            nameMenu={'Chọn trạng thái khách hàng'}
            eOpen={this.openCombobox}
            position={'bottom'}
            value={this.staticParam.Status}
          />
        ) : null}
      </View>
    );
  }

  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetAll();
  }
  //Load lại
  onRefresh = () => {
    this.updateSearch('');
  };

  openView(id) {
    Actions.smitemCustomers({RecordGuid: id});
  }
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListCustomersComponent);
