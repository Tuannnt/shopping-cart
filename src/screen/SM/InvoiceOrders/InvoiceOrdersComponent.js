import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {
  Header,
  Divider,
  Icon,
  ListItem,
  Badge,
  SearchBar,
} from 'react-native-elements';
import {API_InvoiceOrders} from '@network';
import TabBar from '../../component/TabBar';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import Fonts from '../../../theme/fonts';
import {Picker} from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import {FuncCommon} from '../../../utils';
import Combobox from '../../component/Combobox';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import listCombobox from './listCombobox';
import controller from './controller';
const SCREEN_WIDTH = Dimensions.get('window').width;
class InvoiceOrdersComponent extends Component {
  constructor(props) {
    super(props);
    //khai báo biến
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      refreshing: false,
      Data: null,
      ListAll: [],
      ListCountNoti: [],
      EvenFromSearch: false,
      selectedTab: 'profile',
    };
    // dữ liệu trueyenf vao APi
    this._search = {
      StartDate: new Date(),
      EndDate: new Date(),
      Status: 'N',
      NumberPage: 0,
      search: {value: ''},
      Length: 15,
      QueryOrderBy: 'Date DESC',
      Object: null,
      Draw: 3,
    };
    this.Total = 0;
    this.onEndReachedCalledDuringMomentum = true;
    this.listtabbarBotom = [
      {
        Title: 'Chưa xuất HĐ',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã xuất HĐ',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
  }
  componentDidMount = () => {
    this.getAllCustomer();
  };
  getAllCustomer = () => {
    controller.getAllCustomer(rs => {
      let ListCustomer = JSON.parse(rs).map(i => ({
        value: i.CustomerGuid,
        text: `${i.CustomerId} - ${i.CustomerName}`,
      }));
      ListCustomer.unshift({
        value: null,
        text: 'Tất cả',
      });
      this.setState({ListCustomer});
    });
  };
  // khi trả lại trang,thì load vào danh sách
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.nextPage('reload');
    }
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.search.value = search;
    this._search.NumberPage = 0;
    this.nextPage('reload');
  };

  //List danh sách phân trang
  nextPage(type) {
    this.setState({refreshing: true}, () => {
      if (type === 'reload') {
        this._search.NumberPage = 1;
      } else {
        this._search.NumberPage++;
      }
      API_InvoiceOrders.GetAll(this._search)
        .then(res => {
          var _listall = this.state.ListAll;
          let _data = JSON.parse(res.data.data);
          _listall = _listall.concat(_data);
          if (type === 'reload') {
            _listall = _data;
          }
          this.setState({
            ListAll: _listall,
            refreshing: false,
          });
        })
        .catch(error => {
          console.log(error);
          this.setState({
            refreshing: false,
          });
        });
    });
  }

  // khi kéo danh sách,thì phân trang
  loadData() {
    this.nextPage();
  }
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  _openComboboxRation() {}
  openComboboxRation = d => {
    this._openComboboxRation = d;
  };
  onActionComboboxRation() {
    this._openComboboxRation();
  }
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this._search.StartDate = new Date(callback.start);
      this._search.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.nextPage('reload');
    }
  };
  setStartDate = date => {
    this._search.StartDate = date;
  };
  setEndDate = date => {
    this._search.EndDate = date;
  };
  ChangeRation = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === null) {
      return;
    }
    this._search.Type = rs.value;
    this._search.TypeName = rs.text;
    this.nextPage('reload');
  };
  _openComboboxEmployee() {}
  openComboboxEmployee = d => {
    this._openComboboxEmployee = d;
  };
  onActionComboboxEmployee() {
    this._openComboboxEmployee();
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'profile':
        this.onChoDuyet();
        break;
      case 'checkcircleo':
        this.onDaDuyet();
        break;
      default:
        break;
    }
  }
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this._search.Status = 'N';
    this.nextPage('reload');
  };
  // sự kiện đã  duyệt
  onDaDuyet = () => {
    this._search.Status = 'Y';
    this.nextPage('reload');
  };
  // view hiển thị
  CustomeListAll = item => (
    <View style={{flex: 30}}>
      <FlatList
        data={item}
        ListEmptyComponent={this.ListEmpty}
        style={{height: '100%', marginBottom: 10}}
        renderItem={({item, index}) => (
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              subtitle={() => {
                return (
                  <View style={{flexDirection: 'row', marginTop: -25}}>
                    <View style={{flex: 3, flexDirection: 'row'}}>
                      <View>
                        <Text
                          style={[
                            AppStyles.Titledefault,
                            {
                              //color: '#2E77FF',
                            },
                          ]}>
                          {item.RequestInvoiceNo}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          Khách hàng: {item.ObjectName}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1.5,
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                      }}>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {textAlign: 'right', justifyContent: 'center'},
                        ]}>
                        {item.RequirementDateString}
                      </Text>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {
                            textAlign: 'right',
                            justifyContent: 'center',
                            marginTop: 2,
                            fontWeight: 'bold',
                          },
                        ]}>
                        {FuncCommon.addPeriod(item.TotalWithVat)}
                      </Text>
                    </View>
                  </View>
                );
              }}
              bottomDivider
              //chevron
              onPress={() => this.onViewItem(item)}
            />
          </View>
        )}
        onEndReachedThreshold={0.5}
        onMomentumScrollBegin={() => {
          this.onEndReachedCalledDuringMomentum = false;
        }}
        onEndReached={() => this.loadData()}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
    </View>
  );
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Đề nghị xuất HĐBH'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            BackModuleByCode={'HR'}
          />
          <Divider />
          <View>
            {this.state.EvenFromSearch == true ? (
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventStartDate: true})}>
                      <Text>
                        {FuncCommon.ConDate(this._search.StartDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({setEventEndDate: true})}>
                      <Text>{FuncCommon.ConDate(this._search.EndDate, 0)}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'column', padding: 5}}>
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        {textAlign: 'center'},
                        styles.timeHeader,
                      ]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput, {marginHorizontal: 10}]}
                  onPress={() => {
                    this.onActionComboboxEmployee();
                  }}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this._search.employee
                        ? {color: 'black'}
                        : {color: AppColors.gray},
                    ]}>
                    {this._search.ObjectName
                      ? FuncCommon.handleLongText(this._search.ObjectName, 35)
                      : 'Chọn khách hàng...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      color={AppColors.gray}
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={search => this.updateSearch(search)}
                  value={this._search.search.value}
                />
              </View>
            ) : null}
          </View>

          <View style={{flex: 20}}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage('reload'),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage('reload'),
                    this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this._search.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {this.state.ListCustomer && (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeCustomer}
              data={this.state.ListCustomer}
              nameMenu={'Chọn khách hàng'}
              eOpen={this.openComboboxEmployee}
              position={'bottom'}
              value={''}
            />
          )}
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.onCallbackValueBottom(callback)
              }
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  ChangeCustomer = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === '') {
      return;
    }
    this._search.Object = rs.value;
    this._search.ObjectName = rs.text;
    this.nextPage('reload');
  };
  ListEmpty = () => {
    if (this.state.ListAll && this.state.ListAll.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={[AppStyles.centerAligned, {marginTop: 10}]}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //view item,click sang view khác
  onViewItem(item) {
    Actions.GetInvoiceOrders({
      RecordGuid: item.InvoiceOrderGuid,
    });
  }
}
//custom giao diện
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '100',
    fontSize: 30,
    color: '#f6b801',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#CCCCCC',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  ClickApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#CCCCCC',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  timeHeader: {
    marginBottom: 3,
  },
});
const mapStateToProps = state => ({});

const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(InvoiceOrdersComponent);
