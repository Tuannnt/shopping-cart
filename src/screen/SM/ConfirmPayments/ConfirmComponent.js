import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Keyboard,
  StyleSheet,
  Alert,
} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Container} from 'native-base';
import {FuncCommon} from '../../../utils';
import DatePicker from 'react-native-datepicker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import _ from 'lodash';
import {AppStyles, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RequiredText from '../../component/RequiredText';
import LoadingComponent from '../../component/LoadingComponent';
import {API_HR, API_ConfirmPayments} from '@network';
import listCombobox from './listCombobox';
import moment from 'moment';
import Toast from 'react-native-simple-toast';

const headerTable = listCombobox.headerTable;
class AddWorkShiftChangeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [{}],
      TotalReally: '0',
      ConfirmBy: '',
      ActualDate: FuncCommon.ConDate(new Date(), 0),
      Quantity: 0,
      search: '',
      loading: false,
      ListEmployee: [],
      workShiftData: [],
    };
    this.setDate = this.setDate.bind(this);
  }
  componentDidMount(): void {
    if (this.props.item.ActualDate) {
      this.setState({
        ActualDate: FuncCommon.ConDate(new Date(this.props.item.ActualDate), 0),
      });
    }
    if (this.props.item.TotalReally) {
      this.setState({
        TotalReally: this.props.item.TotalReally.toString().replace(
          /\B(?=(\d{3})+(?!\d))/g,
          '.',
        ),
      });
    }
  }
  // getItem = (id) => {
  //   controller.getItem(id, rs => {
  //     let data = JSON.parse(rs);
  //     this.setState({ data });
  //   });
  // };
  onChangeType(value) {
    this.setState({
      Type: value,
    });
  }
  setNote(Note) {
    this.setState({Note});
  }
  setTotalReally = TotalReally => {
    let _str = TotalReally.split('.')
      .join('')
      .replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    this.setState({TotalReally: _str});
  };
  setQuantity(Quantity) {
    this.setState({Quantity});
  }
  onClickBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  Submit() {
    let {TotalReally} = this.state;
    if (!TotalReally) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa điền số tiền thực',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (+TotalReally.split('.').join('') <= 0) {
      Alert.alert(
        'Thông báo',
        'Số tiền thực phải lớn hơn hoặc bằng 0',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    let _obj = {
      OrderPaymentGuid: this.props.RecordGuid,
      ActualDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.ActualDate, 99),
        2,
      ),
      TotalReally: +TotalReally.split('.').join(''),
      Note: this.state.Note,
    };
    let _data = new FormData();
    _data.append('confirm', JSON.stringify(_obj));
    API_ConfirmPayments.Confirm(_data)
      .then(rs => {
        console.log(rs);
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          Alert.alert(
            'Thông báo',
            'Có lỗi xảy ra',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
        } else {
          Toast.showWithGravity(
            'Xác nhận thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onClickBack();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }
  setDate(ActualDate) {
    this.setState({ActualDate: ActualDate});
  }
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  handlePickDateOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisibleDate: true,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({
      rows: res,
      typeTime: null,
      currentIndex: null,
      modalPickerTimeVisible: false,
      modalPickerTimeVisibleDate: false,
    });
  };
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };

  render() {
    const {isEdit, WorkShiftChangeId} = this.state;
    const IS_NV = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'NV';
    if (this.state.loading) {
      return <LoadingComponent />;
    }
    return (
      <Container>
        <View style={{flex: 30}}>
          <View style={{flex: 40}}>
            <TabBar_Title
              title={'Xác nhận tạm ứng'}
              callBack={() => this.onClickBack()}
            />
            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View
                    style={{
                      padding: 10,
                      paddingBottom: 0,
                      flexDirection: 'column',
                    }}>
                    <View style={{flexDirection: 'row', marginBottom: 5}}>
                      <Text style={AppStyles.Labeldefault}>
                        Ngày thực <RequiredText />
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '100%'}}>
                        <DatePicker
                          locale="vie"
                          locale={'vi'}
                          style={{width: '100%'}}
                          date={this.state.ActualDate}
                          mode="date"
                          androidMode="spinner"
                          placeholder="Chọn ngày"
                          format="DD/MM/YYYY"
                          confirmBtnText="Xong"
                          cancelBtnText="Huỷ"
                          customStyles={{
                            dateIcon: {
                              position: 'absolute',
                              right: 0,
                              top: 4,
                              marginLeft: 0,
                            },
                            dateInput: {
                              marginRight: 36,
                            },
                          }}
                          onDateChange={date => this.setDate(date)}
                        />
                      </View>
                    </View>
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>
                        Số tiền thực <RequiredText />
                      </Text>
                    </View>
                    <TextInput
                      style={[AppStyles.FormInput, {color: 'black'}]}
                      underlineColorAndroid="transparent"
                      value={this.state.TotalReally}
                      autoCapitalize="none"
                      keyboardType="numeric"
                      onChangeText={TotalReally => {
                        this.setTotalReally(TotalReally + '');
                      }}
                    />
                  </View>
                  <View style={{padding: 10, paddingBottom: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                    </View>
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      autoCapitalize="none"
                      numberOfLines={4}
                      multiline
                      value={this.state.Note}
                      onChangeText={Note => this.setNote(Note)}
                    />
                  </View>
                </View>

                {/* Table */}
              </View>
            </ScrollView>
            {this.state.modalPickerTimeVisibleDate && (
              <DateTimePickerModal
                locale="vi-VN"
                cancelTextIOS="Huỷ"
                confirmTextIOS="Chọn"
                headerTextIOS="Chọn thời gian"
                isVisible={this.state.modalPickerTimeVisibleDate}
                mode="date"
                onConfirm={this.handleConfirmTime}
                onCancel={this.hideDatePicker}
              />
            )}
          </View>
        </View>

        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
      </Container>
    );
  }
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'arrowleft'}
          type="antdesign"
          size={30}
          onPress={() => this.onPressBack()}
        />
      </View>
    );
  }
  // Validate = () => {
  //      if (_obj.Quantity === null || _obj.Quantity === 0 || _obj.Quantity ===  undefined ) return null;
  //       return (
  //           //View to show when list is empty
  //           <View style={styles.MainContainer}>
  //               <Text style={{ textAlign: 'center' }}>nhập số lượng.</Text>
  //           </View>
  //       );
  //   };
  onPressBack() {
    Actions.Registereats();
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddWorkShiftChangeComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
