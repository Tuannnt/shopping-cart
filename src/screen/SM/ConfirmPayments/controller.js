import {API_ApplyOverTimes, API_ConfirmPayments} from '@network';
import {ErrorHandler} from '@error';

export default {
  // List KH
  getAllCustomer(callback) {
    API_ConfirmPayments.GetCustomerAll()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
      });
  },
  getAllWorkShift(callback) {
    API_ApplyOverTimes.getAllWorkShift()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getItem(value, callback) {
    API_ConfirmPayments.getItem(value)
      .then(res => {
        console.log(res);
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getAllEmployee(callback) {
    API_ApplyOverTimes.GetEmp()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
};
