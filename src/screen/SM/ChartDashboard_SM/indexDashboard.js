import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  FlatList,
  Dimensions,
} from 'react-native';
import {ListItem, Text, Body} from 'native-base';
import {Icon, Divider} from 'react-native-elements';
import {FlatGrid} from 'react-native-super-grid';
import API_Orders from '../../../network/SM/API_Orders';
import API_HomeSM from '../../../network/SM/API_HomeSM';
import TabBar from '../../component/TabBar';
import {ECharts} from 'react-native-echarts-wrapper';
import {AppStyles, AppColors} from '@theme';
import {Actions} from 'react-native-router-flux';

var width = Dimensions.get('window').width; //full width
class indexDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      staticParam: {
        EmployeeGuid: '',
        JobTitleId: '',
        OrganizationGuid: '',
      },
      staticParamRequest: {
        EmployeeGuid: '',
        JobTitleId: '',
        OrganizationGuid: '',
      },
      isHidden: false,
      isHiddenRequest: false,
      listDepartment: [],
      listDataActualSalary: [],
      list: [],
      listRequest: [],
      ListCountNoti: [],
      listMenu: [
        {
          Title: 'Số báo giá chờ duyệt',
          Icon: 'restore',
          type: 'materialicons',
          count: 0,
          code: 'A1',
          action: 'Quotations',
        },
        {
          Title: 'Số báo giá chưa ĐH ',
          Icon: 'file-replace-outline',
          type: 'material-community',
          count: 0,
          code: 'A2',
          action: 'Quotations',
        },
        {
          Title: 'Giá trị báo giá',
          Icon: 'money',
          type: 'font-awesome',
          count: 0,
          code: 'A3',
          action: 'Quotations',
        },
        {
          Title: 'Số đơn',
          Icon: 'format-list-numbered-rtl',
          type: 'material-community',
          count: 0,
          code: 'A4',
          action: '',
          // action: 'Orders',
        },
        {
          Title: 'Số đơn bị hủy',
          Icon: 'file-remove',
          type: 'material-community',
          count: 0,
          code: 'A5',
          action: '',
          // action: 'Orders',
        },
        {
          Title: 'Giá trị đơn hàng',
          Icon: 'cash-usd',
          type: 'material-community',
          count: 0,
          code: 'A6',
          action: '',
          // action: 'Orders',
        },
        {
          Title: 'Doanh thu',
          Icon: 'logo-usd',
          type: 'ionicon',
          count: 0,
          code: 'A7',
        },
        {
          Title: 'Lợi nhuận thực ',
          Icon: 'home-currency-usd',
          type: 'material-community',
          count: 0,
          code: 'A8',
        },
      ],
      optionPie: {
        // title: {
        //     text: 'ABC',
        //     //subtext: '纯属虚构',
        //     left: 'center'
        // },
        tooltip: {
          trigger: 'item',
          formatter: '{b}: {c} ({d}%)',
        },
        legend: {
          orient: 'vertical',
          left: 'center',
          bottom: 50,
          data: [],
        },
        series: [
          {
            name: 'Số lượng',
            type: 'pie',
            radius: '55%',
            center: ['50%', '30%'],
            data: [],
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)',
              },
            },
          },
        ],
      },
      optionLine: {
        grid: {
          left: '15%',
        },
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c}',
        },
        legend: {
          left: 'left',
          data: ['Tổng số lượng', 'Kết quả đã giao'],
        },
        xAxis: {
          type: 'category',
          data: [],
        },
        yAxis: {
          type: 'value',
        },
        series: [
          {
            name: 'Tổng số lượng',
            data: [],
            type: 'line',
            smooth: true,
          },
          {
            name: 'Kết quả đã giao',
            data: [],
            type: 'line',
            smooth: true,
          },
        ],
      },
    };
  }

  loadMoreData() {
    this.nextPage();
  }
  componentDidMount() {
    this.GetAcount();
    // this.Get_HomeReportOrders();
    // this.Get_HomeReportQuotations();
    // this.GetDataChartPie();
    // this.GetDataChartLine();
  }
  GetDataChartPie() {
    API_HomeSM.GetReportCompleteOrdersPie({})
      .then(res => {
        var _option = this.state.optionPie;
        var data = JSON.parse(res.data.data);
        var seriesData = [];
        for (let i = 0; i < data.length; i++) {
          seriesData.push({value: data[i].Quantity, name: data[i].ObjectName});
          _option.legend.data.push(data[i].ObjectName);
        }
        _option.series[0].data = seriesData;
        this.setState({optionPie: _option});
      })
      .catch(error => {
        console.log(error);
      });
  }
  GetDataChartLine() {
    API_HomeSM.GetReportCompleteOrdersSerial({})
      .then(res => {
        var _option = this.state.optionLine;
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
          _option.xAxis.data.push(data[i].DateString);
          _option.series[0].data.push(data[i].QuantityRequire);
          _option.series[1].data.push(data[i].QuantityOK);
        }
        this.setState({optionLine: _option});
      })
      .catch(error => {
        console.log(error);
      });
  }
  GetAcount() {
    API_Orders.GetHomeReport()
      .then(res => {
        var listCount = JSON.parse(res.data.data);
        this.setState({ListCountNoti: listCount});
        var _listMenu = this.state.listMenu;
        for (var i = 0; i < _listMenu.length; i++) {
          if (_listMenu[i].code == 'A1') {
            _listMenu[i].count = listCount.Quotations;
          } else if (_listMenu[i].code == 'A2') {
            _listMenu[i].count = listCount.QuotationsOrders;
          } else if (_listMenu[i].code == 'A3') {
            _listMenu[i].count = this.addPeriod(
              listCount.QuotationsValue.toFixed(0),
            );
          } else if (_listMenu[i].code == 'A4') {
            _listMenu[i].count = listCount.Orders;
          } else if (_listMenu[i].code == 'A5') {
            _listMenu[i].count = listCount.OrdersCancel;
          } else if (_listMenu[i].code == 'A6') {
            _listMenu[i].count = this.addPeriod(
              listCount.OrdersValue.toFixed(0),
            );
          } else if (_listMenu[i].code == 'A7') {
            _listMenu[i].count = this.addPeriod(listCount.Revenue);
          } else if (_listMenu[i].code == 'A8') {
            _listMenu[i].count = this.addPeriod(listCount.Profit.toFixed(0));
          }
        }
        this.setState({
          listMenu: _listMenu,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  Get_HomeReportOrders = () => {
    API_Orders.GetHomeReportOrders(this.state.staticParam)
      .then(res => {
        this.setState({list: JSON.parse(res.data.data)});
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  Get_HomeReportQuotations = () => {
    API_Orders.GetHomeReportQuotations(this.state.staticParamRequest)
      .then(res => {
        this.setState({listRequest: JSON.parse(res.data.data)});
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  ListEmpty = () => {
    if (this.state.list.length > 0 && this.state.listRequest.length > 0)
      return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null && strDate != '' && strDate != undefined) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  handlePress = link => {
    if (!link) {
      return;
    }
    Actions[link]();
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar title={'Dashboard'} BackModuleByCode={'SM'} />
        <ScrollView style={{flex: 1, flexDirection: 'column'}}>
          <View style={{flex: 3}}>
            <ScrollView>
              <FlatGrid
                itemDimension={width / 3}
                items={this.state.listMenu}
                style={styles.gridView}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.handlePress(item.action);
                    }}
                    style={{
                      borderRadius: 10,
                      backgroundColor: '#FFFFFF',
                      borderColor: AppColors.Maincolor,
                      borderWidth: 1,
                    }}>
                    <View style={[styles.itemContainerdetail]}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginBottom: 5,
                          marginTop: 5,
                        }}>
                        <Icon
                          name={item.Icon}
                          type={item.type}
                          size={20}
                          color={AppColors.Maincolor}
                        />
                      </View>
                      <View
                        style={{
                          marginBottom: 5,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {
                              textAlign: 'center',
                              alignItems: 'center',
                              justifyContent: 'center',
                            },
                          ]}>
                          {item.Title}
                        </Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            fontSize: 16,
                            textAlign: 'center',
                          }}>
                          {item.count}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </ScrollView>
          </View>
          {/* <View style={{flexDirection: 'row', paddingRight: 10}}>
            <View style={{flex: 10}}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                  marginLeft: 10,
                  flex: 1,
                }}>
                Danh sách đơn hàng đến hạn
              </Text>
            </View>
            <View style={{flex: 1}}>
              <Icon
                color="black"
                name={'chevron-thin-down'}
                type="entypo"
                size={20}
                onPress={() =>
                  this.state.isHidden == false
                    ? this.setState({isHidden: true})
                    : this.setState({isHidden: false})
                }
              />
            </View>
          </View>
          <View
            style={[
              this.state.isHidden == true ? styles.hidden : '',
              {padding: 10, backgroundColor: '#fff', flex: 1},
            ]}>
            <FlatList
              style={{marginBottom: 20, height: '100%'}}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              ListFooterComponent={this.renderFooter}
              data={this.state.list}
              renderItem={({item, index}) => (
                <ScrollView>
                  <ListItem>
                    <Body>
                      <View>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{flex: 2}}>
                            <Text
                              style={{
                                fontSize: 14,
                                marginLeft: 5,
                                fontWeight: 'bold',
                              }}>
                              {item.ObjectName}
                            </Text>
                          </View>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                          <Text style={{fontSize: 14}}>Số ĐH :</Text>
                          <Text style={{fontSize: 14, marginLeft: 5}}>
                            {item.OrderNumber}
                          </Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                          <Text style={{fontSize: 14}}>Ngày đặt </Text>
                          <View style={styles.FormALl}>
                            <Text style={{fontSize: 14, marginLeft: 5}}>
                              {this.customDate(item.OrderDate)}
                            </Text>
                          </View>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                          <Text style={{fontSize: 14}}>Tổng tiền </Text>
                          <View style={styles.FormALl}>
                            <Text style={{fontSize: 14, marginLeft: 5}}>
                              {this.addPeriod(item.TotalOfMoney)}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </Body>
                  </ListItem>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          </View>
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <View style={{flex: 10}}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                  marginLeft: 10,
                  flex: 1,
                }}>
                Danh sách báo giá
              </Text>
            </View>
            <View style={{flex: 1}}>
              <Icon
                color="black"
                name={'chevron-thin-down'}
                type="entypo"
                size={20}
                onPress={() =>
                  this.state.isHiddenRequest == false
                    ? this.setState({isHiddenRequest: true})
                    : this.setState({isHiddenRequest: false})
                }
              />
            </View>
          </View>
          <View
            style={[
              this.state.isHiddenRequest == true ? styles.hidden : '',
              {padding: 10, backgroundColor: '#fff', flex: 1},
            ]}>
            <FlatList
              style={{marginBottom: 20, height: '100%'}}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              ListFooterComponent={this.renderFooter}
              data={this.state.listRequest}
              renderItem={({item, index}) => (
                <ScrollView>
                  <ListItem>
                    <Body>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={{fontSize: 14, fontWeight: 'bold'}}>
                          Số báo giá :
                        </Text>
                        <Text style={{fontSize: 14, marginLeft: 5}}>
                          {item.QuotationNo}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Text style={{fontSize: 14, fontWeight: 'bold'}}>
                          Ngày{' '}
                        </Text>
                        <View style={styles.FormALl}>
                          <Text style={{fontSize: 14, marginLeft: 5}}>
                            {this.customDate(item.QuotationDate)}
                          </Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Text style={{fontSize: 14, fontWeight: 'bold'}}>
                          Tổng tiền{' '}
                        </Text>
                        <View style={styles.FormALl}>
                          <Text style={{fontSize: 14, marginLeft: 5}}>
                            {this.addPeriod(item.TotalNoVAT)}
                          </Text>
                        </View>
                      </View>
                    </Body>
                  </ListItem>
                </ScrollView>
              )}
              onEndReachedThreshold={0.5}
            />
          </View>
          <Divider style={{marginBottom: 10}} />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ backgroundColor: '#fff', flex: 1 }}>
                        <Text style={[AppStyles.Titledefault, { textAlign: 'center' }]}>BÁO CÁO HOÀN THÀNH PO ĐẾN HIỆN TẠI</Text>
                        <View style={{ height: 500, width: width, paddingBottom: 20 }}>
                            {
                                (this.state.optionPie && this.state.optionPie.legend.data.length > 0 && this.state.optionPie.series[0].data.length > 0) ?
                                    <ECharts
                                        option={this.state.optionPie}
                                    >
                                    </ECharts>
                                    : null
                            }
                        </View>
                    </ScrollView>
                    <Divider />
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{backgroundColor: '#fff', flex: 1}}>
            <View style={{height: 400, width: width}}>
              {this.state.optionLine &&
              this.state.optionLine.series[0].data.length > 0 ? (
                <ECharts option={this.state.optionLine} />
              ) : null}
            </View>
          </ScrollView> */}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainerdetail: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  FormALl: {
    fontSize: 14,
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
  hidden: {display: 'none'},
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(indexDashboard);
