import React, {Component} from 'react';
import {
  Keyboard,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  FlatList,
  StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_POPaymentsCustomer} from '@network';
import {Actions} from 'react-native-router-flux';
import {Divider} from 'react-native-elements';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import {FuncCommon} from '../../../utils';
import moment from 'moment';
import Toast from 'react-native-simple-toast';

const headerTable = [
  {title: 'STT', width: 40},
  // {title: 'Số PO', width: 200},
  // { title: 'Số SO', width: 200 },
  {title: 'Ngày tháng', width: 200},
  {title: 'Mã Đặt Hàng', width: 200},
  {title: 'Tên Đặt Hàng', width: 200},
  {title: 'ĐVT', width: 200},
  {title: 'Thành tiền', width: 200},
  // { title: '% Thanh toán', width: 200 },
  {title: 'Thành tiền TT', width: 200},
  {title: 'Chứng từ kèm theo', width: 200},
  {title: 'Số SO', width: 200},
  // {title: 'GT đã TT', width: 200},
  // {title: 'GT chưa TT', width: 200},
  // {title: 'Ghi chú', width: 200},
];
const list = [
  {width: 40},
  {width: 200},
  // { width: 200 },
  {width: 200},
  {width: 200},
  {width: 200},
  // {width: 200},
  {width: 200, id: 'total'},
  // { width: 200 },
  {width: 200, id: 'totalTT'},
  {width: 200},
  // {width: 200, id: 'gt'},
  // {width: 200},
  {width: 200},
  // {width: 200},
];
const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
  totalText: {
    fontWeight: 'bold',
  },
});
class GetPOPaymentsBeforeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [],
      Type: 'A',
      Data: null,
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      Quantity: '',
      viewId: '',
      Type: '',
    };
  }

  componentDidMount = () => {
    Promise.all([this.getItem()]);
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }
  onClickBack() {
    Actions.pop();
  }
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_POPaymentsCustomer.Delete(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            RegisterEatGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);

          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  };
  getItem() {
    let viewId = '';
    if (this.props.RecordGuid !== undefined) {
      viewId = this.props.RecordGuid;
    } else {
      viewId = this.props.viewId;
    }
    API_POPaymentsCustomer.GetItems(this.props.RecordGuid)
      .then(res => {
        let data = JSON.parse(res.data.data);
        let _data = data.model;
        let rows = data.detail;
        let checkin = {
          PopaymentGuid: this.props.RecordGuid,
          WorkFlowGuid: _data.WorkFlowGuid,
        };
        API_POPaymentsCustomer.CheckLogin(checkin)
          .then(res => {
            this.setState({
              checkInLogin: res.data.data,
              Data: _data,
              rows,
              viewId,
              LoginName: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
            });
          })
          .catch(error => {
            console.log(error.data);
          });
      })
      .catch(error => {
        console.log(error);
      });
  }
  helperPaymentType = type => {
    if (!type) {
      return '';
    }
    switch (type) {
      case '1':
        return 'Tiền mặt';
      case '2':
        return 'Chuyển khoản cá nhân';
      case '3':
        return 'Chuyển khoản';
      default:
        return '';
    }
  };
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_POPaymentsCustomer.Delete(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            RegisterEatGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);

          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.PurchaseOrderId}
          </Text>
        </TouchableOpacity> */}
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, { width: 200 }]}>
          <Text style={[AppStyles.TextTable, { textAlign: 'center' }]}>
            {' '}
            {row.OrderNumber}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {moment(row.Date).format('DD/MM/YYYY')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.ItemId}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.ItemName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.UnitName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'right'}]}>
            {' '}
            {FuncCommon.addPeriod(row.Amount)}
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, { width: 200 }]}>
          <Text style={[AppStyles.TextTable, { textAlign: 'center' }]}>
            {' '}
            {FuncCommon.addPeriod(row.RatePayment)}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'right'}]}>
            {' '}
            {FuncCommon.addPeriod(row.AmountPayment)}
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.TicketNumber}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {FuncCommon.addPeriod(row.TotalTicket)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {FuncCommon.addPeriod(row.TotalPaymentPOYes)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {FuncCommon.addPeriod(row.TotalPaymentPONo)}
          </Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Description}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.SaleOrderNo}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  CustomViewDetailRoutings = () => {
    return (
      <View style={{flex: 1}}>
        <View style={{padding: 15}}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Số phiếu :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {this.state.Data.PopaymentId}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Nhân viên :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {this.state.Data.EmployeeName}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Bộ phận :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {this.state.Data.DepartmentName}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Đơn vị nhận :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {this.state.Data.SupplierName}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Hình thức :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {this.helperPaymentType(this.state.Data.PaymentType)}
              </Text>
            </View>
          </View>

          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Ngày thanh toán :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {moment(this.state.Data.ReceviedDate).format('DD/MM/YYYY')}
              </Text>
            </View>
          </View>

          {/* {this.state.Data.FinishDate && (
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>
                  Ngày hoàn thành thực tế :
                </Text>
              </View>
              <View style={{ flex: 3 }}>
                              <Text style={[AppStyles.Textdefault, {textAlign: 'left', paddingLeft: 10}]}>

                  {moment(this.state.Data.FinishDate).format('DD/MM/YYYY')}
                </Text>
              </View>
            </View>
          )} */}

          {/*Ngày tạo phiếu*/}
          {/* <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Ngày yêu cầu :</Text>
            </View>
            <View style={{ flex: 3 }}>
                            <Text style={[AppStyles.Textdefault, {textAlign: 'left', paddingLeft: 10}]}>

                {moment(this.state.Data.RequireDate).format('DD/MM/YYYY')}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', padding: 5 }}>
            <View style={{ flex: 1 }}>
              <Text style={AppStyles.Labeldefault}>Loại sản phẩm :</Text>
            </View>
            <View style={{ flex: 3 }}>
                            <Text style={[AppStyles.Textdefault, {textAlign: 'left', paddingLeft: 10}]}>

                {this.state.Data.ProductGroupId === 1
                  ? 'Thương mại'
                  : 'Sản xuất'}
              </Text>
            </View>
          </View> */}

          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Lý do thanh toán:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {this.state.Data.Reason}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Giá trị thanh toán:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {FuncCommon.addPeriod(this.state.Data.AmountPayment)}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Tổng tiền:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text
                style={[
                  AppStyles.Textdefault,
                  {textAlign: 'left', paddingLeft: 10},
                ]}>
                {FuncCommon.addPeriod(this.state.Data.Total)}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
            </View>
            <View style={{flex: 3}}>
              {this.state.Data.Status === 'Y' ? (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {
                      justifyContent: 'center',
                      color: AppColors.AcceptColor,
                      textAlign: 'left',
                      paddingLeft: 10,
                    },
                  ]}>
                  {this.state.Data.StatusWF}
                </Text>
              ) : (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {
                      justifyContent: 'center',
                      color: AppColors.PendingColor,
                      textAlign: 'left',
                      paddingLeft: 10,
                    },
                  ]}>
                  {this.state.Data.StatusWF}
                </Text>
              )}
            </View>
          </View>
        </View>
        <Text style={{fontSize: 16, fontWeight: 'bold', padding: 10}}>
          Chi tiết phiếu
        </Text>
        <ScrollView horizontal={true}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              {headerTable.map(item => {
                if (item.hideInDetail) {
                  return null;
                }
                return (
                  <TouchableOpacity
                    key={item.title}
                    style={[AppStyles.table_th, {width: item.width}]}>
                    <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <View>
                  <FlatList
                    data={this.state.rows}
                    renderItem={({item, index}) => {
                      return this.itemFlatList(item, index);
                    }}
                    keyExtractor={(rs, index) => index.toString()}
                  />
                  {this.rowTotal()}
                </View>
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };

  CustomView = item => (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TabBar_Title
        title={'Đề nghị thanh toán'}
        callBack={() => this.callBackList()}
      />
      <Divider />
      {/*hiển thị nội dung chính*/}
      <ScrollView>{this.CustomViewDetailRoutings()}</ScrollView>
      {/*nút xử lý*/}
      <View style={{maxHeight: 50}}>
        {this.state.checkInLogin && (
          <TabBarBottom
            onDelete={() => this.Delete()}
            // tiêu đề hiển thị trong popup xử lý phiếu
            Title="Đề nghị tạm ứng"
            //kiểm tra quyền xử lý
            Permisstion={this.state.Data.Permisstion}
            //kiểm tra bước đầu quy trình
            checkInLogin={this.state.checkInLogin}
            onAttachment={() => this.openAttachments()}
            onSubmitWF={(callback, CommentWF) =>
              this.submitWorkFlow(callback, CommentWF)
            }
            keyCommentWF={{
              ModuleId: 25,
              RecordGuid: this.props.RecordGuid,
              Title: this.state.Data.PopaymentId,
              // Type: 1,
            }}
          />
        )}
      </View>
    </View>
  );
  openAttachments() {
    var obj = {
      ModuleId: 4,
      RecordGuid: this.props.RecordGuid,
      notEdit: this.state.checkInLogin !== '1' ? true : false,
    };
    Actions.attachmentComponent(obj);
  }
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  callbackOpenUpdate = () => {
    Actions.AddQuotationRequest({
      itemData: this.state.Data,
      rowData: this.state.rows,
    });
  };
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  rowTotal = () => {
    const {rows} = this.state;
    return (
      <View style={{flexDirection: 'row'}}>
        {list.map((item, index) => {
          if (item.id === 'totalTT') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    styles.totalText,
                    {textAlign: 'right', fontWeight: 'bold'},
                  ]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.AmountPayment || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          if (item.id === 'total') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    styles.totalText,
                    {textAlign: 'right', fontWeight: 'bold'},
                  ]}>
                  {FuncCommon.addPeriod(
                    rows.reduce((total, row) => total + (+row.Amount || 0), 0),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          if (item.id === 'gt') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    styles.totalText,
                    {textAlign: 'right', fontWeight: 'bold'},
                  ]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.TotalTicket || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          return (
            <TouchableOpacity
              key={index}
              style={[
                AppStyles.table_td_custom,
                {width: item.width, backgroundColor: '#f5f2f2'},
              ]}
            />
          );
        })}
      </View>
    );
  };
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    var obj = {
      PopaymentGuid: this.state.viewId,
      Comment: CommentWF,
      WorkFlowGuid: this.state.Data.WorkFlowGuid,
    };
    if (callback == 'D') {
      API_POPaymentsCustomer.Approve(obj)
        .then(res => {
          if (res.data.errorCode !== 200) {
            Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);
            this.getItem();
          } else {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          }
        })
        .catch(error => {
          console.log(error.data);
        });
    } else {
      API_POPaymentsCustomer.NotApprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity(res.data.message, Toast.SHORT, Toast.CENTER);

            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data);
        });
    }
  }
}

//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetPOPaymentsBeforeComponent);
