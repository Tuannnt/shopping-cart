import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  CheckBox,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, Icon, ListItem, SearchBar} from 'react-native-elements';
import {API_OrderDetailsSX} from '@network';
import TabBar from '../../component/TabBar';
import Fonts from '../../../theme/fonts';
import {FuncCommon} from '../../../utils';
import Combobox from '../../component/Combobox';
import PopupHandleConfirmCommon from '../../component/PopupHandleConfirmCommon';
import {AppStyles, AppColors} from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import controller from './controller';
import { ToastAndroid } from 'react-native';
import Toast from 'react-native-simple-toast';

class OrderDetailsSXComponent extends Component {
  constructor(props) {
    super(props);
    //khai báo biến
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      refreshing: false,
      Data: null,
      ListAll: [],
      ListCountNoti: [],
      EvenFromSearch: false,
      selectedTab: 'profile',
    };
    // dữ liệu trueyenf vao APi
    this._search = {
      GroupItemId: null,
      LoginName: null,
      OrderNumber: null,
      OrderTypeId: null,
      StatusYCSX: 0,
      NumberPage: 0,
      search: {value: ''},
      Length: 10,
      QueryOrderBy: 'Date DESC',
      CustomerId: null,
      Draw: 1,
    };
    this.Total = 0;
    this.onEndReachedCalledDuringMomentum = true;
    this.listtabbarBotom = [
      {
        Title: 'Chưa YCSX',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã YCSX',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
    this.btns = [
      {
        name: 'Xác nhận',
        value: 'D',
        color: '#3366CC',
      },
      {
        name: 'Đóng',
        value: 'H',
        color: '#979797',
      },
    ];
  }
  componentDidMount = () => {
    Promise.all([this.nextPage('reload'), this.getAllCustomer()]);
  };
  getAllCustomer = () => {
    controller.getAllCustomer(rs => {
      let ListCustomer = JSON.parse(rs).map(i => ({
        value: i.CustomerGuid,
        text: `${i.CustomerId} - ${i.CustomerName}`,
      }));
      ListCustomer.unshift({
        value: null,
        text: 'Tất cả',
      });
      this.setState({ListCustomer});
    });
  };
  // khi trả lại trang,thì load vào danh sách
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.nextPage('reload');
    }
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.search.value = search;
    this._search.NumberPage = 0;
    this.nextPage('reload');
  };

  //List danh sách phân trang
  nextPage(type) {
    this.setState({refreshing: true}, () => {
      if (type === 'reload') {
        this._search.NumberPage = 1;
      } else {
        this._search.NumberPage++;
      }
      API_OrderDetailsSX.GetAll(this._search)
        .then(res => {
          var _listall = [];
          let _data = JSON.parse(res.data.data).data;
          if (type === 'reload') {
            _listall = _data;
          } else {
            _listall = this.state.ListAll.concat(_data);
          }
          this.Total = JSON.parse(res.data.data).recordsTotal;
          this.setState({
            ListAll: _listall,
            refreshing: false,
          });
        })
        .catch(error => {
          console.log(error);
          this.setState({
            refreshing: false,
          });
        });
    });
  }

  // khi kéo danh sách,thì phân trang
  _openComboboxEmployee() {}
  openComboboxEmployee = d => {
    this._openComboboxEmployee = d;
  };
  onActionComboboxEmployee() {
    this._openComboboxEmployee();
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'profile':
        this.onChoDuyet();
        break;
      case 'checkcircleo':
        this.onDaDuyet();
        break;
      default:
        break;
    }
  }
  // sự kiện chờ duyệt
  onChoDuyet = () => {
    this._search.StatusYCSX = 0;
    this.nextPage('reload');
  };
  // sự kiện đã  duyệt
  onDaDuyet = () => {
    this._search.StatusYCSX = 1;
    this.nextPage('reload');
  };
  handleCheckbox = id => {
    this.setState({currentItem: id, showModalConfirm: true});
  };
  // view hiển thị
  CustomeListAll = itemList => (
    <View style={{flex: 30}}>
      <FlatList
        data={itemList}
        ListEmptyComponent={this.ListEmpty}
        style={{height: '100%', marginBottom: 10}}
        renderItem={({item, index}) => {
          return (
            <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
              <ListItem
                subtitle={() => {
                  return (
                    <View style={{flexDirection: 'row', marginTop: -25}}>
                      <View
                        style={{
                          flex: 0.5,
                          marginRight: 5,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <CheckBox
                          style={{alignSelf: 'center'}}
                          onValueChange={() => {
                            this.setState({
                              ListAll: itemList.map(x => {
                                if (
                                  x.ItemIdByProvider === item.ItemIdByProvider
                                ) {
                                  x.StatusYCSX = !item.StatusYCSX;
                                }
                                return x;
                              }),
                            });
                            this.handleCheckbox(item.OrderDetailGuid);
                          }}
                          disabled={item.StatusYCSX}
                          value={item.StatusYCSX}
                        />
                      </View>
                      <View style={{flex: 3, flexDirection: 'row'}}>
                        <View>
                          <Text
                            style={[
                              AppStyles.Titledefault,
                              {
                                //color: '#2E77FF',
                              },
                            ]}>
                            {item.ItemIdByProvider}
                          </Text>
                          <Text style={AppStyles.Textdefault}>
                            Khách hàng: {item.ObjectName}
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flex: 1.5,
                          justifyContent: 'center',
                          alignItems: 'flex-end',
                        }}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'right', justifyContent: 'center'},
                          ]}>
                          {item.ItemName}
                        </Text>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {
                              textAlign: 'right',
                              justifyContent: 'center',
                              marginTop: 2,
                              fontWeight: 'bold',
                            },
                          ]}>
                          {FuncCommon.addPeriod(item.Amount)}
                        </Text>
                      </View>
                    </View>
                  );
                }}
                bottomDivider
                //chevron
                onPress={() => {
                  return;
                }}
                er
              />
            </View>
          );
        }}
        onEndReachedThreshold={0.5}
        onMomentumScrollBegin={() => {
          this.onEndReachedCalledDuringMomentum = false;
        }}
        onEndReached={() => this._onEndReached()}
        keyExtractor={(item, index) => item.OrderDetailGuid.toString() + index}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
    </View>
  );
  _onEndReached = () => {
    if (this.Total > this.state.ListAll.length) {
      this.nextPage();
    }
  };
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <TabBar
            title={'Đơn hàng sản xuất'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            BackModuleByCode={'HR'}
          />
          <Divider />
          <View>
            {this.state.EvenFromSearch == true ? (
              <View style={{flexDirection: 'column'}}>
                <TouchableOpacity
                  style={[
                    AppStyles.FormInput,
                    {marginHorizontal: 10, marginTop: 10},
                  ]}
                  onPress={() => {
                    this.onActionComboboxEmployee();
                  }}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this._search.employee
                        ? {color: 'black'}
                        : {color: AppColors.gray},
                    ]}>
                    {this._search.ObjectName
                      ? FuncCommon.handleLongText(this._search.ObjectName, 35)
                      : 'Chọn khách hàng...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      color={AppColors.gray}
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{backgroundColor: '#e1ecf4'}}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={search => this.updateSearch(search)}
                  value={this._search.search.value}
                />
              </View>
            ) : null}
          </View>

          <View style={{flex: 20}}>
            {this.state.ListAll
              ? this.CustomeListAll(this.state.ListAll)
              : null}
          </View>

          {this.state.showModalConfirm && (
            <PopupHandleConfirmCommon
              title={'Bạn có chắc chắn muốn yêu cầu sản xuất ?'}
              submit={this.handleSubmitConfirm}
              btns={this.btns}
            />
          )}
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.onCallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.ListCustomer && (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeCustomer}
              data={this.state.ListCustomer}
              nameMenu={'Chọn khách hàng'}
              eOpen={this.openComboboxEmployee}
              position={'bottom'}
              value={''}
            />
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }
  handleSubmitConfirm = value => {
    if (value === 'D') {
      this.setState({showModalConfirm: false}, () => {
        this.handleConfirm();
      });
    }
    if (value === 'H') {
      this.setState(
        {
          currentItem: null,
          showModalConfirm: false,
        },
        () => {
          this.nextPage('reload');
        },
      );
    }
  };
  handleConfirm = () => {
    let OrderDetailGuid = this.state.currentItem;
    let obj = {
      OrderDetailGuid,
      StatusYCSX: true,
      IsConfirm: undefined,
    };
    this.setState({loading: true}, () => {
      API_OrderDetailsSX.CheckBox(obj)
        .then(response => {
          if (response.data.errorCode == 200) {
            this.setState({
              loading: false,
              currentItem: null,
              showModalConfirm: false,
            });
            Toast.showWithGravity(
              'Thành công',
              Toast.SHORT, Toast.CENTER);
          } else {
            Toast.showWithGravity(
              'Có lỗi',
              Toast.SHORT, Toast.CENTER);
            this.setState({
              loading: false,
            });
          }
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data.data);
        });
    });
    this.nextPage('reload');
  };
  ChangeCustomer = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === '') {
      return;
    }
    this._search.CustomerId = rs.value;
    this._search.ObjectName = rs.text;
    this.nextPage('reload');
  };
  ListEmpty = () => {
    if (this.state.ListAll && this.state.ListAll.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={[AppStyles.centerAligned, {marginTop: 10}]}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //view item,click sang view khác
  onViewItem(item) {
    Actions.OpenOrderDetailsSX({
      RecordGuid: item.OrderDetailGuid,
    });
  }
}
//custom giao diện
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '100',
    fontSize: 30,
    color: '#f6b801',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#CCCCCC',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  ClickApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#CCCCCC',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  timeHeader: {
    marginBottom: 3,
  },
});
const mapStateToProps = state => ({});

const mapDispatchToProps = {
  //GetUserName: state.user.getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(OrderDetailsSXComponent);
