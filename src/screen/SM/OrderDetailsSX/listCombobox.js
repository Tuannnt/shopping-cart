export default {
    headerTable: [
      {title: 'STT', width: 40},
      {title: 'Mã sản phẩm', width: 220},
      {title: 'Tên sản phẩm', width: 220},
      {title: 'ĐVT', width: 100},
      {title: 'Số lượng', width: 100},
      {title: 'Đơn giá PO', width: 220},
      {title: 'Tổng tiền PO', width: 220},
      {title: 'VAT', width: 100},
      {title: 'Tổng thành tiền', width: 220},
      {title: 'Ghi chú', width: 150},
    ],
  Ration: [
    {
      value: 'A',
      label: 'Xuất ăn 12k',
    },
    {
      value: 'B',
      label: 'Xuất ăn 15k',
    },
    {
      value: 'C',
      label: 'Xuất ăn 28k',
    },
  ],
};
