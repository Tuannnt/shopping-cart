import {API_ApplyOverTimes, API_OrderDetailsSX} from '@network';
import {ErrorHandler} from '@error';

export default {
  // List KH
  getAllCustomer(callback) {
    let obj = {
      IsProvider: false,
      IsCustomer: false,
      IsEmployee: true,
    };
    API_OrderDetailsSX.GetCustomerAll(obj)
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
      });
  },
  getAllWorkShift(callback) {
    API_ApplyOverTimes.getAllWorkShift()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getItem(value, callback) {
    API_OrderDetailsSX.getItem(value)
      .then(res => {
        console.log(res);
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
  getAllEmployee(callback) {
    API_ApplyOverTimes.GetEmp()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  },
};
