import React, {Component} from 'react';
import {
  Keyboard,
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_InvoiceOrders} from '@network';
import {Actions} from 'react-native-router-flux';
import {Divider, Button, Icon} from 'react-native-elements';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import moment from 'moment';
import listCombobox from './listCombobox';
import {FuncCommon} from '../../../utils';
import Toast from 'react-native-simple-toast';

const headerTable = listCombobox.headerTable;
class OpenOrderDetailsSXComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [],
      Type: 'A',
      Data: null,
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      Quantity: '',
      viewId: '',
      Type: '',
    };
  }

  componentDidMount = () => {
    Promise.all([this.getItem()]);
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }
  onClickBack() {
    Actions.pop();
  }
  onPressBack() {
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_InvoiceOrders.Delete(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            RegisterEatGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  getItem() {
    let viewId = '';
    if (this.props.RecordGuid) {
      viewId = this.props.RecordGuid;
    } else {
      viewId = this.props.viewId;
    }
    API_InvoiceOrders.GetById(this.props.RecordGuid)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        API_InvoiceOrders.GetDetails(this.props.RecordGuid)
          .then(res => {
            let rows = JSON.parse(res.data.data);
            this.setState({
              Data: _data,
              viewId,
              LoginName: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
              rows,
            });
          })
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log(error);
      });
  }
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 220}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {row.ItemIdbyProvider}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 220}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.ItemName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.UnitName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Quantity}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 220}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {FuncCommon.addPeriod(row.UnitPrice)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 220}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {FuncCommon.addPeriod(+row.UnitPrice * +row.Quantity)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {FuncCommon.addPeriod(row.Vat)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 220}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {FuncCommon.addPeriod(+row.UnitPrice * +row.Quantity - +row.Vat)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Note}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  CustomViewDetailRoutings = () => {
    return (
      <View style={{flex: 1}}>
        <View style={{padding: 15}}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Số đề nghị :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.RequestInvoiceNo}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Khách hàng :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.CustomerName}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Tổng tiền:</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault, {fontWeight: 'bold'}]}>
                {FuncCommon.addPeriod(this.state.Data.TotalWithVat)}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Ngày đề nghị :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {moment(this.state.Data.RequirementDate).format('DD/MM/YYYY')}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1.5}}>
              <Text style={AppStyles.Labeldefault}>Mô tả :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.Description}
              </Text>
            </View>
          </View>
        </View>
        <Text style={{fontSize: 16, fontWeight: 'bold', padding: 10}}>
          Chi tiết phiếu
        </Text>
        <ScrollView horizontal={true}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              {headerTable.map(item => {
                if (item.hideInDetail) {
                  return null;
                }
                return (
                  <TouchableOpacity
                    key={item.title}
                    style={[AppStyles.table_th, {width: item.width}]}>
                    <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <FlatList
                  data={this.state.rows}
                  renderItem={({item, index}) => {
                    return this.itemFlatList(item, index);
                  }}
                  keyExtractor={(rs, index) => index.toString()}
                />
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };

  CustomView = item => (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TabBar_Title
        title={'Chi tiết đề nghị xuất HĐ'}
        callBack={() => this.callBackList()}
      />
      <Divider />
      {/*hiển thị nội dung chính*/}
      <ScrollView>{this.CustomViewDetailRoutings()}</ScrollView>
      {/*nút xử lý*/}
      <View style={{maxHeight: 50}}>
        <TabBarBottom
          //key để quay trở lại danh sách
          onDelete={() => this.Delete()}
          isComplete={this.state.Data.Status === 'Y' ? true : false}
          isDraff={this.state.Data.StatusWF === 'Nháp' ? true : false}
          haveEditButton={this.state.Data.PermissEdit === 1 ? true : false}
          callbackOpenUpdate={this.callbackOpenUpdate}
          backListByKey="Registereats"
          keyCommentWF={{
            ModuleId: 41,
            RecordGuid: this.state.viewId,
            Title: 'Phiếu đổi ca',
            getByGuid: true,
            //LoginName:this.state.Data.LoginName
          }}
          // tiêu đề hiển thị trong popup xử lý phiếu
          Title="Phiếu đổi ca"
          //kiểm tra quyền xử lý
          Permisstion={this.state.Data.Permisstion}
          //kiểm tra bước đầu quy trình
          checkInLogin={this.state.checkInLogin}
          onSubmitWF={(callback, CommentWF) =>
            this.submitWorkFlow(callback, CommentWF)
          }
        />
      </View>
    </View>
  );
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  callbackOpenUpdate = () => {
    Actions.AddWorkShiftChange({
      itemData: this.state.Data,
      rowData: this.state.rows,
    });
  };
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {}
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(OpenOrderDetailsSXComponent);
