import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_Compaigns from '../../../network/SM/API_Compaigns';
import API_Leads from '../../../network/SM/API_Leads';
import ErrorHandler from '../../../error/handler';
import API_Orders from '../../../network/SM/API_Orders';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';

class ItemCompaignsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      Data: [],
      ListQuotations: [],
      ListOrders: [],
      ListLeads: [],
      staticParamOrders: {
        CurrentPage: 1,
        Length: 25,
        Search: '',
        QueryOrderBy: 'OrderNumber',
        StartDate: '',
        EndDate: '',
        OrderStatus: [],
        CustomerId: null,
        Type: null,
        OrderTypeID: null,
      },
      staticParamQuotations: {
        CurrentPage: 1,
        Status: '',
        NumberPage: 1,
        Length: 25,
        search: {
          value: '',
        },
        CompaignGuid: null,
      },
      staticParamLeads: {
        Page: 1,
        ItemPage: 25,
        Search: '',
        CompaignId: null,
        StartDate: '2020-02-01T02:13:13.779Z',
        EndDate: '2020-12-31T02:13:13.779Z',
      },
      tab1: true,
      tab2: false,
      tab3: false,
      tab4: false,
      loading: true,
    };
  }

  componentDidMount(): void {
    this.getItem();
  }

  getItem() {
    let id = this.props.RecordGuid;
    API_Compaigns.GetItem(id)
      .then(res => {
        this.state.Data = JSON.parse(res.data.data);
        if (JSON.parse(res.data.data).CompaignType === 'GG') {
          this.state.Data.CompaignType = 'Giảm giá';
        } else if (JSON.parse(res.data.data).CompaignType === 'TD') {
          this.state.Data.CompaignType = 'Tích điểm';
        } else if (JSON.parse(res.data.data).CompaignType === 'DQ') {
          this.state.Data.CompaignType = 'Đổi quà';
        } else if (JSON.parse(res.data.data).CompaignType === 'VC') {
          this.state.Data.CompaignType = 'Miễn phí vận chuyển';
        } else if (JSON.parse(res.data.data).CompaignType === 'KH') {
          this.state.Data.CompaignType = 'Khác';
        }
        this.setState({Data: this.state.Data});
        this.setState({
          staticParamOrders: {
            CurrentPage: this.state.staticParamOrders.CurrentPage,
            Length: this.state.staticParamOrders.Length,
            Search: this.state.staticParamOrders.Search,
            QueryOrderBy: this.state.staticParamOrders.QueryOrderBy,
            StartDate: this.state.staticParamOrders.StartDate,
            EndDate: this.state.staticParamOrders.EndDate,
            OrderStatus: this.state.staticParamOrders.OrderStatus,
            CustomerId: this.state.Data.CustomerId,
            CompaignId: this.state.Data.CompaignId,
            Type: this.state.staticParamOrders.Type,
            OrderTypeID: this.state.staticParamOrders.OrderTypeID,
          },
          staticParamQuotations: {
            CurrentPage: this.state.staticParamQuotations.CurrentPage,
            Status: this.state.staticParamQuotations.Status,
            NumberPage: this.state.staticParamQuotations.NumberPage,
            Length: this.state.staticParamQuotations.Length,
            search: {
              value: this.state.staticParamQuotations.search.value,
            },
            CompaignGuid: this.state.Data.CompaignGuid,
            CompaignId: this.state.Data.CompaignId,
          },
          staticParamLeads: {
            Page: this.state.staticParamLeads.Page,
            ItemPage: this.state.staticParamLeads.ItemPage,
            Search: '',
            CompaignId: this.state.Data.CompaignId,
            StartDate: '2020-02-01T02:13:13.779Z',
            EndDate: '2020-12-31T02:13:13.779Z',
          },
          loading: false,
        });
      })
      .catch(error => {
        loading: false;
        console.log(error.data.data);
      });
  }
  //Lấy danh sách báo giá
  GetQuotations = () => {
    API_Orders.QuotationsAM_ListAll(this.state.staticParamQuotations)
      .then(res => {
        this.state.ListQuotations =
          res.data.data != 'null'
            ? this.state.ListQuotations.concat(JSON.parse(res.data.data).data)
            : [];
        this.setState({ListQuotations: this.state.ListQuotations});
        this.setState({refreshing: false});
      })
      .catch(error => {
        console.log(error);
      });
  };
  //Lấy danh sách đơn hàng theo khách hàng
  GetOrders = () => {
    this.setState({refreshing: true});
    API_Orders.GetOrderAll(this.state.staticParamOrders)
      .then(res => {
        if (res.data.errorCode == 200) {
          var _lstOrders = this.state.ListOrders;
          var data = JSON.parse(res.data.data).data;
          for (let i = 0; i < data.length; i++) {
            _lstOrders.push(data[i]);
          }
          this.setState({ListOrders: _lstOrders});
          this.setState({refreshing: false});
        }
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };
  //Lấy danh sách cơ hội
  GetLeads = () => {
    API_Leads.GetLeads(this.state.staticParamLeads)
      .then(res => {
        this.state.ListLeads =
          res.data.data != 'null'
            ? this.state.ListLeads.concat(JSON.parse(res.data.data))
            : [];
        this.setState({ListLeads: this.state.ListLeads});
        this.setState({refreshing: false});
      })
      .catch(error => {
        console.log(error);
      });
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  nextPageQuotations() {
    this.state.staticParamQuotations.CurrentPage++;
    this.setState({
      staticParamQuotations: {
        CurrentPage: this.state.staticParamQuotations.CurrentPage,
        Status: this.state.staticParamQuotations.Status,
        NumberPage: this.state.staticParamQuotations.NumberPage,
        Length: this.state.staticParamQuotations.Length,
        search: {
          value: this.state.staticParamQuotations.search.value,
        },
        CompaignId: this.state.staticParamQuotations.CompaignId,
      },
    });
    this.GetQuotations();
  }
  nextPageOrders() {
    this.state.staticParamOrders.CurrentPage++;
    this.setState({
      staticParamOrders: {
        CurrentPage: this.state.staticParamOrders.CurrentPage,
        Length: this.state.staticParamOrders.Length,
        Search: this.state.staticParamOrders.Search,
        QueryOrderBy: this.state.staticParamOrders.QueryOrderBy,
        StartDate: this.state.staticParamOrders.StartDate,
        EndDate: this.state.staticParamOrders.EndDate,
        OrderStatus: this.state.staticParamOrders.OrderStatus,
        CustomerId: this.state.staticParamOrders.CustomerId,
        Type: this.state.staticParamOrders.Type,
        CompaignId: this.state.staticParamOrders.CompaignId,
        OrderTypeID: this.state.staticParamOrders.OrderTypeID,
      },
    });
    this.GetOrders();
  }
  nextPageLeads() {
    this.state.staticParamLeads.Page++;
    this.setState({
      staticParamLeads: {
        Page: this.state.staticParamLeads.Page,
        ItemPage: this.state.staticParamLeads.ItemPage,
        Search: this.state.staticParamLeads.Search,
        CompaignId: this.state.Data.CompaignId,
        StartDate: this.state.staticParamLeads.StartDate,
        EndDate: this.state.staticParamLeads.EndDate,
      },
    });
    this.GetLeads();
  }
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết chiến dịch'}
          callBack={() => this.onPressBack()}
          FormAttachment={false}
          CallbackFormAttachment={() => this.openAttachments()}
        />
        <View
          style={{
            marginLeft: 10,
            marginRight: 10,
            borderRadius: 10,
            flex: 1,
            borderWidth: 0.5,
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => this.OnChangeTab('1')}
            style={styles.TabarPending}>
            <Text
              style={
                this.state.tab1 === true ? {color: AppColors.Maincolor} : ''
              }>
              TT chung
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.OnChangeTab('2')}
            style={styles.TabAlight}>
            <Text
              style={
                this.state.tab2 === true ? {color: AppColors.Maincolor} : ''
              }>
              Báo giá
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.OnChangeTab('3')}
            style={styles.TabAlight}>
            <Text
              style={
                this.state.tab3 === true ? {color: AppColors.Maincolor} : ''
              }>
              Đơn hàng
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.OnChangeTab('4')}
            style={styles.TabarApprove}>
            <Text
              style={
                this.state.tab4 === true ? {color: AppColors.Maincolor} : ''
              }>
              Cơ hội
            </Text>
          </TouchableOpacity>
        </View>
        {/* Thông tin chung */}
        <View style={{flex: 15}}>
          {this.state.tab1 === true ? (
            this.CustomViewTTChung()
          ) : this.state.tab2 == true &&
            this.state.ListQuotations.length > 0 ? (
            this.CustomViewQuotations(this.state.ListQuotations)
          ) : this.state.tab3 == true && this.state.ListOrders.length > 0 ? (
            this.CustomViewOrders(this.state.ListOrders)
          ) : this.state.tab4 == true && this.state.ListLeads.length > 0 ? (
            this.CustomViewLeads(this.state.ListLeads)
          ) : (
            <View style={styles.MainContainer}>
              <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
            </View>
          )}
        </View>
      </View>
    );
  }

  onPressBack() {
    Actions.pop();
  }
  openAttachments() {
    var obj = {
      ModuleId: '15',
      RecordGuid: this.state.Data.CompaignGuid,
    };
    Actions.attachmentComponent(obj);
  }
  //CustomView
  CustomViewTTChung = () => (
    <View>
      {/* Thông tin chung */}
      <View style={{flexDirection: 'row', padding: 10}}>
        <View style={{flex: 10}}>
          <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
        </View>
      </View>
      <View style={[{padding: 10, backgroundColor: '#fff'}]}>
        <View style={{paddingBottom: 5}}>
          <Text style={AppStyles.Labeldefault}>Tên chiến dịch :</Text>
          <Text style={AppStyles.Textdefault}>
            {this.state.Data.CompaignName}
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Mã chiến dịch :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Người quản lý :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Loại chiến dịch :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
              Loại hình Marketing :
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.CompaignId}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.props.ChoiseAssigned}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.CompaignType}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
              {this.state.Data.MarketingTypeId}
            </Text>
          </View>
        </View>
        <View style={{paddingBottom: 5}}>
          <Text style={AppStyles.Labeldefault}>Ghi chú :</Text>
          <Text style={AppStyles.Textdefault}>
            {this.state.Data.Description}
          </Text>
        </View>
      </View>
    </View>
  );
  CustomViewQuotations = DataQuotations => (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <FlatList
        refreshing={this.state.isFetching}
        ref={ref => {
          this.ListView_Ref = ref;
        }}
        ListFooterComponent={this.renderFooter}
        data={DataQuotations}
        renderItem={({item, index}) => (
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              title={() => {
                return (
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[AppStyles.Titledefault, {width: '75%'}]}>
                      {item.QuotationNo}
                    </Text>
                    {/* <Text style={[AppStyles.Textdefault, { width: '25%' }]}>
                                {this.addPeriod(item.TotalNoVAT)}
                            </Text> */}
                  </View>
                );
              }}
              subtitle={() => {
                return (
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Textdefault}>
                        Khách hàng: {item.CustomerName}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={[AppStyles.Textdefault, {width: '75%'}]}>
                        Ngày: {this.customDate(item.QuotationDate)}
                      </Text>
                      {item.Status == 'Y' ? (
                        <Text
                          style={{
                            justifyContent: 'center',
                            color: AppColors.AcceptColor,
                          }}>
                          {item.StatusWF}
                        </Text>
                      ) : (
                        <Text
                          style={{
                            justifyContent: 'center',
                            color: AppColors.PendingColor,
                          }}>
                          {item.StatusWF}
                        </Text>
                      )}
                    </View>
                  </View>
                );
              }}
              bottomDivider
              titleStyle={AppStyles.Textdefault}
              onPress={() => this.openViewQuotations(item.QuotationGuid)}
            />
          </View>
        )}
        onEndReachedThreshold={0.5}
        onEndReached={({distanceFromEnd}) => {
          if (this.state.staticParamQuotations.CurrentPage !== 1) {
            this.nextPageQuotations();
          }
        }}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
    </View>
  );
  CustomViewOrders = DataOrders => (
    <View>
      <FlatList
        style={{backgroundColor: '#fff'}}
        refreshing={this.state.isFetching}
        ref={ref => {
          this.ListView_Ref = ref;
        }}
        ListFooterComponent={this.renderFooter}
        data={DataOrders}
        renderItem={({item, index}) => (
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              title={() => {
                return (
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[AppStyles.Titledefault, {width: '75%'}]}>
                      {item.OrderNumber}
                    </Text>
                    <Text style={[AppStyles.Textdefault, {width: '25%'}]}>
                      {this.addPeriod(item.TotalOfMoney)}
                    </Text>
                  </View>
                );
              }}
              subtitle={() => {
                return (
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Textdefault}>
                        Khách hàng: {item.CustomerName}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={[AppStyles.Textdefault, {width: '75%'}]}>
                        Trạng thái: {item.OrderStatusString}
                      </Text>
                      <Text style={[AppStyles.Textdefault, {width: '25%'}]}>
                        {item.OrderDateString}
                      </Text>
                    </View>
                  </View>
                );
              }}
              bottomDivider
              titleStyle={AppStyles.Textdefault}
              //chevron
              onPress={() => this.openViewOrder(item.OrderGuid)}
            />
          </View>
        )}
        onEndReachedThreshold={0.5}
        onEndReached={({distanceFromEnd}) => {
          if (this.state.staticParamOrders.CurrentPage !== 1) {
            this.nextPageOrders();
          }
        }}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
    </View>
  );
  CustomViewLeads = DataLeads => (
    <View>
      <FlatList
        style={{backgroundColor: '#fff'}}
        refreshing={this.state.isFetching}
        ref={ref => {
          this.ListView_Ref = ref;
        }}
        ListFooterComponent={this.renderFooter}
        data={DataLeads}
        renderItem={({item, index}) => (
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              title={item.LeadName}
              subtitle={() => {
                return (
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%'}}>
                        <Text style={AppStyles.Textdefault}>
                          SĐT: {item.WorkMobile}
                        </Text>
                      </View>
                      <View style={{width: '50%'}}>
                        <Text style={AppStyles.Textdefault}>
                          Xác suất: {item.SuccessRate + '%'}
                        </Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Textdefault}>
                        Email: {item.WorkEmail}
                      </Text>
                    </View>
                  </View>
                );
              }}
              bottomDivider
              titleStyle={AppStyles.Titledefault}
              //chevron
              onPress={() => this.onView(item)}
            />
          </View>
        )}
        onEndReachedThreshold={0.5}
        onEndReached={({distanceFromEnd}) => {
          if (this.state.staticParamLeads.Page !== 1) {
            this.nextPageLeads();
          }
        }}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={['red', 'green', 'blue']}
          />
        }
      />
    </View>
  );
  //Chuyển tab
  OnChangeTab(data) {
    if (data == '1') {
      this.setState({
        tab2: false,
        tab3: false,
        tab4: false,
        tab1: true,
      });
    } else if (data == '2') {
      this.setState({
        tab1: false,
        tab3: false,
        tab4: false,
        tab2: true,
        ListQuotations: [],
      });
      this.GetQuotations();
    } else if (data == '3') {
      this.setState({
        tab1: false,
        tab2: false,
        tab3: true,
        tab4: false,
        ListOrders: [],
      });
      this.GetOrders();
    } else if (data == '4') {
      this.setState({
        tab1: false,
        tab2: false,
        tab3: false,
        tab4: true,
        ListLeads: [],
      });
      this.GetLeads();
    }
  }
  openViewOrder(id) {
    Actions.viewOrder({OrderGuid: id});
  }
  openViewQuotations(id) {
    Actions.openQuotations({RecordGuid: id});
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}
const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 90,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 90,
  },
  TabAlight: {
    flex: 1,
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
  },
});
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemCompaignsComponent);
