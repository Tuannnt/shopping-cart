import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, View, TextInput } from "react-native";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import API_Compaigns from "../../../network/SM/API_Compaigns";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';

class ListCompaignsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                Page: 1,
                ItemPage: 25,
                Search: "",
                StartDate: "2020-02-01T02:13:13.779Z",
                EndDate: "2020-12-31T02:13:13.779Z",
            },
            selected: undefined,
            openSearch: false
        };
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_Compaigns.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        this.GetAll();
    }

    ListEmpty = () => {
        if (this.state.list.ItemPage > 0) return null;
        return (
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                Page: 1,
                ItemPage: this.state.staticParam.ItemPage,
                Search: this.state.staticParam.Search,
                StartDate: "2020-02-01T02:13:13.779Z",
                EndDate: "2020-12-31T02:13:13.779Z",
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Chiến dịch'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'CRM'}
                />
                {this.state.openSearch == true ?
                    <FormSearch
                        CallbackSearch={(callback) => this.updateSearch(callback)}
                    />
                    : null}
                <View style={{ flex: 9 }}>
                    <FlatList
                        style={{ height: '100%' }}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.RowGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family }}>
                                <ListItem
                                    title={item.CompaignName}
                                    subtitle={() => {
                                        return (
                                            <View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ width: '45%' }}>
                                                        <Text style={AppStyles.Textdefault}>Mã: {item.CompaignId}</Text>
                                                        <Text style={AppStyles.Textdefault}>Số cơ hội: {item.LeadTotal}</Text>
                                                        <Text style={AppStyles.Textdefault}>Ngày BĐ: {this.customDate(item.StartDate)}</Text>
                                                    </View>
                                                    <View style={{ width: '55%' }}>
                                                        <Text style={AppStyles.Textdefault}>Người QL: {item.ChoiseAssigned}</Text>
                                                        <Text style={AppStyles.Textdefault}>Tổng số khách: {item.CustomersTotal}</Text>
                                                        <Text style={AppStyles.Textdefault}>Ngày KT: {this.customDate(item.EndDate)}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    titleStyle={AppStyles.Titledefault}
                                    onPress={() => this.openView(item.CompaignGuid, item.ChoiseAssigned)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.Page !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                </View>
            </View>
        );
    }

    nextPage() {
        this.state.staticParam.Page++;
        this.setState({
            staticParam: {
                Page: this.state.staticParam.Page,
                ItemPage: this.state.staticParam.ItemPage,
                Search: this.state.staticParam.Search,
                StartDate: "2020-02-01T02:13:13.779Z",
                EndDate: "2020-12-31T02:13:13.779Z",
            }
        });
        this.GetAll();
    }
    //Load lại
    onRefresh = () => {
        this.setState({ 
            refreshing: true,
            list: []
         })
        this.GetAll();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.ItemPage > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
    openView(id, choiseAssigned) {
        Actions.smitemCompaigns({ RecordGuid: id, ChoiseAssigned: choiseAssigned });
    }
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListCompaignsComponent);
