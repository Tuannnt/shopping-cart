import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Divider, Icon, ListItem} from 'react-native-elements';
import {API_TM_TASKS, API_TM_CALENDAR, API, API_Leads} from '@network';
import TabBar from '../../component/TabBar';
import {AppStyles, AppColors} from '@theme';
import FormSearch from '../../component/FormSearch';
import * as Progress from 'react-native-progress';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import {FuncCommon} from '../../../utils';
import {WebView} from 'react-native-webview';
import LoadingComponent from '../../component/LoadingComponent';
import Combobox from '../../component/Combobox';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const DataMenuBottomKanban = [
  {
    key: 'open',
    name: 'Xem chi tiết',
    icon: {
      name: 'eye',
      type: 'entypo',
      color: AppColors.Maincolor,
    },
  },
  // {
  //     key: 'edit',
  //     name: 'Sửa cơ hội',
  //     icon: {
  //         name: 'edit',
  //         type: 'entypo',
  //         color: AppColors.Maincolor,
  //     }
  // },
  {
    key: 'delete',
    name: 'Xóa cơ hội',
    icon: {
      name: 'delete',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
];
class KanbanLeadsComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      EvenFromSearch: false,
      showList: false,
      refreshing: false,
      selectItem: null,
      //combo box Compaigns
      ListCompaigns: [],
      CompaignPlanGuid: null,
      dropdownName: '',
      CompaignGuid: null,
      //kanban
      KanbanView: [],
      DataStatus: [],
      Count_Tab: 0,
      SearchKanban: false,
      staticParamLeads: {
        Page: 1,
        ItemPage: 25,
        Search: '',
        CompaignId: null,
        StartDate: '2020-02-01T02:13:13.779Z',
        EndDate: '2020-12-31T02:13:13.779Z',
        CategoryId: null,
      },
      ListLeads: [],
      isFetching: false,
      refreshing: true,
    };
  }
  componentDidMount() {
    this.GetCompaignsAll();
  }
  GetCompaignsAll = () => {
    API_Leads.GetCompaignsAll(this.state.CompaignPlanGuid)
      .then(rs => {
        var _lstCompaigns = JSON.parse(rs.data.data);
        var _reCompaigns = [];
        for (var i = 0; i < _lstCompaigns.length; i++) {
          _reCompaigns.push({
            text:
              _lstCompaigns[i].CompaignId +
              ' - ' +
              _lstCompaigns[i].CompaignName,
            value: _lstCompaigns[i].CompaignGuid,
          });
        }
        this.setState({
          ListCompaigns: _reCompaigns,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  GetSteps = () => {
    API_Leads.GetSteps({CompaignGuid: this.state.CompaignGuid, IsDefault: null})
      .then(rs => {
        var obj = JSON.parse(rs.data.data);
        var _data = [];
        var _dataStatus = [];
        for (var i = 0; i < obj.length; i++) {
          _data.push({
            key: obj[i].CategoryId,
            name: obj[i].Title,
            color: obj[i].BackgroudColor,
            StatusSort: false,
            Value: [],
          });
          _dataStatus.push({
            value: obj[i].CategoryId,
            text: obj[i].Title,
          });
        }
        this.setState({
          KanbanView: _data,
          DataStatus: _dataStatus,
        });
        this.GetLeads();
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  //Lấy danh sách cơ hội
  GetLeads = item => {
    API_Leads.GetLeads(this.state.staticParamLeads)
      .then(res => {
        this.state.ListLeads =
          res.data.data != 'null'
            ? this.state.ListLeads.concat(JSON.parse(res.data.data))
            : [];
        this.setState({ListLeads: this.state.ListLeads});
        if (
          this.state.staticParamLeads.CategoryId !== '' &&
          this.state.staticParamLeads.CategoryId !== null
        ) {
          for (let j = 0; j < this.state.KanbanView.length; j++) {
            if (
              this.state.KanbanView[j].key ===
              this.state.staticParamLeads.CategoryId
            ) {
              for (let i = 0; i < this.state.ListLeads.length; i++) {
                this.state.KanbanView[j].Value.push(this.state.ListLeads[i]);
              }
            }
          }
          this.setState({
            KanbanView: this.state.KanbanView,
            refreshing: false,
          });
        } else {
          for (let i = 0; i < this.state.ListLeads.length; i++) {
            for (let j = 0; j < this.state.KanbanView.length; j++) {
              if (
                this.state.ListLeads[i].CategoryId ===
                this.state.KanbanView[j].key
              ) {
                this.state.KanbanView[j].Value.push(this.state.ListLeads[i]);
              }
            }
          }
          this.setState({
            KanbanView: this.state.KanbanView,
            refreshing: false,
          });
        }
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };
  //#region Search
  updateSearch = search => {
    this.state.staticParamLeads.Search = search;
    // this.setState({
    //     ListLeads: [],
    //     staticParamLeads: this.state.staticParamLeads
    // });
    // this.GetLeads();
  };
  //#region Search kanban
  Search_Kanban(search, item) {
    this.state.staticParamLeads.Search = search;
    this.state.staticParamLeads.CategoryId = item.key;
    for (let i = 0; i < this.state.KanbanView.length; i++) {
      if (this.state.KanbanView[i].key === item.key) {
        this.state.KanbanView[i].Value = [];
      }
    }
    this.setState({
      KanbanView: this.state.KanbanView,
      ListLeads: [],
      staticParamLeads: this.state.staticParamLeads,
    });
    this.GetLeads(item);
  }
  //#endregion

  //#region View Kanban
  OnScrollTo(event, para) {
    var y = event.nativeEvent.contentOffset.x;
    if (y >= this.state.Count_Tab * DRIVER.width + DRIVER.width / 3) {
      this.state.Count_Tab = this.state.Count_Tab + 1;
      this.myScroll.scrollTo({
        x:
          DRIVER.width * para.length -
          (para.length - this.state.Count_Tab) * DRIVER.width,
      });
    } else if (y < this.state.Count_Tab * DRIVER.width - DRIVER.width / 3) {
      if (this.state.Count_Tab !== 0) {
        this.state.Count_Tab = this.state.Count_Tab - 1;
        this.myScroll.scrollTo({
          x:
            DRIVER.width * para.length -
            (para.length - this.state.Count_Tab) * DRIVER.width,
        });
      }
    } else {
      this.myScroll.scrollTo({
        x:
          DRIVER.width * para.length -
          (para.length - this.state.Count_Tab) * DRIVER.width,
      });
    }
  }
  //View kanban
  CustomKanban = para => (
    <View style={{flexDirection: 'column', flex: 1}}>
      <ScrollView
        ref={ref => {
          this.myScroll = ref;
        }}
        // onMomentumScrollEnd
        onScrollEndDrag={event => this.OnScrollTo(event, para)}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{top: 0}}>
        {para.map((item, index) => (
          <View
            style={{
              flexDirection: 'column',
              flex: 1,
              width: DRIVER.width,
              height: DRIVER.height,
              borderWidth: 0.5,
              borderColor: AppColors.gray,
            }}>
            {/* top */}
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                backgroundColor: item.color,
                padding: 10,
                borderBottomColor: AppColors.Maincolor,
                borderBottomWidth: 2,
                width: DRIVER.width,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  justifyContent: 'center',
                }}>
                <Text style={[AppStyles.h4, {color: '#fff'}]}>{item.name}</Text>
                <Text style={[AppStyles.Textsmall, {color: '#fff'}]}>
                  {item.Value.length} cơ hội
                </Text>
              </View>
              <View
                style={{
                  flex: 2,
                  flexDirection: 'column',
                  justifyContent: 'center',
                }}>
                {this.state.SearchKanban ? (
                  <TextInput
                    style={[AppStyles.FormInput, {backgroundColor: '#fff'}]}
                    underlineColorAndroid="transparent"
                    placeholder={'Từ khoá'}
                    autoCapitalize="none"
                    // multiline={true}
                    onChangeText={text => this.Search_Kanban(text, item)}
                  />
                ) : null}
              </View>

              <View style={{flexDirection: 'row', flex: 1}}>
                <TouchableOpacity
                  style={[AppStyles.containerCentered, {flex: 1}]}
                  onPress={() =>
                    this.setState({SearchKanban: !this.state.SearchKanban})
                  }>
                  <Icon
                    name="search1"
                    type="antdesign"
                    size={20}
                    color={'#fff'}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[AppStyles.containerCentered, {flex: 1}]}
                  onPress={() => this.sortDate(item)}>
                  <Icon
                    name="sort-amount-desc"
                    type="font-awesome"
                    size={20}
                    color={'#fff'}
                  />
                </TouchableOpacity>
                {/* <TouchableOpacity style={[AppStyles.containerCentered, { flex: 1 }]} onPress={() => Actions.taskAdd({ TypeForm: 'add', StatusCompletedTaskOfUser: item.code })}>
                                    <Icon
                                        name={'plus'}
                                        type={'entypo'}
                                        size={30}
                                        color={'#fff'} />
                                </TouchableOpacity> */}
              </View>
            </TouchableOpacity>
            {/* Body */}
            <View
              style={{
                height: DRIVER.height - 150,
                flexDirection: 'column',
                justifyContent: 'center',
              }}>
              {item.Value.length > 0
                ? this.Kanban_Body(item.Value, item.color)
                : null}
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );

  Kanban_Body = (item, colorKb) => (
    <FlatList
      data={item}
      renderItem={({item, index}) => (
        <ListItem
          subtitle={() => {
            return (
              <View
                style={{
                  flexDirection: 'column',
                  marginTop: -20,
                  borderLeftWidth: 3,
                  borderRadius: 5,
                  paddingLeft: 5,
                  borderLeftColor: colorKb,
                }}>
                <View style={{flex: 5, flexDirection: 'column'}}>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 4}}>
                      <Text style={AppStyles.Titledefault}>
                        {item.LeadName}
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={{
                        flex: 0.5,
                        alignItems: 'flex-end',
                        justifyContent: 'center',
                      }}
                      onPress={() => this.onActionStatus(item)}>
                      <Icon
                        name="edit"
                        type="entypo"
                        size={15}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        flex: 0.5,
                        alignItems: 'flex-end',
                        justifyContent: 'center',
                      }}
                      onPress={() => this.onAction(item)}>
                      <Icon
                        name="dots-three-horizontal"
                        type="entypo"
                        size={15}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{height: 40}}>
                    <Text style={AppStyles.Textdefault}>
                      Số điện thoại: {item.WorkMobile}
                    </Text>
                    <Text style={AppStyles.Textdefault}>
                      Email: {item.WorkEmail}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Progress.Bar
                      style={{marginBottom: 0, marginRight: 0}}
                      progress={item.SuccessRate / 100}
                      height={2}
                      width={250}
                      color={colorKb}
                    />
                    <Text style={[AppStyles.Textdefault, {color: colorKb}]}>
                      {' '}
                      {item.SuccessRate} %
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flexDirection: 'row', flex: 5}}>
                      <View style={{flexDirection: 'row', flex: 1}}>
                        <TouchableOpacity
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginRight: 10,
                          }}>
                          <Icon
                            name="file-document-outline"
                            type="material-community"
                            size={15}
                            color={AppColors.gray}
                          />
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {color: AppColors.gray},
                            ]}>
                            {' '}
                            {item.CountQuotation}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginRight: 10,
                          }}>
                          <Icon
                            name="file1"
                            type="antdesign"
                            size={15}
                            color={AppColors.gray}
                          />
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {color: AppColors.gray},
                            ]}>
                            {' '}
                            {item.CountTasks}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginRight: 10,
                          }}>
                          <Icon
                            name="flag"
                            type="antdesign"
                            size={15}
                            color={
                              item.IsFollow == true
                                ? AppColors.Maincolor
                                : AppColors.gray
                            }
                          />
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {color: AppColors.gray},
                            ]}>
                            {' '}
                            {item.CountOrder}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginRight: 10,
                          }}>
                          <Icon
                            name="comments-o"
                            type="font-awesome"
                            size={15}
                            color={AppColors.gray}
                          />
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {color: AppColors.gray},
                            ]}>
                            {' '}
                            {item.CountComment}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Text style={AppStyles.Textdefault}>
                          {FuncCommon.ConDate(item.CreatedDate, 0)}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            );
          }}
          bottomDivider
          onPress={() => null}
        />
      )}
      //onEndReached={() => this.loadMoreData_kanban()}
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['black']}
        />
      }
    />
  );
  //#endregion

  //ListView
  CustomListView = data => (
    <FlatList
      style={{height: '100%'}}
      refreshing={this.state.isFetching}
      ref={ref => {
        this.ListView_Ref = ref;
      }}
      ListFooterComponent={this.renderFooter}
      ListEmptyComponent={this.ListEmpty}
      keyExtractor={item => item.CustomerGuid}
      data={data}
      renderItem={({item, index}) => (
        <View style={{fontSize: 12}}>
          <ListItem
            style={{}}
            title={item.LeadName}
            subtitle={() => {
              return (
                <View>
                  <Text style={AppStyles.Textdefault}>
                    Số điện thoại: {item.WorkMobile}
                  </Text>
                  <Text style={AppStyles.Textdefault}>
                    Email: {item.WorkEmail}
                  </Text>
                </View>
              );
            }}
            bottomDivider
            titleStyle={AppStyles.Titledefault}
            onPress={() => Actions.smitemLeads({RecordGuid: item.CustomerGuid})}
          />
        </View>
      )}
      onEndReachedThreshold={0.5}
      onEndReached={({distanceFromEnd}) => {
        if (this.state.staticParamLeads.Page !== 1) {
          this.nextPage();
        }
      }}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['red', 'green', 'blue']}
        />
      }
    />
  );
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  ListEmpty = () => {
    if (this.state.KanbanView.length > 0) return null;
    return (
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  nextPage() {
    this.state.staticParamLeads.Page++;
    this.setState({
      staticParamLeads: this.state.staticParamLeads,
    });
    this.GetLeads();
  }
  //Load lại
  onRefresh = () => {
    this.setState({
      refreshing: true,
      ListLeads: [],
    });
    this.GetLeads();
  };
  //#region View Tổng phân biệt các View
  render() {
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={AppStyles.container}>
          <TabBar
            title={'Quản lý cơ hội'}
            BackModuleByCode={'CRM'}
            FormSearch={false}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
          />
          <Divider />
          <View style={{paddingLeft: 5, paddingRight: 5, flexDirection: 'row'}}>
            <View style={{flex: 5}}>
              {this.state.ListCompaigns.length > 0 ? (
                <TouchableOpacity
                  style={[
                    AppStyles.FormInput,
                    {marginLeft: 10, marginRight: 10, flexDirection: 'row'},
                  ]}
                  onPress={() => this.onActionCombobox()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.state.dropdownName == ''
                        ? {color: AppColors.gray, flex: 3}
                        : {flex: 3},
                    ]}>
                    {this.state.dropdownName !== ''
                      ? this.state.dropdownName
                      : 'Chọn chiến dịch...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      color="#888888"
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
              ) : null}
            </View>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Icon
                color={
                  this.state.showList === true ? AppColors.Maincolor : '#888888'
                }
                name={'list'}
                type="entypo"
                onPress={() => this.ChangeOrder('list')}
              />
            </View>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Icon
                color={
                  this.state.showList === false
                    ? AppColors.Maincolor
                    : '#888888'
                }
                name={'columns'}
                type="feather"
                onPress={() => this.ChangeOrder('kanban')}
              />
            </View>
          </View>
          {this.state.EvenFromSearch == true ? (
            <FormSearch
              CallbackSearch={callback => this.updateSearch(callback)}
            />
          ) : null}
          <View style={{flex: 6}}>
            {this.state.showList === false ? (
              this.state.KanbanView.length > 0 ? (
                this.CustomKanban(this.state.KanbanView)
              ) : (
                <View style={[AppStyles.centerAligned]}>
                  <Text style={[AppStyles.Titledefault]}>Không có dữ liệu</Text>
                </View>
              )
            ) : (
              this.CustomListView(this.state.ListLeads)
            )}
          </View>
          {this.state.ListCompaigns.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeCompaigns}
              value={this.state.ListCompaigns[0].value}
              data={this.state.ListCompaigns}
              nameMenu={'Chọn chiến dịch'}
              eOpen={this.openCombobox}
              position={'top'}
            />
          ) : null}
          <MenuActionCompoment
            callback={this.actionMenuCallbackKanban}
            data={DataMenuBottomKanban}
            nameMenu={'Chức năng'}
            eOpen={this.openMenu}
            position={'bottom'}
          />
          {this.state.DataStatus.length > 0 ? (
            <Combobox
              callback={this.UpdateStatus}
              data={this.state.DataStatus}
              nameMenu={'Cập nhật trạng thái'}
              eOpen={this.openMenuStatus}
              position={'bottom'}
            />
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  //#endregion

  //Mở dropdow chức năng
  _openMenu() {}
  openMenu = d => {
    this._openMenu = d;
  };
  onAction(item) {
    this.setState({
      selectItem: item,
    });
    this._openMenu();
  }
  //Mở menu trạng thái
  _openMenuStatus() {}
  openMenuStatus = d => {
    this._openMenuStatus = d;
  };
  onActionStatus(item) {
    this.setState({
      selectItem: item,
    });
    this._openMenuStatus();
  }
  //Chức năng chung
  actionMenuCallbackKanban = d => {
    switch (d) {
      case 'open':
        Actions.smitemLeads({RecordGuid: this.state.selectItem.CustomerGuid});
        break;
      case 'delete':
        this.Delete(this.state.selectItem.CustomerGuid);
        break;
      default:
        break;
    }
  };
  //Cập nhật trạng thái
  UpdateStatus = value => {
    if (value.value !== null) {
      var obj = {
        CustomerGuid: this.state.selectItem.CustomerGuid,
        CategoryId: value.value,
      };
      API_Leads.UpdateStatus(obj)
        .then(res => {
          for (let i = 0; i < this.state.KanbanView.length; i++) {
            this.state.KanbanView[i].Value = [];
          }
          this.setState({
            KanbanView: this.state.KanbanView,
            ListLeads: [],
          });
          this.GetLeads();
          Alert.alert(
            'Thông báo',
            'Cập nhật thành công',
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
        })
        .catch(error => {
          console.log(error);
        });
    }
  };
  //Xóa cơ hội
  Delete = Id => {
    API_Leads.Delete(Id)
      .then(res => {
        for (let i = 0; i < this.state.KanbanView.length; i++) {
          this.state.KanbanView[i].Value = [];
        }
        this.setState({
          KanbanView: this.state.KanbanView,
          ListLeads: [],
        });
        this.GetLeads();
        Alert.alert(
          'Thông báo',
          'Xóa thành công',
          [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
          {cancelable: true},
        );
      })
      .catch(error => {
        console.log(error);
      });
  };
  //Sắp xếp theo tên cơ hội
  sortName = para => {
    var data = this.state.KanbanView;
    if (para.StatusSort === false) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].code === para.code) {
          data[i].Value.sort(function(a, b) {
            if (a.LeadName < b.LeadName) {
              return -1;
            }
            if (a.LeadName > b.LeadName) {
              return 1;
            }
            return 0;
          });
          data[i].StatusSort = !data[i].StatusSort;
        }
      }
    } else {
      for (let i = 0; i < data.length; i++) {
        if (data[i].code === para.code) {
          data[i].Value.sort(function(a, b) {
            if (a.LeadName > b.LeadName) {
              return -1;
            }
            if (a.LeadName < b.LeadName) {
              return 1;
            }
            return 0;
          });
          data[i].StatusSort = !data[i].StatusSort;
        }
      }
    }
    this.setState({KanbanView: data});
  };
  //Sắp xếp theo ngày
  sortDate = para => {
    var data = this.state.KanbanView;
    if (para.StatusSort === false) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].code === para.code) {
          data[i].Value.sort(function(a, b) {
            if (a.CreatedDate < b.CreatedDate) {
              return -1;
            }
            if (a.CreatedDate > b.CreatedDate) {
              return 1;
            }
            return 0;
          });
          data[i].StatusSort = !data[i].StatusSort;
        }
      }
    } else {
      for (let i = 0; i < data.length; i++) {
        if (data[i].code === para.code) {
          data[i].Value.sort(function(a, b) {
            if (a.CreatedDate > b.CreatedDate) {
              return -1;
            }
            if (a.CreatedDate < b.CreatedDate) {
              return 1;
            }
            return 0;
          });
          data[i].StatusSort = !data[i].StatusSort;
        }
      }
    }
    this.setState({KanbanView: data});
  };
  //#endregion
  ChangeCompaigns = value => {
    this.state.CompaignGuid = value.value;
    this.state.staticParamLeads.CompaignId = value.text.split(' -')[0];
    this.setState({
      dropdownName: value.text,
      CompaignGuid: this.state.CompaignGuid,
      staticParamLeads: this.state.staticParamLeads,
    });
    this.GetSteps();
  };
  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCombobox() {
    this._openCombobox();
  }
  ChangeOrder(data) {
    if (data === 'list') {
      this.setState({
        showList: true,
      });
    } else if (data === 'kanban') {
      this.setState({
        showList: false,
      });
    }
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(KanbanLeadsComponet);
