import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {API_Leads} from '@network';
import ErrorHandler from '../../../error/handler';
import API_Orders from '../../../network/SM/API_Orders';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';

class ItemLeadsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      Data: [],
      ListContacts: [],
      CustomerGuid: null,
      tab1: true,
      tab2: false,
      loading: true,
    };
  }

  componentDidMount(): void {
    this.getItem();
  }

  getItem() {
    let id = this.props.RecordGuid;
    API_Leads.GetItems(id)
      .then(res => {
        this.state.Data = JSON.parse(res.data.data);
        this.setState({
          Data: this.state.Data,
          loading: false,
        });
        this.getContacts(id);
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log(error.data.data);
      });
  }
  //Lấy danh sách thông tin liên hệ
  getContacts = CustomerGuid => {
    API_Leads.GetContacts(CustomerGuid)
      .then(res => {
        this.state.ListContacts =
          res.data.data != 'null' ? JSON.parse(res.data.data).data : [];
        this.setState({ListContacts: this.state.ListContacts});
      })
      .catch(error => {
        console.log(error);
      });
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết cơ hội'}
          callBack={() => this.onPressBack()}
          FormAttachment={false}
        />
        <View
          style={{
            marginLeft: 10,
            marginRight: 10,
            borderRadius: 10,
            flex: 1,
            borderWidth: 0.5,
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => this.OnChangeTab('1')}
            style={styles.TabarPending}>
            <Text
              style={
                this.state.tab1 === true ? {color: AppColors.Maincolor} : ''
              }>
              Thông tin chung
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.OnChangeTab('2')}
            style={styles.TabarApprove}>
            <Text
              style={
                this.state.tab4 === true ? {color: AppColors.Maincolor} : ''
              }>
              Thông tin liên hệ
            </Text>
          </TouchableOpacity>
        </View>
        {/* Thông tin chung */}
        <View style={{flex: 15}}>
          {this.state.tab1 === true ? (
            this.CustomViewTTChung()
          ) : this.state.tab2 == true && this.state.ListContacts.length > 0 ? (
            this.CustomViewContacts(this.state.ListContacts)
          ) : (
            <View style={styles.MainContainer}>
              <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
            </View>
          )}
        </View>
      </View>
    );
  }

  onPressBack() {
    Actions.pop();
  }
  //CustomView
  CustomViewTTChung = () => (
    <View>
      {/* Thông tin chung */}
      <View style={[{padding: 10, backgroundColor: '#fff'}]}>
        <View style={{paddingBottom: 10}}>
          <Text style={[AppStyles.Labeldefault, {paddingBottom: 10}]}>
            Tên chiến dịch :
          </Text>
          <Text style={AppStyles.Textdefault}>
            {this.state.Data.CompaignName}
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 10}]}>
              Mã chiến dịch :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 10}]}>
              Số điện thoại :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 10}]}>
              Email :
            </Text>
            <Text style={[AppStyles.Labeldefault, {paddingBottom: 10}]}>
              Xác suất :
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 10}]}>
              {this.state.Data.CompaignId}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 10}]}>
              {this.props.WorkMobile}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 10}]}>
              {this.state.Data.WorkEmail}
            </Text>
            <Text style={[AppStyles.Textdefault, {paddingBottom: 10}]}>
              {this.state.Data.SuccessRate} %
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
  CustomViewContacts = DataContacts => (
    <ScrollView>
      {DataContacts.map((item, index) => (
        <View style={{backgroundColor: '#fff', flex: 1}}>
          <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
            <ListItem
              title={() => {
                return (
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[AppStyles.Titledefault, {width: '75%'}]}>
                      {item.QuotationNo}
                    </Text>
                    {/* <Text style={[AppStyles.Textdefault, { width: '25%' }]}>
                                {this.addPeriod(item.TotalNoVAT)}
                            </Text> */}
                  </View>
                );
              }}
              subtitle={() => {
                return (
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Textdefault}>
                        Khách hàng: {item.CustomerName}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={[AppStyles.Textdefault, {width: '75%'}]}>
                        Ngày: {this.customDate(item.QuotationDate)}
                      </Text>
                      {item.Status == 'Y' ? (
                        <Text
                          style={{
                            justifyContent: 'center',
                            color: AppColors.AcceptColor,
                          }}>
                          {item.StatusWF}
                        </Text>
                      ) : (
                        <Text
                          style={{
                            justifyContent: 'center',
                            color: AppColors.PendingColor,
                          }}>
                          {item.StatusWF}
                        </Text>
                      )}
                    </View>
                  </View>
                );
              }}
              bottomDivider
              titleStyle={AppStyles.Textdefault}
              //onPress={() => this.openViewQuotations(item.QuotationGuid)}
            />
          </View>
        </View>
      ))}
    </ScrollView>
  );
  //Chuyển tab
  OnChangeTab(data) {
    if (data == '1') {
      this.setState({
        tab2: false,
        tab1: true,
      });
    } else if (data == '2') {
      this.setState({
        tab1: false,
        tab2: true,
        ListContacts: [],
      });
    }
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}
const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 90,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 90,
  },
  TabAlight: {
    flex: 1,
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
  },
});
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemLeadsComponent);
