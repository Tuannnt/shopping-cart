import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Keyboard,
  View,
  FlatList,
  Text,
  Dimensions,
  RefreshControl,
  Animated,
  TouchableOpacity,
} from 'react-native';
import { Header, Icon, Divider, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import API_TicketRequests from '../../../network/AM/API_TicketRequests';
import Dialog from 'react-native-dialog';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../utils';
import {
  CustomView,
  TabBarBottom,
  LoadingComponent,
  TabBar_Title,
  Combobox,
} from '@Component';
import Toast from 'react-native-simple-toast';
const styles = StyleSheet.create({
  hidden: { display: 'none' },
});
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class ViewTicketRequestsImportComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TicketRequestGuid: '',
      model: {},
      List: [],
      isFetching: false,
      isHidden: false,
      check: false,
      Title: 'Thông báo',
      desciption: 'Bạn có muốn xác nhận thông tin',
      text: '3463',
      loading: true,
      ViewItemDetail: null,
      //hiển thị thông tin chung
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
      listtabbarBotom: [
        {
          Title: 'Xác nhận thông tin',
          Icon: 'checksquareo',
          Type: 'antdesign',
          Value: 'checksquareo',
          Checkbox: false,
          OffEditcolor: true,
        },
        {
          Title: 'Xóa',
          Icon: 'delete',
          Type: 'antdesign',
          Value: 'delete',
          Checkbox: false,
          OffEditcolor: true,
        },
      ],
    };
    this.btnDelete = [
      {
        name: 'Xác nhận',
        value: 'Y',
        color: '#3366CC',
      },
      {
        name: 'Hủy',
        value: 'H',
        color: 'red',
      },
    ];
  }
  componentDidMount = () => {
    this.state.TicketRequestGuid =
      this.props.TicketRequestGuid || this.props.RecordGuid;
    this.GetTicketRequestsById();
    this.Skill();
    this.setViewOpen('ViewTotal');
  };
  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 1, duration: 333 },
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 0, duration: 333 },
    );
  };
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ ViewAll: !this.state.ViewAll });
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ ViewDetail: !this.state.ViewDetail });
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ ViewDetail: true, ViewAll: true });
    }
  };
  //#endregion

  _openViewDetail() { }
  openViewDetail = d => {
    this._openViewDetail = d;
  };
  openAcctionViewDetail = item => {
    this.setState({ ViewItemDetail: item }, () => {
      this._openViewDetail();
    });
  };
  CustomView = () => {
    const { ViewItemDetail } = this.state;
    return ViewItemDetail === null ? null : (
      <CustomView eOpen={this.openViewDetail}>
        <View style={{ width: DRIVER.width - 50 }}>
          <View style={{ alignItems: 'center', padding: 10 }}>
            <Text style={[AppStyles.Titledefault]}>
              {'Chi tiết ' + ViewItemDetail.ItemId}
            </Text>
          </View>
          <Divider />
          <View style={{ marginBottom: 10 }}>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Mã nội bộ :</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.VenderCode}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Mã sản phẩm :</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.ItemId}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Tên sản phẩm :</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.ItemName}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Số đơn hàng bán :</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.OrderNumberId}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Số đơn mua hàng :</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.PurchaseOrderId}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>ĐVT :</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.UnitAfter}
                </Text>
              </View>
            </View>
            {/* <View style={{padding: 10, paddingBottom: 0, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Số lượng định mức :</Text>
              </View>
              <View style={{flex: 2, alignItems: 'flex-end'}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.InventoryQuotaMin)}
                </Text>
              </View>
            </View> */}
            {/* <View
              style={{padding: 10, paddingBottom: 0, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Kho :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.WarehouseId}
                </Text>
              </View>
            </View> */}
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>SL yêu cầu:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.Quantity)}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>SL chuyển đổi:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(
                    ViewItemDetail.Quantity !== ''
                      ? parseFloat(ViewItemDetail.Quantity) *
                      parseFloat(ViewItemDetail.RateCD)
                      : 0,
                  )}
                </Text>
              </View>
            </View>
            {/* <View style={{padding: 10, paddingBottom: 0, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>SL chuyển đổi:</Text>
              </View>
              <View style={{flex: 2, alignItems: 'flex-end'}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.Quantity)}
                </Text>
              </View>
            </View> */}
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>SL TN:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.QuantityActual)}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Đơn giá:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.UnitPriceBefore)}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Kho:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Labeldefault}>
                  {ViewItemDetail.WarehouseId}
                </Text>
              </View>
            </View>
            <Divider />
            <View style={{ padding: 10, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Thành tiền:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Labeldefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.Amount)}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </CustomView>
    );
  };
  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <TabBar_Title
          title={'Chi tiết đề nghị'}
          callBack={() => this.onPressBack()}
        />
        <View style={{ flex: 1 }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewAll')}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
            </View>
            <Animated.View
              style={{
                transform: [{ rotate: rotateStart_ViewAll }, { perspective: 4000 }],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewAll !== true ? null : (
            <View style={[{ padding: 10 }]}>
              <View style={[{ flexDirection: 'row' }]}>
                <View style={{ flex: 1 }}>
                  <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                    Số phiếu :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                    Ngày yêu cầu :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                    Người yêu cầu :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                    Bộ phận :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                    Nội dung nhập :
                  </Text>
                  <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                    Tên đối tượng :
                  </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                    {this.state.model.TicketRequestNo}
                  </Text>
                  <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                    {FuncCommon.ConDate(this.state.model.RequestDate, 0)}
                  </Text>
                  <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                    {this.state.model.EmployeeName}
                  </Text>
                  <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                    {this.state.model.DepartmentName}
                  </Text>
                  <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                    {this.state.model.VoucherType === 'N'
                      ? 'Nhập mua'
                      : this.state.model.VoucherType === 'L'
                        ? 'Nhập hàng bán bị trả lại'
                        : this.state.model.VoucherType === 'M'
                          ? 'Nhập thành phẩm sản xuất'
                          : this.state.model.VoucherType === 'O'
                            ? 'Nhập khác'
                            : ''}
                  </Text>
                  <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                    {this.state.model.ObjectName}
                  </Text>
                </View>
              </View>
              <View style={[{}]}>
                <Text style={[AppStyles.Labeldefault, { paddingBottom: 5 }]}>
                  Diễn giải:
                </Text>
                <Text style={[AppStyles.Textdefault, { paddingBottom: 5 }]}>
                  {this.state.model.Description}
                </Text>
              </View>
            </View>
          )}
          <Divider />
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewDetail')}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  { rotate: rotateStart_ViewDetail },
                  { perspective: 4000 },
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewDetail !== true ? null : (
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.state.List}
                style={{ flex: 1 }}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      padding: 10,
                      borderBottomColor: AppColors.gray,
                      borderBottomWidth: 0.5,
                    }}
                    onPress={() => {
                      this.openAcctionViewDetail(item);
                    }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row' }}>
                          <Text
                            style={[
                              AppStyles.Titledefault,
                              { width: '100%', flex: 10 },
                            ]}>
                            {item.ItemName}
                          </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                          <Text style={[AppStyles.Textdefault, { width: '50%' }]}>
                            Số lượng YC: {item.Quantity}
                          </Text>
                          <Text style={[AppStyles.Textdefault, { width: '50%' }]}>
                            ĐVT: {item.UnitName}
                          </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                          <Text style={[AppStyles.Textdefault, { width: '50%' }]}>
                            Đ.Giá: {FuncCommon.addPeriod(item.UnitPriceBefore)}
                          </Text>
                          <Text style={[AppStyles.Textdefault, { width: '50%' }]}>
                            T.Tiền:{' '}
                            <Text style={{ fontWeight: 'bold' }}>
                              {FuncCommon.addPeriod(item.Amount)}
                            </Text>
                          </Text>
                        </View>
                      </View>
                      <View style={{ justifyContent: 'center' }}>
                        <Icon
                          color="#888888"
                          name={'chevron-thin-right'}
                          type="entypo"
                          size={20}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['red', 'green', 'blue']}
                  />
                }
              />
            </View>
          )}
        </View>

        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.state.listtabbarBotom}
            onCallbackValueBottom={callback => this.check(callback)}
          />
        </View>

        {/* view chi tiet */}
        {this.CustomView()}
        {/* Thông báo */}
        <Dialog.Container visible={this.state.check}>
          <Dialog.Title>Thông báo</Dialog.Title>
          <Dialog.Description>
            Bạn có muốn xác nhận thông tin ?
          </Dialog.Description>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Dialog.Button
              style={{ color: '#055a70', marginLeft: 5 }}
              label="Xác nhận"
              onPress={() => this.onYes()}
            />
            <Dialog.Button
              style={{ color: '#445464', marginLeft: 5 }}
              label="Không xác nhận"
              onPress={() => this.onNo()}
            />
            <Dialog.Button
              style={{ color: '#dc3545', marginLeft: 5 }}
              label="Đóng"
              onPress={() => this.onClose()}
            />
          </View>
        </Dialog.Container>
        {this.renderdelete(this.state.model.TicketRequestNo, this.btnDelete)}
      </View>
    );
  }
  renderdelete(title, btns) {
    return this.state.EventClickDelete === true ? (
      <View>
        <Dialog.Container visible={true}>
          <Dialog.Title>Xác nhận xóa:</Dialog.Title>
          <Dialog.Description>
            Bạn có chắc chắn muốn xóa {title} không?
          </Dialog.Description>
          {btns && btns.length > 0
            ? btns.map((item, index) => (
              <Dialog.Button
                style={{ color: item.color }}
                label={item.name}
                onPress={() => this.Delete(item.value)}
              />
            ))
            : null}
        </Dialog.Container>
      </View>
    ) : null;
  }
  Delete = para => {
    if (para == 'Y') {
      this.deleteAPI();
      EventClickDelete: false;
    }
    this.setState({
      EventClickDelete: false,
    });
  };
  check = data => {
    if (data === 'checksquareo') {
      this.onConfirm();
    } else {
      this.setState({
        EventClickDelete: true,
      });
    }
  };
  deleteAPI = () => {
    let id = {
      Id: this.props.TicketRequestGuid || this.props.RecordGuid,
    };
    this.setState({ loading: true });
    API_TicketRequests.DeleteTicketRequestById(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data);
      });
  };
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'TKImport', ActionTime: new Date().getTime() });
  }
  //Lấy thông tin đề nghị nhập
  GetTicketRequestsById = () => {
    API_TicketRequests.GetTicketRequestsById(this.state.TicketRequestGuid)
      .then(res => {
        let model = JSON.parse(res.data.data);
        let listtabbarBotom = [
          {
            Title: 'Xác nhận thông tin',
            Icon: 'checksquareo',
            Type: 'antdesign',
            Value: 'checksquareo',
            Checkbox: false,
            OffEditcolor: true,
          },
          {
            Title: 'Xóa',
            Icon: 'delete',
            Type: 'antdesign',
            Value: 'delete',
            Checkbox: false,
            OffEditcolor: true,
          },
        ];
        if (model.Status === 'Y') {
          listtabbarBotom = [];
        }
        this.setState({
          model,
          listtabbarBotom,
        });
        this.GetTicketRequestsDetailsAll();
      })
      .catch(error => { });
  };
  //Lấy danh sách đề nghị nhập
  GetTicketRequestsDetailsAll = () => {
    this.setState({ refreshing: true });
    API_TicketRequests.GetTicketRequestsDetailsAll(this.state.TicketRequestGuid)
      .then(res => {
        this.state.List = this.state.List.concat(JSON.parse(res.data.data));
        this.setState({
          List: this.state.List,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };
  //Xác nhận thông tin
  onConfirm() {
    if (this.state.model.Status === 'Y') {
      Toast.showWithGravity('Phiếu đã được duyệt', Toast.LONG, Toast.CENTER);
      return;
    }
    this.setState({
      check: true,
    });
  }
  onClose() {
    this.setState({
      check: false,
    });
  }
  onYes = () => {
    API_TicketRequests.CheckStatus({
      TicketRequestGuid: this.state.model.TicketRequestGuid,
      Status: 'Y',
    })
      .then(res => {
        if (res.data.errorCode == 200) {
          this.setState(
            {
              check: false,
            },
            () => {
              setTimeout(() => {
                Toast.showWithGravity(
                  'Xử lý thành công',
                  Toast.LONG,
                  Toast.CENTER,
                );
                this.onPressBack();
              }, 1000);
            },
          );
        }
      })
      .catch(error => {
        this.setState({
          check: true,
        });
      });
  };
  onNo = () => {
    API_TicketRequests.CheckStatus({
      TicketRequestGuid: this.state.model.TicketRequestGuid,
      Status: 'N',
    })
      .then(res => {
        if (res.data.errorCode == 200) {
          this.setState(
            {
              check: false,
            },
            () => {
              setTimeout(() => {
                Toast.showWithGravity(
                  'Xử lý thành công',
                  Toast.LONG,
                  Toast.CENTER,
                );
                this.onPressBack();
              }, 1000);
            },
          );
        }
      })
      .catch(error => { });
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ViewTicketRequestsImportComponent);
