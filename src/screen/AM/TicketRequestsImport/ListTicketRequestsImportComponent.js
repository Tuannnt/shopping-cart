import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  TouchableOpacity,
  Dimensions,
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {ListItem, SearchBar, Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_TicketRequests from '../../../network/AM/API_TicketRequests';
import {AppStyles, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '../../../utils';
import Combobox from '../../component/Combobox';
import listCombobox from './listCombobox';
const SCREEN_WIDTH = Dimensions.get('window').width;
class ListTicketRequestsImportComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      model: {},
      List: [],
      isFetching: false,
    };
    this.staticParam = {
      CurrentPage: 1,
      Length: 10,
      search: {value: ''},
      QueryOrderBy: 'TicketRequestNo DESC',
      StartDate: new Date(),
      EndDate: new Date(),
      IsProcessed: null,
      RequestType: 'N',
      Status: 'W',
      Type: [],
    };
    this.Total = 0;
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },

      {
        Title: 'Không duyệt',
        Icon: 'export2',
        Type: 'antdesign',
        Value: 'export2',
        Checkbox: false,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
  }
  // componentDidMount = () => {
  //     this.time = setTimeout(() => {
  //         this.GetListAll()
  //     }, 300)
  // }
  // componentWillUnmount = () => {
  //     clearTimeout(this.time)
  // }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'TKImport') {
      this.updateSearch('');
    }
  }
  //Scroll to top
  upButtonHandler = () => {
    //OnCLick of Up button we scrolled the list to top
    this.ListView_Ref.scrollToOffset({offset: 0, animated: true});
  };
  //Scroll to end
  downButtonHandler = () => {
    //OnCLick of down button we scrolled the list to bottom
    this.ListView_Ref.scrollToEnd({animated: true});
  };
  //Scroll to Index
  goIndex = () => {
    this.ListView_Ref.scrollToIndex({animated: true, index: 5});
  };
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  renderFooter = () => {
    if (!this.state.isFetching) {
      return null;
    }
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  //Message
  ListEmpty = () => {
    if (this.state.List.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  _onEndReached = () => {
    this.nextPage();
    // if (this.Total > this.state.List.length) {
    // }
  };
  handleVoucherType = type => {
    if (!type) {
      return;
    }
    switch (type) {
      case 'N':
        return 'Nhập mua';
      case 'L':
        return 'Nhập hàng bán bị trả lại';
      case 'M':
        return 'Nhập thành phẩm sản xuất';
      case 'O':
        return 'Nhập khác';
    }
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Đề nghị nhập'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'AM'}
          // addForm={'addTicketRequests'}
          // CallbackFormAdd={() => Actions.addTicketRequests()}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  padding: 5,
                }}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian bắt đầu
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventStartDate: true})}>
                  <Text>
                    {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian kết thúc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventEndDate: true})}>
                  <Text>{FuncCommon.ConDate(this.staticParam.EndDate, 0)}</Text>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', padding: 5}}>
                <Text
                  style={[
                    AppStyles.Labeldefault,
                    {textAlign: 'center'},
                    styles.timeHeader,
                  ]}>
                  Lọc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionSearchDate()}>
                  <Icon
                    name={'down'}
                    type={'antdesign'}
                    size={18}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              style={[AppStyles.FormInput, {marginHorizontal: 10}]}
              onPress={() => this.onActionStatus()}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.state.VoucherTypeName
                    ? {color: 'black'}
                    : {color: AppColors.gray},
                ]}>
                {this.state.VoucherTypeName
                  ? this.state.VoucherTypeName
                  : 'Chọn nội dung...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  ncolor={AppColors.gray}
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                  color={AppColors.gray}
                />
              </View>
            </TouchableOpacity>
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.updateSearch(text)}
              value={this.staticParam.search.value}
            />
          </View>
        ) : null}
        <View style={{flex: 10}}>
          <FlatList
            style={{backgroundColor: '#fff', height: '100%'}}
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.List}
            renderItem={({item, index}) => (
              <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
                <ListItem
                  title={
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <Text style={AppStyles.Titledefault}>
                          {item.TicketRequestNo}
                        </Text>
                      </View>
                      {item.Status == 'Y' ? (
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {color: AppColors.AcceptColor},
                          ]}>
                          Đã duyệt
                        </Text>
                      ) : item.Status === 'W' ? (
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {color: AppColors.PendingColor},
                          ]}>
                          Chờ duyệt
                        </Text>
                      ) : (
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {color: AppColors.UntreatedColor},
                          ]}>
                          Không duyệt
                        </Text>
                      )}
                    </View>
                  }
                  subtitle={() => {
                    return (
                      <View style={{flexDirection: 'row'}}>
                        <View style={{flex: 4}}>
                          <Text style={AppStyles.Textdefault}>
                            Người yêu cầu: {item.EmployeeName}
                          </Text>
                          {/* <Text style={AppStyles.Textdefault}>
                            Đối tượng: {item.ObjectName}
                          </Text> */}
                          <Text style={[AppStyles.Textdefault]}>
                            Nội dung nhập: {item.VoucherTypeName}
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 2,
                            alignItems: 'flex-end',
                            justifyContent: 'flex-start',
                            textAlign: 'right',
                          }}>
                          <Text style={AppStyles.Textdefault}>
                            {item.RequestDateString}
                          </Text>
                        </View>
                      </View>
                    );
                  }}
                  bottomDivider
                  titleStyle={AppStyles.Titledefault}
                  //chevron
                  onPress={() => this.onView(item)}
                />
              </View>
            )}
            onEndReachedThreshold={0.5}
            onEndReached={this._onEndReached}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback => this.onClick(callback)}
            />
          </View>
        </View>
        {this.state.setEventStartDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearch(''),
                  this.setState({setEventStartDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.staticParam.StartDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setStartDate(setDate)}
            />
          </View>
        ) : null}
        {this.state.setEventEndDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearch(''), this.setState({setEventEndDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.staticParam.EndDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setEndDate(setDate)}
            />
          </View>
        ) : null}
        <MenuSearchDate
          value={this.state.ValueSearchDate}
          callback={this.CallbackSearchDate}
          eOpen={this.openMenuSearchDate}
        />
        <Combobox
          value={null}
          TypeSelect={'single'}
          callback={this.ChangeStatus}
          data={listCombobox.ListVoucherType}
          nameMenu={'Chọn tình trạng'}
          eOpen={this.openComboboxStatus}
          position={'bottom'}
        />
      </View>
    );
  }
  _openComboboxStatus() {}
  openComboboxStatus = d => {
    this._openComboboxStatus = d;
  };
  onActionStatus() {
    this._openComboboxStatus();
  }
  ChangeStatus = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === null) {
      return;
    }
    this.staticParam.VoucherType = rs.value;
    this.setState({VoucherTypeName: rs.text});
    this.updateSearch('');
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  getAll = () => {
    API_TicketRequests.GetTicketRequestsAll(this.staticParam)
      .then(res => {
        let _data = JSON.parse(res.data.data).data;
        if (!_data) {
          this.setState({refreshing: false});
          return;
        }
        let _list = _data;
        this.state.List = this.state.List.concat(_list);
        // this.Total = _data.list.recordsTotal || 0;
        if (this.staticParam.Status === 'W') {
          this.listtabbarBotom[0].Badge = JSON.parse(
            res.data.data,
          ).recordsFiltered;
        }
        FuncCommon.Data_Offline_Set('TicketRequestsImportAM', this.state.List);
        this.setState({List: this.state.List, refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };
  //Lấy dữ liệu
  GetListAll = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.getAll();
      } else {
        let data = await FuncCommon.Data_Offline_Get('TicketRequestsImportAM');
        this.setState({
          List: data,
          refreshing: false,
        });
      }
    });
  };
  //Xem
  onView(item) {
    Actions.viewTicketRequestsImportComponent(item);
  }
  //Sửa
  onEdit(item) {
    Actions.listOrderDetails(item);
  }
  //Xóa
  onDelete(item) {
    API_TicketRequests.delete(item.OrderGuid)
      .then(res => {
        Actions.pop();
      })
      .catch(error => {});
  }
  //Load lại
  onRefresh = () => {
    this.GetListAll();
  };
  //Tìm kiếm
  updateSearch = text => {
    this.staticParam.search.value = text;
    this.staticParam.CurrentPage = 1;
    this.setState({
      List: [],
    });
    this.GetListAll();
  };
  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetListAll();
  }
  onClick(data) {
    if (data == 'checkcircleo') {
      this.staticParam.Status = 'Y';
      this.updateSearch('');
    } else if (data == 'profile') {
      this.staticParam.Status = 'W';
      this.updateSearch('');
    } else if (data == 'export2') {
      this.staticParam.Status = 'N';
      this.updateSearch('');
    }
  }
}
const styles = StyleSheet.create({
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabCenter: {
    flex: 1,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    paddingTop: 10,
    paddingBottom: 10,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListTicketRequestsImportComponent);
