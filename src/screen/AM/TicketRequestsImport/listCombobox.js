import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Mã sản phẩm', width: 150},
    {title: 'Tên sản phẩm', width: 250},
    {title: 'Số PO', width: 250},
    {title: 'ĐVT', width: 70},
    {title: 'Số lượng YC', width: 100},
    {title: 'Đơn giá', width: 150},
    {title: 'Thành tiền', width: 200},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  ListVoucherType: [
    {
      value: '',
      text: 'Tất cả',
    },
    {
      value: 'N',
      text: 'Nhập mua',
    },
    {
      value: 'L',
      text: 'Nhập hàng bán bị trả lại',
    },
    {
      value: 'M',
      text: 'Nhập thành phẩm sản xuất',
    },
    {
      value: 'Q',
      text: 'Nhập hoàn trả sản xuất',
    },
    // {
    //   value: 'H',
    //   text: 'Nhập hàng mượn',
    // },
    {
      value: 'O',
      text: 'Nhập khác',
    },
  ],
};
