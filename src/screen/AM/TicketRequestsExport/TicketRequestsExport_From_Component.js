import React, {Component} from 'react';
import {
  Alert,
  KeyboardAvoidingView,
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  FlatList,
  TextInput,
  ScrollView,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import {Button, Divider, Icon as IconElement} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker';
import _ from 'lodash';
import {AppStyles, AppColors} from '@theme';
import {
  LoadingComponent,
  TabBar_Title,
  RequiredText,
  Combobox,
  ComboboxV2,
  OpenPhotoLibrary,
} from '@Component';
import Toast from 'react-native-simple-toast';
import controller from './controller';
import listCombobox from './listCombobox';
import {FuncCommon} from '../../../utils';
class TicketRequestsExport_From_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isEdit: false,
      Value: '',
      FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
      DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
      RequestDate: FuncCommon.ConDate(new Date(), 0),
      VoucherType: 'B',
      RequestType: 'X',
      ListStocker: [],
      StockerId: '',
      StockerName: '',
      StockerGuid: null,
      ListObject: [],
      ObjectGuid: props.ListAdd[0].ObjectGuid,
      ObjectId: props.ListAdd[0].ObjectId,
      ObjectName: props.ListAdd[0].ObjectName,
      ObjectAddress: props.ListAdd[0].ObjectAddress ?? '',
      rows: [],
      ListItemsAllJExcel: [],
      ListOrdersAll: [],
      ListWarehousesJexcel: [],
      ListUnitsJexcel: [],
      Description: props.ListAdd[0].Description,
      ListDateJexcel: [],
    };
  }
  componentDidMount = () => {
    this.getNumberAuto();
    this.GetInventoryByListItemId();
    this.GetCustomersAll('');
    this.GetEmployeesMH();
    this.GetItemsAllJExcel('');
    this.GetOrdersAll('');
    this.GetWarehousesJexcel('');
    this.GetUnitsAllJExcel('A');
  };
  getNumberAuto = () => {
    controller.getNumberAuto(rs => {
      this.setState({
        isEdit: rs.IsEdits,
        Value: rs.Value,
        TicketRequestNo: rs.Value,
      });
    });
  };
  GetInventoryByListItemId = () => {
    controller.GetInventoryByListItemId(this.props.ListAdd, rs => {
      this.setState({rows: rs});
    });
  };
  GetCustomersAll = txt => {
    controller.GetCustomersAll(
      txt,
      this.state.VoucherType,
      this.state.RequestType,
      rs => {
        this.setState({ListObject: rs});
      },
    );
  };
  GetEmployeesMH = () => {
    controller.GetEmployeesMH(rs => {
      this.setState({ListStocker: rs});
    });
  };
  GetItemsAllJExcel = txt => {
    controller.GetItemsAllJExcel(txt, rs => {
      this.setState({ListItemsAllJExcel: rs});
    });
  };
  GetOrdersAll = txt => {
    controller.GetOrdersAll(txt, rs => {
      this.setState({ListOrdersAll: rs});
    });
  };
  GetWarehousesJexcel = txt => {
    controller.GetWarehousesJexcel(txt, rs => {
      this.setState({ListWarehousesJexcel: rs});
    });
  };
  GetUnitsAllJExcel = txt => {
    controller.GetUnitsAllJExcel(txt, rs => {
      this.setState({ListUnitsJexcel: rs});
    });
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={[AppStyles.container]}>
            <TabBar_Title
              title={'Thêm mới đề nghị xuất'}
              callBack={() => this.BackList()}
            />
            <ScrollView>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  marginBottom: 10,
                  backgroundColor: 'white',
                  marginLeft: 0,
                }}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Số đề nghị <RequiredText />
                    </Text>
                  </View>
                  {this.state.isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={this.state.TicketRequestNo}
                      autoCapitalize="none"
                      onChangeText={txt =>
                        this.onChangeText('TicketRequestNo', txt)
                      }
                    />
                  )}
                  {!this.state.isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                        {this.state.TicketRequestNo}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người yêu cầu</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Bộ phận</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Ngày đề nghị</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.RequestDate}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        disabled={false}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date =>
                          this.onChangeText('RequestDate', date)
                        }
                      />
                    </View>
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Tên đối tượng</Text>
                  </View>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onAction_Object()}>
                    <Text
                      style={[
                        AppStyles.TextInput,
                        this.state.ObjectId == null
                          ? {color: AppColors.gray}
                          : {color: 'black'},
                      ]}>
                      {this.state.ObjectId !== null
                        ? this.state.ObjectName
                        : 'Chọn đối tượng'}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Thủ kho</Text>
                  </View>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onAction_Stocker()}>
                    <Text
                      style={[
                        AppStyles.TextInput,
                        this.state.StockerId == ''
                          ? {color: AppColors.gray}
                          : {color: 'black'},
                      ]}>
                      {this.state.StockerId !== ''
                        ? this.state.StockerName
                        : 'Chọn thủ kho'}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Nội dung xuất</Text>
                  </View>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => null}>
                    <Text style={[AppStyles.TextInput, {color: 'black'}]}>
                      {listCombobox.ListVoucherType(this.state.VoucherType)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Diễn giải</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={3}
                    value={this.state.Description}
                    onChangeText={Description =>
                      this.onChangeText('Description', Description)
                    }
                  />
                </View>
                <View style={{marginTop: 5}}>
                  {/* <View style={{ width: 120, marginLeft: 10 }}>
                      <Button
                        title="Thêm dòng"
                        type="outline"
                        size={15}
                        onPress={() => this.addRows()}
                        buttonStyle={{ padding: 5, marginBottom: 5 }}
                        titleStyle={{ marginLeft: 5 }}
                        icon={<IconElement name="pluscircleo" type="antdesign" color={AppColors.blue} size={14} />}
                      />
                    </View> */}
                  {/* Table */}
                  <ScrollView horizontal={true}>
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        {listCombobox.headerTable.map(item => {
                          if (
                            !this.state.TypeOfRequest ||
                            this.state.TypeOfRequest === 'TM'
                          ) {
                            if (item.hideInCommercialType) {
                              return null;
                            }
                          }
                          return (
                            <TouchableOpacity
                              key={item.title}
                              style={[AppStyles.table_th, {width: item.width}]}>
                              <Text style={AppStyles.Labeldefault}>
                                {item.title}
                              </Text>
                            </TouchableOpacity>
                          );
                        })}
                      </View>
                      <View>
                        {this.state.rows.length > 0 ? (
                          <View>
                            <FlatList
                              data={this.state.rows}
                              renderItem={({item, index}) => {
                                return this.itemFlatList(item, index);
                              }}
                              keyExtractor={(rs, index) => index.toString()}
                            />
                            {this.rowTotal()}
                          </View>
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'left'},
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View>
                <View
                  style={{flexDirection: 'row', padding: 10, marginBottom: 20}}>
                  <TouchableOpacity
                    style={[
                      AppStyles.containerCentered,
                      {
                        flex: 1,
                        padding: 10,
                        borderRadius: 10,
                        marginLeft: 5,
                        backgroundColor: AppColors.ColorButtonSubmit,
                      },
                    ]}
                    onPress={() => this.Insert()}>
                    <Text style={[AppStyles.Titledefault, {color: '#fff'}]}>
                      Lưu
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
            {this.state.ListObject.length === 0 ? null : (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeObject}
                data={this.state.ListObject}
                nameMenu={'Chọn kho'}
                eOpen={this.openCombobox_Object}
                position={'bottom'}
                value={this.state.ObjectId}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.search_Object(callback, textsearch)
                }
              />
            )}
            {this.state.ListStocker.length === 0 ? null : (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeStocker}
                data={this.state.ListStocker}
                nameMenu={'Chọn nhân viên'}
                eOpen={this.openCombobox_Stocker}
                position={'bottom'}
                value={this.state.StockerId}
              />
            )}
            {this.state.ListItemsAllJExcel.length === 0 ? null : (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeItemsAllJExcel}
                data={this.state.ListItemsAllJExcel}
                nameMenu={'Chọn mã hàng'}
                eOpen={this.openCombobox_ItemsAllJExcel}
                position={'bottom'}
                value={''}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.search_ItemsAllJExcel(callback, textsearch)
                }
              />
            )}
            {this.state.ListOrdersAll.length === 0 ? null : (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeOrders}
                data={this.state.ListOrdersAll}
                nameMenu={'Chọn đơn hàng'}
                eOpen={this.openCombobox_Orders}
                position={'bottom'}
                value={''}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.search_Orders(callback, textsearch)
                }
              />
            )}
            {this.state.ListWarehousesJexcel.length === 0 ? null : (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeWarehouses}
                data={this.state.ListWarehousesJexcel}
                nameMenu={'Chọn kho'}
                eOpen={this.openCombobox_Warehouses}
                position={'bottom'}
                value={''}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.search_Warehouses(callback, textsearch)
                }
              />
            )}
            {this.state.ListUnitsJexcel.length === 0 ? null : (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeUnits}
                data={this.state.ListUnitsJexcel}
                nameMenu={'Chọn đơn vị tính'}
                eOpen={this.openCombobox_Units}
                position={'bottom'}
                value={''}
                searchAPI={true}
                callback_SearchPaging={(callback, textsearch) =>
                  this.search_Units(callback, textsearch)
                }
              />
            )}
            {this.state.ListDateJexcel.length === 0 ? null : (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeDate}
                data={this.state.ListDateJexcel}
                nameMenu={'Chọn ngày giao'}
                eOpen={this.openCombobox_Date}
                position={'bottom'}
                value={''}
              />
            )}
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(0)}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        {/* Mã sản phẩm  */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(1)}]}
          onPress={() => this.onAction_ItemsAllJExcel(index)}>
          <View
            style={{flexDirection: 'row', alignItems: 'center', padding: 2}}>
            <Text style={[AppStyles.TextInput, {color: 'black'}]}>
              {row.ItemId ? row.ItemId : 'Chọn mã hàng'}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <IconElement
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        {/* Tên hàng  */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(2)}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.ItemName}
            autoCapitalize="none"
            onChangeText={txt => {
              this.handleChangeRows(txt, index, 'ItemName');
            }}
          />
        </TouchableOpacity>
        {/* Đơn hàng */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(3)}]}
          onPress={() => this.onAction_Orders(index)}>
          <View
            style={{flexDirection: 'row', alignItems: 'center', padding: 10}}>
            <Text
              style={[
                AppStyles.TextInput,
                row.OrderName,
                {color: 'black', fontSize: 16},
              ]}>
              {row.OrderNumber ? row.OrderNumber : 'Chọn đơn hàng'}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <IconElement
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        {/* Bao gói */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(4)}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Specification}
            autoCapitalize="none"
            onChangeText={txt => {
              this.handleChangeRows(txt, index, 'Specification');
            }}
          />
        </TouchableOpacity>
        {/* ĐVT */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(5)}]}
          onPress={() => this.onAction_Units(index)}>
          <View
            style={{flexDirection: 'row', alignItems: 'center', padding: 2}}>
            <Text style={[AppStyles.TextInput, {color: 'black'}]}>
              {row.UnitId ? row.UnitName : 'Chọn ĐVT'}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <IconElement
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        {/* Số lượng yêu cầu */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(6)}]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              {color: 'black', textAlign: 'right'},
            ]}
            underlineColorAndroid="transparent"
            value={FuncCommon.addPeriodTextInput(row.QuantityTR)}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(
                FuncCommon.CVNumber(txt),
                index,
                'QuantityTR',
              );
            }}
          />
        </TouchableOpacity>
        {/* Đơn giá mua */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(7)}]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              {color: 'black', textAlign: 'right'},
            ]}
            underlineColorAndroid="transparent"
            value={FuncCommon.addPeriodTextInput(row.UnitPrice)}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={txt => {
              this.handleChangeRows(
                FuncCommon.CVNumber(txt),
                index,
                'UnitPrice',
              );
            }}
          />
        </TouchableOpacity>
        {/* thành tiền */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(8)}]}>
          <Text
            style={[AppStyles.TextInput, {color: 'black', textAlign: 'right'}]}>
            {FuncCommon.addPeriodTextInput(row.QuantityTR * row.UnitPrice)}
          </Text>
        </TouchableOpacity>
        {/* ngày giao */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(9)}]}
          onPress={() => {
            this.onAction_Date(index);
          }}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {row.OrderDeliveryDateName === undefined
              ? 'Chọn thời gian'
              : row.OrderDeliveryDateName}
          </Text>
        </TouchableOpacity>
        {/* kho */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(10)}]}
          onPress={() => this.onAction_Warehouses(index)}>
          <View
            style={{flexDirection: 'row', alignItems: 'center', padding: 2}}>
            <Text style={[AppStyles.TextInput, {color: 'black'}]}>
              {row.CustomerId ? row.CustomerId : 'Chọn kho'}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <IconElement
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>

        {/* lô lót */}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: this.widthTable(11)}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={Note => {
              this.handleChangeRows(Note, index, 'Note');
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };
  rowTotal = () => {
    const {rows} = this.state;
    return (
      <View style={{flexDirection: 'row'}}>
        {listCombobox.headerTable.map((item, index) => {
          if (!this.state.TypeOfRequest || this.state.TypeOfRequest === 'TM') {
            if (item.hideInCommercialType) {
              return null;
            }
          }
          if (item.id === 'Qty') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text style={[AppStyles.Textdefault, {textAlign: 'right'}]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.QuantityTR || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          if (item.id === 'Amount') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text style={[AppStyles.Textdefault, {textAlign: 'right'}]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) =>
                        total + (+(row.QuantityTR * row.UnitPrice) || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          return (
            <TouchableOpacity
              key={index}
              style={[
                AppStyles.table_td_custom,
                {width: item.width, backgroundColor: '#f5f2f2'},
              ]}
            />
          );
        })}
      </View>
    );
  };
  //#region thông tin chung
  onChangeText = (key, value) => {
    this.setState({[key]: value});
  };
  _openCombobox_Object() {}
  openCombobox_Object = d => {
    this._openCombobox_Object = d;
  };
  onAction_Object = () => {
    this._openCombobox_Object();
  };
  search_Object = (callback, textsearch) => {
    controller.GetCustomersAll(
      textsearch,
      this.state.VoucherType,
      this.state.RequestType,
      rs => {
        callback(rs);
      },
    );
  };
  ChangeObject = rs => {
    if (rs !== null) {
      this.onChangeText('ObjectId', rs.value);
      this.onChangeText('ObjectName', rs.text);
      this.onChangeText('ObjectGuid', rs.ObjectGuid);
      this.onChangeText('ObjectAddress', rs.ObjectAddress);
    }
  };
  _openCombobox_Stocker() {}
  openCombobox_Stocker = d => {
    this._openCombobox_Stocker = d;
  };
  onAction_Stocker = () => {
    this._openCombobox_Stocker();
  };
  ChangeStocker = rs => {
    if (rs !== null) {
      this.onChangeText('StockerId', rs.value);
      this.onChangeText('StockerName', rs.text);
      this.onChangeText('StockerGuid', rs.EmployeeGuid);
    }
  };
  //#endregion
  //#region  chi tiết
  widthTable = i => {
    var w = listCombobox.headerTable[i].width;
    return w;
  };
  _openCombobox_ItemsAllJExcel() {}
  openCombobox_ItemsAllJExcel = d => {
    this._openCombobox_ItemsAllJExcel = d;
  };
  onAction_ItemsAllJExcel = index => {
    this.setState({currentIndexItem: index}, () => {
      this._openCombobox_ItemsAllJExcel();
    });
  };
  search_ItemsAllJExcel = (callback, textsearch) => {
    controller.GetItemsAllJExcel(textsearch, rs => {
      callback(rs);
    });
  };
  ChangeItemsAllJExcel = rs => {
    if (rs !== null) {
      this.handleChangeRows(rs.value, this.state.currentIndexItem, 'ItemId');
      controller.GetItemsById(rs.value, res => {
        if (res.length > 0) {
          this.handleChangeRows(
            res[0].ItemName !== null ? res[0].ItemName : '',
            this.state.currentIndexItem,
            'ItemName',
          );
          this.handleChangeRows(
            res[0].UnitId !== null ? res[0].UnitId : '',
            this.state.currentIndexItem,
            'UnitId',
          );
          this.handleChangeRows(
            res[0].UnitName !== null ? res[0].UnitName : '',
            this.state.currentIndexItem,
            'UnitName',
          );
          this.handleChangeRows(
            res[0].Specification !== null ? res[0].Specification : '',
            this.state.currentIndexItem,
            'Specification',
          );
        } else {
          this.handleChangeRows('', this.state.currentIndexItem, 'ItemName');
          this.handleChangeRows('', this.state.currentIndexItem, 'UnitId');
          this.handleChangeRows('', this.state.currentIndexItem, 'UnitName');
          this.handleChangeRows(
            '',
            this.state.currentIndexItem,
            'Specification',
          );
        }
      });
      controller.GetInventoryByItemId(rs.value, value => {
        this.handleChangeRows(
          value !== null ? value : '0',
          this.state.currentIndexItem,
          'QuantityReamin',
        );
        this.handleChangeRows(
          this.state.rows[this.state.currentIndexItem].Quantity - value,
          this.state.currentIndexItem,
          'QuantityUsed',
        );
      });
    }
  };
  _openCombobox_Orders() {}
  openCombobox_Orders = d => {
    this._openCombobox_Orders = d;
  };
  onAction_Orders = index => {
    this.setState({currentIndexItem: index}, () => {
      this._openCombobox_Orders();
    });
  };
  search_Orders = (callback, textsearch) => {
    controller.GetOrdersAll(textsearch, rs => {
      callback(rs);
    });
  };
  ChangeOrders = rs => {
    if (rs !== null) {
      this.handleChangeRows(
        rs.value,
        this.state.currentIndexItem,
        'OrderNumber',
      );
    }
  };
  _openCombobox_Warehouses() {}
  openCombobox_Warehouses = d => {
    this._openCombobox_Warehouses = d;
  };
  onAction_Warehouses = index => {
    this.setState({currentIndexItem: index}, () => {
      this._openCombobox_Warehouses();
    });
  };
  search_Warehouses = (callback, textsearch) => {
    controller.GetWarehousesJexcel(textsearch, rs => {
      callback(rs);
    });
  };
  ChangeWarehouses = rs => {
    if (rs !== null) {
      this.handleChangeRows(
        rs.value,
        this.state.currentIndexItem,
        'CustomerId',
      );
    }
  };
  _openCombobox_Units() {}
  openCombobox_Units = d => {
    this._openCombobox_Units = d;
  };
  onAction_Units = index => {
    this.setState({currentIndexItem: index}, () => {
      this._openCombobox_Units();
    });
  };
  search_Units = (callback, textsearch) => {
    controller.GetUnitsJexcel(textsearch, rs => {
      callback(rs);
    });
  };
  ChangeUnits = rs => {
    if (rs !== null) {
      this.handleChangeRows(rs.value, this.state.currentIndexItem, 'UnitId');
      this.handleChangeRows(rs.text, this.state.currentIndexItem, 'UnitName');
    }
  };
  _openCombobox_Date() {}
  openCombobox_Date = d => {
    this._openCombobox_Date = d;
  };
  onAction_Date = index => {
    controller.GetOrderDeliveryDatesAllJExcel(
      this.state.rows[index].OrderDetailGuid,
      rs => {
        this.setState({ListDateJexcel: rs, currentIndexItem: index}, () => {
          this._openCombobox_Date();
        });
      },
    );
  };
  ChangeDate = rs => {
    if (rs !== null) {
      this.handleChangeRows(
        rs.value,
        this.state.currentIndexItem,
        'OrderDeliveryDateId',
      );
      this.handleChangeRows(
        rs.text,
        this.state.currentIndexItem,
        'OrderDeliveryDateName',
      );
    }
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  //#endregion

  Insert = () => {
    var data = this.state.rows;
    for (var i = 0; i < data.length; i++) {
      if (data[i].ItemId !== '' && data[i].ItemId !== null) {
        if (
          parseFloat(data[i].QuantityTR) > parseFloat(data[i].QuantityReamin)
        ) {
          Alert.alert(
            'Xác nhận thông tin',
            'Dòng thứ ' +
              (i + 1) +
              ': Số lượng yêu cầu không được lớn hơn số lượng đáp ứng. Bạn có muốn tiếp tục thêm đơn hàng?.',
            [
              {
                text: 'Xác nhận',
                onPress: () => this.API_Isert(),
              },
              {
                text: 'Đóng',
                onPress: () => console.log('OK Pressed'),
                style: 'cancel',
              },
            ],
          );
        }
      }
    }
  };
  API_Isert = () => {
    controller.Insert(this.state, rs => {
      this.BackList();
    });
  };
  BackList() {
    Actions.pop();
    Actions.refresh({moduleId: 'back', ActionTime: new Date().getTime()});
  }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(TicketRequestsExport_From_Component);
