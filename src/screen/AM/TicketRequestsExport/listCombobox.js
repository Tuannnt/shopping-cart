export default {
  ListVoucherType(key) {
    switch (key) {
      case 'B':
        return 'Xuất bán';
      case 'D':
        return 'Xuất dùng nội bộ và sản xuất';
      case 'T':
        return 'Xuất trả hàng mua';
      case 'M':
        return 'Xuất hàng mượn';
      case 'X':
        return 'Xuất khác';
      default:
        break;
    }
  },

  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Mã sản phẩm', width: 150},
    {title: 'Tên sản phẩm', width: 200},
    {title: 'Số đơn hàng', width: 150},
    {title: 'Bao gói', width: 150},
    {title: 'ĐVT', width: 80},
    {title: 'SL yêu cầu', width: 100, id: 'Qty'},
    {title: 'Đơn giá', width: 150},
    {title: 'Thành tiền', width: 150, id: 'Amount'},
    {title: 'Ngày giao', width: 120},
    {title: 'Kho', width: 120},
    {title: 'Lô lót', width: 150},
  ],
};
