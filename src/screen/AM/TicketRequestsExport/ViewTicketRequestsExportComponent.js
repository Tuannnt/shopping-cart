import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  TouchableHighlight,
  Keyboard,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  RefreshControl,
  Animated,
  Alert,
  Dimensions,
} from 'react-native';
import { Header, Icon, Divider, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import API_TicketRequests from '../../../network/AM/API_TicketRequests';
import { AppStyles, AppColors } from '@theme';
import {
  CustomView,
  TabBarBottom,
  LoadingComponent,
  TabBar_Title,
  Combobox,
} from '@Component';
import { FuncCommon } from '../../../utils';
import Toast from 'react-native-simple-toast';
import listCombobox from './listCombobox';

const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const ListVoucherType = [
  {
    value: 'B',
    text: 'Xuất bán',
  },
  {
    value: 'D',
    text: 'Xuất dùng nội bộ và sản xuất',
  },
  {
    value: 'T',
    text: 'Xuất trả hàng mua',
  },
  {
    value: 'M',
    text: 'Xuất hàng mượn',
  },
  {
    value: 'X',
    text: 'Xuất khác',
  },
  {
    value: 'G',
    text: 'Xuất hàng gia công ngoài',
  },
];
class ViewTicketRequestsExportComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TicketRequestGuid: '',
      model: {
        TicketRequestNo: '',
      },
      List: [],
      isFetching: false,
      isHidden: false,
      checkInLogin: '',
      LoginName: '',
      loading: true,
      ViewItemDetail: null,
      //hiển thị thông tin chung
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
      listApprover: [],
    };
  }
  componentDidMount = () => {
    this.Skill();
    this.setViewOpen('ViewTotal');
    if (this.props.RecordGuid != undefined) {
      this.state.TicketRequestGuid = this.props.RecordGuid;
    } else {
      this.state.TicketRequestGuid = this.props.TicketRequestGuid;
    }
    Promise.all([
      this.GetTicketRequestsById(),
      this.GetTicketRequestsDetailsAll(),
      // this.getApprover(),
    ]);
  };
  ConvertDate = date => {
    if (date !== null && date != '' && date !== undefined) {
      var newdate = new Date(date);
      var month = newdate.getMonth() + 1;
      var day = newdate.getDate();
      var year = newdate.getFullYear();
      var hh = newdate.getHours();
      var mm = newdate.getMinutes();
      if (month < 10) {
        month = '0' + month;
      }
      if (day < 10) {
        day = '0' + day;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      return day + '/' + month + '/' + year;
    } else {
      return null;
    }
  };
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  helperVoucherType = val => {
    if (!val) {
      return '';
    }
    let voucherType = ListVoucherType.find(v => v.value == val);
    if (!voucherType) {
      return '';
    }
    return voucherType.text;
  };
  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Xem đề nghị'}
          callBack={() => this.onPressBack()}
        />
        <View style={{ flex: 1 }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewAll')}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
            </View>
            <Animated.View
              style={{
                transform: [{ rotate: rotateStart_ViewAll }, { perspective: 4000 }],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewAll !== true ? null : (
            <View style={[{ padding: 10, backgroundColor: '#fff' }]}>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Số phiếu :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.TicketRequestNo}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày yêu cầu :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.ConvertDate(this.state.model.RequestDate)}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Người yêu cầu :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.EmployeeName}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Bộ phận :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.DepartmentName}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Nội dung xuất :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.helperVoucherType(this.state.model.VoucherType)}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Tên đối tượng :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.ObjectName}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Diễn giải :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.state.model.Description}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>TT xử lý :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text
                    style={[
                      [AppStyles.Textdefault],
                      this.state.model.Status == 'Y'
                        ? { color: AppColors.AcceptColor }
                        : { color: AppColors.PendingColor },
                    ]}>
                    {this.state.model.StatusWF}
                  </Text>
                </View>
              </View>
            </View>
          )}
          <Divider />
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewDetail')}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  { rotate: rotateStart_ViewDetail },
                  { perspective: 4000 },
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewDetail !== true ? (
            <View style={{ flex: 1 }} />
          ) : (
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.state.List}
                style={{ flex: 1 }}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (
                  <View>
                    <ListItem
                      title={() => {
                        return (
                          <View>
                            <View style={{ flex: 1 }}>
                              <Text
                                style={[
                                  AppStyles.Titledefault,
                                  { width: '100%', flex: 10 },
                                ]}>
                                {item.ItemName}
                              </Text>
                            </View>
                          </View>
                        );
                      }}
                      subtitle={() => {
                        return (
                          <View style={{ marginTop: 5 }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                              <View style={{ flex: 1 }}>
                                <Text style={[AppStyles.Textdefault]}>
                                  ĐVT: {item.UnitName}
                                </Text>
                              </View>
                              <View style={{ flex: 1 }}>
                                <Text style={[AppStyles.Textdefault]}>
                                  SL YC: {item.Quantity}
                                </Text>
                              </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                              <View style={{ flex: 1 }}>
                                <Text style={[AppStyles.Textdefault]}>
                                  Đ.Giá: {this.addPeriod(item.UnitPriceBefore)}
                                </Text>
                              </View>
                              <View style={{ flex: 1 }}>
                                <Text style={[AppStyles.Textdefault]}>
                                  T.Tiền:{' '}
                                  <Text style={{ fontWeight: 'bold' }}>
                                    {this.addPeriod(item.Amount)}
                                  </Text>
                                </Text>
                              </View>
                            </View>
                          </View>
                        );
                      }}
                      bottomDivider
                      titleStyle={AppStyles.Titledefault}
                      onPress={() => this.openAcctionViewDetail(item)}
                    />
                  </View>
                )}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['red', 'green', 'blue']}
                  />
                }
              />
            </View>
          )}
        </View>
        {/*nút xử lý*/}
        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottom
            onDelete={() => this.Delete()}
            //key để quay trở lại danh sách
            backListByKey="TicketRequestsExport"
            // tiêu đề hiển thị trong popup xử lý phiếu
            Title={this.state.model.TicketRequestNo}
            //kiểm tra quyền xử lý
            Permisstion={this.state.model.Permisstion}
            //kiểm tra bước đầu quy trình
            checkInLogin={this.state.checkInLogin}
            onSubmitWF={(callback, CommentWF, approver) =>
              this.submitWorkFlow(callback, CommentWF, approver)
            }
            //Ý kiến
            keyCommentWF={{
              ModuleId: '26',
              Title: this.state.model.TicketRequestNo,
              RecordGuid: this.state.model.TicketRequestGuid,
              Type: 1,
            }}
          // listApprover={this.state.listApprover}
          />
        </View>
        {this.CustomView()}
      </View>
    );
  }
  Delete = () => {
    let id = { Id: this.state.TicketRequestGuid };
    this.setState({ loading: true });
    API_TicketRequests.DeleteTicketRequestById(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error.data);
      });
  };
  CustomView = () => {
    const { ViewItemDetail } = this.state;
    return ViewItemDetail === null ? null : (
      <CustomView eOpen={this.openViewDetail}>
        <View style={{ width: DRIVER.width - 50 }}>
          <View style={{ alignItems: 'center', padding: 10 }}>
            <Text style={[AppStyles.Titledefault]}>
              {'Chi tiết ' + ViewItemDetail.ItemId}
            </Text>
          </View>
          <Divider />
          <View style={{ marginBottom: 10 }}>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Mã tham chiếu :</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.VenderCode}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Mã sản phẩm :</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.ItemId}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Tên sản phẩm :</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.ItemName}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Quy cách :</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Textdefault}>{ViewItemDetail.Note}</Text>
              </View>
            </View>
            {/* <View style={{padding: 10, paddingBottom: 0, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Bao gói :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.ProductName}
                </Text>
              </View>
            </View> */}
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>ĐVT :</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.UnitAfter}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Số đơn hàng bán :</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.OrderNumberId}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Kho :</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.WarehouseName}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>SL yêu cầu:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.Quantity)}
                </Text>
              </View>
            </View>
            {/* <View style={{padding: 10, paddingBottom: 0, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>SL ĐM:</Text>
              </View>
              <View style={{flex: 2, alignItems: 'flex-end'}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.InventoryQuotaMin)}
                </Text>
              </View>
            </View> */}
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>SL quy đổi:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.Quantity)}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, paddingBottom: 0, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>SL TX:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.QuantityActual)}
                </Text>
              </View>
            </View>
            <View style={{ padding: 10, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Đơn giá:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.UnitPriceBefore)}
                </Text>
              </View>
            </View>
            {/* <View style={{padding: 10, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày giao:</Text>
              </View>
              <View style={{flex: 2, alignItems: 'flex-end'}}>
                <Text style={AppStyles.Textdefault}>
                  {ViewItemDetail.DeliveryDate}
                </Text>
              </View>
            </View> */}
            <Divider />
            <View style={{ padding: 10, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Thành tiền:</Text>
              </View>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Text style={AppStyles.Labeldefault}>
                  {FuncCommon.addPeriod(ViewItemDetail.Amount)}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </CustomView>
    );
  };
  _openViewDetail() { }
  openViewDetail = d => {
    this._openViewDetail = d;
  };
  openAcctionViewDetail = item => {
    this.setState({ ViewItemDetail: item }, () => {
      this._openViewDetail();
    });
  };
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'TKExport', ActionTime: new Date().getTime() });
  }
  //Lấy thông tin đề nghị xuất
  GetTicketRequestsById = () => {
    API_TicketRequests.GetTicketRequestsByIdExport(this.state.TicketRequestGuid)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        let check = {
          TicketRequestGuid: this.state.TicketRequestGuid,
          WorkFlowGuid: _data.WorkFlowGuid,
        };
        API_TicketRequests.CheckLogin(check)
          .then(res => {
            this.setState({
              checkInLogin: parseInt(res.data.data),
              model: _data,
            });
          })
          .catch(error => {
            console.log(error.data);
          });
      })
      .catch(error => {
        console.log(error);
      });
  };
  // danh sách người nhận quy trình
  getApprover = () => {
    API_TicketRequests.GetAppovers()
      .then(res => {
        let _data = JSON.parse(res.data.data);
        let list = [
          { value: null, label: 'Chọn người nhận' },
          ..._data.map(x => ({
            value: x.value,
            label: x.text,
          })),
          { value: 'KT', label: 'Kết thúc' },
        ];
        this.setState({ listApprover: list });
      })
      .catch(err => {
        console.log(err.data.data);
      });
  };
  //Lấy danh sách đề nghị xuất
  GetTicketRequestsDetailsAll = () => {
    this.setState({ refreshing: true });
    API_TicketRequests.GetTicketRequestsDetailsAll(this.state.TicketRequestGuid)
      .then(res => {
        this.state.List = this.state.List.concat(JSON.parse(res.data.data));
        this.setState({
          List: this.state.List,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF, approver) {
    console.log('callback==' + callback + CommentWF);
    let obj = {
      TicketRequestGuid: this.state.TicketRequestGuid,
      WorkFlowGuid: this.state.model.WorkFlowGuid,
      // LoginName: this.state.LoginName,
      Comment: CommentWF,
      Approvers: approver,
    };
    if (callback == 'D') {
      API_TicketRequests.Approve(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity('Xử lý thành công', Toast.LONG, Toast.CENTER);
            this.onPressBack();
          } else {
            Toast.showWithGravity(res.data.message, Toast.LONG, Toast.CENTER);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      API_TicketRequests.NotApprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.LONG,
              Toast.CENTER,
            );
            this.onPressBack();
          } else {
            Toast.showWithGravity(res.data.message, Toast.LONG, Toast.CENTER);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  }
  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 1, duration: 333 },
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      { toValue: 0, duration: 333 },
    );
  };
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ ViewAll: !this.state.ViewAll });
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ ViewDetail: !this.state.ViewDetail });
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ ViewDetail: true, ViewAll: true });
    }
  };
  //#endregion
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ViewTicketRequestsExportComponent);
