import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, Divider} from 'react-native-elements';
import {Text, View, StyleSheet} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import HomeComponent from '../../main/HomeComponent';
import {Actions} from 'react-native-router-flux';
import API_TicketRequests from '../../../network/AM/API_TicketRequests';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';

const styles = StyleSheet.create({
  subtitleStyle: {
    fontSize: 13,
    fontFamily: 'Arial',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  textRight: {
    textAlign: 'right',
  },
});

class ListTicketRequestsDetailsExportComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      RowGuid: null,
      model: {
        RowGuid: '',
        ItemId: '',
        ItemName: '',
        UnitName: '',
        Quantity: 0,
        QuantityActual: 0,
        UnitPriceBefore: 0,
        Amount: 0,
      },
      List: [],
      isFetching: false,
    };
  }
  componentDidMount(): void {
    this.state.RowGuid = this.props.RowGuid;
    this.GetTicketRequestsDetailsById();
  }

  ConvertDate = date => {
    if (date !== null && date != '' && date !== undefined) {
      var newdate = new Date(date);
      var month = newdate.getMonth() + 1;
      var day = newdate.getDate();
      var year = newdate.getFullYear();
      var hh = newdate.getHours();
      var mm = newdate.getMinutes();
      if (month < 10) month = '0' + month;
      if (day < 10) day = '0' + day;
      if (mm < 10) mm = '0' + mm;
      return day + '/' + month + '/' + year;
    } else {
      return null;
    }
  };
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết vật tư'}
          callBack={() => this.onPressBack()}
        />
        <View style={{padding: 20}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={AppStyles.Labeldefault}>Mã sản phẩm :</Text>
            <Text style={[AppStyles.Textdefault, styles.textRight, {flex: 1}]}>
              {this.state.model.ItemId}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <Text style={AppStyles.Labeldefault}>Tên sản phẩm :</Text>
            <Text style={[AppStyles.Textdefault, styles.textRight, {flex: 1}]}>
              {this.state.model.ItemName}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <Text style={AppStyles.Labeldefault}>Số PO :</Text>
            <Text style={[AppStyles.Textdefault, styles.textRight, {flex: 1}]}>
              {this.state.model.OrderNumberId}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <Text style={AppStyles.Labeldefault}>Đơn vị tính :</Text>
            <Text style={[AppStyles.Textdefault, styles.textRight, {flex: 1}]}>
              {this.state.model.UnitName}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <Text style={AppStyles.Labeldefault}>Số lượng yêu cầu :</Text>
            <Text style={[AppStyles.Textdefault, styles.textRight, {flex: 1}]}>
              {this.addPeriod(this.state.model.Quantity)}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <Text style={AppStyles.Labeldefault}>Số lượng thực xuất :</Text>
            <Text style={[AppStyles.Textdefault, styles.textRight, {flex: 1}]}>
              {this.addPeriod(this.state.model.QuantityActual)}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <Text style={AppStyles.Labeldefault}>Đơn giá :</Text>
            <Text style={[AppStyles.Textdefault, styles.textRight, {flex: 1}]}>
              {this.addPeriod(this.state.model.UnitPriceBefore)}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <Text style={AppStyles.Labeldefault}>Thành tiền :</Text>
            <Text style={[AppStyles.Textdefault, styles.textRight, {flex: 1}]}>
              {this.addPeriod(this.state.model.Amount)}
            </Text>
          </View>
        </View>
      </View>
    );
  }
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'chevron-thin-left'}
          type="entypo"
          size={30}
          onPress={() => this.onPressBack()}
        />
      </View>
    );
  }

  renderRightComponent() {
    return <View style={{flexDirection: 'row'}} />;
  }
  //Lấy thông tin chi tiết đề nghị xuất
  GetTicketRequestsDetailsById = () => {
    API_TicketRequests.GetTicketRequestsDetailsById(this.state.RowGuid)
      .then(res => {
        this.setState({
          model: JSON.parse(res.data.data),
        });
      })
      .catch(error => {});
  };
  onPressBack() {
    Actions.pop();
  }

  onPressHome() {
    Actions.home();
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ListTicketRequestsDetailsExportComponent);
