import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import {API_ProposePurchases, API, API_TicketRequests} from '@network';
import Toast from 'react-native-simple-toast';
import {FuncCommon} from '@utils';
import {Alert} from 'react-native';
export default {
  getNumberAuto(callback) {
    let val = {AliasId: 'QTDSCT_DNX', Prefixs: null, VoucherType: 'X'};
    API.GetNumberAutoAM(val)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var data = {Value: _data.Value, IsEdits: _data.IsEdit};
        callback(data);
      })
      .catch(error => {
        console.log(error);
      });
  },
  GetInventoryByListItemId(_list, callback) {
    var obj = {list: _list};
    API_ProposePurchases.GetInventoryByListItemId(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        console.log(error);
      });
  },
  GetCustomersAll(txt, voucherType, requestType, callback) {
    var obj = {
      CurrentPage: 1,
      Length: 50,
      Search: txt,
      VoucherType: voucherType,
      RequestType: requestType,
    };
    API_TicketRequests.GetCustomersAll(obj)
      .then(rs => {
        var _lstCustomers = JSON.parse(rs.data.data);
        var _reListCustomers = [];
        for (var i = 0; i < _lstCustomers.length; i++) {
          _reListCustomers.push({
            text:
              _lstCustomers[i].ObjectId + ' - ' + _lstCustomers[i].ObjectName,
            value: _lstCustomers[i].ObjectId,
            ObjectGuid: _lstCustomers[i].ObjectGuid,
            ObjectAddress: _lstCustomers[i].Address,
          });
        }
        callback(_reListCustomers);
      })
      .catch(error => {
        console.log(error.data);
      });
  },
  GetEmployeesMH(callback) {
    API_TicketRequests.GetEmployeesMH()
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        for (var i = 0; i < _data.length; i++) {
          list.push({
            text: _data[i].EmployeeId + ' - ' + _data[i].FullName,
            value: _data[i].EmployeeId,
            EmployeeGuid: _data[i].EmployeeGuid,
          });
        }
        callback(list);
      })
      .catch(error => {
        console.log(error);
      });
  },
  GetItemsAllJExcel(txt, callback) {
    var obj = {txtSearch: txt, Type: ''};
    API_ProposePurchases.GetItemsAllJExcel(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        console.log(error);
      });
  },
  GetOrdersAll(txt, callback) {
    var obj = {txtSearch: txt};
    API_ProposePurchases.GetOrdersAll(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        console.log(error);
      });
  },
  GetWarehousesJexcel(txt, callback) {
    var obj = {txtSearch: txt};
    API_ProposePurchases.GetWarehousesJexcel(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        console.log(error);
      });
  },
  GetUnitsAllJExcel(txt, callback) {
    API_TicketRequests.GetUnitsAllJExcel(txt)
      .then(rs => {
        var _lstUnits = JSON.parse(rs.data.data);
        var _reListUnits = [];
        for (var i = 0; i < _lstUnits.length; i++) {
          _reListUnits.push({
            text: _lstUnits[i].UnitName,
            value: _lstUnits[i].UnitId,
            UnitGuid: _lstUnits[i].UnitGuid,
          });
        }
        callback(_reListUnits);
      })
      .catch(error => {
        console.log(error);
      });
  },
  GetOrderDeliveryDatesAllJExcel(orderDetailGuid, callback) {
    API_TicketRequests.GetOrderDeliveryDatesAllJExcel({
      OrderDetailGuid: orderDetailGuid,
    })
      .then(rs => {
        var _lst = JSON.parse(rs.data.data);
        var _reList = [];
        for (var i = 0; i < _lst.length; i++) {
          _reList.push({
            text: _lst[i].name,
            value: _lst[i].id,
          });
        }
        callback(_reList);
      })
      .catch(error => {
        console.log(error);
      });
  },
  Insert(state, callback) {
    var modelauto = {
      IsEdit: true,
      Value: state.Value,
    };
    if (state.Value.toUpperCase() === state.TicketRequestNo.toUpperCase()) {
      modelauto.IsEdit = false;
    }
    var ListTicketRequests = [];
    var data = state.rows;
    if (data.filter(x => x.ItemId !== '').length === 0) {
      Toast.showWithGravity(
        'Yêu cầu nhập thông tin chi tiết.',
        Toast.LONG,
        Toast.CENTER,
      );
      return;
    } else if (state.ObjectId == undefined) {
      Toast.showWithGravity(
        'Yêu cầu chọn đối tượng.',
        Toast.LONG,
        Toast.CENTER,
      );
      return;
    }
    data.forEach(val => {
      ListTicketRequests.push({
        ItemGuid: val.ItemGuid !== '' ? val.ItemGuid : null,
        ItemId: val.ItemId !== '' ? val.ItemId : null,
        ItemName: val.ItemName,
        OrderDetailGuid: val.OrderDetailGuid,
        OrderNumberId: val.OrderNumber,
        ProductName: val.Specification,
        UnitBefore: val.UnitId,
        UnitName: val.UnitName,
        Quantity: val.QuantityTR,
        QuantityRemain: val.QuantityReamin,
        QuantityActual: 0,
        UnitPriceBefore: val.UnitPrice,
        Amount: val.QuantityTR * val.UnitPrice,
        OrderDeliveryDateId:
          val.OrderDeliveryDateId !== ''
            ? FuncCommon.ConDate(val.OrderDeliveryDateId, 2)
            : null,
        WarehouseId: val.CustomerId,
        PacketNumber: val.Note,
      });
    });
    debugger;
    var string = state.RequestDate.split('/');
    var _string = string[2] + '/' + string[1] + '/' + string[0];
    var model = {
      VoucherType: state.VoucherType,
      RequestType: state.RequestType,
      RequestDate: FuncCommon.ConDate(_string, 2),
      Description: state.Description ?? '',
      Title: '',
      TicketRequestNo: state.TicketRequestNo,
      ObjectId: state.ObjectId,
      ObjectGuid: state.ObjectGuid,
      ObjectName: state.ObjectName,
      ObjectAddress: state.ObjectAddress,
      StockerId: state.StockerId,
      StockerGuid: state.StockerGuid,
      StockerName: state.StockerName,
    };
    var fd = new FormData();
    fd.append('model', JSON.stringify(model));
    fd.append('TicketRequestDetails', JSON.stringify(ListTicketRequests));
    fd.append('auto', JSON.stringify(modelauto));
    API_TicketRequests.InsertTicketRequests(fd)
      .then(rs => {
        Toast.showWithGravity(rs.data.message, Toast.LONG, Toast.CENTER);
        if (rs.data.errorCode === 200) {
          callback(rs.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
};
