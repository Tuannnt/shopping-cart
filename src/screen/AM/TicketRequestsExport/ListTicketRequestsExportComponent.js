import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Dimensions,
  TouchableOpacity,
  Image,
  View,
  TextInput,
  Text,
  StyleSheet,
  StatusBar,
  FlatList,
  ScrollView,
  ActivityIndicator,
  RefreshControl,
  Alert,
} from 'react-native';
import {
  Header,
  Divider,
  Button,
  Input,
  ListItem,
  SearchBar,
  Icon,
  Badge,
} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_TicketRequests from '../../../network/AM/API_TicketRequests';
import TabNavigator from 'react-native-tab-navigator';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '../../../utils';

const SCREEN_WIDTH = Dimensions.get('window').width;
class ListTicketRequestsExportComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngay truoc
      model: {},
      List: [],
      isFetching: false,
      selectedTab: 'profile',
      openSearch: false,
      refreshing: true,
    };
    this.staticParam = {
      CurrentPage: 1,
      Length: 10,
      search: {value: ''},
      QueryOrderBy: 'CreatedDate DESC',
      StartDate: new Date(),
      EndDate: new Date(),
      RequestType: 'X',
      Status: 'W',
      VoucherType: null,
      IsProcessed: null,
      Type: [],
    };
    this.onEndReachedCalledDuringMomentum = true;
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
      // {
      //     Title: "Trả lại",
      //     Icon: "export2",
      //     Type: "antdesign",
      //     Value: "export2",
      //     Checkbox: false
      // }
    ];
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'TKExport') {
      this.updateSearch('');
    }
  }

  renderFooter = () => {
    if (!this.state.isFetching) {
      return null;
    }
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  //Message
  ListEmpty = () => {
    if (this.state.List && this.state.List.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Đề nghị xuất'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'AM'}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  padding: 5,
                }}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian bắt đầu
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventStartDate: true})}>
                  <Text>
                    {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian kết thúc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventEndDate: true})}>
                  <Text>{FuncCommon.ConDate(this.staticParam.EndDate, 0)}</Text>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', padding: 5}}>
                <Text
                  style={[
                    AppStyles.Labeldefault,
                    {textAlign: 'center'},
                    styles.timeHeader,
                  ]}>
                  Lọc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionSearchDate()}>
                  <Icon
                    name={'down'}
                    type={'antdesign'}
                    size={18}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.updateSearch(text)}
              value={this.staticParam.search.value}
            />
          </View>
        ) : null}
        <View style={{flex: 10}}>
          <FlatList
            data={this.state.List}
            style={{flex: 1}}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <View style={{}}>
                <ListItem
                  title={() => (
                    <View
                      style={[
                        AppStyles.ListItemTitle,
                        {
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        },
                      ]}>
                      <Text style={AppStyles.Labeldefault}>
                        {item.TicketRequestNo}
                      </Text>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {
                            color:
                              item.Status === 'Y'
                                ? AppColors.AcceptColor
                                : AppColors.PendingColor,
                          },
                        ]}>
                        {item.StatusWF}
                      </Text>
                    </View>
                  )}
                  subtitle={() => {
                    return (
                      <View>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{flex: 3}}>
                            <Text style={AppStyles.Textdefault}>
                              Người yêu cầu: {item.EmployeeName}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              Nội dung xuất: {item.VoucherTypeName}
                            </Text>
                          </View>
                          <View
                            style={{
                              alignItems: 'flex-end',
                              justifyContent: 'flex-start',
                            }}>
                            <Text style={AppStyles.Textdefault}>
                              {item.RequestDateString}
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  }}
                  bottomDivider
                  titleStyle={AppStyles.Titledefault}
                  //chevron
                  onPress={() => this.onView(item)}
                />
              </View>
            )}
            onEndReachedThreshold={0.5}
            onEndReached={this._onEndReached}
            onMomentumScrollBegin={() => {
              this.onEndReachedCalledDuringMomentum = false;
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
        </View>
        <View style={AppStyles.StyleTabvarBottom}>
          <TabBarBottomCustom
            ListData={this.listtabbarBotom}
            onCallbackValueBottom={callback => this.onClick(callback)}
          />
        </View>
        {this.state.setEventStartDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearch(''),
                  this.setState({setEventStartDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.staticParam.StartDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setStartDate(setDate)}
            />
          </View>
        ) : null}
        {this.state.setEventEndDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearch(''), this.setState({setEventEndDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.staticParam.EndDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setEndDate(setDate)}
            />
          </View>
        ) : null}
        <MenuSearchDate
          value={this.state.ValueSearchDate}
          callback={this.CallbackSearchDate}
          eOpen={this.openMenuSearchDate}
        />
      </View>
    );
  }
  _onEndReached = () => {
    this.nextPage();
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  getAll = () => {
    API_TicketRequests.GetTicketRequestsExport(this.staticParam)
      .then(res => {
        let _data = JSON.parse(res.data.data).data;
        if (!_data) {
          this.setState({refreshing: false});
          return;
        }
        this.state.List = this.state.List.concat(_data);
        FuncCommon.Data_Offline_Set('TicketRequestsExportAM', this.state.List);
        if (this.staticParam.Status === 'W') {
          this.listtabbarBotom[0].Badge = JSON.parse(
            res.data.data,
          ).recordsTotal;
        }
        this.setState({List: this.state.List, refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };
  //Lấy dữ liệu
  GetListAll = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.getAll();
      } else {
        let data = await FuncCommon.Data_Offline_Get('TicketRequestsExportAM');
        this.setState({
          List: data,
          refreshing: false,
        });
      }
    });
  };
  //Xem
  onView(item) {
    Actions.viewTicketRequestsExportComponent(item);
  }
  //Sửa
  onEdit(item) {
    Actions.listOrderDetails(item);
  }
  //Xóa
  onDelete(item) {
    API_TicketRequests.delete(item.OrderGuid)
      .then(res => {
        Actions.pop();
      })
      .catch(error => {});
  }
  //Load lại
  onRefresh = () => {
    this.updateSearch('');
  };
  //Tìm kiếm
  updateSearch = text => {
    this.staticParam.search.value = text;
    this.staticParam.CurrentPage = 1;
    this.setState(
      {
        List: [],
      },
      () => {
        this.GetListAll();
      },
    );
  };
  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetListAll();
  }
  onPressHome() {
    Actions.home();
  }
  onClick(data) {
    if (data == 'checkcircleo') {
      this.staticParam.Status = 'Y';
      this.updateSearch('');
    } else if (data == 'profile') {
      this.staticParam.Status = 'W';
      this.updateSearch('');
    } else if (data == 'export2') {
      this.staticParam.Status = 'T';
      this.updateSearch('');
    }
  }
}

const styles = StyleSheet.create({
  itemName: {
    fontSize: 13,
    color: '#231F20',
    fontWeight: '600',
    textAlign: 'center',
  },
  subtitleStyle: {
    fontSize: 13,
    fontFamily: 'Arial',
  },
  titleStyle: {
    fontSize: 13,
    fontWeight: 'bold',
  },
  searchcontainer: {
    backgroundColor: 'white',
    height: 50,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0, //no effect
    shadowColor: 'white', //no effect
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 23,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 23,
    color: '#f6b801',
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListTicketRequestsExportComponent);
