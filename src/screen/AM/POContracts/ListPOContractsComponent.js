import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, View, TextInput } from "react-native";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import TabNavigator from "react-native-tab-navigator";
import API_POContracts from "../../../network/AM/API_POContracts";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import FormSearch from '../../component/FormSearch';
class ListPOContractsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 15,
                Search: "",
                QueryOrderBy: "SignDate",
                Status: 1
            },
            selected: undefined,
            selectedTab: 'checkcircleo',
            openSearch: false
        };
        this.listtabbarBotom = [
            {
                Title: "Trang chủ",
                Icon: "home",
                Type: "antdesign",
                Value: "home",
                Checkbox: false
            },
            {
                Title: "Chưa thực hiện",
                Icon: "profile",
                Type: "antdesign",
                Value: "profile",
                Checkbox: false
            },
            {
                Title: "Đang thực hiện",
                Icon: "checkcircleo",
                Type: "antdesign",
                Value: "checkcircleo",
                Checkbox: true
            }
        ]
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_POContracts.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        this.GetAll();
    }

    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                Status: this.state.staticParam.Status
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Hợp đồng mua hàng'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'AM'}
                />
                {this.state.openSearch == true ?
                    <FormSearch
                        CallbackSearch={(callback) => this.updateSearch(callback)}
                    />
                    : null}
                <View style={{ flex: 9 }}>
                    <FlatList
                        style={{ height: '100%'}}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.RowGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family }}>
                                <ListItem
                                    title={item.POContractNo}
                                    titleStyle={AppStyles.Titledefault}
                                    subtitle={() => {
                                        return (
                                            <View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ width: '60%' }}>
                                                        <Text style={AppStyles.Textdefault}>Số hợp đồng: {item.POContractNo}</Text>
                                                        <Text
                                                            style={AppStyles.Textdefault}>NV mua hàng:{item.EmployeeName}</Text>
                                                    </View>
                                                    <View style={{ width: '40%' }}>
                                                        <Text style={AppStyles.Textdefault}>Ngày ký: {this.customDate(item.SignDate)}</Text>
                                                        <Text style={AppStyles.Textdefault}>G.Trị hợp đồng:{this.addPeriod(item.AmountOC)}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    onPress={() => this.openView(item.POContractGuid, item.POContractNo)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                    <View style={AppStyles.StyleTabvarBottom}>
                        <TabBarBottomCustom
                            ListData={this.listtabbarBotom}
                            onCallbackValueBottom={(callback) => this.onClick(callback)}
                        />
                    </View>
                </View>
            </View>
        );
    }

    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                StartDate: this.state.staticParam.StartDate,
                EndDate: this.state.staticParam.EndDate,
                Status: this.state.staticParam.Status
            }
        });
        this.GetAll();
    }
    onPressHome() {
        Actions.app();
    }
    //Load lại
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.GetAll();
    };
    onClick(data) {
        if (data == 'checkcircleo') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 15;
            this.state.staticParam.Search = '';
            this.state.staticParam.Status = 1;
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    Status: this.state.staticParam.Status
                }
            });
            this.GetAll();
        }
        else if (data == 'profile') {
            this.state.list = [];
            this.state.staticParam.CurrentPage = 1;
            this.state.staticParam.Length = 15;
            this.state.staticParam.Search = '';
            this.state.staticParam.Status = 0;
            this.setState({
                staticParam: {
                    CurrentPage: this.state.staticParam.CurrentPage,
                    Length: this.state.staticParam.Length,
                    Search: this.state.staticParam.Search,
                    QueryOrderBy: this.state.staticParam.QueryOrderBy,
                    StartDate: this.state.staticParam.StartDate,
                    EndDate: this.state.staticParam.EndDate,
                    Status: this.state.staticParam.Status
                }
            });
            this.GetAll();
        }
        else if (data === 'home') {
            Actions.app();
        }
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
    openView(id, ContractNo) {
        Actions.amitemPOContracts({ RecordGuid: id, ContractNo: ContractNo });
    }
}

const styles = StyleSheet.create({
    itemName: {
        borderBottomWidth: 1,
        borderBottomColor: '#3366CC'
    },
    subtitleStyle: {
        fontSize: 12, fontFamily: 'Arial'
    },
    titleStyle: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    searchcontainer: {
        backgroundColor: 'white',
        height: 50,
        paddingLeft: 10,
        paddingRight: 10,
        borderWidth: 0, //no effect
        shadowColor: 'white', //no effect
        borderBottomColor: 'transparent',
        borderTopColor: 'transparent',
    },
    itemIcon: {
        fontWeight: '600',
        fontSize: 30,
        color: 'black'
    },
    itemIconClick: {
        fontWeight: '600',
        fontSize: 30,
        color: '#f6b801'
    },
    input: {
        margin: 10,
        height: 40,
        borderColor: '#C0C0C0',
        borderWidth: 1
    }
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListPOContractsComponent);
