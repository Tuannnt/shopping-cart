import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_POContracts from '../../../network/AM/API_POContracts';
import ErrorHandler from '../../../error/handler';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  hidden: {
    display: 'none',
  },
  StatusWFR: {
    color: 'red',
  },
  StatusWFG: {
    color: 'green',
  },
  Lable: {
    fontSize: 13,
    flex: 1,
    fontWeight: 'bold',
  },
  Content: {
    fontSize: 13,
    flex: 2,
  },
});

class ItemPOContractsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
      listDetail: [],
      staticParam: {
        QueryOrderBy: 'ItemName desc',
      },
      isHidden: false,
      loading: true,
    };
  }

  componentDidMount(): void {
    this.getItem();
  }

  getItem() {
    let id = this.props.RecordGuid;
    API_POContracts.GetItem(id)
      .then(res => {
        this.setState({Data: JSON.parse(res.data.data)});
        this.GetDetail();
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log(error.data.data);
      });
  }
  GetDetail = () => {
    this.setState({refreshing: true});
    let id = this.props.ContractNo;
    API_POContracts.ListDetail({ContractNo: id})
      .then(res => {
        this.state.listDetail = JSON.parse(res.data.data);
        this.setState({
          list: this.state.listDetail,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };
  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };

  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết hợp đồng'}
          callBack={() => this.onPressBack()}
        />
        <View style={{flexDirection: 'row', padding: 10}}>
          <View style={{flex: 10}}>
            <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
          </View>
          <View style={{flex: 1}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHidden == false
                  ? this.setState({isHidden: true})
                  : this.setState({isHidden: false})
              }
            />
          </View>
        </View>
        <View>
          <View
            style={[
              this.state.isHidden == true ? styles.hidden : '',
              {padding: 20, backgroundColor: '#fff', flexDirection: 'row'},
            ]}>
            <View style={{flex: 1}}>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Số hợp đồng :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Ngày ký :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Tình trạng :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Nhà cung cấp :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Giá trị hợp đồng :
              </Text>
            </View>
            <View style={{flex: 2, alignItems: 'flex-end'}}>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.state.Data.PocontractNo}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.customDate(this.state.Data.SignDate)}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.state.Data.Status == 0
                  ? 'Chưa thực hiện'
                  : this.state.Data.Status == 1
                  ? 'Đang thực hiện'
                  : this.state.Data.Status == 2
                  ? 'Đã thanh lý'
                  : this.state.Data.Status == 3
                  ? 'Đã hủy bỏ'
                  : ''}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.state.Data.ObjectName}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.addPeriod(this.state.Data.AmountOc)}
              </Text>
            </View>
          </View>
          <Divider style={{marginBottom: 10}} />
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <Text style={[AppStyles.Titledefault, {flex: 1, paddingLeft: 10}]}>
              Chi tiết
            </Text>
            <Icon
              style={{color: 'black', flex: 1}}
              name={'attach-file'}
              type="MaterialIcons"
              size={20}
              onPress={() => this.openAttachments()}
            />
          </View>
        </View>
        {this.state.listDetail.length > 0 ? (
          <ScrollView>
            {this.state.listDetail.map((item, index) => (
              <View style={{}}>
                <ListItem
                  title={item.Description}
                  titleStyle={AppStyles.Titledefault}
                  subtitle={() => {
                    return (
                      <View>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{width: '50%'}}>
                            <Text style={AppStyles.Textdefault}>
                              Số lượng: {this.addPeriod(item.Quantity)}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              ĐVT : {item.UnitName}
                            </Text>
                          </View>
                          <View style={{width: '50%'}}>
                            <Text style={AppStyles.Textdefault}>
                              Đơn giá: {this.addPeriod(item.UnitPrice)}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              Thành tiền: {this.addPeriod(item.AmountOc)}
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  }}
                />
              </View>
            ))}
          </ScrollView>
        ) : (
          <View>
            <Text>Không có dữ liệu</Text>
          </View>
        )}
      </View>
    );
  }
  onPressBack() {
    Actions.pop();
  }
  openAttachments() {
    var obj = {
      ModuleId: '9',
      RecordGuid: this.state.Data.PocontractGuid,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemPOContractsComponent);
