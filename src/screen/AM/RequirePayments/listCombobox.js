import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  dropDownType: [
    {value: null, text: 'Tất cả'},
    {
      value: 'N',
      text: 'Chưa thanh toán',
    },
    {
      value: 'P',
      text: 'Đã thanh toán một phần',
    },
    {
      value: 'Y',
      text: 'Đã thanh toán hết',
    },
  ],
  //#endregion
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Số Hóa đơn', width: 200},
    {title: 'Ngày hóa đơn', width: 200},
    {title: 'Tên nhà cung cấp', width: 350},
    {title: 'Số đơn hàng', width: 200},
    {title: 'Số tiền', width: 200},
    {title: 'Hạn thanh toán', width: 200},
    {title: 'Hình thức TT', width: 200},
    {title: 'Người nhận', width: 200},
    {title: 'Số tài khoản', width: 200},
    {title: 'Ngân hàng', width: 350},
    {title: 'Chi nhánh', width: 200},
    {title: 'Ghi chú', width: 200},
  ],
  list: [
    {width: 40},
    {width: 200},
    {width: 200},
    {width: 350},
    {width: 200},
    {width: 200, id: 'total'},
    {width: 200},
    {width: 200},
    {width: 200},
    {width: 200},
    {width: 350},
    {width: 200},
    {width: 200},
  ],
};
