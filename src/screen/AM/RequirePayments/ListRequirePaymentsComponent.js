import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import { Header, SearchBar, Icon, Badge } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { API_AM } from '@network';
import { Container, ListItem } from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import MenuSearchDate from '../../component/MenuSearchDate';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { FuncCommon } from '@utils';
import TabBar from '../../component/TabBar';
import DatePicker from 'react-native-date-picker';
import Combobox from '../../component/Combobox';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import listCombobox from './listCombobox';
import LoadingComponent from '../../component/LoadingComponent';
const SCREEN_WIDTH = Dimensions.get('window').width;
const dropDownType = listCombobox.dropDownType;
class indexAdvances extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước

      EvenFromSearch: false,
      name: '',
      refreshing: true,
      list: [],
      ListCountNoti: [],
      Notapprove: '',
      selectedTab: 'profile',
      Notapprove_Advance: '',
      loading: false,
    };
    this.staticParam = {
      CurrentPage: 1,
      StartDate: new Date(),
      EndDate: new Date(),
      CurrentPage: 0,
      Length: 10,
      search: {
        value: '',
      },
      IsCurrency: null,
      PartyPayment: null,
      IsPayment: '1',
      QueryOrderBy: 'PaymentDate DESC',
    };
    this.Total = 0;
  }

  loadMoreData() {
    if (this.Total > this.state.list.length) {
      this.GetAll();
    }
  }
  _openMenuSearchDate() { }
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  _openComboboxType() { }
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    this.staticParam.IsCurrency = rs.value;
    this.staticParam.IsCurrencyName = rs.text;
    this.updateSearch('');
  };
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  render() {
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <Container>
        <View style={{ flex: 1 }}>

          <View style={{ flex: 1 }}>
            <TabBar
              title={'Đề nghị TT NCC'}
              FormSearch={true}
              CallbackFormSearch={callback =>
                this.setState({ EvenFromSearch: callback })
              }
              // addForm={true}
              // CallbackFormAdd={() => {
              //   Actions.addAdvances();
              // }}
              BackModuleByCode={'AM'}
            />
            {this.state.EvenFromSearch == true ? (
              <View style={{ flexDirection: 'column' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() =>
                        this.setState({ setEventStartDate: true })
                      }>
                      <Text>
                        {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({ setEventEndDate: true })}>
                      <Text>
                        {FuncCommon.ConDate(this.staticParam.EndDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ flexDirection: 'column', padding: 5 }}>
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        { textAlign: 'center' },
                        styles.timeHeader,
                      ]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput, { marginHorizontal: 10 }]}
                  onPress={() => this.onActionComboboxType()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.staticParam.IsCurrency
                        ? { color: 'black' }
                        : { color: AppColors.gray },
                    ]}>
                    {this.staticParam.IsCurrencyName
                      ? this.staticParam.IsCurrencyName
                      : 'Chọn loại...'}
                  </Text>
                  <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <Icon
                      color={AppColors.gray}
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{ backgroundColor: '#e1ecf4' }}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={text => this.updateSearch(text)}
                  value={this.staticParam.search.value}
                />
              </View>
            ) : null}
            {this.state.list ? (
              <View style={{ flex: 1 }}>
                <FlatList
                  style={{ marginBottom: 20, height: '100%' }}
                  ref={ref => {
                    this.ListView_Ref = ref;
                  }}
                  ListEmptyComponent={this.ListEmpty}
                  keyExtractor={item => item.BankGuid}
                  data={this.state.list}
                  renderItem={({ item, index }) => (
                    <ScrollView style={{}}>
                      <View>
                        <ListItem onPress={() => this.openView(item)}>
                          <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 3 }}>
                              <Text style={[AppStyles.Titledefault]}>
                                {item.RequiredId}
                              </Text>
                              <Text style={[AppStyles.Textdefault]}>
                                Người đề nghị: {item.EmployeeName}
                              </Text>
                              <Text style={[AppStyles.Textdefault]}>
                                Diễn giải: {item.Title}
                              </Text>
                            </View>
                            <View
                              style={{
                                flex: 2,
                                alignItems: 'flex-end',
                                justifyContent: 'center',
                              }}>
                              {item.Status == 'Y' ? (
                                <Text
                                  style={[
                                    {
                                      justifyContent: 'center',
                                      color: AppColors.AcceptColor,
                                    },
                                    AppStyles.Textdefault,
                                  ]}>
                                  {item.StatusWF}
                                </Text>
                              ) : (
                                <Text
                                  style={[
                                    {
                                      justifyContent: 'center',
                                      color: AppColors.PendingColor,
                                    },
                                    AppStyles.Textdefault,
                                  ]}>
                                  {item.StatusWF}
                                </Text>
                              )}
                              <Text style={[AppStyles.Textdefault]}>
                                {item.RequiredDateString}
                              </Text>
                              <Text style={[AppStyles.Textdefault]}>
                                {this.addPeriod(item.Amount)}
                              </Text>
                            </View>
                          </View>
                        </ListItem>
                      </View>
                    </ScrollView>
                  )}
                  onEndReached={() => this.loadMoreData()}
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={this._onRefresh}
                      tintColor="#f5821f"
                      titleColor="#fff"
                      colors={['black']}
                    />
                  }
                />
              </View>
            ) : null}
            {this.state.setEventStartDate === true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 1000,
                  width: SCREEN_WIDTH,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.updateSearch(''),
                      this.setState({ setEventStartDate: false });
                  }}>
                  <Text style={{ textAlign: 'right' }}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={this.staticParam.StartDate}
                  mode="date"
                  style={{ width: SCREEN_WIDTH }}
                  onDateChange={setDate => this.setStartDate(setDate)}
                />
              </View>
            ) : null}
            {this.state.setEventEndDate === true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 1000,
                  width: SCREEN_WIDTH,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.updateSearch(''),
                      this.setState({ setEventEndDate: false });
                  }}>
                  <Text style={{ textAlign: 'right' }}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={this.staticParam.EndDate}
                  mode="date"
                  style={{ width: SCREEN_WIDTH }}
                  onDateChange={setDate => this.setEndDate(setDate)}
                />
              </View>
            ) : null}
            <MenuSearchDate
              value={this.state.ValueSearchDate}
              callback={this.CallbackSearchDate}
              eOpen={this.openMenuSearchDate}
            />
            {dropDownType.length > 0 ? (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeType}
                data={dropDownType}
                nameMenu={'Chọn loại'}
                eOpen={this.openComboboxType}
                position={'bottom'}
                value={this.staticParam.IsCurrency}
              />
            ) : null}
          </View>
        </View>
      </Container>
    );
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'profile':
        this.onClickPending();
        break;
      case 'checkcircleo':
        this.onClickAprover();
        break;
      case 'export2':
        this.onClickNotApprove();
        break;
      default:
        break;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.updateSearch('');
    }
  }
  // sự kiện chờ duyệt
  onClickPending = () => {
    this.staticParam.Status = 'W';
    this.state.selectedTab = 'profile';
    this.setState({
      stylex: 1,
    });
    this.setState({
      list: [],
      selectedTab: this.state.selectedTab,
    });
    this.updateSearch('');
  };
  //Load lại
  _onRefresh = () => {
    this.updateSearch('');
    this.setState({ refreshing: true });
  };
  onClickAprover = () => {
    this.staticParam.Status = 'Y';
    this.state.selectedTab = 'checkcircleo';
    this.setState({
      stylex: 2,
    });
    this.setState({
      list: [],
      selectedTab: this.state.selectedTab,
    });
    this.updateSearch('');
  };
  onClickNotApprove = () => {
    this.staticParam.Status = 'R';
    this.state.selectedTab = 'export2';
    this.setState({
      stylex: 3,
    });
    this.setState({
      list: [],
      selectedTab: this.state.selectedTab,
    });
    this.updateSearch('');
  };
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({ EvenFromSearch: true });
    } else {
      this.setState({ EvenFromSearch: false });
    }
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  updateSearch = text => {
    this.staticParam.search.value = text;
    this.setState(
      {
        list: [],
      },
      () => {
        this.GetAll(true);
      },
    );
  };
  listAll = s => {
    if (s === true) {
      this.staticParam.CurrentPage = 0;
    }
    this.staticParam.CurrentPage++;
    API_AM.RequirePayments_ListAll(this.staticParam)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        if (s) {
          this.state.list = _data.data;
        } else {
          this.state.list = this.state.list.concat(_data.data);
        }
        this.Total = _data.recordsTotal;
        FuncCommon.Data_Offline_Set('RequirePayments', this.state.list);
        this.setState({
          loading: false,
          list: this.state.list,
          refreshing: false,
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log('===========> error');
        console.log(error);
      });
  };
  GetAll = s => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.listAll(s);
      } else {
        let data = await FuncCommon.Data_Offline_Get('RequirePayments');
        this.setState({
          list: data,
          refreshing: false,
        });
      }
    });
  };
  openView(item) {
    Actions.ViewRequirePayments({
      RecordGuid: item.RequiredGuid,
      Permisstion: item.Permisstion,
      StatusWF: item.StatusWF,
      PermissEdit: item.PermissEdit,
    });
  }

  clickItem(item) {
    console.log('================> ' + JSON.stringify(item));
    switch (item) {
      case 'Back':
        Actions.app();
        break;
      case 'Open':
        Actions.openAdvances();
        break;
      default:
        break;
    }
  }
}
const styles = StyleSheet.create({
  statusNew: {
    marginTop: 5,
    width: 8,
    height: 8,
    borderRadius: 4,
    borderWidth: 0.1,
    borderColor: 'white',
    backgroundColor: '#4baf57',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 23,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 23,
    color: '#f6b801',
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(indexAdvances);
