import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import { Header, SearchBar, Icon, Badge } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { API_Advances } from '@network';
import API from '../../../network/API';
import { Container, ListItem } from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import MenuSearchDate from '../../component/MenuSearchDate';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { FuncCommon } from '@utils';
import TabBar from '../../component/TabBar';
import DatePicker from 'react-native-date-picker';
import Combobox from '../../component/Combobox';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import listCombobox from './listCombobox';
import LoadingComponent from '../../component/LoadingComponent';
const SCREEN_WIDTH = Dimensions.get('window').width;
const dropDownType = listCombobox.dropDownType;
class indexAdvances extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      EvenFromSearch: true,
      name: '',
      refreshing: false,
      list: [],
      ListCountNoti: [],
      Notapprove: '',
      selectedTab: 'profile',
      Notapprove_Advance: '',
      loading: true,
      isFetching: false
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
      // {
      //   Title: 'Trả lại',
      //   Icon: 'export2',
      //   Type: 'antdesign',
      //   Value: 'export2',
      //   Checkbox: false,
      // },
    ];
    this.staticParam = {
      CurrentPage: 1,
      StartTime: new Date(),
      EndTime: new Date(),
      Status: 'W',
      Type: '',
      CurrentPage: 0,
      Length: 10,
      search: {
        value: '',
      },
      stylex: 0,
      AdvanceForm: null,
      IsCurrency: 'N',
      TypeOfTicket: 'A',
      EmployeeId: null,
      QueryOrderBy: 'ActualRefundDate DESC',
    };
    this.Total = 0;
  }

  loadMoreData() {
    if (this.Total > this.state.list.length) {
      this.GetAll();
    }
  }
  _openMenuSearchDate() { }
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  setStartTime = date => {
    this.staticParam.StartTime = date;
    this.GetAll(true);
  };
  setEndTime = date => {
    this.staticParam.EndTime = date;
    this.GetAll(true);
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartTime = new Date(callback.start);
      this.staticParam.EndTime = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.GetAll(true);
    }
  };
  _openComboboxType() { }
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    this.staticParam.TypeOfTicket = rs.value;
    this.staticParam.TypeOfTicketName = rs.text;
    this.GetAll(true);
  };
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  render() {
    const title =
      this.staticParam.TypeOfTicket === 'A'
        ? 'Phiếu tạm ứng'
        : 'Phiếu thanh toán';
    return this.state.loading ? (
      <LoadingComponent />
    ) : (
      <Container>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <TabBar
              title={title}
              FormSearch={true}
              CallbackFormSearch={callback =>
                this.setState({ EvenFromSearch: callback })
              }
              // addForm={true}
              // CallbackFormAdd={() => {
              //   Actions.addAdvances();
              // }}
              BackModuleByCode={'AM'}
            />
            {this.state.EvenFromSearch == true ? (
              <View style={{ flexDirection: 'column' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian bắt đầu
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() =>
                        this.setState({ setEventStartTime: true })
                      }>
                      <Text>
                        {FuncCommon.ConDate(this.staticParam.StartTime, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      padding: 5,
                    }}>
                    <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                      Thời gian kết thúc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.setState({ setEventEndTime: true })}>
                      <Text>
                        {FuncCommon.ConDate(this.staticParam.EndTime, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ flexDirection: 'column', padding: 5 }}>
                    <Text
                      style={[
                        AppStyles.Labeldefault,
                        { textAlign: 'center' },
                        styles.timeHeader,
                      ]}>
                      Lọc
                    </Text>
                    <TouchableOpacity
                      style={[AppStyles.FormInput]}
                      onPress={() => this.onActionSearchDate()}>
                      <Icon
                        name={'down'}
                        type={'antdesign'}
                        size={18}
                        color={AppColors.gray}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput, { marginHorizontal: 10 }]}
                  onPress={() => this.onActionComboboxType()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.staticParam.TypeOfTicket
                        ? { color: 'black' }
                        : { color: AppColors.gray },
                    ]}>
                    {this.staticParam.TypeOfTicketName
                      ? this.staticParam.TypeOfTicketName
                      : 'Chọn loại...'}
                  </Text>
                  <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <Icon
                      color={AppColors.gray}
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
                <SearchBar
                  placeholder="Tìm kiếm..."
                  lightTheme
                  round
                  inputContainerStyle={{ backgroundColor: '#e1ecf4' }}
                  containerStyle={AppStyles.FormSearchBar}
                  onChangeText={text => this.updateSearch(text)}
                  value={this.staticParam.search.value}
                />
              </View>
            ) : null}

            <View style={{ flex: 10 }}>
              <FlatList
                style={{ marginBottom: 20, height: '100%' }}
                refreshing={this.state.isFetching}
                ListEmptyComponent={this.ListEmpty}
                keyExtractor={(item, index) => index.toString()}
                data={this.state.list}
                renderItem={({ item, index }) => (
                  <ScrollView style={{}}>
                    <View>
                      <ListItem onPress={() => this.openView(item)}>
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{ flex: 3 }}>
                            <Text style={[AppStyles.Titledefault]}>
                              {item.AdvanceId}
                            </Text>
                            <Text style={[AppStyles.Textdefault]}>
                              Lý do: {item.AdvanceReason}
                            </Text>
                          </View>
                          <View style={{ flex: 2, alignItems: 'flex-end' }}>
                            {item.Status == 'Y' ? (
                              <Text
                                style={[
                                  {
                                    justifyContent: 'flex-end',
                                    alignItems: 'flex-end',
                                    color: AppColors.AcceptColor,
                                  },
                                  AppStyles.Textdefault,
                                ]}>
                                {item.StatusWF}
                              </Text>
                            ) : (
                              <Text
                                style={[
                                  {
                                    justifyContent: 'flex-end',
                                    alignItems: 'flex-end',
                                    color: AppColors.PendingColor,
                                  },
                                  AppStyles.Textdefault,
                                ]}>
                                {item.StatusWF}
                              </Text>
                            )}
                            <Text style={[AppStyles.Textdefault]}>
                              {this.customDate(item.ActualRefundDate)}
                            </Text>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {
                                  fontWeight: 'bold',
                                  textAlign: 'right',
                                },
                              ]}>
                              {this.addPeriod(item.TotalAdvance)}
                            </Text>
                          </View>
                        </View>
                      </ListItem>
                    </View>
                  </ScrollView>
                )}
                onEndReached={() => this.loadMoreData()}
                onEndReachedThreshold={0.5}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    tintColor="#f5821f"
                    titleColor="#fff"
                    colors={['red', 'green', 'blue']}
                  />
                }
              />
              {/* hiển thị nút tuỳ chọn */}
              <View style={AppStyles.StyleTabvarBottom}>
                <TabBarBottomCustom
                  ListData={this.listtabbarBotom}
                  onCallbackValueBottom={callback =>
                    this.onCallbackValueBottom(callback)
                  }
                />
              </View>
            </View>

            {this.state.setEventStartTime === true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 1000,
                  width: SCREEN_WIDTH,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.GetAll(true),
                      this.setState({ setEventStartTime: false });
                  }}>
                  <Text style={{ textAlign: 'right' }}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={this.staticParam.StartTime}
                  mode="date"
                  style={{ width: SCREEN_WIDTH }}
                  onDateChange={setDate => this.setStartTime(setDate)}
                />
              </View>
            ) : null}
            {this.state.setEventEndTime === true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'column',
                  position: 'absolute',
                  bottom: 0,
                  zIndex: 1000,
                  width: SCREEN_WIDTH,
                }}>
                <TouchableOpacity
                  style={{
                    borderWidth: 0.5,
                    borderColor: AppColors.gray,
                    padding: 10,
                  }}
                  onPress={() => {
                    this.GetAll(true),
                      this.setState({ setEventEndTime: false });
                  }}>
                  <Text style={{ textAlign: 'right' }}>Xong</Text>
                </TouchableOpacity>
                <DatePicker
                  locale="vie"
                  date={this.staticParam.EndTime}
                  mode="date"
                  style={{ width: SCREEN_WIDTH }}
                  onDateChange={setDate => this.setEndTime(setDate)}
                />
              </View>
            ) : null}
            <MenuSearchDate
              value={this.state.ValueSearchDate}
              callback={this.CallbackSearchDate}
              eOpen={this.openMenuSearchDate}
            />
            {dropDownType.length > 0 ? (
              <Combobox
                TypeSelect={'single'} // single or multiple
                callback={this.ChangeType}
                data={dropDownType}
                nameMenu={'Chọn loại'}
                eOpen={this.openComboboxType}
                position={'bottom'}
                value={this.staticParam.TypeOfTicket}
              />
            ) : null}
          </View>
        </View>
      </Container>
    );
  }
  onCallbackValueBottom(value) {
    switch (value) {
      case 'profile':
        this.onClickPending();
        break;
      case 'checkcircleo':
        this.onClickAprover();
        break;
      case 'export2':
        this.onClickNotApprove();
        break;
      default:
        break;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.GetAll(true);
    }
  }
  // sự kiện chờ duyệt
  onClickPending = () => {
    this.staticParam.Status = 'W';
    this.state.selectedTab = 'profile';
    this.setState({
      stylex: 1,
    });
    this.setState({
      list: [],
      selectedTab: this.state.selectedTab,
    });
    this.GetAll(true);
  };
  //Load lại
  _onRefresh = () => {
    this.GetAll(true);
    this.setState({ refreshing: true });
  };
  onClickAprover = () => {
    this.staticParam.Status = 'Y';
    this.state.selectedTab = 'checkcircleo';
    this.setState({
      stylex: 2,
    });
    this.setState({
      list: [],
      selectedTab: this.state.selectedTab,
    });
    this.GetAll(true);
  };
  onClickNotApprove = () => {
    this.staticParam.Status = 'R';
    this.state.selectedTab = 'export2';
    this.setState({
      stylex: 3,
    });
    this.setState({
      list: [],
      selectedTab: this.state.selectedTab,
    });
    this.GetAll(true);
  };
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({ EvenFromSearch: true });
    } else {
      this.setState({ EvenFromSearch: false });
    }
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  updateSearch = text => {
    this.staticParam.search.value = text;
    this.setState(
      {
        list: [],
      },
      () => {
        this.GetAll(true);
      },
    );
  };
  GetAll = s => {
    if (s === true) {
      this.staticParam.CurrentPage = 0;
    }
    this.staticParam.CurrentPage++;
    API_Advances.Advances_ListAll(this.staticParam)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        if (s) {
          this.state.list = _data.data;
        } else {
          this.state.list = this.state.list.concat(_data.data);
        }
        this.Total = _data.recordsTotal;
        if (this.staticParam.Status === 'W') {
          this.listtabbarBotom[0].Badge = _data.recordsFiltered;
        }
        this.setState({
          loading: false,
          list: this.state.list,
          refreshing: false,
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log('===========> error');
        console.log(error);
      });
  };
  openView(item) {
    Actions.openAdvancesAM({
      RecordGuid: item.AdvanceGuid,
    });
  }
  componentDidMount() {
    this.GetAll(true);
  }
  clickItem(item) {
    console.log('================> ' + JSON.stringify(item));
    switch (item) {
      case 'Back':
        Actions.app();
        break;
      case 'Open':
        Actions.openAdvances();
        break;
      default:
        break;
    }
  }
}
const styles = StyleSheet.create({
  statusNew: {
    marginTop: 5,
    width: 8,
    height: 8,
    borderRadius: 4,
    borderWidth: 0.1,
    borderColor: 'white',
    backgroundColor: '#4baf57',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 23,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 23,
    color: '#f6b801',
  },
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(indexAdvances);
