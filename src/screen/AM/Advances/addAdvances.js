import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  FlatList,
  Keyboard,
  StyleSheet,
  Alert,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {Header, Button, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_RegisterCars} from '@network';
import {Container, Content, Item, Label, Picker} from 'native-base';
import {FuncCommon} from '../../../utils';
import DatePicker from 'react-native-datepicker';
import API from '../../../network/API';
import _ from 'lodash';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RNPickerSelect from 'react-native-picker-select';
import Combobox from '../../component/Combobox';
import RequiredText from '../../component/RequiredText';
import controller from './controller';
import listCombobox from './listCombobox';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full heigth
const {headerTable} = listCombobox;
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
});
class AddAdvances extends Component {
  constructor(props) {
    super(props);
    this.DataProcess = FuncCommon.DataProcess();
    this.state = {
      rows: [{}],
      refreshing: false,
      bankData: [],
      bankDataItem: null,
      search: '',
      LiEmp: [],
      selected: undefined,
      Dropdown: [
        {
          value: null,
          label: 'Chọn trạng thái',
        },
        {
          value: 'A',
          label: 'Tạm ứng công trình',
        },
        {
          value: 'B',
          label: 'Tạm ứng công tác',
        },
        {
          value: 'C',
          label: 'Tạm ứng lương',
        },
        {
          value: 'D',
          label: 'Tạm ứng khác',
        },
      ],
      Type: 'A',
      TypeName: '',
      RefundDate: FuncCommon.ConDate(new Date(), 0),
      ActualRefundDate: FuncCommon.ConDate(new Date(), 0),
    };
  }
  Submit() {
    var data1 = this.ConDate(
      FuncCommon.ConDate(this.state.ActualRefundDate, 99),
      4,
    );
    var data2 = this.ConDate(FuncCommon.ConDate(this.state.RefundDate, 99), 4);
    if (data1 > data2) {
      Alert.alert(
        'Thông báo',
        'Ngày hoàn ứng phải lớn hơn hoặc bằng ngày tạm ứng',
        [{text: 'OK', onPress: () => Keyboard.dismiss()}],
        {cancelable: false},
      );
      return;
    }
    if (!this.state.AdvanceReason) {
      Alert.alert(
        'Thông báo',
        'Bạn cần nhập lý do tạm ứng',
        [{text: 'OK', onPress: () => Keyboard.dismiss()}],
        {cancelable: false},
      );
      return;
    }
    if (!this.state.TotalAdvance) {
      Alert.alert(
        'Thông báo',
        'Bạn cần nhập số tiền tạm ứng',
        [{text: 'OK', onPress: () => Keyboard.dismiss()}],
        {cancelable: false},
      );
      return;
    }
    if (this.props.itemData) {
      this.Update();
      return;
    }
    let _obj = {
      EmpAdvanceGuid: this.state.selected2,
      EmpAdvanceName: this.state.FullName,
      Type: this.state.Type,
      AdvanceReason: this.state.AdvanceReason,
      RefundDate:
        this.state.RefundDate === '' || this.state.RefundDate === null
          ? null
          : FuncCommon.ConDate(
              FuncCommon.ConDate(this.state.RefundDate, 99),
              2,
            ),
      ActualRefundDate:
        this.state.ActualRefundDate === '' ||
        this.state.ActualRefundDate === null
          ? null
          : FuncCommon.ConDate(
              FuncCommon.ConDate(this.state.ActualRefundDate, 99),
              2,
            ),
      TotalAdvance: this.state.TotalAdvance,
      Description: this.state.Description,
      AdvanceId: this.state.AdvanceId,
    };
    let lstDetails = [...this.state.rows];
    let auto = {Value: this.state.AdvanceId, IsEdit: this.state.isEdit};
    let _data = new FormData();
    _data.append('submit', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteAttach', JSON.stringify([]));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    API_RegisterCars.Advances_InsertAPI(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.Error) {
          console.log(_rs);
        } else {
          Alert.alert(
            'Thông báo',
            'Thêm mới thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.callBackList(JSON.parse(rs.data.data));
        }
      })
      .catch(error => {
        console.log('Error when call API insert Mobile.');
        console.log(error);
      });
  }
  Update = () => {
    let edit = {
      ...this.props.itemData,
      EmpAdvanceGuid: this.state.selected2,
      Type: this.state.Type,
      AdvanceReason: this.state.AdvanceReason,
      RefundDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.RefundDate, 99),
        2,
      ),
      ActualRefundDate: FuncCommon.ConDate(
        FuncCommon.ConDate(this.state.ActualRefundDate, 99),
        2,
      ),
      TotalAdvance: this.state.TotalAdvance,
      Description: this.state.Description,
    };
    let lstDetails = [...this.state.rows];
    let auto = {IsEdit: true};
    let _data = new FormData();
    _data.append('edit', JSON.stringify(edit));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteDetail', JSON.stringify([]));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    API_RegisterCars.Advances_Update(_data)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode === 200) {
          Alert.alert(
            'Thông báo',
            'Cập nhật thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.onPressBack();
        } else {
          console.log(_rs.message);
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
        console.log(error);
      });
  };
  //#region hàm Quay lại
  callBackList(rs) {
    this.setState({loading: false});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back_Advances',
      Data: !this.state.itemData
        ? {
            Module: 'HR_Advances',
            Subject: this.state.AdvanceReason,
            StartDate:
              this.state.ActualRefundDate === '' ||
              this.state.ActualRefundDate === null
                ? null
                : FuncCommon.ConDate(this.state.ActualRefundDate, 0),
            EndDate:
              this.state.RefundDate === '' || this.state.RefundDate === null
                ? null
                : FuncCommon.ConDate(this.state.RefundDate, 0),
            RowGuid: rs,
            AssignTo: [],
          }
        : {},
      ActionTime: new Date().getTime(),
    });
  }
  //#endregion
  componentDidMount() {
    const {itemData, rows} = this.props;
    if (itemData) {
      this.setState({
        rows: rows.map(row => ({
          ...row,
          RefundAmount: row.RefundAmount ? row.RefundAmount + '' : '',
        })),
        isEdit: false,
        AdvanceId: itemData.AdvanceId,
        FullName: itemData.FullName,
        DepartmentName: itemData.DepartmentName,
        selected2: itemData.EmpAdvanceGuid,
        Type: itemData.Type,
        AdvanceReason: itemData.AdvanceReason,
        ActualRefundDate: FuncCommon.ConDate(
          new Date(itemData.ActualRefundDate),
          0,
        ),
        RefundDate: itemData.RefundDate,
        TotalAdvance: itemData.TotalAdvance + '',
        Description: itemData.Description,
      });
    } else {
      this.setState({
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
      });
      this.getNumberAuto();
    }
    this.getEmp();
  }
  getEmp = () => {
    API_RegisterCars.GetEmpAdvances({})
      .then(rs => {
        var _listEmp = rs.data.map(data => ({
          label: data.FullName,
          value: data.EmployeeGuid,
        }));
        _listEmp.unshift({
          label: 'Chọn',
          value: null,
        });
        this.setState({
          LiEmp: _listEmp,
        });
      })
      .catch(error => {
        console.log('Error when call API GetEmployee Mobile.');
        console.log(error);
      });
  };
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_TU',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({isEdit: data.IsEdit, AdvanceId: data.Value});
    });
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td, {width: 200}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Title}
            autoCapitalize="none"
            onChangeText={Result => {
              this.handleChangeRows(Result, index, 'Title');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td, {width: 200}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.RefundAmount}
            keyboardType="numeric"
            autoCapitalize="none"
            onChangeText={Result => {
              this.handleChangeRows(Result, index, 'RefundAmount');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td, {width: 200}]}>
          <TextInput
            style={AppStyles.FormInput_custom}
            underlineColorAndroid="transparent"
            value={row.Description}
            autoCapitalize="none"
            onChangeText={Result => {
              this.handleChangeRows(Result, index, 'Description');
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td, {width: 80}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    const {isEdit, AdvanceId} = this.state;
    return (
      <Container>
        <View style={{flex: 1}}>
          <View style={{flex: 1}}>
            {!this.props.itemData ? (
              <TabBar_Title
                title={'Đăng ký phiếu tạm ứng'}
                callBack={() => Actions.pop()}
              />
            ) : (
              <TabBar_Title
                title={'Chỉnh sửa phiếu tạm ứng'}
                callBack={() => Actions.pop()}
              />
            )}
            <ScrollView ref={scrollView => (this.scrollView = scrollView)}>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Mã phiếu</Text>
                  </View>
                  {isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={AdvanceId}
                      autoCapitalize="none"
                      onChangeText={Title => this.setState({AdvanceId: Title})}
                    />
                  )}
                  {!isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                        {AdvanceId}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người tạo</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Phòng ban</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <Item stackedLabel>
                    <Label style={AppStyles.Labeldefault}>Người tạm ứng</Label>
                    <RNPickerSelect
                      doneText="Xong"
                      onValueChange={value => this.onValueChange2(value)}
                      items={this.state.LiEmp}
                      placeholder={{}}
                      value={this.state.selected2}
                    />
                  </Item>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>Loại tạm ứng</Text>
                  <RNPickerSelect
                    doneText="Xong"
                    onValueChange={value => this.onValueChangeType(value)}
                    items={this.state.Dropdown}
                    placeholder={{}}
                    value={this.state.Type}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Lý do tạm ứng <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.AdvanceReason}
                    onChangeText={AdvanceReason =>
                      this.setAdvanceReason(AdvanceReason)
                    }
                  />
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>Ngày tạm ứng</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.ActualRefundDate}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        disabled={true}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setActualRefundDate(date)}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày hoàn ứng dự kiến
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.RefundDate}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setRefundDate(date)}
                      />
                    </View>
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Số tiền tạm ứng <RequiredText />
                    </Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    keyboardType="numeric"
                    autoCapitalize="none"
                    value={this.state.TotalAdvance}
                    onChangeText={TotalAdvance =>
                      this.setTotalAdvance(TotalAdvance)
                    }
                  />
                </View>

                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Mô tả</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={4}
                    value={this.state.Description}
                    onChangeText={Description =>
                      this.setDescription(Description)
                    }
                  />
                </View>
                <View
                  style={{marginTop: 5, marginBottom: 10, paddingBottom: 10}}>
                  <View style={{width: 150, marginLeft: 10}}>
                    <Button
                      title="Thêm dòng"
                      type="outline"
                      size={15}
                      onPress={() => {
                        this.addRows();
                        this.scrollView.scrollToEnd();
                      }}
                      buttonStyle={{padding: 5, marginBottom: 5}}
                      titleStyle={{marginLeft: 5}}
                      icon={
                        <Icon
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>

                  {/* Table */}
                  <ScrollView horizontal={true}>
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        {headerTable.map(item => (
                          <TouchableOpacity
                            key={item.title}
                            style={[AppStyles.table_th, {width: item.width}]}>
                            <Text style={AppStyles.Labeldefault}>
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        ))}
                      </View>
                      <View>
                        {this.state.rows.length > 0 ? (
                          <FlatList
                            data={this.state.rows}
                            renderItem={({item, index}) => {
                              return this.itemFlatList(item, index);
                            }}
                            keyExtractor={(rs, index) => index.toString()}
                          />
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'left'},
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>

        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
      </Container>
    );
  }
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'arrowleft'}
          type="antdesign"
          size={30}
          onPress={() => this.indexRegistercar()}
        />
      </View>
    );
  }
  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCombobox() {
    this._openCombobox();
  }
  setAdvanceReason(AdvanceReason) {
    this.setState({AdvanceReason});
  }
  setTotalAdvance(TotalAdvance) {
    this.setState({TotalAdvance});
  }
  setDescription(Description) {
    this.setState({Description});
  }
  setActualRefundDate(ActualRefundDate) {
    this.setState({ActualRefundDate: ActualRefundDate});
  }
  setRefundDate(RefundDate) {
    this.setState({RefundDate: RefundDate});
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      ActionTime: new Date().getTime(),
    });
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
    let rs = this.state.LiEmp.find(x => x.EmployeeGuid === value);
    if (rs != null) {
      this.setState({FullName: rs.FullName});
    }
  }
  onValueChangeType(value) {
    this.setState({
      Type: value,
    });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  ConDate = function(data, number) {
    try {
      if (data == null || data == '') {
        return '';
      }
      if (data !== null && data != '' && data != undefined) {
        try {
          if (data.indexOf('SA') != -1 || data.indexOf('CH') != -1) {
            if (data.indexOf('SA') != -1) {
            }
            if (data.indexOf('CH') != -1) {
            }
          }

          if (data.indexOf('Date') != -1) {
            data = data.substring(data.indexOf('Date') + 5);
            data = parseInt(data);
          }
        } catch (ex) {}
        var newdate = new Date(data);
        if (number == 3) {
          return newdate;
        }
        if (number == 2) {
          return '/Date(' + newdate.getTime() + ')/';
        }
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();
        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;
        if (mm < 10) mm = '0' + mm;
        if (number == 0) {
          return (todayDate = day + '/' + month + '/' + year);
        }
        if (number == 1) {
          return (todayDate =
            day + '/' + month + '/' + year + ' ' + hh + ':' + mm);
        }
        if (number == 4) {
          return new Date(year, month - 1, day);
        }
      } else {
        return '';
      }
    } catch (ex) {
      return '';
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddAdvances);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 10,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
});
