import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  dropDownType: [
    {
      value: 'A',
      text: 'Tạm ứng',
    },
    {
      value: 'B',
      text: 'Thanh toán',
    },
  ],
  //#endregion
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Nội dung TT', width: 200},
    // {title: 'Hình thức TT', width: 200},
    {title: 'Đối tượng nhận', width: 200},
    {title: 'Số tài khoản', width: 200, showInTypeCK: true},
    {title: 'Ngân hàng', width: 200, showInTypeCK: true},
    {title: 'Số tiền', width: 200},
    {title: 'Ghi chú', width: 200},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
};
