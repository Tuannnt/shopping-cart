import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import {
    ActivityIndicator,
    FlatList,
    RefreshControl,
    TouchableOpacity,
    Text, View,
    TextInput
} from "react-native";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import API_Items from "../../../network/AM/API_Items";
import { DatePicker, Container, Content, Item, Label, Picker } from 'native-base';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import Searchable_Dropdown from '../../component/Searchable_Dropdown';
import Combobox from '../../component/Combobox';

class ListItemsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 25,
                Search: "",
                QueryOrderBy: "ItemId",
                ItemId: null,
                WarehouseId: null,
            },
            ListWarehouses: [],
            openSearch: false,
            selectedItems: [],
            dropdownName: ''
        };
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_Items.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        //this.GetAll();
        API_Items.GetWarehouse().then(rs => {
            var _listWarehouses = rs.data;
            let _reListWarehouses = [];
            for (var i = 0; i < _listWarehouses.length; i++) {
                _reListWarehouses.push({
                    text: _listWarehouses[i].WarehouseId + ' - ' + _listWarehouses[i].WarehouseName,
                    value: _listWarehouses[i].WarehouseId,
                });
            }
            this.setState(
                {
                    ListWarehouses: _reListWarehouses,
                });
        }).catch(error => {
            console.log(error)
        });
    }

    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                ItemId: this.state.staticParam.ItemId,
                WarehouseId: this.state.staticParam.WarehouseId
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Hàng hóa, vật tư'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'AM'}
                />
                <TouchableOpacity style={[AppStyles.FormInput, { marginLeft: 10, marginRight: 10, flexDirection: 'row' }]} onPress={() => this.onActionCombobox()}>
                    <Text style={[AppStyles.TextInput, this.state.dropdownName == "" ? { color: AppColors.gray, flex: 3 } : { flex: 3 }]}>{this.state.dropdownName !== "" ? this.state.dropdownName : "Chọn kho..."}</Text>
                    <View style={{ flex: 1, alignItems:'flex-end' }}>
                        <Icon color='#888888'
                            name={'chevron-thin-down'} type='entypo'
                            size={20} />
                    </View>
                </TouchableOpacity>
                {this.state.openSearch == true ?
                    <FormSearch
                        CallbackSearch={(callback) => this.updateSearch(callback)}
                    />
                    : null}
                <View style={{ flex: 9 }}>
                    <FlatList
                        style={{ height: '100%' }}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.RowGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family }}>
                                <ListItem
                                    title={item.ItemName}
                                    titleStyle={AppStyles.Titledefault}
                                    subtitle={() => {
                                        return (
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ width: '60%' }}>
                                                    <Text style={AppStyles.Textdefault}>Mã sản phẩm: {item.ItemId}</Text>
                                                    <Text
                                                        style={AppStyles.Textdefault}>Mã tham chiếu:{item.VenderCode}</Text>
                                                </View>
                                                <View style={{ width: '40%' }}>
                                                    <Text style={AppStyles.Textdefault}>SL tồn cuối: {this.addPeriod(item.InventoryQuantity)}</Text>
                                                    <Text style={AppStyles.Textdefault}>Kho: {item.WarehouseId}</Text>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    onPress={() => this.openView(item.ItemGuid)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                </View>
                {this.state.ListWarehouses.length > 0 ?
                    <Combobox
                        TypeSelect={"single"}// single or multiple
                        callback={this.ChangWarehouses}
                        data={this.state.ListWarehouses}
                        nameMenu={'Chọn kho'}
                        eOpen={this.openCombobox}
                        position={'top'}
                    />
                    : null}
            </View>
        );
    }
    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                ItemId: this.state.staticParam.ItemId,
                WarehouseId: this.state.staticParam.WarehouseId
            }
        });
        this.GetAll();
    }
    //Load lại
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.GetAll();
    };
    onPressHome() {
        Actions.app();
    }
    ChangWarehouses = (value) => {
        this.state.list = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        this.state.staticParam.WarehouseId = value.value;
        this.setState({
            list: [],
            dropdownName: value.text,
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                ItemId: this.state.staticParam.ItemId,
                WarehouseId: value.value
            }
        });
        this.GetAll();
    }
    _openCombobox() { }
    openCombobox = (d) => {
        this._openCombobox = d;
    }
    onActionCombobox() {
        this._openCombobox();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
    openView(id) {
        Actions.amViewItems({ RecordGuid: id });
    }
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListItemsComponent);
