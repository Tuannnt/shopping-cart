import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_Items from '../../../network/AM/API_Items';
import configApp from '../../../configApp';
import ErrorHandler from '../../../error/handler';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';

const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});

class ViewItemsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
      listDetail: [],
      staticParam: {
        QueryOrderBy: 'ItemName desc',
      },
      isHidden: false,
      NoImages: false,
      loading: true,
    };
    this.state.FileAttackments = {
      renderViewImage(guid) {
        return configApp.url_View_Thumb_Photo + guid;
      },
    };
  }

  componentDidMount(): void {
    this.getItem();
  }

  getItem() {
    let id = this.props.RecordGuid;
    API_Items.GetItem(id)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log(error.data.data);
      });
    //this.GetDetail();
  }
  CustomViewImage = item => (
    <View style={{alignItems: 'center', height: '100%'}}>
      <Image
        style={{width: '70%', height: '100%', borderRadius: 5}}
        source={{
          uri: this.state.FileAttackments.renderViewImage(
            this.props.RecordGuid,
          ),
        }}
      />
    </View>
  );
  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };

  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết vật tư'}
          callBack={() => this.onPressBack()}
        />
        <View style={{marginTop: 5, flex: 1}}>
          {this.state.Data != null
            ? this.CustomViewImage(this.state.Data)
            : null}
          <View
            style={{
              flex: 1,
              bottom: 0,
              borderColor: '#fff',
              borderTopWidth: 0.5,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {this.state.ListdataView && this.state.ListdataView.length > 0
              ? this.CustomTabbarImage(this.state.ListdataView)
              : null}
          </View>
        </View>
        {this.state.Data !== [] ? (
          <View style={{flex: 4}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1, paddingLeft: 10, paddingTop: 10}}>
                <Text style={AppStyles.Titledefault}>
                  {this.state.Data.ItemName}
                </Text>
              </View>
            </View>
            <View
              style={[
                this.state.isHidden == true ? styles.hidden : '',
                {
                  paddingLeft: 20,
                  paddingRight: 20,
                  paddingTop: 20,
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                },
              ]}>
              <View style={{flex: 1}}>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Mã vật tư, hàng hóa :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Tính chất :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Đơn vị tính :
                </Text>
              </View>
              <View style={{flex: 2, alignItems: 'flex-end'}}>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.state.Data.ItemId}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.state.Data.Type == 'V'
                    ? 'Vật tư hàng hóa'
                    : this.state.Data.Type == 'P'
                    ? 'Thành phẩm'
                    : this.state.Data.Type == 'D'
                    ? 'Dịch vụ'
                    : this.state.Data.Type == 'G'
                    ? 'Chỉ là diễn giải'
                    : this.state.Data.Type == 'H'
                    ? 'Đại lý, gia công, ủy thác'
                    : ''}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.state.Data.UnitName}
                </Text>
              </View>
            </View>
            <View style={{paddingLeft: 20, paddingRight: 20}}>
              <Text style={AppStyles.Labeldefault}>Mô tả :</Text>
              <Text style={AppStyles.Textdefault}>
                {this.state.Data.Description}
              </Text>
            </View>
          </View>
        ) : null}
      </View>
    );
  }

  onPressBack() {
    Actions.pop();
  }

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ViewItemsComponent);
