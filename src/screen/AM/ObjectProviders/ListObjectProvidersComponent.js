import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, TouchableOpacity, Text, View, TextInput } from "react-native";
import { Actions } from "react-native-router-flux";
import FormSearch from '../../component/FormSearch';
import API_ObjectProviders from "../../../network/AM/API_ObjectProviders";
import { DatePicker, Container, Content, Item, Label, Picker } from 'native-base';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import MultiSelect_Component from '../../component/MultiSelect_Component';
import Combobox from '../../component/Combobox';
class ListObjectProvidersComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 25,
                Search: "",
                QueryOrderBy: "ObjectId DESC",
                IsBusiness: null,
                lstObjectGroups: [],
                Status: null
            },
            ListObjectGroups: [],
            openSearch: false,
            selectedItems: [],
            dropdownName: ''
        };
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_ObjectProviders.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        //this.GetAll();
        API_ObjectProviders.GetObjectGroups().then(rs => {
            var _lstObjectGroups = rs.data;
            let _relstObjectGroups = [];
            for (var i = 0; i < _lstObjectGroups.length; i++) {
                _relstObjectGroups.push({
                    text: _lstObjectGroups[i].Title,
                    value: _lstObjectGroups[i].ObjectGroupGuid,
                });
            }
            this.setState(
                {
                    ListObjectGroups: _relstObjectGroups,
                });
        }).catch(error => {
            console.log(error)
        });
    }

    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                IsBusiness: this.state.staticParam.IsBusiness,
                lstObjectGroups: this.state.staticParam.lstObjectGroups,
                Status: this.state.staticParam.Status
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Nhà cung cấp'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'AM'}
                />
                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    {this.state.ListObjectGroups.length > 0 ?
                        <TouchableOpacity style={[AppStyles.FormInput, { marginLeft: 10, marginRight: 10, flexDirection: 'row' }]} onPress={() => this.onActionCombobox()}>
                            <Text style={[AppStyles.TextInput, this.state.dropdownName == "" ? { color: AppColors.gray, flex: 3 } : { flex: 3 }]}>{this.state.dropdownName !== "" ? this.state.dropdownName : "Chọn nhóm nhà cung cấp..."}</Text>
                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <Icon color='#888888'
                                    name={'chevron-thin-down'} type='entypo'
                                    size={20} />
                            </View>
                        </TouchableOpacity>
                        : null
                    }
                </View>
                {this.state.openSearch == true ?
                    <FormSearch
                        CallbackSearch={(callback) => this.updateSearch(callback)}
                    />
                    : null}
                <View style={{ flex: 9 }}>
                    <FlatList
                        style={{ height: '100%' }}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        //ListHeaderComponent={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.RowGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{}}>
                                <ListItem
                                    title={item.ObjectName}
                                    titleStyle={AppStyles.Titledefault}
                                    subtitle={() => {
                                        return (
                                            <View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={AppStyles.Textdefault}>Mã: {item.ObjectId}</Text>
                                                        <Text
                                                            style={AppStyles.Textdefault}>Trạng thái: {item.Status == true ? 'Theo dõi' : item.Status == false ? 'Ngừng theo dõi' : ''}</Text>
                                                    </View>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={AppStyles.Textdefault}>Mã số thuế: {item.TaxCode}</Text>
                                                        <Text style={AppStyles.Textdefault}>Nhóm NCC: {item.ObjectInGroupCount} nhóm</Text>
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ width: '100%' }}>
                                                        <Text style={AppStyles.Textdefault}>Địa chỉ: {item.Address}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    onPress={() => this.openView(item.ObjectGuid)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                </View>
                {this.state.ListObjectGroups.length > 0 ?
                    <Combobox
                        TypeSelect={"multiple"}// single or multiple
                        callback={this.ChangeObjectGroups}
                        data={this.state.ListObjectGroups}
                        nameMenu={'Chọn nhóm nhà cung cấp'}
                        eOpen={this.openCombobox}
                        position={'top'}
                    />
                    : null}
            </View>
        );
    }

    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                IsBusiness: this.state.staticParam.IsBusiness,
                lstObjectGroups: this.state.staticParam.lstObjectGroups,
                Status: this.state.staticParam.Status
            }
        });
        this.GetAll();
    }
    //Load lại
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.GetAll();
    };
    onPressHome() {
        Actions.app();
    }
    ChangeObjectGroups = (value) => {
        var text = '';
        this.state.list = [];
        this.state.staticParam.lstObjectGroups = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        if (value[0].value != null) {
            for (let i = 0; i < value.length; i++) {
                this.state.staticParam.lstObjectGroups.push(value[i].value);
                if (text === '') {
                    text = value[i].text;
                }
                else {
                    text = text + ', ' + value[i].text
                }
            }
        }
        this.setState({
            dropdownName: text,
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                IsBusiness: this.state.staticParam.IsBusiness,
                lstObjectGroups: this.state.staticParam.lstObjectGroups,
                Status: this.state.staticParam.Status
            }
        });
        this.GetAll();
    }
    _openCombobox() { }
    openCombobox = (d) => {
        this._openCombobox = d;
    }
    onActionCombobox() {
        this._openCombobox();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
    openView(id) {
        Actions.amitemObjectProviders({ RecordGuid: id });
    }

}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListObjectProvidersComponent);
