import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_ObjectProviders from '../../../network/AM/API_ObjectProviders';
import ErrorHandler from '../../../error/handler';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';

class ItemObjectProvidersComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
      listBankAccounts: [],
      listContracts: [],
      staticParam: {
        QueryOrderBy: 'ItemName desc',
      },
      isHiddenObjectProvider: false,
      isHiddenContract: true,
      isHiddenBankAccount: true,
      loading: true,
    };
  }

  componentDidMount(): void {
    this.getItem();
  }

  getItem() {
    let id = this.props.RecordGuid;
    API_ObjectProviders.GetItem(id)
      .then(res => {
        this.setState({Data: JSON.parse(res.data.data)});
        if (this.state.Data.ObjectInGroups.length > 0) {
          this.state.Data.ObjectInGroups = this.state.Data.ObjectInGroups.toString();
        }
      })
      .catch(error => {
        console.log('Error');
        console.log(error.data.data);
      });
    API_ObjectProviders.GetBankAccounts(id)
      .then(res => {
        this.state.listBankAccounts = JSON.parse(res.data.data);
        this.setState({list: this.state.listBankAccounts});
        this.setState({refreshing: false});
      })
      .catch(error => {
        this.setState({refreshing: false});
      });
    API_ObjectProviders.GetItemContracts(id)
      .then(res => {
        this.state.listContracts = JSON.parse(res.data.data);
        this.setState({
          list: this.state.listContracts,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  }

  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };

  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết nhà cung cấp'}
          callBack={() => this.onPressBack()}
        />
        <View style={{flexDirection: 'row', padding: 10}}>
          <View style={{flex: 10}}>
            <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
          </View>
          <View style={{flex: 1}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHiddenObjectProvider == false
                  ? this.setState({isHiddenObjectProvider: true})
                  : this.setState({isHiddenObjectProvider: false})
              }
            />
          </View>
        </View>
        <View>
          <View
            style={[
              this.state.isHiddenObjectProvider == true ? styles.hidden : '',
              {padding: 20, backgroundColor: '#fff'},
            ]}>
            <View style={{flexDirection: 'row'}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                Mã nhà cung cấp :
              </Text>
              <Text
                style={
                  ([AppStyles.Textdefault],
                  {flex: 2, textAlign: 'right', textAlign: 'right'})
                }>
                {this.state.Data.ObjectId}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                Tình trạng :
              </Text>
              <Text
                style={
                  ([AppStyles.Textdefault], {flex: 2, textAlign: 'right'})
                }>
                {this.state.Data.Status == false
                  ? 'Ngừng theo dõi'
                  : this.state.Data.Status == true
                  ? 'Theo dõi'
                  : ''}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                Mã số thuế :
              </Text>
              <Text
                style={
                  ([AppStyles.Textdefault], {flex: 2, textAlign: 'right'})
                }>
                {this.state.Data.TaxCode}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={AppStyles.Labeldefault}>Tên nhà cung cấp :</Text>
              <Text style={AppStyles.Textdefault}>
                {this.state.Data.ObjectName}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={AppStyles.Labeldefault}>Nhóm nhà cung cấp :</Text>
              <Text style={AppStyles.Textdefault}>
                {this.state.Data.ObjectInGroups}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <Text style={[AppStyles.Titledefault, {flex: 10, paddingLeft: 10}]}>
              Tài khoản ngân hàng
            </Text>
            <View style={{flex: 1}}>
              <Icon
                color="#888888"
                name={'chevron-thin-down'}
                type="entypo"
                size={20}
                onPress={() =>
                  this.state.isHiddenBankAccount == false
                    ? this.setState({isHiddenBankAccount: true})
                    : this.setState({isHiddenBankAccount: false})
                }
              />
            </View>
          </View>
          <Divider style={{marginTop: 10}} />
        </View>
        <ScrollView
          style={[this.state.isHiddenBankAccount == true ? styles.hidden : '']}>
          {this.state.listBankAccounts.length > 0 ? (
            this.state.listBankAccounts.map((item, index) => (
              <View style={{}}>
                <ListItem
                  title={item.BankAccountNo}
                  titleStyle={AppStyles.Titledefault}
                  subtitle={() => {
                    return (
                      <View>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{width: '50%'}}>
                            <Text style={AppStyles.Textdefault}>
                              Ngân hàng: {item.BankName}
                            </Text>
                          </View>
                          <View style={{width: '50%'}}>
                            <Text style={AppStyles.Textdefault}>
                              Thành phố: {item.ProvinceId}
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  }}
                />
              </View>
            ))
          ) : (
            <View style={AppStyles.centerAligned}>
              <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
            </View>
          )}
        </ScrollView>
        <View style={{flexDirection: 'row', paddingRight: 10}}>
          <Text style={[AppStyles.Titledefault, {flex: 10, paddingLeft: 10}]}>
            Thông tin liên hệ
          </Text>
          <View style={{flex: 1}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
              onPress={() =>
                this.state.isHiddenContract == false
                  ? this.setState({isHiddenContract: true})
                  : this.setState({isHiddenContract: false})
              }
            />
          </View>
        </View>
        <Divider style={{marginTop: 10}} />
        <ScrollView
          style={[this.state.isHiddenContract == true ? styles.hidden : '']}>
          {this.state.listContracts.length > 0 ? (
            this.state.listContracts.map((item, index) => (
              <View style={{}}>
                <ListItem
                  title={item.FullName}
                  titleStyle={AppStyles.Titledefault}
                  subtitle={() => {
                    return (
                      <View>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{width: '50%'}}>
                            <Text style={AppStyles.Textdefault}>
                              Chức vụ: {item.JobTitle}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              Địa chỉ: {item.Address}
                            </Text>
                          </View>
                          <View style={{width: '50%'}}>
                            <Text style={AppStyles.Textdefault}>
                              SĐT: {item.Phone}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              Email: {item.Email}
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  }}
                />
              </View>
            ))
          ) : (
            <View style={AppStyles.centerAligned}>
              <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }

  onPressBack() {
    Actions.pop();
  }
  openAttachments() {
    var obj = {
      ModuleId: '10',
      RecordGuid: this.state.Data.ContractGuid,
    };
    Actions.attachmentComponent(obj);
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}
const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    padding: 20,
    alignSelf: 'baseline',
    borderBottomWidth: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  avatarMargin: {
    marginLeft: 36,
  },
  Icon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    marginRight: 20,
  },
  ViewFlatList: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
  },
  hidden: {
    display: 'none',
  },
  StatusWFR: {
    color: 'red',
  },
  StatusWFG: {
    color: 'green',
  },
  Lable: {
    fontSize: 13,
    flex: 1,
    fontWeight: 'bold',
  },
  Content: {
    fontSize: 13,
    flex: 2,
    textAlign: 'right',
  },
  subtitleStyle: {
    fontSize: 11,
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 80,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    //borderTopLeftRadius: 10,
    //borderBottomLeftRadius: 10,
    width: 80,
  },
  ClickApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#F5DA81',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 80,
  },
  ClickSearch: {
    backgroundColor: '#F5DA81',
  },
});
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemObjectProvidersComponent);
