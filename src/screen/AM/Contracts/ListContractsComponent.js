import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, View, TextInput } from "react-native";
import { Actions } from "react-native-router-flux";
import Swipeout from "react-native-swipeout";
import Fonts from "../../../theme/fonts";
import TabNavigator from "react-native-tab-navigator";
import API_Contracts from "../../../network/AM/API_Contracts";
import Dialog from "react-native-dialog";
import { DatePicker, Container, Content, Item, Label, Picker } from 'native-base';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import Searchable_Dropdown from '../../component/Searchable_Dropdown';
class ListContractsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 25,
                Search: "",
                QueryOrderBy: "SignDate DESC",
                DepartmentId: null,
                Status: []
            },
            ListDepartments: [],
            selected: undefined,
            selectedTab: 'checkcircleo',
            openSearch: false
        };
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_Contracts.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        this.GetAll();
        API_Contracts.GetDepartments().then(rs => {
            var _lstDepartment = rs.data;
            let _relstDepartment = [{
                name: 'Tất cả',
                id: null,
            }];
            for (var i = 0; i < _lstDepartment.length; i++) {
                _relstDepartment.push({
                    name: _lstDepartment[i].DepartmentId + ' - ' + _lstDepartment[i].DepartmentName,
                    id: _lstDepartment[i].DepartmentId,
                });
            }
            this.setState(
                {
                    ListDepartments: _relstDepartment,
                });
        }).catch(error => {
            console.log(error)
        });
    }

    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                Status: this.state.staticParam.Status,
                DepartmentId: this.state.staticParam.DepartmentId
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Hợp đồng bán hàng'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'AM'}
                />
                {/* <Item
                    picker
                    style={{ paddingLeft: 10, paddingRight: 10 }}>
                    {this.renderDepartments()}
                </Item> */}
                {this.state.ListDepartments.length > 0 ?
                    <Searchable_Dropdown
                        ListCombobox={this.state.ListDepartments}
                        placeholder='Chọn bộ phận'
                        defaultItems={0}
                        CallBackListCombobox={(id) => this.ChangDepartments(id)}
                    />
                    : null
                }
                {this.state.openSearch == true ?
                    <FormSearch
                    CallbackSearch={(callback) => this.updateSearch(callback)}
                />
                    : null}
                <View style={{ flex: 9 }}>
                    <FlatList
                        style={{ height: '100%' }}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.RowGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{}}>
                                <ListItem
                                    title={item.ContractNo}
                                    titleStyle={AppStyles.Titledefault}
                                    subtitle={() => {
                                        return (
                                            <View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={AppStyles.Textdefault}>Số hợp đồng: {item.ContractNo}</Text>
                                                        <Text
                                                            style={AppStyles.Textdefault}>Bộ phận thực hiện:{item.DepartmentName}</Text>
                                                    </View>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={AppStyles.Textdefault}>Ngày ký: {this.customDate(item.SignDate)}</Text>
                                                        <Text style={AppStyles.Textdefault}>G.Trị hợp đồng:{this.addPeriod(item.AmountOC)}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    //chevron
                                    onPress={() => this.openView(item.ContractGuid, item.ContractNo)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                </View>
            </View>
        );
    }

    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                Status: this.state.staticParam.Status,
                DepartmentId: this.state.staticParam.DepartmentId
            }
        });
        this.GetAll();
    }
    //Load lại
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.GetAll();
    };
    onPressHome() {
        Actions.app();
    }
    renderDepartments = () => (
        <Picker
            mode="dropdown"
            iosIcon={<Icon name="arrow-down" />}
            style={{ width: undefined }}
            placeholder="Chọn bộ phận"
            placeholderStyle={{ color: "#bfc6ea" }}
            placeholderIconColor="#007aff"
            selectedValue={this.state.selected}
            onValueChange={this.ChangDepartments.bind(this)}
        >
            {
                (this.state.ListDepartments && this.state.ListDepartments.length > 0) ? this.state.ListDepartments.map((_item) =>
                    <Picker.Item label={_item.DepartmentName} value={_item.DepartmentId} />
                ) : <Picker.Item label="Chọn bộ phận" value="" />
            }
        </Picker>
    )
    ChangDepartments(value) {
        this.state.list = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        this.state.staticParam.DepartmentId = value;
        this.setState({
            selected: value,
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                QueryOrderBy: this.state.staticParam.QueryOrderBy,
                Status: this.state.staticParam.Status,
                DepartmentId: this.state.staticParam.DepartmentId
            }
        });
        this.GetAll();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
    openView(id, ContractNo) {
        Actions.amitemContracts({ RecordGuid: id, ContractNo: ContractNo });
    }
}

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListContractsComponent);
