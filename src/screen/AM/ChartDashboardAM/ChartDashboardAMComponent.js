// import {
//     LineChart,
//     BarChart,
//     PieChart,
//     ProgressChart,
//     ContributionGraph,
//     StackedBarChart
// } from 'react-native-chart-kit';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  FlatList,
} from 'react-native';
import {Dimensions} from 'react-native';
import {ListItem, Text, Body} from 'native-base';
import {Header, SearchBar, Icon, Badge} from 'react-native-elements';
import {FlatGrid} from 'react-native-super-grid';
import {Actions} from 'react-native-router-flux';
import API_Orders from '../../../network/SM/API_Orders';
import API_AM from '../../../network/AM/API_AM';
import {ECharts} from 'react-native-echarts-wrapper';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
var width = Dimensions.get('window').width; //full width
class ChartDashboardAMComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      staticParam: {
        dateNow: 20201305,
      },
      staticParamRequest: {
        EmployeeGuid: '',
        JobTitleId: '',
        OrganizationGuid: '',
      },
      isHidden: true,
      isHiddenRequest: true,
      listDepartment: [],
      listDataActualSalary: [],
      list: [],
      listRequest: [],
      ListCountNoti: [],
      listMenu: [
        {
          Title: 'YC nhập kho',
          Icon: 'export2',
          type: 'antdesign',
          count: 0,
          code: 'A1',
        },
        {
          Title: 'YC xuất kho ',
          Icon: 'export',
          type: 'antdesign',
          count: 0,
          code: 'A2',
        },
        {
          Title: 'Số đơn ĐH',
          Icon: 'v-card',
          type: 'entypo',
          count: 0,
          code: 'A3',
        },
        {
          Title: 'ĐN thanh toán',
          Icon: 'areachart',
          type: 'antdesign',
          count: 0,
          code: 'A4',
        },
        {
          Title: 'ĐN tạm ứng',
          Icon: 'add-user',
          type: 'entypo',
          count: 0,
          code: 'A5',
        },
        {
          Title: 'Nghỉ phép',
          Icon: 'account-alert',
          type: 'material-community',
          count: 0,
          code: 'A6',
        },
        {
          Title: 'Số PO',
          Icon: 'clipboard-text-outline',
          type: 'material-community',
          count: 0,
          code: 'A7',
        },
        {
          Title: 'ĐK làm thêm',
          Icon: 'timetable',
          type: 'material-community',
          count: 0,
          code: 'A8',
        },
      ],
      option: {
        grid: {
          containLabel: true,
          right: 10,
          left: 10,
          height: 200,
        },
        xAxis: {
          data: [],
          axisLabel: {
            interval: 0,
            rotate: 90,
            fontSize: 8,
          },
        },
        yAxis: {
          type: 'value',
        },
        series: [
          {
            label: {
              show: false,
              //position: 'top',
              color: 'black',
              fontSize: 8,
              marginTop: 0,
              position: 'inside',
              rotate: 90,
            },
            data: [],
            type: 'bar',
            color: 'rgba(254, 165, 0, 0.8)',
          },
        ],
      },
    };
  }

  loadMoreData() {
    this.nextPage();
  }
  componentDidMount() {
    this.GetAcount();
    this.GetItem_Dashboad();
  }
  GetAcount() {
    API_AM.GetHomeAM()
      .then(res => {
        var listCount = JSON.parse(res.data.data);
        this.setState({ListCountNoti: listCount});
        var _listMenu = this.state.listMenu;
        for (var i = 0; i < _listMenu.length; i++) {
          if (_listMenu[i].code == 'A1') {
            _listMenu[i].count = listCount.TicketRequestsImport;
          } else if (_listMenu[i].code == 'A2') {
            _listMenu[i].count = listCount.TicketRequestsExport;
          } else if (_listMenu[i].code == 'A3') {
            _listMenu[i].count = this.addPeriod(listCount.PurchaseOrders);
          } else if (_listMenu[i].code == 'A4') {
            _listMenu[i].count = listCount.POPaymentsBefore;
          } else if (_listMenu[i].code == 'A5') {
            _listMenu[i].count = listCount.POPaymentsAfter;
          } else if (_listMenu[i].code == 'A6') {
            _listMenu[i].count = this.addPeriod(listCount.ApplyLeaves);
          } else if (_listMenu[i].code == 'A7') {
            _listMenu[i].count = this.addPeriod(listCount.ListPO);
          } else if (_listMenu[i].code == 'A8') {
            _listMenu[i].count =
              listCount.ApplyOvertimes === null ? 0 : listCount.ApplyOvertimes;
          }
        }
        this.setState({
          listMenu: _listMenu,
        });
      })
      .catch(error => {
        console.log(error.data);
      });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        0;
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  GetItem_Dashboad = () => {
    API_AM.GetWarehouses(this.state.staticParam)
      .then(res => {
        //console.log("ccccccccccccc" , res)
        var _listDepartment = JSON.parse(res.data.data);
        //console.log("cccccccccccccaaaaaaaaaaaaaaaaaaa" , _listDepartment)
        //return
        var _listMenu = [];
        var _listDataActualSalary = [];
        for (var i = 0; i < _listDepartment.length; i++) {
          _listMenu.push(_listDepartment[i].WarehouseId);
          _listDataActualSalary.push(
            parseFloat(
              _listDepartment[i].InventoryAmount === null
                ? 0
                : _listDepartment[i].InventoryAmount,
            ),
          );
        }
        var _option = this.state.option;
        _option.xAxis.data = _listMenu;
        _option.series[0].data = _listDataActualSalary;
        this.setState({
          option: _option,
        });
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  ListEmpty = () => {
    if (this.state.list.length > 0 && this.state.listRequest.length > 0)
      return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null && strDate != '' && strDate != undefined) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <Header
          leftComponent={() => (
            <TouchableOpacity onPress={() => Actions.app()}>
              <Icon
                style={{color: 'black'}}
                name={'left'}
                type="antdesign"
                size={30}
              />
            </TouchableOpacity>
          )}
          centerComponent={{
            text: 'Dashboard',
            style: {color: 'black', fontSize: 20},
          }}
          containerStyle={{backgroundColor: '#fff'}}
        />
        <ScrollView>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={{flex: 3}}>
              <ScrollView>
                <FlatGrid
                  //Tùy chỉnh số ô
                  itemDimension={120}
                  items={this.state.listMenu}
                  style={styles.gridView}
                  renderItem={({item, index}) => (
                    <View
                      style={{
                        borderRadius: 10,
                        backgroundColor: '#FFFFFF',
                        height: 70,
                      }}>
                      <View style={[styles.itemContainerdetail]}>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{flex: 1}}>
                            <Icon
                              name={item.Icon}
                              type={item.type}
                              size={20}
                              color={AppColors.Maincolor}
                            />
                          </View>
                          <View style={{flex: 2}}>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                textAlign: 'right',
                                paddingRight: 20,
                                fontSize: 16,
                              }}>
                              {item.count}
                            </Text>
                          </View>
                        </View>
                        <View>
                          <Text style={{fontSize: 14}}>{item.Title}</Text>
                        </View>
                      </View>
                    </View>
                  )}
                />
              </ScrollView>
            </View>
            <View style={{flexDirection: 'row', paddingRight: 10}}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  marginLeft: 10,
                  flex: 1,
                }}>
                Báo cáo tiền tồn kho
              </Text>
            </View>
            <View style={{flex: 2}}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {/* <BarChart
                                    data={{
                                        labels: this.state.listDepartment,
                                        datasets: [
                                            {
                                                data: this.state.listDataActualSalary
                                            }
                                        ]
                                    }}
                                    onEndReached={() => this.loadMoreData()}
                                    width={1400}
                                    height={300}
                                    chartConfig={{
                                        backgroundGradientFrom: "#fff",
                                        color: (opacity = 1) => `rgba(20,0,240, ${opacity})`,
                                        labelColor: (opacity = 1) => `rgba(0,0,0, ${opacity})`,
                                    }}
                                    bezier
                                    style={{
                                        borderRadius: 10,
                                        margin: 10
                                    }}
                                /> */}
                <View style={styles.chartContainer}>
                  {this.state.option &&
                  this.state.option.xAxis.data.length > 0 &&
                  this.state.option.series[0].data.length > 0 ? (
                    <ECharts option={this.state.option} />
                  ) : null}
                </View>
              </ScrollView>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainerdetail: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 17,
    flexDirection: 'column',
  },
  FormALl: {
    fontSize: 14,
    marginLeft: 10,
    alignItems: 'flex-end',
    flex: 1,
  },
  chartContainer: {
    flexDirection: 'column',
    width: width,
    flex: 1,
    height: 300,
    margin: 0,
  },
  hidden: {display: 'none'},
});
//Redux
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChartDashboardAMComponent);
