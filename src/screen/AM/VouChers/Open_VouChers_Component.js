import React, {Component} from 'react';
import {
  Alert,
  FlatList,
  ImageBackground,
  RefreshControl,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Thumbnail} from 'native-base';
import {Divider, Button, Icon} from 'react-native-elements';
import {API_AM} from '../../../network';
import TabBar from '../../component/TabBar';

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  black: {
    color: 'black',
  },
  container: {
    flex: 1,
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
});

class Open_VouChers_Component extends Component {
  static title = 'Card';

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      Search: '',
      Data: null,
      ListAll: [],
    };
  }

  updateSearch = search => {
    this.setState({search});
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <TabBar addForm="home" />
        <Divider />
        <View style={{flexDirection: 'row', padding: 10, alignItems: 'center'}}>
          <View style={{flex: 3}}>
            <Thumbnail
              style={{width: '100%', height: 100}}
              source={require('../../../images/AM/Vouchers/iconVouchers.jpg')}
            />
          </View>
          <View style={{flex: 7}}>
            <Text style={{fontSize: 20, padding: 5}}>
              Trần Quang Trọng Trầ n Quang TrọngTrần Quang Trọng
            </Text>
          </View>
        </View>
        <Divider />
        <View style={{flexDirection: 'row', padding: 10}}>
          <View style={{flex: 3}}>
            <Text style={{fontSize: 20, padding: 5}}>Họ và tên:</Text>
            <Text style={{fontSize: 20, padding: 5}}>Giới tính:</Text>
            <Text style={{fontSize: 20, padding: 5}}>Ngày sinh:</Text>
            <Text style={{fontSize: 20, padding: 5}}>Email:</Text>
            <Text style={{fontSize: 20, padding: 5}}>Địa chỉ:</Text>
          </View>
          <View style={{flex: 7}}>
            <Text style={{fontSize: 20, padding: 5}}>Trần Quang Trọng</Text>
            <Text style={{fontSize: 20, padding: 5}}>Nam</Text>
            <Text style={{fontSize: 20, padding: 5}}>15/08/1998</Text>
            <Text style={{fontSize: 20, padding: 5}}>trongtq@esvn.com.vn</Text>
            <Text style={{fontSize: 20, padding: 5}}>
              Số 8/273 Trần Cung, Cổ nhuế 1,Bắc Từ Liêm, TP.Hà Nội
            </Text>
          </View>
        </View>

        <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
          <Button
            buttonStyle={{backgroundColor: 'red', borderRadius: 0}}
            icon={<Icon name="trash" size={30} color="#fff" type="evilicon" />}
            title=" Xóa chứng từ"
            titleStyle={{fontSize: 20}}
          />
        </View>
      </View>
    );
  }

  //form xóa
  deleteAPI = (id, name) => {
    Alert.alert(
      'Xóa ngân hàng',
      'Bạn có muốn xóa ' + name,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => this.deleteView(id)},
      ],
      {cancelable: false},
    );
  };
  //Xóa
  deleteView = id => {
    API.getById(id)
      .then(res => {
        // console.log(JSON.stringify(res))
        Actions.viewAPI({viewBank: res.data.Data});
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Open_VouChers_Component);
