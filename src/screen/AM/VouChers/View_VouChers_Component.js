import React, {Component} from 'react';
import {
  Alert,
  FlatList,
  ImageBackground,
  RefreshControl,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Thumbnail} from 'native-base';
import {Divider, Icon} from 'react-native-elements';
import {API_AM} from '../../../network';
import TabBar from '../../component/TabBar';

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  black: {
    color: 'black',
  },
  container: {
    flex: 1,
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
});

class View_VouChers_Component extends Component {
  static title = 'Card';

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      Search: '',
      Data: null,
      ListAll: [],
    };
  }

  updateSearch = search => {
    this.setState({search});
  };

  render() {
    return null;
  }
  GetAll = () => {
    API_AM.vouChersListAll()
      .then(res => {
        console.log('===========>' + res.data.data);
        this.setState({ListAll: JSON.parse(res.data.data)});
      })
      .catch(error => {
        console.log(error.data.data);
        console.log(error);
      });
  };
  //form xóa
  deleteAPI = (id, name) => {
    Alert.alert(
      'Xóa ngân hàng',
      'Bạn có muốn xóa ' + name,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => this.deleteView(id)},
      ],
      {cancelable: false},
    );
  };
  //Xóa
  deleteView = id => {
    API_AM.getById(id)
      .then(res => {
        // console.log(JSON.stringify(res))
        Actions.viewAPI({viewBank: res.data.Data});
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  //Chi tiết
  openView = () => {
    Actions.openvouchers();
    // API_AM.getById(id).then(res => {
    //     // console.log(JSON.stringify(res))
    //     Actions.viewAPI({ViewItem: res.data.Data});
    // }).catch(error => {
    //     console.log(error.data.data);
    // });
  };
  //Load lại
  _onRefresh = () => {
    this.setState({refreshing: true});
    API_AM.vouChersListAll()
      .then(res => {
        // console.log(JSON.stringify(res))
        this.setState({ListAll: JSON.parse(res.data.data), refreshing: false});
      })
      .catch(error => {
        this.setState({refreshing: false});
        console.log(error.data.data);
      });
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(View_VouChers_Component);
