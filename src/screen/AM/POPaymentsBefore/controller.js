import {
  API_ApplyOverTimes,
  API_TicketChecking,
  API_QuotationRequest,
} from '@network';
import {ErrorHandler} from '@error';

export default {
  // List nhan vien
  getAllEmployeeByDepart(callback) {
    API_ApplyOverTimes.GetEmployeeByDepartment()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  },
  getPeople_Quotation(callback) {
    API_QuotationRequest.getPeople_Quotation()
      .then(res => {
        const data = JSON.parse(res.data.data);
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  },
  getAllObjectKH(callback) {
    API_QuotationRequest.getAllObjectKH()
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        var list = [];
        for (let i = 0; i < _data.length; i++) {
          list.push({
            value: _data[i].CustomerId,
            text: '[' + _data[i].CustomerId + ']' + ' ' + _data[i].CustomerName,
          });
        }
        callback(list);
      })
      .catch(error => {
        alert(error);
      });
  },
  getKHbyCustomerId(data, callback) {
    var obj = {IdS: [data]};
    API_QuotationRequest.getKHbyCustomerId(obj)
      .then(rs => {
        var _data = JSON.parse(rs.data.data);
        callback(_data);
      })
      .catch(error => {
        alert(error);
      });
  },
  getAllWorkShift(callback) {
    API_ApplyOverTimes.getAllWorkShift()
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  },
  getNumberAuto(value, callback) {
    API_TicketChecking.GetNumberAuto(value)
      .then(res => {
        console.log(res + '=======> get Number auto');
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  },
  getAllOrder(search, callback) {
    let obj = {search: search || ''};
    API_QuotationRequest.getAllProduct(obj)
      .then(res => {
        const {data} = res.data;
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  },
  GetItemsByItemId(search, callback) {
    let obj = {search: search || ''};
    API_QuotationRequest.GetItemsByItemId(obj)
      .then(res => {
        const data = JSON.parse(res.data.data);
        callback(data);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  },
  GetUnitsAllJExcel(callback) {
    API_QuotationRequest.GetUnitsAllJExcel()
      .then(rs => {
        var _lstUnits = JSON.parse(rs.data.data);
        callback(_lstUnits);
      })
      .catch(error => {
        console.log(error.data);
      });
  },
  getEmployees(callback) {
    API_QuotationRequest.getEmployees()
      .then(rs => {
        var _lstUnits = JSON.parse(rs.data.data);
        callback(_lstUnits);
      })
      .catch(error => {
        console.log(error.data);
      });
  },
  removeAttactment(data, list, callback) {
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != data.FileName) {
        listNewValue.push(list[i]);
      }
    }
    callback(listNewValue);
  },
};
