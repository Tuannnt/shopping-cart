import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, Icon, ListItem, SearchBar} from 'react-native-elements';
import {API_VouchersAM} from '@network';
import TabBar from '../../component/TabBar';
import stylesAM from '../StyleAM';
import PopupSearch from '../PopupSearch';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import LoadingComponent from '../../component/LoadingComponent';
import FormSearch from '../../component/FormSearch';
import Combobox from '../../component/Combobox';
const ListCombobox_Status = [
  {
    text: 'Chọn tất cả',
    value: '',
  },
  {
    text: 'Đã hạch toán',
    value: 'H',
  },
  {
    text: 'Chưa hạch toán',
    value: 'T',
  },
];
const ListCombobox_VoucherType = [
  {
    text: 'Nhập mua',
    value: 'N',
  },
  {
    text: 'Nhập hàng bán bị trả lại',
    value: 'L',
  },
  {
    text: 'Nhập thành phẩm sản xuất',
    value: 'M',
  },
  {
    text: 'Nhập khác',
    value: 'O',
  },
  {
    text: 'Phân bổ chi phí',
    value: 'P',
  },
  {
    text: 'Xuất khác',
    value: 'X',
  },
  {
    text: 'Xuất trả hàng mua',
    value: 'T',
  },
  {
    text: 'Xuất chuyển kho',
    value: 'C',
  },
  {
    text: 'Xuất dùng nội bộ và sản xuất',
    value: 'D',
  },
  {
    text: 'Xuất bán hàng',
    value: 'B',
  },
  {
    text: 'Xuất chuyển dự án',
    value: 'A',
  },
  {
    text: 'Giảm giá hàng mua',
    value: 'G',
  },
  {
    text: 'Giảm giá hàng bán',
    value: 'H',
  },
  {
    text: 'Nhập hàng đại lý, gia công, ủy thác',
    value: 'U',
  },
  {
    text: 'Xuất hàng đại lý, gia công, ủy thác',
    value: 'E',
  },
];
class GetAllPaging_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ListAll: [],
      EvenType: '',
      EvenClickSearch: false,
      ListVoucherType: [],
      EvenFromSearch: false,
      EvenFormQR: false,
      refreshing: false,
      Data: null,
      loading: false,
      VoucherTypeName: '',
    };
    this._search = {
      Title: '',
      VoucherType: '',
      ListObjectType: [],
      IntStartDate: '20200401',
      IntEndDate: '20200430',
      Status: '',
      Objectvalue: '',
      Length: 25,
      NumberPage: 0,
      QueryOrderBy: 'AccountingDate DESC',
    };
  }

  componentDidMount() {
    // this.nextPage();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'QRCode') {
      var link = nextProps.Data.Value; // dữ liệu QR quét được
      Actions.voucherAMViewItem({Id: link});
    } else {
      this.nextPage();
    }
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.Title = search;
    this._search.NumberPage = 0;
    this.nextPage();
  };
  //List danh sách phân trang
  nextPage() {
    this.setState({loading: true});
    this._search.NumberPage++;
    API_VouchersAM.VouchersAM_GetAllPaging(this._search)
      .then(res => {
        this.state.ListAll = this.state.ListAll.concat(
          JSON.parse(res.data.data).data,
        );
        this.setState({
          ListAll: this.state.ListAll,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log(error);
      });
  }
  loadMoreData() {
    this.nextPage();
  }
  CustomeListAll = item => (
    <FlatList
      data={item}
      style={{flex: 1}}
      renderItem={({item, index}) => (
        <View>
          <ListItem
            subtitle={() => {
              return (
                <View style={{flexDirection: 'column', marginTop: -20}}>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 2, flexDirection: 'row'}}>
                      <View>
                        <Text style={AppStyles.Titledefault}>
                          {item.VoucherNo}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          {item.Description}
                        </Text>
                      </View>
                    </View>
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {textAlign: 'right', justifyContent: 'center'},
                        ]}>
                        {this.customVoucherType(item.VoucherType)}
                      </Text>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {textAlign: 'right', justifyContent: 'center'},
                          item.Status == 'H'
                            ? {color: AppColors.AcceptColor}
                            : {color: AppColors.PendingColor},
                        ]}>
                        {this.customStatus(item.Status)}
                      </Text>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {textAlign: 'right', justifyContent: 'center'},
                        ]}>
                        {this.addPeriod(item.Amount)}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                      }}>
                      <Text style={AppStyles.Textdefault}>Ngày HT : </Text>
                      <Text style={AppStyles.Textdefault}>
                        {this.CustomHT(item.AccountingDate)}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                      }}>
                      <Text style={AppStyles.Textdefault}>Ngày CT : </Text>
                      <Text style={AppStyles.Textdefault}>
                        {this.customCT(item.VoucherDate)}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
            bottomDivider
            //chevron
            onPress={() => this.onViewItem(item.Id)}
          />
        </View>
      )}
      onEndReached={() =>
        this.state.ListAll.length >=
        this._search.NumberPage * this._search.Length
          ? this.loadMoreData()
          : null
      }
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['black']}
        />
      }
    />
  );
  render() {
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={AppStyles.container}>
          <TabBar
            title={'Phiếu nhập - xuất kho'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            FormQR={true}
            QRMultiple={true}
            BackModuleByCode={'AM'}
          />
          <Divider />
          <View>
            <View style={{padding: 10}}>
              <TouchableOpacity
                style={[AppStyles.FormInput]}
                onPress={() => this.onActionCombobox_VoucherType()}>
                <Text
                  style={[
                    AppStyles.TextInput,
                    this.state.VoucherTypeName == ''
                      ? {color: AppColors.gray}
                      : null,
                  ]}>
                  {this.state.VoucherTypeName !== ''
                    ? this.state.VoucherTypeName
                    : 'Tìm kiếm...'}
                </Text>
              </TouchableOpacity>
            </View>
            {this.state.EvenFromSearch == true ? (
              <FormSearch
                CallbackSearch={callback => this.updateSearch(callback)}
              />
            ) : null}
          </View>
          <View style={{flex: 1}}>
            {this.state.ListAll.length > 0 ? (
              this.CustomeListAll(this.state.ListAll)
            ) : (
              <View style={AppStyles.centerAligned}>
                <Text>Không có dữ liệu</Text>
              </View>
            )}
          </View>
          {/* <Combobox
                            value={this._search.ListObjectType}
                            TypeSelect={"multiple"}// single or multiple
                            callback={this.onPressClickVoucherType}
                            data={ListCombobox_VoucherType}
                            nameMenu={'chọn '}
                            eOpen={this.openCombobox_VoucherType}
                            position={'bottom'}
                        /> */}
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  //Begin cretaed popup
  CustomSearch = () =>
    this.state.EvenType == 'Status' ? (
      <View style={{color: AppColors.Maincolor}}>
        <PopupSearch
          Title={'Trạng thái'}
          ListCheckBox={ListCombobox_Status}
          callbackclose={() => this.setState({EvenClickSearch: false})}
          TypeCheck={'CheckOne'}
          callbackValue={callback => this.onPressClickStatus(callback)}
        />
      </View>
    ) : this.state.EvenType == 'VoucherType' ? (
      <View style={{color: AppColors.Maincolor}}>
        <PopupSearch
          Title={'Loại chứng từ'}
          ListCheckBox={ListCombobox_VoucherType}
          callbackclose={() => this.setState({EvenClickSearch: false})}
          TypeCheck={'CheckLot'}
          callbackValue={callback => this.onPressClickVoucherType(callback)}
        />
      </View>
    ) : null;
  //view item
  onViewItem(item) {
    Actions.voucherAMViewItem({Id: item});
  }
  //#region  combobox Priority
  _openCombobox_VoucherType() {}
  openCombobox_VoucherType = d => {
    this._openCombobox_VoucherType = d;
  };
  onActionCombobox_VoucherType() {
    this._openCombobox_VoucherType();
  }
  onPressClickVoucherType = rs => {
    var value = [];
    var _text = '';
    for (let i = 0; i < rs.length; i++) {
      if (_text == '') {
        _text = rs[i].text;
      } else {
        _text = _text + ' , ' + rs[i].text;
      }
      if (rs[i].value !== null) {
        value.push(rs[i].value);
      }
    }
    this._search.ListObjectType = value;
    this.setState({VoucherTypeName: _text});
    this.updateSearch(this._search.Title);
  };
  //#endregion
  onPressClick(para) {
    switch (para) {
      case 'Status': {
        this.setState({
          EvenClickSearch: true,
          EvenType: para,
        });
        break;
      }
      case 'VoucherType': {
        this.setState({
          EvenClickSearch: true,
          EvenType: para,
        });
        break;
      }
      default: {
        break;
      }
    }
  }
  onPressClickStatus(value) {
    this.setState({EvenClickSearch: false});
    this._search.Status = value;
    this.updateSearch(this._search.Title);
  }

  customStatus(item) {
    if (item == 'H') {
      return 'Đã hạch toán';
    } else if (item == 'T') {
      return 'Chưa hạch toán';
    } else {
      return '';
    }
  }
  customVoucherType(data) {
    if (data === 'N') {
      return 'Nhập hàng mua';
    } else if (data === 'L') {
      return 'Nhập hàng bán bị trả lại';
    } else if (data === 'M') {
      return 'Nhập thành phẩm sản xuất';
    } else if (data === 'O') {
      return 'Nhập khác ';
    } else if (data === 'P') {
      return 'Phân bổ chi phí ';
    } else if (data === 'X') {
      return 'Xuất khác';
    } else if (data === 'T') {
      return 'Xuất trả hàng mua';
    } else if (data === 'C') {
      return 'Xuất chuyển kho';
    } else if (data === 'D') {
      return 'Xuất dùng nội bộ và sản xuất';
    } else if (data === 'B') {
      return 'Xuất bán hàng';
    } else if (data === 'A') {
      return 'Xuất chuyển dự án';
    } else if (data === 'G') {
      return 'Giảm giá hàng mua';
    } else if (data === 'H') {
      return 'Giảm giá hàng bán';
    } else if (data === 'U') {
      return 'Nhập hàng đại lý, gia công, ủy thác';
    } else if (data === 'E') {
      return 'Xuất hàng đại lý, gia công, ủy thác';
    }
  }
  //custom ngày HT
  CustomHT(item) {
    if (item != null && item != undefined && item != '') {
      var yyyy = item.substring(0, 4);
      var MM = item.substring(4, 6);
      var dd = item.substring(6, 8);
      return dd + '/' + MM + '/' + yyyy;
    } else {
      return '';
    }
  }
  //định dạng ngày
  customCT(DataTime) {
    if (DataTime != null) {
      var SplitTime = String(DataTime).split(' ');
      //định dạng ngày
      var SplitTime_v1 = SplitTime[0].split('/');
      var MM = '';
      if (SplitTime_v1[0].length < 2) {
        MM = '0' + SplitTime_v1[0];
      } else {
        MM = SplitTime_v1[0];
      }
      var dd = '';
      if (SplitTime_v1[1].length < 2) {
        dd = '0' + SplitTime_v1[1];
      } else {
        dd = SplitTime_v1[1];
      }
      var date = dd + '/' + MM + '/' + SplitTime_v1[2];
      return date;
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  //Load lại
  _onRefresh = () => {
    // this.nextPage();
    this.setState({refreshing: true});
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(GetAllPaging_Component);
