import React, { Component } from 'react';
import { FlatList, Keyboard, StyleSheet, Text, TouchableWithoutFeedback, View, Alert, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_VouchersAM, API } from "../../../network";
import TabBar_Title from "../../component/TabBar_Title";
import { Divider, ListItem } from "react-native-elements";
import Fonts from "../../../theme/fonts";
import { Actions } from 'react-native-router-flux';
import stylesAM from '../StyleAM';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Dialog from "react-native-dialog";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
const listtabbarBotom = [
    {
        Title: "Hàng hoá",
        Icon: "list",
        Type: "entypo",
        Value: "HH",
        Checkbox: true
    },
    {
        Title: "Thống kê",
        Icon: "barschart",
        Type: "antdesign",
        Value: "TK",
        Checkbox: false
    },
    {
        Title: "Đính kèm",
        Icon: "attachment",
        Type: "entypo",
        Value: "DK",
        Checkbox: false
    }
]
class VoucherAM_ViewItem_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Data: null,
            DataDetail: [],
            VoucherDetails: [],
            InventoryVouchersAttachments: [],
            VoucherCostDetails: [],
            EvenViewDetail: "HH",
        };
    }
    componentDidMount(){
        this.getItem();
    }
    getItem() {
        let obj = { Id: this.props.Id }
        console.log(this.props.Id)
        API_VouchersAM.VouchersAM_GetItem(obj).then(res => {
            this.setState({
                Data: JSON.parse(res.data.data).Header,
                DataDetail: JSON.parse(res.data.data).Detail,
                VoucherDetails: JSON.parse(res.data.data).VoucherDetails,
                InventoryVouchersAttachments: JSON.parse(res.data.data).InventoryVouchersAttachments,
                VoucherCostDetails: JSON.parse(res.data.data).VoucherCostDetails,
            });
        }).catch(error => {
            Alert.alert("Thông báo", "Không tìm thấy dữ liệu",
                [
                    { text: 'Đóng', onPress: () => this.callBackList() },
                ],
                { cancelable: true }
            )
        });
    }
    CustomView = (item) => (
        <View style={{ flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 6, flexDirection: 'column', marginLeft: 5 }}>
                    <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Thông tin chung</Text>
                </View>
                <View style={{ flex: 3, flexDirection: 'column', marginLeft: 5 }}>
                    <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Chứng Từ</Text>
                </View>
            </View>
            <View style={stylesAM.ViewContent_full}>
                <View style={{ flex: 6, flexDirection: 'column', margin: 5, borderRightWidth: 0.3 }}>
                    <View style={stylesAM.ViewContent}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Mã nhà cung cấp:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.ObjectId}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Tên nhà cung cấp:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.ObjectName}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Người giao/nhận:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.EmployeeName}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Loại chứng từ:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {this.customVoucherType(item.VoucherType)}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Diễn giải:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.Description}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 3, flexDirection: 'column', margin: 5 }}>
                    {/* chứng từ */}
                    <View style={stylesAM.ViewContent}>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Ngày hạch toán:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {this.CustomHT(item.AccountingDate)}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Ngày chứng từ:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {this.customDate(item.VoucherDate)}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Quyển chứng từ:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.VoucherTemplateNo}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Số chứng từ:</Text>
                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.VoucherNo}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Trạng thái:</Text>
                            <Text style={[AppStyles.Textdefault, item.Status == 'H' ? { color: AppColors.AcceptColor } : { color: AppColors.UntreatedColor }]}> {this.customStatus(item.Status)}</Text>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
    CustomViewDetail = (para) => (
        <ScrollView>
            {para.map((item, index) => (
                <View style={{ fontSize: 11, fontFamily: Fonts.base.family }}>
                    <ListItem
                        title={<Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}> {this.checkMessage(item.ItemName, 7)}</Text>}
                        subtitle={() => {
                            return (
                                <View style={{ flexDirection: 'column' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4, flexDirection: 'row' }}>
                                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Mã hàng:</Text>
                                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.ItemId}</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row' }}>
                                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>ĐVT:</Text>
                                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.UnitId}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>

                                        <View style={{ flex: 1, flexDirection: 'row' }}>
                                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>SL:</Text>
                                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {this.addPeriod(item.Quantity)}</Text>
                                        </View>
                                        <View style={{ flex: 2, flexDirection: 'row' }}>
                                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Đơn giá:</Text>
                                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {this.addPeriod(item.UnitPrice)}</Text>
                                        </View>
                                        <View style={{ flex: 2, flexDirection: 'row' }}>
                                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Thành tiền:</Text>
                                            <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {this.addPeriod(item.Amount)}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText]}>Kho:</Text>
                                        <Text style={[AppStyles.Textdefault, stylesAM.PaddingText]}> {item.WarehouseId}</Text>
                                    </View>
                                </View>
                            );
                        }}
                        bottomDivider
                        onPress={() => this.ViewPopupDetail(item.OrganizationGuid, item.VoucherGuid, item.SortOrder)}
                    />
                </View>
            ))}
        </ScrollView>
    )
    CustomViewVoucherDetails = (para) => (
        <View style={{ flexDirection: 'column' }}>

        </View>
    )
    CustomViewInventoryVouchersAttachments = (para) => (
        <View style={{ flexDirection: 'column' }}>

        </View>
    )
    render() {
        return (
            this.state.Data != null ?
                <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => { Keyboard.dismiss() }}>
                    <View style={{ flex: 1, backgroundColor: '#fff', flexDirection: 'column' }}>
                        <TabBar_Title
                            title={this.state.Data.VoucherNo}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        {/* hiển thị thông tin chung */}
                        {this.CustomView(this.state.Data)}
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <View style={{ flex: 1, marginBottom: 35 }}>
                                {this.state.EvenViewDetail == "HH" ?
                                    // Thông tin hàng hoá
                                    <View style={{ flexDirection: 'column' }}>
                                        <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText, { marginLeft: 5, paddingBottom: 10, borderBottomWidth: 0.5 }]}>Thông tin hàng hoá</Text>
                                        {this.state.DataDetail.length > 0 ? this.CustomViewDetail(this.state.DataDetail) :
                                            <View style={stylesAM.NullData}>
                                                <Text style={[AppStyles.Textdefault, AppStyles.containerCentered, stylesAM.PaddingText]}>Không có dữ liệu</Text>
                                            </View>
                                        }
                                    </View>
                                    : this.state.EvenViewDetail == "TK" ?
                                        // thông tin thống kê
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText, { marginLeft: 5, paddingBottom: 10, borderBottomWidth: 0.5 }]}>Thông tin thống kê</Text>
                                            {this.state.VoucherDetails.length > 0 ? this.CustomViewVoucherDetails(this.state.VoucherDetails) :
                                                <View style={stylesAM.NullData}>
                                                    <Text style={[AppStyles.Textdefault, AppStyles.containerCentered, stylesAM.PaddingText]}>Không có dữ liệu</Text>
                                                </View>
                                            }
                                        </View>
                                        : this.state.EvenViewDetail == "DK" ?
                                            // Thông tin đính kèm
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text style={[AppStyles.Labeldefault, stylesAM.PaddingText, { marginLeft: 5, paddingBottom: 10, borderBottomWidth: 0.5 }]}>Thông tin đính kèm</Text>
                                                {this.state.InventoryVouchersAttachments.length > 0 ? this.CustomViewInventoryVouchersAttachments(this.state.InventoryVouchersAttachments) :
                                                    <View style={stylesAM.NullData}>
                                                        <Text style={[AppStyles.Textdefault, AppStyles.containerCentered, stylesAM.PaddingText]}>Không có dữ liệu</Text>
                                                    </View>
                                                }
                                            </View>
                                            : null
                                }
                            </View>
                            {/* hiển thị nút tuỳ chọn */}
                            <View style={AppStyles.StyleTabvarBottom}>
                                <TabBarBottomCustom
                                    ListData={listtabbarBotom}
                                    onCallbackValueBottom={(callback) => this.setState({ EvenViewDetail: callback })}
                                />
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                : null
        );
    }
    callBackList() {
        Actions.pop();
    }
    ViewPopupDetail(org, vcg, sod) {
        var _listDetail = this.state.DataDetail;
        var obj = _listDetail.find(x => x.OrganizationGuid == org && x.VoucherGuid == vcg && x.SortOrder == sod);
        if (obj !== null) {
            this.setState({ ItemDetail: obj })
        }
        Actions.voucherAMViewDetail({ ItemDetail: obj })
    }
    offPopup() {
        this.setState({ ItemDetail: null })
    }
    //custom ngày HT
    CustomHT(item) {
        var _item = item.toString();
        if (_item != null && _item != undefined && _item != "") {
            var yyyy = _item.substring(0, 4);
            var MM = _item.substring(4, 6);
            var dd = _item.substring(6, 8);
            return dd + "/" + MM + "/" + yyyy
        } else {
            return ""
        }
    }
    //định dạng ngày "2020-04-01T00:00:00"
    customDate(DataTime) {
        if (DataTime != null) {
            var SplitTime = String(DataTime).split('T');
            //định dạng ngày
            var SplitTime_v1 = SplitTime[0].split('-');
            var date = SplitTime_v1[2] + "/" + SplitTime_v1[1] + "/" + SplitTime_v1[0]
            return date
        } else {
            return "";
        }
    }
    customStatus(item) {
        if (item == "H") {
            return "Đã hạch toán"
        } else if (item == "T") {
            return "Chưa hạch toán"
        } else {
            return ""
        }
    }
    customVoucherType(data) {
        if (data === "N") {
            return "Nhập hàng mua";
        }
        else if (data === "L") {
            return "Nhập hàng bán bị trả lại";
        }
        else if (data === "M") {
            return "Nhập thành phẩm sản xuất";
        }
        else if (data === "O") {
            return "Nhập khác ";
        }
        else if (data === "P") {
            return "Phân bổ chi phí ";
        }
        else if (data === "X") {
            return "Xuất khác";
        }
        else if (data === "T") {
            return "Xuất trả hàng mua";
        }
        else if (data === "C") {
            return "Xuất chuyển kho";
        }
        else if (data === "D") {
            return "Xuất dùng nội bộ và sản xuất";
        }
        else if (data === "B") {
            return "Xuất bán hàng";
        }
        else if (data === "A") {
            return "Xuất chuyển dự án";
        }
        else if (data === "G") {
            return "Giảm giá hàng mua";
        }
        else if (data === "H") {
            return "Giảm giá hàng bán";
        }
        else if (data === "U") {
            return "Nhập hàng đại lý, gia công, ủy thác";
        }
        else if (data === "E") {
            return "Xuất hàng đại lý, gia công, ủy thác";
        }
    }
    //định dạng tiền tệ
    addPeriod(nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //kiểm tra độ dài tin nhắn
    checkMessage(str, i) {
        if (str !== null && str !== "") {
            var splitted = str.split(" ");
            var strreturn = "";
            var j = 0;
            for (var a = 0; a < splitted.length; a++) {
                j = j + 1;
                if (j == i) {
                    strreturn = strreturn + " " + splitted[a] + "...";
                    break
                }
                else {
                    strreturn = strreturn + " " + splitted[a];
                }
            }
            return strreturn.trim();
        } else {
            return "Nhóm vừa khởi tạo"
        }
    }
}
const mapStateToProps = state => ({
    applications: state.user.data
})
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(VoucherAM_ViewItem_Component);
