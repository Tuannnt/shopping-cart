import React, { Component } from 'react';
import { FlatList, Keyboard, StyleSheet, Text, TouchableWithoutFeedback, View, Alert, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_VouchersAM, API } from "../../../network";
import TabBar_Title from "../../component/TabBar_Title";
import { Divider, ListItem } from "react-native-elements";
import Fonts from "../../../theme/fonts";
import { Actions } from 'react-native-router-flux';
import stylesAM from '../StyleAM';
import Dialog from "react-native-dialog";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
class VoucherAM_ViewDetail_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Data: null,
            DataDetail: [],
            VoucherDetails: [],
            InventoryVouchersAttachments: [],
            VoucherCostDetails: [],
            EvenViewDetail: "HH",
            ItemDetail: this.props.ItemDetail
        };
    }

    componentDidMount(){
    }
    render() {
        return (
            <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => { Keyboard.dismiss() }}>
                <View style={{ flex: 1, backgroundColor: '#fff', flexDirection: 'column' }}>
                    <TabBar_Title
                        title='Thông tin chi tiết'
                        callBack={() => this.callBackList()} />
                    <Divider />
                    {this.state.ItemDetail != null ? this.returnPopupDetail(this.state.ItemDetail) : null}
                </View>
            </TouchableWithoutFeedback>
        );
    }
    //hiển thị chi tiết hàng hoá
    returnPopupDetail = (para) => (
        <View style={{ flexDirection: 'column', margin: 10 }}>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Mã hàng :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText,  { flex: 3}]}>{para.ItemId}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Tên hàng :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 3 }]}>{para.ItemName}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Kho :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 3 }]}>{para.WarehouseId}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>ĐVT :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 3 }]}>{para.UnitId}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Tài khoản nợ :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 1, textAlign: 'right' }]}>{this.addPeriod(para.DebtAccountId)}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Tài khoản có :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 1, textAlign: 'right' }]}>{this.addPeriod(para.CreditAccountId)}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Thống Kê nợ :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 1, textAlign: 'right' }]}>{this.addPeriod(para.DebtStatisticId)}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Giá vốn :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 1, textAlign: 'right' }]}>{this.addPeriod(para.CapitalPrice)}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Tiền vốn :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 1, textAlign: 'right' }]}>{this.addPeriod(para.Quantity)}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Số Lượng :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 1, textAlign: 'right' }]}>{this.addPeriod(para.Quantity)}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Đơn giá :</Text>
                <Text style={[AppStyles.Textdefault,stylesAM.PaddingText, { flex: 1, textAlign: 'right' }]}>{this.addPeriod(para.UnitPrice)}</Text>
            </View>
            <View style={{ flexDirection: 'row' }} >
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1 }]}>Thành tiền :</Text>
                <Text style={[AppStyles.Labeldefault,stylesAM.PaddingText, { flex: 1, textAlign: 'right' }]}>{this.addPeriod(para.Amount)}</Text>
            </View>
        </View>
    )
    callBackList() {
        Actions.pop();
    }
    ViewPopupDetail(org, vcg, sod) {
        var _listDetail = this.state.DataDetail;
        var obj = _listDetail.find(x => x.OrganizationGuid == org && x.VoucherGuid == vcg && x.SortOrder == sod);
        if (obj !== null) {
            this.setState({ ItemDetail: obj })
        }
    }
    customStatus(item) {
        if (item == "H") {
            return "Đã hạch toán"
        } else if (item == "T") {
            return "Chưa hạch toán"
        } else {
            return ""
        }
    }
    customVoucherType(data) {
        if (data === "N") {
            return "Nhập hàng mua";
        }
        else if (data === "L") {
            return "Nhập hàng bán bị trả lại";
        }
        else if (data === "M") {
            return "Nhập thành phẩm sản xuất";
        }
        else if (data === "O") {
            return "Nhập khác ";
        }
        else if (data === "P") {
            return "Phân bổ chi phí ";
        }
        else if (data === "X") {
            return "Xuất khác";
        }
        else if (data === "T") {
            return "Xuất trả hàng mua";
        }
        else if (data === "C") {
            return "Xuất chuyển kho";
        }
        else if (data === "D") {
            return "Xuất dùng nội bộ và sản xuất";
        }
        else if (data === "B") {
            return "Xuất bán hàng";
        }
        else if (data === "A") {
            return "Xuất chuyển dự án";
        }
        else if (data === "G") {
            return "Giảm giá hàng mua";
        }
        else if (data === "H") {
            return "Giảm giá hàng bán";
        }
        else if (data === "U") {
            return "Nhập hàng đại lý, gia công, ủy thác";
        }
        else if (data === "E") {
            return "Xuất hàng đại lý, gia công, ủy thác";
        }
    }
    //định dạng tiền tệ
    addPeriod(nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //kiểm tra độ dài tin nhắn
    checkMessage(str, i) {
        if (str !== null && str !== "") {
            var splitted = str.split(" ");
            var strreturn = "";
            var j = 0;
            for (var a = 0; a < splitted.length; a++) {
                j = j + 1;
                if (j == i) {
                    strreturn = strreturn + " " + splitted[a] + "...";
                    break
                }
                else {
                    strreturn = strreturn + " " + splitted[a];
                }
            }
            return strreturn.trim();
        } else {
            return "Nhóm vừa khởi tạo"
        }
    }
}
const mapStateToProps = state => ({
    applications: state.user.data
})
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(VoucherAM_ViewDetail_Component);
