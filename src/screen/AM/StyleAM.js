import { StyleSheet } from 'react-native';
import { Label } from 'native-base';

export default StyleSheet.create({
    //-------------------------------------------------GetAllPaging----------------------------
    GetAllPaging_container: {
        flex: 1,
        backgroundColor: '#FFF',
        borderBottomWidth: 0.5
    },
    GetAllPaging_ViewTabOnchange: {
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row',
        padding:3
    },
    
    GetAllPaging_ClickType: {
        borderWidth: 0.5,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    GetAllPaging_ClickStatus: {
        borderWidth: 0.5,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    
    GetAllPaging_ClickSearch: {
        backgroundColor: '#F5DA81'
    },
    GetAllPaging_ViewTabOnchange_Even: {
        flex:1
    },
    GetAllPaging_Even: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    //-------------------------------------------------VoucherAM_ViewItem----------------------------

    PaddingText:{
        padding:3
    },
    ViewContent:{
        flexDirection: 'column', 
    },
    ViewContent_full:{
        flexDirection: 'row', 
        padding: 2, 
        margin:3,
        borderRadius: 10, 
        borderWidth: 0.5,
        paddingBottom:10,
        paddingTop:10
    },
     //-------------------------------------------------Popupsearch----------------------------

});
