import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, ListItem} from 'react-native-elements';
import {API_Transfer} from '@network';
import TabBar from '../../component/TabBar';
import stylesAM from '../StyleAM';
import FormSearch from '../../component/FormSearch';
import {AppStyles, AppColors} from '@theme';
import MultiSelect_Component from '../../component/MultiSelect_Component';
const ListCombobox_Status = [
  {
    title: 'Chọn tất cả',
    value: '',
  },
  {
    title: 'Đã hạch toán',
    value: 'H',
  },
  {
    title: 'Chưa hạch toán',
    value: 'T',
  },
];
const ListCombobox_VoucherType = [
  {
    name: 'Xuất chuyển kho dự án',
    id: 'A',
  },
  {
    name: 'Xuất chuyển kho nội bộ',
    id: 'C',
  },
  {
    name: 'Xuất chuyển kho gửi bán đại lý',
    id: 'Y',
  },
  {
    name: 'Xuất chuyển kho kiêm vận chuyển nội bộ',
    id: 'V',
  },
];
class Transfer_ListAll_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ListAll: [],
      EvenType: '',
      EvenClickSearch: false,
      ListVoucherType: [],
      EvenFromSearch: false,
      EvenFormQR: false,
      refreshing: false,
      Data: null,
      loading: false,
    };
    this._search = {
      Title: '',
      VoucherType: '',
      ListObjectType: [],
      IntStartDate: '20200401',
      IntEndDate: '20200430',
      Status: '',
      Length: 25,
      NumberPage: 0,
      QueryOrderBy: 'AccountingDate DESC',
    };
  }

  componentDidMount(): void {
    this.nextPage();
  }

  componentWillReceiveProps(nextProps) {
    this.nextPage();
  }
  //Tìm kiếm
  updateSearch = search => {
    this.setState({
      ListAll: [],
    });
    this._search.Title = search;
    this._search.NumberPage = 0;
    this.nextPage();
  };

  //List danh sách phân trang
  nextPage() {
    console.log(this.props);
    this.setState({loading: true});
    this._search.NumberPage++;
    API_Transfer.Transfer_GetAllPaging(this._search)
      .then(res => {
        this.state.ListAll = this.state.ListAll.concat(
          JSON.parse(res.data.data).data,
        );
        this.setState({
          ListAll: this.state.ListAll,
          refreshing: false,
        });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log(error);
      });
  }
  loadMoreData() {
    this.nextPage();
  }
  CustomeListAll = item => (
    <FlatList
      data={item}
      style={{flex: 1}}
      renderItem={({item, index}) => (
        <View>
          <ListItem
            subtitle={() => {
              return (
                <View style={{flexDirection: 'column', marginTop: -20}}>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 3, flexDirection: 'row'}}>
                      <View>
                        <Text style={AppStyles.Titledefault}>
                          {item.VoucherNo}
                        </Text>
                        <Text style={AppStyles.Textdefault}>
                          {item.Description}
                        </Text>
                      </View>
                    </View>
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {textAlign: 'right', justifyContent: 'center'},
                        ]}>
                        {this.customVoucherType(item.VoucherType)}
                      </Text>
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {textAlign: 'right', justifyContent: 'center'},
                          item.Status == 'H'
                            ? {color: AppColors.AcceptColor}
                            : {color: AppColors.PendingColor},
                        ]}>
                        {this.customStatus(item.Status)}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <Text style={AppStyles.Textdefault}>Ngày HT</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <Text style={AppStyles.Textdefault}>Ngày CT</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <Text style={AppStyles.Textdefault}>Tổng tiền</Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <Text style={AppStyles.Textdefault}>
                        {this.CustomHT(item.AccountingDate)}
                      </Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <Text style={AppStyles.Textdefault}>
                        {this.customCT(item.VoucherDate)}
                      </Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <Text style={AppStyles.Textdefault}>
                        {this.addPeriod(item.Amount)}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
            bottomDivider
            //chevron
            onPress={() => this.onViewItem(item.Id)}
          />
        </View>
      )}
      onEndReached={() => this.loadMoreData()}
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['black']}
        />
      }
    />
  );
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={stylesAM.GetAllPaging_container}>
          <TabBar
            title={'Phiếu chuyển kho'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
            FormQR={true}
            CallbackFormQR={callback => this.setState({EvenFormQR: callback})}
            Module={'AM'}
            // addForm={"fdsàds"}
          />
          <Divider />
          <View>
            <View style={stylesAM.GetAllPaging_ViewTabOnchange}>
              <MultiSelect_Component
                ListCombobox={ListCombobox_VoucherType}
                CallBackListCombobox={callback =>
                  this.onPressClickVoucherType(callback)
                }
              />
            </View>
            {this.state.EvenClickSearch == true ? this.CustomSearch() : null}
            {this.state.EvenFromSearch == true ? (
              <FormSearch
                CallbackSearch={callback => this.updateSearch(callback)}
              />
            ) : null}
          </View>
          <View style={{flex: 1}}>
            {this.state.ListAll.length > 0 ? (
              this.CustomeListAll(this.state.ListAll)
            ) : (
              <View style={AppStyles.centerAligned}>
                <Text>Không có dữ liệu</Text>
              </View>
            )}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  //view item
  onViewItem(item) {
    Actions.transferViewItem({Id: item});
  }

  onSwipeClose(item) {
    this.setState({
      model: {},
    });
  }
  onclickreturnSearch() {
    this._search.Title = '';
    this.updateSearch('');
  }
  onPressClick(para) {
    switch (para) {
      case 'Status': {
        this.setState({
          EvenClickSearch: true,
          EvenType: para,
        });
        break;
      }
      case 'VoucherType': {
        this.setState({
          EvenClickSearch: true,
          EvenType: para,
        });
        break;
      }
      default: {
        break;
      }
    }
  }
  onPressClickStatus(value) {
    this.setState({EvenClickSearch: false});
    this._search.Status = value;
    this.updateSearch(this._search.Title);
  }
  onPressClickVoucherType(item) {
    this.setState({EvenClickSearch: false});
    this._search.ListObjectType = item;
    this.updateSearch(this._search.Title);
  }
  customStatus(item) {
    if (item == 'H') {
      return 'Đã hạch toán';
    } else if (item == 'T') {
      return 'Chưa hạch toán';
    } else {
      return '';
    }
  }
  customVoucherType(data) {
    if (data === 'A') {
      return 'Xuất chuyển kho dự án';
    } else if (data === 'C') {
      return 'Xuất chuyển kho nội bộ';
    } else if (data === 'Y') {
      return 'Xuất chuyển kho gửi bán đại lý';
    } else if (data === 'V') {
      return 'Xuất chuyển kho kiêm vận chuyển nội bộ';
    }
  }
  //custom ngày HT
  CustomHT(item) {
    if (item != null && item != undefined && item != '') {
      var yyyy = item.substring(0, 4);
      var MM = item.substring(4, 6);
      var dd = item.substring(6, 8);
      return dd + '/' + MM + '/' + yyyy;
    } else {
      return '';
    }
  }
  //định dạng ngày
  customCT(DataTime) {
    if (DataTime != null) {
      var SplitTime = String(DataTime).split(' ');
      //định dạng ngày
      var SplitTime_v1 = SplitTime[0].split('/');
      var MM = '';
      if (SplitTime_v1[0].length < 2) {
        MM = '0' + SplitTime_v1[0];
      } else {
        MM = SplitTime_v1[0];
      }
      var dd = '';
      if (SplitTime_v1[1].length < 2) {
        dd = '0' + SplitTime_v1[1];
      } else {
        dd = SplitTime_v1[1];
      }
      var date = dd + '/' + MM + '/' + SplitTime_v1[2];
      return date;
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  //Load lại
  _onRefresh = () => {
    this.nextPage();
    this.setState({refreshing: true});
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Transfer_ListAll_Component);
