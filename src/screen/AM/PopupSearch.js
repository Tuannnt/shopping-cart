import React, { Component } from 'react';
import { View, CheckBox, Text } from 'react-native';
import { connect } from 'react-redux';
import Dialog from "react-native-dialog";
import stylesAM from './StyleAM';
class PopupSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Title: this.props.Title,
            ListCheckBox: this.props.ListCheckBox
        };
        this.callbackclose = this.props.callbackclose
        this.callbackValue= this.props.callbackValue
    }

    onCheckBox(para, index) {
        var _list = this.state.ListCheckBox;
        _list[index].status = !_list[index].status;
        this.setState({
            ListCheckBox: _list
        });

        // var _dataView=this.state.DataView;
        // if (_list[index].check== true){
        //     _dataView.push(
        //         {
        //             LoginName:_list[index].LoginName,
        //             EmployeeGuid:_list[index].EmployeeGuid
        //         });
        // }else{
        //     for (let i = 0; i < _dataView.length; i++) {
        //         if(_dataView[i].LoginName=== _list[index].LoginName){
        //             _dataView.splice(i,1);
        //         }
        //     }
        // }
        // _countcheck= _dataView.length;
        // this.setState({
        //     Countcheck: _countcheck,
        //     DataView: _dataView,
        // })
    }
    onCheckOne(para, index) {
        var _list = this.state.ListCheckBox;
        for (let i = 0; i < _list.length; i++) {
            if(_list[i]!==_list[index]){
                _list[i].status= false
            }
            else{
                _list[i].status= true
            }
        }
        this.setState({
            ListCheckBox: _list
        });
        this.callbackValue(para.value);
        this.callbackclose()
    }
    onCheckLot(para, index) {
        var _list = this.state.ListCheckBox;
        _list[index].status = !_list[index].status;
        if(_list[index].value== ""){
            if(_list[index].status== true){
                for (let i = 0; i < _list.length; i++) {
                    if(_list[i]!==_list[index]){
                        _list[i].status= false
                    }
                    else{
                        _list[i].status= true
                    }
                }
            }
        } else {
            for (let i = 0; i < _list.length; i++) {
                if (_list[i].value == "") {
                    _list[i].status = false
                }
            }
        }
        this.setState({
            ListCheckBox: _list
        });
    }
    render() {
        return (
            this.props.TypeCheck == "CheckOne" ? this.ViewCheckOne() : this.ViewCheckLot()
        );
    }
    ViewCheckOne = (item) => (
        <View>
            <Dialog.Container visible={true} style={{ height: 10 }}>
                <Dialog.Title style={{ textAlign: 'center', fontSize: 14, fontWeight: 'bold' }}>{this.state.Title}</Dialog.Title>
                <View style={{ flexDirection: 'column', borderTopWidth: 0.5, borderBottomWidth: 0.5, paddingBottom: 5, paddingTop: 5 }}>
                    {this.state.ListCheckBox.map((item, index) =>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                            <View style={{ flex: 1 }} >
                                <CheckBox
                                    value={item.status}
                                    onValueChange={() => this.onCheckOne(item, index)}
                                />
                            </View>
                            <View style={{ flex: 5 }} >
                                <Text style={[stylesAM.TextDetail]}>{item.title}</Text>
                            </View>
                        </View>
                    )}
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <Dialog.Button style={{ fontSize: 12,color: 'red'}} label="Huỷ" onPress={() => this.offPopup()} />
                </View>
            </Dialog.Container>
        </View>
    )
    ViewCheckLot = () => (
        <View>
            <Dialog.Container visible={true} style={{ height: 10 }}>
                <Dialog.Title style={{ textAlign: 'center', fontSize: 14, fontWeight: 'bold' }}>{this.state.Title}</Dialog.Title>
                <View style={{ flexDirection: 'column', borderTopWidth: 0.5, borderBottomWidth: 0.5, paddingBottom: 5, paddingTop: 5 }}>
                    {this.state.ListCheckBox.map((item, index) =>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                            <View style={{ flex: 1 }} >
                                <CheckBox
                                    value={item.status}
                                    onValueChange={() => this.onCheckLot(item, index)}
                                />
                            </View>
                            <View style={{ flex: 5 }} >
                                <Text style={[stylesAM.TextDetail]}>{item.title}</Text>
                            </View>
                        </View>
                    )}
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <Dialog.Button style={{ fontSize: 12, backgroundColor: 'blue', color: '#fff', borderRadius: 5, margin: 5 }} label="Lọc" onPress={() => this.OnSearch()} />
                    <Dialog.Button style={{ fontSize: 12, backgroundColor: '#dc3545', color: '#fff', borderRadius: 5, margin: 5 }} label="Huỷ" onPress={() => this.offPopup()} />
                </View>
            </Dialog.Container>
        </View>
    )
    offPopup() {
        this.callbackclose()
    }
    OnSearch(){
        var _dataValue=[];
        for (let i = 0; i < this.state.ListCheckBox.length; i++) {
            if(this.state.ListCheckBox[i].status== true){
                _dataValue.push(this.state.ListCheckBox[i].value)
            }
        }
        if(_dataValue[0]==""){
            _dataValue=[]
        }
        this.callbackValue(_dataValue);
        this.callbackclose()
    }
}

const mapStateToProps = state => ({})
const mapDispatchToProps = {}
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(PopupSearch);
