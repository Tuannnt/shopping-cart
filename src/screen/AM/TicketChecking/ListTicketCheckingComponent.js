import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, ListItem, SearchBar, Badge} from 'react-native-elements';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_TicketChecking from '../../../network/AM/API_TicketChecking';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import {FuncCommon} from '../../../utils';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '100',
    fontSize: 30,
    color: '#f6b801',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  ClickPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#CCCCCC',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  ClickApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    backgroundColor: '#CCCCCC',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  timeHeader: {
    marginBottom: 3,
  },
});
class ListTicketCheckingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: {},
      list: [],
      isFetching: false,
      ValueSearchDate: '4', // 30 ngày trước
      openSearch: false,
    };
    this.staticParam = {
      CurrentPage: 1,
      Length: 30,
      Search: '',
      QueryOrderBy: 'TicketID',
      StartDate: new Date(),
      EndDate: new Date(),
    };
  }

  GetAll = () => {
    this.setState({refreshing: true});
    API_TicketChecking.ListAll(this.staticParam)
      .then(res => {
        this.state.list =
          res.data.data != 'null'
            ? this.state.list.concat(JSON.parse(res.data.data))
            : [];
        this.setState({list: this.state.list});
        this.setState({refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'BackByTicketChecking') {
      this.updateSearch('');
    }
  }

  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  updateSearch = text => {
    this.staticParam.Search = text;
    this.setState({
      list: [],
    });
    this.GetAll();
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Kiểm kê hàng hóa'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'AM'}
          addForm={'addTicketChecking'}
          CallbackFormAdd={() => Actions.amAddTicketChecking()}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  padding: 5,
                }}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian bắt đầu
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventStartDate: true})}>
                  <Text>
                    {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian kết thúc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventEndDate: true})}>
                  <Text>{FuncCommon.ConDate(this.staticParam.EndDate, 0)}</Text>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', padding: 5}}>
                <Text
                  style={[
                    AppStyles.Labeldefault,
                    {textAlign: 'center'},
                    styles.timeHeader,
                  ]}>
                  Lọc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionSearchDate()}>
                  <Icon
                    name={'down'}
                    type={'antdesign'}
                    size={18}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <FormSearch
              CallbackSearch={callback => this.updateSearch(callback)}
            />
          </View>
        ) : null}
        <View style={{flex: 9}}>
          <FlatList
            style={{height: '100%'}}
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListFooterComponent={this.renderFooter}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={item => item.TicketGuid}
            data={this.state.list}
            renderItem={({item, index}) => (
              <View style={{fontSize: 5, fontFamily: Fonts.base.family}}>
                <ListItem
                  titleStyle={AppStyles.Titledefault}
                  subtitle={() => {
                    return (
                      <View style={{flexDirection: 'row', marginTop: -25}}>
                        <View style={{flex: 3, flexDirection: 'row'}}>
                          <View>
                            <Text
                              style={[
                                AppStyles.Titledefault,
                                {
                                  //color: '#2E77FF',
                                },
                              ]}>
                              {item.TicketId}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              Đến ngày: {this.customDate(2, item.EndDate)}
                            </Text>
                            <Text style={AppStyles.Textdefault}>
                              Mục đích: {item.Title}
                            </Text>
                          </View>
                        </View>
                        <View
                          style={{
                            flex: 1.5,
                            justifyContent: 'flex-start',
                            alignItems: 'flex-end',
                          }}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right', justifyContent: 'center'},
                            ]}>
                            Kho: {item.WarehouseId}
                          </Text>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right', justifyContent: 'center'},
                            ]}>
                            Kết quả: {item.Result}
                          </Text>
                        </View>
                      </View>
                    );
                  }}
                  bottomDivider
                  onPress={() => this.openView(item.TicketGuid, item.TicketId)}
                />
              </View>
            )}
            onEndReachedThreshold={0.5}
            onEndReached={({distanceFromEnd}) => {
              if (this.staticParam.CurrentPage !== 1) {
                this.nextPage();
              }
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
        </View>
        {this.state.setEventStartDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearch(''),
                  this.setState({setEventStartDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.staticParam.StartDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setStartDate(setDate)}
            />
          </View>
        ) : null}
        {this.state.setEventEndDate === true ? (
          <View
            style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              position: 'absolute',
              bottom: 0,
              zIndex: 1000,
              width: SCREEN_WIDTH,
            }}>
            <TouchableOpacity
              style={{
                borderWidth: 0.5,
                borderColor: AppColors.gray,
                padding: 10,
              }}
              onPress={() => {
                this.updateSearch(''), this.setState({setEventEndDate: false});
              }}>
              <Text style={{textAlign: 'right'}}>Xong</Text>
            </TouchableOpacity>
            <DatePicker
              locale="vie"
              date={this.staticParam.EndDate}
              mode="date"
              style={{width: SCREEN_WIDTH}}
              onDateChange={setDate => this.setEndDate(setDate)}
            />
          </View>
        ) : null}
        <MenuSearchDate
          value={this.state.ValueSearchDate}
          callback={this.CallbackSearchDate}
          eOpen={this.openMenuSearchDate}
        />
      </View>
    );
  }
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetAll();
  }
  //Load lại
  onRefresh = () => {
    this.setState({refreshing: true});
    this.updateSearch('');
  };
  onPressHome() {
    Actions.app();
  }
  //định dạng ngày
  customDate(option, strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      var dd = date.getDate();
      var MM = date.getMonth() + 1; //January is 0!
      var yyyy = date.getFullYear();
      var hh = date.getHours();
      var mm = date.getMinutes();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (MM < 10) {
        MM = '0' + MM;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (hh < 10) {
        hh = '0' + hh;
      }
      if (option === 1) {
        date = dd + '/' + MM + '/' + yyyy + ' ' + hh + ':' + mm;
      } else if (option === 2) {
        date = dd + '/' + MM + '/' + yyyy;
      }
      return date.toString();
    } else {
      return '';
    }
  }
  ConvertIntoToDate(item) {
    var dd = item.toString().substring(6, 8);
    var mm = item.toString().substring(4, 6);
    var yyyy = item.toString().substring(0, 4);
    return dd + '/' + mm + '/' + yyyy;
  }
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  openView(guid, id) {
    Actions.amViewTicketChecking({RecordGuid: guid});
  }
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListTicketCheckingComponent);
