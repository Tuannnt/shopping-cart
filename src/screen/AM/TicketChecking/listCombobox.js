export default {
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Mã hàng', width: 200},
    {title: 'Tên hàng', width: 300},
    {title: 'Mã kho', width: 150},
    {title: 'ĐVT', width: 150},
    {title: 'SL Kế toán', width: 150},
    {title: 'SL Kiểm kê', width: 150},
    {title: 'Chênh lệch', width: 150},
    {title: 'GT Kế toán', width: 150},
    {title: 'GT theo kiểm kê', width: 150},
    {title: 'Chênh lệch', width: 150},
    {title: 'Còn tốt 100%', width: 150},
    {title: 'Kém phẩm chất', width: 150},
    {title: 'Mất phẩm chất', width: 150},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  Ration: [
    {
      value: 'A',
      label: 'Xuất ăn 12k',
    },
    {
      value: 'B',
      label: 'Xuất ăn 15k',
    },
    {
      value: 'C',
      label: 'Xuất ăn 28k',
    },
  ],
};
