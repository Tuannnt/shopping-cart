import React, {Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
  Alert,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import API_TicketRequests from '../../../network/AM/API_TicketRequests';
import API_TicketChecking from '../../../network/AM/API_TicketChecking';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, Button, Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import RequiredText from '../../component/RequiredText';
import DatePicker from 'react-native-datepicker';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import {FuncCommon} from '../../../utils';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import Combobox from '../../component/Combobox';
import {LoadingComponent} from '@Component';
import _ from 'lodash';
import listCombobox from './listCombobox';
const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 90,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 90,
  },
});
const headerTable = listCombobox.headerTable;
class AddTicketCheckingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      selectItem: null,
      position: null,
      dWahouse: '',
      dItems: '',
      dUnits: '',
      selectedItems: [],
      tabTTC: true,
      tabCT: false,
      ListWahouse: [],
      ListItems: [],
      ListUnits: [],
      TicketCheckingItems: [],
      TicketId: '',
      Title: '',
      EndDate: new Date(),
      EmployeeName: '',
      DepartmentName: '',
      WarehouseGuid: null,
      Result: '',
      ItemGuid: null,
      ItemId: '',
      ItemName: '',
      OrderNumberId: '',
      UnitBefore: '',
      UnitName: '',
      Quantity: 0,
      QuantityActual: 0,
      UnitPriceBefore: 0,
      Amount: 0,
      SortOrder: 0,
      VoucherGuid: null,
      WarehouseId: null,
      PurchaseOrderId: '',
      editItem: null,
      pageItems: 1,
      pageWahouse: 1,
      length: 20,
      keyWordItems: '',
      keyWordWahouse: '',
      modelauto: {
        Value: null,
        IsEdit: null,
      },
      rows: [],
      items: [],
      TiketDate: new Date(),
    };
    this.state.dataMenu = [
      {
        key: 'delete',
        name: 'Xóa',
        icon: {
          name: 'delete',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
    ];
  }
  GetNumberAuto() {
    API_TicketChecking.GetNumberAuto({AliasId: 'QTDSCT_BBKK'})
      .then(rs => {
        if (
          rs.data.data !== null &&
          rs.data.data !== '' &&
          rs.data.data !== undefined
        ) {
          this.state.modelauto.Value = JSON.parse(rs.data.data).Value;
          this.state.modelauto.IsEdit = JSON.parse(rs.data.data).IsEdit;
          this.setState({
            TicketId: JSON.parse(rs.data.data).Value,
            modelauto: {
              Value: this.state.modelauto.Value,
              IsEdit: this.state.modelauto.IsEdit,
            },
          });
        }
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  componentDidMount() {
    Promise.all([this.GetWahouse()]);
    const {itemData, rowData = []} = this.props;
    if (itemData) {
      this.setState(
        {
          Title: itemData.Title,
          Result: itemData.Result,
          EndDate: new Date(itemData.EndDate),
          WarehouseId: itemData.WarehouseId,
          rows: [...rowData],
          isEdit: false,
          TicketId: itemData.TicketId,
          dWahouse: itemData.WarehouseId,
          modelauto: {
            Value: itemData.TicketId,
            IsEdit: false,
          },
        },
        () => {
          this.GetItemsByWareHouse();
        },
      );
    } else {
      this.GetNumberAuto();
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'QRCode') {
      var link = nextProps.Data.Value; // dữ liệu QR quét được
      this.QRCheck(nextProps.Data.Value);
    }
  }
  GetWahouse() {
    var obj = {
      CurrentPage: this.state.pageWahouse,
      Length: this.state.length,
      Search: this.state.keyWordWahouse,
    };
    API_TicketChecking.GetWahouse(obj)
      .then(rs => {
        var _lstWahouse = JSON.parse(rs.data.data);
        var _reListWahouse = [
          {
            text: 'Chọn đối tượng',
            value: null,
            id: null,
          },
        ];
        for (var i = 0; i < _lstWahouse.length; i++) {
          _reListWahouse.push({
            text: _lstWahouse[i].WarehouseName,
            value: _lstWahouse[i].WarehouseGuid,
            id: _lstWahouse[i].WarehouseId,
          });
        }
        this.setState({
          ListWahouse: _reListWahouse,
          loading: false,
        });
        // this.GetItemsById();
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  nextpageWahouse = callback => {
    this.state.pageWahouse++;
    this.setState({
      pageWahouse: this.state.pageWahouse,
    });
    var obj = {
      CurrentPage: this.state.pageWahouse,
      Length: this.state.length,
      Search: this.state.keyWordWahouse,
    };
    API_TicketChecking.GetWahouse(obj)
      .then(rs => {
        var _lstWahouse = JSON.parse(rs.data.data);
        var _reListWahouse = [];
        for (var i = 0; i < _lstWahouse.length; i++) {
          _reListWahouse.push({
            text:
              _lstWahouse[i].WarehouseId + ' - ' + _lstWahouse[i].WarehouseName,
            value: _lstWahouse[i].WarehouseGuid,
          });
        }
        callback(_reListWahouse);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  searchWahouse = (callback, textsearch) => {
    this.setState({
      pageWahouse: 1,
      keyWordWahouse: textsearch,
    });
    var obj = {
      CurrentPage: this.state.pageWahouse,
      Length: this.state.length,
      Search: this.state.keyWordWahouse,
    };
    API_TicketChecking.GetWahouse(obj)
      .then(rs => {
        var _lstWahouse = JSON.parse(rs.data.data);
        var _reListWahouse = [];
        for (var i = 0; i < _lstWahouse.length; i++) {
          _reListWahouse.push({
            text:
              _lstWahouse[i].WarehouseId + ' - ' + _lstWahouse[i].WarehouseName,
            value: _lstWahouse[i].WarehouseGuid,
          });
        }
        callback(_reListWahouse);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  // GetItemsById = () => {
  //     var obj = {
  //         CurrentPage: this.state.pageItems,
  //         Length: this.state.length,
  //         Search: this.state.keyWordItems
  //     }
  //     API_TicketRequests.GetItemsById(obj).then(rs => {
  //         var _lstItems = JSON.parse(rs.data.data);
  //         var _reListItems = [];
  //         for (var i = 0; i < _lstItems.length; i++) {
  //             _reListItems.push({
  //                 text: _lstItems[i].ItemId + ' - ' + _lstItems[i].ItemName,
  //                 value: _lstItems[i].ItemGuid,
  //                 ItemName: _lstItems[i].ItemName,
  //                 UnitId: _lstItems[i].UnitId,
  //                 UnitName: _lstItems[i].UnitName,
  //             });
  //         }
  //         this.setState(
  //             {
  //                 ListItems: _reListItems,
  //             });
  //         if (this.props.Id !== undefined) {
  //             var data = [{ data: this.props.Id }];
  //             this.QRCheck(data);
  //         }
  //         this.GetUnitsAllJExcel();
  //     }).catch(error => {
  //         console.log(error.data.data);
  //     });
  // }
  nextpageItem = callback => {
    this.state.pageItems++;
    this.setState({
      pageItems: this.state.pageItems,
    });
    var obj = {
      CurrentPage: this.state.pageItems,
      Length: this.state.length,
      Search: this.state.keyWordItems,
    };
    API_TicketRequests.GetItemsById(obj)
      .then(rs => {
        var _lstItems = JSON.parse(rs.data.data);
        var _reListItems = [];
        for (var i = 0; i < _lstItems.length; i++) {
          _reListItems.push({
            text: _lstItems[i].ItemId + ' - ' + _lstItems[i].ItemName,
            value: _lstItems[i].ItemGuid,
            ItemName: _lstItems[i].ItemName,
            UnitId: _lstItems[i].UnitId,
            UnitName: _lstItems[i].UnitName,
          });
        }
        callback(_reListItems);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  searchItem = (callback, textsearch) => {
    this.setState({
      pageItems: 1,
      keyWordItems: textsearch,
    });
    var obj = {
      CurrentPage: this.state.pageItems,
      Length: this.state.length,
      Search: this.state.keyWordItems,
    };
    API_TicketRequests.GetItemsById(obj)
      .then(rs => {
        var _lstItems = JSON.parse(rs.data.data);
        var _reListItems = [];
        for (var i = 0; i < _lstItems.length; i++) {
          _reListItems.push({
            text: _lstItems[i].ItemId + ' - ' + _lstItems[i].ItemName,
            value: _lstItems[i].ItemGuid,
            ItemName: _lstItems[i].ItemName,
            UnitId: _lstItems[i].UnitId,
            UnitName: _lstItems[i].UnitName,
          });
        }
        callback(_reListItems);
      })
      .catch(error => {
        console.log(error.data.data);
      });
  };
  GetUnitsAllJExcel() {
    API_TicketRequests.GetUnitsAllJExcel('A')
      .then(rs => {
        var _lstUnits = JSON.parse(rs.data.data);
        var _reListUnits = [];
        for (var i = 0; i < _lstUnits.length; i++) {
          _reListUnits.push({
            text: _lstUnits[i].UnitName,
            value: _lstUnits[i].UnitId,
            UnitGuid: _lstUnits[i].UnitGuid,
          });
        }
        this.setState({
          ListUnits: _reListUnits,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  CustomTTC = () => (
    <View style={{flexDirection: 'column', flex: 1}}>
      {/* Số phiếu */}
      <View style={{flexDirection: 'row'}}>
        <View style={{padding: 10, flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={AppStyles.Labeldefault}>Số kiểm kê</Text>
            <Text style={{color: 'red'}}> *</Text>
          </View>
          <TextInput
            style={AppStyles.FormInput}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            multiline={true}
            value={this.state.TicketId}
            editable={this.state.modelauto.IsEdit}
          />
        </View>
        <View style={{padding: 10, flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={AppStyles.Labeldefault}>Đến ngày</Text>
            <Text style={{color: 'red'}}> *</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <TextInput
              style={[AppStyles.FormInput, {flex: 4}]}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              multiline={true}
              value={FuncCommon.ConDate(this.state.EndDate, 0)}
              editable={false}
            />
            <View style={{flex: 1}}>
              <DatePicker
                locale="vie"
                style={{width: 150}}
                date={this.state.EndDate}
                androidMode="spinner"
                mode="date"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 36,
                  },
                }}
                hideText={true}
                onDateChange={date => {
                  this.setState({EndDate: date});
                }}
              />
            </View>
          </View>
        </View>
      </View>
      {/* Ngày yêu cầu */}
      {/* mục đích */}
      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Mục đích</Text>
          <Text style={{color: 'red'}}> *</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.state.Title}
          onChangeText={text => this.setState({Title: text})}
        />
      </View>
      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>
            Kiểm kê kho <RequiredText />
          </Text>
        </View>
        <TouchableOpacity
          style={[AppStyles.FormInput, {flexDirection: 'row'}]}
          onPress={() => this.onActionComboboxWahouse()}>
          <Text
            style={[
              AppStyles.TextInput,
              this.state.dWahouse == ''
                ? {color: AppColors.gray, flex: 3}
                : {flex: 3},
            ]}>
            {this.state.dWahouse !== ''
              ? this.state.dWahouse
              : 'Chọn đối tượng...'}
          </Text>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <Icon
              color="#888888"
              name={'chevron-thin-down'}
              type="entypo"
              size={20}
            />
          </View>
        </TouchableOpacity>
      </View>
      {/* diễn giải */}
      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={AppStyles.Labeldefault}>Kết quả xử lý</Text>
        </View>
        <TextInput
          style={AppStyles.FormInput}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline={true}
          value={this.state.Result}
          onChangeText={text => this.setState({Result: text})}
        />
      </View>
    </View>
  );
  CustomCT = () => (
    <View>
      <View style={{marginTop: 15}}>
        <ScrollView
          horizontal={true}
          style={{paddingBottom: 25, marginBottom: 20}}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              {headerTable.map(item => (
                <TouchableOpacity
                  key={item.title}
                  style={[AppStyles.table_th, {width: item.width}]}>
                  <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                </TouchableOpacity>
              ))}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <FlatList
                  data={this.state.rows}
                  renderItem={({item, index}) => {
                    return this.itemFlatList(item, index);
                  }}
                  keyExtractor={(rs, index) => index.toString()}
                />
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#ffffff'} />
    ) : (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1, backgroundColor: '#ffffff'}}>
        <ScrollView
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={[AppStyles.container, {flex: 1}]}>
            {this.props.itemData ? (
              <TabBar_Title
                title={'Sửa phiếu kiểm kê'}
                callBack={() => this.callBackList({})}
                FormQR={true}
                QRMultiple={true}
              />
            ) : (
              <TabBar_Title
                title={'Thêm phiếu kiểm kê'}
                callBack={() => this.callBackList({})}
                FormQR={true}
                QRMultiple={true}
              />
            )}

            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderRadius: 10,
                borderWidth: 0.5,
                flexDirection: 'row',
                height: 40,
              }}>
              <TouchableOpacity
                onPress={() => this.OnChangeTab('1')}
                style={styles.TabarPending}>
                <Text
                  style={
                    this.state.tabTTC === true
                      ? {color: AppColors.Maincolor}
                      : {color: 'black'}
                  }>
                  Thông tin chung
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.OnChangeTab('2')}
                style={styles.TabarApprove}>
                <Text
                  style={
                    this.state.tabCT === true
                      ? {color: AppColors.Maincolor}
                      : {color: 'black'}
                  }>
                  Chi tiết
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[AppStyles.container, {flex: 1}]}>
              {/* hiển thị thông tin chung */}
              {this.state.tabTTC === true
                ? this.CustomTTC()
                : this.state.tabCT === true
                ? this.CustomCT()
                : null}
            </View>
            {/* Submit*/}
          </View>
        </ScrollView>
        <Button
          buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
          title="Lưu"
          onPress={() => this.Submit()}
        />
        <MenuActionCompoment
          callback={this.actionMenuCallback}
          data={this.state.dataMenu}
          name={this.state.NameMenu}
          eOpen={this.openMenu}
          position={'bottom'}
        />
        {this.state.ListWahouse.length > 0 ? (
          <Combobox
            value={null}
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeWahouse}
            data={this.state.ListWahouse}
            nameMenu={'Chọn đối tượng'}
            eOpen={this.openComboboxWahouse}
            position={'bottom'}
          />
        ) : null}
        {this.state.ListItems.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeItems}
            data={this.state.ListItems}
            nameMenu={'Chọn sản phẩm'}
            eOpen={this.openComboboxItems}
            position={'bottom'}
            paging={true}
            callback_Paging={callBack => this.nextpageItem(callBack)}
            callback_SearchPaging={(callback, textsearch) =>
              this.searchItem(callback, textsearch)
            }
          />
        ) : null}
        {this.state.ListUnits.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            //value={this.state.UnitBefore}
            callback={this.ChangeUnit}
            data={this.state.ListUnits}
            nameMenu={'Chọn đơn vị tính'}
            eOpen={this.openComboboxUnits}
            position={'bottom'}
          />
        ) : null}
      </KeyboardAvoidingView>
    );
  }
  //   addRows = () => {
  //     const {rows} = this.state;
  //     let data = _.cloneDeep(rows);
  //     if (data.length === 0) {
  //       data = [];
  //     } else {
  //       data.push();
  //     }
  //     this.setState({rows: data});
  //   };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);
    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  itemFlatList = (row, index) => {
    let DiffQuantity = 0;
    let DifflValue = 0;
    let GoodOfQuantity = 0;
    if (row.BookQuantity && row.ActualQuantity) {
      DiffQuantity = +row.BookQuantity - +row.ActualQuantity;
    }
    if (row.BookValue && row.ActualValue) {
      DifflValue = +row.BookValue - +row.ActualValue;
    }
    GoodOfQuantity =
      +row.ActualQuantity -
      +(row.LowOfQuantity || 0) -
      +(row.LossOfQuantity || 0);
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.ItemId}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 300}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.ItemName}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.WarehouseId}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.UnitId}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.BookQuantity}
          </Text>
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              {color: +row.ActualQuantity < 0 ? 'red' : 'black'},
            ]}
            underlineColorAndroid="transparent"
            value={row.ActualQuantity}
            autoCapitalize="none"
            keyboardType="numeric"
            defaultValue={'0'}
            onChangeText={ActualQuantity => {
              this.handleChangeRows(
                ActualQuantity + '',
                index,
                'ActualQuantity',
              );
            }}
          />
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {marginLeft: 10, color: +DiffQuantity < 0 ? 'red' : 'black'},
            ]}>
            {DiffQuantity}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                marginLeft: 10,
                color: +(row.BookValue || 0) < 0 ? 'red' : 'black',
              },
            ]}>
            {row.BookValue}
          </Text>
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              {color: +row.ActualValue < 0 ? 'red' : 'black'},
            ]}
            underlineColorAndroid="transparent"
            value={row.ActualValue}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={ActualValue => {
              this.handleChangeRows(ActualValue + '', index, 'ActualValue');
            }}
          />
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {marginLeft: 10, color: +DifflValue < 0 ? 'red' : 'black'},
            ]}>
            {DifflValue}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {marginLeft: 10, color: +GoodOfQuantity < 0 ? 'red' : 'black'},
            ]}>
            {GoodOfQuantity}
          </Text>
        </View>

        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              {color: +row.LowOfQuantity < 0 ? 'red' : 'black'},
            ]}
            underlineColorAndroid="transparent"
            value={row.LowOfQuantity}
            autoCapitalize="none"
            defaultValue={'0'}
            keyboardType="numeric"
            onChangeText={LowOfQuantity => {
              this.handleChangeRows(LowOfQuantity + '', index, 'LowOfQuantity');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[
              AppStyles.FormInput_custom,
              {color: +row.LossOfQuantity < 0 ? 'red' : 'black'},
            ]}
            underlineColorAndroid="transparent"
            value={row.LossOfQuantity}
            autoCapitalize="none"
            defaultValue={'0'}
            keyboardType="numeric"
            onChangeText={LossOfQuantity => {
              this.handleChangeRows(
                LossOfQuantity + '',
                index,
                'LossOfQuantity',
              );
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td, {width: 80}]}>
          <Icon
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  ChangeWahouse = value => {
    if (!value.value) {
      return;
    }
    var rs = this.state.ListWahouse.find(x => x.value === value.value);
    if (rs !== null && rs !== undefined) {
      this.setState(
        {
          WarehouseId: rs.id,
          dWahouse: value.text,
          WarehouseGuid: value.value,
          loading: true,
          rows: [],
        },
        () => {
          this.GetItemsByWareHouse();
        },
      );
    }
  };
  _openComboboxWahouse() {}
  openComboboxWahouse = d => {
    this._openComboboxWahouse = d;
  };
  onActionComboboxWahouse() {
    this._openComboboxWahouse();
  }
  GetItemsByWareHouse = () => {
    let {WarehouseId, EndDate} = this.state;
    API_TicketChecking.GetItemsByWareHouse(
      WarehouseId,
      FuncCommon.ConDate(EndDate, 77),
    )
      .then(res => {
        var rs = JSON.parse(res.data.data);
        this.setState({items: rs, loading: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({loading: false});
      });
  };
  QRCheck = data => {
    let {rows, items} = this.state;
    if (!data || data.length === 0) {
      return;
    }
    data.map(item => {
      if (!item.data) {
        return;
      }
      let itemInList = items.find(i => i.ItemId === item.data);
      // tim item trong listItem api tra ve
      if (!itemInList) {
        return;
      }
      let itemInRowsIndex = rows.findIndex(i => i.ItemId === item.data);
      // tim item trong table kiem ke
      if (itemInRowsIndex === -1) {
        rows.push({
          ...itemInList,
          ItemId: itemInList.ItemId,
          ItemName: itemInList.ItemName,
          WarehouseId: itemInList.WarehouseId,
          UnitId: itemInList.UnitId,
          BookQuantity: itemInList.ImportQuantity || '0',
          BookValue: itemInList.PurchasePrice || '0',
          ActualQuantity: '1',
        });
        return;
        // khong thay item trong table thi them moi
      }
      rows[itemInRowsIndex].ActualQuantity = (
        +rows[itemInRowsIndex].ActualQuantity + 1
      ).toString();
      // thay item trong table thi tang so luong kiem ke +1
    });
    this.setState({rows});
  };
  ChangeItems = value => {
    if (value.value !== null) {
      var obj = this.state.TicketCheckingItems[this.state.position];
      obj.ItemGuid = value.value;
      this.setState({
        dItems: value.text,
      });
      API_TicketChecking.GetInfoItem({
        ItemId: value.text.split(' -')[0],
        EndDate: new Date(),
      })
        .then(rs => {
          var rs = JSON.parse(rs.data.data);
          if (rs !== null && rs !== undefined) {
            obj.ItemId = rs.ItemId;
            obj.ItemName = rs.ItemName;
            obj.UnitGuid = rs.UnitGuid;
            obj.UnitId = rs.UnitId;
            obj.UnitName = rs.UnitName;
            obj.WarehouseGuid = rs.WarehouseGuid;
            obj.WarehouseId = rs.WarehouseId;
            obj.BookQuantity = rs.ImportQuantity;
            obj.BookValue = rs.PurchasePrice;
            obj.ActualQuantity = 0;
            obj.ActualValue = 0;
            obj.LowOfQuantity = 0;
            obj.LossOfQuantity = 0;
            obj.GoodOfQuantity = 0;
            this.setState({
              TicketCheckingItems: this.state.TicketCheckingItems,
            });
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  };
  _openComboboxItems() {}
  openComboboxItems = d => {
    this._openComboboxItems = d;
  };
  onActionComboboxItems(index) {
    this.setState({
      position: index,
    });
    this._openComboboxItems();
  }

  ChangeUnit = value => {
    if (value.value !== null) {
      var obj = this.state.TicketCheckingItems[this.state.position];
      var rs = this.state.ListUnits.find(x => x.value === value.value);
      if (rs !== null && rs !== undefined) {
        obj.UnitId = value.value;
        obj.UnitName = rs.text;
        obj.UnitGuid = rs.UnitGuid;
        this.setState({
          TicketCheckingItems: this.state.TicketCheckingItems,
        });
      }
    }
  };
  _openComboboxUnits() {}
  openComboboxUnits = d => {
    this._openComboboxUnits = d;
  };
  onActionComboboxUnits(index) {
    this.setState({
      position: index,
    });
    this._openComboboxUnits();
  }
  setBookQuantity(value, index) {
    var obj = this.state.TicketCheckingItems[index];
    obj.BookQuantity = value !== '' && value !== null ? parseFloat(value) : 0;
    obj.DiffQuantity =
      parseFloat(obj.BookQuantity) - parseFloat(obj.ActualQuantity);
    this.setState((TicketCheckingItems = this.state.TicketCheckingItems));
  }
  setActualQuantity(value, index) {
    var obj = this.state.TicketCheckingItems[index];
    obj.ActualQuantity = value !== '' && value !== null ? parseFloat(value) : 0;
    obj.DiffQuantity =
      parseFloat(obj.BookQuantity) - parseFloat(obj.ActualQuantity);
    obj.GoodOfQuantity =
      parseFloat(obj.ActualQuantity) -
      parseFloat(obj.LowOfQuantity) -
      parseFloat(obj.LossOfQuantity);
    this.setState((TicketCheckingItems = this.state.TicketCheckingItems));
  }
  setBookValue(value, index) {
    var obj = this.state.TicketCheckingItems[index];
    obj.BookValue = value !== '' && value !== null ? parseFloat(value) : 0;
    obj.DifflValue = parseFloat(obj.BookValue) - parseFloat(obj.ActualValue);
    this.setState((TicketCheckingItems = this.state.TicketCheckingItems));
  }
  setActualValue(value, index) {
    var obj = this.state.TicketCheckingItems[index];
    obj.ActualValue = value !== '' && value !== null ? parseFloat(value) : 0;
    obj.DifflValue = parseFloat(obj.BookValue) - parseFloat(obj.ActualValue);
    this.setState((TicketCheckingItems = this.state.TicketCheckingItems));
  }
  setLowOfQuantity(value, index) {
    var obj = this.state.TicketCheckingItems[index];
    obj.LowOfQuantity = value !== '' && value !== null ? parseFloat(value) : 0;
    obj.GoodOfQuantity =
      parseFloat(obj.ActualQuantity) -
      parseFloat(obj.LowOfQuantity) -
      parseFloat(obj.LossOfQuantity);
    this.setState((TicketCheckingItems = this.state.TicketCheckingItems));
  }
  setLossOfQuantity(value, index) {
    var obj = this.state.TicketCheckingItems[index];
    obj.LossOfQuantity = value !== '' && value !== null ? parseFloat(value) : 0;
    obj.GoodOfQuantity =
      parseFloat(obj.ActualQuantity) -
      parseFloat(obj.LowOfQuantity) -
      parseFloat(obj.LossOfQuantity);
    this.setState((TicketCheckingItems = this.state.TicketCheckingItems));
  }
  Submit = () => {
    let {rows} = this.state;
    if (!this.state.Title) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa điền mục đích',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (!this.state.WarehouseId) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa chọn kho kiểm kê',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    if (!rows || rows.length === 0) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa thêm chi tiết phiếu kiểm kê',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    rows = rows.map(row => {
      if (row.BookQuantity && row.ActualQuantity) {
        row.DiffQuantity = (+row.BookQuantity - +row.ActualQuantity).toString();
      }
      if (row.BookValue && row.ActualValue) {
        row.DifflValue = (+row.BookValue - +row.ActualValue).toString();
      }
      row.GoodOfQuantity = (
        +row.ActualQuantity -
        +(row.LowOfQuantity || 0) -
        +(row.LossOfQuantity || 0)
      ).toString();
      return row;
    });
    var model = {
      TicketCheckingItems: rows,
      TicketId: this.state.TicketId,
      Title: this.state.Title,
      EndDate: this.state.EndDate,
      WarehouseGuid: this.state.WarehouseGuid,
      WarehouseID: this.state.WarehouseId,
      Result: this.state.Result,
      TiketDate: this.ConvertDateToInt(this.state.TiketDate),
      TiketTime:
        new Date().getHours().toString() +
        ':' +
        new Date().getMinutes().toString() +
        ':' +
        new Date().getSeconds().toString(),
    };
    if (this.props.itemData) {
      this.Update(model);
      return;
    }
    API_TicketChecking.Insert(model)
      .then(res => {
        console.log(res);
        if (res.data.errorCode === 200) {
          Alert.alert(
            'Thông báo',
            'Thêm mới thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.callBackList(JSON.parse(res.data.data));
        } else {
          console.log(res);
          Alert.alert(
            'Thông báo',
            'Có lỗi xảy ra',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  Update = obj => {
    let _obj = {
      ...this.props.itemData,
      ...obj,
      TiketDate: this.props.itemData.TiketDate,
      WarehouseGuid: this.state.ListWahouse.find(
        x => x.id === this.state.WarehouseId,
      ).value,
    };
    API_TicketChecking.Update(_obj)
      .then(rs => {
        var _rs = rs.data;
        if (_rs.errorCode !== 200) {
          console.log(_rs);
          Alert.alert(
            'Thông báo',
            'Có lỗi xảy ra',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Thông báo',
            'Chỉnh sửa thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.callBackList({});
        }
      })
      .catch(error => {
        console.log('Error when call API update Mobile.');
      });
  };
  //#region hàm Quay lại
  callBackList(rs) {
    this.setState({loading: false});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({
      moduleId: 'BackByTicketChecking',
      Data: {
        Module: 'AM_TicketChecking',
        Subject: this.state.Title,
        StartDate:
          this.state.EndDate === '' || this.state.EndDate === null
            ? null
            : FuncCommon.ConDate(this.state.EndDate, 0),
        EndDate:
          this.state.EndDate === '' || this.state.EndDate === null
            ? null
            : FuncCommon.ConDate(this.state.EndDate, 0),
        RowGuid: rs,
        AssignTo: [],
      },
      ActionTime: new Date().getTime(),
    });
  }
  //endregion
  OnChangeTab(data) {
    if (data == '1') {
      this.setState({
        tabCT: false,
        tabTTC: true,
      });
    } else if (data == '2') {
      this.setState({
        tabTTC: false,
        tabCT: true,
      });
    }
  }
  //Menu Item
  actionMenuCallback = d => {
    if (d === 'delete') {
      var list = this.state.TicketCheckingItems;
      list.splice(this.state.position, 1);
      this.setState({
        TicketCheckingItems: list,
      });
    }
    console.log('Event: ', d);
  };
  _openMenu() {}
  openMenu = d => {
    this._openMenu = d;
  };
  onAction(item, index) {
    this.setState({
      selectItem: item,
      position: index,
    });
    var _option = {
      isChangeAction: true,
      data: this.state.dataMenu,
      NameMenu: 'Chức năng',
    };
    this._openMenu(_option);
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  ConvertDateToInt(datetime) {
    if (datetime !== null && datetime !== undefined) {
      var Str = datetime.toString();
      if (Str.indexOf('/Date') >= 0) {
        var newdate = new Date(parseInt(datetime.substr(6)));
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;
        return year.toString() + month.toString() + day.toString();
      } else {
        if (datetime.toString().split('/').length > 1) {
          var value = datetime.split('/');
          return (
            value[2].toString() + value[1].toString() + value[0].toString()
          );
        } else {
          var month = datetime.getMonth() + 1;
          var day = datetime.getDate();
          var year = datetime.getFullYear();
          if (month < 10) month = '0' + month;
          if (day < 10) day = '0' + day;
          return year.toString() + month.toString() + day.toString();
        }
      }
    }
    return null;
  }
}
const mapStateToProps = state => ({
  applications: state.user.data,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(AddTicketCheckingComponent);
