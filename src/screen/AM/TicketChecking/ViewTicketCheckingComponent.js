import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import API_TicketChecking from '../../../network/AM/API_TicketChecking';
import Dialog from 'react-native-dialog';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottom from '../../component/TabBarBottom';
import LoadingComponent from '../../component/LoadingComponent';
import listCombobox from './listCombobox';
import {ToastAndroid} from 'react-native';
import Toast from 'react-native-simple-toast';

const styles = StyleSheet.create({
  hidden: {display: 'none'},
});
const headerTable = listCombobox.headerTable;
class ViewTicketCheckingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TicketGuid: props.RecordGuid,
      TicketId: null,
      model: {
        TicketRequestNo: '',
      },
      List: [],
      isFetching: false,
      isHidden: false,
      check: false,
      Title: 'Thông báo',
      desciption: 'Bạn có muốn xác nhận thông tin',
      text: '3463',
      loading: true,
      rows: [],
    };
    this.listtabbarBotom = [
      {
        Title: 'Xác nhận thông tin',
        Icon: 'checksquareo',
        Type: 'antdesign',
        Value: 'checksquareo',
        Checkbox: false,
      },
    ];
  }

  componentDidMount = () => {
    this.GetItem();
  };
  //Lấy thông tin đề nghị nhập
  GetItem = () => {
    API_TicketChecking.GetItem({TicketGuid: this.props.RecordGuid})
      .then(res => {
        this.setState({
          model: JSON.parse(res.data.data),
          rows: JSON.parse(res.data.data).TicketCheckingItems,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log(error);
      });
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <TabBar_Title
          title={'Chi tiết kiểm kê kho'}
          callBack={() => this.onPressBack()}
        />
        <ScrollView style={{paddingBottom: 30}}>
          <View style={{flexDirection: 'row', padding: 10}}>
            <View style={{flex: 10}}>
              <Text style={AppStyles.Titledefault}>Thông tin chung</Text>
            </View>
            <View style={{flex: 1}}>
              <Icon
                color="#888888"
                name={'chevron-thin-down'}
                type="entypo"
                size={20}
                onPress={() =>
                  this.state.isHidden == false
                    ? this.setState({isHidden: true})
                    : this.setState({isHidden: false})
                }
              />
            </View>
          </View>
          <View
            style={[
              this.state.isHidden == true ? styles.hidden : '',
              {padding: 10, backgroundColor: '#fff', flexDirection: 'row'},
            ]}>
            <View style={{flex: 1}}>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Số kiểm kê :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Ngày kiểm kê :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Đến ngày :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Mục đích :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Kiểm kê kho :
              </Text>
              <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                Kết quả xử lý :
              </Text>
            </View>
            <View style={{flex: 2, alignItems: 'flex-end'}}>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.state.model.TicketId}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.ConvertIntoToDate(this.state.model.TiketDate)}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.customDate(2, this.state.model.EndDate)}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.state.model.Title}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.state.model.WarehouseId}
              </Text>
              <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                {this.state.model.Result}
              </Text>
            </View>
          </View>
          <Divider style={{marginBottom: 10}} />
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <Text style={[AppStyles.Titledefault, {flex: 1, paddingLeft: 10}]}>
              Chi tiết
            </Text>
          </View>
          <View style={{flex: 6}}>
            <ScrollView
              horizontal={true}
              style={{paddingBottom: 25, marginBottom: 20}}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  {headerTable.map(item => {
                    if (item.hideInDetail) {
                      return null;
                    }
                    return (
                      <TouchableOpacity
                        key={item.title}
                        style={[AppStyles.table_th, {width: item.width}]}>
                        <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>
                <View>
                  {this.state.rows.length > 0 ? (
                    <FlatList
                      data={this.state.rows}
                      renderItem={({item, index}) => {
                        return this.itemFlatList(item, index);
                      }}
                      keyExtractor={(rs, index) => index.toString()}
                    />
                  ) : (
                    <TouchableOpacity style={[AppStyles.table_foot]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                        Không có dữ liệu!
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ScrollView>
          </View>
        </ScrollView>
        {/* <View style={AppStyles.StyleTabvarBottom}>
                    <TabBarBottomCustom
                        ListData={this.listtabbarBotom}
                        onCallbackValueBottom={(callback) => this.onConfim()}
                    />
                </View> */}
        <View style={{maxHeight: 50}}>
          {this.state.checkInLogin !== '' ? (
            <TabBarBottom
              //key để quay trở lại danh sách
              onDelete={() => this.Delete()}
              isDraff={true}
              callbackOpenUpdate={this.callbackOpenUpdate}
            />
          ) : null}
        </View>
      </View>
    );
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'BackByTicketChecking') {
      this.GetItem();
    }
  }
  callBackList() {
    Actions.pop();
    Actions.refresh({
      moduleId: 'BackByTicketChecking',
      ActionTime: new Date().getTime(),
    });
  }
  Delete = () => {
    let id = {Id: this.props.RecordGuid};
    this.setState({loading: true});
    API_TicketChecking.Delete(id.Id)
      .then(response => {
        this.setState({
          loading: false,
        });
        if (response.data.errorCode == 200) {
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  callbackOpenUpdate = () => {
    Actions.amAddTicketChecking({
      itemData: this.state.model,
      rowData: this.state.rows,
    });
  };
  onPressBack() {
    Actions.pop();
  }
  //Xác nhận thông tin
  onConfim() {
    this.setState({
      check: true,
    });
  }
  onClose() {
    this.setState({
      check: false,
    });
  }
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10, padding: 10}]}>
            {row.ItemId}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 300}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.ItemName}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.WarehouseId}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.UnitId}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.BookQuantity}
          </Text>
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.ActualQuantity}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {marginLeft: 10, color: +row.DiffQuantity < 0 ? 'red' : 'black'},
            ]}>
            {row.DiffQuantity}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                marginLeft: 10,
                color: +(row.BookValue || 0) < 0 ? 'red' : 'black',
              },
            ]}>
            {row.BookValue}
          </Text>
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.ActualValue}
          </Text>
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {marginLeft: 10, color: +row.DifflValue < 0 ? 'red' : 'black'},
            ]}>
            {row.DifflValue}
          </Text>
        </View>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text
            style={[
              AppStyles.TextInput,
              {
                marginLeft: 10,
                color: +row.GoodOfQuantity < 0 ? 'red' : 'black',
              },
            ]}>
            {row.GoodOfQuantity}
          </Text>
        </View>

        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.LowOfQuantity}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <Text style={[AppStyles.TextInput, {marginLeft: 10}]}>
            {row.LossOfQuantity}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  ConvertIntoToDate(item) {
    if (item !== null && item !== undefined) {
      var dd = item.toString().substring(6, 8);
      var mm = item.toString().substring(4, 6);
      var yyyy = item.toString().substring(0, 4);
      return dd + '/' + mm + '/' + yyyy;
    }
  }
  //định dạng ngày
  customDate(option, strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      var dd = date.getDate();
      var MM = date.getMonth() + 1; //January is 0!
      var yyyy = date.getFullYear();
      var hh = date.getHours();
      var mm = date.getMinutes();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (MM < 10) {
        MM = '0' + MM;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (hh < 10) {
        hh = '0' + hh;
      }
      if (option === 1) {
        date = dd + '/' + MM + '/' + yyyy + ' ' + hh + ':' + mm;
      } else if (option === 2) {
        date = dd + '/' + MM + '/' + yyyy;
      }
      return date.toString();
    } else {
      return '';
    }
  }
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ViewTicketCheckingComponent);
