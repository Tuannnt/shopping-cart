import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    ScrollView,
    View,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { AppStyles, AppColors } from '@theme';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
}
class SearchDay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListDay: [],
            ChoiceDate: '',
            SearchToday: null,
            dd: null
        };
        this.CallBackValue = this.props.CallBackValue
        this.CallBackOffSearch = this.props.CallBackOffSearch
    }

    componentDidMount() {
        this.DateOfSearch(new Date().getMonth() + 1, new Date().getFullYear(), false, "newMonth", null);
    }
    DateOfSearch(m, y, status, newMonth, scroll) {
        var _listday = status == true ? [] : this.state.ListDay;
        var count = 0;
        var _nday = new Date(y, m, 0).getDate();
        var _today = (new Date()).getDate();
        if (newMonth == 'newMonth') {
            if (this.state.ListDay.length > 0) {
                count = this.state.ListDay[this.state.ListDay.length - 1].orderId;
            }
            for (let i = 1; i <= _nday; i++) {
                var _m = m < 10 ? '0' + m : m
                var _i = i < 10 ? '0' + i : i
                _listday.push({
                    Key: '' + y + '' + _m + '' + _i + '',
                    dd: _i,
                    MM: m,
                    yyyy: y,
                    date: new Date(y, m - 1, i),
                    Dayofweek: ((new Date(y, m - 1, i).getDay() == 0) ? 'CN' : 'T' + (new Date(y, m - 1, i).getDay() + 1)),
                    CheckBox: false,
                    Today: (count + i == _today) ? true : false,
                    orderId: count + i
                })
            }
        } else {
            if (this.state.ListDay.length > 0) {
                count = this.state.ListDay[0].orderId;
            }
            var _listdayV2 = [];
            for (let i = 1; i <= _nday; i++) {
                var _m = m < 10 ? '0' + m : m
                var _i = i < 10 ? '0' + i : i
                _listdayV2.unshift({
                    Key: '' + y + '' + _m + '' + _i + '',
                    dd: _i,
                    MM: m,
                    yyyy: y,
                    date: new Date(y, m - 1, i),
                    Dayofweek: ((new Date(y, m - 1, i).getDay() == 0) ? 'CN' : 'T' + (new Date(y, m - 1, i).getDay() + 1)),
                    CheckBox: false
                })
            }
            for (let i = 0; i <= _listdayV2.length - 1; i++) {
                _listday.unshift({
                    Key: _listdayV2[i].Key,
                    dd: _listdayV2[i].dd,
                    MM: _listdayV2[i].MM,
                    yyyy: _listdayV2[i].yyyy,
                    date: _listdayV2[i].date,
                    Dayofweek: _listdayV2[i].Dayofweek,
                    CheckBox: _listdayV2[i].CheckBox,
                    Today: (count - i - 1 == _today) ? true : false,
                    orderId: count - i - 1
                })
            }
        }
        this.setState({ ListDay: _listday })
        if (scroll == null) {
            var obj = _listday.find(x => x.orderId == _today)
            setTimeout(() => {
                this.scrollToView(obj)
            }, 100);
        } else {
            setTimeout(() => {
                this.myScroll.scrollTo({ x: ((_nday + 1) * 50 - 50) });
            }, 100);
        }
    }
    onchoiceDate = (item) => {
        if (typeof (item) === "undefined") {
            var y = (new Date()).getFullYear();
            var m = (new Date()).getMonth() + 1;
            var i = (new Date()).getDate();
            var _today = (new Date()).getDate();
            var _m = m < 10 ? '0' + m : m;
            var _i = i < 10 ? '0' + i : i;
            item = {
                Key: '' + y + '' + _m + '' + _i + '',
                dd: _i,
                MM: m,
                yyyy: y,
                date: (new Date(y, m - 1, i)),
                Dayofweek: (((new Date(y, m - 1, i)).getDay() == 0) ? 'CN' : 'T' + ((new Date(y, m - 1, i)).getDay() + 1)),
                CheckBox: false,
                Today: (i == _today) ? true : false,
                orderId: i
            };
        }
        var _listday = this.state.ListDay;
        var _listdayView = [];
        for (let i = 0; i < _listday.length; i++) {
            if (_listday[i].Key == item.Key) {
                _listday[i].CheckBox = true
            } else {
                _listday[i].CheckBox = false
            }
            _listdayView.push(_listday[i])
        }
        this.setState({ ListDay: _listdayView })
        var _item = item.Key.toString();
        if (_item != null && _item != undefined && _item != "") {
            var yyyy = _item.substring(0, 4);
            var MM = _item.substring(4, 6)
            if (MM.substring(0, 1) == 0) {
                MM = MM.substring(1, 2)
            };
            var dd = _item.substring(6, 8);
            if (dd.substring(0, 1) == 0) {
                dd = dd.substring(1, 2)
            };
            var _Date = dd + " tháng " + MM + " năm " + yyyy;
            var _count = 0
            for (let z = 0; z < this.state.ListDay.length; z++) {
                if (this.state.ListDay[z].orderId <= item.orderId) {
                    _count = _count + 1
                }
            }
            this.state.dd = _count;
            this.setState({
                ChoiceDate: _Date,
                SearchToday: item,
            })

            this.CallBackValue(item.Key)
        } else {
            return ""
        }
    }
    scrollToView(item) {
        this.onchoiceDate(item);
        this.myScroll.scrollTo({ x: ((this.state.dd) * 50 - (DRIVER.width / 2) -25) });
    }
    LoadDateEnd(event) {
        var x = event.nativeEvent.contentOffset.x
        var EndDateView = ((this.state.ListDay.length * 50) - DRIVER.width);
        if (x == EndDateView) {
            var obj = this.state.ListDay.find(x => x.Key == this.state.ListDay[this.state.ListDay.length - 1].Key)
            var ddnew = new Date(new Date(obj.date).setDate(new Date(obj.date).getDate() + 1))
            this.DateOfSearch(new Date(ddnew).getMonth() + 1, new Date(ddnew).getFullYear(), false, 'newMonth', 'Scroll');
            this.myScroll.scrollTo(x)
        }
        if (x == 0) {
            var obj = this.state.ListDay.find(x => x.Key == this.state.ListDay[0].Key)
            var ddnew = new Date(new Date(obj.date).setDate(new Date(obj.date).getDate() - 1))
            this.DateOfSearch(new Date(ddnew).getMonth() + 1, new Date(ddnew).getFullYear(), false, 'oldMonth', 'Scroll');
            this.myScroll.scrollTo(x)
        }
    }
    render() {
        return (
            <View style={{flexDirection: 'column', borderBottomWidth: 0.5, borderBottomColor: AppColors.gray, paddingBottom: 10 }}>
                <View style={[{ marginTop: 10, flexDirection: 'row' }]}>
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    </View>
                    <View style={[{ flex: 3 }, AppStyles.containerCentered]}>
                        <Text style={AppStyles.Titledefault, { color: AppColors.Maincolor }}>{this.state.ChoiceDate}</Text>
                    </View>
                    {this.state.SearchToday != null ?
                        <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end', paddingRight: 10 }} onPress={() => this.scrollToView()}>
                            <Text style={AppStyles.Titledefault, { color: this.state.SearchToday.Today == true ? AppColors.Maincolor : AppColors.gray }}>Hôm nay</Text>
                        </TouchableOpacity>
                        : null}
                </View>
                {this.state.ListDay && this.state.ListDay.length > 0 ?
                    <ScrollView
                        ref={(ref) => {
                            this.myScroll = ref;
                        }}
                        onMomentumScrollEnd={(event) => this.LoadDateEnd(event)}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        style={{ flexDirection: 'row', marginTop: 10, width: DRIVER.width }}
                    >
                        {this.state.ListDay.map((item, index) => (
                            <TouchableOpacity style={[AppStyles.StyleContentDateTop, {width:50, flexDirection: 'column' }]} onPress={() => this.scrollToView(item)}>
                                <Text style={[AppStyles.Textsmall, { color: (item.Today == true) ? AppColors.red : AppColors.gray }]}>{item.Dayofweek}</Text>
                                <Text style={[
                                    AppStyles.Titledefault,
                                    AppStyles.StyleDateTop,
                                    item.CheckBox == true ?
                                        item.Today == true ?
                                            {
                                                color: AppColors.red
                                            } :
                                            {
                                                color: AppColors.Maincolor
                                            }
                                        : ''
                                ]}>{item.dd}</Text>
                            </TouchableOpacity>
                        ))}
                    </ScrollView>
                    : null
                }

            </View>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(SearchDay);
