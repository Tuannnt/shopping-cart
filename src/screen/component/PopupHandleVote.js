import React, {Component} from 'react';
import {Alert, Platform, View} from 'react-native';
import {connect} from 'react-redux';
import Dialog from 'react-native-dialog';
import {AppStyles, AppColors} from '@theme';
import RNPickerSelect from 'react-native-picker-select';
const customStyle =
  Platform.OS === 'android'
    ? [
        AppStyles.FormInput,
        {borderRadius: 3, padding: 10, fontSize: 16, paddingLeft: 15},
      ]
    : {};
const customReveiver = Platform.OS === 'ios' ? {marginBottom: 10} : {};
class PopupHandleVote extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Title: this.props.title,
      text: 'Duyệt',
      btns: this.props.btns,
      approver: null, // nguoi nhan trong quy trinh
    };
    this.props.onClick(this.callback);
    this.submit = this.props.onSubmit;
  }

  componentDidMount = () => {
    if (this.props.checkInLogin == '1') {
      this.setState({text: 'Trình duyệt'});
    }
  };
  // Duyệt phiếu
  handleCancel = para => {
    var obj = this.state;
    if (para == 'D' || para == 'TL') {
      if (obj.text == '') {
        Alert.alert('Thông báo', 'Yêu cầu nhập ý kiến');
      } else {
        this.submit(para, obj);
      }
    } else {
      this.submit(para, obj);
    }
  };
  callback(para) {
    this.setState({
      Title: new Date().getTime(),
    });
  }
  handleChangeApprover = val => {
    this.setState({approver: val});
  };
  render() {
    return (
      <View>
        <Dialog.Container visible={true}>
          <Dialog.Title>Xử lý phiếu</Dialog.Title>
          <Dialog.Input
            label={this.state.desciption}
            onChangeText={text => this.setState({text})}
            value={this.state.text}
            style={customStyle}
            placeholder="Ý kiến..."
            autoFocus={true}
            multiline={true}
            numberOfLines={3}
          />
          {this.props.listApprover && this.props.checkInLogin != '1' && (
            <View
              style={[
                {
                  ...AppStyles.FormInputPicker,
                  justifyContent: 'center',
                  marginRight: 10,
                  marginLeft: 5,
                },
                customReveiver,
              ]}>
              <RNPickerSelect
                doneText="Xong"
                doneText="Xong"
                onValueChange={value => this.handleChangeApprover(value)}
                items={this.props.listApprover}
                value={this.state.approver}
                placeholder={{}}
                style={{
                  inputIOS: {
                    padding: 10,
                    color: 'black',
                    fontSize: 14,
                    paddingRight: 15, // to ensure the text is never behind the icon
                    paddingBottom: 15,
                  },
                  inputAndroid: {
                    padding: 10,
                    color: 'black',
                    fontSize: 14,
                    paddingRight: 15, // to ensure the text is never behind the icon
                  },
                }}
                // useNativeAndroidPickerStyle={false}
              />
            </View>
          )}
          {this.state.btns && this.state.btns.length > 0
            ? this.state.btns.map((item, index) => (
                <Dialog.Button
                  style={{color: item.color}}
                  label={item.name}
                  onPress={() => this.handleCancel(item.value)}
                />
              ))
            : null}
        </Dialog.Container>
      </View>
    );
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(PopupHandleVote);
