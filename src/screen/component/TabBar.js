import React, { Component } from 'react';
import {
  Dimensions,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { ActionConst, Actions } from 'react-native-router-flux';
import { Header, Icon } from 'react-native-elements';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';

const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class TabBar extends Component {
  NetInfoSubscribtion = null;
  constructor(props) {
    super(props);
    this.state = {
      //Search
      offSearch: false,
      //lịch
      Formcalendar: false,
      offCalendar: false,
      //QR
      FormQR: false,
      offQR: false,
      QRMultiple: false,
      //tắt quay lại
      OffBack: false,
      StartPop: this.props.StartPop,
      //task
      offTask: false,
      //checkbox
      FormCheckBox: false,
      offCheckBox: false,
      connection_status: true,
      connection_type: null,
      connection_net_reachable: false,
      connection_wifi_enabled: false,
      connection_details: null,
    };
    this.CallbackFormSearch = this.props.CallbackFormSearch;
    this.CallbackFormQR = this.props.CallbackFormQR;
    this.CallbackFormAdd = this.props.CallbackFormAdd;
    this.CallbackFormcalendar = this.props.CallbackFormcalendar;
    this.CallbackFormTask = this.props.CallbackFormTask;
    this.CallbackFormCheckBox = this.props.CallbackFormCheckBox;
  }
  componentDidMount() {
    if (this.props.QRMultiple) {
      this.setState({ QRMultiple: this.props.QRMultiple });
    }
    if (this.props.Formcalendar) {
      this.setState({ Formcalendar: this.props.Formcalendar });
    }
    if (this.props.task) {
      this.setState({ offCalendar: this.props.task });
    }
    if (this.props.FormQR == true) {
      this.setState({ FormQR: this.props.FormQR });
    }
    if (this.props.OffBack == true) {
      this.setState({ OffBack: this.props.OffBack });
    }
    if (this.props.FormCheckBox == true) {
      this.setState({ FormCheckBox: this.props.FormCheckBox });
    }
    this.NetInfoSubscribtion = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
  }
  componentWillUnmount() {
    this.NetInfoSubscribtion && this.NetInfoSubscribtion();
  }
  _handleConnectivityChange = (state) => {
    this.setState({
      connection_status: state.isConnected,
      connection_type: state.type,
      connection_net_reachable: state.isInternetReachable,
      connection_wifi_enabled: state.isWifiEnabled,
      connection_details: state.details,
    })
  }
  render() {
    const {
      hideLeft,
      leftIcon,
      leftIconSet,
      rightIcon,
      rightIconSet,
      title,
      subTitle,
    } = this.props;
    return (
      <View>
        {this.state.connection_status ? null : (<View style={{ backgroundColor: 'black', display: 'flex', alignItems: 'center', justifyContent: 'center', padding: 3 }}><Text style={{ textAlign: 'center', color: '#ffffff' }}>Không có kết nối</Text></View>)}
        <Header
          // containerStyle={{shadowColor: "#000",
          // shadowOffset: {
          //     width: 0,
          //     height: 5,
          // },
          // shadowOpacity: 0.2,
          // shadowRadius: 10.97,
          // elevation: 2,}}
          statusBarProps={AppStyles.statusbarProps}
          {...AppStyles.headerProps}
          leftComponent={this.renderLeftComponent(
            hideLeft,
            leftIcon,
            leftIconSet,
          )}
          centerComponent={this.renderTitle(title, subTitle)}
          rightComponent={this.renderRightComponent(rightIcon, rightIconSet)}
        />
      </View>
    );
  }
  renderLeftComponent = (hideLeft, leftIcon, leftIconSet) => {
    if (!leftIcon) {
      leftIcon = 'arrowleft';
      leftIconSet = 'antdesign';
    }
    return (
      <View>
        {this.state.OffBack == false ? (
          <TouchableOpacity
            style={[AppStyles.TabbarTop_left, { flex: 1, width: 50 }]}
            onPress={() => this.onPressback()}>
            {!hideLeft && (
              <Icon
                name={'arrow-left'}
                type="feather"
                size={AppSizes.TabBar_SizeIcon}
                color={AppColors.black}
              />
            )}
          </TouchableOpacity>
        ) : null}
      </View>
    );
  };
  renderRightComponent = (addForm, rightIcon, rightIconSet) => {
    if (!rightIcon) {
      rightIcon = 'plus';
      rightIconSet = 'antdesign';
    }
    return (
      <View
        style={[
          AppStyles.TabbarTop_Right,
          { flexDirection: 'row', width: '100%', justifyContent: 'flex-end' },
        ]}>
        {/* QR */}
        {this.state.FormQR == true ? (
          <TouchableOpacity
            style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]}
            onPress={() => this.openFormQR()}>
            <Icon
              name="qrcode-scan"
              type="material-community"
              color={AppColors.IconcolorTabbar}
              size={AppSizes.TabBar_SizeIcon}
            />
          </TouchableOpacity>
        ) : null}
        {/* calendar */}
        {this.state.Formcalendar == true && this.state.offCalendar == true ? (
          <TouchableOpacity
            style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]}
            onPress={() => this.openFormcalendar()}>
            <Icon
              name="tasks"
              type="font-awesome"
              color={AppColors.IconcolorTabbar}
              size={AppSizes.TabBar_SizeIcon}
            />
          </TouchableOpacity>
        ) : this.state.Formcalendar == true &&
          this.state.offCalendar == false ? (
          <TouchableOpacity
            style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]}
            onPress={() => this.openFormcalendar()}>
            <Icon
              name="calendar"
              type="font-awesome"
              color={AppColors.IconcolorTabbar}
              size={AppSizes.TabBar_SizeIcon}
            />
          </TouchableOpacity>
        ) : null}

        {/* Search */}
        {this.props.FormSearch == true ? (
          <TouchableOpacity
            style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]}
            onPress={() => this.openFormSearch()}>
            <Icon
              name="search1"
              type="antdesign"
              color={
                this.state.offSearch == true
                  ? AppColors.Maincolor
                  : AppColors.IconcolorTabbar
              }
              size={AppSizes.TabBar_SizeIcon}
            />
          </TouchableOpacity>
        ) : null}
        {/* CheckBox */}
        {this.props.FormCheckBox == true ? (
          <TouchableOpacity
            style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]}
            onPress={() => this.openFormCheckBox()}>
            <Icon
              name="checksquareo"
              type="antdesign"
              color={
                this.state.offCheckBox == true
                  ? AppColors.Maincolor
                  : AppColors.IconcolorTabbar
              }
              size={AppSizes.TabBar_SizeIcon}
            />
          </TouchableOpacity>
        ) : null}
        {/* Task */}
        {this.props.FormTask == true ? (
          <TouchableOpacity
            style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]}
            onPress={() => this.openFormTask()}>
            <Icon
              name="tasks"
              type="font-awesome"
              color={
                this.state.offTask == true
                  ? AppColors.Maincolor
                  : AppColors.IconcolorTabbar
              }
              size={AppSizes.TabBar_SizeIcon}
            />
          </TouchableOpacity>
        ) : null}
        {/* Add */}
        {this.props.addForm !== undefined ? (
          <TouchableOpacity
            style={[
              AppStyles.TabbarTop_Right,
              { marginRight: 0, flexDirection: 'row', marginLeft: 5 },
            ]}
            onPress={() => this.openFormAdd(this.props.addForm)}>
            <Icon
              name="plussquareo"
              type={rightIconSet}
              color={AppColors.IconcolorTabbar}
              size={AppSizes.TabBar_SizeIcon}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    );
  };
  renderTitle = (title, subTitle) => {
    return (
      <View
        style={[
          AppStyles.TabbarTop_Center,
          { flex: 1, width: DRIVER.width / 2.2 },
        ]}>
        <Text
          style={[
            AppStyles.Titledefault,
            { textAlign: 'center', color: AppColors.TextColorMSS },
          ]}>
          {title}
        </Text>
      </View>
    );
  };
  onPressback() {
    Actions.pop();
    Actions.refresh({ moduleId: 'home', BackModuleByCode: this.props.BackModuleByCode, ActionTime: (new Date()).getTime() });
    // if (this.state.StartPop != undefined && this.state.StartPop == true) {
    // } else {
    //   Actions.home({ BackModuleByCode: this.props.BackModuleByCode })
    // }
  }
  openFormSearch() {
    var _offSearch = !this.state.offSearch;
    this.setState({ offSearch: _offSearch });
    this.CallbackFormSearch(_offSearch);
  }
  openFormCheckBox = () => {
    this.setState({ offCheckBox: !this.state.offCheckBox }, () => {
      this.CallbackFormCheckBox(this.state.offCheckBox);
    });
  };
  openFormQR() {
    if (this.state.QRMultiple === true) {
      Actions.qrCode({ typeQR: 'multiple' });
    } else {
      Actions.qrCode({ typeQR: 'single' });
    }

    // var _offQR = !this.state.offQR;
    // this.setState({ offQR: _offQR })
    // this.CallbackFormQR(_offQR)
  }
  openFormAdd() {
    if (!this.state.connection_status) {
      Toast.showWithGravity(
        'Vui lòng kết nối mạng',
        Toast.SHORT,
        Toast.CENTER,
      );
      return
    }
    this.CallbackFormAdd();
  }
  openFormcalendar() {
    var _offCalendar = !this.state.offCalendar;
    this.setState({ offCalendar: _offCalendar });
    this.CallbackFormcalendar(_offCalendar);
  }
  openFormTask = () => {
    this.CallbackFormTask('');
  };
}

// CSS
const navHeight = Platform.OS === 'ios' ? 80 : 80 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
const styles = StyleSheet.create({
  renderRightComponent: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginRight: 10,
  },
});

export default TabBar;
