import React, { Component } from 'react';
import { Text, TouchableOpacity, View, } from 'react-native';
import { connect } from 'react-redux';
import { Icon, Badge } from 'react-native-elements';
import { AppStyles, AppSizes, AppColors } from '@theme';

class TabBarBottomCustom extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.CallbackValueBottom = this.props.onCallbackValueBottom;
        this.ListView = this.props.ListData
    }
    render() {
        const { ListView } = this
        return (
            <View style={{ flexDirection: 'row', position: 'absolute', borderWidth: 0.3, borderColor: AppColors.gray, bottom: 0, backgroundColor: '#fff' }}>
                {ListView.length > 0 ? ListView.map((item, index) =>
                    <TouchableOpacity key={index} style={{ flex: 1, paddingTop: 10, paddingBottom: 10 }} onPress={() => this.onPressClick(item.Value)}>
                        <View style={{ flex: 1 }}>
                            <Icon iconStyle={[(item.OffEditcolor == true) ? "" : (item.Checkbox) ? { color: AppColors.Maincolor } : ""]} name={item.Icon} type={item.Type}
                                size={20} />
                            {ListView.length == 5 && (item.Badge !== null && item.Badge !== undefined && item.Badge > 0) ?
                                <Badge
                                    status="error"
                                    value={item.Badge}
                                    containerStyle={{ position: 'absolute', right: 10, top: -5 }} />
                                : ListView.length == 4 && (item.Badge !== null && item.Badge !== undefined && item.Badge > 0) ?
                                    <Badge
                                        status="error"
                                        value={item.Badge}
                                        containerStyle={{ position: 'absolute', right: 20, top: -5 }} />
                                    : ListView.length == 3 && (item.Badge !== null && item.Badge !== undefined && item.Badge > 0) ?
                                        <Badge
                                            status="error"
                                            value={item.Badge}
                                            containerStyle={{ position: 'absolute', right: 35, top: -5 }} />
                                        : ListView.length == 2 && (item.Badge !== null && item.Badge !== undefined && item.Badge > 0) ?
                                            <Badge
                                                status="error"
                                                value={item.Badge}
                                                containerStyle={{ position: 'absolute', right: 65, top: -5 }} />
                                            : ListView.length == 1 && (item.Badge !== null && item.Badge !== undefined && item.Badge > 0) ?
                                                <Badge
                                                    status="error"
                                                    value={item.Badge}
                                                    containerStyle={{ position: 'absolute', right: 150, top: -5 }} />
                                                : null
                            }
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={[AppStyles.Textsmall, (item.OffEditcolor == true) ? "" : (item.Checkbox) ? { color: AppColors.Maincolor } : { color: "black" }]}>{item.Title}</Text>
                        </View>
                    </TouchableOpacity>
                ) : null}

            </View>
        );
    }
    onPressClick(para) {
        let _listView = this.ListView;
        for (let i = 0; i < _listView.length; i++) {
            if (_listView[i].Value == para) {
                _listView[i].Checkbox = true
            } else {
                _listView[i].Checkbox = false
            }
        }
        this.ListView = _listView
        this.props.onCallbackValueBottom(para)
    }
}
const mapStateToProps = state => ({})
const mapDispatchToProps = {}
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(TabBarBottomCustom);
