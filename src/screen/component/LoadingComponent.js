import React, { Component } from 'react';
import {
    View, ActivityIndicator, Text
} from 'react-native';
import { connect } from 'react-redux';
import { AppStyles, AppColors, AppFonts } from '@theme';

class LoadingComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _backgroundColor: props.backgroundColor !== undefined ? props.backgroundColor : 'rgba(169,169,169, 0.5)',
            Color: props.Color !== undefined ? props.Color : AppColors.Maincolor,
        };
    }
    render() {
        return (
            <View style={[AppStyles.Loading, { backgroundColor: this.state._backgroundColor }]}>
                <ActivityIndicator size="large" color={this.state.Color} />
                <Text style={[AppStyles.TextMessage, { color: "#848484" }]}>Đang tải . . .</Text>
            </View>
        );
    }

}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(LoadingComponent);
