import React, { Component } from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import { ActionConst, Actions } from 'react-native-router-flux';
import { Header, Icon } from 'react-native-elements';
import { connect } from 'react-redux'
import { Title } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

class TabBar_Title extends Component {
    NetInfoSubscribtion = null;
    constructor(props) {
        super(props);
        this.state = {
            //QA
            FormQR: false,
            QRMultiple: false,
            callBack: this.props.callBack,
            FormAttachment: false,
            FormEdit: false,
            FromImformation: false,
            IconSubmit: (this.props.IconSubmit && this.props.IconSubmit == true) ? this.props.IconSubmit : false,
            connection_status: true,
            connection_type: null,
            connection_net_reachable: false,
            connection_wifi_enabled: false,
            connection_details: null,
        };
        this.CallbackFormAttachment = this.props.CallbackFormAttachment
        this.CallbackFormEdit = this.props.CallbackFormEdit
        this.CallbackFromImformation = this.props.CallbackFromImformation
        this.CallbackIconSubmit = this.props.CallbackIconSubmit
        this.CallbackFormQR = this.props.CallbackFormQR
    }

    componentDidMount() {
        if (this.props.QRMultiple) {
            this.setState({ QRMultiple: this.props.QRMultiple })
        }
        if (this.props.FormQR == true) {
            this.setState({ FormQR: this.props.FormQR })
        }
        if (this.props.FormAttachment && this.props.FormAttachment == true) {
            this.setState({ FormAttachment: this.props.FormAttachment })
        }
        if (this.props.FormEdit && this.props.FormEdit == true) {
            this.setState({ FormEdit: this.props.FormEdit })
        }
        if (this.props.FromImformation && this.props.FromImformation == true) {
            this.setState({ FromImformation: this.props.FromImformation })
        }
        this.NetInfoSubscribtion = NetInfo.addEventListener(
            this._handleConnectivityChange,
        );
    }
    componentWillUnmount() {
        this.NetInfoSubscribtion && this.NetInfoSubscribtion();
    }
    _handleConnectivityChange = (state) => {
        this.setState({
            connection_status: state.isConnected,
            connection_type: state.type,
            connection_net_reachable: state.isInternetReachable,
            connection_wifi_enabled: state.isWifiEnabled,
            connection_details: state.details,
        })
    }
    render() {
        const {
            hideLeft,
            leftIcon,
            leftIconSet,
            rightIcon,
            rightIconSet,
            title,
            subTitle,
            addForm
        } = this.props;

        return (
            //view tabbar
            <View>
                {this.state.connection_status ? null : (<View style={{ backgroundColor: 'black', display: 'flex', alignItems: 'center', justifyContent: 'center', padding: 3 }}><Text style={{ textAlign: 'center', color: '#ffffff' }}>Không có kết nối</Text></View>)}
                <Header
                    statusBarProps={AppStyles.statusbarProps}
                    {...AppStyles.headerProps}
                    leftComponent={this.renderLeftComponent(hideLeft, leftIcon, leftIconSet)}
                    centerComponent={this.renderTitle(title, subTitle)}
                    rightComponent={this.renderRightComponent(rightIcon, rightIconSet)}
                />
            </View>


            // <View style={{ flexDirection: 'row', backgroundColor: "#fff" }}>
            //     <View style={{ flex: 1 }}>
            //         {this.renderLeftComponent(hideLeft)}
            //     </View>
            //     <View style={{ flex: 5 }}>
            //         {this.renderTitle(title)}
            //     </View>
            //     <View style={{ flex: 1 }}>
            //         {this.renderRightComponent(rightIcon, rightIconSet)}
            //     </View>
            // </View>
        );
    }

    renderLeftComponent = (hideLeft, leftIcon, leftIconSet) => {
        if (!leftIcon) {
            leftIcon = 'arrowleft';
            leftIconSet = 'antdesign'
        }
        return (
            <TouchableOpacity
                style={[AppStyles.TabbarTop_left, { flex: 1, width: 50 }]}
                onPress={() => this.onPressback()}>
                {!hideLeft && (
                    <Icon
                        name={'arrow-left'}
                        type="feather"
                        size={AppSizes.TabBar_SizeIcon}
                        color={AppColors.black}
                    />
                )}
            </TouchableOpacity>
        );
    }
    renderRightComponent = (title, addForm, rightIcon, rightIconSet) => {
        if (!rightIcon) {
            rightIconSet = 'ionicon'
        }
        return (
            <View style={[AppStyles.TabbarTop_Right, { flexDirection: 'row' }]}>
                {/* QR */}
                {this.state.FormQR == true ?
                    <TouchableOpacity style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]} onPress={() => this.openFormQR()}>
                        <Icon name="qrcode-scan" type="material-community" color={AppColors.IconcolorTabbar} size={AppSizes.TabBar_SizeIcon} />
                    </TouchableOpacity>
                    : null}
                {this.state.FormEdit == true ?
                    <TouchableOpacity style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]} onPress={() => this.openFormEdit()}>
                        <Icon name={'edit'} type='antdesign' color={AppColors.IconcolorTabbar} size={AppSizes.TabBar_SizeIcon} />
                    </TouchableOpacity>
                    : null}
                {this.state.FormAttachment == true ?
                    <TouchableOpacity style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]} onPress={() => this.openFormAttachment()}>
                        <Icon name="attachment" type="entypo" color={AppColors.IconcolorTabbar} size={AppSizes.TabBar_SizeIcon} />
                    </TouchableOpacity>
                    : null}
                {this.state.FromImformation == true ?
                    <TouchableOpacity style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]} onPress={() => this.openFromImformation()}>
                        <Icon name="menu" type="feather" color={AppColors.IconcolorTabbar} size={AppSizes.TabBar_SizeIcon} />
                    </TouchableOpacity>
                    : null}
                {this.state.IconSubmit == true ?
                    <TouchableOpacity style={[AppStyles.TabbarTop_Right, { marginRight: 10 }]} onPress={() => this.openIconSubmit()}>
                        <Icon name="check" type="entypo" color={AppColors.ColorEdit} size={AppSizes.TabBar_SizeIcon} />
                    </TouchableOpacity>
                    : null}
            </View>
        );
    }
    renderTitle = (title, subTitle) => {
        return (
            <View style={AppStyles.TabbarTop_Center}>
                <Text style={[AppStyles.Titledefault, { textAlign: 'center', color: AppColors.TextColorMSS }]}>{title}</Text>
            </View>
        );
    }
    onPressback() {
        this.state.callBack();
    }

    openFormEdit() {
        this.CallbackFormEdit();
    }
    openFormAttachment() {
        this.CallbackFormAttachment();
    }
    openFromImformation() {
        this.CallbackFromImformation()
    }
    openIconSubmit() {
        this.CallbackIconSubmit()
    }
    openFormQR() {
        if (this.state.QRMultiple === true) {
            Actions.qrCode({ typeQR: "multiple" })
        } else {
            Actions.qrCode({ typeQR: "single" })
        }
    }
}

// CSS
const navHeight = Platform.OS === 'ios' ? 80 : 80 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
const styles = StyleSheet.create({
    squareContainer: {
        height: toolbarHeight,
        width: toolbarHeight,
    }
});
//Redux
const mapStateToProps = state => ({
})
//Using call para public all page
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(TabBar_Title);
