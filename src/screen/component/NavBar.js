import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  Platform,
  Text,
  Image,
} from 'react-native';
import {AppColors, AppStyles, AppSizes, getStatusBarHeight} from '@theme';
import {Actions, ActionConst} from 'react-native-router-flux';
import {Header, Icon} from 'react-native-elements';
import TabNavigator from 'react-native-tab-navigator';
import {API} from '../../network';

class NavBar extends Component {
  render() {
    const {
      hideLeft,
      leftIcon,
      leftIconSet,
      hideRight,
      rightIcon,
      rightIconSet,
      title,
      subTitle,
      onRightPress,
    } = this.props;

    return (
      //view tabbar
      <View>
        <Header
          statusBarProps={AppStyles.statusbarProps}
          {...AppStyles.headerProps}
          // placement="left"
          leftComponent={this.renderLeftComponent(
            hideLeft,
            leftIcon,
            leftIconSet,
          )}
          centerComponent={this.renderTitle(title, subTitle)}
          rightComponent={this.renderRightComponent(
            hideRight,
            rightIcon,
            rightIconSet,
          )}
        />
      </View>
    );
  }

  renderLeftComponent = (hideLeft, leftIcon, leftIconSet) => {
    if (!leftIcon) {
      leftIcon = 'menu';
      leftIconSet = 'material';
    }
    return (
      <TouchableOpacity
        style={[
          styles.squareContainer,
          {alignItems: 'center', justifyContent: 'center'},
        ]}
        onPress={() => this.openDrawer()}>
        {!hideLeft && (
          <Icon name={leftIcon} type={leftIconSet} color="black" size={26} />
        )}
      </TouchableOpacity>
    );
  };
  renderRightComponent = (hideRight, rightIcon, rightIconSet) => {
    if (!rightIcon) {
      rightIcon = 'search1';
      rightIconSet = 'antdesign';
    }
    return (
      <TouchableOpacity
        style={[
          styles.squareContainer,
          {alignItems: 'center', justifyContent: 'center'},
        ]}
        onPress={() => this.openAPI()}>
        <Icon name={rightIcon} type={rightIconSet} color="black" size={26} />
      </TouchableOpacity>
    );
  };
  renderTitle = (title, subTitle) => {
    return (
      <View
        style={{
          alignItems: 'center',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Image source={require('../../images/logo/logoesvn1.png')} />
        <Text
          style={[
            AppStyles.headerStyle,
            {fontWeight: 'bold', marginLeft: 0, fontSize: 26, color: 'black'},
          ]}>
          {title}
        </Text>
      </View>
    );
  };
  openDrawer() {
    Actions.drawerOpen();
  }
  openAPI() {
    API.getAll()
      .then(res => {
        // console.log(JSON.stringify(res))
        Actions.APIComponent({bankList: res.data.Data});
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
}

// CSS
const navHeight = Platform.OS === 'ios' ? 80 : 80 - getStatusBarHeight();
const toolbarHeight = navHeight - getStatusBarHeight();
const styles = StyleSheet.create({
  squareContainer: {
    height: toolbarHeight,
    width: toolbarHeight,
  },
});

export default NavBar;
