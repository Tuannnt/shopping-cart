import React, { Component } from 'react';
import { View } from 'react-native';
import SearchableDropdown from 'react-native-searchable-dropdown';
import { connect } from 'react-redux';

class Searchable_Dropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListCombobox: this.props.ListCombobox,
            placeholder: this.props.placeholder,
            defaultItems: this.props.defaultItems,
            selectedItems: []
        };
        this.ValueListCombobox = this.props.CallBackListCombobox
    }
    ChangeValue = (value) => {
        this.ValueListCombobox(value)
    };
    render() {
        const { selectedItems } = this.state;
        return (
            <View style={{ width: '100%', paddingTop: 10 }}>
                <SearchableDropdown
                    onItemSelect={(item) => {
                        const items = this.state.selectedItems;
                        items.push(item)
                        this.setState({ selectedItems: items });
                        this.ChangeValue(item.id)
                    }}
                    containerStyle={{ padding: 5 }}
                    // onRemoveItem={(item, index) => {
                    //     const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                    //     this.setState({ selectedItems: items });
                    // }}
                    itemStyle={{
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: '#fff',
                        borderColor: '#d3d9dd',
                        borderWidth: 1,
                        borderRadius: 5,
                    }}
                    itemTextStyle={{ color: '#222' }}
                    itemsContainerStyle={{ maxHeight: 100 }}
                    items={this.state.ListCombobox}
                    defaultIndex={this.props.defaultItems}
                    resetValue={false}
                    textInputProps={
                        {
                            placeholder: this.state.placeholder,
                            underlineColorAndroid: "transparent",
                            style: {
                                padding: 12,
                                borderWidth: 1,
                                borderColor: '#ccc',
                                borderRadius: 5,
                            },
                            //onTextChange: text => alert(text)
                        }
                    }
                    listProps={
                        {
                            nestedScrollEnabled: true,
                        }
                    }
                />
            </View>
        );
    }

}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Searchable_Dropdown);
