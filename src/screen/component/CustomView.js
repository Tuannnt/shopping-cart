import React, { Component, useRef, useEffect } from 'react';
import { connect } from "react-redux";
import { Text, View, Animated, Dimensions, Easing, TouchableOpacity, StyleSheet, ScrollView, KeyboardAvoidingView } from "react-native";
import { Icon, ListItem, Header, Divider } from 'react-native-elements';
import { AppStyles, AppColors } from '@theme';
import { Actions } from 'react-native-router-flux';
import FormSearch from './FormSearch';
import { stubFalse } from 'lodash';
import QRCode from 'react-native-qrcode-svg';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
const Timeout = 200
class CustomView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            opacityTabView: new Animated.Value(0),
            isOpen: false,
            animatedValue: new Animated.Value(0)
        };
        props.eOpen(this.eOpen);
    }
    componentDidMount() {
        this.skill();
    }
    skill = () => {
        // this.animatedValue.setValue(0)
        this.opacityQR = Animated.timing(
            this.state.animatedValue,
            {
                toValue: 1,
                duration: Timeout,
                easing: Easing.linear,
                useNativeDriver: true,
            }
        );
        this.opacityQRoff = Animated.timing(
            this.state.animatedValue,
            {
                toValue: 0,
                duration: Timeout,
                easing: Easing.linear,
                useNativeDriver: true,
            }
        );
        // mờ nền
        this.opacityTabView = Animated.timing(
            this.state.opacityTabView,
            {
                toValue: 1,
                duration: Timeout,
                useNativeDriver: true,
            }
        );
        //hết mờ
        this.OffopacityTabView = Animated.timing(
            this.state.opacityTabView,
            {
                toValue: 0,
                duration: Timeout,
                useNativeDriver: true,
            }
        );
    }

    //#region  đóng cửa sổ
    eOpen = (option) => {
        if (option) {
            if (option.isChangeAction) {
                this.ae();
                this.setState({
                    data: option.data,
                })
            }
        }
        if (!this.state.isOpen) {
            this.setState({ isOpen: true, })
        } else {
            setTimeout(() => { this.setState({ isOpen: false, }) }, Timeout);
        }
        this.openAction();
    }
    //#endregion

    //#region mở cửa sổ
    openAction = () => {
        if (!this.state.isOpen) {
            Animated.sequence([this.opacityQR]).start();
            Animated.sequence([this.opacityTabView]).start();
        } else {
            Animated.sequence([this.OffopacityTabView]).start();
            Animated.sequence([this.opacityQRoff]).start();
        }
    }
    //#endregion

    render() {
        const opacityTabView = this.state.opacityTabView.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0.8]
        })
        const opacity = this.state.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
        })
        const scale = this.state.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
        })
        return (
            this.state && this.state.isOpen ?
                <View style={[{ position: 'absolute', width: '100%', height: DRIVER.height, alignItems: "center", justifyContent: 'center'}]}>
                    <Animated.View style={[{ width: DRIVER.width, position: "absolute", backgroundColor: 'black', opacity: opacityTabView, height: DRIVER.height }]} >
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => { this.eOpen() }} />
                    </Animated.View>
                    <Animated.View style={{
                        opacity,
                        transform: [{ scale }],
                        backgroundColor: '#fff',
                        alignItems: 'center',
                        borderRadius:10 
                    }}>
                        {this.props.children}
                    </Animated.View>

                </View>
                : null
        );
    }
}
export default CustomView;