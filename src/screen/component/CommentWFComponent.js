import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Image,
  TextInput,
  TouchableWithoutFeedback,
  RefreshControl,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Header} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Content,
  List,
  ListItem,
  Footer,
  FooterTab,
  Button,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
} from 'native-base';
import _ from 'lodash';
import {ErrorHandler} from '@error';
import API_CommentWF from '../../network/Comment/API_CommentWF';
import API_HR from '../../network/HR/API_HR';
import API from '../../network/API';
import configApp from '../../configApp';
import {FuncCommon} from '../../utils';
class CommentWFComponent extends Component {
  constructor() {
    super();
    this.page = 1;
    this.Length = 15;
    this.TotalRow = 0;
    this.state = {
      Comment: '',
      LoginName: '',
      loading: false,
      listWofflowData: [],
    };
  }
  componentDidMount() {
    this.getAllCommentPage(this.page);
    API.getProfile().then(rs => {
      console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
      this.setState({LoginName: rs.data.LoginName});
    });
  }
  onClickBack() {
    Actions.pop();
  }

  keyExtractor = (item, index) => index.toString();
  renderItem = ({item}) => {
    // just standard if statement
    if (item.LoginName == this.state.LoginName) {
      return (
        <View>
          <ListItem avatar style={{width: '65%', marginLeft: 120}}>
            <Body style={styles.boxCommentLogin}>
              <Text style={{color: 'white'}} note>
                {item.Comment}
              </Text>
            </Body>
            <Left>
              <Thumbnail
                style={{width: 40, height: 40}}
                source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
              />
            </Left>
          </ListItem>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginRight: 60,
            }}>
            <Text style={{fontSize: 13}}>
              {item.EmployeeName}{' '}
              {FuncCommon.ConDate(item.CreatedDate, 1, 'iso')}
            </Text>
          </View>
        </View>
      );
    }
    return (
      <View>
        <ListItem avatar style={{width: '65%'}}>
          <Left>
            <Thumbnail
              style={{width: 40, height: 40}}
              source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
            />
          </Left>
          <Body style={styles.boxComment}>
            <Text style={{color: 'black'}} note>
              {item.Comment}
            </Text>
          </Body>
        </ListItem>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginLeft: 75,
          }}>
          <Text style={{fontSize: 13}}>
            {item.EmployeeName} {FuncCommon.ConDate(item.CreatedDate, 1, 'iso')}
          </Text>
        </View>
      </View>
    );
  };
  ListEmpty = () => {
    if (this.state.listWofflowData.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        <Header
          leftComponent={this.renderLeftComponent()}
          centerComponent={{
            text: 'Thông tin xử lý - ' + this.props.Title,
            style: {color: 'black', fontSize: 13},
          }}
          //rightComponent={this.renderRightComponent()}
          containerStyle={{backgroundColor: '#fff'}}
        />
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <Container>
            <FlatList
              style={{marginBottom: 55}}
              keyExtractor={this.keyExtractor}
              ListEmptyComponent={this.ListEmpty}
              data={this.state.listWofflowData}
              refreshing={this.state.loading}
              renderItem={this.renderItem}
              onEndReached={this.handleLoadMore}
              onRefresh={this._onRefresh}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.loading}
                  tintColor="#f5821f"
                  titleColor="#fff"
                  colors={['red', 'green', 'blue']}
                />
              }
            />
          </Container>
        </View>
      </View>
    );
  }

  getAllCommentPage = Page => {
    this.page = Page;
    let obj = {
      ModuleId: this.props.ModuleId,
      RecordGuid: this.props.RecordGuid,
      Type: this.props.Type ? this.props.Type : undefined,
    };
    console.log(obj);
    const OLD_PROCESS = 1;
    this.setState({loading: true});
    // if (this.props.Type && this.props.Type === OLD_PROCESS) {
    //   //phieu dung quy trinh cu OrderId
    //   API_CommentWF.getAllCommentPageOldProcess(obj)
    //     .then(response => {
    //       if (response.data.data !== 'null') {
    //         this.state.listWofflowData = this.state.listWofflowData.concat(
    //           JSON.parse(response.data.data),
    //         );
    //       }
    //       this.setState({
    //         listWofflowData: this.state.listWofflowData,
    //         loading: false,
    //       });
    //     })
    //     .catch(error => {
    //       this.setState({loading: false});
    //       ErrorHandler.handle(error.data);
    //     });
    //   return;
    // }
    API_CommentWF.getAllCommentPage(obj)
      .then(response => {
        if (response.data.data !== 'null') {
          this.state.listWofflowData = this.state.listWofflowData.concat(
            JSON.parse(response.data.data),
          );
        }
        this.setState({
          listWofflowData: this.state.listWofflowData,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({loading: false});
        ErrorHandler.handle(error.data);
      });
  };
  handleLoadMore = () => {
    if (
      !this.state.loading &&
      this.TotalRow > this.state.listWofflowData.length
    ) {
      this.page = this.page + 1;
      this.getAllCommentPage(this.page);
    }
  };
  renderLeftComponent() {
    return (
      <View>
        <Icon
          style={{color: 'black'}}
          name={'arrow-left'}
          type="feather"
          size={20}
          onPress={() => this.onPressBack()}
        />
      </View>
    );
  }

  renderRightComponent() {
    return <View style={{flexDirection: 'row'}} />;
  }
  renderFooter = () => {
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
  //onclick
  onPressBack() {
    Actions.pop();
  }
  ConvertTime = date => {
    if (date !== null && date != '' && date !== undefined) {
      var newdate = new Date(date);
      var month = newdate.getMonth() + 1;
      var day = newdate.getDate();
      var year = newdate.getFullYear();
      var hh = newdate.getHours();
      var mm = newdate.getMinutes();
      if (month < 10) month = '0' + month;
      if (day < 10) day = '0' + day;
      if (mm < 10) mm = '0' + mm;
      var newday = new Date();
      if (newday.getDate() == day) {
        return hh + ':' + mm;
      } else {
        return day + '/' + month + '/' + year + ' ' + hh + ':' + mm;
      }
    } else {
      return null;
    }
  };
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerContent: {
    flex: 1,
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  boxSearch: {
    flexDirection: 'row',
  },
  line: {
    borderBottomColor: '#eff0f1',
    borderBottomWidth: 1,
    marginBottom: 3,
    marginTop: 3,
  },
  boxComment: {
    flex: 1,
    padding: 3,
    justifyContent: 'center',
    borderRadius: 20,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    margin: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#e8e8e8',
    width: 10,
  },
  boxCommentLogin: {
    flex: 1,
    padding: 3,
    justifyContent: 'center',
    borderRadius: 20,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    margin: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#24c79f',
  },
  boxBottom: {
    flex: 1,
    width: '100%',
    position: 'absolute',
    height: 50,
    bottom: 0,
    borderTopColor: '#eff0f1',
    borderTopWidth: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
});
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommentWFComponent);
