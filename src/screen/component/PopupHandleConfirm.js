import React, {Component} from 'react';
import {Alert, View} from 'react-native';
import {connect} from 'react-redux';
import Dialog from 'react-native-dialog';
import {AppStyles, AppColors} from '@theme';

class PopupHandleConfirm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Title: this.props.title,
      text: '',
    };
    this.btns = this.props.btns;
  }

  // Duyệt phiếu
  handleCancel = para => {
    this.props.submit(para);
  };

  render() {
    return (
      <View>
        <Dialog.Container visible={true}>
          <Dialog.Title>Thông báo</Dialog.Title>
          <Dialog.Description>Bạn có muốn xác nhận?</Dialog.Description>
          {this.btns && this.btns.length > 0
            ? this.btns.map((item, index) => (
                <Dialog.Button
                  style={{color: item.color}}
                  label={item.name}
                  onPress={() => this.handleCancel(item.value)}
                />
              ))
            : null}
        </Dialog.Container>
      </View>
    );
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(PopupHandleConfirm);
