import React, { Component } from 'react';
import { View } from 'react-native';
import {SectionedMultiSelect} from 'react-native-sectioned-multi-select';
import MultiSelect from 'react-native-multiple-select';
import { connect } from 'react-redux';
class MultiSelect_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ForrmPopup: (this.props.ForrmPopup != null && this.props.ForrmPopup == true) ? true : false,
            ListCombobox: this.props.ListCombobox,
            selectedItems: (this.props.selectedItems== null || this.props.selectedItems== undefined)? [] : this.props.selectedItems,
            selectText:(this.props.titleSearch== null || this.props.titleSearch== undefined)? "Tìm kiếm...": this.props.titleSearch
        };
        this.ValueListCombobox= this.props.CallBackListCombobox
    }
    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
        this.ValueListCombobox(selectedItems)
    };
    render() {
        const { selectedItems } = this.state;
        return (
            <View style={{ width: '100%', paddingTop:10}}>
                {this.state.ForrmPopup == true ?
                    <SectionedMultiSelect
                        items={this.state.ListCombobox}
                        uniqueKey="id"
                        subKey="detail"
                        selectText={this.state.selectText}
                        showDropDowns={true}
                        readOnlyHeadings={true}
                        onSelectedItemsChange={this.onSelectedItemsChange}
                        selectedItems={this.state.selectedItems}
                    />
                    :
                    <MultiSelect
                        //hideTags
                        items={this.state.ListCombobox}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedItemsChange}
                        selectedItems={selectedItems}
                        selectText={this.state.selectText}
                        onChangeInput={(text) => console.log(text)}
                        altFontFamily="ProximaNova-Light"
                        tagRemoveIconColor="red"
                        tagBorderColor="#CCC"
                        tagTextColor="black"
                        selectedItemTextColor="#00BFFF"
                        selectedItemIconColor="#00BFFF"
                        itemTextColor="#000"
                        displayKey="name"
                        searchInputStyle={{ color: 'black' }}
                        submitButtonColor="#FE642E"
                        submitButtonText="Chọn"
                    />
                }
                {/* <View>
                    {this.multiSelect.getSelectedItemsExt(selectedItems)}
                </View> */}
            </View>
        );
    }

}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MultiSelect_Component);
