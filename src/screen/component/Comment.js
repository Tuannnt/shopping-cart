import React, {Component} from 'react';
import {
  Keyboard,
  Text,
  View,
  Image,
  TextInput,
  Dimensions,
  LayoutAnimation,
  TouchableHighlight,
  Linking,
  Animated,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Alert,
  TouchableWithoutFeedback,
  PermissionsAndroid,
  KeyboardAvoidingView,
  Easing,
} from 'react-native';
import TabBar_Title from '../component/TabBar_Title';
import {connect} from 'react-redux';
import {Icon, Divider, Badge, Header} from 'react-native-elements';
import {Actions, Drawer} from 'react-native-router-flux';
import styles from './StyleMS';
import {API, API_HR, API_Message} from '../../network';
import {FlatGrid} from 'react-native-super-grid';
import configApp from '../../configApp';
import RNFetchBlob from 'rn-fetch-blob';
import TopBarNotification from './TopBarNotification';
import {AppStyles, AppColors} from '@theme';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import CameraRoll from '@react-native-camera-roll/camera-roll';
import OpenGroupImages from './OpenGroupImages';
import LoadingComponent from '../component/LoadingComponent';
const Sound = require('react-native-sound');
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const TimeOut = 300;
//#region Tuỳ chỉnh cấu hình chọn ảnh
const options = {
  title: 'Chọn ảnh',
  cancelButtonTitle: 'Huỷ bỏ',
  // takePhotoButtonTitle: 'Chụp ảnh',
  chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
  chooseWhichLibraryTitle: 'Chọn thư viện',
};
//custom ảnh
const _styles = StyleSheet.create({
  imageGrid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  image: {
    width: SCREEN_WIDTH / 3.03,
    height: SCREEN_WIDTH / 3.03,
    margin: 0.5,
  },
});

//#endregion
class Comment extends Component {
  constructor() {
    super();
    this.state = {
      IsGroup: '',
      Comment: '',
      Id_User: '',
      LoginName: '',
      OrganizationGuid: '',
      ChatboxGroupGuid: '',
      UserSend: '',
      ViewName: '',
      FullName: '',
      EventBoxIcon: false,
      LiTaskAttachments: [],
      LiComment: [],
      LiFiles: [],
      ListViewIcon: [],
      notifications: [],
      //-------------------------Custom ảnh
      GroupImage: [], //-danh sách thư mục ảnh
      images: [], //-danh sách ảnh
      CameraRoll_first: 0, //-số lượng ảnh lấy ra
      GroupName: '', //-Tên thư mục ảnh lấy ảnh
      ChoiceImages: false, //-phân biệt ảnh được chọn
      ListImageSubmit: [], //-danh sách ảnh được chọn
      ViewAllImages: false, //-Bật tắt full màn hình
      OpenGroup: false,
      statusloadImage: false,
      loading: true,
      down: false,
      //------------------------Các hiệu ứng chuyển động
      index: false,
      indexIcon: false,
      indexFull: false,
      offsetY: new Animated.Value(2000),
      offsetButtonX: new Animated.Value(100),
      IconOffsetX: new Animated.Value(-SCREEN_WIDTH),
      ViewFullOffsetY: new Animated.Value(SCREEN_HEIGHT),
    };
    this._ListComment = {
      ChatboxGroupGuid: '',
      Length: 50,
      NumberPage: 1,
    };
    this.DataSubmit = {
      Name: '',
      IsGroup: 0,
      detail: [],
    };
    this.state.ListIconComment = [];
    this.state.ListIcontab = [];
    (this.ValueType = 'M'),
      (this.Comment = ''),
      (this.state.COMMON_ICON_ANIMATION = {
        init: ['QAK', 'mini'],
        QAK: {
          link: '/img/chatbox/icon_q/a',
          name: 'QAK',
          default: '/img/chatbox/icon_q/icon.png',
          type: '.png',
          data: 32,
        },
        mini: {
          link: '/img/chatbox/icon_d/a',
          name: 'mini',
          default: '/img/chatbox/icon_d/icon.png',
          type: '.png',
          data: 40,
        },
        renderCode: function(code, sort) {
          return code + '%' + sort;
        },
        renderImg: function(code) {
          var _a = code.split('%');
          return (
            configApp.url_icon_chat +
            this[_a[0]].link +
            _a[1] +
            this[_a[0]].type
          );
        },
      });
    this.state.CodeIconComment = this.state.COMMON_ICON_ANIMATION.init[0];
    try {
      var _listIconComment = this.state.COMMON_ICON_ANIMATION;
      if (_listIconComment.init.length > 0) {
        for (var i = 0; i < _listIconComment.init.length; i++) {
          this.state.ListIcontab.push({
            code: _listIconComment.init[i],
            name: _listIconComment[_listIconComment.init[i]].name,
            active: false,
            link:
              configApp.url_icon_chat +
              _listIconComment[_listIconComment.init[i]].default,
          });
          for (
            var j = 1;
            j <= _listIconComment[_listIconComment.init[i]].data;
            j++
          ) {
            this.state.ListIconComment.push({
              code: _listIconComment.init[i],
              sort: j,
              link:
                configApp.url_icon_chat +
                _listIconComment[_listIconComment.init[i]].link +
                j +
                _listIconComment[_listIconComment.init[i]].type,
            });
          }
        }
        this.state.ListIcontab[0].active = true;
      }
    } catch (ex) {
      this.callBack();
      console.log('Không lấy được danh sách icon');
    }
    try {
      this.state.FileAttackments = {
        renderImage(_check, guid) {
          var check = _check.toLowerCase();
          if (check === 'png' || check == 'jpg' || check == 'jpeg') {
            return configApp.url_Image_chat + guid;
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              check +
              '-icon.png'
            );
          }
        },
        renderFile(check) {
          var _check = check.name.split('.')[check.name.split('.').length - 1];
          if (_check === 'png' || _check == 'jpg' || _check == 'jpeg') {
            return check.uri;
          } else if (
            _check === 'pdf' ||
            _check === 'doc' ||
            _check === 'excel' ||
            _check === 'xlsx' ||
            _check === 'xsl'
          ) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default-icon.png'
            );
          }
        },
      };
    } catch (ex) {
      this.callBack();
      console.log('Có lỗi khi lấy icon file');
    }
    global.__appSIGNALR.addReviceMess(this.ReviceMess);
    this.scrollHeightMess_last = 0;
    this.scrollHeight_isActive = true;
    this.clickDow = 0;

    //bật tab image
    this.OpenView = Animated.timing(this.state.offsetY, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //tắt image
    this.OffView = Animated.timing(this.state.offsetY, {
      toValue: 250,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //bật tab Icon
    this.OpenViewIcon = Animated.timing(this.state.IconOffsetX, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //tắt tab Icon
    this.OffViewIcon = Animated.timing(this.state.IconOffsetX, {
      toValue: -SCREEN_WIDTH,
      duration: TimeOut,
      useNativeDriver: true,
    });

    this.ButtonUp = Animated.timing(this.state.offsetButtonX, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });

    this.OffButtonUp = Animated.timing(this.state.offsetButtonX, {
      toValue: 100,
      duration: TimeOut,
      useNativeDriver: true,
    });

    //bật full màn hình thư mục ảnh
    this.OpenViewFull = Animated.timing(this.state.ViewFullOffsetY, {
      toValue: 0,
      duration: TimeOut,
      useNativeDriver: true,
    });
    //tắt full màn hình thư mục ảnh
    this.OffViewFull = Animated.timing(this.state.ViewFullOffsetY, {
      toValue: SCREEN_HEIGHT,
      duration: TimeOut,
      useNativeDriver: true,
    });
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    this._ListComment.ChatboxGroupGuid = this.props.guidGroup;
    this.setState({IsGroup: this.props.IsGroup});
    //#region  thông tin người đăng nhập
    API.getProfile()
      .then(rs => {
        this.setState({
          Id_User: rs.data.EmployeeGuid,
          LoginName: rs.data.LoginName,
          OrganizationGuid: rs.data.OrganizationGuid,
          FullName: rs.data.FullName,
        });
      })
      .catch(error => {
        this.callBack();
        Alert.alert('Thông báo', 'Có lỗi khi lấy thông tin người đăng nhập');
        console.log(error.data.data);
      });
    //#endregion

    //#region lấy ra tên của người mới chat(nếu là lần chat đầu tiên)
    if (
      this.props.Name == '' ||
      this.props.Name == null ||
      this.props.Name == undefined
    ) {
      this.setState({
        ViewName: this.props.FullName,
      });
      this.DataSubmit.Name = '';
    } else {
      this.setState({
        ViewName: this.props.Name,
      });
      this.DataSubmit.Name = this.props.Name;
    }
    //#endregion

    //#region lấy chuỗi login trong nhóm
    if (
      this._ListComment.ChatboxGroupGuid != '' &&
      this._ListComment.ChatboxGroupGuid != null &&
      this._ListComment.ChatboxGroupGuid != undefined
    ) {
      var obj_userSend = {Id: this._ListComment.ChatboxGroupGuid};
      API_Message.Messenger_UserSend(obj_userSend)
        .then(ress => {
          var _UserSend = JSON.parse(ress.data.data).UserSend.split(';');
          this.setState({UserSend: _UserSend});
        })
        .catch(error => {
          this.callBack();
          console.log(error.data.data);
        });
    }
    //#endregion
    this.openImages();
    this.ListData();
  }

  //#endregion
  //#region Quay lại trang
  async componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'back') {
      this._ListComment.ChatboxGroupGuid = nextProps.Data.guidGroup;
      await this.request_storage_runtime_permission();
      this.setState({
        IsGroup: nextProps.Data.IsGroup,
      });
      //thông tin người đăng nhập
      API.getProfile()
        .then(rs => {
          this.setState({
            Id_User: rs.data.EmployeeGuid,
            LoginName: rs.data.LoginName,
            OrganizationGuid: rs.data.OrganizationGuid,
            FullName: rs.data.FullName,
          });
        })
        .catch(error => {
          this.callBack();
          console.log(error.data.data);
        });
      // lấy ra tên của người mới chat(nếu là lần chat đầu tiên)
      if (
        nextProps.Data.Name == '' ||
        nextProps.Data.Name == null ||
        nextProps.Data.Name == undefined
      ) {
        this.setState({
          ViewName: nextProps.Data.FullName,
        });
        this.DataSubmit.Name = '';
      } else {
        this.setState({
          ViewName: nextProps.Data.Name,
        });
        this.DataSubmit.Name = nextProps.Data.Name;
      }
      //lấy chuỗi login trong nhóm
      if (
        this._ListComment.ChatboxGroupGuid != '' &&
        this._ListComment.ChatboxGroupGuid != null &&
        this._ListComment.ChatboxGroupGuid != undefined
      ) {
        var obj_userSend = {Id: this._ListComment.ChatboxGroupGuid};
        API_Message.Messenger_UserSend(obj_userSend)
          .then(ress => {
            var _UserSend = JSON.parse(ress.data.data).UserSend.split(';');
            this.setState({UserSend: _UserSend});
          })
          .catch(error => {
            this.callBack();
            console.log(error.data.data);
          });
      }
      CameraRoll.getAlbums({assetType: 'Photos'}).then(rs => {
        var _groupImage = [];
        var count = 0;
        var _groupName = '';
        for (let n = 0; n < rs.length; n++) {
          if (rs[n].count > count) {
            (_groupName = rs[n].title), (count = rs[n].count);
          }
        }
        this.setState({GroupName: _groupName});
        for (let v = 0; v < rs.length; v++) {
          CameraRoll.getPhotos({
            first: 1,
            assetType: 'Photos',
            groupName: rs[v].title,
          }).then(data => {
            _groupImage.push({
              text: rs[v].title,
              value: rs[v].title,
              count: rs[v].count,
              image: data.edges[0].node.image.uri,
            });
          });
        }
        this.setState({GroupImage: _groupImage});
        this._storeImages();
      });
      this.ListData();
    }
    if (nextProps.moduleId == 'InformationGotoAddGroup') {
      Actions.pop();
      Actions.refresh({
        moduleId: nextProps.moduleId,
        Data: {
          ListAddAcount: nextProps.Data.ListAddAcount,
          guidGroup: nextProps.Data.guid,
          Name: nextProps.Data.Name,
          IsGroup: nextProps.Data.IsGroup,
        },
        ActionTime: new Date().getTime(),
      });
    }
  }
  //#endregion

  //#region hàm nhận tin nhắn
  ReviceMess = (LoginName, Comment) => {
    var _LiComment = this.state.LiComment;
    var CommentString = JSON.parse(Comment);
    if (
      this._ListComment.ChatboxGroupGuid != null &&
      this._ListComment.ChatboxGroupGuid != undefined &&
      this._ListComment.ChatboxGroupGuid != ''
    ) {
      if (CommentString.Code == 'CHAT') {
        if (
          CommentString.ChatboxGroupGuid == this._ListComment.ChatboxGroupGuid
        ) {
          if (CommentString.EmployeeGuid != this.state.Id_User) {
            var whoosh = new Sound(
              'tinnhanfb.mp3',
              Sound.MAIN_BUNDLE,
              error => {
                if (error) {
                  return;
                }
                whoosh.play(success => {
                  if (success) {
                  } else {
                  }
                });
              },
            );
          }
          if (CommentString.Type == 'A') {
            _LiComment.push({
              ChatboxMessageGuid: CommentString.ChatboxMessageGuid,
              EmployeeGuid: CommentString.EmployeeGuid,
              Message: CommentString.Message,
              Time: this.customdateBydb(CommentString.Time),
              FullName: CommentString.FullName,
              Type: CommentString.Type,
            });
          } else if (CommentString.Type == 'L') {
            _LiComment.push({
              ChatboxMessageGuid: CommentString.ChatboxMessageGuid,
              EmployeeGuid: CommentString.EmployeeGuid,
              Message: CommentString.Message,
              Time: this.customdateBydb(CommentString.Time),
              FullName: CommentString.FullName,
              Type: CommentString.Type,
            });
          } else {
            _LiComment.push({
              ChatboxMessageGuid: CommentString.ChatboxMessageGuid,
              EmployeeGuid: CommentString.EmployeeGuid,
              Message: CommentString.Message,
              Time: this.customdateBydb(CommentString.Time),
              FullName: CommentString.FullName,
              Type: CommentString.Type,
              ChatboxAttackments: CommentString.ChatboxAttackments,
            });
          }
          this.setState({LiComment: _LiComment});
          this.LoadListdataByAvata();
        } else {
          var listNotification = [
            {
              id: CommentString.ChatboxMessageGuid,
              message: CommentString.Message,
              FullName: CommentString.FullName,
              avatar: API_HR.GetPicApplyLeaves(CommentString.EmployeeGuid),
            },
          ];
          var whoosh = new Sound('huawei.mp3', Sound.MAIN_BUNDLE, error => {
            if (error) {
              console.log('failed to load the sound', error);
              return;
            }
            whoosh.play(success => {
              if (success) {
              } else {
              }
            });
          });
          this.setState({notifications: listNotification});
        }
      }
    }
  };
  //#endregion

  //#region hàm tự động tắt thông báo
  removeNotification(id) {
    this.setState(state => {
      return {
        notifications: state.notifications.filter(
          notification => notification.id !== id,
        ),
      };
    });
  }
  //#endregion

  //#region thêm mới tin nhắn với người mới bắt đầu chat
  submit() {
    API.getProfile()
      .then(rs => {
        let dataSubmit = {
          Name: this.DataSubmit.Name,
          IsGroup: this.DataSubmit.IsGroup,
          detail: [this.props.LoginName, rs.data.LoginName],
        };
        API_Message.Messenger_ChatboxGroup_Insert(dataSubmit)
          .then(_res => {
            var _ChatboxGroup = JSON.parse(_res.data.data).ChatboxGroupGuid;
            this._ListComment.ChatboxGroupGuid = _ChatboxGroup;
            var obj_userSend = {Id: this._ListComment.ChatboxGroupGuid};
            //lấy chuỗi
            API_Message.Messenger_UserSend(obj_userSend)
              .then(ress => {
                var _UserSend = JSON.parse(ress.data.data).UserSend.split(';');
                this.setState({UserSend: _UserSend});
                //khai báo biến
                var ListImg = [];
                if (this.state.LiFiles.length > 0) {
                  for (let y = 0; y < this.state.LiFiles.length; y++) {
                    ListImg.push(this.state.LiFiles[y]);
                  }
                }
                if (this.state.ListImageSubmit.length > 0) {
                  for (let y = 0; y < this.state.ListImageSubmit.length; y++) {
                    ListImg.push({
                      name: this.state.ListImageSubmit[y].name,
                      size: this.state.ListImageSubmit[y].size,
                      type: this.state.ListImageSubmit[y].type,
                      uri: this.state.ListImageSubmit[y].uri,
                    });
                  }
                }
                if (
                  this.Comment === null ||
                  this.Comment === undefined ||
                  this.Comment === ''
                ) {
                  if (this.state.Comment !== '' && this.state.Comment.trim()) {
                    this.Comment = this.state.Comment;
                    this.ValueType = 'M';
                  } else {
                    this.ValueType = 'M';
                    this.Comment =
                      ListImg.length > 0
                        ? 'GFDK%KND'
                        : 'Tin nhắn gặp sự cố gửi đi';
                  }
                }
                var objMessage = {
                  ChatboxGroupGuid: this._ListComment.ChatboxGroupGuid,
                  Message: this.Comment,
                  Type: this.ValueType,
                };
                var _data = new FormData();
                _data.append('insert', JSON.stringify(objMessage));
                for (var i = 0; i < ListImg.length; i++) {
                  _data.append(ListImg[i].name, ListImg[i]);
                }
                this.setState({Comment: '', LiFiles: [], ListImageSubmit: []});
                this.Comment = '';
                API_Message.Messenger_ChatboxMessages_Insert(_data)
                  .then(res => {
                    var _ChatboxAttackments = [];
                    var _data_v2 = JSON.parse(res.data.data_v2);
                    var _data = JSON.parse(res.data.data);
                    if (_data_v2 != null) {
                      _ChatboxAttackments = _data_v2;
                    }
                    var _comment = {
                      ChatboxMessageGuid: _data.ChatboxMessageGuid,
                      ChatboxGroupGuid: _data.ChatboxGroupGuid,
                      EmployeeGuid: _data.EmployeeGuid,
                      Time: _data.Time,
                      ParentMessageGuid: null,
                      Message: _data.Message,
                      FullName: _data.FullName,
                      Type: _data.Type,
                      ChatboxAttackments: _ChatboxAttackments,
                      Code: 'CHAT',
                    };
                    var obj = {};
                    obj.LoginName = this.state.LoginName;
                    obj.OrganizationGuid = this.state.OrganizationGuid;
                    obj.ToUser = this.state.UserSend;
                    obj.DataJson = JSON.stringify(_comment);
                    global.__appSIGNALR.sendMess(obj);
                    this._CleanListImages();
                  })
                  .catch(error => {
                    this.callBack();
                    Alert.alert('Thông báo', 'Có lỗi khi thêm mới tin nhắn');
                    console.log(error.data.data);
                  });
              })
              .catch(error => {
                this.callBack();
                Alert.alert(
                  'Thông báo',
                  'Có lỗi khi lấy danh sách thành viên trong nhóm',
                );
                console.log(error.data.data);
              });
          })
          .catch(error => {
            this.callBack();
            Alert.alert('Thông báo', 'Có lỗi khi Thêm mới nhóm');
            console.log(error.data.data);
          });
      })
      .catch(error => {
        this.callBack();
        Alert.alert('Thông báo', 'Có lỗi khi lấy Thông tin người đăng nhập');
        console.log(error.data.data);
      });
  }
  //#endregion

  //#region Gửi tin nhắn
  sendComment() {
    var ListImg = [];
    if (this.state.LiFiles.length > 0) {
      for (let y = 0; y < this.state.LiFiles.length; y++) {
        ListImg.push(this.state.LiFiles[y]);
      }
    }
    if (this.state.ListImageSubmit.length > 0) {
      for (let y = 0; y < this.state.ListImageSubmit.length; y++) {
        ListImg.push({
          name: this.state.ListImageSubmit[y].name,
          size: this.state.ListImageSubmit[y].size,
          type: this.state.ListImageSubmit[y].type,
          uri: this.state.ListImageSubmit[y].uri,
        });
      }
    }
    if (
      this.Comment === null ||
      this.Comment === undefined ||
      this.Comment === ''
    ) {
      if (this.state.Comment !== '' && this.state.Comment.trim()) {
        this.Comment = this.state.Comment;
        this.ValueType = 'M';
      } else {
        this.ValueType = 'M';
        this.Comment =
          ListImg.length > 0 ? 'GFDK%KND' : 'Tin nhắn gặp sự cố gửi đi';
      }
    }
    var objMessage = {
      ChatboxGroupGuid: this._ListComment.ChatboxGroupGuid,
      Message: this.Comment,
      Type: this.ValueType,
    };
    var _data = new FormData();
    _data.append('insert', JSON.stringify(objMessage));
    for (var i = 0; i < ListImg.length; i++) {
      _data.append(ListImg[i].name, ListImg[i]);
    }

    this.setState({Comment: '', LiFiles: [], ListImageSubmit: []});
    this.Comment = '';
    API_Message.Messenger_ChatboxMessages_Insert(_data)
      .then(res => {
        var _data_v2 = [];
        var _data = JSON.parse(res.data.data);
        if (JSON.parse(res.data.data_v2) != null) {
          _data_v2 = JSON.parse(res.data.data_v2);
        }
        var _comment = {
          ChatboxMessageGuid: _data.ChatboxMessageGuid,
          ChatboxGroupGuid: _data.ChatboxGroupGuid,
          EmployeeGuid: _data.EmployeeGuid,
          Time: _data.Time,
          ParentMessageGuid: null,
          Message: _data.Message,
          FullName: _data.FullName,
          Type: _data.Type,
          ChatboxAttackments: _data_v2,
          Code: 'CHAT',
        };
        var obj = {};
        obj.LoginName = this.state.LoginName;
        obj.OrganizationGuid = this.state.OrganizationGuid;
        obj.ToUser = this.state.UserSend;
        obj.DataJson = JSON.stringify(_comment);
        global.__appSIGNALR.sendMess(obj);
        this._CleanListImages();
      })
      .catch(error => {
        this.callBack();
        Alert.alert('Thông báo', 'Có lỗi khi thêm mới tin nhắn');
        console.log(error.data.data);
      });
  }
  //#endregion

  //#region hiển thị danh sách tin nhắn
  ListData() {
    API_Message.Messenger_ChatboxMessager_ListAll(this._ListComment)
      .then(res => {
        var _list = this.state.LiComment;

        var _list_v1 = JSON.parse(res.data.data).data.reverse();
        var _list_v2 = [];
        for (let i = 0; i < _list_v1.length; i++) {
          if (_list_v1[i].ChatboxAttackments !== '') {
            _list_v1[i].ChatboxAttackments = JSON.parse(
              _list_v1[i].ChatboxAttackments,
            );
          }
          _list_v2.unshift(_list_v1[i]);
        }
        for (let i = 0; i < _list_v2.length; i++) {
          _list.unshift(_list_v2[i]);
        }
        this.setState({
          LiComment: _list,
          refreshing: false,
          CheckScoll: true,
        });
        this.LoadListdataByAvata();
      })
      .catch(error => {
        this.callBack();
        Alert.alert('Thông báo', 'Có lỗi khi lấy danh sách');
        console.log(error.data.data);
      });
  }
  //#endregion

  //#region gộp avatar cùng phút
  LoadListdataByAvata() {
    try {
      var _list = this.state.LiComment;
      var data_time = _list[0];
      var data_Date = _list[0];
      for (let j = 0; j < _list.length; j++) {
        _list[j].ViewDate = false;
        _list[j].ViewTime = false;
        _list[j].margin = false;
        if (data_Date.ChatboxMessageGuid != _list[j].ChatboxMessageGuid) {
          if (
            this.customdateTime(data_Date.Time).split(' ')[0] !=
            this.customdateTime(_list[j].Time).split(' ')[0]
          ) {
            _list[j].ViewDate = true;
            data_Date = _list[j];
          }
        }
        if (data_time.ChatboxMessageGuid != _list[j].ChatboxMessageGuid) {
          if (
            this.customdateTime(data_time.Time) !=
              this.customdateTime(_list[j].Time) &&
            data_time.EmployeeGuid == _list[j].EmployeeGuid
          ) {
            _list[j - 1].ViewTime = true;
            _list[j].margin = true;
          }
          data_time = _list[j];
        }
      }
      this.setState({
        LiComment: _list,
        loading: false,
      });
    } catch (error) {
      console.log(error);
      this.callBack();
    }
  }
  //#endregion

  //#region cuộn tin nhắn
  Up_Down(e) {
    this.clickDow = e.nativeEvent.contentSize.height;
    var y = e.nativeEvent.contentOffset.y;
    if (y < e.nativeEvent.contentSize.height - SCREEN_HEIGHT) {
      this.setState({down: true});
    } else {
      this.setState({down: false});
    }
  }
  handleScroll(event) {
    this.setState({EventBoxIcon: false});
    var y = event.nativeEvent.contentOffset.y;
    if (y <= 20) {
      this.setState({loading: true});
      this._ListComment.NumberPage++;
      this.scrollHeightMess_last = event.nativeEvent.contentSize.height;
      this.ListData();
    }
  }
  lazyGetHeight = (w, h) => {
    if (this.scrollHeight_isActive && h !== 0) {
      this.scrollRef.scrollToEnd();
      this.scrollHeight_isActive = false;
    } else {
      this.scrollRef.scrollTo(h - this.scrollHeightMess_last);
    }
    setTimeout(() => {
      this.setState({loading: false});
    }, 1000);
  };
  //#endregion

  //#region lấy icon tin nhắn
  onclickicon(para) {
    var _listIconView = [];
    for (let i = 0; i < this.state.ListIconComment.length; i++) {
      if (this.state.ListIconComment[i].code == para) {
        _listIconView.push(this.state.ListIconComment[i]);
      }
    }
    this.setState({ListViewIcon: _listIconView, EventBoxIcon: true});
  }
  //#endregion

  //#region  lấy thư mục ảnh
  openImages() {
    CameraRoll.getAlbums({assetType: 'All'}).then(rs => {
      try {
        var _groupImage = [];
        var count = 0;
        var _groupName = '';
        for (let n = 0; n < rs.length; n++) {
          if (rs[n].count > count) {
            (_groupName = rs[n].title), (count = rs[n].count);
          }
        }
        this.setState({GroupName: _groupName});
        for (let v = 0; v < rs.length; v++) {
          CameraRoll.getPhotos({
            first: 1,
            assetType: 'Photos',
            groupName: rs[v].title,
          }).then(data => {
            _groupImage.push({
              text: rs[v].title,
              value: rs[v].title,
              count: rs[v].count,
              image: data.edges[0].node.image.uri,
            });
          });
        }
        this.setState({GroupImage: _groupImage});
        this.end_cursor = undefined;
        this._storeImages();
      } catch (ex) {
        this.callBack();
        console.log('Có lỗi khi lấy album anh');
      }
    });
  }
  //#endregion

  //#region File ảnh
  OnScrollEndDrag(event, item) {
    var y = event.nativeEvent.contentOffset.y;
    var i = ((SCREEN_WIDTH / 3.03) * item.length) / 3 - 500;
    if (y >= i) {
      if (this.state.statusloadImage) {
        this._storeImages();
      }
    }
  }
  OnScrollEndDragFull(event, item) {
    var y = event.nativeEvent.contentOffset.y;
    var i = ((SCREEN_WIDTH / 3.03) * item.length) / 3 - SCREEN_HEIGHT;
    if (y >= i) {
      if (this.state.statusloadImage) {
        this._storeImages();
      }
    }
  }

  _storeImages() {
    CameraRoll.getPhotos({
      first: 50,
      assetType: 'Photos',
      groupName: this.state.GroupName,
      // toTime:100
      after: this.end_cursor,
    })
      .then(data => {
        this.setState({statusloadImage: data.page_info.has_next_page});
        this.end_cursor = data.page_info.end_cursor;
        var assets = data.edges;
        var _images = this.state.images;
        for (let i = 0; i < assets.length; i++) {
          var name = assets[i].node.image.uri.split('/');
          var check = this.state.ListImageSubmit.find(
            x => x.uri === assets[i].node.image.uri,
          );
          if (check !== undefined) {
            _images.push({
              name:
                assets[i].node.image.filename !== null
                  ? assets[i].node.image.filename
                  : name[name.length - 1].toLowerCase(),
              size: assets[i].node.image.fileSize,
              type: assets[i].node.type.toLowerCase(),
              uri: assets[i].node.image.uri,
              check: true,
              CountSelect: check.CountSelect,
            });
          } else {
            _images.push({
              name:
                assets[i].node.image.filename !== null
                  ? assets[i].node.image.filename
                  : name[name.length - 1].toLowerCase(),
              size: assets[i].node.image.fileSize,
              type: assets[i].node.type.toLowerCase(),
              uri: assets[i].node.image.uri,
            });
          }
        }
        this.setState({
          images: _images,
        });
      })
      .catch(err => {
        this.callBack();
        console.log(err);
        alert(err);
      });
  }
  _selectMultiple(item) {
    var listSubmit = [];
    if (this.state.ListImageSubmit.length > 0) {
      var obj = this.state.ListImageSubmit.find(x => x.name === item.name);
      if (obj !== undefined) {
        var count = 0;
        for (let i = 0; i < this.state.ListImageSubmit.length; i++) {
          if (this.state.ListImageSubmit[i].name !== item.name) {
            listSubmit.push({
              name: this.state.ListImageSubmit[i].name,
              size: this.state.ListImageSubmit[i].size,
              type: this.state.ListImageSubmit[i].type,
              uri: this.state.ListImageSubmit[i].uri,
              check: true,
              CountSelect: count + 1,
            });
            count = count + 1;
          }
        }
        this.state.ListImageSubmit = listSubmit;
      } else {
        this.state.ListImageSubmit.push({
          name: item.name,
          size: item.size,
          type: item.type,
          uri: item.uri,
          check: true,
          CountSelect: this.state.ListImageSubmit.length + 1,
        });
      }
    } else {
      this.state.ListImageSubmit.push({
        name: item.name,
        size: item.size,
        type: item.type,
        uri: item.uri,
        check: true,
        CountSelect: this.state.ListImageSubmit.length + 1,
      });
    }
    var AllListImages = this.state.images;
    for (let y = 0; y < AllListImages.length; y++) {
      if (AllListImages[y].name === item.name) {
        AllListImages[y].check = !AllListImages[y].check;
      }
      for (let z = 0; z < this.state.ListImageSubmit.length; z++) {
        if (AllListImages[y].name === this.state.ListImageSubmit[z].name) {
          AllListImages[y].CountSelect = this.state.ListImageSubmit[
            z
          ].CountSelect;
          break;
        }
      }
    }
    this.setState({
      images: AllListImages,
    });
  }
  _CleanListImages() {
    var ListImages = this.state.images;
    for (let i = 0; i < ListImages.length; i++) {
      ListImages[i].check = false;
    }
    this.setState({
      images: ListImages,
      ListImageSubmit: [],
    });
  }
  //#endregion

  //#region hiển thị danh sách tin nhắn
  renderComment = () =>
    this.state.LiComment.length > 0
      ? this.state.LiComment.map((item, index) => {
          return (
            <TouchableOpacity style={{flexDirection: 'column'}}>
              {item.ViewDate == true ? (
                <View
                  style={[
                    AppStyles.containerCentered,
                    {marginTop: 20, justifyContent: 'center'},
                  ]}>
                  <View
                    style={[
                      {
                        borderTopWidth: 0.5,
                        borderTopColor: '#CCCCCC',
                        width: SCREEN_WIDTH,
                      },
                    ]}
                  />
                  <View
                    style={[AppStyles.containerCentered, styles.styleDateMess]}>
                    <Text style={[AppStyles.Textsmall]}>
                      {this.customDate(item.Time)}
                    </Text>
                  </View>
                </View>
              ) : null}
              {item.EmployeeGuid == this.state.Id_User ? (
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 3}} />
                  <View
                    style={[
                      item.margin == true ? {marginTop: 20} : null,
                      {flex: 7, alignItems: 'flex-end'},
                    ]}>
                    {item.Type == 'A' ? (
                      <View>
                        <TouchableOpacity style={styles.boxImage}>
                          <Image
                            style={AppStyles.ImageStyle}
                            source={{
                              uri: this.state.COMMON_ICON_ANIMATION.renderImg(
                                item.Message,
                              ),
                            }}
                          />
                        </TouchableOpacity>
                        {item.ViewTime == true ? (
                          <View
                            style={{alignItems: 'flex-end', marginRight: 10}}>
                            <Text style={styles.TextViewTime}>
                              {this.customTime(item.Time)}
                            </Text>
                          </View>
                        ) : (
                          <View style={{marginTop: 5}} />
                        )}
                      </View>
                    ) : item.ChatboxAttackments &&
                      item.ChatboxAttackments.length > 0 ? (
                      <View>
                        {item.ChatboxAttackments.length == 1 ? (
                          <View style={{marginBottom: 3, marginRight: 20}}>
                            <TouchableOpacity
                              onPress={() =>
                                this.onViewImage(item.ChatboxAttackments[0])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.ChatboxAttackments[0].FileExtension,
                                    item.ChatboxAttackments[0].AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        ) : item.ChatboxAttackments.length == 2 ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              width: '100%',
                              margin: 3,
                              marginRight: 10,
                            }}>
                            <TouchableOpacity
                              style={{marginRight: 5}}
                              onPress={() =>
                                this.onViewImage(item.ChatboxAttackments[0])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.ChatboxAttackments[0].FileExtension,
                                    item.ChatboxAttackments[0].AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() =>
                                this.onViewImage(item.ChatboxAttackments[1])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.ChatboxAttackments[1].FileExtension,
                                    item.ChatboxAttackments[1].AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        ) : item.ChatboxAttackments.length == 3 ? (
                          <View
                            style={{
                              flexDirection: 'column',
                              marginBottom: 3,
                              marginRight: 10,
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                marginBottom: 5,
                              }}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[0])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[0].FileExtension,
                                      item.ChatboxAttackments[0].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[1])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[1].FileExtension,
                                      item.ChatboxAttackments[1].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row', width: '100%'}}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[2])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[2].FileExtension,
                                      item.ChatboxAttackments[2].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          </View>
                        ) : (
                          <View
                            style={{
                              flexDirection: 'column',
                              marginBottom: 3,
                              marginRight: 10,
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                marginBottom: 5,
                              }}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[0])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[0].FileExtension,
                                      item.ChatboxAttackments[0].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[1])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[1].FileExtension,
                                      item.ChatboxAttackments[1].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row', width: '100%'}}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[2])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[2].FileExtension,
                                      item.ChatboxAttackments[2].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{flex: 1}}
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[0])
                                }>
                                <View
                                  style={[
                                    AppStyles.containerCentered,
                                    {
                                      width: 100,
                                      height: 100,
                                      borderRadius: 15,
                                      backgroundColor: AppColors.gray,
                                      opacity: 0.5,
                                    },
                                  ]}>
                                  <Text style={AppStyles.Titledefault}>
                                    + {item.ChatboxAttackments.length - 3}
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                          </View>
                        )}
                        {item.Message !== 'GFDK%KND' ? (
                          <TouchableOpacity
                            style={[styles.boxComment, {paddingBottom: 5}]}>
                            <Text style={AppStyles.TextMessage}>
                              {item.Message}
                            </Text>
                            {item.ViewTime == true ? (
                              <Text style={styles.TextViewTime}>
                                {this.customTime(item.Time)}
                              </Text>
                            ) : null}
                          </TouchableOpacity>
                        ) : item.ViewTime == true ? (
                          <View
                            style={{
                              alignItems: 'flex-end',
                              marginRight: 10,
                              marginTop: 10,
                              marginBottom: 3,
                            }}>
                            <Text style={styles.TextViewTime}>
                              {this.customTime(item.Time)}
                            </Text>
                          </View>
                        ) : null}
                      </View>
                    ) : item.ViewTime == false ? (
                      <TouchableOpacity style={styles.boxComment}>
                        {item.Type == 'L' ? (
                          <Text
                            style={AppStyles.TextMessage}
                            onPress={() => Linking.openURL(item.Message)}>
                            {item.Message}
                          </Text>
                        ) : (
                          <Text style={AppStyles.TextMessage}>
                            {item.Message}
                          </Text>
                        )}
                      </TouchableOpacity>
                    ) : (
                      <View style={{flexDirection: 'column'}}>
                        <TouchableOpacity
                          style={[styles.boxComment, {paddingBottom: 5}]}>
                          {item.Type == 'L' ? (
                            <Text
                              style={AppStyles.TextMessage}
                              onPress={() => Linking.openURL(item.Message)}>
                              {item.Message}
                            </Text>
                          ) : (
                            <Text style={AppStyles.TextMessage}>
                              {item.Message}
                            </Text>
                          )}
                          <Text style={styles.TextViewTime}>
                            {this.customTime(item.Time)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>
                </View>
              ) : (
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={[
                      item.margin == true ? {marginTop: 20} : null,
                      {flex: 7, flexDirection: 'row'},
                    ]}>
                    {item.margin == false ? (
                      <View style={{marginTop: 10, marginLeft: 5, opacity: 0}}>
                        <Image
                          style={{width: 20, height: 20, borderRadius: 50}}
                          source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                        />
                      </View>
                    ) : (
                      <View style={{marginLeft: 5}}>
                        <Image
                          style={{width: 20, height: 20, borderRadius: 50}}
                          source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                        />
                      </View>
                    )}

                    {item.Type == 'A' ? (
                      <View>
                        {this.state.IsGroup == 'True' &&
                        item.ViewTime == true ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'flex-start',
                              marginLeft: 5,
                              marginTop: 10,
                              marginBottom: 3,
                            }}>
                            <Text style={AppStyles.Textsmall}>
                              {this.customFullName(item.FullName)} |{' '}
                            </Text>
                            <Text style={styles.TextViewTime}>
                              {this.customTime(item.Time)}
                            </Text>
                          </View>
                        ) : null}
                        <View style={styles.boxImage_user}>
                          <Image
                            style={AppStyles.ImageStyle}
                            source={{
                              uri: this.state.COMMON_ICON_ANIMATION.renderImg(
                                item.Message,
                              ),
                            }}
                          />
                        </View>
                      </View>
                    ) : item.ChatboxAttackments &&
                      item.ChatboxAttackments.length > 0 ? (
                      <View>
                        {this.state.IsGroup == 'True' &&
                        item.ViewTime == true ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'flex-start',
                              marginLeft: 5,
                              marginTop: 10,
                              marginBottom: 3,
                            }}>
                            <Text style={AppStyles.Textsmall}>
                              {this.customFullName(item.FullName)} |{' '}
                            </Text>
                            <Text style={styles.TextViewTime}>
                              {this.customTime(item.Time)}
                            </Text>
                          </View>
                        ) : item.ViewTime == true ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'flex-start',
                              marginLeft: 5,
                              marginTop: 10,
                              marginBottom: 3,
                            }}>
                            <Text style={styles.TextViewTime}>
                              {this.customTime(item.Time)}
                            </Text>
                          </View>
                        ) : (
                          <View style={{marginTop: 5}} />
                        )}
                        {item.ChatboxAttackments.length == 1 ? (
                          <View style={{marginBottom: 3}}>
                            <TouchableOpacity
                              onPress={() =>
                                this.onViewImage(item.ChatboxAttackments[0])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.ChatboxAttackments[0].FileExtension,
                                    item.ChatboxAttackments[0].AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        ) : item.ChatboxAttackments.length == 2 ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              width: '100%',
                              margin: 3,
                            }}>
                            <TouchableOpacity
                              style={{marginRight: 5}}
                              onPress={() =>
                                this.onViewImage(item.ChatboxAttackments[0])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.ChatboxAttackments[0].FileExtension,
                                    item.ChatboxAttackments[0].AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() =>
                                this.onViewImage(item.ChatboxAttackments[1])
                              }>
                              <Image
                                style={AppStyles.ImageStyle}
                                source={{
                                  uri: this.state.FileAttackments.renderImage(
                                    item.ChatboxAttackments[1].FileExtension,
                                    item.ChatboxAttackments[1].AttachmentGuid,
                                  ),
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        ) : item.ChatboxAttackments.length == 3 ? (
                          <View style={{flexDirection: 'column', margin: 3}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                marginBottom: 5,
                              }}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[0])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[0].FileExtension,
                                      item.ChatboxAttackments[0].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[1])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[1].FileExtension,
                                      item.ChatboxAttackments[1].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row', width: '100%'}}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[2])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[2].FileExtension,
                                      item.ChatboxAttackments[2].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          </View>
                        ) : (
                          <View
                            style={{flexDirection: 'column', marginBottom: 3}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                marginBottom: 5,
                              }}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[0])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[0].FileExtension,
                                      item.ChatboxAttackments[0].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[1])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[1].FileExtension,
                                      item.ChatboxAttackments[1].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row', width: '100%'}}>
                              <TouchableOpacity
                                style={{marginRight: 5}}
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[2])
                                }>
                                <Image
                                  style={AppStyles.ImageStyle}
                                  source={{
                                    uri: this.state.FileAttackments.renderImage(
                                      item.ChatboxAttackments[2].FileExtension,
                                      item.ChatboxAttackments[2].AttachmentGuid,
                                    ),
                                  }}
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{flex: 1}}
                                onPress={() =>
                                  this.onViewImage(item.ChatboxAttackments[0])
                                }>
                                <View
                                  style={[
                                    AppStyles.containerCentered,
                                    {
                                      width: 100,
                                      height: 100,
                                      borderRadius: 15,
                                      backgroundColor: AppColors.gray,
                                      opacity: 0.5,
                                    },
                                  ]}>
                                  <Text style={AppStyles.Titledefault}>
                                    + {item.ChatboxAttackments.length - 3}
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                          </View>
                        )}
                        {item.Message !== 'GFDK%KND' ? (
                          <TouchableOpacity style={styles.boxComment_user}>
                            <Text style={AppStyles.TextMessage}>
                              {item.Message}
                            </Text>
                          </TouchableOpacity>
                        ) : null}
                      </View>
                    ) : item.ViewTime == false ? (
                      <TouchableOpacity style={styles.boxComment_user}>
                        {item.Type == 'L' ? (
                          <Text
                            style={AppStyles.TextMessage}
                            onPress={() => Linking.openURL(item.Message)}>
                            {item.Message}
                          </Text>
                        ) : (
                          <Text style={AppStyles.TextMessage}>
                            {item.Message}
                          </Text>
                        )}
                      </TouchableOpacity>
                    ) : this.state.IsGroup == 'False' ? (
                      <TouchableOpacity
                        style={[styles.boxComment_user, {paddingBottom: 5}]}>
                        {item.Type == 'L' ? (
                          <Text
                            style={AppStyles.TextMessage}
                            onPress={() => Linking.openURL(item.Message)}>
                            {item.Message}
                          </Text>
                        ) : (
                          <Text style={AppStyles.TextMessage}>
                            {item.Message}
                          </Text>
                        )}
                        <Text style={styles.TextViewTime}>
                          {this.customTime(item.Time)}
                        </Text>
                      </TouchableOpacity>
                    ) : (
                      <View style={{flexDirection: 'column'}}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            marginLeft: 5,
                            marginTop: 10,
                          }}>
                          <Text style={AppStyles.Textsmall}>
                            {this.customFullName(item.FullName)} |{' '}
                          </Text>
                          <Text style={styles.TextViewTime}>
                            {this.customTime(item.Time)}
                          </Text>
                        </View>
                        <TouchableOpacity style={styles.boxComment_user}>
                          {item.Type == 'L' ? (
                            <Text
                              style={AppStyles.TextMessage}
                              onPress={() => Linking.openURL(item.Message)}>
                              {item.Message}
                            </Text>
                          ) : (
                            <Text style={AppStyles.TextMessage}>
                              {item.Message}
                            </Text>
                          )}
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>
                  <View style={{flex: 3}} />
                </View>
              )}
            </TouchableOpacity>
          );
        })
      : null;
  //#endregion

  //#region View Tổng
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={styles.container}>
            {this.state.notifications.length > 0 ? (
              <TopBarNotification
                Listview={this.state.notifications}
                removeNotification={id => this.removeNotification(id)}
              />
            ) : null}
            <TabBar_Title
              title={this.state.ViewName}
              callBack={() => this.callBack()}
              FromImformation={true}
              CallbackFromImformation={() => this.onPressInformation()}
            />
            <Divider />
            <View style={[{flex: 1, paddingBottom: 20}]}>
              <ScrollView
                ref={ref => (this.scrollRef = ref)}
                onContentSizeChange={this.lazyGetHeight}
                onScrollEndDrag={e => this.Up_Down(e)}
                onMomentumScrollEnd={event => this.handleScroll(event)}>
                {this.state.LiComment && this.state.LiComment.length > 0
                  ? this.renderComment()
                  : null}
              </ScrollView>
              {this.state.loading == true ? <LoadingComponent /> : null}
              {this.state.down == true ? (
                <TouchableOpacity
                  style={styles.ClickDown}
                  onPress={() => {
                    this.scrollRef.scrollTo(this.clickDow);
                    this.setState({down: false});
                  }}>
                  <Icon
                    color={'#fff'}
                    size={20}
                    name="angle-double-down"
                    type="font-awesome"
                  />
                </TouchableOpacity>
              ) : null}
            </View>
            <View
              style={
                Platform.OS === 'ios'
                  ? styles.boxBottom_contentIOS
                  : styles.boxBottom_content
              }>
              {/* icon */}
              <TouchableOpacity
                style={[AppStyles.containerCentered, {flex: 1}]}
                onPress={() => this.OnEventBoxIcon()}>
                <Icon
                  iconStyle={{
                    color: this.state.EventBoxIcon
                      ? AppColors.Maincolor
                      : '#AAAAAA',
                  }}
                  name="smile"
                  type="feather"
                />
              </TouchableOpacity>
              {/* Text */}
              <TouchableOpacity style={{flex: 5}}>
                <TextInput
                  onFocus={() => this.OffAllForm()}
                  underlineColorAndroid="transparent"
                  placeholder="Tin nhắn"
                  style={{fontSize: 16}}
                  autoCapitalize="none"
                  multiline={true}
                  value={this.state.Comment}
                  onChangeText={text => this.setState({Comment: text})}
                />
              </TouchableOpacity>
              {/* File */}
              {(this.state.Comment !== '' && this.state.Comment.trim()) ||
              (this.state.LiFiles && this.state.LiFiles.length > 0) ? (
                this._ListComment.ChatboxGroupGuid == '' ||
                this._ListComment.ChatboxGroupGuid == null ||
                this._ListComment.ChatboxGroupGuid == undefined ? (
                  <TouchableOpacity
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      flex: 1,
                    }}
                    onPress={() => this.submit()}>
                    <Icon
                      iconStyle={{color: 'blue'}}
                      name="send"
                      type="Feather"
                    />
                    {this.state.ListImageSubmit.length > 0 ? (
                      <Badge
                        badgeStyle={{
                          width: 15,
                          height: 15,
                          borderWidth: 0,
                          borderRadius: 20,
                        }}
                        value={this.state.ListImageSubmit.length}
                        containerStyle={{
                          position: 'absolute',
                          right: 10,
                          top: 3,
                        }}
                      />
                    ) : null}
                  </TouchableOpacity>
                ) : (
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      flex: 1,
                    }}>
                    <Icon
                      iconStyle={{color: 'blue'}}
                      onPress={() => this.sendComment()}
                      name="send"
                      type="Feather"
                    />
                    {this.state.ListImageSubmit.length > 0 ? (
                      <Badge
                        badgeStyle={{
                          width: 15,
                          height: 15,
                          borderWidth: 0,
                          borderRadius: 20,
                        }}
                        value={this.state.ListImageSubmit.length}
                        containerStyle={{
                          position: 'absolute',
                          right: 10,
                          top: 3,
                        }}
                      />
                    ) : null}
                  </View>
                )
              ) : (
                <View style={{flexDirection: 'row', flex: 2}}>
                  <TouchableOpacity
                    style={[AppStyles.containerCentered, {flex: 1}]}
                    onPress={() => this.choiceFile()}>
                    <Icon
                      iconStyle={{color: '#AAAAAA'}}
                      name="attachment"
                      type="entypo"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[AppStyles.containerCentered, {flex: 1}]}
                    onPress={() => {
                      this.EventChoiceImages();
                    }}>
                    <Icon
                      iconStyle={{
                        color: this.state.ChoiceImages
                          ? AppColors.Maincolor
                          : '#AAAAAA',
                      }}
                      name="picture"
                      type="antdesign"
                    />
                    {this.state.ListImageSubmit.length > 0 ? (
                      <Badge
                        badgeStyle={{
                          width: 15,
                          height: 15,
                          borderWidth: 0,
                          borderRadius: 20,
                        }}
                        value={this.state.ListImageSubmit.length}
                        containerStyle={{
                          position: 'absolute',
                          right: 5,
                          top: 5,
                        }}
                      />
                    ) : null}
                  </TouchableOpacity>
                </View>
              )}
            </View>
            {this.CustomChoiceImages()}
            {this.state.ListViewIcon && this.state.ListViewIcon.length > 0
              ? this.CustomChoiceIcon()
              : null}
            {this.state.GroupImage.length > 0 ? this.CustomViewAllImg() : null}
            <Animated.View
              style={{
                transform: [{translateX: this.state.offsetButtonX}],
                position: 'absolute',
                zIndex: 100,
                right: 20,
                bottom: 30,
              }}>
              <TouchableOpacity onPress={() => this.EvenViewImgFull()}>
                <Badge
                  badgeStyle={{
                    width: 40,
                    height: 40,
                    borderWidth: 0,
                    borderRadius: 20,
                  }}
                  value={
                    <Icon
                      color={'#fff'}
                      name="chevron-small-up"
                      type="entypo"
                      size={30}
                    />
                  }
                />
              </TouchableOpacity>
            </Animated.View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  //#endregion

  //#region hiên thị list Icon dưới form nhập
  CustomChoiceIcon = () => {
    const TabIcon = this.state.indexIcon == true ? {height: 250} : {height: 0};
    return (
      <Animated.View
        style={[
          TabIcon,
          {
            transform: [{translateX: this.state.IconOffsetX}],
            backgroundColor: '#fff',
          },
        ]}>
        <View style={{flexDirection: 'column'}}>
          <View style={{height: 200}}>
            <ScrollView style={styles.boxBottomIcon}>
              <View style={[_styles.imageGrid, {width: SCREEN_WIDTH}]}>
                {this.state.ListViewIcon.map(item => {
                  return (
                    <TouchableOpacity
                      style={{padding: 5}}
                      onPress={() => this.ValueCode(item.code, item.sort)}>
                      <Image
                        style={{width: 80, height: 80}}
                        source={{uri: item.link}}
                      />
                    </TouchableOpacity>
                  );
                })}
              </View>
            </ScrollView>
          </View>
          <View style={styles.boxBottomIconTab}>
            {this.state.ListIcontab.map((item, index) => (
              <TouchableOpacity
                style={{marginRight: 5}}
                onPress={() => this.onclickicon(item.code)}>
                <Image
                  style={{width: 40, height: 40}}
                  source={{uri: item.link}}
                />
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </Animated.View>
    );
  };
  //#endregion

  //#region hiên thị list ảnh dưới form nhập
  CustomChoiceImages = () => {
    const TabImage = this.state.index == true ? {height: 250} : {height: 0};
    return (
      <Animated.View
        style={[TabImage, {transform: [{translateY: this.state.offsetY}]}]}>
        <ScrollView
          ref={ref => {
            this.myScroll = ref;
          }}
          onScrollEndDrag={event =>
            this.OnScrollEndDrag(event, this.state.images)
          }>
          <View style={[_styles.imageGrid, {width: SCREEN_WIDTH}]}>
            <TouchableOpacity
              style={[
                _styles.image,
                AppStyles.containerCentered,
                {backgroundColor: '#fff'},
              ]}
              onPress={() => this.ChoicePicture()}>
              <Icon
                color={'#777777'}
                name="camera"
                type="font-awesome"
                size={30}
              />
              <Text style={{color: '#777777'}}>Chụp ảnh</Text>
            </TouchableOpacity>
            {this.state.images.map(image => {
              return (
                <View>
                  <TouchableOpacity
                    style={{
                      padding: 10,
                      zIndex: 1,
                      position: 'absolute',
                      right: 0,
                    }}
                    onPress={() => this._selectMultiple(image)}>
                    {image.check == true ? (
                      <Badge
                        badgeStyle={{
                          width: 25,
                          height: 25,
                          borderWidth: 0,
                          borderRadius: 20,
                        }}
                        value={image.CountSelect}
                      />
                    ) : (
                      <Badge
                        badgeStyle={{
                          width: 25,
                          height: 25,
                          borderWidth: 2,
                          borderRadius: 20,
                          backgroundColor: 'rgba(52, 52, 52, 0.2)',
                        }}
                      />
                    )}
                  </TouchableOpacity>
                  <TouchableOpacity
                  // onPress={() => this._selectImage(image)}
                  >
                    <Image style={_styles.image} source={{uri: image.uri}} />
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </Animated.View>
    );
  };
  //#endregion

  //#region hiển thị danh sách chọn hình ảnh cả màn hình
  CustomViewAllImg = () => {
    return (
      <Animated.View
        style={{
          backgroundColor: 'black',
          transform: [{translateY: this.state.ViewFullOffsetY}],
          position: 'absolute',
          zIndex: 1000,
          bottom: 0,
          width: SCREEN_WIDTH,
        }}>
        <View
          style={{
            height: SCREEN_HEIGHT,
            width: SCREEN_WIDTH,
            backgroundColor: 'black',
          }}>
          <OpenGroupImages
            value={this.state.GroupName} // giá trị mặc định
            TypeSelect={'single'} // chọn 1 ("single") , chọn nhiều  ("multiple")
            callback={this.ChangeGroupIMG} //callback lại dữ liệu được chọn gồm text và value
            data={this.state.GroupImage} // danh sách hiển thị
            nameMenu={'Chọn người chủ trì'} // tiêu đề
            eOpen={this.openGroupIMG} // đóng cửa sổ
            position={'top'} //danh sách hiển thị từ dưới lên (bottom), hiển thị từ trên xuống ("top")
          />
          <Header
            containerStyle={{
              backgroundColor: 'rgba(52, 52, 52,0.8)',
              position: 'absolute',
              zIndex: 200,
              width: SCREEN_WIDTH,
              top: 0,
            }}
            leftComponent={
              <TouchableOpacity
                style={{
                  flex: 1,
                  width: 45,
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => this.EvenViewImgFull()}>
                <Icon name="close" type="antdesign" color={'#fff'} size={20} />
              </TouchableOpacity>
            }
            centerComponent={
              <TouchableOpacity
                style={{
                  flex: 8,
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                }}
                onPress={() => this.onActionGroupIMG()}>
                <Text
                  style={[
                    AppStyles.Titledefault,
                    {textAlign: 'center', color: '#fff'},
                  ]}>
                  {this.state.GroupName}{' '}
                </Text>
                <Icon color={'#fff'} name="down" type="antdesign" size={18} />
              </TouchableOpacity>
            }
          />
          <ScrollView
            ref={ref => {
              this.myScroll = ref;
            }}
            onScrollEndDrag={event =>
              this.OnScrollEndDragFull(event, this.state.images)
            }>
            <View style={_styles.imageGrid}>
              <TouchableOpacity
                style={[
                  _styles.image,
                  AppStyles.containerCentered,
                  {backgroundColor: '#fff'},
                ]}
                onPress={() => this.ChoicePicture()}>
                <Icon
                  color={'#777777'}
                  name="camera"
                  type="font-awesome"
                  size={30}
                />
                <Text style={{color: '#777777'}}>Chụp ảnh</Text>
              </TouchableOpacity>
              {this.state.images.map(image => {
                return (
                  <View>
                    <TouchableOpacity
                      style={{
                        padding: 10,
                        zIndex: 1,
                        position: 'absolute',
                        right: 0,
                      }}
                      onPress={() => this._selectMultiple(image)}>
                      {image.check == true ? (
                        <Badge
                          badgeStyle={{
                            width: 25,
                            height: 25,
                            borderWidth: 0,
                            borderRadius: 20,
                          }}
                          value={image.CountSelect}
                        />
                      ) : (
                        <Badge
                          badgeStyle={{
                            width: 25,
                            height: 25,
                            borderWidth: 2,
                            borderRadius: 20,
                            backgroundColor: 'rgba(52, 52, 52, 0.2)',
                          }}
                        />
                      )}
                    </TouchableOpacity>
                    <TouchableOpacity
                    // onPress={() => this._selectImage(image)}
                    >
                      <Image style={_styles.image} source={{uri: image.uri}} />
                    </TouchableOpacity>
                  </View>
                );
              })}
            </View>
          </ScrollView>
          <View
            style={{
              backgroundColor: 'rgba(52, 52, 52,0.8)',
              position: 'absolute',
              zIndex: 200,
              width: SCREEN_WIDTH,
              height: 50,
              bottom: 0,
            }}>
            {this.state.ListImageSubmit.length > 0 ? (
              <TouchableOpacity
                style={{
                  marginBottom: 30,
                  marginRight: 20,
                  position: 'absolute',
                  zIndex: 1,
                  right: 0,
                  bottom: 0,
                }}>
                <Badge
                  badgeStyle={{
                    width: 40,
                    height: 40,
                    borderWidth: 0,
                    borderRadius: 20,
                    backgroundColor: '#EEEEEE',
                  }}
                  value={
                    this._ListComment.ChatboxGroupGuid == '' ||
                    this._ListComment.ChatboxGroupGuid == null ||
                    this._ListComment.ChatboxGroupGuid == undefined ? (
                      <TouchableOpacity
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          flex: 1,
                        }}
                        onPress={() => {
                          this.submit(), this.setState({ViewAllImages: false});
                        }}>
                        <Icon
                          iconStyle={{color: 'blue'}}
                          name="send"
                          type="Feather"
                        />
                        <Badge
                          badgeStyle={{
                            width: 15,
                            height: 15,
                            borderWidth: 0,
                            borderRadius: 20,
                          }}
                          value={this.state.ListImageSubmit.length}
                          containerStyle={{
                            position: 'absolute',
                            left: 20,
                            top: 0,
                          }}
                        />
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          flex: 1,
                        }}
                        onPress={() => {
                          this.sendComment(),
                            this.setState({ViewAllImages: false});
                        }}>
                        <Icon
                          iconStyle={{color: 'blue'}}
                          name="send"
                          type="Feather"
                        />
                        <Badge
                          badgeStyle={{
                            width: 15,
                            height: 15,
                            borderWidth: 0,
                            borderRadius: 20,
                          }}
                          value={this.state.ListImageSubmit.length}
                          containerStyle={{
                            position: 'absolute',
                            left: 20,
                            top: 0,
                          }}
                        />
                      </TouchableOpacity>
                    )
                  }
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={{
                  marginBottom: 30,
                  marginRight: 20,
                  position: 'absolute',
                  zIndex: 1,
                  right: 0,
                  bottom: 0,
                }}>
                <Badge
                  badgeStyle={{
                    width: 40,
                    height: 40,
                    borderWidth: 0,
                    borderRadius: 20,
                    backgroundColor: '#EEEEEE',
                  }}
                  value={
                    <TouchableOpacity
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flex: 1,
                      }}
                      onPress={() => {
                        this.submit(), this.setState({ViewAllImages: false});
                      }}>
                      <Icon
                        iconStyle={{color: 'blue'}}
                        name="send"
                        type="Feather"
                      />
                      {this.state.ListImageSubmit.length > 0 ? (
                        <Badge
                          badgeStyle={{
                            width: 15,
                            height: 15,
                            borderWidth: 0,
                            borderRadius: 20,
                          }}
                          value={this.state.ListImageSubmit.length}
                          containerStyle={{
                            position: 'absolute',
                            left: 20,
                            top: 0,
                          }}
                        />
                      ) : null}
                    </TouchableOpacity>
                  }
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Animated.View>
    );
  };
  //#endregion

  //#region hiển thị nhóm ảnh
  _openGroupIMG() {}
  openGroupIMG = d => {
    this._openGroupIMG = d;
  };
  onActionGroupIMG() {
    this._openGroupIMG();
  }
  ChangeGroupIMG = rs => {
    try {
      console.log('=======================');
      console.log(rs);
      if (rs !== null) {
        this.state.GroupName = rs.value;
        this.setState({
          GroupName: this.state.GroupName,
          images: [],
        });
        this.end_cursor = undefined;
        this._storeImages();
      }
    } catch (error) {
      console.log(error);
    }
  };
  //#endregion

  //#region sự kiện bật tắt form chọn ảnh
  EventChoiceImages = () => {
    Keyboard.dismiss();
    if (!this.state.index) {
      Animated.sequence([this.ButtonUp]).start();
      Animated.sequence([this.OpenView]).start();
      //tắt form khác
      Animated.sequence([this.OpenViewIcon]).start();
      this.setState({indexIcon: false, EventBoxIcon: false});
    } else {
      Animated.sequence([this.OffView]).start();
      Animated.sequence([this.OffButtonUp]).start();
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      index: !this.state.index,
      ChoiceImages: !this.state.ChoiceImages,
    });
  };
  //#endregion

  //#region sự iện bật tắt form Icon
  OnEventBoxIcon() {
    Keyboard.dismiss();
    var _listIconView = [];
    for (let i = 0; i < this.state.ListIconComment.length; i++) {
      if (this.state.ListIconComment[i].code == this.state.CodeIconComment) {
        _listIconView.push(this.state.ListIconComment[i]);
      }
    }
    this.setState({ListViewIcon: _listIconView});
    if (!this.state.indexIcon) {
      Animated.sequence([this.OpenViewIcon]).start();
      //tắt form khác
      Animated.sequence([this.OffView]).start();
      Animated.sequence([this.OffButtonUp]).start();
      this.setState({index: false, ChoiceImages: false});
    } else {
      Animated.sequence([this.OffViewIcon]).start();
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      indexIcon: !this.state.indexIcon,
      EventBoxIcon: !this.state.EventBoxIcon,
    });
    // this.EventChoiceImages();
  }
  //#endregion

  //#region sự kiện tắt hết form khi nhập tin nhắn
  OffAllForm = () => {
    //tắt form khác
    Animated.sequence([this.OffView]).start();
    Animated.sequence([this.OffButtonUp]).start();
    Animated.sequence([this.OpenViewIcon]).start();
    this.setState({
      indexIcon: false,
      EventBoxIcon: false,
      index: false,
      ChoiceImages: false,
    });
  };
  //#endregion

  //#region sự kiện mở album ảnh toàn màn hình
  EvenViewImgFull = () => {
    if (!this.state.indexFull) {
      Animated.sequence([this.OpenViewFull]).start();
    } else {
      Animated.sequence([this.OffViewFull]).start();
    }
    this.setState({indexFull: !this.state.indexFull});
  };
  //#endregion

  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.LiFiles;

      for (let index = 0; index < res.length; index++) {
        _liFiles.push(res[index]);
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].name = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({
        LiFiles: _liFiles,
        EventBoxIcon: false,
        ChoiceImages: false,
      });
    } catch (err) {
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async ChoicePicture() {
    try {
      var _liFiles = this.state.LiFiles;
      await ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            name:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          LiFiles: _liFiles,
          EventBoxIcon: false,
          ChoiceImages: false,
        });
        if (
          this._ListComment.ChatboxGroupGuid == '' ||
          this._ListComment.ChatboxGroupGuid == null ||
          this._ListComment.ChatboxGroupGuid == undefined
        ) {
          this.submit();
        } else {
          this.sendComment();
        }
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion

  //#region Tải file đính kèm
  OnDownload(attachObject) {
    Alert.alert(
      //title
      'Thông báo',
      //body
      'Bạn muốn tải file đính kèm về máy',
      [
        {text: 'Tải về', onPress: () => this.DownloadFile(attachObject)},
        {text: 'Huỷ', onPress: () => null},
      ],
      {cancelable: true},
    );
  }
  //tair file đính kèm về
  DownloadFile(attachObject) {
    let dirs = RNFetchBlob.fs.dirs;
    if (Platform.OS !== 'ios') {
      RNFetchBlob.config({
        addAndroidDownloads: {
          useDownloadManager: true, // <-- this is the only thing required
          // Optional, override notification setting (default to true)
          notification: true,
          // Optional, but recommended since android DownloadManager will fail when
          // the url does not contains a file extension, by default the mime type will be text/plain
          mime: '/',
          description: 'File downloaded by download manager.',
          //title: new Date().toLocaleString() + ' - test.xlsx',
          //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
          path: 'file://' + dirs.DownloadDir + '/' + attachObject.Title, //using for android
        },
      })
        .fetch(
          'GET',
          configApp.url_View_Image + attachObject.AttachmentGuid,
          {},
        )
        .then(resp => {
          Alert.alert(
            //title
            'Thông báo',
            //body
            'Tải thành công',
            [{text: 'Đóng', onPress: () => null}],
            {cancelable: true},
          );
          // the path of downloaded file
          resp.path();
        });
    } else {
      RNFetchBlob.config({
        path: dirs.DocumentDir + '/' + attachObject.Title,
        addAndroidDownloads: {
          useDownloadManager: true, // <-- this is the only thing required
          // Optional, override notification setting (default to true)
          notification: true,
          IOSDownloadTask: true,
          // Optional, but recommended since android DownloadManager will fail when
          // the url does not contains a file extension, by default the mime type will be text/plain
          mime: '/',
          description: 'File downloaded by download manager.',
          //title: new Date().toLocaleString() + ' - test.xlsx',
          //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
          //using for android
        },
      })
        .fetch(
          'GET',
          configApp.url_View_Image + attachObject.AttachmentGuid,
          {},
        )
        .then(resp => {
          // the path of downloaded file
          // resp.path()
          CameraRoll.saveToCameraRoll(
            dirs.DocumentDir + '/' + attachObject.Title,
          )
            .then(
              Alert.alert(
                //title
                'Thông báo',
                //body
                'Tải thành công',
                [{text: 'Đóng', onPress: () => null}],
                {cancelable: true},
              ),
              // alert('Tải đính kèm thành công', 'Photo added to camera roll!')
            )
            .catch(err => console.log('err:', err));
          // alert('Image Downloaded Successfully.');
        });
    }
  }
  //#endregion

  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  //#region Mở tab xem ảnh
  onViewImage(obj) {
    Actions.viewAllImage({
      ChatboxGroupGuid: this.props.guidGroup,
      ItemImage: obj,
    });
  }
  //#endregion

  removeAttactment(para) {
    var list = this.state.LiFiles;
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].name != para.name) {
        listNewValue.push(list[i]);
      }
    }
    this.setState({
      LiFiles: listNewValue,
    });
  }

  callBack() {
    this.setState({EventBoxIcon: true});
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'chat', ActionTime: new Date().getTime()});
  }

  ValueCode(code, sort) {
    this.ValueType = 'A';
    this.Comment = this.state.COMMON_ICON_ANIMATION.renderCode(code, sort);
    this.sendComment();
  }

  onPressInformation() {
    Keyboard.dismiss();
    Actions.Information({
      Name: this.state.ViewName,
      guid: this._ListComment.ChatboxGroupGuid,
    });
  }

  //#region custom lấy tên
  customFullName(para) {
    if (para != null) {
      var _name = para.split(' ');
      var _nameView = '';
      for (let i = 0; i < _name.length; i++) {
        if (i == _name.length - 1) {
          _nameView = _name[i];
        }
      }
      return _nameView;
    }
  }
  //#endregion

  //#region định dạng thời gian
  //định dạng giờ phút
  customTime(Time) {
    if (Time != null) {
      var SplitTime = String(Time).split(' ');
      //định dạng giờ
      if (SplitTime[2] == 'PM') {
        var SplitTime_v2 = SplitTime[1].split(':');
        var HH = (parseInt(SplitTime_v2[0]) + 12).toString();
        if (HH == 24) {
          HH = '00';
        }
        var mm = '';
        if (SplitTime_v2[1].length < 2) {
          mm = '0' + SplitTime_v2[1];
        } else {
          mm = SplitTime_v2[1];
        }
        var ss = '';
        if (SplitTime_v2[2].length < 2) {
          ss = '0' + SplitTime_v2[2];
        } else {
          ss = SplitTime_v2[2];
        }
      } else {
        var SplitTime_v2 = SplitTime[1].split(':');
        var HH = SplitTime_v2[0];
        var mm = '';
        if (SplitTime_v2[1].length < 2) {
          mm = '0' + SplitTime_v2[1];
        } else {
          mm = SplitTime_v2[1];
        }
        var ss = '';
        if (SplitTime_v2[2].length < 2) {
          ss = '0' + SplitTime_v2[2];
        } else {
          ss = SplitTime_v2[2];
        }
      }
      var time = HH + ':' + mm;
      return time;
    } else {
      return '';
    }
  }
  //định dạng ngày
  customDate(item) {
    if (item != null) {
      var SplitTime = String(item).split(' ');
      //định dạng ngày
      var SplitTime_v1 = SplitTime[0].split('/');
      var MM = '';
      if (SplitTime_v1[0].length < 2) {
        MM = '0' + SplitTime_v1[0];
      } else {
        MM = SplitTime_v1[0];
      }
      var dd = '';
      if (SplitTime_v1[1].length < 2) {
        dd = '0' + SplitTime_v1[1];
      } else {
        dd = SplitTime_v1[1];
      }
      var date = dd + '-' + MM + '-' + SplitTime_v1[2];

      //định dạng ngày hiện tại
      var MMNow = '';
      if ((new Date().getMonth() + 1).toString().length < 2) {
        MMNow = '0' + (new Date().getMonth() + 1).toString();
      } else {
        MMNow = (new Date().getMonth() + 1).toString();
      }
      var ddNow = '';
      if (new Date().getDate().toString().length < 2) {
        ddNow = '0' + new Date().getDate().toString();
      } else {
        ddNow = new Date().getDate().toString();
      }
      var SplitDateNow = ddNow + '-' + MMNow + '-' + new Date().getFullYear();
      if (date === SplitDateNow) {
        return 'Hôm nay';
      } else {
        return date;
      }
    } else {
      return '';
    }
  }
  //#region định dạng ngày
  customdateTime(DataTime) {
    if (DataTime != null) {
      var SplitTime = String(DataTime).split(' ');
      //định dạng ngày
      var SplitTime_v1 = SplitTime[0].split('/');
      var MM = '';
      if (SplitTime_v1[0].length < 2) {
        MM = '0' + SplitTime_v1[0];
      } else {
        MM = SplitTime_v1[0];
      }
      var dd = '';
      if (SplitTime_v1[1].length < 2) {
        dd = '0' + SplitTime_v1[1];
      } else {
        dd = SplitTime_v1[1];
      }
      var date = dd + '-' + MM + '-' + SplitTime_v1[2];

      //định dạng giờ
      if (SplitTime[2] == 'PM') {
        var SplitTime_v2 = SplitTime[1].split(':');
        var HH = (parseInt(SplitTime_v2[0]) + 12).toString();
        if (HH == 24) {
          HH = '00';
        }
        var mm = '';
        if (SplitTime_v2[1].length < 2) {
          mm = '0' + SplitTime_v2[1];
        } else {
          mm = SplitTime_v2[1];
        }
        var ss = '';
        if (SplitTime_v2[2].length < 2) {
          ss = '0' + SplitTime_v2[2];
        } else {
          ss = SplitTime_v2[2];
        }
      } else {
        var SplitTime_v2 = SplitTime[1].split(':');
        var HH = SplitTime_v2[0];
        var mm = '';
        if (SplitTime_v2[1].length < 2) {
          mm = '0' + SplitTime_v2[1];
        } else {
          mm = SplitTime_v2[1];
        }
        var ss = '';
        if (SplitTime_v2[2].length < 2) {
          ss = '0' + SplitTime_v2[2];
        } else {
          ss = SplitTime_v2[2];
        }
      }
      var time = HH + ':' + mm;
      //định dạng ngày hiện tại
      var MMNow = '';
      if ((new Date().getMonth() + 1).toString().length < 2) {
        MMNow = '0' + (new Date().getMonth() + 1).toString();
      } else {
        MMNow = (new Date().getMonth() + 1).toString();
      }
      var ddNow = '';
      if (new Date().getDate().toString().length < 2) {
        ddNow = '0' + new Date().getDate().toString();
      } else {
        ddNow = new Date().getDate().toString();
      }
      var SplitDateNow = ddNow + '-' + MMNow + '-' + new Date().getFullYear();
      if (date === SplitDateNow) {
        return time;
      } else {
        return date + ' ' + time;
      }
    } else {
      return '';
    }
  }
  //#endregion

  //#region định dạnh ngày của dữ liệu db
  customdateBydb(para) {
    if (para !== null) {
      var splipDateTime = String(para).split('T');
      var splipTime = splipDateTime[1].split('.');
      var HHTime = splipTime[0].split(':');
      var HH = '';
      var PM = '';
      if (parseInt(HHTime[0]) > 12) {
        HH = (parseInt(HHTime[0]) - 12).toString();
        PM = 'PM';
      } else {
        HH = HHTime[0];
        PM = 'AM';
      }
      var timedb = HH + ':' + HHTime[1] + ':' + HHTime[2] + ' ' + PM;

      var splipDate = splipDateTime[0].split('-');
      var datedb = splipDate[1] + '/' + splipDate[2] + '/' + splipDate[0];
      return (datedb + ' ' + timedb).toString();
    } else {
      return '';
    }
  }
  //#endregion
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Comment);
