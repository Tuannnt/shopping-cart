import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import { connect } from 'react-redux';
import { Divider, Icon, ListItem, SearchBar } from 'react-native-elements';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
class FormSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Title: ""
        };
        this.CallbackSearch=this.props.CallbackSearch
    }
    //Tìm kiếm
    updateSearch = search => {
        this.state.Title= search;
        this.setState({Title:this.state.Title});
        this.CallbackSearch(this.state.Title);
    };
    render() {
        return (
            <View>
                <SearchBar
                    placeholder="Tìm kiếm..."
                    lightTheme
                    round
                    inputContainerStyle={{}}
                    containerStyle={AppStyles.FormSearchBar}
                    onChangeText={search => this.updateSearch(search)}
                    value={this.state.Title}
                />
            </View>

        );
    }

}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(FormSearch);
