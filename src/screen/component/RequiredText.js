import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {AppColors} from '@theme';

export default class RequiredText extends Component {
  render() {
    return <Text style={{color: AppColors.ColorDelete}}>*</Text>;
  }
}
