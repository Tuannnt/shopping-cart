import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Icon, ListItem, CheckBox} from 'react-native-elements';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Button,
  PermissionsAndroid,
  Alert,
  Keyboard,
} from 'react-native';
import API_Attachment from '../../network/Attachment/API_Attachment';
import {FuncCommon} from '@utils';
import {AppStyles, AppSizes, AppColors} from '@theme';
import TabBar from '../../screen/component/TabBar';
import FormSearch from '../../screen/component/FormSearch';
import TabBarBottomCustom from '../../screen/component/TabBarBottomCustom';
import {FlatGrid} from 'react-native-super-grid';
import configApp from '../../configApp';
import Modal, {
  ModalTitle,
  ModalContent,
  ModalFooter,
  ModalButton,
} from 'react-native-modals';
import MenuActionCompoment from '../../screen/component/MenuActionCompoment';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import OpenPhotoLibrary from '../component/OpenPhotoLibrary';
import Toast from 'react-native-simple-toast';

class AttachmentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenAction: false,
      model: {},
      list: [],
      isFetching: false,
      // staticParam: {
      //   ModuleId: this.props.ModuleId,
      //   ParentGuid: null,
      //   RecordGuid: this.props.RecordGuid,
      //   Keyword: "",
      //   //QueryOrderBy: "Name DESC",
      // },
      ModuleId: this.props.ModuleId,
      ParentGuid: null,
      RecordGuid: this.props.RecordGuid,
      Keyword: '',
      selectItem: null,
      selected: undefined,
      openSearch: false,
      DropdownOrder: [
        {
          value: 'Name',
          label: 'Sắp xếp theo tên',
        },
        {
          value: 'Date',
          label: 'Sắp xếp theo ngày',
        },
        {
          value: 'Size',
          label: 'Sắp xếp theo kích thước',
        },
      ],
      showList: true,
      showGrid: false,
      showModalOpenFile: false,
      showModalRenameFile: false,
      showModalAddFolder: false,
      showModalListFile: false,
      NameMenu: '',
      ChoiseFolder: [
        {
          AttachmentGuid: null,
          FileName: 'Thư mục gốc',
        },
      ],
      ItemFile: {},
      Title: '',
      FolderName: '',
      lstFiles: [],
      EventBoxIcon: false,
      editName: '',
      checkBox: false,
      checked: false,
      DataCheck: [],
    };
    this.state.FileAttackments = {
      renderImage(check) {
        let _check = check.replace(/\./g, '');
        return (
          configApp.url_icon_chat +
          configApp.link_type_icon +
          _check +
          '-icon.png'
        );
      },
    };
    this.listtabbarBotom = [
      {
        Title: 'Xóa',
        Icon: 'delete',
        Type: 'antdesign',
        Value: 'delete',
        Checkbox: false,
      },
      {
        Title: 'Download',
        Icon: 'download',
        Type: 'antdesign',
        Value: 'download',
        Checkbox: false,
      },
    ];
    this.state.dataMenuFile = [
      {
        key: 'delete',
        name: 'Xóa',
        icon: {
          name: 'delete',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
      {
        key: 'details',
        name: 'Chi tiết',
        icon: {
          name: 'infocirlceo',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
      // {
      //   key: 'edit',
      //   name: 'Đổi tên',
      //   icon: {
      //     name: 'edit',
      //     type: 'antdesign',
      //     color: AppColors.Maincolor,
      //   }
      // },
      {
        key: 'download',
        name: 'Tải xuống',
        icon: {
          name: 'download',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
    ];
    this.state.dataMenuFolder = [
      {
        key: 'delete',
        name: 'Xóa',
        icon: {
          name: 'delete',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
      {
        key: 'details',
        name: 'Chi tiết',
        icon: {
          name: 'infocirlceo',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
      // {
      //   key: 'edit',
      //   name: 'Đổi tên',
      //   icon: {
      //     name: 'edit',
      //     type: 'antdesign',
      //     color: AppColors.Maincolor,
      //   }
      // }
    ];
    this.state.dataMenuTop = [
      {
        key: 'upload',
        name: 'Tải lên file',
        icon: {
          name: 'upload',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
      {
        key: 'uploadImg',
        name: 'Tải lên hình ảnh',
        icon: {
          name: 'picture',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
      {
        key: 'addfolder',
        name: 'Tạo thư mục',
        icon: {
          name: 'addfolder',
          type: 'antdesign',
          color: AppColors.Maincolor,
        },
      },
    ];
  }
  GetFiles = () => {
    var _obj = {
      ModuleId: this.state.ModuleId,
      ParentGuid: this.state.ParentGuid,
      RecordGuid: this.state.RecordGuid,
      Keyword: this.state.Keyword,
    };
    this.state.list = [];
    this.setState({
      refreshing: true,
      list: this.state.list,
    });
    API_Attachment.GetAttList(_obj)
      .then(res => {
        //this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
        var data = res.data.data != 'null' ? JSON.parse(res.data.data) : [];
        this.setState({list: data});
        this.setState({refreshing: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({refreshing: false});
      });
  };
  async componentDidMount(): void {
    await this.request_storage_runtime_permission();
    this.GetFiles();
  }
  ListEmpty = () => {
    if (this.state.list.length > 0) return null;
    return (
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  updateSearch = text => {
    this.state.Keyword = text;
    this.setState({
      list: [],
      Keyword: this.state.Keyword,
    });
    this.GetFiles();
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  hideOnboarding() {
    if (this.state.move === -100) {
      this.setState({
        move: 0,
      });
    } else {
      this.setState({
        move: -100,
      });
    }
    Animated.timing(this.state.offsetY, {toValue: this.state.move}).start();
  }
  render() {
    return (
      <View style={[AppStyles.container, {backgroundColor: null}]}>
        <TabBar
          title={'Danh sách đính kèm'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          //BackModuleByCode={'TM'}
          StartPop={true}
          addForm={'abc'}
          CallbackFormAdd={() => this.onActionTop()}
          notEdit={this.props.notEdit}
        />
        {this.state.ChoiseFolder.length > 1 ? (
          <View
            style={{
              marginLeft: 10,
              marginRight: 10,
              borderRadius: 10,
              borderWidth: 0.5,
              flexDirection: 'row',
              height: 30,
              alignItems: 'center',
            }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              style={{}}
              overScrollMode="always">
              {this.state.ChoiseFolder.map((item, index) => (
                <TouchableOpacity
                  style={{paddingLeft: 5}}
                  onPress={() => this.BackFolder(item.AttachmentGuid, index)}>
                  <Text>{item.FileName}</Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        ) : null}
        {this.state.openSearch == true ? (
          <FormSearch
            CallbackSearch={callback => this.updateSearch(callback)}
          />
        ) : null}
        <View style={{flexDirection: 'row', backgroundColor: '#fff'}}>
          <View style={{flex: 5, paddingLeft: 10, paddingRight: 10}} />
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Icon
              color={
                this.state.showList === true ? AppColors.Maincolor : '#888888'
              }
              name={'list'}
              type="entypo"
              onPress={() => this.ChangeOrder('list')}
            />
          </View>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Icon
              color={
                this.state.showGrid === true ? AppColors.Maincolor : '#888888'
              }
              name={'grid'}
              type="entypo"
              onPress={() => this.ChangeOrder('grid')}
            />
          </View>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Icon
              color={
                this.state.checkBox === true ? AppColors.Maincolor : '#888888'
              }
              name={'checksquareo'}
              type="antdesign"
              onPress={() => {
                this.state.checkBox === true
                  ? this.setState({checkBox: false})
                  : this.setState({checkBox: true});
              }}
            />
          </View>
        </View>
        <View style={{flex: 1}}>
          {/* begin xem chi tiết */}
          <Modal
            width={0.9}
            visible={this.state.showModalOpenFile}
            rounded
            actionsBordered
            onTouchOutside={() => {
              this.setState({showModalOpenFile: false});
            }}
            modalTitle={
              <ModalTitle title={this.state.ItemFile.FileName} align="left" />
            }
            footer={
              <ModalFooter>
                <ModalButton
                  text="Tải xuống"
                  bordered
                  onPress={() => {
                    this.setState({showModalOpenFile: false}),
                      this.DownloadFile(this.state.ItemFile);
                  }}
                  key="button-1"
                  style={[
                    this.state.ItemFile.FileExtension === 'FOLDER'
                      ? styles.hidden
                      : '',
                    {flex: 1},
                  ]}
                />
                <ModalButton
                  text="Đóng"
                  bordered
                  onPress={() => {
                    this.setState({showModalOpenFile: false});
                  }}
                  key="button-1"
                  style={{flex: 1}}
                />
              </ModalFooter>
            }>
            <ModalContent style={{backgroundColor: '#fff', height: 200}}>
              {/* <View style={{ flex: 1, backgroundColor: '#fff', padding: 10 }}> */}
              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                  Ngày tạo :
                </Text>
                <Text style={[AppStyles.Textdefault, {flex: 2}]}>
                  {FuncCommon.ConDate(this.state.ItemFile.CreatedDate, 0)}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                  Ngày sửa :
                </Text>
                <Text style={[AppStyles.Textdefault, {flex: 2}]}>
                  {FuncCommon.ConDate(this.state.ItemFile.ModifiedDate, 0)}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                  Dung lượng :
                </Text>
                <Text style={[AppStyles.Textdefault, {flex: 2}]}>
                  {this.ConFileSize(this.state.ItemFile.FileSize, 1)}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                  Người tạo :
                </Text>
                <Text style={[AppStyles.Textdefault, {flex: 2}]}>
                  {this.state.ItemFile.CreatedBy}
                </Text>
              </View>
              {/* </View> */}
            </ModalContent>
          </Modal>
          {/* end xem chi tiết */}

          {/* begin đổi tên file */}
          <Modal
            width={0.9}
            visible={this.state.showModalRenameFile}
            rounded
            actionsBordered
            onTouchOutside={() => {
              this.setState({showModalRenameFile: false});
            }}
            modalTitle={<ModalTitle title={this.state.editName} align="left" />}
            footer={
              <ModalFooter>
                <ModalButton
                  text="Xác nhận"
                  bordered
                  onPress={() => {
                    this.setState({showModalRenameFile: false});
                  }}
                  key="button-1"
                />
                <ModalButton
                  text="Đóng"
                  bordered
                  onPress={() => {
                    this.setState({showModalRenameFile: false});
                  }}
                  key="button-2"
                />
              </ModalFooter>
            }>
            <ModalContent style={{backgroundColor: '#fff', height: 80}}>
              <View style={{paddingLeft: 10, paddingRight: 10}}>
                <TextInput
                  style={{borderColor: '#BDBDBD', marginTop: 10}}
                  underlineColorAndroid="transparent"
                  placeholderTextColor="#C0C0C0"
                  numberOfLines={1}
                  multiline={true}
                  value={this.state.Title}
                  onChangeText={Title => this.setTitle(Title)}
                />
              </View>
            </ModalContent>
          </Modal>
          {/* end đổi tên file */}

          {/* begin tạo thư mục */}
          <Modal
            width={0.9}
            visible={this.state.showModalAddFolder}
            rounded
            actionsBordered
            onTouchOutside={() => {
              this.setState({showModalAddFolder: false});
            }}
            modalTitle={<ModalTitle title={'Tạo thư mục'} align="left" />}
            footer={
              <ModalFooter>
                <ModalButton
                  text="Tạo"
                  bordered
                  onPress={() => this.SubmitAddFolder()}
                  key="button-1"
                />
                <ModalButton
                  text="Đóng"
                  bordered
                  onPress={() => {
                    this.setState({showModalAddFolder: false});
                  }}
                  key="button-2"
                />
              </ModalFooter>
            }>
            <ModalContent style={{backgroundColor: '#fff', height: 80}}>
              <View style={{paddingLeft: 10, paddingRight: 10}}>
                <TextInput
                  style={{borderColor: '#BDBDBD', marginTop: 10}}
                  underlineColorAndroid="transparent"
                  placeholderTextColor="#C0C0C0"
                  placeholder="Nhập tên thư mục"
                  numberOfLines={1}
                  multiline={true}
                  value={this.state.FolderName}
                  onChangeText={FolderName => this.setFolderName(FolderName)}
                />
              </View>
            </ModalContent>
          </Modal>
          {/* end tạo thư mục */}

          {/* hiển thị dạng danh sách */}
          {this.state.showList === true
            ? this.CustomViewList(this.state.list)
            : // hiển thị dạng lưới
            this.state.showGrid === true
            ? this.CustomViewGrid(this.state.list)
            : null}
          {this.state.checkBox === true ? (
            <View style={AppStyles.StyleTabvarBottom}>
              <TabBarBottomCustom
                ListData={this.listtabbarBotom}
                onCallbackValueBottom={callback => this.onClickBottom(callback)}
              />
            </View>
          ) : null}
        </View>
        <MenuActionCompoment
          callback={this.actionMenuCallback}
          //data={this.state.selectItem.FileExtension === 'FOLDER' ? this.state.dataMenuFolder : this.state.dataMenuFile}
          data={this.state.dataMenuFile}
          name={this.state.NameMenu}
          eOpen={this.openMenu}
          position={'bottom'}
        />
        <MenuActionCompoment
          callback={this.actionMenuTopCallback}
          data={this.state.dataMenuTop}
          name={''}
          eOpen={this.openMenuTop}
          position={'bottom'}
        />
        <OpenPhotoLibrary
          callback={this.callbackLibarary}
          openLibrary={this.openLibrary}
        />
      </View>
    );
  }
  CustomViewList = data => (
    <FlatList
      style={{height: '100%'}}
      refreshing={this.state.isFetching}
      ref={ref => {
        this.ListView_Ref = ref;
      }}
      ListFooterComponent={this.renderFooter}
      ListEmptyComponent={this.ListEmpty}
      keyExtractor={item => item.AttachmentGuid}
      data={data}
      renderItem={({item, index}) => (
        <View>
          <ListItem
            style={{}}
            subtitle={() => {
              return (
                <View style={{flexDirection: 'row', marginTop: -20}}>
                  {this.state.checkBox === true ? (
                    <TouchableOpacity
                      style={{flex: 1}}
                      onPress={() => this.onCheckBox(item, index)}>
                      <CheckBox
                        checked={this.state.list[index].checked}
                        onPress={() => this.onCheckBox(item, index)}
                      />
                    </TouchableOpacity>
                  ) : null}
                  <View style={{flex: 1}}>
                    <Image
                      style={{width: '70%', height: 50}}
                      source={{
                        uri: this.state.FileAttackments.renderImage(
                          item.FileExtension,
                        ),
                      }}
                    />
                  </View>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Titledefault}>{item.FileName}</Text>
                    <Text style={AppStyles.Textdefault}>
                      {this.customDate(item.CreatedDate)}
                    </Text>
                    <Text style={AppStyles.Textdefault}>
                      Kích thước file: {this.ConFileSize(item.FileSize)}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{flex: 1}}
                    onPress={() => this.onAction(item)}>
                    <Icon
                      onPress={() => this.onAction(item)}
                      color="#888888"
                      name={'ellipsis1'}
                      type="antdesign"
                    />
                  </TouchableOpacity>
                </View>
              );
            }}
            bottomDivider
            onPress={() => this.onClick(item)}
          />
        </View>
      )}
      onEndReachedThreshold={0.5}
      onEndReached={({distanceFromEnd}) => {
        // if (this.state.staticParam.CurrentPage !== 1) {
        //   //this.nextPage();
        // }
      }}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={['red', 'green', 'blue']}
        />
      }
    />
  );
  //Menu Item
  actionMenuCallback = d => {
    if (d === 'delete') {
      if (this.props.notEdit === true) {
        Alert.alert(
          'Thông báo',
          'Bạn không có quyền chỉnh sửa file đính kèm',
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        return;
      }
      Alert.alert(
        'Thông báo',
        this.state.DataCheck.length > 0
          ? 'Bạn có muốn xóa những file này không?'
          : 'Bạn có muốn xóa ' + this.state.selectItem.FileName + ' không?',
        [
          {
            text: 'Xác nhận',
            onPress: () => this.deleteDocument(this.state.selectItem),
          },
          {text: 'Huỷ', onPress: () => console.log('No Pressed')},
        ],
        {cancelable: true},
      );
      //this.deleteDocument(this.state.selectItem);
    } else if (d === 'details') {
      this.openFile(this.state.selectItem);
    } else if (d === 'edit') {
      this.setState({
        showModalRenameFile: true,
        Title: this.state.selectItem.FileName,
      });
      if (this.state.selectItem.FileExtension === 'FOLDER') {
        this.setState({editName: 'Sửa thư mục'});
      } else {
        this.setState({editName: 'Sửa tệp tin'});
      }
      //this.updateFile(this.state.selectItem);
    } else if (d === 'download') {
      this.DownloadFile(this.state.selectItem);
    }
    console.log('Event: ', d);
  };
  _openMenu() {}
  openMenu = d => {
    this._openMenu = d;
  };
  onAction(item) {
    this.setState({
      selectItem: item,
    });
    var _option = {};
    if (item.FileExtension === 'FOLDER') {
      _option = {
        isChangeAction: true,
        data: this.state.dataMenuFolder,
        NameMenu: item.FileName,
      };
    } else {
      _option = {
        isChangeAction: true,
        data: this.state.dataMenuFile,
        NameMenu: item.FileName,
      };
    }
    this._openMenu(_option);
  }
  //Menu Top
  actionMenuTopCallback = d => {
    if (d === 'addfolder') {
      this.addFolder();
    } else if (d === 'upload') {
      this.choiseFile();
    } else if (d === 'uploadImg') {
      this.openPhotoLibrary();
    }
  };
  _openMenuTop() {}
  openMenuTop = d => {
    this._openMenuTop = d;
  };
  onActionTop(item) {
    if (this.props.notEdit === true) {
      Alert.alert(
        'Thông báo',
        'Bạn không có quyền chỉnh sửa file đính kèm',
        [{text: 'OK', onPress: () => Keyboard.dismiss()}],
        {cancelable: false},
      );
      return;
    }
    this._openMenuTop();
  }
  CustomViewGrid = data => (
    <FlatGrid
      itemDimension={130}
      items={this.state.list}
      style={{paddingRight: 5}}
      renderItem={({item, index}) => (
        <View style={{borderRadius: 5, backgroundColor: '#fff'}}>
          <View style={{flexDirection: 'row'}}>
            {this.state.checkBox === true ? (
              <TouchableOpacity
                style={{flex: 1}}
                onPress={() => this.onCheckBox(item, index)}>
                <CheckBox
                  checked={this.state.list[index].checked}
                  onPress={() => this.onCheckBox(item, index)}
                />
              </TouchableOpacity>
            ) : null}
            <TouchableOpacity
              style={{
                alignItems: 'flex-end',
                position: 'relative',
                width: '100%',
                flex: 1,
              }}
              onPress={() => this.onAction(item)}>
              <Icon
                color="#888888"
                name={'ellipsis1'}
                type="antdesign"
                onPress={() => this.onAction(item)}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={[styles.itemContainerdetail]}
            onPress={() => this.onClick(item)}>
            <Image
              style={{width: '45%', height: 80}}
              source={{
                uri: this.state.FileAttackments.renderImage(item.FileExtension),
              }}
            />
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 5}}>
                <Text style={[AppStyles.Titledefault, {textAlign: 'center'}]}>
                  {item.FileName.length > 12
                    ? item.FileName.substring(0, 10) + ' ...'
                    : item.FileName}
                </Text>
              </View>
            </View>
            <TouchableOpacity style={{}} onPress={() => this.onClick(item)}>
              <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                {this.convertDate(item.CreatedDate)}
              </Text>
              <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                {this.ConFileSize(item.FileSize)}
              </Text>
            </TouchableOpacity>
          </TouchableOpacity>
        </View>
      )}
    />
  );
  nextPage() {
    this.GetFiles();
  }
  //Load lại
  onRefresh = () => {
    this.setState({
      refreshing: true,
      list: [],
    });
    this.GetFiles();
  };
  setMenuTopRef = ref => {
    this._menuTop = ref;
  };
  hideMenuTop = () => {
    this._menuTop.hide();
  };
  showMenuTop = () => {
    this._menuTop.show();
  };
  addFolder = () => {
    //Actions.tmaddFolder();
    //this.hideMenuTop();
    this.setState({showModalAddFolder: true});
  };
  ChangeDropDownOrder(value) {
    //this.setState({ list: [] })
    if (value === 'Name') {
      //this.state.staticParam.QueryOrderBy = value;
      this.setState({
        selected: value,
        list: [],
      });
      this.GetFiles();
    } else if (value === 'Date') {
      //this.state.staticParam.QueryOrderBy = value;
      this.setState({
        selected: value,
        list: [],
      });
      this.GetFiles();
    } else if (value === 'Size') {
      //this.state.staticParam.QueryOrderBy = value;
      this.setState({
        selected: value,
        list: [],
      });
      this.GetFiles();
    }
  }
  onClick(obj) {
    if (obj.FileExtension === 'FOLDER') {
      this.openFolder(obj);
    } else {
      this.openFile(obj);
    }
  }
  ChangeOrder(data) {
    if (data === 'list') {
      this.setState({
        showGrid: false,
        showList: true,
      });
    } else if (data === 'grid') {
      this.setState({
        showList: false,
        showGrid: true,
      });
    }
  }
  openFolder = obj => {
    this.state.list = [];
    this.state.ParentGuid = obj.AttachmentGuid;
    this.state.ChoiseFolder.push(obj);
    this.setState({
      ParentGuid: obj.AttachmentGuid,
    });
    this.GetFiles();
  };
  BackFolder = (id, position) => {
    this.state.list = [];
    this.state.ParentGuid = id;
    this.setState({
      ParentGuid: this.state.ParentGuid,
    });
    this.state.ChoiseFolder.splice(
      position + 1,
      this.state.ChoiseFolder.length - 1,
    );
    this.GetFiles();
  };
  //upload file
  async choiseFile() {
    try {
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.lstFiles;
      //_liFiles.push(res);
      for (let index = 0; index < res.length; index++) {
        _liFiles.push(res[index]);
      }
      this.setState({
        lstFiles: _liFiles,
        EventBoxIcon: false,
        //showModalListFile: true
      });
      this.uploadFile();
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
    //this.setState({ showModalListFile: true });
  }
  removeAttactment(para) {
    var list = this.state.lstFiles;
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].name != para.name) {
        listNewValue.push(list[i]);
      }
    }
    this.setState({
      lstFiles: listNewValue,
    });
  }
  uploadFile = () => {
    let _obj = {
      ModuleId: this.state.ModuleId,
      ParentGuid: this.state.ParentGuid,
      RecordGuid: this.state.RecordGuid,
    };
    let fd = new FormData();
    for (var i = 0; i < this.state.lstFiles.length; i++) {
      fd.append(this.state.lstFiles[i].name, this.state.lstFiles[i]);
    }
    fd.append('insert', JSON.stringify(_obj));
    API_Attachment.Insert(fd)
      .then(res => {
        this.setState({
          list: [],
          lstFiles: [],
        });
        this.GetFiles();
      })
      .catch(error => {});
  };
  //insert Folder
  setFolderName(FolderName) {
    this.setState({FolderName: FolderName});
  }
  SubmitAddFolder = () => {
    API_Attachment.InsertFolder({
      ModuleId: this.state.ModuleId,
      Title: this.state.FolderName,
      ParentGuid: this.state.ParentGuid,
      RecordGuid: this.state.RecordGuid,
    })
      .then(res => {
        this.setState({
          showModalAddFolder: false,
          list: [],
          FolderName: '',
        });
        this.GetFiles();
      })
      .catch(error => {
        this.setState({
          showModalAddFolder: false,
        });
        console.log(error);
      });
  };
  //begin chức năng item
  openFile = obj => {
    var newData = obj;
    if (obj.CreatedBy !== null && obj.CreatedBy !== undefined) {
      newData.CreatedBy = obj.CreatedBy.split('#')[1];
      this.setState();
    }
    this.setState({ItemFile: newData});
    this.setState({showModalOpenFile: true});
  };
  //update
  setTitle(Title) {
    this.setState({Title: Title});
  }
  updateFile = obj => {
    API_Attachment.updateFile({Title: this.state.Title})
      .then(res => {
        this.setState({showModalRenameFile: false});
      })
      .catch(error => {
        this.setState({showModalRenameFile: false});
        console.log(error);
      });
  };
  deleteDocument = obj => {
    if (this.state.DataCheck.length > 0) {
      for (let i = 0; i < this.state.DataCheck.length; i++) {
        API_Attachment.Delete({
          ModuleId: this.state.ModuleId,
          AttachmentGuid: this.state.DataCheck[i].AttachmentGuid,
        })
          .then(res => {
            if (res.data.errorCode === 200) {
              console.log(res);
              this.GetFiles();
              Toast.showWithGravity(`Thành công`, Toast.SHORT, Toast.CENTER);
            } else {
              Toast.showWithGravity(`Có lỗi`, Toast.SHORT, Toast.CENTER);
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    } else {
      API_Attachment.Delete({
        ModuleId: this.state.ModuleId,
        AttachmentGuid: obj.AttachmentGuid,
      })
        .then(res => {
          if (res.data.errorCode === 200) {
            console.log(res);
            this.GetFiles();
            Toast.showWithGravity(`Thành công`, Toast.SHORT, Toast.CENTER);
          } else {
            Toast.showWithGravity(`Có lỗi`, Toast.SHORT, Toast.CENTER);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setState({
      list: [],
      DataCheck: [],
    });
    this.GetFiles();
  };
  //end chức năng item
  //tải file đính kèm về
  DownloadFile(attachObject) {
    if (this.state.DataCheck.length > 0) {
      for (let i = 0; i < this.state.DataCheck.length; i++) {
        var val = this.state.DataCheck[i];
        let dirs = RNFetchBlob.fs.dirs;
        if (Platform.OS !== 'ios') {
          RNFetchBlob.config({
            addAndroidDownloads: {
              useDownloadManager: true, // <-- this is the only thing required
              // Optional, override notification setting (default to true)
              notification: true,
              // Optional, but recommended since android DownloadManager will fail when
              // the url does not contains a file extension, by default the mime type will be text/plain
              mime: '/',
              description: 'File downloaded by download manager.',
              //title: new Date().toLocaleString() + ' - test.xlsx',
              //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
              path: 'file://' + dirs.DownloadDir + '/' + val.FileName, //using for android
            },
          })
            .fetch(
              'get',
              configApp.url_Attachment_Download +
                this.state.ModuleId +
                '/' +
                val.AttachmentGuid,
            )
            .then(resp => {
              // the path of downloaded file
              resp.path();
            });
        } else {
          RNFetchBlob.config({
            path: 'file://' + dirs.DownloadDir + '/' + val.FileName, //using for android
            addAndroidDownloads: {
              useDownloadManager: true, // <-- this is the only thing required
              // Optional, override notification setting (default to true)
              notification: true,
              IOSDownloadTask: true,
              // Optional, but recommended since android DownloadManager will fail when
              // the url does not contains a file extension, by default the mime type will be text/plain
              mime: '/',
              description: 'File downloaded by download manager.',
              path:
                dirs.DocumentDir +
                '/' +
                new Date().toLocaleString() +
                '-' +
                val.FileName, //using for ios
            },
          })
            .fetch(
              'get',
              configApp.url_Attachment_Download +
                this.state.ModuleId +
                '/' +
                val.AttachmentGuid,
            )
            .then(resp => {
              RNFetchBlob.ios.openDocument(resp.data);
            });
        }
      }
      for (let j = 0; j < this.state.list.length; j++) {
        var val = this.state.list[j];
        if (val.checked === true) {
          val.checked = false;
        }
      }
      this.setState({
        DataCheck: [],
        //checkBox: false
        list: this.state.list,
      });
    } else if (attachObject) {
      let dirs = RNFetchBlob.fs.dirs;
      if (Platform.OS !== 'ios') {
        RNFetchBlob.config({
          addAndroidDownloads: {
            useDownloadManager: true, // <-- this is the only thing required
            // Optional, override notification setting (default to true)
            notification: true,
            // Optional, but recommended since android DownloadManager will fail when
            // the url does not contains a file extension, by default the mime type will be text/plain
            mime: '/',
            description: 'File downloaded by download manager.',
            //title: new Date().toLocaleString() + ' - test.xlsx',
            //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
            path: 'file://' + dirs.DownloadDir + '/' + attachObject.FileName, //using for android
          },
        })
          .fetch(
            'get',
            configApp.url_Attachment_Download +
              this.state.ModuleId +
              '/' +
              attachObject.AttachmentGuid,
          )
          .then(resp => {
            // the path of downloaded file
            resp.path();
          });
      } else {
        RNFetchBlob.config({
          path: 'file://' + dirs.DownloadDir + '/' + attachObject.FileName, //using for android
          addAndroidDownloads: {
            useDownloadManager: true, // <-- this is the only thing required
            // Optional, override notification setting (default to true)
            notification: true,
            IOSDownloadTask: true,
            // Optional, but recommended since android DownloadManager will fail when
            // the url does not contains a file extension, by default the mime type will be text/plain
            mime: '/',
            description: 'File downloaded by download manager.',
            path:
              dirs.DocumentDir +
              '/' +
              new Date().toLocaleString() +
              '-' +
              attachObject.FileName, //using for ios
          },
        })
          .fetch(
            'get',
            configApp.url_Attachment_Download +
              this.state.ModuleId +
              '/' +
              attachObject.AttachmentGuid,
          )
          .then(resp => {
            RNFetchBlob.ios.openDocument(resp.data);
          });
      }
    } else {
      return;
    }
  }
  onCheckBox(para, index) {
    var _list = this.state.list;
    _list[index].checked = !_list[index].checked;
    var _dataCheck = [];
    for (let i = 0; i < _list.length; i++) {
      if (_list[i].checked === true) {
        _dataCheck.push(_list[i]);
      }
    }
    this.setState({
      DataCheck: _dataCheck,
    });
  }
  //Chức năng xóa nhiều, tải nhiều
  onClickBottom(data) {
    if (data == 'delete') {
      if (this.props.notEdit === true) {
        Alert.alert(
          'Thông báo',
          'Bạn không có quyền chỉnh sửa file đính kèm',
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        return;
      }
      if (this.state.list && this.state.list.length === 0) {
        return;
      }
      Alert.alert(
        'Thông báo',
        this.state.DataCheck.length > 0
          ? 'Bạn có muốn xóa những file này không?'
          : 'Bạn có muốn xóa ' + this.state.selectItem.FileName + ' không?',
        [
          {
            text: 'Xác nhận',
            onPress: () => this.deleteDocument(this.state.selectItem),
          },
          {text: 'Huỷ', onPress: () => console.log('No Pressed')},
        ],
        {cancelable: true},
      );
    } else if (data == 'download') {
      this.DownloadFile();
    }
  }
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      // if (dd < 10) {
      //     dd = '0' + dd;
      // }
      // if (mm < 10) {
      //     mm = '0' + mm;
      // }
      date = 'Ngày ' + dd + ' tháng ' + mm + ' năm ' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  convertDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      var dd = date.getDate();
      var MM = date.getMonth() + 1; //January is 0!
      var yyyy = date.getFullYear();
      var hh = date.getHours();
      var mm = date.getMinutes();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (MM < 10) {
        MM = '0' + MM;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (hh < 10) {
        hh = '0' + hh;
      }
      date = dd + '/' + MM + '/' + yyyy + ' ' + hh + ':' + mm;
      return date.toString();
    } else {
      return '';
    }
  }
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  ConFileSize(d) {
    var _size = ' KB';
    try {
      if (d === 0 || d === null || d === undefined) {
        _size = '';
      } else {
        _size = parseFloat(d / 1024).toFixed(2) + _size;
      }
    } catch (ex) {
      _size = '';
    }
    return _size;
  }
  ConDate = (data, number) => {
    if (data == null || data == '') {
      return '';
    }
    if (data !== null && data != '' && data != undefined) {
      try {
        if (data.indexOf('SA') != -1 || data.indexOf('CH') != -1) {
          if (data.indexOf('SA') != -1) {
          }
          if (data.indexOf('CH') != -1) {
          }
        }

        if (data.indexOf('Date') != -1) {
          data = data.substring(data.indexOf('Date') + 5);
          data = parseInt(data);
        }
      } catch (ex) {}
      var newdate = new Date(data);
      if (number == 3) {
        return newdate;
      }
      if (number == 2) {
        return '/Date(' + newdate.getTime() + ')/';
      }
      var month = newdate.getMonth() + 1;
      var day = newdate.getDate();
      var year = newdate.getFullYear();
      var hh = newdate.getHours();
      var mm = newdate.getMinutes();
      if (month < 10) month = '0' + month;
      if (day < 10) day = '0' + day;
      if (mm < 10) mm = '0' + mm;
      if (number == 0) {
        return (todayDate = day + '/' + month + '/' + year);
      }
      if (number == 1) {
        return (todayDate =
          day + '/' + month + '/' + year + ' ' + hh + ':' + mm);
      }
      if (number == 4) {
        return new Date(year, month - 1, day);
      }
    } else {
      return '';
    }
  };
  //upload images
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    var _liFiles = this.state.lstFiles;
    for (let index = 0; index < rs.length; index++) {
      _liFiles.push(rs[index]);
    }
    this.setState({
      lstFiles: _liFiles,
      EventBoxIcon: false,
    });
    this.uploadFile();
  };
}
const styles = StyleSheet.create({
  itemContainerdetail: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 17,
    flexDirection: 'column',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  boxBottom_Full: {
    flex: 1,
    width: '100%',
    position: 'absolute',
    bottom: 0,
    borderTopColor: '#eff0f1',
    borderTopWidth: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  hidden: {
    display: 'none',
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AttachmentComponent);
