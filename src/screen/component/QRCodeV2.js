import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Dimensions, Text, TouchableOpacity, Alert } from "react-native";
import QRCodeScanner from "react-native-qrcode-scanner";
import * as Animatable from "react-native-animatable";
import { Actions } from 'react-native-router-flux';
import { RNCamera } from 'react-native-camera';
import { Divider, Icon, Header } from 'react-native-elements';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency

const rectDimensions = SCREEN_WIDTH * 0.65; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.005; // this is equivalent to 2 from a 393 device width
const rectBorderColor = "red";

const scanBarWidth = SCREEN_WIDTH * 0.46; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const scanBarColor = "#22ff00";

const iconScanColor = "blue";
console.disableYellowBox = true;
const styles = {
    rectangleContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },
    rectangle: {
        height: rectDimensions,
        width: rectDimensions,
        borderWidth: 0.5,
        borderColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },
    topOverlay: {
        flex: 1,
        height: SCREEN_WIDTH,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor,
        justifyContent: "center",
        alignItems: "center"
    },
    bottomOverlay: {
        flex: 1,
        height: SCREEN_WIDTH,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor,
        paddingBottom: SCREEN_WIDTH * 0.25
    },
    leftAndRightOverlay: {
        height: SCREEN_WIDTH * 0.65,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor
    },
    scanBar: {
        width: scanBarWidth,
        height: scanBarHeight,
        backgroundColor: scanBarColor
    }
};
class QRCodeV2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListValue: [],
            type: "single",// or single or multiple
            Link: props.link
        };
    }
    componentDidMount() {
        if (this.props.typeQR !== undefined && this.props.typeQR == "multiple") {
            this.setState({ type: "multiple" })
        }
    }
    makeSlideOutTranslation(translationType, fromValue) {
        return {
            from: {
                [translationType]: SCREEN_HEIGHT * -0.01
            },
            to: {
                [translationType]: fromValue
            }
        };
    }
    onSuccess = value => {
        Actions.pop();
        Actions.refresh({ moduleId: 'QRCodeV2', Data: { Value: value, Link: this.state.Link }, ActionTime: (new Date()).getTime() });
        // Linking.openURL( ).catch(err =>
        //     console.error('Không tìm thấy đường dẫn', err)
        // );
    };
    onSuccessMultiple = value => {
        var _list = this.state.ListValue;
        _list.push(value);
        Alert.alert("Thông báo", "Đã lưu thông tin mã QR",
            [
                { text: 'Đóng', onPress: () => this.scanner.reactivate() },
            ],
        )

        console.log(this.state.ListValue)
    }
    SaveMultiple = () => {
        Actions.pop();
        Actions.refresh({ moduleId: 'QRCode', Data: { Value: this.state.ListValue }, ActionTime: (new Date()).getTime() });
    }
    backToList() {
        Actions.pop();
        Actions.refresh({ moduleId: 'Back', ActionTime: (new Date()).getTime() });
    }
    render() {
        return (
            this.state.type == "single" ? this.Single() : this.Multiple()
        );
    }
    Single = () => {
        return (
            <View>
                <Header
                    {...AppStyles.headerImage}
                    leftComponent={
                        <TouchableOpacity style={{ flex: 1, width: 45, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.backToList()}>
                            <Icon name="close" type="antdesign" color={"#fff"} size={16} />
                        </TouchableOpacity>
                    }
                    centerComponent={<View style={{ flex: 8, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', color: '#fff' }}>QUÉT QR CODE</Text></View>}
                />

                <QRCodeScanner

                    showMarker
                    onRead={this.onSuccess.bind(this)}
                    cameraStyle={{ height: SCREEN_HEIGHT }}
                    customMarker={
                        <View style={[styles.rectangleContainer]}>
                            <View style={styles.topOverlay} />
                            <View style={{ flexDirection: "row" }}>
                                <View style={styles.leftAndRightOverlay} />
                                <View style={styles.rectangle}>
                                    {/* <Animatable.View
                                            style={styles.scanBar}
                                            direction="alternate-reverse"
                                            iterationCount="infinite"
                                            duration={1000}
                                            easing="linear"
                                        // animation={this.makeSlideOutTranslation(
                                        //     "translateY",
                                        //     SCREEN_WIDTH * -0.55
                                        // )}
                                        /> */}
                                </View>
                                <View style={styles.leftAndRightOverlay} />
                            </View>
                            <View style={[styles.bottomOverlay]}>
                                <View style={[{ flex: 1, paddingLeft: 60, paddingRight: 60, paddingTop: 10 }]}>
                                    <Text style={{ color: '#fff', textAlign: 'center' }}>Di chuyển camera đến vùng chứa mã QR để quét</Text>
                                </View>
                            </View>
                        </View>
                    }
                />
            </View>
        )
    }
    Multiple = () => {
        return (
            <View>
                <Header
                    {...AppStyles.headerImage}
                    leftComponent={
                        <TouchableOpacity style={{ flex: 1, width: 45, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.backToList()}>
                            <Icon name="close" type="antdesign" color={"#fff"} size={16} />
                        </TouchableOpacity>
                    }
                    centerComponent={<View style={{ flex: 8, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', color: '#fff' }}>QUÉT QR CODE</Text></View>}
                    rightComponent={
                        <TouchableOpacity style={{ flex: 1, width: 45, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.SaveMultiple()}>
                            <Text style={{ color: "#2196f3", fontSize: 16 }}>Xong</Text>
                        </TouchableOpacity>
                    }
                />

                <QRCodeScanner
                    reactivate={false}
                    ref={(node) => { this.scanner = node }}
                    containerStyle={{ flex: 1 }}
                    showMarker
                    onRead={this.onSuccessMultiple.bind(this)}
                    cameraStyle={{ height: SCREEN_HEIGHT }}
                    customMarker={
                        <View style={[styles.rectangleContainer]}>
                            <View style={styles.topOverlay} />
                            <View style={{ flexDirection: "row" }}>
                                <View style={styles.leftAndRightOverlay} />
                                <View style={styles.rectangle}>
                                </View>
                                <View style={styles.leftAndRightOverlay} />
                            </View>
                            <View style={[styles.bottomOverlay]}>
                                <View style={[{ flex: 1, paddingLeft: 60, paddingRight: 60, paddingTop: 10 }]}>
                                    <Text style={{ color: '#fff', textAlign: 'center' }}>Di chuyển camera đến vùng chứa mã QR để quét</Text>
                                </View>
                            </View>
                        </View>
                    }
                />
            </View>
        )
    }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true },)(QRCodeV2);
