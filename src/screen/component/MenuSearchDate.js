import React, { Component, useRef, useEffect } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge, Divider } from "react-native-elements";
import { Text, View, Animated, Dimensions, Easing, TouchableOpacity, StyleSheet, ScrollView, KeyboardAvoidingView } from "react-native";
import { Directions } from 'react-native-gesture-handler';
import { AppStyles, AppColors } from '@theme';
import { Actions } from 'react-native-router-flux';
import FormSearch from './FormSearch';
import { stubFalse } from 'lodash';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
}
class MenuSearchDate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valueSelect: (props.value !== undefined && props.value !== null) ? props.value : null,//giá trị mặc định
            offsetY: new Animated.Value(12 * 50 + 30),
            isOpen: false,
            height: 12 * 50 + 30,
            dataSubmit: null,
            DataMenuSearchDay: [
                {
                    value: '1',
                    text: 'Năm hiện tại',
                    icon: {
                        name: 'eye-outline',
                        type: 'material-community',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '2',
                    text: 'Quý hiện tại',
                    icon: {
                        name: 'edit',
                        type: 'antdesign',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '3',
                    text: 'Tháng hiện tại',
                    icon: {
                        name: 'delete',
                        type: 'antdesign',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '4',
                    text: '30 ngày trước',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '5',
                    text: '30 ngày sau',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                // {
                //     value: '6',
                //     text: 'Tuần hiện tại',
                //     icon: {
                //         name: 'attachment',
                //         type: 'entypo',
                //         color: AppColors.Maincolor,
                //     }
                // },
                {
                    value: '7',
                    text: 'Ngày hiện tại',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '8',
                    text: 'Năm trước',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '9',
                    text: 'Quý trước',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '10',
                    text: 'Tháng trước',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '11',
                    text: 'Tháng sau',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '12',
                    text: 'Tuần sau',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                },
                {
                    value: '13',
                    text: 'Ngày trước',
                    icon: {
                        name: 'attachment',
                        type: 'entypo',
                        color: AppColors.Maincolor,
                    }
                }
            ]
        };

        props.eOpen(this.eOpen);
        this.actionClick = props.callback;
    }
    actionClick() { }
    componentDidMount() {
        this.ae(this.state.DataMenuSearchDay, this.state.valueSelect);
    }

    //#region Xử lý dữ liệu truyền vào
    ae(d, value) {
        var obj = d.find(x => x.value === value);
        if (obj !== undefined) {
            this.SaveValues(obj);
        }
        // var itemselect = null
        // if (value !== null) {
        //     for (var i = 0; i < d.length; i++) {
        //         if (value == d[i].value) {
        //             d[i].check = true;
        //             itemselect = d[i]
        //         } else {
        //             d[i].check = false
        //         }
        //     }
        // } else {
        //     for (var i = 0; i < d.length; i++) {
        //         d[i].check = false
        //         itemselect = { text: "", value: null }
        //     }
        // }
        // this.setState({ dataView: d, dataSubmit: itemselect })
        // this.actionClick(itemselect);
    }
    //#endregion

    //#region  đóng cửa sổ
    eOpen = (option) => {
        if (option) {
            if (option.isChangeAction) {
                this.ae(option.data);
                this.setState({
                    data: option.data,
                })
            }
            if (option.NameMenu !== undefined && option.NameMenu !== null) {
                this.setState({
                    NameMenu: option.NameMenu,
                })
            }
        }
        this.openAction();
        for (let i = 0; i < this.state.DataMenuSearchDay.length; i++) {
            this.state.DataMenuSearchDay[i].check = false;
            if (this.props.value === this.state.DataMenuSearchDay[i].value) {
                this.state.DataMenuSearchDay[i].check = true;
            }
        }
        this.setState((prevState) => {
            return {
                ...prevState,
                isOpen: !prevState.isOpen,
                EvenFromSearch: false,
                DataMenuSearchDay: this.state.DataMenuSearchDay
            }
        }
        )

    }
    //#endregion
    //#region mở cửa sổ
    openAction = () => {
        if (!this.state.isOpen) {
            Animated.timing(
                this.state.offsetY,
                {
                    toValue: 0,
                    duration: 800,
                    useNativeDriver: true,
                }
            ).start();
        } else {
            Animated.timing(
                this.state.offsetY,
                {
                    toValue: this.state.height,
                    duration: 800,
                    useNativeDriver: true,
                }
            ).start();
        }
    }
    //#endregion

    //#region chọn 
    GetWeekObject = (d) => {
        d = new Date(d);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
        var _data = {};
        _data.StartDate = new Date(d.setDate(diff));
        _data.EndDate = this.addDays(_data.StartDate, 6);
        return _data;
    }
    daysInMonth = (year, month) => {
        return new Date(year, month, 0).getDate();
    }
    addDays = (_date, days) => {
        var date = new Date(_date);
        date.setDate(date.getDate() + days);
        return date;
    }
    GetMonthObject = (d) => {
        var _data = {};
        _data.StartDate = new Date(d.getFullYear(), d.getMonth(), 1);   //new DateTime(year, month, 1);
        _data.EndDate = new Date(d.getFullYear(), d.getMonth(), this.daysInMonth(d.getFullYear(), d.getMonth() + 1)); //new DateTime(year, month, DateTime.DaysInMonth(year, month));
        return _data;
    }
    getThisQuarter = (_tem) => {
        var _i = (new Date()).getMonth() + 1;
        var _d = [];
        if (1 <= _i && _i <= 3) {
            if (_tem === 1) {
                _d[0] = new Date((new Date()).getFullYear() - 1, 9, 1);
                _d[1] = new Date((new Date()).getFullYear() - 1, 12, 0);
            } else {
                _d[0] = new Date((new Date()).getFullYear(), 0, 1);
                _d[1] = new Date((new Date()).getFullYear(), 3, 0);
            }
            return _d;
        }
        if (4 <= _i && _i <= 6) {
            if (_tem === 1) {
                _d[0] = new Date((new Date()).getFullYear(), 0, 1);
                _d[1] = new Date((new Date()).getFullYear(), 3, 0);
            } else {
                _d[0] = new Date((new Date()).getFullYear(), 3, 1);
                _d[1] = new Date((new Date()).getFullYear(), 6, 0);
            }
            return _d;
        }
        if (7 <= _i && _i <= 9) {
            if (_tem === 1) {
                _d[0] = new Date((new Date()).getFullYear(), 3, 1);
                _d[1] = new Date((new Date()).getFullYear(), 6, 0);
            } else {
                _d[0] = new Date((new Date()).getFullYear(), 6, 1);
                _d[1] = new Date((new Date()).getFullYear(), 9, 0);
            }
            return _d;
        }
        if (10 <= _i && _i <= 12) {
            if (_tem === 1) {
                _d[0] = new Date((new Date()).getFullYear(), 6, 1);
                _d[1] = new Date((new Date()).getFullYear(), 9, 0);
            } else {
                _d[0] = new Date((new Date()).getFullYear(), 9, 1);
                _d[1] = new Date((new Date()).getFullYear(), 12, 0);
            }
            return _d;
        }
    }
    SaveValues = (item) => {
        var _submit = null;
        var _data = this.state.DataMenuSearchDay;
        for (var i = 0; i < _data.length; i++) {
            if (_data[i].value == item.value) {
                switch (item.value) {
                    case "1":
                        _data[i].start = new Date((new Date()).getFullYear(), 0, 1);
                        _data[i].end = new Date((new Date()).getFullYear(), 12, 0);
                        break;
                    case "2":
                        var _x = this.getThisQuarter();
                        _data[i].start = _x[0];
                        _data[i].end = _x[1];
                        break;
                    case "3":
                        _data[i].start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), 1);
                        _data[i].end = new Date((new Date()).getFullYear(), (new Date()).getMonth() + 1, 0);
                        break;
                    case "4":
                        _data[i].start = new Date((new Date()).getFullYear(), (new Date()).getMonth() - 1, (new Date()).getDate());
                        _data[i].end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
                        break;
                    case "5":
                        _data[i].start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
                        _data[i].end = new Date((new Date()).getFullYear(), (new Date()).getMonth() + 1, (new Date()).getDate(), 23, 59, 59);
                        break;
                    // case "6":
                    //     var _x = (new Date()).getWeek() - 1;
                    //     for (var y = 0; y < scope.liWeek.length; y++) {
                    //         if (scope.liWeek[y].Value === _x) {
                    //             _data[i].start = scope.liWeek[y].StartDate;
                    //             _data[i].end = scope.liWeek[y].EndDate;
                    //             break;
                    //         }
                    //     }
                    //     break;
                    case "7":
                        _data[i].start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
                        _data[i].end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 23, 59, 59);
                        break;
                    case "8":
                        _data[i].start = new Date((new Date()).getFullYear() - 1, 0, 1);
                        _data[i].end = new Date((new Date()).getFullYear(), 0, 0);
                        break;
                    case "9":
                        var _x = this.getThisQuarter(1);
                        _data[i].start = _x[0];
                        _data[i].end = _x[1];
                        break;
                    case "10":
                        _data[i].start = new Date((new Date()).getFullYear(), (new Date()).getMonth() - 1, 1);
                        _data[i].end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), 0);
                        break;
                    case "11":
                        var _x = new Date((new Date()).getFullYear(), (new Date()).getMonth() + 1);
                        _data[i].start = this.GetMonthObject(_x).StartDate;
                        _data[i].end = this.GetMonthObject(_x).EndDate;
                        break;
                    case "12":
                        var _x = this.addDays(new Date(), 6);
                        _data[i].start = this.GetWeekObject(_x).StartDate;
                        _data[i].end = this.GetWeekObject(_x).EndDate;
                        break;
                    case "13":
                        _data[i].start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1);
                        _data[i].end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1, 23, 59, 59);
                        break;
                    default:
                        break;
                }
                _data[i].check = true;
                _submit = _data[i]
            } else (
                _data[i].check = false
            )
        }
        this.setState({ dataSubmit: _submit, DataMenuSearchDay: _data, })
        this.actionClick(_submit);
    }
    Change = (val) => {
        this.SaveValues(val);
        this.eOpen();
    }
    //#endregion

    render() {
        return (
            this.state && this.state.isOpen ?
                <TouchableOpacity style={[styles.positionBottom,
                {
                    width: DRIVER.width,
                    position: "absolute",
                    backgroundColor: 'rgba(52, 52, 52, 0.5)',
                    height: DRIVER.height,
                }]}
                    onPress={() => { this.eOpen() }}>
                    <View style={[{ bottom: 0, position: 'absolute', width: '100%' }]}>
                        <Animated.View style={{
                            backgroundColor: '#CED0CE',
                            transform: [{ translateY: this.state.offsetY }],
                            borderRadius: 10,
                            bottom: 0
                        }}>
                            <KeyboardAvoidingView
                                behavior={Platform.OS === "ios" ? "padding" : null}
                                style={{ flex: 1 }}
                            >
                                <View style={{
                                    flexDirection: 'column',
                                    backgroundColor: 'white',
                                    borderTopColor: '#CED0CE',
                                    borderTopWidth: 1,
                                    bottom: 0,
                                    maxHeight: DRIVER.height / 2,
                                    borderRadius: 10, opacity: 1
                                }}>
                                    <View>
                                        <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => { this.eOpen() }}>
                                            <View style={{ flex: 1 }}>
                                                <Icon color={AppColors.gray} name={'close'} type='antdesign' />
                                            </View>
                                            <View style={{ flex: 6 }}>
                                                <Text style={{ fontWeight: 'bold' }}>
                                                    Lọc ngày
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                        <Divider />
                                    </View>
                                    <ScrollView style={{ padding: 10, marginBottom: 20 }}
                                        ref={(ref) => {
                                            this.myScroll = ref;
                                        }}
                                    >
                                        {this.state.DataMenuSearchDay.map((item, i) => {
                                            return (
                                                <TouchableOpacity key={i}
                                                    onPress={() => { this.Change(item) }}
                                                >
                                                    <View style={{ flexDirection: 'row', paddingTop: 10, paddingBottom: 10, borderBottomColor: '#eff0f1', borderBottomWidth: 1, height: 40 }}>
                                                        <View style={{ flex: 10, justifyContent: 'center' }}>
                                                            <Text>
                                                                {item.text}
                                                            </Text>
                                                        </View>
                                                        <View style={{ flex: 1, justifyContent: 'center' }}>
                                                            {item.check === true ?
                                                                <Icon color={AppColors.gray} name="checkcircle" type="antdesign" color={AppColors.ColorEdit} size={20} />
                                                                : null}
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        })
                                        }
                                    </ScrollView>
                                </View>
                            </KeyboardAvoidingView>
                        </Animated.View>

                    </View>
                </TouchableOpacity>
                : null
        );
    }
}
const styles = StyleSheet.create({
    positionTop: {
        top: 80
    },
    positionBottom: {
        bottom: -10
    }
})
export default MenuSearchDate;