import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import TabNavigator from 'react-native-tab-navigator';
import Icon from 'react-native-vector-icons/AntDesign';
import { API } from '../../network';
import ViewOrderComponent from '../SM/Orders/ViewOrderComponent';
import Dialog from 'react-native-dialog';

const styles = StyleSheet.create({
  tabNavigatorItem: {
    marginTop: 10,
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
});

class MenuformBottomCompoment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: '',
      open: false,
      Visible_D: false,
      Visible_T: false,
    };
  }
  setSelectedTab = tab => {
    this.setState({ selectedTab: tab });
  };
  // Duyệt phiếu
  showDialog_D = () => {
    this.setState({ Visible_D: true });
  };
  handleCancel_D = () => {
    this.setState({ Visible_D: false });
  };
  // trả lại
  showDialog_T = () => {
    this.setState({ Visible_T: true });
  };
  handleCancel_T = () => {
    this.setState({ Visible_T: false });
  };
  render() {
    const { backListByKey, ItemID, Title } = this.props;
    return (
      <View
        style={{ position: 'absolute', bottom: 0, height: 60, width: '100%' }}>
        <TabNavigator
          tabBarStyle={{
            height: 60,
            borderTopWidth: 1,
            borderTopColor: '#f6b801',
          }}>
          <TabNavigator.Item
            tabStyle={styles.tabNavigatorItem}
            selected={this.state.selectedTab === 'home'}
            title={'Trang chủ'}
            renderIcon={() => <Icon style={styles.itemIcon} name="home" />}
            renderSelectedIcon={() => (
              <Icon style={styles.itemIconClick} name="home" />
            )}
            onPress={() => this.onPressHome('home')}
          />
          <TabNavigator.Item
            tabStyle={styles.tabNavigatorItem}
            selected={this.state.selectedTab === 'profile'}
            title={'Danh sách'}
            renderIcon={() => <Icon style={styles.itemIcon} name="profile" />}
            renderSelectedIcon={() => (
              <Icon style={styles.itemIconClick} name="profile" />
            )}
            onPress={() => this.onPressbackList(backListByKey)}
          />
          <TabNavigator.Item
            tabStyle={styles.tabNavigatorItem}
            selected={this.state.selectedTab === 'checkcircleo'}
            title={'Duyệt phiếu'}
            renderIcon={() => (
              <Icon style={styles.itemIcon} name="checkcircleo" />
            )}
            renderSelectedIcon={() => (
              <Icon style={styles.itemIconClick} name="checkcircleo" />
            )}
            onPress={() => this.onPressBrowser(ItemID)}
          />
          <TabNavigator.Item
            tabStyle={styles.tabNavigatorItem}
            selected={this.state.selectedTab === 'export2'}
            title={'Trả lại'}
            renderIcon={() => <Icon style={styles.itemIcon} name="export2" />}
            renderSelectedIcon={() => (
              <Icon style={styles.itemIconClick} name="export2" />
            )}
            onPress={() => this.onPressReturn(ItemID)}
          />
          <TabNavigator.Item
            tabStyle={styles.tabNavigatorItem}
            selected={this.state.selectedTab === 'message1'}
            title={'Ý kiến'}
            renderIcon={() => <Icon style={styles.itemIcon} name="message1" />}
            renderSelectedIcon={() => (
              <Icon style={styles.itemIconClick} name="message1" />
            )}
          />
        </TabNavigator>
        {/*/!*Duyệt phiếu*!/*/}
        <View>
          <Dialog.Container visible={this.state.Visible_D}>
            <Dialog.Title>Duyệt phiếu: {Title}</Dialog.Title>
            <Dialog.Input
              style={{ borderBottomWidth: 1, borderBottomColor: '#3366CC' }}
              label="Ý kiến:"
            />
            <Dialog.Button
              style={{ color: '#3366CC' }}
              label="Duyệt"
              onPress={this.handleCancel_D}
            />
            <Dialog.Button
              style={{ color: 'red' }}
              label="Hủy"
              onPress={this.handleCancel_D}
            />
          </Dialog.Container>
        </View>
        {/*trả lại*/}
        <View>
          <Dialog.Container visible={this.state.Visible_T}>
            <Dialog.Title>Trả lại phiếu: {Title}</Dialog.Title>
            <Dialog.Input
              style={{ borderBottomWidth: 1, borderBottomColor: '#3366CC' }}
              label="Ý kiến:"
            />
            <Dialog.Button
              style={{ color: '#3366CC' }}
              label="Trả lại"
              onPress={this.handleCancel_T}
            />
            <Dialog.Button
              style={{ color: 'red' }}
              label="Hủy"
              onPress={this.handleCancel_T}
            />
          </Dialog.Container>
        </View>
      </View>
    );
  }
  onPressHome() {
    Actions.app();
  }
  onPressbackList(item) {
    Actions.popTo(item);
  }
  onPressBrowser(item) {
    this.showDialog_D();
    // Actions.applyOutsidesItem
  }
  onPressReturn(item) {
    this.showDialog_T();
  }
  sendNotification = data => {
    let headers = {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: "Basic 'NWE5MmJhZjYtNjE3Ni00YTBkLWIyYmQtMTViY2NhMDM0MmI3'",
    };

    let endpoint = 'https://onesignal.com/api/v1/notifications';

    let params = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        app_id: '3459ec28-1ef3-45d5-914a-81716539984a',
        included_segments: ['All'],
        contents: { en: data },
        headings: { en: 'Công việc' },
      }),
    };
    fetch(endpoint, params).then(res => console.log(res));
  };
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(MenuformBottomCompoment);
