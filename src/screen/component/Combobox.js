import React, { Component, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Header,
  Icon,
  ListItem,
  SearchBar,
  Badge,
  Divider,
} from 'react-native-elements';
import {
  Text,
  View,
  Animated,
  Dimensions,
  Easing,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import { Directions } from 'react-native-gesture-handler';
import { AppStyles, AppColors } from '@theme';
import { Actions } from 'react-native-router-flux';
import FormSearch from './FormSearch';
import { stubFalse } from 'lodash';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class Combobox extends Component {
  constructor(props) {
    super(props);
    var _data = props.data;
    this.state = {
      TypeSelect:
        props.TypeSelect !== undefined && props.TypeSelect !== null
          ? props.TypeSelect
          : 'single', // kiểu chọn nhiều hay chọn 1
      paging:
        props.paging !== undefined && props.paging !== null
          ? props.paging
          : false, // phân biệt danh sách có phân trang hay không
      valueSelect:
        props.value !== undefined && props.value !== null ? props.value : null, //giá trị mặc định
      EvenFromSearch: false, // bật tắt form search
      position: props.position, // hiển thị ở đâu?
      offsetY:
        props.position === 'top'
          ? new Animated.Value(props.data.length * -50)
          : props.position === 'bottom'
            ? new Animated.Value(props.data.length * 50 + 30)
            : '',
      isOpen: false,
      height:
        props.position === 'top'
          ? props.data.length * -110
          : props.position === 'bottom'
            ? props.data.length * 50 + 50
            : '',
      dataSubmit: null,
      data: _data,
      dataView: _data,
      NameMenu: props.nameMenu ? props.nameMenu : 'Chức năng',
      CheckAllValue: false,
      searchAPI:
        props.searchAPI !== undefined && props.searchAPI !== null
          ? props.searchAPI
          : false,
    };

    props.eOpen(this.eOpen);
    this.callback_Paging = props.callback_Paging;
    this.callback_SearchPaging = props.callback_SearchPaging;
  }
  checkPosition = position => {
    switch (position) {
      case 'top':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      case 'bottom':
        this.setState({
          offsetY: new Animated.Value(props.data.length * 50 + 30),
          height: props.data.length * 50 + 30,
        });
        break;

      case 'left':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      case 'right':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      default:
        Alert.alert('NUMBER NOT FOUND');
    }
  };
  actionClick = value => {
    this.props.callback(value);
  };

  componentDidMount() {
    this.ae(this.state.dataView, this.state.TypeSelect, this.state.valueSelect);
  }

  //#region  lấy dữ liệu phân trang
  Paging = data => {
    var _dataSubmit = this.state.dataSubmit;
    var _dataview = this.state.dataView;
    if (this.state.TypeSelect == 'multiple') {
      for (let y = 0; y < data.length; y++) {
        for (let i = 0; i < _dataSubmit.length; i++) {
          if (_dataSubmit[i].value !== null) {
            if (_dataSubmit[i].value == data[y].value) {
              data[y].check = true;
              break;
            } else {
              data[y].check = false;
            }
          } else {
            data[y].check = false;
          }
        }
        _dataview.push(data[y]);
      }
    } else {
      for (let y = 0; y < data.length; y++) {
        if (_dataSubmit.value !== null) {
          if (_dataSubmit.value == data[y].value) {
            data[y].check = true;
          } else {
            data[y].check = false;
          }
        } else {
          data[y].check = false;
        }
        _dataview.push(data[y]);
      }
    }
    this.setState({ dataView: _dataview });
  };
  //#endregion

  //#region lấy dữ liệu khi search API
  SearchPaging = datasearch => {
    var _dataSubmit = this.state.dataSubmit;
    var _dataView = [];
    if (this.state.TypeSelect == 'multiple') {
      for (let y = 0; y < datasearch.length; y++) {
        for (let i = 0; i < _dataSubmit.length; i++) {
          if (_dataSubmit[i].value == datasearch[y].value) {
            datasearch[y].check = true;
            break;
          } else {
            datasearch[y].check = false;
          }
        }
        _dataView.push(datasearch[y]);
      }
    } else {
      for (let y = 0; y < datasearch.length; y++) {
        if (_dataSubmit.value == datasearch[y].value) {
          datasearch[y].check = true;
        } else {
          datasearch[y].check = false;
        }
        _dataView.push(datasearch[y]);
      }
    }
    this.setState({ dataView: _dataView });
  };
  //#endregion

  //#region Xử lý dữ liệu truyền vào
  ae(d, type, value) {
    switch (type) {
      case 'multiple':
        var itemselect = [];
        if (value !== null) {
          if (value.length > 0) {
            for (var i = 0; i < d.length; i++) {
              for (let y = 0; y < value.length; y++) {
                if (value[y] == d[i].value) {
                  d[i].check = true;
                  itemselect.push(d[i]);
                  break;
                } else {
                  d[i].check = false;
                }
              }
            }
          } else {
            for (var i = 0; i < d.length; i++) {
              d[i].check = false;
            }
            itemselect.push({ text: '', value: null });
          }
        } else {
          for (var i = 0; i < d.length; i++) {
            d[i].check = false;
          }
          itemselect.push({ text: '', value: null });
        }
        this.setState({ dataView: d, dataSubmit: itemselect });
        this.actionClick(itemselect);
        break;
      case 'single':
        var itemselect = null;
        if (value !== null) {
          for (var i = 0; i < d.length; i++) {
            if (value == d[i].value) {
              d[i].check = true;
              itemselect = d[i];
            } else {
              d[i].check = false;
            }
          }
        } else {
          for (var i = 0; i < d.length; i++) {
            d[i].check = false;
            itemselect = { text: '', value: null };
          }
        }
        this.setState({ dataView: d, dataSubmit: itemselect });
        this.actionClick(itemselect);
        break;
      default:
        break;
    }
  }
  //#endregion

  //#region  đóng cửa sổ
  eOpen = option => {
    if (option) {
      if (option.isChangeAction) {
        this.ae(option.data);
        this.setState({
          data: option.data,
        });
      }
      if (option.NameMenu !== undefined && option.NameMenu !== null) {
        this.setState({
          NameMenu: option.NameMenu,
        });
      }
    }
    this.openAction();
    for (let i = 0; i < this.state.data.length; i++) {
      this.state.data[i].check = false;
    }
    if (this.props.TypeSelect === 'multiple') {
      for (let i = 0; i < this.props.value.length; i++) {
        for (let y = 0; y < this.state.data.length; y++) {
          if (this.props.value[i] === this.state.data[y].value) {
            this.state.data[y].check = true;
          }
        }
      }
    }
    this.setState(prevState => {
      return {
        ...prevState,
        isOpen: !prevState.isOpen,
        EvenFromSearch: false,
      };
    });
  };
  //#endregion

  //#region mở cửa sổ
  openAction = () => {
    if (!this.state.isOpen) {
      Animated.timing(this.state.offsetY, {
        toValue: 0,
        // duration: 1000,
        // easing: Easing.ease
        duration: 800,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this.state.offsetY, {
        toValue: this.state.height,
        // duration: 1000,
        // easing: Easing.ease
        duration: 800,
        useNativeDriver: true,
      }).start();
    }
  };
  //#endregion

  //#region  Tìm kiếm
  TextSearch = search => {
    if (this.props.callback_SearchPaging) {
      this.callback_SearchPaging(this.SearchPaging, search);
    } else {
      var data = this.state.data;
      var matches = [],
        i;
      for (i = data.length; i--;) {
        if (
          data[i] &&
          data[i].text &&
          (data[i].text + '')
            .toUpperCase()
            .includes((search + '').toUpperCase())
        )
          matches.push(data[i]);
      }
      this.setState({ dataView: matches });
    }
  };
  //#endregion

  //#region chọn nhiều chọn 1
  callBack_multiple = () => {
    var _dataview = this.state.dataView;
    var _submit = [];
    for (let z = 0; z < _dataview.length; z++) {
      if (_dataview[z].check == true) {
        _submit.push(_dataview[z]);
      }
    }
    this.setState({ dataSubmit: _submit });
    this.actionClick(_submit);
    this.eOpen();
  };
  SaveValues = item => {
    if (this.state.TypeSelect == 'single') {
      var _submit = null;
      var _data = this.state.dataView;
      for (var i = 0; i < _data.length; i++) {
        if (_data[i].value == item.value) {
          _data[i].check = !item.check;
        } else _data[i].check = false;
      }
      for (let z = 0; z < _data.length; z++) {
        if (_data[z].check == true) {
          _submit = _data[z];
          break;
        } else {
          _submit = { text: '', value: null };
        }
      }
      this.setState({ dataSubmit: _submit, dataView: _data });
      this.actionClick(_submit);
      this.eOpen();
    } else {
      var _data = this.state.dataView;
      for (var i = 0; i < _data.length; i++) {
        if (_data[i].value == item.value) {
          _data[i].check = !item.check;
          break;
        }
      }
      this.setState({
        dataView: _data,
      });
    }
  };
  //#endregion

  //#region chọn tất cả
  CheckAllValue = () => {
    var data = this.state.dataView;
    var dataNew = [];
    for (let i = 0; i < data.length; i++) {
      data[i].check = true;
      dataNew.push(data[i]);
    }
    this.setState({
      CheckAllValue: true,
      dataView: dataNew,
    });
  };
  ClearAllValue = () => {
    var data = this.state.dataView;
    var dataNew = [];
    for (let i = 0; i < data.length; i++) {
      data[i].check = false;
      dataNew.push(data[i]);
    }
    this.setState({
      CheckAllValue: false,
      dataView: dataNew,
      dataSubmit: [],
    });
  };
  //#endregion

  //#region sự kiện load đế cuối trang
  LoadDateEnd(event) {
    console.log(
      '=====',
      event.nativeEvent.contentOffset.y,
      'X',
      this.state.dataView.length * 40 - 310,
    );
    var y = event.nativeEvent.contentOffset.y;
    var heightView = this.state.dataView.length * 40 - 310;
    if (y >= heightView) {
      if (this.state.paging == true) {
        this.callback_Paging(this.Paging);
      }
    }
  }
  //#endregion

  render() {
    return this.state && this.state.isOpen ? (
      <TouchableOpacity
        style={[
          this.state.position === 'top'
            ? styles.positionTop
            : this.state.position === 'bottom'
              ? styles.positionBottom
              : '',
          {
            width: DRIVER.width,
            position: 'absolute',
            backgroundColor: 'rgba(52, 52, 52, 0.5)',
            height: DRIVER.height,
            zIndex: 100,
          },
        ]}
        onPress={() => {
          this.TextSearch('');
          this.eOpen();
        }}>
        {Platform.OS == 'ios' ?
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            style={[
              this.state.position === 'top'
                ? { top: 0 }
                : this.state.position === 'bottom'
                  ? { bottom: 0 }
                  : '',
              { position: 'absolute', width: '100%', flex: 1 },
            ]}>
            <Animated.View
              style={{
                backgroundColor: '#CED0CE',
                transform: [{ translateY: this.state.offsetY }],
                borderRadius: 10,
                bottom: 0,
              }}>
              <View
                style={{ flex: 1 }}>
                <View
                  style={{
                    flexDirection: 'column',
                    backgroundColor: 'white',
                    borderTopColor: '#CED0CE',
                    borderTopWidth: 1,
                    bottom: 0,
                    maxHeight: DRIVER.height / 2,
                    borderRadius: 10,
                    opacity: 1,
                  }}>
                  {this.state.position === 'bottom' ? (
                    <View>
                      <TouchableOpacity
                        style={{ flexDirection: 'row', padding: 10 }}
                        onPress={() => {
                          this.ClearAllValue();
                          this.TextSearch('');
                          this.eOpen();
                        }}>
                        <View style={{ flex: 1 }}>
                          <Icon
                            color={AppColors.gray}
                            name={'close'}
                            type="antdesign"
                          />
                        </View>
                        <View style={{ flex: 6 }}>
                          <Text style={{ fontWeight: 'bold' }}>
                            {this.state.NameMenu.length > 20
                              ? this.state.NameMenu.substring(0, 30) + ' ...'
                              : this.state.NameMenu}
                          </Text>
                        </View>
                        <TouchableOpacity
                          style={{ flex: 1 }}
                          onPress={() => {
                            this.setState({
                              EvenFromSearch: !this.state.EvenFromSearch,
                            });
                            this.TextSearch('');
                          }}>
                          <Icon
                            color={AppColors.gray}
                            name="search1"
                            type="antdesign"
                          />
                        </TouchableOpacity>
                        {this.state.TypeSelect == 'multiple' ? (
                          <TouchableOpacity
                            style={{ flex: 1 }}
                            onPress={() => {
                              this.state.CheckAllValue
                                ? this.ClearAllValue()
                                : this.CheckAllValue();
                              this.TextSearch('');
                            }}>
                            <Icon
                              color={
                                this.state.CheckAllValue
                                  ? AppColors.ColorEdit
                                  : AppColors.gray
                              }
                              name="playlist-check"
                              type="material-community"
                            />
                          </TouchableOpacity>
                        ) : null}
                        {this.state.TypeSelect == 'multiple' ? (
                          <TouchableOpacity
                            style={[{ flex: 1 }, AppStyles.containerCentered]}
                            onPress={() => this.callBack_multiple()}>
                            <Text style={{ color: AppColors.ColorAdd }}>Chọn</Text>
                          </TouchableOpacity>
                        ) : null}
                      </TouchableOpacity>
                      <Divider />
                      {this.state.EvenFromSearch == true ? (
                        <FormSearch
                          CallbackSearch={callback => this.TextSearch(callback)}
                        />
                      ) : null}
                    </View>
                  ) : null}
                  <ScrollView
                    style={{ padding: 10, marginBottom: 20 }}
                    ref={ref => {
                      this.myScroll = ref;
                    }}
                    onMomentumScrollEnd={event => this.LoadDateEnd(event)}>
                    {this.state.dataView &&
                      this.state.dataView.length > 0 &&
                      this.state.dataView.map((item, i) => {
                        return (
                          <TouchableOpacity
                            key={i}
                            onPress={() => {
                              this.SaveValues(item);
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                paddingTop: 10,
                                paddingBottom: 10,
                                borderBottomColor: '#eff0f1',
                                borderBottomWidth: 1,
                                height: 40,
                              }}>
                              <View style={{ flex: 10, justifyContent: 'center' }}>
                                <Text>
                                  {item.text && item.text.length > 40
                                    ? item.text.substring(0, 40) + '...'
                                    : item.text}
                                </Text>
                              </View>
                              <View style={{ flex: 1, justifyContent: 'center' }}>
                                {item.check && item.check == true ? (
                                  <Icon
                                    color={AppColors.gray}
                                    name="checkcircle"
                                    type="antdesign"
                                    color={AppColors.ColorEdit}
                                    size={20}
                                  />
                                ) : null}
                              </View>
                            </View>
                          </TouchableOpacity>
                        );
                      })}
                  </ScrollView>
                  {this.state.position === 'top' ? (
                    <View>
                      <TouchableOpacity
                        style={{ flexDirection: 'row', padding: 10 }}
                        onPress={() => {
                          this.setState({ EvenFromSearch: false });
                          this.TextSearch('');
                          this.eOpen();
                        }}>
                        <View style={{ flex: 1 }}>
                          <Icon
                            color={AppColors.gray}
                            name={'close'}
                            type="antdesign"
                          />
                        </View>
                        <View style={{ flex: 6 }}>
                          <Text style={{ fontWeight: 'bold' }}>
                            {this.state.NameMenu.length > 20
                              ? this.state.NameMenu.substring(0, 30) + ' ...'
                              : this.state.NameMenu}
                          </Text>
                        </View>
                        <TouchableOpacity
                          style={{ flex: 1 }}
                          onPress={() => {
                            this.setState({
                              EvenFromSearch: !this.state.EvenFromSearch,
                            });
                            this.TextSearch('');
                          }}>
                          <Icon
                            color={AppColors.gray}
                            name="search1"
                            type="antdesign"
                          />
                        </TouchableOpacity>
                        {this.state.TypeSelect == 'multiple' ? (
                          <TouchableOpacity
                            style={[{ flex: 1 }, AppStyles.containerCentered]}
                            onPress={() => this.callBack_multiple()}>
                            <Text style={{ color: AppColors.ColorAdd }}>Chọn</Text>
                          </TouchableOpacity>
                        ) : null}
                      </TouchableOpacity>
                      <Divider />
                      {this.state.EvenFromSearch == true ? (
                        <FormSearch
                          CallbackSearch={callback => this.TextSearch(callback)}
                        />
                      ) : null}
                    </View>
                  ) : null}
                </View>
              </View>
            </Animated.View>
          </KeyboardAvoidingView>
          : (
            <View
              style={[
                this.state.position === 'top'
                  ? { top: 0 }
                  : this.state.position === 'bottom'
                    ? { bottom: 0 }
                    : '',
                { position: 'absolute', width: '100%' },
              ]}>
              <Animated.View
                style={{
                  backgroundColor: '#CED0CE',
                  transform: [{ translateY: this.state.offsetY }],
                  borderRadius: 10,
                  bottom: 0,
                }}>
                <KeyboardAvoidingView
                  behavior={Platform.OS === 'ios' ? 'padding' : null}
                  style={{ flex: 1 }}>
                  <View
                    style={{
                      flexDirection: 'column',
                      backgroundColor: 'white',
                      borderTopColor: '#CED0CE',
                      borderTopWidth: 1,
                      bottom: 0,
                      maxHeight: DRIVER.height / 2,
                      borderRadius: 10,
                      opacity: 1,
                    }}>
                    {this.state.position === 'bottom' ? (
                      <View>
                        <TouchableOpacity
                          style={{ flexDirection: 'row', padding: 10 }}
                          onPress={() => {
                            this.ClearAllValue();
                            this.TextSearch('');
                            this.eOpen();
                          }}>
                          <View style={{ flex: 1 }}>
                            <Icon
                              color={AppColors.gray}
                              name={'close'}
                              type="antdesign"
                            />
                          </View>
                          <View style={{ flex: 6 }}>
                            <Text style={{ fontWeight: 'bold' }}>
                              {this.state.NameMenu.length > 20
                                ? this.state.NameMenu.substring(0, 30) + ' ...'
                                : this.state.NameMenu}
                            </Text>
                          </View>
                          <TouchableOpacity
                            style={{ flex: 1 }}
                            onPress={() => {
                              this.setState({
                                EvenFromSearch: !this.state.EvenFromSearch,
                              });
                              this.TextSearch('');
                            }}>
                            <Icon
                              color={AppColors.gray}
                              name="search1"
                              type="antdesign"
                            />
                          </TouchableOpacity>
                          {this.state.TypeSelect == 'multiple' ? (
                            <TouchableOpacity
                              style={{ flex: 1 }}
                              onPress={() => {
                                this.state.CheckAllValue
                                  ? this.ClearAllValue()
                                  : this.CheckAllValue();
                                this.TextSearch('');
                              }}>
                              <Icon
                                color={
                                  this.state.CheckAllValue
                                    ? AppColors.ColorEdit
                                    : AppColors.gray
                                }
                                name="playlist-check"
                                type="material-community"
                              />
                            </TouchableOpacity>
                          ) : null}
                          {this.state.TypeSelect == 'multiple' ? (
                            <TouchableOpacity
                              style={[{ flex: 1 }, AppStyles.containerCentered]}
                              onPress={() => this.callBack_multiple()}>
                              <Text style={{ color: AppColors.ColorAdd }}>Chọn</Text>
                            </TouchableOpacity>
                          ) : null}
                        </TouchableOpacity>
                        <Divider />
                        {this.state.EvenFromSearch == true ? (
                          <FormSearch
                            CallbackSearch={callback => this.TextSearch(callback)}
                          />
                        ) : null}
                      </View>
                    ) : null}
                    <ScrollView
                      style={{ padding: 10, }}
                      ref={ref => {
                        this.myScroll = ref;
                      }}
                      onMomentumScrollEnd={event => this.LoadDateEnd(event)}>
                      <View style={{ paddingBottom: 50 }}>

                        {this.state.dataView &&
                          this.state.dataView.length > 0 &&
                          this.state.dataView.map((item, i) => {
                            return (
                              <TouchableOpacity
                                key={i}
                                onPress={() => {
                                  this.SaveValues(item);
                                }}>
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    borderBottomColor: '#eff0f1',
                                    borderBottomWidth: 1,
                                    height: 40,
                                  }}>
                                  <View style={{ flex: 10, justifyContent: 'center' }}>
                                    <Text>
                                      {item.text && item.text.length > 80
                                        ? item.text.substring(0, 80) + '...'
                                        : item.text}
                                    </Text>
                                  </View>
                                  <View style={{ flex: 1, justifyContent: 'center' }}>
                                    {item.check && item.check == true ? (
                                      <Icon
                                        color={AppColors.gray}
                                        name="checkcircle"
                                        type="antdesign"
                                        color={AppColors.ColorEdit}
                                        size={20}
                                      />
                                    ) : null}
                                  </View>
                                </View>
                              </TouchableOpacity>
                            );
                          })}
                      </View>

                    </ScrollView>
                    {this.state.position === 'top' ? (
                      <View>
                        <TouchableOpacity
                          style={{ flexDirection: 'row', padding: 10 }}
                          onPress={() => {
                            this.setState({ EvenFromSearch: false });
                            this.TextSearch('');
                            this.eOpen();
                          }}>
                          <View style={{ flex: 1 }}>
                            <Icon
                              color={AppColors.gray}
                              name={'close'}
                              type="antdesign"
                            />
                          </View>
                          <View style={{ flex: 6 }}>
                            <Text style={{ fontWeight: 'bold' }}>
                              {this.state.NameMenu.length > 20
                                ? this.state.NameMenu.substring(0, 30) + ' ...'
                                : this.state.NameMenu}
                            </Text>
                          </View>
                          <TouchableOpacity
                            style={{ flex: 1 }}
                            onPress={() => {
                              this.setState({
                                EvenFromSearch: !this.state.EvenFromSearch,
                              });
                              this.TextSearch('');
                            }}>
                            <Icon
                              color={AppColors.gray}
                              name="search1"
                              type="antdesign"
                            />
                          </TouchableOpacity>
                          {this.state.TypeSelect == 'multiple' ? (
                            <TouchableOpacity
                              style={[{ flex: 1 }, AppStyles.containerCentered]}
                              onPress={() => this.callBack_multiple()}>
                              <Text style={{ color: AppColors.ColorAdd }}>Chọn</Text>
                            </TouchableOpacity>
                          ) : null}
                        </TouchableOpacity>
                        <Divider />
                        {this.state.EvenFromSearch == true ? (
                          <FormSearch
                            CallbackSearch={callback => this.TextSearch(callback)}
                          />
                        ) : null}
                      </View>
                    ) : null}
                  </View>
                </KeyboardAvoidingView>
              </Animated.View>
            </View>
          )}
      </TouchableOpacity>
    ) : null;
  }
}
const styles = StyleSheet.create({
  positionTop: {
    top: 80,
  },
  positionBottom: {
    bottom: -10,
  },
});
export default Combobox;
