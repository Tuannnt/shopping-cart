import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    ScrollView,
    View
} from 'react-native';
import { connect } from 'react-redux';
import { Divider, Icon, ListItem, SearchBar, Badge } from 'react-native-elements';
import { AppStyles, AppColors } from '@theme';

class SearchOfTheDay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListDay: [],
            ChoiceDate: '',
            OffSearch: props.OffSearch,
            TypeSearch: (props.TypeSearch == undefined || props.TypeSearch == null) ? false : props.TypeSearch
        };
        this.CallBackValue = this.props.CallBackValue
        this.CallBackOffSearch = this.props.CallBackOffSearch
    }

    componentDidMount(): void {

        this.DateOfSearch(new Date().getMonth() + 1, new Date().getFullYear());
    }
    DateOfSearch(m, y) {
        var _listday = this.state.ListDay;
        var _nday = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).getDate();
        var _today = new Date().getDate();
        for (let i = 1; i <= _nday; i++) {
            var _m = m < 10 ? '0' + m : m
            var _i = i < 10 ? '0' + i : i
            _listday.push({
                Key: '' + y + '' + _m + '' + _i + '',
                dd: _i,
                MM: m,
                yyyy: y,
                date: new Date(y, m - 1, i),
                Dayofweek: ((new Date(y, m - 1, i).getDay() == 0) ? 'CN' : 'T' + (new Date(y, m - 1, i).getDay() + 1)),
                CheckBox: false
            })
        }
        this.setState({ ListDay: _listday })
        var obj = _listday.find(x => x.dd == _today)
        if (obj != null) {
            this.onchoiceDate(obj)
        }
    }
    onchoiceDate(item) {
        var _listday = this.state.ListDay;
        var _listdayView = [];
        for (let i = 0; i < _listday.length; i++) {
            if (_listday[i].Key == item.Key) {
                _listday[i].CheckBox = true
            } else {
                _listday[i].CheckBox = false
            }
            _listdayView.push(_listday[i])
        }
        this.setState({ ListDay: _listdayView })
        var _item = item.Key.toString();
        if (_item != null && _item != undefined && _item != "") {
            var yyyy = _item.substring(0, 4);
            var MM = _item.substring(4, 6)
            if (MM.substring(0, 1) == 0) {
                MM = MM.substring(1, 2)
            };
            var dd = _item.substring(6, 8);
            if (dd.substring(0, 1) == 0) {
                dd = dd.substring(1, 2)
            };
            var Date = dd + " tháng " + MM + " năm " + yyyy
            this.setState({ ChoiceDate: Date })
            this.CallBackValue(item.Key)
        } else {
            return ""
        }
    }
    render() {
        return (

            <View style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 0.5, borderBottomColor: AppColors.gray, paddingBottom: 10 }}>
                <View style={[{ flex: 2 }]}>
                    <Text>Thứ 3 Ngày 09 năm 2020</Text>
                </View>
                <View style={[AppStyles.containerCentered, { flex: 1,flexDirection: 'row',justifyContent:'flex-start', }]}>
                    <TouchableOpacity style={{ flex: 0.5, backgroundColor:'red', padding:10 }} onPress={() => this.onAction(item)}>
                        <Icon
                            name="left"
                            type="antdesign"
                            size={15}
                            color={AppColors.gray} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 0.5, padding:10,width:'100%'  }} onPress={() => this.onAction(item)}>
                        <Icon
                            name="right"
                            type="antdesign"
                            size={15}
                            color={AppColors.gray} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(SearchOfTheDay);
