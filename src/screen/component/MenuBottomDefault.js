import React, { Component } from 'react';
import { Alert, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import TabNavigator from 'react-native-tab-navigator';
import { Icon } from 'react-native-elements';
import Dialog from "react-native-dialog";
import PopupHandleVote from "./MenuBottom/WorkFlow/PopupHandleVote";

const styles = StyleSheet.create({
    tabNavigatorItem: {
        marginTop: 10,
        justifyContent: 'space-between',
        alignSelf: 'center',
    },
    itemIcon: {
        fontWeight: '600',
        fontSize: 30,
        color: 'black'
    },
    itemIconClick: {
        fontWeight: '600',
        fontSize: 30,
        color: '#f6b801'
    },
    itemQR: {
        borderRadius: 15,
        backgroundColor: '#f6b801',
        fontWeight: '600',
        fontSize: 30,
        color: '#fff',
        padding: 10
    }
});

class MenuBottomDefault extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: '',
            open: false,
            showDialog: '',
            EventClick: false,
            CommentWF: ''
        };
        this.callbackSubmit = this.props.onSubmit;
        this.ViewMenu = [
            {
                title: 'Trang chủ',
                iconName: 'home',
                icontype: 'antdesign',
                iconSize: 20,
                onPress: 'Home'
            },
            {
                title: 'Danh sách',
                iconName: 'profile',
                icontype: 'antdesign',
                iconSize: 20,
                onPress: this.props.backListByKey
            },
            {
                title: 'Sửa',
                iconName: 'edit',
                icontype: 'font-awesome',
                iconSize: 22,
                onPress: 'Edit'
            },
            {
                title: 'Xóa',
                iconName: 'trash-o',
                icontype: 'font-awesome',
                iconSize: 22,
                onPress: 'Delete'
            },
        ];
        this.btns = [
            {
                name: 'Xác nhận',
                value: 'Y',
                color: '#3366CC'
            },
            {
                name: 'Hủy',
                value: 'N',
                color: 'red'
            }
        ];
    }
    setSelectedTab = (tab) => {
        this.setState({ selectedTab: tab });
    };
    render() {
        return (
            <View style={{ flexDirection: 'row', height: '100%' }}>
                {this.ViewMenu.length > 0 ? this.ViewMenu.map((item, index) =>
                    <TouchableOpacity style={{ flex: 1, marginTop: 5 }} onPress={() => this.onPressClick(item.onPress)}>
                        <View style={{ flex: 1 }}>
                            <Icon style={styles.itemIcon} name={item.iconName} type={item.icontype}
                                size={item.iconSize} />
                        </View>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <Text style={{ fontSize: 10 }}>{item.title}</Text>
                        </View>
                    </TouchableOpacity>
                ) : null}
                {this.renderPopup(this.props.Title, this.btns)}
            </View>
        );
    }

    //Begin cretaed popup
    renderPopup(title, btns) {
        return (
            this.state.EventClick === true ?
                <View>
                    <Dialog.Container visible={true}>
                        <Dialog.Title>Xác nhận xóa:</Dialog.Title>
                        <Dialog.Description>Bạn có chắc chắn xóa : {this.props.Title}</Dialog.Description>
                        {
                            (this.btns && this.btns.length > 0) ? this.btns.map((item, index) =>
                                <Dialog.Button
                                    style={{ color: item.color }}
                                    label={item.name}
                                    onPress={() => this.handleCancel(item.value)} />
                            )
                                : null
                        }
                    </Dialog.Container>
                </View>
                : null
        )
    }
    onPressClick(para) {
        console.log(para);
        switch (para) {
            case 'Home': {
                Actions.app();
                break;
            }
            case 'Delete': {
                this.setState({
                    EventClick: true
                })
                break;
            }
            case 'Edit': {
                Alert.alert('Đang cập nhật chức năng');
                break;
            }
            default: {
                Actions.popTo(para);
                break;
            }
        }

    }
    // Duyệt phiếu
    handleCancel = (para) => {
        if (para == 'Y') {
            this.callbackSubmit();
        }
        this.setState({
            EventClick: false
        })
    };

    callbackPopup() {
    }
    clickPopup(callback) {
        this.callbackPopup = callback;
    }

    sendNotification = data => {

    };
}

const mapStateToProps = state => ({})
const mapDispatchToProps = {}
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(MenuBottomDefault);
