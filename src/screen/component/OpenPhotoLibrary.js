import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Image,
  Animated,
  PermissionsAndroid,
} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import {Divider, Icon, Header, Badge} from 'react-native-elements';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import OpenGroupImages from '../MS/OpenGroupImages';
import CameraRoll from '@react-native-camera-roll/camera-roll';
import Toast from 'react-native-simple-toast';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const options = {
  title: 'Chọn ảnh',
  cancelButtonTitle: 'Huỷ bỏ',
  // takePhotoButtonTitle: 'Chụp ảnh',
  chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
  chooseWhichLibraryTitle: 'Chọn thư viện',
};
//custom ảnh
const _styles = StyleSheet.create({
  imageGrid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  image: {
    width: SCREEN_WIDTH / 3.03,
    height: SCREEN_WIDTH / 3.03,
    margin: 0.5,
  },
});
class OpenPhotoLibrary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //Animated
      offsetY: new Animated.Value(SCREEN_HEIGHT),
      //
      GroupName: '',
      images: [],
      ListImageSubmit: [],
      GroupImage: [],
      statusloadImage: false,
      isOpen: false,
      ImageCamera: [],
    };
    props.openLibrary(this.openLibrary);
    this.actionClick = props.callback;
  }
  actionClick() {}

  openLibrary = option => {
    this._CleanListImages();
    if (option) {
      if (option.isChangeAction) {
        this.setState({
          data: option.data,
        });
      }
    }
    if (!this.state.isOpen) {
      this.setState({
        isOpen: true,
      });
    } else {
      setTimeout(() => {
        this.setState({
          isOpen: false,
        });
      }, 500);
    }
    if (!this.state.isOpen) {
      Animated.sequence([this.OpenTabView]).start();
    } else {
      Animated.sequence([this.OffTabView]).start();
    }
  };
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    this.SkillAnimated();
    this.openImages();
  }
  //#region SkillAnimated
  SkillAnimated = () => {
    //bật tab
    this.OpenTabView = Animated.timing(this.state.offsetY, {
      toValue: 0,
      duration: 400,
      useNativeDriver: true,
    });
    //đóng tab
    this.OffTabView = Animated.timing(this.state.offsetY, {
      toValue: SCREEN_HEIGHT,
      duration: 400,
      useNativeDriver: true,
    });
  };

  //#endregion

  //#region  lấy thư mục ảnh
  openImages = () => {
    CameraRoll.getAlbums({assetType: 'All'}).then(async rs => {
      try {
        let _groupImage = [];
        let count = 0;
        let _groupName = '';
        for (let n = 0; n < rs.length; n++) {
          if (rs[n].count > count) {
            (_groupName = rs[n].title), (count = rs[n].count);
          }
        }
        for (let v = 0; v < rs.length; v++) {
          if (!rs[v].title) {
            return;
          }
          await CameraRoll.getPhotos({
            first: 1,
            assetType: 'Photos',
            groupName: rs[v].title,
          }).then(
            _rs => {
              try {
                if (_rs.edges.length > 0) {
                  _groupImage.push({
                    text: rs[v].title,
                    value: rs[v].title,
                    count: rs[v].count,
                    image: _rs.edges[0].node.image.uri,
                  });
                }
              } catch (error) {
                console.log('Có lỗi khi lấy file');
              }
            },
            _reject => {
              console.log(_reject);
            },
          );
        }
        this.setState({GroupImage: _groupImage, GroupName: _groupName});
        this.end_cursor = undefined;
        this._storeImages();
      } catch (ex) {
        this.callBack();
        console.log('Có lỗi khi lấy album anh');
      }
    });
  };
  //#endregion

  //#region File ảnh
  OnScrollEndDrag(event, item) {
    var y = event.nativeEvent.contentOffset.y;
    var i = ((SCREEN_WIDTH / 3.03) * item.length) / 3 - 500;
    if (y >= i) {
      if (this.state.statusloadImage) {
        this._storeImages();
      }
    }
  }
  OnScrollEndDragFull(event, item) {
    var y = event.nativeEvent.contentOffset.y;
    var i = ((SCREEN_WIDTH / 3.03) * item.length) / 3 - SCREEN_HEIGHT;
    if (y >= i) {
      if (this.state.statusloadImage) {
        this._storeImages();
      }
    }
  }

  _storeImages() {
    CameraRoll.getPhotos({
      first: 50,
      assetType: 'Photos',
      groupName: this.state.GroupName,
      // toTime:100
      after: this.end_cursor,
    })
      .then(data => {
        this.setState({statusloadImage: data.page_info.has_next_page});
        this.end_cursor = data.page_info.end_cursor;
        var assets = data.edges;
        var _images = this.state.images;
        for (let i = 0; i < assets.length; i++) {
          var name = assets[i].node.image.uri.split('/');
          var check = this.state.ListImageSubmit.find(
            x => x.uri === assets[i].node.image.uri,
          );
          if (check !== undefined) {
            _images.push({
              name:
                assets[i].node.image.filename !== null
                  ? assets[i].node.image.filename
                  : name[name.length - 1].toLowerCase(),
              size: assets[i].node.image.fileSize,
              type: assets[i].node.type.toLowerCase(),
              uri: assets[i].node.image.uri,
              check: true,
              CountSelect: check.CountSelect,
            });
          } else {
            _images.push({
              name:
                assets[i].node.image.filename !== null
                  ? assets[i].node.image.filename
                  : name[name.length - 1].toLowerCase(),
              size: assets[i].node.image.fileSize,
              type: assets[i].node.type.toLowerCase(),
              uri: assets[i].node.image.uri,
            });
          }
        }
        this.setState({
          images: _images,
        });
      })
      .catch(err => {
        this.callBack();
        console.log(err);
        alert(err);
      });
  }
  _selectMultiple(item) {
    var listSubmit = [];
    if (this.state.ListImageSubmit.length > 0) {
      var obj = this.state.ListImageSubmit.find(x => x.name === item.name);
      if (obj !== undefined) {
        var count = 0;
        for (let i = 0; i < this.state.ListImageSubmit.length; i++) {
          if (this.state.ListImageSubmit[i].name !== item.name) {
            listSubmit.push({
              name: this.state.ListImageSubmit[i].name,
              size: this.state.ListImageSubmit[i].size,
              type: this.state.ListImageSubmit[i].type,
              uri: this.state.ListImageSubmit[i].uri,
              check: true,
              CountSelect: count + 1,
            });
            count = count + 1;
          }
        }
        this.state.ListImageSubmit = listSubmit;
      } else {
        this.state.ListImageSubmit.push({
          name: item.name,
          size: item.size,
          type: item.type,
          uri: item.uri,
          check: true,
          CountSelect: this.state.ListImageSubmit.length + 1,
        });
      }
    } else {
      this.state.ListImageSubmit.push({
        name: item.name,
        size: item.size,
        type: item.type,
        uri: item.uri,
        check: true,
        CountSelect: this.state.ListImageSubmit.length + 1,
      });
    }
    var AllListImages = this.state.images;
    for (let y = 0; y < AllListImages.length; y++) {
      if (AllListImages[y].name === item.name) {
        AllListImages[y].check = !AllListImages[y].check;
      }
      for (let z = 0; z < this.state.ListImageSubmit.length; z++) {
        if (AllListImages[y].name === this.state.ListImageSubmit[z].name) {
          AllListImages[y].CountSelect = this.state.ListImageSubmit[
            z
          ].CountSelect;
          break;
        }
      }
    }
    this.setState({
      images: AllListImages,
    });
  }
  _CleanListImages() {
    var ListImages = this.state.images;
    for (let i = 0; i < ListImages.length; i++) {
      ListImages[i].check = false;
    }
    this.setState({
      images: ListImages,
      ListImageSubmit: [],
    });
  }
  //#endregion

  callBack = () => {
    this.actionClick(this.state.ListImageSubmit);
    this.openLibrary();
  };
  render() {
    return (
      <Animated.View
        style={{
          backgroundColor: '#CED0CE',
          transform: [{translateY: this.state.offsetY}],
          borderRadius: 10,
          position: 'absolute',
          zIndex: 1000,
          bottom: 0,
          width: SCREEN_WIDTH,
        }}>
        <View
          style={{
            height: SCREEN_HEIGHT,
            width: SCREEN_WIDTH,
            backgroundColor: 'black',
          }}>
          <OpenGroupImages
            value={this.state.GroupName} // giá trị mặc định
            TypeSelect={'single'} // chọn 1 ("single") , chọn nhiều  ("multiple")
            callback={this.ChangeGroupIMG} //callback lại dữ liệu được chọn gồm text và value
            data={this.state.GroupImage} // danh sách hiển thị
            eOpen={this.openGroupIMG} // đóng cửa sổ
            position={'top'} //danh sách hiển thị từ dưới lên (bottom), hiển thị từ trên xuống ("top")
          />
          <Header
            containerStyle={{backgroundColor: 'black'}}
            leftComponent={
              <TouchableOpacity
                style={{
                  flex: 1,
                  width: 45,
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => this.openLibrary()}>
                <Icon name="close" type="antdesign" color={'#fff'} size={20} />
              </TouchableOpacity>
            }
            centerComponent={
              <TouchableOpacity
                style={{
                  flex: 8,
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                }}
                onPress={() => this.onActionGroupIMG()}>
                <Text
                  style={[
                    AppStyles.Titledefault,
                    {textAlign: 'center', color: '#fff'},
                  ]}>
                  {this.state.GroupName}{' '}
                </Text>
                <Icon color={'#fff'} name="down" type="antdesign" size={18} />
              </TouchableOpacity>
            }
          />
          <ScrollView
            ref={ref => {
              this.myScroll = ref;
            }}
            onScrollEndDrag={event =>
              this.OnScrollEndDragFull(event, this.state.images)
            }>
            <View style={_styles.imageGrid}>
              {/* <TouchableOpacity style={[_styles.image, AppStyles.containerCentered, { backgroundColor: '#fff' }]} onPress={() => this.ChoicePicture()}>
                                <Icon color={"#777777"} name='camera' type='font-awesome' size={30} />
                                <Text style={{ color: '#777777' }}>Chụp ảnh</Text>
                            </TouchableOpacity> */}
              {/* lỗi ios chụp ảnh máy thật, tạm ẩn */}
              {this.state.images.map(image => {
                return (
                  <View>
                    <TouchableOpacity
                      style={{
                        padding: 10,
                        zIndex: 1,
                        position: 'absolute',
                        right: 0,
                      }}
                      onPress={() => this._selectMultiple(image)}>
                      {image.check == true ? (
                        <Badge
                          badgeStyle={{
                            width: 25,
                            height: 25,
                            borderWidth: 0,
                            borderRadius: 20,
                          }}
                          value={image.CountSelect}
                        />
                      ) : (
                        <Badge
                          badgeStyle={{
                            width: 25,
                            height: 25,
                            borderWidth: 2,
                            borderRadius: 20,
                            backgroundColor: 'rgba(52, 52, 52, 0.2)',
                          }}
                        />
                      )}
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <Image style={_styles.image} source={{uri: image.uri}} />
                    </TouchableOpacity>
                  </View>
                );
              })}
            </View>
          </ScrollView>
          <View style={{backgroundColor: 'rgba(52, 52, 52,0.8)', height: 50}}>
            {this.state.ListImageSubmit &&
            this.state.ListImageSubmit.length > 0 ? (
              <TouchableOpacity
                style={{
                  marginBottom: 30,
                  marginRight: 20,
                  position: 'absolute',
                  zIndex: 1,
                  right: 0,
                  bottom: 0,
                }}
                onPress={() => this.callBack()}>
                <Badge
                  badgeStyle={{
                    width: 40,
                    height: 40,
                    borderWidth: 0,
                    borderRadius: 20,
                    backgroundColor: '#EEEEEE',
                  }}
                  value={
                    <TouchableOpacity
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flex: 1,
                      }}
                      onPress={() => this.callBack()}>
                      <Icon
                        iconStyle={{color: 'blue'}}
                        name="send"
                        type="Feather"
                      />
                      <Badge
                        badgeStyle={{
                          width: 15,
                          height: 15,
                          borderWidth: 0,
                          borderRadius: 20,
                        }}
                        value={this.state.ListImageSubmit.length}
                        containerStyle={{
                          position: 'absolute',
                          left: 20,
                          top: 0,
                        }}
                      />
                    </TouchableOpacity>
                  }
                />
              </TouchableOpacity>
            ) : (
              <View
                style={{
                  marginBottom: 30,
                  marginRight: 20,
                  position: 'absolute',
                  zIndex: 1,
                  right: 0,
                  bottom: 0,
                }}
              />
            )}
          </View>
        </View>
      </Animated.View>
    );
  }
  //#region hiển thị nhóm ảnh
  _openGroupIMG() {}
  openGroupIMG = d => {
    this._openGroupIMG = d;
  };
  onActionGroupIMG() {
    this._openGroupIMG();
  }
  ChangeGroupIMG = rs => {
    try {
      if (rs !== null) {
        this.state.GroupName = rs.value;
        this.setState({
          GroupName: this.state.GroupName,
          images: [],
        });
        this.end_cursor = undefined;
        this._storeImages();
      }
    } catch (error) {
      console.log(error);
    }
  };

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }
  ChoicePicture() {
    try {
      var _liFiles = this.state.ImageCamera;
      ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          return;
          // console.log('User cancelled image picker');
        } else if (response.error) {
          Toast.showWithGravity(response.error, Toast.SHORT, Toast.CENTER);
          return;
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            name:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        var typeImage = _liFiles[0].type;
        var albumImage = 'ESVN';
        var tagImage = _liFiles[0].uri;
        CameraRoll.save(tagImage, {typeImage, albumImage});
        this.setState({
          ImageCamera: _liFiles,
        });
      });
    } catch (err) {
      console.log(err);
    }
  }
  //#endregion

  //#endregion
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(OpenPhotoLibrary);
