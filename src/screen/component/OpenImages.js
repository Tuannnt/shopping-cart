import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Dimensions, Text, TouchableOpacity, ScrollView, Image, Animated, PermissionsAndroid } from "react-native";
import { Divider, Icon, Header } from 'react-native-elements';
import configApp from '../../configApp';
import RNFetchBlob from 'rn-fetch-blob';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
class OpenImages extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //Animated
            offsetY: new Animated.Value(SCREEN_HEIGHT),
            isOpen: false,
            value: props.value,
            data: props.data,
            Count_Tab: 0,
            Title: ""
        };
        this.state.FileAttackments = {
            renderFileAttackments: function (item) {
                return configApp.url_icon_chat + configApp.link_type_icon + item + '-icon.png';
            },
            renderImage(guid) {
                return configApp.url_Task_ViewImg + guid;
            },
            renderViewImage(guid) {
                return configApp.url_Task_DownloadImg + guid;
            },
        }
        props.openImg(this.openImg);
    }

    openImg = (option) => {
        if (option) {
            if (option.isChangeAction) {
                this.setState({
                    data: option.data,
                })
            }
        }
        if (!this.state.isOpen) {
            this.setState({
                isOpen: true,
            })
        } else {
            setTimeout(() => {
                this.setState({
                    isOpen: false,
                })
            }, 500);
        }
        if (!this.state.isOpen) {
            Animated.timing(
                this.state.offsetY,
                {
                    toValue: SCREEN_HEIGHT,
                    duration: 400,
                    useNativeDriver: true,
                }
            ).start();
        } else {
            Animated.timing(
                this.state.offsetY,
                {
                    toValue: 0,
                    duration: 400,
                    useNativeDriver: true,
                }
            ).start();
        }
        for (let i = 0; i < this.state.data.length; i++) {
            if (this.state.data[i].AttachmentGuid === this.state.value.AttachmentGuid) {
                this.EventViewImage(this.state.value, i);
                break;
            }
        }

    }
    componentDidMount() {
        this.openImg();
    }
    //#region SkillAnimated
    SkillAnimated = () => {
        //bật tab
        this.OpenTabView = Animated.timing(
            this.state.offsetY,
            {
                toValue: 0,
                duration: 400,
                useNativeDriver: true,
            }
        );
        //đóng tab
        this.OffTabView = Animated.timing(
            this.state.offsetY,
            {
                toValue: SCREEN_HEIGHT,
                duration: 400,
                useNativeDriver: true,
            }
        );
    }
    //#endregion

    EventViewImage = (item, index) => {
        this.myScroll.scrollTo({
            x: (
                (SCREEN_WIDTH * this.state.data.length) - ((this.state.data.length - index) * SCREEN_WIDTH)
            )
        });
        this.setState({
            value: item,
            Count_Tab: index
        })

    }
    OnScrollTo = (event, para) => {
        var x = event.nativeEvent.contentOffset.x;
        if (x >= ((this.state.Count_Tab * SCREEN_WIDTH) + (SCREEN_WIDTH / 3))) {
            this.state.value = this.state.data[this.state.Count_Tab + 1];

            this.state.Count_Tab = this.state.Count_Tab + 1;
            this.myScroll.scrollTo({
                x: (
                    (SCREEN_WIDTH * para.length) - ((para.length - this.state.Count_Tab) * SCREEN_WIDTH)
                )
            });
        } else if (x < ((this.state.Count_Tab * SCREEN_WIDTH) - (SCREEN_WIDTH / 3))) {
            if (this.state.Count_Tab !== 0) {
                this.state.value = this.state.data[this.state.Count_Tab - 1]
                this.state.Count_Tab = this.state.Count_Tab - 1
                this.myScroll.scrollTo({
                    x: (
                        (SCREEN_WIDTH * para.length) - ((para.length - this.state.Count_Tab) * SCREEN_WIDTH)
                    )
                });
            }
        } else {
            this.state.value = this.state.data[this.state.Count_Tab]
            this.myScroll.scrollTo({
                x: (
                    (SCREEN_WIDTH * para.length) - ((para.length - this.state.Count_Tab) * SCREEN_WIDTH)
                )
            });
        }
        this.SetTitle();
    }
    SetTitle = () => {
        this.setState({ Title: this.state.value.Title })
    }
    back = () => {
        this.setState({ value: null})
        this.openImg();
    }
    render() {
        return (
            <Animated.View style={{
                backgroundColor: '#CED0CE',
                transform: [{ translateY: this.state.offsetY }],
                borderRadius: 10,
                position: 'absolute',
                zIndex: 1000,
                bottom: 0,
                width: SCREEN_WIDTH
            }}>
                < View style={{ height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: "black" }}>
                    <Header
                        statusBarProps={AppStyles.statusbarProps}
                        {...AppStyles.headerImage}
                        leftComponent={
                            <TouchableOpacity style={{ flex: 1, width: 45, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.back()}>
                                <Icon name="close" type="antdesign" color={"#fff"} size={16} />
                            </TouchableOpacity>
                        }
                        centerComponent={<View style={{ flex: 8, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', color: '#fff' }}>{this.state.value.FileName}</Text></View>}
                        rightComponent={
                            <TouchableOpacity style={{ flex: 1, width: 45, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.DownloadFile(this.state.value)}>
                                <Icon name="download" type="antdesign" color={"#fff"} size={16} />
                            </TouchableOpacity>
                        }
                    />
                    <Divider />
                    <View style={{ flex: 1, marginTop: 5 }}>
                        <View style={{ flex: 5 }}>
                            {this.state.data && this.state.data.length > 0 ? this.CustomViewImage(this.state.data) : null}
                        </View>
                        <View style={{
                            flex: 1,
                            bottom: 0,
                            borderColor: '#fff',
                            borderTopWidth: 0.5,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            {this.state.data && this.state.data.length > 0 ? this.CustomTabbarImage(this.state.data) : null}
                        </View>

                    </View>
                </View >
            </Animated.View>
        );
    }
    CustomTabbarImage = (item) => (
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {item.map((obj, index) => (
                <TouchableOpacity style={{ height: 50, width: 50, margin: 10, borderColor: '#fff' }} onPress={() => this.EventViewImage(obj, index)}>
                    {this.state.value.AttachmentGuid == obj.AttachmentGuid ?
                        (obj.FileExtension == "png" || obj.FileExtension == "jpg" || obj.FileExtension == "jpeg") ?
                            <Image
                                style={{ width: 50, height: 50, borderRadius: 10, borderWidth: 1, borderColor: AppColors.Maincolor }}
                                source={{ uri: this.state.FileAttackments.renderImage(obj.AttachmentGuid) }}
                            />
                            :
                            <View style={{ backgroundColor: "#fff", borderRadius: 10 }}>
                                <Image
                                    style={{ width: 50, height: 50 }}
                                    source={{ uri: this.state.FileAttackments.renderFileAttackments(obj.FileExtension) }}
                                />
                            </View>
                        :
                        (obj.FileExtension == "png" || obj.FileExtension == "jpg" || obj.FileExtension == "jpeg") ?

                            <Image
                                style={{ width: 50, height: 50, borderRadius: 10 }}
                                source={{ uri: this.state.FileAttackments.renderImage(obj.AttachmentGuid) }}
                            />
                            :
                            <View style={{ backgroundColor: "#fff", borderRadius: 10, opacity: 0.5 }}>
                                <Image
                                    style={{ width: 50, height: 50 }}
                                    source={{ uri: this.state.FileAttackments.renderFileAttackments(obj.FileExtension) }}
                                />
                            </View>
                    }
                </TouchableOpacity>
            ))}
        </ScrollView>
    )
    CustomViewImage = (item) => (
        <ScrollView ref={(ref) => { this.myScroll = ref; }}
            onScrollEndDrag={(event) => this.OnScrollTo(event, item)}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={AppStyles.centerComponent}>
            {item.map((obj, i) => (
                <TouchableOpacity key={i} style={[{ justifyContent: 'center', alignItems: 'center', width: SCREEN_WIDTH }]} >
                    {(obj.FileExtension == "png" || obj.FileExtension == "jpg" || obj.FileExtension == "jpeg") ?
                        <Image
                            style={{ width: SCREEN_WIDTH, height: "50%" }}
                            source={{ uri: this.state.FileAttackments.renderViewImage(obj.AttachmentGuid) }}
                        />
                        :
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Image
                                style={{ width: 100, height: 100 }}
                                source={{ uri: this.state.FileAttackments.renderFileAttackments(obj.FileExtension) }}
                            />
                            <Text style={{ color: '#fff', marginTop: 10 }}>{obj.Title}</Text>
                            <Text style={{ color: '#fff' }}>Đây là file {obj.FileExtension}. Không thể hiển thị</Text>
                            <TouchableOpacity style={{ flexDirection: 'row', width: '100%', height: 30, marginTop: 10 }} onPress={() => this.DownloadFile(this.state.value)}>
                                <Icon name="download" type="antdesign" color={'#0066FF'} size={16} />
                                <Text style={{ color: '#0066FF' }}>Tải về</Text>
                            </TouchableOpacity>
                        </View>
                    }
                </TouchableOpacity>
            ))}
        </ScrollView>
    )
    //#region hiển thị nhóm ảnh
    _openGroupIMG() { }
    openGroupIMG = (d) => {
        this._openGroupIMG = d;
    }
    onActionGroupIMG() {
        this._openGroupIMG();
    }
    ChangeGroupIMG = (rs) => {
        try {
            if (rs !== null) {
                this.state.GroupName = rs.value;
                this.setState({
                    GroupName: this.state.GroupName,
                    images: []
                })
                this.end_cursor = undefined;
                this._storeImages();
            }
        } catch (error) {
            console.log(error);
        }
    }
    //#endregion
    //tair file đính kèm về
    DownloadFile(attachObject) {
        let dirs = RNFetchBlob.fs.dirs;
        if (Platform.OS !== "ios") {
            RNFetchBlob
                .config({
                    addAndroidDownloads: {
                        useDownloadManager: true, // <-- this is the only thing required
                        // Optional, override notification setting (default to true)
                        notification: true,
                        // Optional, but recommended since android DownloadManager will fail when
                        // the url does not contains a file extension, by default the mime type will be text/plain
                        mime: '/',
                        description: 'File downloaded by download manager.',
                        //title: new Date().toLocaleString() + ' - test.xlsx',
                        //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
                        path: "file://" + dirs.DownloadDir + '/' + attachObject.Title, //using for android
                    }
                })
                .fetch('GET', configApp.url_Task_DownloadImg + attachObject.AttachmentGuid, {})
                .then((resp) => {
                    Alert.alert(
                        //title
                        'Thông báo',
                        //body
                        'Tải thành công',
                        [
                            { text: 'Đóng', onPress: () => console.log('No Pressed') },
                        ],
                        { cancelable: true }
                    )
                    // the path of downloaded file
                    resp.path()
                })
        }
        else {
            RNFetchBlob
                .config({
                    path: dirs.DocumentDir + '/' + attachObject.Title,
                    addAndroidDownloads: {
                        useDownloadManager: true, // <-- this is the only thing required
                        // Optional, override notification setting (default to true)
                        notification: true,
                        IOSDownloadTask: true,
                        // Optional, but recommended since android DownloadManager will fail when
                        // the url does not contains a file extension, by default the mime type will be text/plain
                        mime: '/',
                        description: 'File downloaded by download manager.',
                        title: new Date().toLocaleString() + '-' + attachObject.Title,
                        path: dirs.DocumentDir + "/" + new Date().toLocaleString() + '-' + attachObject.Title, //using for ios
                        //using for android
                    }

                })
                .fetch('GET', configApp.url_Task_DownloadImg + attachObject.AttachmentGuid, {})
                .then((resp) => {
                    console.log("88888888888:", resp.data)
                    RNFetchBlob.ios.openDocument(resp.data);
                    // the path of downloaded file
                    // resp.path()
                    // CameraRoll.saveToCameraRoll(dirs.DocumentDir + '/' + attachObject.Title)
                    //     .then(
                    //         Alert.alert(
                    //             //title
                    //             'Thông báo',
                    //             //body
                    //             'Tải thành công',
                    //             [
                    //                 { text: 'Đóng', onPress: () => console.log('No Pressed') },
                    //             ],
                    //             { cancelable: true }
                    //         )
                    //         // alert('Tải đính kèm thành công', 'Photo added to camera roll!')
                    //     )
                    //     .catch(err => console.log('err:', err))
                    // alert('Image Downloaded Successfully.');
                })
        }
    }
    //#endregion
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true },)(OpenImages);
