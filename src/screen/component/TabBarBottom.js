import React, {Component} from 'react';
import {Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import TabNavigator from 'react-native-tab-navigator';
import {Icon} from 'react-native-elements';
import Dialog from 'react-native-dialog';
import PopupHandleVote from './PopupHandleVote';
import PopupHandleConfirm from './PopupHandleConfirm';
import {AppStyles, AppSizes, AppColors} from '@theme';
import Toast from 'react-native-simple-toast';
import {FuncCommon} from '@utils';

class TabBarBottom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: '',
      open: false,
      showDialog: '',
      EventClick: false,
      EventClickDelete: false,
      Eventitle: '',
      CommentWF: '',
      Attachment: false,
      showModalConfirm: false,
    };
    this.callbackSubmit1 = this.props.onSubmitWF;
    this.callbackDelete = this.props.onDelete;
    this.callbackUpdate = this.props.onUpdate;
    this.callbackOpenUpdate = this.props.callbackOpenUpdate;
    this.callbackAttachment = this.props.onAttachment;
    //kiểm tra bước đầu tiên
    // Đã hoàn thành

    this.btns = [
      {
        name: this.props.checkInLogin == '1' ? 'Gửi' : 'Duyệt',
        value: 'D',
        color: '#3366CC',
      },
      {
        name: 'Trả lại',
        value: 'TL',
        color: '#EE0000',
      },
      {
        name: 'Hủy',
        value: 'H',
        color: '#979797',
      },
    ];
    this.btnDelete = [
      {
        name: 'Xác nhận',
        value: 'Y',
        color: '#3366CC',
      },
      {
        name: 'Hủy',
        value: 'H',
        color: 'red',
      },
    ];
  }
  componentWillReceiveProps(nextProps) {}
  setSelectedTab = tab => {
    this.setState({selectedTab: tab});
  };

  render() {
    // 1 so phieu ko update, viet lai ViewMenu duoi render
    let ViewMenu = [];
    if (this.props.checkInLogin == '1') {
      ViewMenu = [
        {
          title: 'Chỉnh sửa',
          iconName: 'edit',
          icontype: 'antdesign',
          iconSize: 20,
          onPress: 'OpenEdit',
        },
        {
          title: 'Xử lý',
          iconName: 'send',
          icontype: 'feather',
          iconSize: 20,
          onPress: 'Browser',
        },
        {
          title: 'Xóa',
          iconName: 'delete',
          icontype: 'antdesign',
          iconSize: 20,
          onPress: 'Delete',
        },
        {
          title: 'Đính kèm',
          iconName: 'attachment',
          icontype: 'entypo',
          iconSize: 20,
          onPress: 'Attachment',
        },
        {
          title: 'Ý kiến',
          iconName: 'message1',
          icontype: 'antdesign',
          iconSize: 20,
          onPress: 'Comment',
        },
      ];
    } else if (
      // được duyệt
      this.props.Permisstion === 1
    ) {
      ViewMenu = [
        {
          title: 'Xử lý',
          iconName: 'send',
          icontype: 'feather',
          iconSize: 20,
          onPress: 'Browser',
        },
        {
          title: 'Đính kèm',
          iconName: 'attachment',
          icontype: 'entypo',
          iconSize: 20,
          onPress: 'Attachment',
        },
        {
          title: 'Ý kiến',
          iconName: 'message1',
          icontype: 'antdesign',
          iconSize: 20,
          onPress: 'Comment',
        },
      ];
    }
    // co nut xac nhan
    else if (this.props.isHaveConfirm === true) {
      //   props Confirm:
      //   + btnsConfirm = {[
      //  {   name: 'Xác nhận',
      //  color: 'green',
      //  value: 'Y'},
      //  name: 'Hủy',
      //  color: 'red',
      //  value: 'N'
      //   ]}
      //   + handleConfirm={this.handleSubmitConfirm}
      // (required)
      ViewMenu = [
        {
          title: 'Xác nhận',
          iconName: 'form',
          icontype: 'antdesign',
          iconSize: 20,
          onPress: 'Confirm',
        },
        {
          title: 'Đính kèm',
          iconName: 'attachment',
          icontype: 'entypo',
          iconSize: 20,
          onPress: 'Attachment',
        },
        {
          title: 'Ý kiến',
          iconName: 'message1',
          icontype: 'antdesign',
          iconSize: 20,
          onPress: 'Comment',
        },
      ];
    } // default
    else {
      ViewMenu = [
        {
          title: 'Đính kèm',
          iconName: 'attachment',
          icontype: 'entypo',
          iconSize: 20,
          onPress: 'Attachment',
        },
        {
          title: 'Ý kiến',
          iconName: 'message1',
          icontype: 'antdesign',
          iconSize: 20,
          onPress: 'Comment',
        },
      ];
    }
    return (
      <View
        style={{
          flexDirection: 'row',
          height: '100%',
          borderTopWidth: 0.3,
          borderTopColor: AppColors.gray,
        }}>
        {ViewMenu.length > 0
          ? ViewMenu.map((item, index) => {
              if (!this.props.onAttachment && item.iconName === 'attachment') {
                return null;
              }
              if (!this.props.callbackOpenUpdate && item.iconName === 'edit') {
                return null;
              }
              if (!this.props.keyCommentWF && item.iconName === 'message1') {
                return null;
              }
              if (!this.props.onSubmitWF && item.iconName === 'send') {
                return null;
              }
              if (!this.props.onDelete && item.iconName === 'delete') {
                return null;
              }
              return (
                <TouchableOpacity
                  key={item.iconName}
                  style={[
                    AppStyles.containerCentered,
                    {flex: 1, paddingTop: 5},
                  ]}
                  onPress={() => this.onPressClick(item.onPress)}>
                  <View style={{flex: 1, justifyContent: 'center'}}>
                    <Icon
                      iconStyle={AppStyles.TabNavigator_Icon}
                      name={item.iconName}
                      type={item.icontype}
                    />
                  </View>
                  <View style={{flex: 1, alignItems: 'center'}}>
                    <Text style={AppStyles.Textsmall}>{item.title}</Text>
                  </View>
                </TouchableOpacity>
              );
            })
          : null}
        {this.state.Eventitle === 'B'
          ? this.renderPopup('Duyệt phiếu: ' + this.props.Title, this.btns)
          : this.state.Eventitle === 'R'
          ? this.renderPopup('Trả lại: ' + this.props.Title, this.btns)
          : null}
        {this.renderdelete(this.props.Title, this.btnDelete)}
        {this.state.showModalConfirm && this.renderConfirm()}
      </View>
    );
  }

  //Begin cretaed popup
  renderPopup(title, btns) {
    if (this.props.checkInLogin == '1') {
      btns = btns.filter(btn => btn.value !== 'TL');
    }
    return this.state.EventClick === true ? (
      <PopupHandleVote
        onSubmit={(callback, object) => this.callbackSubmit(callback, object)}
        onClick={callback => this.clickPopup(callback)}
        title={title}
        desciption={'Nhập nội dung'}
        btns={btns}
        listApprover={this.props.listApprover}
        checkInLogin={this.props.checkInLogin}
      />
    ) : null;
  }
  // render Confirm
  renderConfirm = () => {
    return (
      <PopupHandleConfirm
        submit={this.handleSubmitConfirm}
        btns={this.props.btnsConfirm}
      />
    );
  };
  // begin delete
  renderdelete(title, btns) {
    return this.state.EventClickDelete === true ? (
      <View>
        <Dialog.Container visible={true}>
          <Dialog.Title>Xác nhận xóa:</Dialog.Title>
          <Dialog.Description>
            Bạn có chắc chắn muốn xóa {title} không?
          </Dialog.Description>
          {btns && btns.length > 0
            ? btns.map((item, index) => (
                <Dialog.Button
                  style={{color: item.color}}
                  label={item.name}
                  onPress={() => this.Delete(item.value)}
                />
              ))
            : null}
        </Dialog.Container>
      </View>
    ) : null;
  }
  handleClick = para => {
    console.log(para);
    switch (para) {
      case 'Home': {
        Actions.app();
        break;
      }
      case 'OpenEdit': {
        this.callbackOpenUpdate();
        break;
      }
      case 'Browser': {
        this.callbackPopup(para);
        this.setState({
          EventClick: true,
          Eventitle: 'B',
        });
        break;
      }
      case 'Return': {
        this.callbackPopup(para);
        this.setState({
          EventClick: true,
          Eventitle: 'R',
        });
        break;
      }
      case 'Delete': {
        this.setState({
          EventClickDelete: true,
        });
        break;
      }
      case 'Edit': {
        this.callbackUpdate();
        break;
      }
      case 'Attachment': {
        this.callbackAttachment();
        break;
      }
      case 'Confirm': {
        this.callbackConfirm();
        break;
      }
      case 'Comment': {
        console.log(this.props.keyCommentWF.Type);
        Actions.commentWF({
          ModuleId: this.props.keyCommentWF.ModuleId,
          RecordGuid: this.props.keyCommentWF.RecordGuid,
          Title: this.props.keyCommentWF.Title,
          getByGuid: this.props.keyCommentWF.getByGuid,
          Type: this.props.keyCommentWF.Type,
        });
        break;
      }
      default: {
        Actions.push(para);
        break;
      }
    }
  };
  onPressClick(para) {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.handleClick(para);
      } else {
        Toast.showWithGravity(
          'Vui lòng kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      }
    });
  }

  callbackSubmit(callback, object) {
    this.setState({
      EventClick: false,
    });
    if (callback != 'H') {
      this.callbackSubmit1(callback, object.text, object.approver);
    }
  }
  // xác nhận xóa
  Delete = para => {
    if (para == 'Y') {
      this.callbackDelete();
      EventClickDelete: false;
    }
    this.setState({
      EventClickDelete: false,
    });
  };
  callbackPopup() {}
  clickPopup(callback) {
    this.callbackPopup = callback;
  }
  sendNotification = data => {};
  // xác nhận thông tin
  callbackConfirm = () => {
    this.setState({showModalConfirm: true});
  };
  handleSubmitConfirm = value => {
    this.setState({showModalConfirm: false}, () => {
      this.props.handleConfirm(value);
    });
    return;
  };
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(TabBarBottom);
