import React, {Component, useRef, useEffect} from 'react';
import {connect} from 'react-redux';
import {
  Header,
  Icon,
  ListItem,
  SearchBar,
  Badge,
  Divider,
} from 'react-native-elements';
import {
  Text,
  View,
  Animated,
  Dimensions,
  Easing,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {Directions} from 'react-native-gesture-handler';
import {AppStyles, AppColors} from '@theme';
import {Actions} from 'react-native-router-flux';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class MenuActionCompoment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      position: props.position,
      offsetY:
        props.position === 'top'
          ? new Animated.Value(props.data.length * -50)
          : props.position === 'bottom'
          ? new Animated.Value(props.data.length * 50 + 30)
          : '',
      isOpen: false,
      height:
        props.position === 'top'
          ? props.data.length * -110
          : props.position === 'bottom'
          ? props.data.length * 50 + 50
          : '',
      data: props.data,
      NameMenu: props.nameMenu ? props.nameMenu : 'Chức năng',
      offsetopacity: new Animated.Value(0),
    };
    this.ae(this.state.data);
    props.eOpen(this.eOpen);
    this.actionClick = props.callback;
  }
  checkPosition = position => {
    switch (position) {
      case 'top':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      case 'bottom':
        this.setState({
          offsetY: new Animated.Value(props.data.length * 50 + 30),
          height: props.data.length * 50 + 30,
        });
        break;

      case 'left':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      case 'right':
        this.setState({
          offsetY: new Animated.Value(props.data.length * -50),
          height: props.data.length * -110,
        });
        break;

      default:
        Alert.alert('NUMBER NOT FOUND');
    }
  };
  ae(d) {
    for (var i = 0; i < d.length; i++) {
      if (d[i].icon === undefined) {
        d[i].icon = {
          name: 'grid',
          type: 'entypo',
          color: '#f6b801',
        };
      }
      if (d[i].key === undefined) {
        d[i].key = new Date().getTime();
      }
      d[i].callBack = this.callBack;
    }
  }
  actionClick() {}
  eOpen = option => {
    if (option) {
      if (option.isChangeAction) {
        this.ae(option.data);
        this.setState({
          data: option.data,
        });
      }
      if (option.NameMenu !== undefined && option.NameMenu !== null) {
        this.setState({
          NameMenu: option.NameMenu,
        });
      }
    }
    if (!this.state.isOpen) {
      this.setState({
        isOpen: true,
      });
    } else {
      setTimeout(() => {
        this.setState({
          isOpen: false,
        });
      }, 200);
    }
    //this.checkPosition(this.props.position);
    this.openAction();
  };
  componentDidMount() {}

  openAction = () => {
    if (!this.state.isOpen) {
      Animated.timing(this.state.offsetY, {
        toValue: 0,
        // duration: 1000,
        // easing: Easing.ease
        duration: 200,
        useNativeDriver: true,
      }).start();
      Animated.timing(this.state.offsetopacity, {
        toValue: 1,
        // duration: 1000,
        // easing: Easing.ease
        duration: 200,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this.state.offsetY, {
        toValue: this.state.height,
        // duration: 1000,
        // easing: Easing.ease
        duration: 200,
        useNativeDriver: true,
      }).start();
      Animated.timing(this.state.offsetopacity, {
        toValue: 0,
        // duration: 1000,
        // easing: Easing.ease
        duration: 200,
        useNativeDriver: true,
      }).start();
    }
  };
  callBack = d => {
    this.actionClick(d);
    this.eOpen();
  };
  render() {
    const opacityTabView = this.state.offsetopacity.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 0.5],
    });
    return this.state && this.state.isOpen ? (
      <Animated.View
        style={[
          {
            width: DRIVER.width,
            position: 'absolute',
            height: DRIVER.height + 10,
          },
        ]}>
        <Animated.View
          style={[
            {
              backgroundColor: 'black',
              opacity: opacityTabView,
              height: DRIVER.height,
            },
          ]}>
          <TouchableOpacity style={{flex: 1}} onPress={() => this.eOpen()} />
        </Animated.View>
        <View
          style={[
            this.state.position === 'top'
              ? {top: 0}
              : this.state.position === 'bottom'
              ? {bottom: 0}
              : '',
            {position: 'absolute', width: '100%', paddingBottom: 20},
          ]}>
          <Animated.View
            style={{
              backgroundColor: '#CED0CE',
              transform: [{translateY: this.state.offsetY}],
              borderRadius: 10,
              bottom: 0,
            }}>
            <View
              style={{
                flexDirection: 'column',
                backgroundColor: 'white',
                bottom: 0,
                height: this.state.height,
                borderRadius: 10,
                opacity: 1,
              }}>
              {this.state.position === 'bottom' ? (
                <View>
                  <TouchableOpacity
                    style={{flexDirection: 'row', padding: 10}}
                    onPress={() => this.eOpen()}>
                    <View style={{flex: 1}}>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontWeight: 'bold',
                          fontSize: 18,
                        }}>
                        {this.state.NameMenu.length > 30
                          ? this.state.NameMenu.substring(0, 30) + ' ...'
                          : this.state.NameMenu}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      right: 10,
                      position: 'absolute',
                      top: 10,
                      zIndex: 10,
                    }}
                    onPress={() => this.eOpen()}>
                    <Icon
                      color={AppColors.gray}
                      name={'close'}
                      type="antdesign"
                    />
                  </TouchableOpacity>
                  <Divider />
                </View>
              ) : null}
              {this.state.data && this.state.data.length > 0
                ? this.state.data.map(function(item, i) {
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          item.callBack(item.key);
                        }}>
                        <View>
                          <View
                            style={{
                              flexDirection: 'row',
                              paddingTop: 10,
                              paddingBottom: 10,
                            }}>
                            <View style={{width: 40, justifyContent: 'center'}}>
                              <Icon
                                color={item.icon.color}
                                name={item.icon.name}
                                type={item.icon.type}
                              />
                            </View>
                            <View style={{flex: 10, justifyContent: 'center'}}>
                              <Text>{item.name}</Text>
                            </View>
                          </View>
                          <View
                            style={{
                              borderBottomColor: '#eff0f1',
                              borderBottomWidth: 1,
                            }}
                          />
                        </View>
                      </TouchableOpacity>
                    );
                  })
                : null}
              {this.state.position === 'top' ? (
                <View>
                  <TouchableOpacity
                    style={{flexDirection: 'row', padding: 10}}
                    onPress={() => this.eOpen()}>
                    <View style={{width: 40, flex: 11}}>
                      <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                        {this.state.NameMenu.length > 30
                          ? this.state.NameMenu.substring(0, 30) + ' ...'
                          : this.state.NameMenu}
                      </Text>
                    </View>
                    <View
                      style={{
                        right: 10,
                        flex: 1,
                      }}>
                      <Icon
                        color={AppColors.gray}
                        name={'close'}
                        type="antdesign"
                      />
                    </View>
                  </TouchableOpacity>
                  <Divider />
                </View>
              ) : null}
            </View>
          </Animated.View>
        </View>
      </Animated.View>
    ) : null;
  }
}
const styles = StyleSheet.create({
  positionTop: {
    top: 80,
  },
  positionBottom: {
    bottom: -10,
  },
});
export default MenuActionCompoment;
