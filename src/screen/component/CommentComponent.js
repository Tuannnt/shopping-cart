import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Keyboard,
    TouchableOpacity,
    Image,
    View,
    TextInput,
    Text,
    StyleSheet,
    StatusBar,
    FlatList,
    ScrollView,
    ActivityIndicator, RefreshControl, PermissionsAndroid
} from 'react-native';
import {
    Header,
    Divider,
    Button,
    Input,
    ListItem,
    SearchBar,
    Icon,
    Body,
    Left,
    Thumbnail,
    Avatar
} from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Fonts from '../../theme/fonts';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import API_Comment from '../../network/Comment/API_Comment'
import API from '../../network/API'
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";
import signalr from 'react-native-signalr';
class CommentComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            List: [],
            isFetching: false,
            CurrentPage: 1,
            Length: 100,
            LoginName: '',
            Comment: "",
            ListAttachments: [],
            LiFiles: [],
        };
    }
    ConvertTime = (date) => {
        if (date !== null && date != "" && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            var newday = new Date();
            if (newday.getDate() == day) {
                return hh + ":" + mm;
            } else {
                return day + "/" + month + "/" + year + ' ' + hh + ":" + mm;
            }
        } else {
            return null;
        }
    };
    async request_storage_runtime_permission() {

        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    'title': 'ReactNativeCode Storage Permission',
                    'message': 'ReactNativeCode App needs access to your storage to download Photos.'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                console.log("Storage Permission Granted.");
            }
            else {

                console.log("Storage Permission Not Granted");

            }
        } catch (err) {
            console.warn(err)
        }
    }
    async componentDidMount(): void {
        //Xin quyền
        await this.request_storage_runtime_permission();
        //Lấy thông tin người đăng nhập
        API.getProfile().then(rs => {
            console.log(('callback==_______________++>>>>>>' + rs.data.LoginName))
            this.setState({ LoginName: rs.data.LoginName });
        })
        //Lấy danh sách ý kiến
        this.GetListAll();
        //signalr
        var connection = signalr.hubConnection(configApp.url_hubConnection
            // ,{
            //   qs: {
            //     access_token:
            //     'YOUR_API_TOKEN',
            //   },
            // }
        );
        connection.logging = true;
        this.proxy = connection.createHubProxy('ChatHub');

        //receives broadcast messages from a hub function, called "messageFromServer"
        this.proxy.on('broadcastMessage', (name, message) => {
            var mes = JSON.parse(message);
            if (mes.TableName === this.props.TableName && mes.RecordGuid === this.props.RecordGuid) {
                var _LiComment = this.state.List;
                _LiComment.push({
                    CommentGuid: mes.CommentGuid,
                    LoginName: mes.LoginName,
                    EmployeeGuid: mes.EmployeeGuid,
                    EmployeeName: mes.EmployeeName,
                    CreatedDate: mes.CreatedDate,
                    Comment: mes.Comment,
                    CommentAttachments: mes.CommentAttachments
                });
                this.setState(
                    {
                        List: _LiComment
                    }
                );
            }
        });
        // atempt connection, and handle errors
        connection.start().done(() => {
            console.log('Now connected, connection ID=' + connection.id);
            console.log('Now connected');
        }).fail(() => {
            console.log('Failed');
        });

        //connection-handling
        connection.connectionSlow(function () {
            console.log('We are currently experiencing difficulties with the connection.')
        });

        connection.error(function (error) {
            console.log('SignalR error: ' + error)
        });
    }
    //Scroll to top
    upButtonHandler = () => {
        //OnCLick of Up button we scrolled the list to top
        this.ListView_Ref.scrollToOffset({ offset: 0, animated: true });
    };
    //Scroll to end
    downButtonHandler = () => {
        //OnCLick of down button we scrolled the list to bottom
        this.ListView_Ref.scrollToEnd({ animated: true });
    };
    //Scroll to Index
    goIndex = () => {
        this.ListView_Ref.scrollToIndex({ animated: true, index: 5 });
    };
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: '#CED0CE',
                }}>
                <ActivityIndicator animating size="large" />
            </View>
        );
    };
    //Message
    ListEmpty = () => {
        if (this.state.List.length > 0) return null;
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    //Upload file
    async uploadFile() {
        // Pick a single file
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            var _liFiles = this.state.LiFiles;
            _liFiles.push(res);
            this.setState(
                {
                    LiFiles: _liFiles
                }
            );
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }
    //Attachment
    Attachment = (item) => {
        if (item == null) return null;
        return (
            <View style={{ backgroundColor: '#eff0f1', width: '50%', borderRadius: 5 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                    <Text style={{ textAlign: 'left' }}>{item.split('|')[1]}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    {
                        item.split('|')[1].split('.')[1] === 'txt' ?
                            <Icon onPress={() => this.onDownload(item)}
                                name='filetext1'
                                type='antdesign'
                                size={40}
                            /> : (item.split('|')[1].split('.')[1] === 'xlsx' || item.split('|')[1].split('.')[1] === 'xls') ?
                                <Icon onPress={() => this.onDownload(item)}
                                    name='exclefile1'
                                    type='antdesign'
                                    size={40}
                                />
                                : (item.split('|')[1].split('.')[1] === 'docx' || item.split('|')[1].split('.')[1] === 'doc') ?
                                    <Icon onPress={() => this.onDownload(item)}
                                        name='wordfile1'
                                        type='antdesign'
                                        size={40}
                                    />
                                    : (item.split('|')[1].split('.')[1] === 'pdf') ?
                                        <Icon onPress={() => this.onDownload(item)}
                                            name='pdffile1'
                                            type='antdesign'
                                            size={40}
                                        />
                                        : (item.split('|')[1].split('.')[1] === 'jpg' || item.split('|')[1].split('.')[1] === 'jpeg') ?
                                            <Icon onPress={() => this.onDownload(item)}
                                                name='jpgfile1'
                                                type='antdesign'
                                                size={40}
                                            />
                                            : <Icon onPress={() => this.onDownload(item)}
                                                name='file1'
                                                type='Feather'
                                                size={40}
                                            />
                    }
                </View>

            </View>
        );
    };
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <Header
                    leftComponent={this.renderLeftMenu()}
                    centerComponent={{
                        text: 'Nội dung trao đổi',
                        style: { color: 'black', fontSize: 20, marginLeft: -30 },
                    }}
                    rightComponent={this.renderRightMenu()}
                    containerStyle={{ backgroundColor: '#fff', height: 60 }}
                />
                <View style={{ flex: 10 }}>
                    <FlatList
                        style={{ backgroundColor: '#fff' }}
                        refreshing={this.state.isFetching}
                        ref={ref => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.CommentGuid}
                        data={this.state.List}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family }}>
                                <ListItem
                                    key={item}
                                    Component={() => {
                                        return (
                                            item.LoginName == this.state.LoginName ?
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ width: '85%' }}>
                                                        {(item.Comment.length != 4000) ? <View style={{ flexDirection: 'row' }}>
                                                            <Text style={[styles.boxComment, { backgroundColor: '#24c79f', color: '#fff' }]}>
                                                                {item.Comment}
                                                            </Text>
                                                        </View> : null}
                                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                                            <Text style={{ fontSize: 11, color: '#6c737c' }}>
                                                                {item.EmployeeName} {this.ConvertTime(item.CreatedDate)}
                                                            </Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                                            {this.Attachment(item.CommentAttachments)}
                                                        </View>
                                                    </View>
                                                    <View style={{ width: '15%', alignItems: 'center' }}>
                                                        <Avatar
                                                            size="medium"
                                                            rounded
                                                            source={API_Comment.GetPicApplyLeaves(item.EmployeeGuid)}
                                                            containerStyle={{ margin: 5 }}
                                                        />

                                                    </View>
                                                </View>
                                                :
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ width: '15%', alignItems: 'center' }}>
                                                        <Avatar
                                                            size="medium"
                                                            rounded
                                                            source={API_Comment.GetPicApplyLeaves(item.EmployeeGuid)}
                                                            containerStyle={{ margin: 5 }}
                                                        />

                                                    </View>
                                                    <View style={{ width: '85%' }}>
                                                        {item.Comment != "" ? <View style={{ flexDirection: 'row' }}>
                                                            <Text style={[styles.boxComment, { backgroundColor: '#eff0f1' }]}>
                                                                {item.Comment}
                                                            </Text>
                                                        </View> : null}
                                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                                            <Text style={{ fontSize: 11, color: '#6c737c' }}>
                                                                {item.EmployeeName} {this.ConvertTime(item.CreatedDate)}
                                                            </Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                                            {this.Attachment(item.CommentAttachments)}
                                                        </View>
                                                    </View>
                                                </View>
                                        )
                                    }}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            this.nextPage();
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                        onContentSizeChange={() => {
                            this.ListView_Ref.scrollToEnd({ animated: true });
                        }}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <View style={styles.boxBottom}>
                        <TextInput
                            style={{ flex: 1 }}
                            placeholder="Nhập nội dung"
                            onChangeText={(text) => this.setState({ Comment: text })}
                            value={this.state.Comment}
                            ref="commentInput"
                        ></TextInput>
                        <View style={{ flexDirection: 'row' }}>
                            {this.state.refreshing == false && (this.state.Comment != '' || this.state.LiFiles.length > 0) ? <View style={[styles.container, { width: 40, height: 40, marginTop: 7, marginBottom: 7, marginLeft: 3, marginRight: 3, textAlign: 'center', justifyContent: "center" }]} onPress={() => this.sendComment()}>
                                <Icon onPress={() => this.sendComment()}
                                    name='send'
                                    type='Feather'
                                />
                            </View> : null}
                            {this.state.refreshing == false ? <View style={[styles.container, { width: 40, height: 40, marginTop: 7, marginBottom: 7, marginLeft: 3, marginRight: 3, textAlign: 'center', justifyContent: "center", }]} onPress={() => this.uploadFile()}>
                                <Icon onPress={() => this.uploadFile()}
                                    name='attach-file'
                                    type='Feather'
                                />
                            </View> : null}
                        </View>
                    </View>
                </View>


            </View>
        );
    }
    renderLeftMenu = () => {
        return (
            <View>
                <Icon
                    style={{ color: 'black' }}
                    name={'arrowleft'}
                    type="antdesign"
                    size={20}
                    onPress={() => Actions.pop()}
                />
            </View>
        );
    };
    renderRightMenu = () => {
        return (
            <View>
                <Icon
                    style={{ padding: 5, margin: 0 }}
                    name="plus-square"
                    size={20}
                    color="#fff"
                />
            </View>
        );
    };
    //Lấy dữ liệu
    GetListAll = () => {
        this.setState({ refreshing: true });
        let obj = {
            Page: this.state.CurrentPage,
            Length: this.state.Length,
            RecordGuid: this.props.RecordGuid,
            ModuleId: this.props.ModuleId,
            ModuleIdDocument: this.props.ModuleIdDocument
        }
        API_Comment.getAllCommentPage(obj)
            .then(res => {
                this.state.List = this.state.List.concat(JSON.parse(res.data.data).reverse())
                this.setState({ List: this.state.List });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                this.setState({ refreshing: false });
            });
    };
    //Thêm mới ý kiến
    sendComment = () => {
        this.setState({ refreshing: true });
        let _obj = {
            RecordGuid: this.props.RecordGuid,
            ModuleId: this.props.ModuleId,
            Comment: this.state.Comment,
            ModuleIdDocument: this.props.ModuleIdDocument
        };
        let fd = new FormData();
        for (var i = 0; i < this.state.LiFiles.length; i++) {
            fd.append(this.state.LiFiles[i].name, this.state.LiFiles[i]);
        }
        fd.append('insert', JSON.stringify(_obj));
        API_Comment.Insert(fd).then(rs => {
            debugger
            if (rs.status !== 200) {
                this.setState({ refreshing: false });
            } else {
                var cm = JSON.parse(rs.data.data);
                this.proxy.invoke('Send', 'me', JSON.stringify({ TableName: this.props.TableName, RecordGuid: this.props.RecordGuid, CommentGuid: cm.CommentGuid, LoginName: cm.LoginName, EmployeeGuid: cm.EmployeeGuid, EmployeeName: cm.EmployeeName, CreatedDate: cm.CreatedDate, Comment: cm.Comment, CommentAttachments: cm.CommentAttachments }));
                this.onRefresh();
                this.setState({ refreshing: false });
            }
        }).catch(error => {
            this.setState({ refreshing: false });
        });
    }
    onDownload(item) {
        const headers = {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + AccessTokenManager.accessToken,
        };
        let obj = {
            RecordGuid: item.split('|')[0],
            ModuleIdDocument: this.props.ModuleIdDocument
        }
        let dirs = RNFetchBlob.fs.dirs;
        console.log(dirs.DownloadDir);
        RNFetchBlob
            .config({
                addAndroidDownloads: {
                    useDownloadManager: true, // <-- this is the only thing required
                    // Optional, override notification setting (default to true)
                    notification: true,
                    // Optional, but recommended since android DownloadManager will fail when
                    // the url does not contains a file extension, by default the mime type will be text/plain
                    mime: '/',
                    description: 'File downloaded by download manager.',
                    //title: new Date().toLocaleString() + ' - test.xlsx',
                    //path : dirs.DocumentDir + new Date().toLocaleString() + ' - test.xlsx', //using for ios
                    path: "file://" + dirs.DownloadDir + '/' + item.split('|')[1], //using for android
                }
            })
            .fetch('POST', configApp.url_api_mb + '/api/Comment/DownloadAttachments', JSON.stringify(obj))
            .then((resp) => {
                // the path of downloaded file
                resp.path()
            })
        // API_Comment.DownloadAttachments(obj)
        //     .then(res => {
        //         console.log("++++++++++++"+res)
        //     })
        //     .catch(error => {
        //     });
    }
    //Load lại
    onRefresh = () => {
        this.state.CurrentPage = 1;
        //this.state.List = [];
        this.state.Comment = "";
        this.state.LiFiles = [];
        this.setState({
            List: this.state.List,
            CurrentPage: 1,
            Comment: this.state.Comment,
            LiFiles: this.state.LiFiles
        });
        //this.GetListAll();
    }
    nextPage() {
        if (this.state.CurrentPage != 1) {
            this.state.CurrentPage++;
            this.setState({
                CurrentPage: this.state.CurrentPage
            });
            this.GetListAll();
        }
    }
    onPressHome() {
        Actions.home();
    }
}
const styles = StyleSheet.create({
    container: {
        borderRadius: 100,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    },
    containerfile: {
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    },
    containerContent: {
        flex: 1,
        marginBottom: 50,
        backgroundColor: 'white',
        marginLeft: 0
    },
    boxComment: {
        flex: 1, padding: 3, justifyContent: "center",
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        margin: 10,
        paddingLeft: 10,
        paddingRight: 10
    },
    boxBottom: {
        flex: 1,
        width: '100%',
        position: "absolute",
        height: 50,
        bottom: 0,
        borderTopColor: '#eff0f1',
        borderTopWidth: 1,
        flexDirection: 'row',
        backgroundColor: 'white'
    },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CommentComponent);
