import React, { Component, useRef, useEffect } from 'react';
import { connect } from "react-redux";
import { Text, View, Animated, Dimensions, Easing, TouchableOpacity, StyleSheet, ScrollView, KeyboardAvoidingView } from "react-native";
import { Icon, ListItem, Header, Divider } from 'react-native-elements';
import { AppStyles, AppColors } from '@theme';
import { Actions } from 'react-native-router-flux';
import FormSearch from './FormSearch';
import { stubFalse } from 'lodash';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
const Timeout = 400
class ComboboxV2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            offsetY: new Animated.Value(DRIVER.height),
            opacityTabView: new Animated.Value(0),
            isOpen: false,
            height: props.data.length * 60 + 50,
            dataView: props.data,
        };
        props.eOpen(this.eOpen);
        this.actionClick = props.callback;
    }
    actionClick() { }

    componentDidMount() {
        this.ae();
        this.skill();
    }
    skill = () => {
        //mở tab
        this.openTabView = Animated.timing(
            this.state.offsetY,
            {
                toValue: 0,
                duration: Timeout,
                useNativeDriver: true,
            }
        );
        //đóng tab
        this.offTabView = Animated.timing(
            this.state.offsetY,
            {
                toValue: DRIVER.height,
                duration: Timeout,
                useNativeDriver: true,
            }
        );
        // mờ nền
        this.opacityTabView = Animated.timing(
            this.state.opacityTabView,
            {
                toValue: 1,
                duration: Timeout,
                useNativeDriver: true,
            }
        );
        //hết mờ
        this.OffopacityTabView = Animated.timing(
            this.state.opacityTabView,
            {
                toValue: 0,
                duration: Timeout,
                useNativeDriver: true,
            }
        );
    }
    //#region Xử lý dữ liệu truyền vào
    ae() {
        var itemselect = { text: "", value: null }
        this.actionClick(itemselect);
    }
    //#endregion

    //#region  đóng cửa sổ
    eOpen = (option) => {
        if (option) {
            if (option.isChangeAction) {
                this.ae();
                this.setState({
                    data: option.data,
                })
            }
        }
        if (!this.state.isOpen) {
            this.setState({ isOpen: true, })
        } else {
            setTimeout(() => { this.setState({ isOpen: false, }) }, Timeout);
        }
        this.openAction();
    }
    //#endregion

    //#region mở cửa sổ
    openAction = () => {
        if (!this.state.isOpen) {
            Animated.sequence([this.openTabView]).start();
            Animated.sequence([this.opacityTabView]).start();
        } else {
            Animated.sequence([this.offTabView]).start();
            Animated.sequence([this.OffopacityTabView]).start();
        }
    }
    //#endregion


    //#region chọn nhiều chọn 1
    SaveValues = (item) => {
        var _submit = { text: item !== null ? item.text : "", value: item !== null ? item.value : null }
        this.actionClick(_submit);
        this.eOpen();
    }
    //#endregion

    render() {
        const opacityTabView = this.state.opacityTabView.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0.3]
        })
        return (
            this.state && this.state.isOpen ?
                <View style={[{ bottom: 0 }, { position: 'absolute', width: '100%' }]}>
                    <Animated.View style={[styles.positionBottom, { width: DRIVER.width, position: "absolute", backgroundColor: 'black', opacity: opacityTabView, height: DRIVER.height + 10 }]} >
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => { this.eOpen() }} />
                    </Animated.View>
                    <Animated.View style={{
                        margin: 10,
                        transform: [{ translateY: this.state.offsetY }],
                        borderRadius: 10,
                        bottom: 0
                    }}>
                        <View style={{
                            backgroundColor: 'rgba(52, 52, 52,0)',
                            bottom: 0,
                            borderRadius: 10,
                        }}>
                            <View style={{
                                backgroundColor: 'rgba(255, 255, 255, 1)',
                                bottom: 0,
                                borderRadius: 10,
                            }}>
                                {this.state.dataView && this.state.dataView.length > 0 && this.state.dataView.map((item, i) => {
                                    return (
                                        <TouchableOpacity key={i} style={[AppStyles.containerCentered, { padding: 10, height: 50 }]} onPress={() => { this.SaveValues(item) }}>
                                            <Text style={{ fontSize: 18, color: '#0a87eb' }}>
                                                {item.text}
                                            </Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </View>
                            <TouchableOpacity style={[AppStyles.containerCentered, { padding: 10, height: 50, marginTop: 10, backgroundColor: "#fff", borderRadius: 10 }]} onPress={() => { this.SaveValues(null) }}>
                                <Text style={{ fontSize: 18, color: '#0a87eb', fontWeight: 'bold' }}>Huỷ</Text>
                            </TouchableOpacity>
                        </View>
                    </Animated.View>

                </View>
                : null
        ); 
    }
}
const styles = StyleSheet.create({
    positionTop: {
        top: 80
    },
    positionBottom: {
        bottom: -10
    }
})
export default ComboboxV2;