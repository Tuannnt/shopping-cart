import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView,
  ImageBackground,
  StyleSheet,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Divider, Icon, ListItem} from 'react-native-elements';
import {API_Follow} from '@network';
import TabBar from '../../component/TabBar';
import {AppStyles, AppColors} from '@theme';
import FormSearch from '../../component/FormSearch';
import SearchDay from '../../component/SearchDay';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import * as Progress from 'react-native-progress';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import {FuncCommon} from '../../../utils';
import {WebView} from 'react-native-webview';
import LoadingComponent from '../../component/LoadingComponent';
import {Container} from 'native-base';
import Combobox from '../../component/Combobox';
import sizes from '../../../theme/sizes';
import fonts from '../../../theme/fonts';
import {bool} from 'prop-types';
import {ECharts} from 'react-native-echarts-wrapper';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const MenuBottom = [
  {
    key: 'Kanban',
    name: 'Kanban',
    icon: {
      name: 'eye-outline',
      type: 'material-community',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'X',
    name: 'Chờ giao',
    icon: {
      name: 'edit',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'F',
    name: 'Theo dõi',
    icon: {
      name: 'attachment',
      type: 'entypo',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'DG',
    name: 'Đã giao',
    icon: {
      name: 'message1',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'CVL',
    name: 'Công việc lưu',
    icon: {
      name: 'calendar-check-o',
      type: 'font-awesome',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'W',
    name: 'Chờ người khác',
    icon: {
      name: 'delete',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'D',
    name: 'Hoãn lại',
    icon: {
      name: 'check-circle',
      type: 'feather',
      color: AppColors.Maincolor,
    },
  },
];
const stylesData = StyleSheet.create({
  PERMACHINE: {
    height: 300,
    padding: 1,
    borderColor: '#eee',
    borderBottomWidth: 1,
  },
});

class ProductionFollowComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingSearch: false,
      offload: false,
      loading: false,
      data_DATA: [],
      data_EVENT: [],
      data_FOLLOW: [],
      ActiveTab: 'FOLLOW',
      EchartElement: [],
      ActiveTitle: 'Màn hình máy hoạt động',
      DataPERMACHINEChart: {
        color: [],
        grid: {
          containLabel: true,
          right: 20,
          left: 30,
          height: 200,
        },
        xAxis: {
          data: [],
          axisLabel: {
            interval: 0,
            rotate: 90,
            fontSize: 6,
          },
        },
        yAxis: {
          type: 'value',
        },
        legend: {
          data: [],
        },
        series: [],
      },
      DataPERMACHINEChart_PIE: [],
      DataPERMACHINESChart: {
        color: [],
        grid: {
          containLabel: true,
          right: 20,
          left: 30,
          height: 200,
        },
        xAxis: {
          data: [],
          axisLabel: {
            interval: 0,
            rotate: 90,
            fontSize: 6,
          },
        },
        yAxis: {
          type: 'value',
        },
        legend: {
          data: [],
        },
        series: [],
      },
      DataPERMACHINESChart_PIE: [],
      DataQUANTITYChart: {
        tooltip: {
          trigger: 'item',
          formatter: '{b}: {c}',
          axisPointer: {
            type: 'shadow',
          },
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true,
        },
        legend: {
          data: [],
          display: true,
        },
        xAxis: {
          type: 'category',
          data: [],
        },
        yAxis: {
          type: 'value',
        },
        series: [],
      },
    };
    this.KeyDate = new Date().getTime();
    global.PRO_FOLLOW = {
      KeyDate: this.KeyDate,
    };
    this.DataBase = {
      Machines: [],
      Machinetypes: [],
      LiState: [],
    };
    this.findSearch = {
      MachineTypeIds: [],
      MachineIds: [
        '008db3dc-3efe-48ce-8e66-c71bcf814e2f',
        '0a315cc6-6638-48b6-bd24-89ef8faa3b93',
        '1f184048-703a-49b0-957d-ff0dc3ea0251',
        '23921ba8-3b38-4180-88d5-b387c22c0d24',
        '296947e4-da09-4e32-9cbc-4ad95967108c',
      ],
      ShiftIds: [],
      CheckAllDataMachine: false,
      StartDate: new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate(),
      ),
      EndDate: new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate(),
        23,
        59,
        59,
      ),
    };
    this.listtabbarBotom = [
      {
        Title: 'Theo dõi',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'FOLLOW',
        Checkbox: false,
      },
      {
        Title: 'Dữ liệu',
        Icon: 'layers',
        Type: 'feather',
        Value: 'DATA',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Sự kiện',
        Icon: 'pushpino',
        Type: 'antdesign',
        Value: 'EVENT',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'H.S máy',
        Icon: 'barschart',
        Type: 'antdesign',
        Value: 'PERMACHINE',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'H.S tổng',
        Icon: 'piechart',
        Type: 'antdesign',
        Value: 'PERMACHINES',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Sản lượng',
        Icon: 'barchart',
        Type: 'antdesign',
        Value: 'QUANTITY',
        Checkbox: false,
        Badge: 0,
      },
    ];
    this.utils = {
      srand: function(seed) {
        this._seed = seed;
      },

      rand: function(min, max) {
        var seed = this._seed;
        min = min === undefined ? 0 : min;
        max = max === undefined ? 1 : max;
        this._seed = (seed * 9301 + 49297) % 233280;
        return parseInt(Math.abs(min + (this._seed / 233280) * (max - min)));
      },
    };
    this.utils.srand(Date.now());
    this.liData = [];
    this.LiData = {};
    this.liDefind = 9;
  }
  componentDidMount() {
    //GetMachines
    API_Follow.GetMachines({})
      .then(rs => {
        this.DataBase.Machines = rs.data.Data;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
    //Machinetypes
    API_Follow.GetMachinetypes({})
      .then(rs => {
        this.DataBase.Machinetypes = rs.data.Data;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
    //Machinetypes
    API_Follow.GetStates({})
      .then(rs => {
        for (var i = 0; i < rs.data.Data.length; i++) {
          if (
            rs.data.Data[i].StateName &&
            rs.data.Data[i].StateName.trim() != ''
          ) {
            this.DataBase.LiState.push(rs.data.Data[i]);
          }
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
    this.LoadData(this.state.ActiveTab);
  }

  //load lại trang sau khi xử lí từ trang khác
  componentWillReceiveProps(nextProps) {}
  _Render_FOLLOW = () => {
    return this.state.data_FOLLOW.map((item, index) => {
      return (
        <View style={{flexDirection: 'column', width: '50%'}}>
          <View
            style={{
              borderWidth: 1,
              borderColor: '#55555566',
              margin: 2,
              height: 150,
              backgroundColor:
                item.StateColor === 'Black' ? '#000000' : item.StateColor,
            }}>
            <View
              style={{
                width: '100%',
                alignItems: 'center',
                backgroundColor: '#55555566',
              }}>
              <Text style={{c: 30, textAlign: 'justify', color: '#fff'}}>
                {item.MachineName}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <View style={{width: '100%', height: 20, alignItems: 'center'}}>
                  <Text
                    style={{
                      lineHeight: 30,
                      textAlign: 'justify',
                      color: '#fff',
                    }}>
                    {item.ShiftName}
                  </Text>
                </View>
                <View style={{width: '100%', height: 70, alignItems: 'center'}}>
                  <Text
                    style={{
                      lineHeight: 70,
                      fontSize: 40,
                      textAlign: 'justify',
                      color: '#fff',
                    }}>
                    {item.ProductCount}
                  </Text>
                </View>
                <View style={{width: '100%', height: 30, alignItems: 'center'}}>
                  <Text
                    style={{
                      lineHeight: 30,
                      textAlign: 'justify',
                      color: '#fff',
                    }}>
                    {item.StateName}
                  </Text>
                </View>
              </View>
              <View style={{flex: 1, paddingTop: 3}}>
                <View style={{flexDirection: 'row', height: 17, width: '100%'}}>
                  <View style={{flex: 7}}>
                    <Text
                      style={{color: '#fff', fontSize: 8, textAlign: 'left'}}>
                      K.XÁC ĐỊNH
                    </Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 8,
                        textAlign: 'right',
                        paddingRight: 2,
                      }}>
                      {item.Value0}'
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', height: 17, width: '100%'}}>
                  <View style={{flex: 7}}>
                    <Text
                      style={{color: '#fff', fontSize: 8, textAlign: 'left'}}>
                      MÁY CHẠY
                    </Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 8,
                        textAlign: 'right',
                        paddingRight: 2,
                      }}>
                      {item.Value1}'
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', height: 17, width: '100%'}}>
                  <View style={{flex: 7}}>
                    <Text
                      style={{color: '#fff', fontSize: 8, textAlign: 'left'}}>
                      KIỂM TRA
                    </Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 8,
                        textAlign: 'right',
                        paddingRight: 2,
                      }}>
                      {item.Value2}'
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', height: 17, width: '100%'}}>
                  <View style={{flex: 7}}>
                    <Text
                      style={{color: '#fff', fontSize: 8, textAlign: 'left'}}>
                      GÁ ĐẶT
                    </Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 8,
                        textAlign: 'right',
                        paddingRight: 2,
                      }}>
                      {item.Value3}'
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', height: 17, width: '100%'}}>
                  <View style={{flex: 7}}>
                    <Text
                      style={{color: '#fff', fontSize: 8, textAlign: 'left'}}>
                      THÁO LẮP
                    </Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 8,
                        textAlign: 'right',
                        paddingRight: 2,
                      }}>
                      {item.Value4}'
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', height: 17, width: '100%'}}>
                  <View style={{flex: 7}}>
                    <Text
                      style={{color: '#fff', fontSize: 8, textAlign: 'left'}}>
                      CHỜ
                    </Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 8,
                        textAlign: 'right',
                        paddingRight: 2,
                      }}>
                      {item.Value5}'
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', height: 17, width: '100%'}}>
                  <View style={{flex: 7}}>
                    <Text
                      style={{color: '#fff', fontSize: 8, textAlign: 'left'}}>
                      KHÁC
                    </Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 8,
                        textAlign: 'right',
                        paddingRight: 2,
                      }}>
                      {item.Value6}'
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    });
  };
  _Render_DATA = () => {
    return this.state.data_DATA.map((item, index) => {
      return (
        <View
          style={{
            height: 'auto',
            padding: 3,
            borderBottom: 1,
            borderColor: 'black',
            borderBottomWidth: 1,
          }}>
          <View
            style={{
              padding: 3,
              flexDirection: 'row',
              borderBottom: 1,
              borderColor: '#eee',
              borderBottomWidth: 1,
            }}>
            <Text style={{flex: 2, color: 'black'}}>STT {index + 1}</Text>
            <Text style={{flex: 2, color: 'gray'}}>Ngày giờ</Text>
            <Text style={{flex: 4}}>{item.DateTime}</Text>
          </View>
          <View
            style={{
              padding: 3,
              flexDirection: 'row',
              borderBottom: 1,
              borderColor: '#eee',
              borderBottomWidth: 1,
            }}>
            <Text style={{flex: 2, color: 'gray'}}>Loại máy</Text>
            <Text style={{flex: 2, color: 'gray'}}>Tên máy</Text>
            <Text style={{flex: 2, color: 'gray'}}>Ca làm</Text>
          </View>
          <View style={{padding: 3, flexDirection: 'row'}}>
            <Text style={{flex: 2}}>{item.MachineTypeName}</Text>
            <Text style={{flex: 2}}>{item.MachineName}</Text>
            <Text style={{flex: 2}}>{item.ShiftName}</Text>
          </View>
          <View
            style={{
              padding: 3,
              flexDirection: 'row',
              borderBottom: 1,
              borderColor: '#eee',
              borderBottomWidth: 1,
            }}>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}>
              K.XÁC ĐỊNH
            </Text>
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}}>
              {item.TimeCount0}
            </Text>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}>
              Máy chạy
            </Text>
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}}>
              {item.TimeCount1}
            </Text>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}>
              KIỂM TRA
            </Text>
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}}>
              {item.TimeCount2}
            </Text>
          </View>
          <View
            style={{
              padding: 3,
              flexDirection: 'row',
              borderBottom: 1,
              borderColor: '#eee',
              borderBottomWidth: 1,
            }}>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}>
              GÁ ĐẶT
            </Text>
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}}>
              {item.TimeCount3}
            </Text>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}>
              THÁO LẮP
            </Text>
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}}>
              {item.TimeCount4}
            </Text>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}>
              CHỜ
            </Text>
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}}>
              {item.TimeCount5}
            </Text>
          </View>
          <View style={{padding: 3, flexDirection: 'row'}}>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}>
              Khác
            </Text>
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}}>
              {item.TimeCount6}
            </Text>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}>
              SẢN PHẨM
            </Text>
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}}>
              {item.ProductCount}
            </Text>
            <Text
              style={{flex: 3, fontSize: 11, lineHeight: 20, color: 'gray'}}
            />
            <Text style={{flex: 1, fontSize: 11, lineHeight: 20}} />
          </View>
        </View>
      );
    });
  };
  _Render_EVENT = () => {
    return this.state.data_EVENT.map((item, index) => {
      return (
        <View
          style={{
            height: 'auto',
            padding: 3,
            borderBottom: 1,
            borderColor: 'black',
            borderBottomWidth: 1,
          }}>
          <View
            style={{
              padding: 3,
              flexDirection: 'row',
              borderBottom: 1,
              borderColor: '#eee',
              borderBottomWidth: 1,
            }}>
            <Text style={{flex: 2, color: 'black'}}>STT {index + 1}</Text>
            <Text style={{flex: 2, color: 'gray'}}>Ngày giờ</Text>
            <Text style={{flex: 4}}>{item.DateTime}</Text>
          </View>
          <View
            style={{
              padding: 3,
              flexDirection: 'row',
              borderBottom: 1,
              borderColor: '#eee',
              borderBottomWidth: 1,
            }}>
            <Text style={{flex: 3, color: 'gray'}}>Loại máy</Text>
            <Text style={{flex: 4, color: 'gray'}}>Tên máy</Text>
            <Text style={{flex: 4, color: 'gray'}}>Trạng thái</Text>
            <Text style={{flex: 2, color: 'gray'}}>Ca làm</Text>
          </View>
          <View style={{padding: 3, flexDirection: 'row'}}>
            <Text style={{flex: 3}}>{item.MachineTypeName}</Text>
            <Text style={{flex: 4}}>{item.MachineName}</Text>
            <Text style={{flex: 4}}>{item.StateName}</Text>
            <Text style={{flex: 2}}>{item.ShiftName}</Text>
          </View>
        </View>
      );
    });
  };
  _Render_MACHINE_CHARTPIE = () => {
    return this.state.DataPERMACHINEChart_PIE.map((item, index) => {
      return (
        <View style={{height: 300, width: 350, flex: 1, flexDirection: 'row'}}>
          <ECharts
            ref={echart => {
              this.state.EchartElement[this.state.ActiveTab] = echart;
            }}
            option={item}
            backgroundColor="rgba(93, 169, 81, 0.3)"
          />
        </View>
      );
    });
  };
  _Render_MACHINES_CHARTPIE = () => {
    return this.state.DataPERMACHINESChart_PIE.map((item, index) => {
      return (
        <View style={{height: 300, width: 350, flex: 1, flexDirection: 'row'}}>
          <ECharts
            ref={echart => {
              this.state.EchartElement[this.state.ActiveTab] = echart;
            }}
            option={item}
            backgroundColor="rgba(93, 169, 81, 0.3)"
          />
        </View>
      );
    });
  };
  ViewRuntime = item => {
    switch (this.state.ActiveTab) {
      case 'FOLLOW':
        return (
          <ScrollView
            style={{
              width: DRIVER.width,
              height: DRIVER.height,
              flex: 1,
              flexDirection: 'column',
            }}>
            <TouchableOpacity>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  alignSelf: 'flex-start',
                }}>
                {this.state['data_' + this.state.ActiveTab]
                  ? this._Render_FOLLOW()
                  : null}
              </View>
            </TouchableOpacity>
          </ScrollView>
        );
      case 'DATA':
        return (
          <ScrollView
            style={{
              width: DRIVER.width,
              height: DRIVER.height,
              marginHorizontal: 3,
              flex: 1,
              flexDirection: 'column',
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              {this.state['data_' + this.state.ActiveTab]
                ? this._Render_DATA()
                : null}
            </View>
          </ScrollView>
        );
      case 'EVENT':
        return (
          <ScrollView
            style={{
              width: DRIVER.width,
              height: DRIVER.height,
              marginHorizontal: 3,
              flex: 1,
              flexDirection: 'column',
            }}>
            <TouchableOpacity>
              <View style={{flex: 1, flexDirection: 'column'}}>
                {this.state['data_' + this.state.ActiveTab]
                  ? this._Render_EVENT()
                  : null}
              </View>
            </TouchableOpacity>
          </ScrollView>
        );
      case 'PERMACHINE':
        return (
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={stylesData.PERMACHINE}>
              {this.state.DataPERMACHINEChart &&
              this.state.DataPERMACHINEChart.series &&
              this.state.DataPERMACHINEChart.series.length > 0 &&
              this.state.DataPERMACHINEChart.xAxis &&
              this.state.DataPERMACHINEChart.xAxis.data ? (
                <ECharts
                  ref={echart => {
                    this.state.EchartElement[this.state.ActiveTab] = echart;
                  }}
                  option={this.state.DataPERMACHINEChart}
                  backgroundColor="rgba(93, 169, 81, 0.3)"
                />
              ) : null}
            </View>
            <View
              style={{
                height: 300,
                borderBottom: 1,
                borderColor: '#eee',
                borderBottomWidth: 1,
                flex: 1,
              }}>
              <ScrollView
                horizontal={true}
                style={{width: '100%', height: 300, marginHorizontal: 3}}>
                <TouchableOpacity>
                  {this.state.DataPERMACHINEChart_PIE
                    ? this._Render_MACHINE_CHARTPIE()
                    : null}
                </TouchableOpacity>
              </ScrollView>
            </View>
          </View>
        );
      case 'PERMACHINES':
        return (
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={stylesData.PERMACHINE}>
              {this.state.DataPERMACHINESChart &&
              this.state.DataPERMACHINESChart.series &&
              this.state.DataPERMACHINESChart.series.length > 0 &&
              this.state.DataPERMACHINESChart.xAxis &&
              this.state.DataPERMACHINESChart.xAxis.data ? (
                <ECharts
                  ref={echart => {
                    this.state.EchartElement[this.state.ActiveTab] = echart;
                  }}
                  option={this.state.DataPERMACHINESChart}
                  backgroundColor="rgba(93, 169, 81, 0.3)"
                />
              ) : null}
            </View>
            <View
              style={{
                height: 300,
                borderBottom: 1,
                borderColor: '#eee',
                borderBottomWidth: 1,
                flex: 1,
              }}>
              <ScrollView
                horizontal={true}
                style={{width: '100%', height: 300, marginHorizontal: 3}}>
                <TouchableOpacity>
                  {this.state.DataPERMACHINESChart_PIE
                    ? this._Render_MACHINES_CHARTPIE()
                    : null}
                </TouchableOpacity>
              </ScrollView>
            </View>
          </View>
        );
      case 'QUANTITY':
        return (
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View
              style={{
                height: 600,
                borderBottom: 1,
                borderColor: '#eee',
                borderBottomWidth: 1,
              }}>
              {this.state.DataQUANTITYChart &&
              this.state.DataQUANTITYChart.series &&
              this.state.DataQUANTITYChart.series.length > 0 &&
              this.state.DataQUANTITYChart.xAxis &&
              this.state.DataQUANTITYChart.xAxis.data ? (
                <ECharts
                  ref={echart => {
                    this.state.EchartElement[this.state.ActiveTab] = echart;
                  }}
                  height={600}
                  option={this.state.DataQUANTITYChart}
                  backgroundColor="rgba(93, 169, 81, 0.3)"
                />
              ) : null}
            </View>
          </View>
        );
      case 'LOADING':
        return (
          <View
            style={{
              width: DRIVER.width,
              height: DRIVER.height,
              flex: 1,
              flexDirection: 'column',
            }}
          />
        );
      default:
        return <View style={{flex: 1, flexDirection: 'row'}} />;
    }
  };
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={AppStyles.container}>
          <TabBar
            title={this.state ? this.state.ActiveTitle : ''}
            BackModuleByCode={'TM'}
            //Formcalendar={true}
            //FormTask={true}
            // FormSearch={true}
            CallbackFormTask={() => this.setState({EvenKanban: false})}
            CallbackFormcalendar={callback =>
              this.CallbackFormcalendar(callback)
            }
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
          />
          <Divider />
          {this.state && this.state.ActiveTab ? this.ViewRuntime() : null}
          {this.state && this.state.loading === true ? (
            <View
              style={{
                width: DRIVER.width,
                height: DRIVER.height,
                flex: 1,
                flexDirection: 'column',
                backgroundColor: '#fff',
                marginTop: 85,
                position: 'absolute',
              }}>
              <View
                style={{
                  height: DRIVER.height - (DRIVER.height * 25) / 100,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../../images/icon/loader.gif')}
                  style={{width: 50, height: 50}}
                />
              </View>
            </View>
          ) : null}
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.CallbackValueBottom(callback)
              }
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  //#endregion
  //#region Chức năng dùng chung
  CallbackMenuBottom = d => {
    this.CallbackValueBottom(d);
  };
  //#endregion
  //#endregion
  LoadData(value) {
    switch (value) {
      case 'FOLLOW':
        API_Follow.GetDataToSend({})
          .then(rs => {
            if (rs.data.Data && rs.data.Data.length > 0) {
              var _d = rs.data.Data[0];
              var _c = true;
              for (var i = 0; i < this.liData.length; i++) {
                if (this.liData[i].MachineId === _d.MachineId) {
                  _c = false;
                  this.liData[i].MachineName = _d.MachineName;
                  this.liData[i].StateName = _d.StateName;
                  this.liData[i].StateColor = _d.StateColor;
                  this.liData[i].ShiftName = _d.ShiftName;
                  this.liData[i].ProcessTime = _d.ProcessTime;
                  for (var j = 0; j <= this.liDefind; j++) {
                    this.liData[i]['Postion' + j] =
                      _d.StateName &&
                      _d['Name' + j] &&
                      _d.StateName.toUpperCase() ===
                        _d['Name' + j].toUpperCase()
                        ? 1
                        : 0;
                    this.liData[i]['Name' + j] = _d['Name' + j];
                    this.liData[i]['Value' + j] = _d['Value' + j];
                  }
                  this.liData[i].ProductCount = _d.ProductCount;
                  break;
                }
              }
              if (_c) {
                this.liData.push(_d);
              }
              this.setState({
                data_FOLLOW: this.liData,
              });
            }
            this.setState({loading: false});
            setTimeout(() => {
              if (
                this.state.ActiveTab === 'FOLLOW' &&
                this.KeyDate === global.PRO_FOLLOW.KeyDate
              ) {
                this.LoadData(value);
              }
            }, 1000);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data.data);
          });
        break;
      case 'DATA':
        this.setState({loading: true});
        API_Follow.GetDatatimes(this.findSearch)
          .then(rs => {
            var _d = rs.data.Data;
            if (_d && _d.length > 0) {
              for (var i = 0; i < this.DataBase.Machines.length; i++) {
                for (var j = 0; j < _d.length; j++) {
                  if (this.DataBase.Machines[i].MachineId === _d[j].MachineId) {
                    _d[j].MachineName = this.DataBase.Machines[i].MachineName;
                    _d[j].MachineTypeId = this.DataBase.Machines[
                      i
                    ].MachineTypeId;
                  }
                }
              }
              for (var i = 0; i < this.DataBase.Machinetypes.length; i++) {
                for (var j = 0; j < _d.length; j++) {
                  if (
                    this.DataBase.Machinetypes[i].MachineTypeId ===
                    _d[j].MachineTypeId
                  ) {
                    _d[j].MachineTypeName = this.DataBase.Machinetypes[
                      i
                    ].MachineTypeName;
                  }
                }
              }
            }
            this.setState({
              data_DATA: _d,
            });
            this.setState({loading: false});
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data.data);
          });
        break;
      case 'EVENT':
        this.setState({loading: true});
        API_Follow.GetEventStates(this.findSearch)
          .then(rs => {
            var _d = [];
            for (var i = 0; i < 50; i++) {
              _d.push(rs.data.Data[i]);
            }
            if (_d && _d.length > 0) {
              for (var i = 0; i < this.DataBase.Machines.length; i++) {
                for (var j = 0; j < _d.length; j++) {
                  if (this.DataBase.Machines[i].MachineId === _d[j].MachineId) {
                    _d[j].MachineName = this.DataBase.Machines[i].MachineName;
                    _d[j].MachineTypeId = this.DataBase.Machines[
                      i
                    ].MachineTypeId;
                  }
                }
              }
              for (var i = 0; i < this.DataBase.Machinetypes.length; i++) {
                for (var j = 0; j < _d.length; j++) {
                  if (
                    this.DataBase.Machinetypes[i].MachineTypeId ===
                    _d[j].MachineTypeId
                  ) {
                    _d[j].MachineTypeName = this.DataBase.Machinetypes[
                      i
                    ].MachineTypeName;
                  }
                }
              }
              for (var i = 0; i < this.DataBase.LiState.length; i++) {
                for (var j = 0; j < _d.length; j++) {
                  if (this.DataBase.LiState[i].StateId === _d[j].StateId) {
                    _d[j].StateName = this.DataBase.LiState[i].StateName;
                    _d[j].StateColor = this.DataBase.LiState[i].StateColor;
                  }
                }
              }
            }
            this.setState({
              data_EVENT: _d,
            });
            this.setState({loading: false});
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data.data);
          });
        break;
      case 'PERMACHINE':
        this.setState({loading: true});
        API_Follow.GetDatatimes(this.findSearch)
          .then(rs => {
            var _LiData = rs.data.Data;
            var _DataState = [];
            for (var j = 0; j < this.DataBase.LiState.length; j++) {
              _DataState[j] = [];
            }
            for (var i = 0; i < this.DataBase.Machines.length && i < 5; i++) {
              for (var j = 0; j < _DataState.length; j++) {
                _DataState[j].push(0);
              }
              for (var x = 0; x < _LiData.length; x++) {
                if (
                  _LiData[x].MachineId === this.DataBase.Machines[i].MachineId
                ) {
                  for (var j = 0; j < this.DataBase.LiState.length; j++) {
                    _DataState[j][_DataState[j].length - 1] =
                      _DataState[j][_DataState[j].length - 1] +
                      _LiData[x]['TimeCount' + j];
                  }
                }
              }
            }
            //Begin Chart bar
            var _state = this.state.DataPERMACHINEChart;
            _state.color = [];
            _state.legend.data = [];
            _state.series = [];
            _state.xAxis.data = [];
            var _datasets = [];
            for (var j = 0; j < this.DataBase.LiState.length; j++) {
              _datasets.push({
                label: {
                  show: true,
                  position: 'top',
                  color: 'black',
                  fontSize: 8,
                  marginTop: 0,
                },
                data: _DataState[j],
                borderWidth: 1,
                type: 'bar',
              });
            }
            _state.series = _datasets;
            for (var i = 0; i < this.DataBase.LiState.length; i++) {
              _state.legend.data.push(this.DataBase.LiState[i].StateName);
              _state.color.push(this.DataBase.LiState[i].StateColor);
            }
            for (var i = 0; i < this.DataBase.Machines.length && i < 5; i++) {
              _state.xAxis.data.push(this.DataBase.Machines[i].MachineName);
            }
            //End chart bar
            //Begin chart pie
            var _state_PIE = [];
            for (var x = 0; x < this.DataBase.Machines.length && x < 5; x++) {
              _state_PIE.push({
                color: [],
                title: {
                  text: this.DataBase.Machines[x].MachineName,
                  subtext: '',
                  left: 'center',
                },
                tooltip: {
                  trigger: 'item',
                  formatter: '{b}: {c} ({d}%)',
                },
                legend: {
                  orient: 'horizontal',
                  left: 'center',
                  bottom: 50,
                  data: [],
                },
                series: [
                  {
                    type: 'pie',
                    selectedMode: 'single',
                    radius: '50%',
                    center: ['50%', '50%'],
                    data: [],
                    emphasis: {
                      itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)',
                      },
                    },
                  },
                ],
              });
              for (var i = 0; i < this.DataBase.LiState.length; i++) {
                //_state_PIE.legend.data.push(this.DataBase.LiState[i].StateName);
                _state_PIE[_state_PIE.length - 1].color.push(
                  this.DataBase.LiState[i].StateColor,
                );
              }
              for (var i = 0; i < _DataState.length; i++) {
                _state_PIE[_state_PIE.length - 1].series[0].data.push({
                  value: _DataState[i][x],
                  name: this.DataBase.LiState[i].StateName,
                });
              }
            }

            //End chart pie
            this.setState({
              DataPERMACHINEChart: _state,
            });
            this.setState({
              DataPERMACHINEChart_PIE: _state_PIE,
            });
            this.setState({loading: false});
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data.data);
          });
        break;
      case 'PERMACHINES':
        this.setState({loading: true});
        API_Follow.GetDatatimes(this.findSearch)
          .then(rs => {
            var ctrl = {
              DataBase: this.DataBase,
              findSearch: this.findSearch,
              LiData: {
                PERMACHINES: rs.data.Data,
              },
            };
            ctrl.ViewTab = value;
            ctrl.LiData[ctrl.ViewTab] = rs.data.Data;
            var _DataState = [];
            var _DataStateColor = [];
            var _Chart_label = [];
            var _label_state = [];
            var _startData = [];
            for (var j = 0; j < ctrl.DataBase.LiState.length; j++) {
              _DataState[j] = [];
              _DataStateColor.push(ctrl.DataBase.LiState[j].StateColor);
              _label_state.push(ctrl.DataBase.LiState[j].StateName);
            }

            var _start_day = new Date(
              ctrl.findSearch.StartDate.getFullYear(),
              ctrl.findSearch.StartDate.getMonth(),
              ctrl.findSearch.StartDate.getDate(),
            );
            var _end_day = new Date(
              ctrl.findSearch.EndDate.getFullYear(),
              ctrl.findSearch.EndDate.getMonth(),
              ctrl.findSearch.EndDate.getDate(),
            );

            while (
              new Date(_start_day.getFullYear(), _start_day.getMonth(), 1) <=
              new Date(_end_day.getFullYear(), _end_day.getMonth(), 1)
            ) {
              _startData.push({
                Name:
                  'Tháng ' +
                  (_start_day.getMonth() + 1) +
                  '/' +
                  _start_day.getFullYear(),
                StartDate: new Date(_start_day),
                EndDate: new Date(
                  _start_day.getFullYear(),
                  _start_day.getMonth() + 1,
                  0,
                ),
              });

              _start_day = new Date(
                _start_day.setMonth(_start_day.getMonth() + 1),
              );
            }
            for (var i = 0; i < _startData.length; i++) {
              _Chart_label.push(_startData[i].Name);
              for (var j = 0; j < _DataState.length; j++) {
                _DataState[j].push(0);
              }
              for (var x = 0; x < ctrl.LiData[ctrl.ViewTab].length; x++) {
                if (
                  _startData[i].StartDate <=
                    this.ConvertDate_DMY(
                      ctrl.LiData[ctrl.ViewTab][x].DateTime,
                    ) &&
                  _startData[i].EndDate >=
                    this.ConvertDate_DMY(ctrl.LiData[ctrl.ViewTab][x].DateTime)
                ) {
                  for (var j = 0; j < ctrl.DataBase.LiState.length; j++) {
                    _DataState[j][_DataState[j].length - 1] =
                      _DataState[j][_DataState[j].length - 1] +
                      ctrl.LiData[ctrl.ViewTab][x]['TimeCount' + j];
                  }
                }
              }
            }
            //Begin Chart bar
            var _state = this.state.DataPERMACHINESChart;
            _state.color = [];
            _state.legend.data = [];
            _state.series = [];
            _state.xAxis.data = [];
            var _datasets = [];
            for (var j = 0; j < this.DataBase.LiState.length; j++) {
              _datasets.push({
                label: {
                  show: true,
                  position: 'top',
                  color: 'black',
                  fontSize: 8,
                  marginTop: 0,
                },
                data: _DataState[j],
                borderWidth: 1,
                type: 'bar',
              });
            }
            _state.series = _datasets;
            for (var i = 0; i < this.DataBase.LiState.length; i++) {
              _state.legend.data.push(this.DataBase.LiState[i].StateName);
              _state.color.push(this.DataBase.LiState[i].StateColor);
            }
            _state.xAxis.data = _Chart_label;
            //End chart bar
            //Begin chart pie
            var _state_PIE = [];
            for (var x = 0; x < _Chart_label.length && x < 5; x++) {
              _state_PIE.push({
                color: [],
                title: {
                  text: _Chart_label[x],
                  subtext: '',
                  left: 'center',
                },
                tooltip: {
                  trigger: 'item',
                  formatter: '{b}: {c} ({d}%)',
                },
                legend: {
                  orient: 'horizontal',
                  left: 'center',
                  bottom: 50,
                  data: [],
                },
                series: [
                  {
                    type: 'pie',
                    selectedMode: 'single',
                    radius: '50%',
                    center: ['50%', '50%'],
                    data: [],
                    emphasis: {
                      itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)',
                      },
                    },
                  },
                ],
              });
              for (var i = 0; i < this.DataBase.LiState.length; i++) {
                //_state_PIE.legend.data.push(this.DataBase.LiState[i].StateName);
                _state_PIE[_state_PIE.length - 1].color.push(
                  this.DataBase.LiState[i].StateColor,
                );
              }
              for (var i = 0; i < _DataState.length; i++) {
                _state_PIE[_state_PIE.length - 1].series[0].data.push({
                  value: _DataState[i][x],
                  name: this.DataBase.LiState[i].StateName,
                });
              }
            }

            //End chart pie
            this.setState({
              DataPERMACHINESChart: _state,
            });
            this.setState({
              DataPERMACHINESChart_PIE: _state_PIE,
            });
            this.setState({loading: false});
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data.data);
          });
        break;
      case 'QUANTITY':
        this.setState({loading: true});
        API_Follow.GetDatatimes(this.findSearch)
          .then(rs => {
            var _liColor = [
              '#ffebee',
              '#ffcdd2',
              '#ef9a9a',
              '#e57373',
              '#ef5350',
              '#f44336',
              '#e53935',
              '#d32f2f',
              '#c62828',
              '#b71c1c',
              '#ff8a80',
              '#ff5252',
              '#ff1744',
              '#d50000',
              '#ede7f6',
              '#d1c4e9',
              '#b39ddb',
              '#9575cd',
              '#7e57c2',
              '#673ab7',
              '#5e35b1',
              '#512da8',
              '#4527a0',
              '#311b92',
              '#b388ff',
              '#7c4dff',
              '#651fff',
              '#6200ea',
              '#e1f5fe',
              '#b3e5fc',
              '#81d4fa',
              '#4fc3f7',
              '#29b6f6',
              '#03a9f4',
              '#039be5',
              '#0288d1',
              '#0277bd',
              '#01579b',
              '#80d8ff',
              '#40c4ff',
              '#00b0ff',
              '#0091ea',
              '#e8f5e9',
              '#c8e6c9',
              '#a5d6a7',
              '#81c784',
              '#66bb6a',
              '#4caf50',
              '#43a047',
              '#388e3c',
              '#2e7d32',
              '#1b5e20',
              '#b9f6ca',
              '#69f0ae',
              '#00e676',
              '#00c853',
              '#fffde7',
              '#fff9c4',
              '#fff59d',
              '#fff176',
              '#ffee58',
              '#ffeb3b',
              '#fdd835',
              '#fbc02d',
              '#f9a825',
              '#f57f17',
              '#ffff8d',
              '#ffff00',
              '#ffea00',
              '#ffd600',
              '#fbe9e7',
              '#ffccbc',
              '#ffab91',
              '#ff8a65',
              '#ff7043',
              '#ff5722',
              '#f4511e',
              '#e64a19',
              '#d84315',
              '#bf360c',
              '#ff9e80',
              '#ff6e40',
              '#ff3d00',
              '#dd2c00',
              '#eceff1',
              '#cfd8dc',
              '#b0bec5',
              '#90a4ae',
              '#78909c',
              '#607d8b',
              '#546e7a',
              '#455a64',
              '#37474f',
              '#263238',
            ];

            var ctrl = {
              DataBase: this.DataBase,
              findSearch: this.findSearch,
              LiData: {
                PERMACHINES: rs.data.Data,
              },
              DataMachine: this.DataBase.Machines,
            };
            ctrl.ViewTab = value;
            ctrl.LiData[ctrl.ViewTab] = rs.data.Data;
            var _DataState = [];
            var _DataState_machine = [];
            var _DataStateColor = [];
            var _Chart_label = [];
            var _label_state = [];
            var _startData = [];
            for (var j = 0; j < ctrl.DataMachine.length; j++) {
              if (ctrl.DataMachine[j].Checked) {
                _DataState[_DataState.length] = [];
                _DataStateColor.push(_liColor[j]);
                _label_state.push(ctrl.DataMachine[j].MachineName);
                _DataState_machine.push(ctrl.DataMachine[j].MachineId);
              }
            }
            var _start_day = new Date(
              ctrl.findSearch.StartDate.getFullYear(),
              ctrl.findSearch.StartDate.getMonth(),
              ctrl.findSearch.StartDate.getDate(),
            );
            var _end_day = new Date(
              ctrl.findSearch.EndDate.getFullYear(),
              ctrl.findSearch.EndDate.getMonth(),
              ctrl.findSearch.EndDate.getDate(),
              23,
              59,
              59,
            );
            while (
              new Date(
                _start_day.getFullYear(),
                _start_day.getMonth(),
                _start_day.getDate(),
              ) <=
              new Date(
                _end_day.getFullYear(),
                _end_day.getMonth(),
                _end_day.getDate(),
              )
            ) {
              _startData.push({
                Name:
                  _start_day.getDate() +
                  '/' +
                  (_start_day.getMonth() + 1) +
                  '/' +
                  _start_day.getFullYear(),
                StartDate: new Date(_start_day),
                EndDate: new Date(
                  _start_day.getFullYear(),
                  _start_day.getMonth(),
                  _start_day.getDate(),
                  23,
                  59,
                  59,
                ),
              });
              _start_day = new Date(
                _start_day.setDate(_start_day.getDate() + 1),
              );
            }
            for (var i = 0; i < _startData.length; i++) {
              _Chart_label.push(_startData[i].Name);
              for (var j = 0; j < _DataState.length; j++) {
                _DataState[j].push(0);
              }
              for (var x = 0; x < ctrl.LiData[ctrl.ViewTab].length; x++) {
                if (
                  _startData[i].StartDate <=
                    this.ConvertDate_DMY(
                      ctrl.LiData[ctrl.ViewTab][x].DateTime,
                    ) &&
                  _startData[i].EndDate >=
                    this.ConvertDate_DMY(ctrl.LiData[ctrl.ViewTab][x].DateTime)
                ) {
                  for (var j = 0; j < _DataState_machine.length; j++) {
                    if (
                      _DataState_machine[j] ===
                      ctrl.LiData[ctrl.ViewTab][x].MachineId
                    ) {
                      _DataState[j][_DataState[j].length - 1] +=
                        _DataState[j][_DataState[j].length - 1] +
                        ctrl.LiData[ctrl.ViewTab][x].ProductCount;
                      break;
                    }
                  }
                }
              }
            }
            console.log(_DataState, _DataStateColor);
            console.log(_Chart_label, _label_state);
            //Begin Chart bar
            var _state = this.state.DataQUANTITYChart;
            _state.color = [];

            _state.series = [];
            _state.xAxis.data = [];
            var _datasets = [];
            for (var i = 0; i < ctrl.DataMachine.length && i < 7; i++) {
              _state.legend.data.push(ctrl.DataMachine[i].MachineName);
              _state.color.push(_liColor[i]);
            }
            _state.xAxis.data = _Chart_label;
            for (var j = 0; j < _Chart_label.length; j++) {
              var _d = [];
              // for (var j = 0; j < _DataState_machine.length; j++) {
              //   _d.push(_DataState_machine[j][i]);
              // }
              for (var i = 0; i < ctrl.DataMachine.length && i < 7; i++) {
                _datasets.push({
                  stack: _Chart_label[j],
                  name: ctrl.DataMachine[i].MachineName,
                  type: 'bar',
                  data: [this.randomScalingFactor()],
                });
              }
            }
            _state.series = _datasets;
            //End chart bar
            console.log(_state);
            this.setState({
              DataQUANTITYChart: _state,
            });
            this.setState({loading: false});
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data.data);
          });
        break;
      default:
        break;
    }
  }
  CallbackValueBottom = value => {
    switch (value) {
      case 'FOLLOW':
        this.setState({
          ActiveTab: value,
          ActiveTitle: 'Màn hình máy hoạt động',
        });
        break;
      case 'DATA':
        this.setState({
          ActiveTab: 'LOADING',
          ActiveTitle: 'Danh sách dữ liệu',
        });
        this.setState({
          ActiveTab: value,
        });
        break;
      case 'EVENT':
        this.setState({
          ActiveTab: 'LOADING',
          ActiveTitle: 'Danh sách sự kiện',
        });
        this.setState({
          ActiveTab: value,
        });
        break;
      case 'PERMACHINE':
        // for (var _item in this.state.EchartElement) {
        //   if (this.state.EchartElement[_item] && typeof (this.state.EchartElement[_item].clear) === 'function' && _item !== 'PERMACHINE') {
        //     this.state.EchartElement[_item].clear();
        //   }
        // }
        this.setState({
          ActiveTab: 'LOADING',
          ActiveTitle: 'Hiệu suất theo máy',
        });
        setTimeout(() => {
          this.setState({
            ActiveTab: value,
          });
        }, 1000);

        break;
      case 'PERMACHINES':
        // for (var _item in this.state.EchartElement) {
        //   if (this.state.EchartElement[_item] && typeof (this.state.EchartElement[_item].clear) === 'function' && _item !== 'PERMACHINES') {
        //     this.state.EchartElement[_item].clear();
        //   }
        // }
        this.setState({
          ActiveTab: 'LOADING',
          ActiveTitle: 'Hiệu suất tổng thể',
        });
        setTimeout(() => {
          this.setState({
            ActiveTab: value,
          });
        }, 1000);
        break;
      case 'QUANTITY':
        // for (var _item in this.state.EchartElement) {
        //   if (this.state.EchartElement[_item] && typeof (this.state.EchartElement[_item].clear) === 'function' && _item !== 'QUANTITY') {
        //     this.state.EchartElement[_item].clear();
        //   }
        // }
        this.setState({
          ActiveTab: 'LOADING',
          ActiveTitle: 'Sản lượng',
        });
        setTimeout(() => {
          this.setState({
            ActiveTab: value,
          });
        }, 1000);
        break;
      default:
        break;
    }
    this.LoadData(value);
  };
  ConvertDate_DMY(s) {
    var _d = s.split(' ');
    var _dmy = _d[0].split('/');
    var _hms = _d[1].split(':');
    return new Date(
      parseInt(_dmy[2]),
      parseInt(_dmy[1]) - 1,
      parseInt(_dmy[0]),
      parseInt(_hms[2]),
      parseInt(_hms[1]),
      parseInt(_hms[0]),
    );
  }
  randomScalingFactor() {
    return Math.round(this.utils.rand(-100, 100));
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(ProductionFollowComponent);
