import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Icon, ListItem, SearchBar, Badge } from "react-native-elements";
import { ActivityIndicator, FlatList, RefreshControl, TouchableOpacity, Text, View, TextInput } from "react-native";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import API_ProductionResult from "../../../network/PRO/API_ProductionResult";
import { DatePicker, Container, Content, Item, Label, Picker } from 'native-base';
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar from '../../component/TabBar';
import FormSearch from '../../component/FormSearch';
import Searchable_Dropdown from '../../component/Searchable_Dropdown';
import Combobox from '../../component/Combobox';

class ListProductionResultsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {},
            list: [],
            isFetching: false,
            staticParam: {
                CurrentPage: 1,
                Length: 25,
                Search: "",
                OrderNumber: null,
            },
            ListOrderNumber: [],
            selected: undefined,
            openSearch: false,
            dropdownName: ''
        };
    }

    GetAll = () => {
        this.setState({ refreshing: true });
        API_ProductionResult.ListAll(this.state.staticParam)
            .then(res => {
                this.state.list = res.data.data != "null" ? this.state.list.concat(JSON.parse(res.data.data)) : [];
                this.setState({ list: this.state.list });
                this.setState({ refreshing: false });
            })
            .catch(error => {
                console.log(error);
                this.setState({ refreshing: false });
            });
    }

    componentDidMount(): void {
        this.GetAll();
        API_ProductionResult.GetOrderNumber().then(rs => {
            var _listOrderNumber = rs.data;
            let _reListOrderNumber = [{
                text: 'Bỏ chọn',
                value: null,
            }];
            for (var i = 0; i < _listOrderNumber.length; i++) {
                _reListOrderNumber.push({
                    text: _listOrderNumber[i].OrderNumberId,
                    value: _listOrderNumber[i].OrderNumberId,
                });
            }
            this.setState(
                {
                    ListOrderNumber: _reListOrderNumber,
                });
        }).catch(error => {
            console.log(error)
        });
    }

    ListEmpty = () => {
        if (this.state.list.length > 0) return null;
        return (
            <View style={AppStyles.centerAligned}>
                <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
            </View>
        );
    };
    updateSearch = text => {
        this.state.staticParam.Search = text;
        this.setState({
            list: [],
            staticParam: {
                CurrentPage: 1,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                OrderNumber: this.state.staticParam.OrderNumber
            }
        });
        this.GetAll();
    }
    renderFooter = () => {
        if (!this.state.isFetching) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar
                    title={'Kết quản sản xuất'}
                    FormSearch={true}
                    CallbackFormSearch={(callback) => this.setState({ openSearch: callback })}
                    BackModuleByCode={'PRO'}
                />
                {this.state.ListOrderNumber.length > 0 ?
                    <TouchableOpacity style={[AppStyles.FormInput, { marginLeft: 10, marginRight: 10, flexDirection: 'row' }]} onPress={() => this.onActionCombobox()}>
                        <Text style={[AppStyles.TextInput, this.state.dropdownName == "" ? { color: AppColors.gray, flex: 3 } : { flex: 3 }]}>{this.state.dropdownName !== "" ? this.state.dropdownName : "Chọn đơn hàng..."}</Text>
                        <View style={{ flex: 1, alignItems: 'flex-end' }}>
                            <Icon color='#888888'
                                name={'chevron-thin-down'} type='entypo'
                                size={20} />
                        </View>
                    </TouchableOpacity>
                    : null
                }
                {this.state.openSearch == true ?
                    <FormSearch
                        CallbackSearch={(callback) => this.updateSearch(callback)}
                    />
                    : null}
                <View style={{ flex: 9 }}>
                    <FlatList
                        style={{ height: '100%' }}
                        refreshing={this.state.isFetching}
                        ref={(ref) => {
                            this.ListView_Ref = ref;
                        }}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                        keyExtractor={item => item.RowGuid}
                        data={this.state.list}
                        renderItem={({ item, index }) => (
                            <View style={{ fontSize: 13, fontFamily: Fonts.base.family }}>
                                <ListItem
                                    title={item.ItemName}
                                    titleStyle={AppStyles.Titledefault}
                                    subtitle={() => {
                                        return (
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ width: '60%' }}>
                                                    <Text style={AppStyles.Textdefault}>Đơn hàng: {item.OrderNumberId}</Text>
                                                    <Text
                                                        style={AppStyles.Textdefault}>Mã sản phẩm:{item.ItemId}</Text>
                                                </View>
                                                <View style={{ width: '40%' }}>
                                                    <Text style={AppStyles.Textdefault}>SL yêu cầu: {this.addPeriod(item.QuantityRequire)}</Text>
                                                    <Text style={AppStyles.Textdefault}>Đã sản xuất: {this.addPeriod(item.Quantity)}</Text>
                                                </View>
                                            </View>
                                        )
                                    }}
                                    bottomDivider
                                    onPress={() => this.openView(item)}
                                />
                            </View>
                        )}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            if (this.state.staticParam.CurrentPage !== 1) {
                                this.nextPage();
                            }
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                tintColor="#f5821f"
                                titleColor="#fff"
                                colors={["red", "green", "blue"]}
                            />
                        }
                    />
                </View>
                {this.state.ListOrderNumber.length > 0 ?
                    <Combobox
                        TypeSelect={"single"}// single or multiple
                        callback={this.ChangOrderNumber}
                        data={this.state.ListOrderNumber}
                        nameMenu={'Chọn đơn hàng'}
                        eOpen={this.openCombobox}
                        position={'top'}
                    />
                    : null
                }
            </View>
        );
    }
    nextPage() {
        this.state.staticParam.CurrentPage++;
        this.setState({
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                OrderNumber: this.state.staticParam.OrderNumber
            }
        });
        this.GetAll();
    }
    //Load lại
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.GetAll();
    };
    _openCombobox() { }
    openCombobox = (d) => {
        this._openCombobox = d;
    }
    onActionCombobox() {
        this._openCombobox();
    }
    ChangOrderNumber = (value) => {
        this.state.list = [];
        this.state.staticParam.CurrentPage = 1;
        this.state.staticParam.Length = 25;
        this.state.staticParam.Search = '';
        this.state.staticParam.OrderNumber = value.value;
        this.setState({
            selected: value.value,
            dropdownName: value.text,
            staticParam: {
                CurrentPage: this.state.staticParam.CurrentPage,
                Length: this.state.staticParam.Length,
                Search: this.state.staticParam.Search,
                OrderNumber: this.state.staticParam.OrderNumber
            }
        });
        this.GetAll();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
    openView(obj) {
        Actions.proitemProductionResult(obj);
    }
}
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListProductionResultsComponent);
