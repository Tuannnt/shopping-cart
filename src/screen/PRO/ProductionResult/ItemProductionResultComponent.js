import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, SafeAreaView, View, FlatList, Text, Image, RefreshControl, ScrollView, Alert, TouchableOpacity, Dimensions } from 'react-native';
import { Header, Icon, Divider, ListItem } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import Fonts from "../../../theme/fonts";
import API_Contracts from "../../../network/AM/API_Contracts";
import ErrorHandler from "../../../error/handler";
import { AppStyles, getStatusBarHeight, AppSizes, AppColors } from '@theme';
import TabBar_Title from "../../component/TabBar_Title";
import * as Progress from 'react-native-progress';

var width = Dimensions.get('window').width; //full width
const styles = StyleSheet.create({
    hidden: {
        display: 'none'
    }
});

class ItemProductionResultComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Data: [],
            isHidden: false
        };
        this.rate = (this.props.Quantity / this.props.QuantityRequire)
    }

    componentDidMount(): void {

    }
    render() {
        return (
            <View style={AppStyles.container}>
                <TabBar_Title
                    title={'Chi tiết'}
                    callBack={() => this.onPressBack()} />
                <View>
                    <View style={[this.state.isHidden == true ? styles.hidden : '', { padding: 10, backgroundColor: '#fff' }]}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Mã sản phẩm :</Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.props.ItemId}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Tên sản phẩm :</Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.props.ItemName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Số đơn hàng :</Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.props.OrderNumberId}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Số lượng yêu cầu :</Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.addPeriod(this.props.QuantityRequire)}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Đã sản xuất :</Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.addPeriod(this.props.Quantity)}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Đã giao :</Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.addPeriod(this.props.QuantityOK)}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Số lượng còn :</Text>
                            <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{this.addPeriod(this.props.RemainQuantity)}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <View style={{ flex: 1 }}>
                                <Text style={[AppStyles.Labeldefault]}>Tiến độ sản xuất :</Text>
                            </View>
                            <View style={{ justifyContent: 'center', flex: 2 }}>
                                <Progress.Bar
                                    progress={this.rate}
                                    width={200}
                                    height={3}
                                    color={this.rate < 0.5 ? '#eb3b48' : this.rate < 0.8 ? '#f48221' : '#269af1'}
                                />
                            </View>

                        </View>
                    </View>
                </View>
            </View>
        );
    }

    onPressBack() {
        Actions.ProductionResult();
    }
    //định dạng ngày
    customDate(strDate) {
        if (strDate != null) {
            var strSplitDate = String(strDate).split(' ');
            var date = new Date(strSplitDate[0]);
            // alert(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + "/" + mm + "/" + yyyy;
            return date.toString();
        } else {
            return "";
        }
    }

    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        } else {
            return 0;
        }
    };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(ItemProductionResultComponent);
