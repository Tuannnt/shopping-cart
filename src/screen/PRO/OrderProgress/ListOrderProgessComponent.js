import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  TouchableOpacity,
  Dimensions,
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {ListItem, SearchBar, Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_OrderProgress from '../../../network/PRO_V2/API_OrderProgress';
import {AppStyles, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import {FuncCommon} from '../../../utils';
import Combobox from '../../component/Combobox';

class ListOrderProgessComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: {},
      List: [],
      isFetching: false,
    };
    this.staticParam = {
      CurrentPage: 1,
      Length: 15,
      Title: '',
      Year: new Date().getFullYear(),
      Month: new Date().getMonth() + 1,
      Week: this.getNumberOfWeek(),
      TypePlan: 0,
      DepartmentId: global.__appSIGNALR.SIGNALR_object.USER.DepartmentId,
    };
    this.Total = 0;
    this.onEndReachedCalledDuringMomentum = true;
    this.DropdownMonth = FuncCommon.getListTime().DropdownMonth;
    this.DropdownYear = FuncCommon.getListTime().DropdownYear;
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.updateSearch('');
    }
  }
  componentDidMount = () => {
    this.updateSearch('');
  };
  // combobox tháng
  _openComboboxMonth() {}

  openComboboxMonth = d => {
    this._openComboboxMonth = d;
  };

  onActionComboboxMonth() {
    this._openComboboxMonth();
  }

  // combobox năm
  _openComboboxYear() {}

  openComboboxYear = d => {
    this._openComboboxYear = d;
  };

  onActionComboboxYear() {
    this._openComboboxYear();
  }

  //Scroll to top
  upButtonHandler = () => {
    //OnCLick of Up button we scrolled the list to top
    this.ListView_Ref.scrollToOffset({offset: 0, animated: true});
  };
  //Scroll to end
  downButtonHandler = () => {
    //OnCLick of down button we scrolled the list to bottom
    this.ListView_Ref.scrollToEnd({animated: true});
  };
  //Scroll to Index
  goIndex = () => {
    this.ListView_Ref.scrollToIndex({animated: true, index: 5});
  };
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  renderFooter = () => {
    if (!this.state.isFetching) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  //Message
  ListEmpty = () => {
    if (this.state.List.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Tiến độ sản xuất'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'PRO'}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 10,
                paddingTop: 5,
              }}>
              <View style={{flexDirection: 'column', flex: 3}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Tháng</Text>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput, {marginRight: 5}]}
                  onPress={() => this.onActionComboboxMonth()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.staticParam.Month == '' ||
                      this.staticParam.Month === null
                        ? {color: AppColors.gray}
                        : {color: 'black'},
                    ]}>
                    {this.staticParam.Month !== '' &&
                    this.staticParam.Month !== null
                      ? this.staticParam.Month
                      : 'Chọn tháng...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      color="#888888"
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', flex: 3}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Năm</Text>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionComboboxYear()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.staticParam.Year == '' ||
                      this.staticParam.Year == null
                        ? {color: AppColors.gray}
                        : {color: 'black'},
                    ]}>
                    {this.staticParam.Year !== '' &&
                    this.staticParam.Year !== null
                      ? this.staticParam.Year
                      : 'Chọn năm...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      color="#888888"
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.updateSearch(text)}
              value={this.staticParam.Title}
            />
          </View>
        ) : null}
        <View style={{flex: 10}}>
          <FlatList
            style={{backgroundColor: '#fff', height: '100%'}}
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.List}
            renderItem={({item, index}) => (
              <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
                <ListItem
                  title={
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={{fontWeight: 'bold'}}>
                          {item.OrderNumber}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text>{this.customDate(item.DeliveryDate)}</Text>
                      </View>
                    </View>
                  }
                  subtitle={() => {
                    return (
                      <View>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{flex: 3}}>
                            <Text style={AppStyles.Textdefault}>
                              Mã: {item.ItemID}
                            </Text>
                            <Text
                              style={[AppStyles.Textdefault, {marginTop: 2}]}>
                              Tên:{' '}
                              {FuncCommon.handleLongText(item.ItemName, 50)}
                            </Text>
                            {/* <Text
                              style={[AppStyles.Textdefault, {marginTop: 2}]}>
                              Lũy kế Slg: {item.CumulativeQuantityOK}
                            </Text> */}
                          </View>
                          {/* <View
                            style={{
                              alignItems: 'flex-start',
                              justifyContent: 'flex-start',
                              flex: 1.5,
                            }}>
                            <Text style={[AppStyles.Textdefault]} />
                            <Text style={[AppStyles.Textdefault]}>
                              SL PO: {item.QuantityPO}
                            </Text>
                            <Text style={[AppStyles.Textdefault]}>
                              HT: {item.PercentComplete}%
                            </Text>
                          </View> */}
                        </View>
                      </View>
                    );
                  }}
                  bottomDivider
                  titleStyle={AppStyles.Titledefault}
                  //chevron
                  onPress={() => this.onView(item)}
                />
              </View>
            )}
            onEndReachedThreshold={0.5}
            onEndReached={() => this.loadMoreData()}
            onMomentumScrollBegin={() => {
              this.onEndReachedCalledDuringMomentum = false;
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
        </View>
        {this.DropdownMonth.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.onValueChangeTypeMonth}
            data={this.DropdownMonth}
            nameMenu={'Chọn tháng'}
            eOpen={this.openComboboxMonth}
            position={'bottom'}
          />
        ) : null}
        {this.DropdownYear.length > 0 ? (
          <Combobox
            TypeSelect={'single'} // single or multiple
            callback={this.onValueChangeTypeYear}
            data={this.DropdownYear}
            nameMenu={'Chọn năm'}
            eOpen={this.openComboboxYear}
            position={'bottom'}
          />
        ) : null}
      </View>
    );
  }
  onValueChangeTypeMonth = data => {
    if (!data.value) {
      return;
    }
    this.staticParam.Month = data.value;
    this.updateSearch('');
  };
  onValueChangeTypeYear = data => {
    if (!data.value) {
      return;
    }
    this.staticParam.Year = data.value;
    this.updateSearch('');
  };
  loadMoreData() {
    if (this.Total > this.state.List.length) {
      this.nextPage();
    }
  }
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState(
        {
          ValueSearchDate: callback.value,
        },
        () => {
          this.updateSearch('');
        },
      );
    }
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  listAll = () => {
    this.setState({refreshing: true}, () => {
      API_OrderProgress.JTable(this.staticParam)
        .then(res => {
          let _data = JSON.parse(res.data.data);
          if (!_data) {
            this.setState({refreshing: false});
            return;
          }
          FuncCommon.Data_Offline_Set('DataOrderProgress', _data);
          this.setState({List: _data || [], refreshing: false});
        })
        .catch(error => {
          console.log('errrrrrrrrrrrrrrrrrr');
          this.setState({refreshing: false});
        });
    });
  };
  //Lấy dữ liệu
  GetListAll = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.listAll();
      } else {
        let data = await FuncCommon.Data_Offline_Get('DataOrderProgress');
        this.setState({
          List: data || [],
          refreshing: false,
        });
      }
    });
  };
  //Xem
  onView(item) {
    Actions.ItemOrderProgress({item, staticParam: this.staticParam});
  }
  //Sửa
  onEdit(item) {
    Actions.listOrderDetails(item);
  }
  //Xóa
  onDelete(item) {
    API_TicketRequests.delete(item.OrderGuid)
      .then(res => {
        Actions.pop();
      })
      .catch(error => {});
  }
  //Load lại
  onRefresh = () => {
    this.updateSearch('');
  };
  //Tìm kiếm
  updateSearch = text => {
    this.staticParam.Title = text;
    this.staticParam.CurrentPage = 1;
    this.setState(
      {
        List: [],
      },
      () => {
        this.GetListAll();
      },
    );
  };
  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetListAll();
  }
  getNumberOfWeek() {
    var date = new Date();
    const firstDayOfYear = new Date(date.getFullYear(), 0, 1);
    const pastDaysOfYear = (date - firstDayOfYear) / 86400000;
    return Math.ceil((pastDaysOfYear + firstDayOfYear.getDay() + 1) / 7);
  }
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}
const styles = StyleSheet.create({
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabCenter: {
    flex: 1,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    paddingTop: 10,
    paddingBottom: 10,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListOrderProgessComponent);
