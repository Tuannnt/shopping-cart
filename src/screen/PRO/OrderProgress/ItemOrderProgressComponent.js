import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  Keyboard,
  View,
  Dimensions,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import {Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import {AppStyles} from '@theme';
import API_OrderProgress from '../../../network/PRO_V2/API_OrderProgress';
import TabBar_Title from '../../component/TabBar_Title';
import LoadingComponent from '../../component/LoadingComponent';
import ProgressCircle from 'react-native-progress-circle';
import {FuncCommon} from '../../../utils';
const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});
const Width = Dimensions.get('window').width; //full width
class ItemTicketRequestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      ListDetails: [],
      loading: false,
      listOrders: [],
      isActive: 0,
    };
    this.staticParam = {
      TicketRequestGuid: null,
    };
    this.DataView = {};
    this.flatListRef = React.createRef();
  }

  componentDidMount(): void {
    this.GetListAll();
  }

  componentWillUnmount = async () => {
    await FuncCommon.Data_Offline_Set('DataOrderProgress', []);
  };
  //Lấy dữ liệu
  GetListAll = () => {
    this.setState({loading: true}, async () => {
      const cacheData = await FuncCommon.Data_Offline_Get('DataOrderProgress');
      if (cacheData && cacheData.length > 0) {
        this.getItem(cacheData);
        this.setState({loading: false});
        return;
      }
      API_OrderProgress.JTable(this.props.staticParam)
        .then(res => {
          let _data = JSON.parse(res.data.data);
          if (!_data) {
            this.setState({loading: false});
            return;
          }
          this.getItem(_data);
          this.setState({loading: false});
        })
        .catch(error => {
          console.log('errrrrrrrrrrrrrrrrrr');
          this.setState({loading: false});
        });
    });
  };
  getItem = data => {
    const {OrderNumber} = this.props.item;
    let listOrders = data.filter(item => item.OrderNumber === OrderNumber);
    this.setState({listOrders}, () => {
      this.DataView = listOrders[0];
      this.getDetailByOrderPlanId(listOrders[0].ItemID);
    });
  };
  getDetailByOrderPlanId = id => {
    const {OrderNumber} = this.props.item;
    const {listOrders, isActive} = this.state;
    const obj = {
      ItemID: id,
      OrderNumber,
    };
    API_OrderProgress.Detail(obj)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        let ListDetails = [];
        if (!_data) {
          return;
        }
        for (var i = 0; i < _data.length; i++) {
          if (
            listOrders[isActive]?.OrderPlanID_Detail === _data[i].OrderPlanID
          ) {
            _data[i].ListError = [];
            _data[i].ListPrice = [];
            for (var j = 0; j < _data.length; j++) {
              if (
                _data[j].LocationID === _data[i].LocationID &&
                _data[j].OrderPlanID === _data[i].OrderPlanID &&
                _data[j].Type === 'VD'
              ) {
                _data[i].ListError.push(_data[j]);
              }
              if (
                _data[j].LocationID === _data[i].LocationID &&
                _data[j].OrderPlanID === _data[i].OrderPlanID &&
                _data[j].Type === 'CP'
              ) {
                _data[i].ListPrice.push(_data[j]);
              }
            }
            if (
              _data[i].LocationName !== '' &&
              _data[i].LocationName !== null &&
              _data[i].LocationName !== undefined
            ) {
              ListDetails.push(_data[i]);
            }
          }
        }
        this.setState({ListDetails});
      })
      .catch(error => {
        console.log('errrrrrrrrrrrrrrrrrr');
      });
  };
  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  handleProcess = (item, index) => {
    this.DataView = item;
    this.setState({isActive: index}, () => {
      this.scrollToIndex(+index);
      this.getDetailByOrderPlanId(item.ItemID);
    });
  };
  scrollToIndex = index => {
    if (this.flatListRef.current) {
      this.flatListRef.current.scrollToIndex({animated: false, index});
    }
  };
  itemFlatListNavBar = ({item, index}) => {
    let colorProcess = '#dce0e4';
    let colorText = '#ada7ab';
    let colorNumber = '#ada7ab';
    if (index === this.state.isActive) {
      colorProcess = '#0094f2';
      colorText = '#ffffff';
      colorNumber = '#1997f2';
    }
    let style = {
      triangles: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 20,
        borderRightWidth: 0,
        borderBottomWidth: 20,
        borderLeftWidth: 10,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: colorProcess,
      },
      trianglesReveser: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 20,
        borderRightWidth: 0,
        borderBottomWidth: 20,
        borderLeftWidth: 10,
        borderTopColor: colorProcess,
        borderRightColor: colorProcess,
        borderBottomColor: colorProcess,
        borderLeftColor: 'transparent',
      },
    };
    return (
      <TouchableOpacity
        style={{height: 40, flexDirection: 'row'}}
        onPress={() => {
          this.handleProcess(item, index);
        }}>
        <View
          style={{backgroundColor: colorProcess, ...style.trianglesReveser}}
        />
        <View
          style={{
            flexDirection: 'row',
            width: Width * 0.65,
            backgroundColor: colorProcess,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={{
              borderRadius: 50,
              backgroundColor: 'white',
              width: 25,
              height: 25,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: colorNumber}}>{(+index + 1).toString()}</Text>
          </View>
          <Text style={{color: colorText, fontSize: 14, marginLeft: 10}}>
            {item.OrderPlanID_Detail}
          </Text>
        </View>
        <View style={{backgroundColor: colorProcess, ...style.triangles}} />
      </TouchableOpacity>
    );
  };
  render() {
    return this.state.loading === true ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={{flex: 1}}>
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          <TabBar_Title
            title={'Chi tiết tiến độ đơn hàng'}
            callBack={() => this.onPressBack()}
          />
          <Divider />
          <View style={{paddingVertical: 10}}>
            <FlatList
              data={this.state.listOrders}
              horizontal
              renderItem={(item, index) => {
                return this.itemFlatListNavBar(item, index);
              }}
              keyExtractor={(rs, index) => index.toString()}
              ref={this.flatListRef}
            />
          </View>
          {/* <ScrollView > */}
          {this.DataView !== null ? (
            <View style={{flex: 1}}>
              <View
                style={[
                  this.state.isHidden == true ? styles.hidden : '',
                  {backgroundColor: '#fff', paddingLeft: 10},
                ]}>
                <View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Số đơn hàng :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text
                        style={[AppStyles.Textdefault, {fontWeight: 'bold'}]}>
                        {this.DataView.OrderNumber}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Mã sản phẩm :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.DataView.ItemID}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Tên sản phẩm :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.DataView.ItemName}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Quy cách :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.DataView.Description}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>ĐVT :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.DataView.UnitID}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Slg PO :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.DataView.QuantityPO}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>
                        Lũy kế Slg thực hiện :
                      </Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.DataView.CumulativeQuantityOK}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Slg còn lại :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.DataView.QuantityRemain}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>HT :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.DataView.PercentComplete}%
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 1}}>
                      <Text style={AppStyles.Labeldefault}>Ngày giao YC :</Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 10}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.customDate(this.DataView.DeliveryDate)}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <Divider style={{marginTop: 10}} />
              <View
                style={{flexDirection: 'row', paddingRight: 10, marginTop: 10}}>
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    marginLeft: 10,
                    flex: 1,
                  }}>
                  Chi tiết
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View style={{paddingBottom: 5}}>
                  <FlatList
                    data={this.state.ListDetails}
                    renderItem={this._renderItem}
                    keyExtractor={(rs, index) => index.toString()}
                  />
                </View>
              </View>
            </View>
          ) : null}
          {/* </ScrollView> */}
        </View>
      </View>
    );
  }

  onPressBack = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'PPurchase', ActionTime: new Date().getTime()});
  };

  _renderItem = ({item}) => {
    return <MyListItem item={item} />;
  };

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemTicketRequestsComponent);

class MyListItem extends React.PureComponent {
  render() {
    const {item} = this.props;
    return (
      <View style={{fontSize: 11, fontFamily: Fonts.base.family}}>
        <ListItem
          title={() => {
            return (
              <View>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      marginRight: 30,
                      marginLeft: 15,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <ProgressCircle
                      percent={+item.PercentComplete}
                      radius={30}
                      borderWidth={10}
                      color="#0094f2"
                      shadowColor="#e4e4e4"
                      bgColor="#ffffff">
                      <Text style={{fontSize: 10}}>{`${
                        item.PercentComplete
                      }%`}</Text>
                    </ProgressCircle>
                  </View>
                  <View style={{flex: 3}}>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {fontWeight: 'bold', textTransform: 'uppercase'},
                      ]}>
                      {item.LocationName}
                    </Text>
                    <Text style={[AppStyles.Textdefault, {marginTop: 2}]}>
                      KH: {item.QuantityPlan}
                    </Text>
                    <Text style={[AppStyles.Textdefault, {marginTop: 2}]}>
                      OK: {item.QuantityOK}
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 2,
                    }}>
                    <Text style={[AppStyles.Textdefault]}>
                      NG: {item.QuantityNG}
                    </Text>
                    <Text style={[AppStyles.Textdefault, {marginTop: 2}]}>
                      Còn lại: {item.QuantityRemain}
                    </Text>
                    <Text style={[AppStyles.Textdefault, {marginTop: 2}]}>
                      Lũy kế: {item.AccumulatedAmount}
                    </Text>
                  </View>
                </View>
              </View>
            );
          }}
          bottomDivider
          titleStyle={AppStyles.Titledefault}
        />
      </View>
    );
  }
}
