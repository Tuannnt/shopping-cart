import {AppStyles, AppColors} from '@theme';
export default {
  //#region
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Mã sản phẩm', width: 150},
    {title: 'Tên sản phẩm', width: 150},
    {title: 'Số PO', width: 150},
    {title: 'ĐVT', width: 70},
    {title: 'Số lượng YC', width: 100},
    {title: 'Số lượng TN', width: 100},
    {title: 'Đơn giá', width: 150},
    {title: 'Thành tiền', width: 150},
  ],
};
