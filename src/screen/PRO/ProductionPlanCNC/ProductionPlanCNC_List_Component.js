import React, { Component } from 'react';
import {
  Alert,
  FlatList,
  Image,
  RefreshControl,
  Text,
  View,
  Keyboard,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import { Divider, Icon, ListItem, SearchBar } from 'react-native-elements';
import { API_ProductionPlanCNC } from '../../../network';
import TabBar from '../../component/TabBar';
import { FuncCommon } from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import { AppStyles, AppColors } from '@theme';
import FormSearch from '../../component/FormSearch';
import MenuSearchDate from '../../component/MenuSearchDate';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import DatePicker from 'react-native-date-picker';
import configApp from '../../../configApp';
const SCREEN_WIDTH = Dimensions.get('window').width;

class ProductionPlan_List_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      setEventEndDate: false,
      setEventStartDate: false,
      ValueSearchDate: '1', // 30 ngày trước
      loadingsearch: true,
      loading: true,
      ListAll: [], //biến chứa danh sách
      EvenFromSearch: false, // on/off form search
      //#region dữ liệu để lấy ra list
      JTableStore: {
        EndDate: new Date(),
        StartDate: new Date(),
        length: 15,
        IsProcess: '1',
        LocationCode: '',
        Type: 'U',
        search: {
          value: '',
        },
        CurrentPage: 1,
        QueryOrderBy: 'CreatedDate DESC',
      },
      //#endregion
    };
    //#region tabbarbottom
    this.listtabbarBotom = [
      {
        Title: 'GC Đồng',
        Icon: 'server',
        Type: 'feather',
        Value: 'U',
        Checkbox: true,
        Badge: 0,
      },
      {
        Title: 'GC Cơ khí',
        Icon: 'server',
        Type: 'feather',
        Value: 'K',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Lắp ráp & hoàn thiện',
        Icon: 'server',
        Type: 'feather',
        Value: 'D',
        Checkbox: false,
        Badge: 0,
      },
    ];
    //#endregion
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.JTable(true);
    }
  }

  //#region Hàm tìm kiếm
  Search = txt => {
    this.state.JTableStore.search.value = txt;
    this.setState({ JTableStore: this.state.JTableStore });
    this.JTable(true);
  };
  //#endregion

  //#region Refresh lại danh sách
  _onRefresh = () => {
    this.JTable(true);
  };
  //#endregion

  //#region API Danh sách

  JTable(s) {
    if (s === true) {
      this.state.JTableStore.CurrentPage = 1;
      this.setState({
        loadingsearch: true,
        ListAll: [],
      });
    } else {
      this.state.JTableStore.CurrentPage = ++this.state.JTableStore.CurrentPage;
    }
    let _obj = {
      ...this.state.JTableStore,
      StartDate: FuncCommon.ConDate(this.state.JTableStore.StartDate, 2),
      EndDate: FuncCommon.ConDate(this.state.JTableStore.EndDate, 2),
    };
    this.setState({ JTableStore: this.state.JTableStore });
    API_ProductionPlanCNC.JTableStore(_obj)
      .then(res => {
        console.log(res);
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
          this.state.ListAll.push(data[i]);
        }
        this.setState({
          ListAll: this.state.ListAll,
          loading: false,
          loadingsearch: false,
        });
      })
      .catch(error => {
        this.setState({ loading: false, loadingsearch: false });
        console.log(error);
      });
  }
  //#endregion

  //#region View tổng
  render() {
    return (
      <View
        style={{ flex: 1 }}
      >
        <View style={[AppStyles.container]}>
          <TabBar
            title={'KH bộ phận CNC'}
            BackModuleByCode={'PRO'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({ EvenFromSearch: callback })
            }
          />
          {this.state.EvenFromSearch === true ? (
            <View style={{ flexDirection: 'column' }}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                  <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({ setEventStartDate: true })}>
                    <Text>
                      {FuncCommon.ConDate(this.state.JTableStore.StartDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                  <Text style={AppStyles.Labeldefault}>Thời gian kết thúc</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({ setEventEndDate: true })}>
                    <Text>
                      {FuncCommon.ConDate(this.state.JTableStore.EndDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'column', padding: 5 }}>
                  <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <FormSearch CallbackSearch={callback => this.Search(callback)} />
            </View>
          ) : null}
          {this.state.loading !== true ? (

            this.ViewList(this.state.ListAll)
          ) : (
            <LoadingComponent backgroundColor={'#fff'} />
          )}
          {/* <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.CallbackValueBottom(callback)
              }
            />
          </View> */}
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.JTable(true), this.setState({ setEventStartDate: false });
                }}>
                <Text style={{ textAlign: 'right' }}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.JTableStore.StartDate}
                mode="date"
                style={{ width: SCREEN_WIDTH }}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.JTable(true), this.setState({ setEventEndDate: false });
                }}>
                <Text style={{ textAlign: 'right' }}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.JTableStore.EndDate}
                mode="date"
                style={{ width: SCREEN_WIDTH }}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
        </View>
      </View>
    );
  }
  //#endregion
  ListEmpty = () => {
    if (this.state.ListAll.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //#region View danh sách đã bóc tách
  ViewList = item => {
    return (
      <FlatList
        data={item}
        ListEmptyComponent={this.ListEmpty}
        renderItem={({ item, index }) => (
          <TouchableOpacity onPress={() => this.ActionViewOpen(item)}>
            <ListItem
              key={index}
              leftAvatar={
                <Image
                  style={{ width: 50, height: 50 }}
                  source={
                    item.UrlImage
                      ? { uri: item.UrlImage }
                      : require('./no_image.png')
                  }
                />
              }
              title={
                <View style={{ flexDirection: 'column' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>Mã NC: </Text>
                    <Text style={{ fontWeight: 'bold' }}>{item.CamSheetID}</Text>
                  </View>
                </View>
              }
              subtitle={() => {
                return (
                  <View style={{ flexDirection: 'column' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      {item.OrderItemName.length > 35
                        ? item.OrderItemName.substring(0, 35) + '...'
                        : item.OrderItemName}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontWeight: 'bold' }}>Mã bản vẽ: </Text>
                      <Text>{item.OrderItemId}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={{ fontWeight: 'bold' }}>ĐVT: </Text>
                        <Text>{item.UnitName}</Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={{ fontWeight: 'bold' }}>Số lượng: </Text>
                        <Text>{this.addPeriod(item.Quantity)}</Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={{ fontWeight: 'bold' }}>Ngày BĐ: </Text>
                        <Text>{FuncCommon.ConDate(item.StartDate, 0)}</Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'flex-end',
                        }}>
                        <Text>{item.LocationTitle}</Text>
                      </View>
                    </View>
                  </View>
                );
              }}
              bottomDivider
            />
          </TouchableOpacity>
        )}
        onEndReached={() =>
          item.length >= 50 * this.state.JTableStore.CurrentPage
            ? this.JTable(false)
            : null
        }
        keyExtractor={(rs, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    );
  };
  //#endregion

  //#region thay đổi thời gian tìm kiếm
  setStartDate = date => {
    this.state.JTableStore.StartDate = date;
    this.setState({ JTableStore: this.state.JTableStore });
    this.JTable(true);
  };
  setEndDate = date => {
    this.state.JTableStore.EndDate = date;
    this.setState({ JTableStore: this.state.JTableStore });
    this.JTable(true);
  };
  //#endregion
  //#region chức năng lọc này cố định
  _openMenuSearchDate() { }
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.state.JTableStore.StartDate = new Date(callback.start);
      this.state.JTableStore.EndDate = new Date(callback.end);
      this.setState({
        JTableStore: this.state.JTableStore,
        ValueSearchDate: callback.value,
      });
      this.JTable(true);
    }
  };
  //#endregion
  //#region CallbackValueBottom
  CallbackValueBottom = callback => {
    this.state.JTableStore.Type = callback;
    this.setState({ JTableStore: this.state.JTableStore });
    this.JTable(true);
  };
  //#endregion
  //#region ViewOpen
  ActionViewOpen = item => {
    Actions.ProductionPlanCNC_Open({ Data: item });
  };
  //#endregion
  //#region Converdate
  ConvertDateSearch = type => {
    var newdate = new Date();
    var month = newdate.getMonth() + 1;
    var monthBefore = newdate.getMonth();
    var year = newdate.getFullYear();
    var day = newdate.getDate();
    if (month < 10) month = '0' + month;
    if (monthBefore < 10) monthBefore = '0' + monthBefore;
    if (day < 10) day = '0' + day;
    if (type === 1) {
      return year + '-' + monthBefore + '-' + day;
    } else {
      return year + '-' + month + '-' + day;
    }
  };
  //#endregion
  //#region CheckStatus
  CheckStatus = data => {
    if (data === 'Y') {
      return 'Đã duyệt';
    } else if (data === 'N') {
      return 'Chưa duyệt';
    } else if (data === 'NY') {
      return 'Không duyệt';
    } else {
      return 'Bản nháp';
    }
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ProductionPlan_List_Component);
