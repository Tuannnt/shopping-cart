import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  Keyboard,
  View,
  TouchableOpacity,
  Text,
  Animated,
  Dimensions,
  ScrollView,
  Alert,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_ProposePurchases from '../../../network/BM/API_ProposePurchases';
import API from '../../../network/API';
import ErrorHandler from '../../../error/handler';
import TabBarBottom from '../../component/TabBarBottom';
import Dialog from 'react-native-dialog';
import {DatePicker, Container, Content, Item, Label, Picker} from 'native-base';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import RNPickerSelect from 'react-native-picker-select';
import LoadingComponent from '../../component/LoadingComponent';
import CustomView from '../../component/CustomView';
import {ToastAndroid} from 'react-native';
import {FuncCommon} from '@utils';
import Toast from 'react-native-simple-toast';

const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const styles = StyleSheet.create({
  hidden: {
    display: 'none',
  },
});

class ItemProposedPurchasesComponent extends Component {
  constructor(props) {
    super(props);
    this.DataView = null;
    this.state = {
      Data: null,
      listDetail: [],
      staticParam: {
        ProposedPurchaseGuid: null,
        QueryOrderBy: 'ItemName desc',
      },
      isHidden: false,
      checkInLogin: '',
      visible: false,
      ListReasons: [
        {
          ReasonName: 'Chọn',
          ReasonId: '',
        },
      ],
      selected: undefined,
      RequestComment: '',
      ReasonName: '',
      Type: '',
      Receivers: [],
      FullName: '',
      loading: true,
      CancelRequest: false,

      //hiển thị thông tin chung
      ViewAll: false,
      transIcon_ViewAll: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewDetail: false,
      transIcon_ViewDetail: new Animated.Value(0),
      Data_ViewCustom: null,
    };
  }

  componentDidMount = () => {
    this.getItem();
    this.Skill();
    this.setViewOpen('ViewTotal');
    // API_ProposePurchases.GetCancelReasons()
    //   .then(rs => {
    //     var _listReasons = rs.data;
    //     let _reListReasons = [
    //       {
    //         ReasonName: 'Danh sách lý do',
    //         ReasonId: '',
    //       },
    //     ];
    //     for (var i = 0; i < _listReasons.length; i++) {
    //       _reListReasons.push({
    //         ReasonName: _listReasons[i].ReasonName,
    //         ReasonId: _listReasons[i].ReasonId,
    //         Type: _listReasons[i].Type,
    //       });
    //     }
    //     this.setState({
    //       ListReasons: _reListReasons,
    //     });
    //   })
    //   .catch(error => {
    //     console.log(error);
    //   });
    API.getProfile().then(rs => {
      this.setState({FullName: rs.data.FullName});
    });
  };

  getItem() {
    let id = this.props.RecordGuid;
    API_ProposePurchases.ProposedPurchases_GetItem(id)
      .then(res => {
        this.DataView = JSON.parse(res.data.data);
        let check = {
          ProposedPurchaseGuid: this.props.RecordGuid,
          WorkFlowGuid: JSON.parse(res.data.data).WorkFlowGuid,
        };
        API_ProposePurchases.CheckLogin(check)
          .then(rs => {
            this.setState({checkInLogin: rs.data.data});
          })
          .catch(error => {
            console.log(error);
          });
        this.GetDetail();
      })
      .catch(error => {
        this.setState({loading: false});
      });
  }

  GetDetail = () => {
    this.setState({refreshing: true});
    this.state.staticParam.ProposedPurchaseGuid = this.props.RecordGuid;
    API_ProposePurchases.ProposedPurchaseDetails_ListDetail(
      this.state.staticParam,
    )
      .then(res => {
        this.state.listDetail = JSON.parse(res.data.data);
        this.setState({
          list: this.state.listDetail,
          refreshing: false,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          refreshing: false,
          loading: false,
        });
      });
  };

  ListEmpty = () => {
    if (this.state.listDetail.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  callbackOpenUpdate = () => {
    Actions.bmitemAddProposedPurchase({
      // itemData: this.DataView.Data,
      // rowData: this.DataView.DataDetail,
    });
  };
  render() {
    const rotateStart_ViewAll = this.state.transIcon_ViewAll.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewDetail = this.state.transIcon_ViewDetail.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading ? (
      <LoadingComponent backgroundColor={'#fff'} />
    ) : (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết đề nghị mua'}
          callBack={() => this.onPressBack()}
          CallbackFormEdit={() => this.onCancelRequests()}
        />
        <View style={{flex: 1}}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewAll')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
            </View>
            <Animated.View
              style={{
                transform: [{rotate: rotateStart_ViewAll}, {perspective: 4000}],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewAll !== true ? null : (
            <View style={[{padding: 10, flexDirection: 'row'}]}>
              <View style={{flex: 1}}>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Số phiếu :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Người đề nghị :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Bộ phận :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Loại phiếu :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Ngày đề nghị :
                </Text>

                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  TT xử lý :
                </Text>
                <Text style={[AppStyles.Labeldefault, {paddingBottom: 5}]}>
                  Mục đích đề xuất mua hàng :
                </Text>
              </View>
              <View style={{flex: 2, alignItems: 'flex-end'}}>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.DataView.ProposedPurchaseId}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.DataView.EmployeeName}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.DataView.DepartmentName}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {this.DataView.TypeOfRequest == 'SX'
                    ? 'Sản xuất'
                    : this.DataView.TypeOfRequest == 'TM'
                    ? 'Thương mại'
                    : this.DataView.TypeOfRequest == 'GT'
                    ? 'Hỗ trợ sản xuất'
                    : ''}
                </Text>
                <Text style={[AppStyles.Textdefault, {paddingBottom: 5}]}>
                  {FuncCommon.ConDate(this.DataView.CreatedDate, 0)}
                </Text>
                <Text
                  style={[
                    [AppStyles.Textdefault],
                    {paddingBottom: 5},
                    this.DataView.Status == 'Y'
                      ? {color: AppColors.AcceptColor}
                      : {color: AppColors.PendingColor},
                  ]}>
                  {this.DataView.StatusWF}
                </Text>
                <Text style={[AppStyles.Textdefault]}>
                  {this.DataView.Description}
                </Text>
              </View>
            </View>
          )}
          <Divider />
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewDetail')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewDetail},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewDetail !== true ? (
            <View style={{flex: 1}} />
          ) : (
            <View style={{flex: 1}}>
              <ScrollView>
                {this.state.listDetail.map((item, index) => (
                  <View key={index}>
                    <ListItem
                      title={() => {
                        return (
                          <Text
                            style={[
                              AppStyles.Titledefault,
                              {width: '100%', flex: 10},
                            ]}>
                            {item.ItemName}
                          </Text>
                        );
                      }}
                      subtitle={() => {
                        return (
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <View style={{width: '65%'}}>
                              <Text style={AppStyles.Textdefault}>
                                Ngày yêu cầu:{' '}
                                {FuncCommon.ConDate(item.RequestDate, 0)}
                              </Text>
                              <Text style={AppStyles.Textdefault}>
                                ĐVT: {item.UnitName}
                              </Text>
                            </View>
                            <View style={{width: '35%'}}>
                              <Text style={AppStyles.Textdefault}>
                                Số lượng:{' '}
                                {FuncCommon.addPeriodTextInput(
                                  item.QuantityUsed,
                                )}
                              </Text>
                              <Text style={AppStyles.Textdefault}>
                                Đơn giá:{' '}
                                {FuncCommon.addPeriodTextInput(item.UnitPrice)}
                              </Text>
                            </View>
                          </View>
                        );
                      }}
                      bottomDivider
                      titleStyle={AppStyles.Titledefault}
                      onPress={() => {
                        this.setState({Data_ViewCustom: item}),
                          this.openAcctionViewDetail();
                      }}
                    />
                  </View>
                ))}
              </ScrollView>
            </View>
          )}
          {/*nút xử lý*/}
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottom
              onDelete={() => this.onDelete()}
              onAttachment={() => this.openAttachments()}
              keyCommentWF={{
                Type: 1,
                ModuleId: '23',
                RecordGuid: this.DataView.ProposedPurchaseGuid,
                Title: this.DataView.ProposedPurchaseId,
                // phieu cu khong dung RowGuid thi them Type = 1
              }}
              Title={this.DataView.ProposedPurchaseId}
              Permisstion={this.DataView.Permisstion}
              checkInLogin={this.state.checkInLogin}
              onSubmitWF={(callback, CommentWF) =>
                this.submitWorkFlow(callback, CommentWF)
              }
            />
          </View>
          {/* Popup */}
          <View>
            <Dialog.Container visible={this.state.visible}>
              <Dialog.Title style={AppStyles.Titledefault}>
                {this.state.Title}
              </Dialog.Title>
              <Dialog.Description>
                Yêu cầu sửa, hủy {this.DataView.ProposedPurchaseId}
              </Dialog.Description>
              <Item stackedLabel>
                <Label style={[AppStyles.Labeldefault, {paddingLeft: 5}]}>
                  Nguyên nhân
                </Label>
                {/* <RNPickerSelect
                        doneText="Xong"
            doneText="Xong"
                                        onValueChange={(value) => this.onValueChange(value)}
                                        items={this.state.ListReasons}
                                        placeholder={{}}
                                    /> */}
                <Item picker>{this.renderReasons()}</Item>
              </Item>
              <Dialog.Input
                style={{borderBottomWidth: 1, borderBottomColor: '#3366CC'}}
                label={this.state.desciption}
                onChangeText={text => this.setState({RequestComment: text})}
                value={this.state.RequestComment}
              />
              <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <Dialog.Button
                  style={{
                    fontSize: 12,
                    backgroundColor: '#055a70',
                    color: '#fff',
                    borderRadius: 5,
                    marginLeft: 5,
                  }}
                  label="Xác nhận"
                  onPress={() => this.onYes()}
                />
                <Dialog.Button
                  style={{
                    fontSize: 12,
                    backgroundColor: '#dc3545',
                    color: '#fff',
                    borderRadius: 5,
                    marginLeft: 5,
                  }}
                  label="Đóng"
                  onPress={() => this.onClose()}
                />
              </View>
            </Dialog.Container>
          </View>
        </View>
        {this.CustomView()}
      </View>
    );
  }
  CustomView = () => {
    const {Data_ViewCustom} = this.state;
    return (
      <CustomView eOpen={this.openViewDetail}>
        {Data_ViewCustom === null ? null : (
          <View style={{padding: 10, width: DRIVER.width - 50}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                Mã đề nghị:
              </Text>
              <Text style={[AppStyles.Textdefault, {flex: 2}]}>
                {Data_ViewCustom.ItemId}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Tên hàng:</Text>
              <Text style={[AppStyles.Textdefault, {flex: 2}]}>
                {Data_ViewCustom.ItemName}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                Tiêu chuẩn kĩ thuật:
              </Text>
              <Text style={[AppStyles.Textdefault, {flex: 2}]}>
                {Data_ViewCustom.Specification}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>ĐVT :</Text>
              <Text style={[AppStyles.Textdefault, {flex: 2}]}>
                {Data_ViewCustom.UnitName}
              </Text>
            </View>

            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                SL đề nghị:
              </Text>
              <Text
                style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
                {FuncCommon.addPeriodTextInput(Data_ViewCustom.QuantityUsed)}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Đơn giá:</Text>
              <Text
                style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
                {FuncCommon.addPeriodTextInput(Data_ViewCustom.UnitPrice)}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                Ngày yêu cầu:
              </Text>
              <Text
                style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
                {FuncCommon.ConDate(Data_ViewCustom.RequestDate, 0)}
              </Text>
            </View>
            {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
              <Text style={[AppStyles.Labeldefault, { flex: 1 }]}>Ngày cần :</Text>
              <Text style={[AppStyles.Textdefault, { flex: 2 }]}>{FuncCommon.ConDate(Data_ViewCustom.DateNeed, 0)}</Text>
            </View> */}
            {/* <Divider style={{marginTop: 10}} />
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
                Tổng tiền :
              </Text>
              <Text
                style={[
                  AppStyles.Labeldefault,
                  {flex: 2, textAlign: 'right'},
                ]}>
                {FuncCommon.addPeriodTextInput(
                  Data_ViewCustom.QuantityUsed * Data_ViewCustom.UnitPrice,
                )}
              </Text>
            </View> */}
          </View>
        )}
      </CustomView>
    );
  };
  _openViewDetail() {}
  openViewDetail = d => {
    this._openViewDetail = d;
  };
  openAcctionViewDetail = () => {
    this._openViewDetail();
  };
  onPressBack = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'PPurchase', ActionTime: new Date().getTime()});
  };
  onDelete = () => {
    let obj = {
      ProposedPurchaseGuid: this.DataView.ProposedPurchaseGuid,
    };
    this.setState({loading: true});
    API_ProposePurchases.DeleteProposedPurchaseById(obj.ProposedPurchaseGuid)
      .then(response => {
        let errorCode = response.data.errorCode;
        if (errorCode == 200) {
          this.setState(
            {
              loading: false,
            },
            () => {
              Toast.showWithGravity(
                'Xóa thành công',
                Toast.SHORT,
                Toast.CENTER,
              );
              this.onPressBack();
            },
          );
        } else {
          Toast.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            Toast.SHORT,
            Toast.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  };

  renderReasons = () => (
    <Picker
      mode="dropdown"
      iosIcon={<Icon name="arrow-down" />}
      style={AppStyles.Textdefault}
      placeholder="Chọn nguyên nhân"
      placeholderStyle={{color: '#bfc6ea'}}
      placeholderIconColor="#007aff"
      selectedValue={this.state.selected}
      onValueChange={this.onValueChange.bind(this)}>
      {this.state.ListReasons && this.state.ListReasons.length > 0 ? (
        this.state.ListReasons.map((_item, _index) => (
          <Picker.Item
            key={_index}
            label={_item.ReasonName}
            value={_item.ReasonId}
          />
        ))
      ) : (
        <Picker.Item key={_index} label="Chọn nguyên nhân" value="" />
      )}
    </Picker>
  );
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    this.setState({loading: true}, () => {
      let obj = {
        ProposedPurchaseGuid: this.props.RecordGuid,
        WorkFlowGuid: this.DataView.WorkFlowGuid,
        // LoginName: this.state.LoginName,
        Comment: CommentWF,
      };
      if (callback === 'D') {
        API_ProposePurchases.Approve(obj)
          .then(res => {
            if (res.data.errorCode === 200) {
              Toast.showWithGravity(
                'Xử lý thành công',
                Toast.SHORT,
                Toast.CENTER,
              );
              this.onPressBack();
              this.setState({loading: false});
            } else {
              Toast.showWithGravity(
                'Có lỗi khi xử lý phiếu',
                Toast.SHORT,
                Toast.CENTER,
              );
              this.getItem();
              this.setState({loading: false});
            }
          })
          .catch(error => {
            console.log(error.data);
          });
      } else {
        this.NotApprove(obj);
      }
    });
  }

  NotApprove = obj => {
    API_ProposePurchases.NotApprove(obj)
      .then(response => {
        if (response.data.errorCode === 200) {
          Toast.showWithGravity(
            'Trả lại thành công',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.onPressBack();
          this.setState({loading: false});
        } else {
          Toast.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            Toast.SHORT,
            Toast.CENTER,
          );
          this.getItem();
          this.setState({loading: false});
        }
      })
      .catch(error => {
        console.log(error.response.data);
        console.log(error.data);
      });
  };

  onValueChange(value) {
    this.setState({
      selected: value,
    });
    let rs = this.state.ListReasons.find(x => x.ReasonId === value);
    if (rs != null) {
      this.setState({ReasonName: rs.ReasonName});
      this.setState({Type: rs.Type});
    }
  }
  onCancelRequests() {
    if (this.state.checkInLogin === 1) {
      Alert.alert('Thông báo', '', [{text: 'Đóng'}], {cancelable: true});
    } else {
      this.setState({
        visible: true,
      });
    }
  }
  onYes() {
    let obj = {
      RecordGuid: this.DataView.ProposedPurchaseGuid,
      RequestComment: this.state.RequestComment,
      ReasonId: this.state.selected,
      ReasonName: this.state.ReasonName,
      Type: this.state.Type,
    };
    API_ProposePurchases.CancelRequest(obj)
      .then(res => {
        this.setState({
          visible: false,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      })
      .catch(error => {
        this.setState({
          visible: true,
        });
        Alert.alert('Thông báo', res.data.message, [{text: 'Đóng'}], {
          cancelable: true,
        });
      });
  }
  onClose() {
    this.setState({
      visible: false,
    });
  }
  openDetail(id) {
    Actions.bmitemPPDetails({rowGuid: id});
  }
  openAttachments() {
    var obj = {
      ModuleId: '2',
      RecordGuid: this.DataView.ProposedPurchaseGuid,
      notEdit: this.state.checkInLogin === '1' ? false : true,
    };
    Actions.attachmentComponent(obj);
  }
  //#region Skill
  Skill = () => {
    this.Animated_on_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAll = Animated.timing(this.state.transIcon_ViewAll, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_on_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewDetail = Animated.timing(
      this.state.transIcon_ViewDetail,
      {toValue: 0, duration: 333},
    );
  };
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewAll') {
      if (this.state.ViewAll === false) {
        Animated.sequence([this.Animated_on_ViewAll]).start();
        // Animated.sequence([this.Animated_off_ViewDetail]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAll]).start();
      }
      this.setState({ViewAll: !this.state.ViewAll});
    } else if (val === 'ViewDetail') {
      if (this.state.ViewDetail === false) {
        Animated.sequence([this.Animated_on_ViewDetail]).start();
        // Animated.sequence([this.Animated_off_ViewAll]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewDetail]).start();
      }
      this.setState({ViewDetail: !this.state.ViewDetail});
    } else if (val === 'ViewTotal') {
      Animated.sequence([
        this.Animated_on_ViewAll,
        this.Animated_on_ViewDetail,
      ]).start();
      // Animated.sequence([this.Animated_off_ViewAll]).start();
      this.setState({ViewDetail: true, ViewAll: true});
    }
  };
  //#endregion
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemProposedPurchasesComponent);
