import React, {Component} from 'react';
import {
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';
import {Button, Divider, Icon as IconElement} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_RegisterCars} from '@network';
import {Container} from 'native-base';
import {FuncCommon} from '../../../utils';
import {AppStyles, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import DatePicker from 'react-native-datepicker';
import listCombobox from './listCombobox';
import ComboboxV2 from '../../component/ComboboxV2';
import RequiredText from '../../component/RequiredText';
import OpenPhotoLibrary from '../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import configApp from '../../../configApp';
import controller from './controller';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Combobox from '../../component/Combobox';
import moment from 'moment';
import _ from 'lodash';

const ListType = [
  {
    value: null,
    label: '--Chọn loại--',
  },
  {
    value: 'TM',
    label: 'Thương mại',
  },
  {
    value: 'SX',
    label: 'Sản xuất',
  },
  {
    value: 'GCN',
    label: 'Gia công ngoài',
  },
  {
    value: 'DV',
    label: 'Dịch vụ - GCN',
  },
];
const ListTypeConfig = [
  {
    value: null,
    label: '--Chọn nội dung--',
  },
  {
    value: 'MM',
    label: 'Mua máy móc, ccdc',
  },
  {
    value: 'VT',
    label: 'Mua vật tư sản xuất',
  },
  {
    value: 'HH',
    label: 'Mua hàng thương mại',
  },
  {
    value: 'DH',
    label: 'Mua vật tư tủ điện',
  },
  {
    value: 'GCN',
    label: 'Gia công ngoài',
  },
];
const headerTable = listCombobox.headerTable;
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 5,
    paddingVertical: 4,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 10, // to ensure the text is never behind the icon
  },
});
class AddProposedPurchase extends Component {
  constructor(props) {
    super(props);
    this.DataProcess = FuncCommon.DataProcess();
    this.state = {
      rows: [{}],
      isEdit: true,
      Attachment: [],
      refreshing: false,
      Data: null,
      DataDetail: null,
      search: '',
      RequestDate: FuncCommon.ConDate(new Date(), 0),
      modalPickerTimeVisible: false,
      ListEmployee: [],
      allItems: [],
      allOrders: [],
      allUnits: [],
      currentIndexItem: null,
      modalPickerTimeVisible: false,
    };
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  helperNumber = num => {
    if (!num) {
      return '';
    }
    return num + '';
  };
  componentDidMount(): void {
    const {dataEdit, rows = []} = this.props;
    if (dataEdit) {
      this.setState({
        isEdit: false,
        Title: dataEdit.Title,
        Description: dataEdit.Description,
        Note: dataEdit.Note,
        ProposedPurchaseId: dataEdit.ProposedPurchaseId,
        FullName: dataEdit.FullName,
        DepartmentName: dataEdit.DepartmentName,
        JobTitleNameEmployee: dataEdit.JobTitleName,
        rows: rows,
      });
    } else {
      Promise.all([
        this.getNumberAuto(),
        this.GetItemsAllJExcel(),
        this.GetAllOrders(),
        this.GetAllUnits(),
      ]);
      this.setState({
        FullName: global.__appSIGNALR.SIGNALR_object.USER.FullName,
        DepartmentName: global.__appSIGNALR.SIGNALR_object.USER.DepartmentName,
        JobTitleNameEmployee:
          global.__appSIGNALR.SIGNALR_object.USER.JobTitleName,
      });
    }
  }
  getNumberAuto = () => {
    let val = {
      AliasId: 'QTDSCT_DNM',
      Prefixs: null,
      VoucherType: '',
    };
    controller.getNumberAuto(val, rs => {
      let data = JSON.parse(rs);
      console.log(data.Value + '=====> isEdit');
      this.setState({isEdit: data.IsEdit, ProposedPurchaseId: data.Value});
    });
  };
  GetItemsAllJExcel = () => {
    this.setState({
      allItems: [{value: '000000111', text: '[ 000000111 ] - Máy A'}],
    });
    let val = {Searchs: ''};
    controller.GetItemsAllJExcel(val, rs => {});
  };
  GetAllOrders = () => {
    this.setState({
      allOrders: [
        {
          ContracNo: null,
          OrderGuid: '01f0d7a2-0763-4f1d-97b6-bf81d18a1216',
          value: 'DDH20200822-001',
          text: 'DDH20200822-001',
        },
      ],
    });
    let val = {};
    controller.GetAllOrders(val, rs => {});
  };
  GetAllUnits = () => {
    this.setState({
      allUnits: [
        {
          value: null,
          label: 'Chọn Đvt',
        },
        {
          value: 'BAG',
          label: 'Túi',
        },
      ],
    });
    let val = {};
    controller.GetAllUnits(val, rs => {});
  };
  Submit() {
    Keyboard.dismiss();
    const {
      Title,
      Description,
      Note,
      ProposedPurchaseId,
      Attachment,
    } = this.state;
    let data = {
      Title: this.state.Title,
      Description,
      ProposedPurchaseId,
      Note,
    };
    var _obj = {
      FileAttachments: [],
      ...data,
    };

    if (!Title) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập tiêu đề kế hoạch',
        [{text: 'OK', onPress: () => Keyboard.dismiss()}],
        {cancelable: false},
      );
      return;
    }
    if (!ProposedPurchaseId) {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập Mã kế hoạch',
        [{text: 'OK', onPress: () => Keyboard.dismiss()}],
        {cancelable: false},
      );
      return;
    }
    let isNotValidated = false;
    let lstDetails = [...this.state.rows];
    for (let i = 0; i < lstDetails.length; i++) {
      let lst = lstDetails[i];
      if (!lst.EmployeeGuid) {
        Alert.alert(
          'Thông báo',
          'Bạn chưa chọn nhân viên',
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        isNotValidated = true;
        return;
      }
      if (!lst.StartDate) {
        Alert.alert(
          'Thông báo',
          'Bạn chưa chọn ngày bắt đầu',
          [{text: 'OK', onPress: () => Keyboard.dismiss()}],
          {cancelable: false},
        );
        isNotValidated = true;
        return;
      }
    }
    if (isNotValidated) {
      return;
    }
    let auto = {
      Value: this.state.ProposedPurchaseId,
      IsEdit: this.state.isEdit,
    };
    lstDetails = lstDetails.map(detail => ({
      ...detail,
    }));
    if (this.props.dataEdit) {
      let _edit = {
        ...this.props.dataEdit,
        ...data,
        Id: ProposedPurchaseId,
      };
      let _dataEdit = new FormData();
      _dataEdit.append('TrainingPlans', JSON.stringify(_edit));
      _dataEdit.append('auto', JSON.stringify({IsEdit: true}));
      _dataEdit.append('DeleteAttach_orders', JSON.stringify([]));
      _dataEdit.append('lstDetails', JSON.stringify(lstDetails));
      _dataEdit.append('lstTitlefile', JSON.stringify([]));
      _dataEdit.append('ListDetailDelete', JSON.stringify([]));
      API_RegisterCars.TrainingPlans_UpdateAPI(_dataEdit)
        .then(rs => {
          if (rs.data.errorCode === 200) {
            Alert.alert(
              'Thông báo',
              'Cập nhật thành công',
              [{text: 'OK', onPress: () => Keyboard.dismiss()}],
              {cancelable: false},
            );
            this.onPressBack();
          }
        })
        .catch(error => {
          console.log('Error when call API update Mobile.');
          console.log(error);
        });
      return;
    }
    let _data = new FormData();
    var _lstTitlefile = [];
    for (var i = 0; i < Attachment.length; i++) {
      _lstTitlefile.push({
        STT: i,
        Title: Attachment[i].FileName,
      });
    }
    _data.append('TrainingPlans', JSON.stringify(_obj));
    _data.append('auto', JSON.stringify(auto));
    _data.append('DeleteAttach', JSON.stringify([]));
    _data.append('lstDetails', JSON.stringify(lstDetails));
    _data.append('lstTitlefile', JSON.stringify(_lstTitlefile));

    for (var i = 0; i < Attachment.length; i++) {
      _data.append(Attachment[i].FileName, {
        name: Attachment[i].FileName,
        size: Attachment[i].size,
        type: Attachment[i].type,
        uri: Attachment[i].uri,
      });
    }
    API_RegisterCars.TrainingPlans_InsertAPI(_data)
      .then(rs => {
        if (rs.data.errorCode === 200) {
          Alert.alert(
            'Thông báo',
            'Thêm mới thành công',
            [{text: 'OK', onPress: () => Keyboard.dismiss()}],
            {cancelable: false},
          );
          this.onPressBack();
        }
      })
      .catch(error => {
        console.log('Error when call API insert Mobile.');
        console.log(error);
      });
  }
  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion
  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(listCombobox.options, response => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: _liFiles,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  openItems(index) {
    this.setState({currentIndexItem: index}, () => {
      this._openCombobox();
    });
  }
  _openComboboxOrder() {}
  openComboboxOrder = d => {
    this._openComboboxOrder = d;
  };
  openOrders(index) {
    this.setState({currentIndexItem: index}, () => {
      this._openComboboxOrder();
    });
  }
  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  removeAttactment(para) {
    controller.removeAttactment(para, this.state.Attachment, rs => {
      this.setState({
        Attachment: rs,
      });
    });
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };
  //#endregion
  onChangeText = (key, value) => {
    this.setState({[key]: value});
  };
  addRows = () => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    if (data.length === 0) {
      data = [{}];
    } else {
      data.push({});
    }
    this.setState({rows: data});
  };
  onDelete = index => {
    const {rows} = this.state;
    let data = _.cloneDeep(rows);
    let res = data.filter((row, i) => i !== index);

    this.setState({rows: res});
  };
  handleChangeRows = (val, index, name) => {
    const {rows} = this.state;
    let res = _.cloneDeep(rows);
    res[index][name] = val;
    this.setState({rows: res});
  };
  helperPosition = id => {
    if (!id) {
      return '';
    }
    let employee = this.state.ListEmployee.find(emp => emp.value === id);
    if (!employee) {
      return '';
    }
    return employee.JobTitleName;
  };
  handlePickTimeOpen = (type, index) => {
    this.setState({
      typeTime: type,
      currentIndex: index,
      modalPickerTimeVisible: true,
    });
  };
  handleConfirmTime = date => {
    const {typeTime, currentIndex, rows} = this.state;
    let res = _.cloneDeep(rows);
    res[currentIndex][typeTime] = date;
    this.setState({rows: res, typeTime: null, currentIndex: null});
    this.hideDatePicker();
  };
  hideDatePicker = () => {
    this.setState({
      modalPickerTimeVisible: false,
      typeTime: null,
      currentIndex: null,
    });
  };
  itemFlatList = (row, index) => {
    const stylePicker =
      Platform.OS === 'android'
        ? pickerSelectStyles.inputAndroid
        : pickerSelectStyles.inputIOS;
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: 200}]}
          onPress={() => this.openItems(index)}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 2,
            }}>
            <Text style={[AppStyles.TextInput, {color: 'black', fontSize: 16}]}>
              {row.ItemName ? row.ItemName : 'Chọn hàng'}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <IconElement
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.ItemName}
            autoCapitalize="none"
            onChangeText={() => {}}
          />
        </TouchableOpacity>
        {this.state.TypeOfRequest && this.state.TypeOfRequest !== 'TM' && (
          <TouchableOpacity
            style={[AppStyles.table_td_custom, {width: 150}]}
            onPress={() => this.openItems(index)}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 2,
              }}>
              <Text
                style={[AppStyles.TextInput, {color: 'black', fontSize: 16}]}>
                {row.ItemName ? row.ItemName : 'Chọn hàng'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <IconElement
                  color={'black'}
                  name={'caretdown'}
                  type="antdesign"
                  size={10}
                />
              </View>
            </View>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={[AppStyles.table_td_custom, {width: 200}]}
          onPress={() => this.openOrders(index)}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <Text
              style={[
                AppStyles.TextInput,
                row.OrderName,
                {color: 'black', fontSize: 16},
              ]}>
              {row.OrderName ? row.OrderName : 'Chọn đơn hàng'}
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <IconElement
                color={'black'}
                name={'caretdown'}
                type="antdesign"
                size={10}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Specification}
            autoCapitalize="none"
            onChangeText={Specification => {
              this.handleChangeRows(Specification, index, 'Specification');
            }}
          />
        </TouchableOpacity>
        <View style={[AppStyles.table_td_custom, {width: 150}]}>
          <RNPickerSelect
            doneText="Xong"
            doneText="Xong"
            onValueChange={value => {
              this.handleChangeRows(value, index, 'UnitId');
            }}
            items={this.state.allUnits}
            style={{
              ...stylePicker,
              iconContainer: {
                top: 18,
                right: 10,
              },
            }}
            value={row.UnitId}
            placeholder={{}}
            Icon={() => {
              return (
                <View>
                  <IconElement
                    color={'black'}
                    name={'caretdown'}
                    type="antdesign"
                    size={10}
                  />
                </View>
              );
            }}
          />
        </View>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Quantity}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={Quantity => {
              this.handleChangeRows(Quantity, index, 'Quantity');
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <Text
            style={[
              AppStyles.Textdefault,
              {textAlign: 'left', color: AppColors.red, paddingLeft: 5},
            ]}>
            {row.QuantityReamin || ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.QuantityUsed}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={QuantityUsed => {
              this.handleChangeRows(QuantityUsed, index, 'QuantityUsed');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 100}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.UnitPrice}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={UnitPrice => {
              this.handleChangeRows(UnitPrice, index, 'UnitPrice');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.InventoryNumber}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={InventoryNumber => {
              this.handleChangeRows(InventoryNumber, index, 'InventoryNumber');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.handlePickTimeOpen('RequestDate', index);
          }}
          style={[AppStyles.table_td_custom, {width: 120}]}>
          <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
            {(row.RequestDate &&
              moment(row.RequestDate).format('DD/MM/YYYY')) ||
              'Chọn thời gian'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.Note}
            autoCapitalize="none"
            onChangeText={Note => {
              this.handleChangeRows(Note, index, 'Note');
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 150}]}>
          <TextInput
            style={[AppStyles.FormInput_custom, {color: 'black'}]}
            underlineColorAndroid="transparent"
            value={row.AlternateInformation}
            autoCapitalize="none"
            keyboardType="numeric"
            onChangeText={AlternateInformation => {
              this.handleChangeRows(
                AlternateInformation,
                index,
                'AlternateInformation',
              );
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.onDelete(index);
          }}
          style={[AppStyles.table_td_custom, {width: 80}]}>
          <IconElement
            name="delete"
            type="antdesign"
            color={AppColors.red}
            size={16}
          />
        </TouchableOpacity>
      </View>
    );
  };
  helperToStringCurrency = (number, n = 2, x = 3, s = '.', c = ',') => {
    if (!number) {
      return '';
    }
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
      num = Number(number).toFixed(Math.max(0, ~~n));
    return (c ? num.replace('.', c) : num).replace(
      new RegExp(re, 'g'),
      '$&' + (s || ','),
    );
  };
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  render() {
    const {isEdit, ProposedPurchaseId} = this.state;
    return (
      <Container style={{paddingBottom: 10, height: '100%'}}>
        <View
          style={{flex: 30}}
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={{flex: 40}}>
            {!this.props.dataEdit ? (
              <TabBar_Title
                title={'Thêm đề nghị mua'}
                callBack={() => this.onPressBack()}
              />
            ) : (
              <TabBar_Title
                title={'Chỉnh sửa đề nghị mua'}
                callBack={() => this.onPressBack()}
              />
            )}
            <ScrollView>
              <View style={styles.containerContent}>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Số đề nghị <RequiredText />
                    </Text>
                  </View>
                  {isEdit && (
                    <TextInput
                      style={AppStyles.FormInput}
                      underlineColorAndroid="transparent"
                      value={ProposedPurchaseId}
                      autoCapitalize="none"
                      onChangeText={ProposedPurchaseId =>
                        this.onChangeText(
                          'ProposedPurchaseId',
                          ProposedPurchaseId,
                        )
                      }
                    />
                  )}
                  {!isEdit && (
                    <View style={[AppStyles.FormInput]}>
                      <Text
                        style={[AppStyles.TextInput, {color: AppColors.gray}]}>
                        {ProposedPurchaseId}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Người đề nghị</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.FullName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Bộ phận</Text>
                  </View>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.DepartmentName}
                    editable={false}
                  />
                </View>
                <View
                  style={{
                    padding: 10,
                    paddingBottom: 0,
                    flexDirection: 'column',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>
                      Ngày đề nghị <RequiredText />
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <DatePicker
                        locale="vie"
                        locale="vie"
                        locale={'vi'}
                        style={{width: '100%'}}
                        date={this.state.RequestDate}
                        mode="date"
                        androidMode="spinner"
                        placeholder="Chọn ngày"
                        format="DD/MM/YYYY"
                        confirmBtnText="Xong"
                        cancelBtnText="Huỷ"
                        disabled={false}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            marginRight: 36,
                          },
                        }}
                        onDateChange={date => this.setDate(date)}
                      />
                    </View>
                  </View>
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>Loại đề nghị</Text>
                  <RNPickerSelect
                    doneText="Xong"
                    doneText="Xong"
                    onValueChange={value =>
                      this.onChangeState('TypeOfRequest', value)
                    }
                    items={ListType}
                    placeholder={{}}
                    value={this.state.TypeOfRequest}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <Text style={AppStyles.Labeldefault}>Nội dung mua</Text>
                  <RNPickerSelect
                    doneText="Xong"
                    doneText="Xong"
                    onValueChange={value => this.onChangeState('Type', value)}
                    items={ListTypeConfig}
                    placeholder={{}}
                    value={this.state.Type}
                  />
                </View>
                <View style={{padding: 10, paddingBottom: 0}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Diễn giải</Text>
                  </View>
                  <TextInput
                    style={AppStyles.FormInput}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    numberOfLines={3}
                    value={this.state.Description}
                    onChangeText={Description =>
                      this.onChangeText('Description', Description)
                    }
                  />
                </View>
                <View style={{marginTop: 5}}>
                  <View style={{width: 150, marginLeft: 10}}>
                    <Button
                      title="Thêm dòng"
                      type="outline"
                      size={15}
                      onPress={() => this.addRows()}
                      buttonStyle={{padding: 5, marginBottom: 5}}
                      titleStyle={{marginLeft: 5}}
                      icon={
                        <IconElement
                          name="pluscircleo"
                          type="antdesign"
                          color={AppColors.blue}
                          size={14}
                        />
                      }
                    />
                  </View>

                  {/* Table */}
                  <ScrollView horizontal={true}>
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        {headerTable.map(item => {
                          if (
                            !this.state.TypeOfRequest ||
                            this.state.TypeOfRequest === 'TM'
                          ) {
                            if (item.hideInCommercialType) {
                              return null;
                            }
                          }
                          return (
                            <TouchableOpacity
                              key={item.title}
                              style={[AppStyles.table_th, {width: item.width}]}>
                              <Text style={AppStyles.Labeldefault}>
                                {item.title}
                              </Text>
                            </TouchableOpacity>
                          );
                        })}
                      </View>
                      <View
                      // style={{
                      //   maxHeight: Dimensions.get('window').height / 7,
                      // }}
                      >
                        {this.state.rows.length > 0 ? (
                          <View>
                            <FlatList
                              data={this.state.rows}
                              renderItem={({item, index}) => {
                                return this.itemFlatList(item, index);
                              }}
                              keyExtractor={(rs, index) => index.toString()}
                            />
                            {this.rowTotal()}
                          </View>
                        ) : (
                          <TouchableOpacity style={[AppStyles.table_foot]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'left'},
                              ]}>
                              Không có dữ liệu!
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </ScrollView>
                </View>
                {/* attachment */}
                {!this.props.dataEdit && (
                  <View
                    style={{
                      flexDirection: 'column',
                      padding: 10,
                      marginTop: 5,
                    }}>
                    <View style={{flexDirection: 'row', marginBottom: 5}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={AppStyles.Labeldefault}>Đính kèm</Text>
                        <Text style={{color: 'red'}}> </Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          flexDirection: 'row',
                        }}
                        onPress={() => this.openAttachment()}>
                        <IconElement
                          name="attachment"
                          type="entypo"
                          size={15}
                          color={AppColors.ColorAdd}
                        />
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {color: AppColors.ColorAdd},
                          ]}>
                          {' '}
                          Chọn file
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <Divider style={{marginBottom: 10}} />
                    {this.state.Attachment.length > 0
                      ? this.state.Attachment.map((para, i) => (
                          <View
                            key={i}
                            style={[{flexDirection: 'row', marginBottom: 3}]}>
                            <View
                              style={[
                                AppStyles.containerCentered,
                                {padding: 10},
                              ]}>
                              <Image
                                style={{
                                  width: 30,
                                  height: 30,
                                  borderRadius: 10,
                                }}
                                source={{
                                  uri: this.FileAttackments.renderImage(
                                    para.FileName,
                                  ),
                                }}
                              />
                            </View>
                            <TouchableOpacity
                              key={i}
                              style={{flex: 1, justifyContent: 'center'}}>
                              <Text style={AppStyles.Textdefault}>
                                {para.FileName}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[
                                AppStyles.containerCentered,
                                {padding: 10},
                              ]}
                              onPress={() => this.removeAttactment(para)}>
                              <IconElement
                                name="close"
                                type="antdesign"
                                size={20}
                                color={AppColors.ColorDelete}
                              />
                            </TouchableOpacity>
                          </View>
                        ))
                      : null}
                  </View>
                )}
              </View>
              {this.state.allItems.length > 0 ? (
                <Combobox
                  TypeSelect={'single'} // single or multiple
                  callback={this.ChangeItem}
                  data={this.state.allItems}
                  nameMenu={'Chọn'}
                  eOpen={this.openCombobox}
                  position={'bottom'}
                  value={undefined}
                />
              ) : null}
              {this.state.allOrders.length > 0 ? (
                <Combobox
                  TypeSelect={'single'} // single or multiple
                  callback={this.ChangeOrder}
                  data={this.state.allOrders}
                  nameMenu={'Chọn'}
                  eOpen={this.openComboboxOrder}
                  position={'bottom'}
                  value={undefined}
                />
              ) : null}
            </ScrollView>
          </View>
        </View>
        {this.state.modalPickerTimeVisible && (
          <DateTimePickerModal
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            locale="vi-VN"
            cancelTextIOS="Huỷ"
            confirmTextIOS="Chọn"
            headerTextIOS="Chọn thời gian"
            isVisible={this.state.modalPickerTimeVisible}
            mode="date"
            onConfirm={this.handleConfirmTime}
            onCancel={this.hideDatePicker}
          />
        )}
        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Lưu"
            onPress={() => this.Submit()}
          />
        </View>
        <OpenPhotoLibrary
          callback={this.callbackLibarary}
          openLibrary={this.openLibrary}
        />
        <ComboboxV2
          callback={this.ChoiceAtt}
          data={listCombobox.ListComboboxAtt}
          eOpen={this.openCombobox_Att}
        />
      </Container>
    );
  }
  rowTotal = () => {
    const {rows} = this.state;
    const {list} = listCombobox;
    return (
      <View style={{flexDirection: 'row'}}>
        {list.map((item, index) => {
          if (!this.state.TypeOfRequest || this.state.TypeOfRequest === 'TM') {
            if (item.hideInCommercialType) {
              return null;
            }
          }
          if (item.id === 'tonTT') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text style={[AppStyles.Textdefault, styles.totalText]}>
                  {this.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.Quantity || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          if (item.id === 'po') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text style={[AppStyles.Textdefault, styles.totalText]}>
                  {this.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.QuantityReamin || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          if (item.id === 'tonDU') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text style={[AppStyles.Textdefault, styles.totalText]}>
                  {this.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.QuantityUsed || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          return (
            <TouchableOpacity
              key={index}
              style={[
                AppStyles.table_td_custom,
                {width: item.width, backgroundColor: '#f5f2f2'},
              ]}
            />
          );
        })}
      </View>
    );
  };
  ChangeItem = rs => {
    const {currentIndexItem, rows} = this.state;
    let {value, text} = rs;
    if (!rs) {
      return;
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.ItemName = text;
        row.ItemId = value;
        row.InventoryNumber = '313';
        row.QuantityReamin = '-997';
      }
      return row;
    });
    this.setState({rows: data, currentIndexItem: null});
  };
  ChangeOrder = rs => {
    const {currentIndexItem, rows} = this.state;
    let {value, text} = rs;
    if (!rs) {
      return;
    }
    let data = rows.map((row, index) => {
      if (currentIndexItem === index) {
        row.OrderName = text;
        row.OrderNumber = value;
      }
      return row;
    });
    this.setState({rows: data, currentIndexItem: null});
  };
  setTitle(Title) {
    this.setState({Title});
  }
  setDate(Date) {
    this.setState({RequestDate: Date});
  }
  setStartTime(StartTime) {
    this.setState({StartTime: StartTime});
  }
  setEndTime(EndTime) {
    this.setState({EndTime: EndTime});
  }
  onPressBack() {
    Actions.pop();
    Actions.refresh({
      moduleId: 'Back',
      Data: {},
      ActionTime: new Date().getTime(),
    });
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  //#region  customFloat
  customFloat = val => {
    if (val !== null) {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        return valview + ',' + string[1].substring(0, 2);
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddProposedPurchase);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 10,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
  totalText: {
    padding: 10,
    paddingLeft: 5,
    textAlign: 'left',
  },
});
