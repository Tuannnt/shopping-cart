import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {Header, Icon, Divider, ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Fonts from '../../../theme/fonts';
import API_ProposePurchases from '../../../network/BM/API_ProposePurchases';
import ErrorHandler from '../../../error/handler';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';

class ItemProposedPurchaseDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
    };
  }

  componentDidMount(): void {
    let id = this.props.rowGuid;
    API_ProposePurchases.ProposedPurchaseDetails_GetItems(id)
      .then(res => {
        this.setState({Data: JSON.parse(res.data.data)});
      })
      .catch(error => {
        console.log(error.data);
      });
  }

  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar_Title
          title={'Chi tiết vật tư'}
          callBack={() => this.onPressBack()}
        />
        <View style={{padding: 20, backgroundColor: '#fff'}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Mã vật tư :</Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.state.Data.ItemId}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
              Tên vật tư :
            </Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.state.Data.ItemName}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>ĐVT :</Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.state.Data.UnitName}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Số SO :</Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.state.Data.OrderNumber}
            </Text>
          </View>
          <View style={{marginTop: 5}}>
            <Text style={AppStyles.Labeldefault}>Thông số kỹ thuật :</Text>
            <Text style={AppStyles.Textdefault}>
              {this.state.Data.Specification}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>
              SL cần mua :
            </Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.addPeriod(this.state.Data.QuantityUsed)}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Đơn giá :</Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.addPeriod(this.state.Data.UnitPrice)}
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Ngày cần :</Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.customDate(this.state.Data.DateNeed)}
            </Text>
          </View>
          <Divider style={{marginTop: 10}} />
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <Text style={[AppStyles.Labeldefault, {flex: 1}]}>Tổng tiền :</Text>
            <Text
              style={[AppStyles.Textdefault, {flex: 2, textAlign: 'right'}]}>
              {this.addPeriod(this.state.Data.Amount)}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  onPressBack() {
    Actions.pop();
  }

  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }

  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ItemProposedPurchaseDetailsComponent);
