import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  RefreshControl,
  Text,
  View,
  Keyboard,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, Icon, ListItem, SearchBar} from 'react-native-elements';
import {API_ProductionOrdersV2} from '../../../network';
import TabBar from '../../component/TabBar';
import {FuncCommon} from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import {AppStyles, AppColors} from '@theme';
import FormSearch from '../../component/FormSearch';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import MenuSearchDate from '../../component/MenuSearchDate';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import Combobox from '../../component/Combobox';
import DatePicker from 'react-native-date-picker';
const SCREEN_WIDTH = Dimensions.get('window').width;
const ListStatus = [
  {value: '1', text: 'Sản phẩm chờ bóc tách'},
  {value: '2', text: 'Sản phẩm chờ lập kế hoạch'},
  {value: '3', text: 'Sản phẩm đã lập KH và đang sản xuất'},
  {value: '4', text: 'Sản phẩm hoàn thành'},
  {value: '5', text: 'Sản phẩm hủy sx và GCN'},
];
class ProductionOrdersComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingsearch: true,
      loading: true,
      ValueSearchDate: '4', // 30 ngày trước
      ListType: [],
      setEventEndDate: false,
      setEventStartDate: false,
      ListAll: [], //biến chứa danh sách
      EvenFromSearch: false, // on/off form search
    };
    this.DataSearch = {
      EndDate: new Date(),
      StartDate: new Date(),
      CurrentPage: 0,
      Length: 50,
      search: {
        value: '',
      },
      CurrentStatus: '',
      OrderStatus: '1',
      OrderTypeID: '',
      PICId: '',
      TypeDayId: '1',
      QueryOrderBy: '',
    };

    //#region tabbarbottom
    this.listtabbarBotom = [
      {
        Title: 'Tất cả',
        Icon: 'server',
        Type: 'feather',
        Value: [1, 2, 3],
        Checkbox: true,
        Badge: 0,
      },
      // {
      //   Title: "Chờ Lập KH",
      //   Icon: "rotate-cw",
      //   Type: "feather",
      //   Value: "2",
      //   Checkbox: false,
      //   Badge: 0
      // },
      {
        Title: 'Hoàn thành',
        Icon: 'check-circle',
        Type: 'feather',
        Value: [2],
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Đang thực hiện',
        Icon: 'flag',
        Type: 'feather',
        Value: [1],
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Huỷ',
        Icon: 'appstore-o',
        Type: 'antdesign',
        Value: [3],
        Checkbox: false,
        Badge: 0,
      },
    ];
    this.DataMenuBottom = [
      {
        key: [3],
        name: 'Huỷ',
        icon: {
          name: 'eye-outline',
          type: 'material-community',
          color: AppColors.Maincolor,
        },
      },
    ];
    //#endregion
  }
  componentDidMount = () => {
    this.GetOrderTypes();
  };
  GetOrderTypes = () => {
    API_ProductionOrdersV2.GetOrderTypes()
      .then(res => {
        console.log(res);
        if (res.data.errorCode === 200) {
          let _data = JSON.parse(res.data.data);
          _data.unshift({value: '', text: 'Tất cả'});
          this.setState({ListType: _data});
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  //#region Hàm tìm kiếm
  Search = txt => {
    this.DataSearch.search.value = txt;
    this.nextPage(true);
  };
  //#endregion

  //#region Refresh lại danh sách
  _onRefresh = () => {
    this.setState({loading: true}, () => {
      this.nextPage(true);
    });
  };
  //#endregion

  //#region API Danh sách
  nextPage = s => {
    let List = this.state.ListAll || [];
    if (s === true) {
      this.DataSearch.CurrentPage = 1;
      List = [];
    } else {
      this.DataSearch.CurrentPage++;
    }
    let _obj = {
      ...this.DataSearch,
      StartDate: FuncCommon.ConDate(this.DataSearch.StartDate, 15),
      EndDate: FuncCommon.ConDate(this.DataSearch.EndDate, 15),
    };
    API_ProductionOrdersV2.JTable(_obj)
      .then(res => {
        let _data = JSON.parse(res.data.data).data;
        if (this.DataSearch.CurrentPage === 1) {
          List = _data;
        } else {
          List = List.concat(_data);
        }
        this.setState({
          ListAll: List,
          loading: false,
          loadingsearch: false,
        });
      })
      .catch(error => {
        this.setState({loading: false, loadingsearch: false});
        console.log(error);
      });
  };
  //#endregion

  //#region View tổng
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar
            title={'Yêu cầu sản xuất'}
            BackModuleByCode={'PRO'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
          />
          {this.state.EvenFromSearch === true ? (
            <View style={{flexDirection: 'column'}}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventStartDate: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.DataSearch.StartDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={AppStyles.Labeldefault}>Thời gian kết thúc</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventEndDate: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.DataSearch.EndDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column', padding: 5}}>
                  <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity
                style={[
                  AppStyles.FormInput,
                  {marginHorizontal: 10, marginTop: 5},
                ]}
                onPress={() => this.onActionComboboxStatus()}>
                <Text
                  style={[
                    AppStyles.TextInput,
                    this.DataSearch.OrderStatus
                      ? {color: 'black'}
                      : {color: AppColors.gray},
                  ]}>
                  {this.DataSearch.OrderStatus
                    ? this.DataSearch.OrderStatusName
                    : 'Chọn trạng thái...'}
                </Text>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  <Icon
                    color={AppColors.gray}
                    name={'chevron-thin-down'}
                    type="entypo"
                    size={20}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  AppStyles.FormInput,
                  {marginHorizontal: 10, marginTop: 5},
                ]}
                onPress={() => this.onActionComboboxType()}>
                <Text
                  style={[
                    AppStyles.TextInput,
                    this.DataSearch.OrderTypeName
                      ? {color: 'black'}
                      : {color: AppColors.gray},
                  ]}>
                  {this.DataSearch.OrderTypeName
                    ? this.DataSearch.OrderTypeName
                    : 'Chọn loại đơn hàng...'}
                </Text>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  <Icon
                    color={AppColors.gray}
                    name={'chevron-thin-down'}
                    type="entypo"
                    size={20}
                  />
                </View>
              </TouchableOpacity>
              <FormSearch CallbackSearch={callback => this.Search(callback)} />
            </View>
          ) : null}
          {this.state.ListAll.length === 0 ? (
            <View style={{flex: 1, alignItems: 'center'}}>
              <Text>Không có dữ liệu</Text>
            </View>
          ) : (
            this.ViewList(this.state.ListAll)
          )}
          {/* <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.CallbackValueBottom(callback)
              }
            />
          </View> */}

          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(true),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.DataSearch.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(true), this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.DataSearch.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {this.state.ListType.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeType}
              data={this.state.ListType}
              nameMenu={'Chọn loại'}
              eOpen={this.openComboboxType}
              position={'bottom'}
              value={this.DataSearch.OrderTypeID}
            />
          ) : null}
          {ListStatus.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeStatus}
              data={ListStatus}
              nameMenu={'Chọn loại'}
              eOpen={this.openComboboxStatus}
              position={'bottom'}
              value={this.DataSearch.OrderStatus}
            />
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }

  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.DataSearch.OrderTypeID = rs.value;
    this.DataSearch.OrderTypeName = rs.text;
    this.nextPage(true);
  };
  _openComboboxStatus() {}
  openComboboxStatus = d => {
    this._openComboboxStatus = d;
  };
  onActionComboboxStatus() {
    this._openComboboxStatus();
  }
  ChangeStatus = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.DataSearch.OrderStatus = rs.value;
    this.DataSearch.OrderStatusName = rs.text;
    this.nextPage(true);
  };
  //#endregion
  handleStatus = OrderStatus => {
    if (OrderStatus === 0) {
      return 'Chưa thực hiện';
    } else if (OrderStatus === 1) {
      return 'Đang thực hiện';
    } else if (OrderStatus === 2) {
      return 'Hoàn thành';
    } else if (OrderStatus === 3) {
      return 'Đã hủy bỏ';
    } else if (OrderStatus === 4) {
      return 'Tất cả';
    } else {
      return '';
    }
  };
  //#region View danh sách
  ViewList = item => {
    return (
      <FlatList
        data={item}
        renderItem={({item, index}) => (
          <ListItem
            key={index}
            leftAvatar={
              <Image
                style={{width: 50, height: 50}}
                source={
                  item.UrlImage
                    ? {uri: item.UrlImage}
                    : require('./no_image.png')
                }
              />
            }
            title={
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Text style={{fontWeight: 'bold'}}>{item.OrderNumber}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={{
                      color: item.Priority === 'CAO' ? 'red' : 'black',
                    }}>
                    {FuncCommon.ConDate(item.ConfirmDate, 0)}
                  </Text>
                </View>
              </View>
            }
            subtitle={() => {
              return (
                <View
                  style={{
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1.5}}>
                      <Text>Mã SP: {item.ItemId}</Text>
                      <Text>Tên SP: {item.ItemName}</Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                      }}>
                      <Text>Số lượng: </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          textAlign: 'right',
                        }}>
                        {FuncCommon.addPeriod(item.Quantity)}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
            bottomDivider
            onPress={() => this.ActionOpen(item)}
          />
        )}
        onEndReached={() =>
          item.length >= this.DataSearch.Length * this.DataSearch.CurrentPage
            ? this.nextPage(false)
            : null
        }
        keyExtractor={(rs, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    );
  };
  //#endregion

  //#region Action open
  ActionOpen = item => {
    Actions.ProductionOrders_Open({
      Data: item,
    });
  };
  //#endregion

  //#region thay đổi thời gian tìm kiếm
  setStartDate = date => {
    this.DataSearch.StartDate = date;
    this.nextPage(true);
  };
  setEndDate = date => {
    this.DataSearch.EndDate = date;
    this.nextPage(true);
  };
  //#endregion

  //#region chức năng lọc này cố định
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.DataSearch.StartDate = new Date(callback.start);
      this.DataSearch.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.nextPage(true);
    }
  };
  //#endregion

  //#region CallbackValueBottom
  CallbackValueBottom = callback => {
    if (callback !== '' && callback !== null) {
      this.DataSearch.OrderStatus = callback;
      this.nextPage(true);
    }
  };
  //#endregion

  //#region MenuAction
  _openMenu() {}
  openMenu = d => {
    this._openMenu = d;
  };
  onActionMenuBottom() {
    this._openMenu();
  }
  actionMenuCallback = d => {
    this.CallbackValueBottom(d);
  };
  //#endregion

  ConvertDateSearch = type => {
    var newdate = new Date();
    var month = newdate.getMonth() + 1;
    var monthBefore = newdate.getMonth();
    var year = newdate.getFullYear();
    var day = newdate.getDate();
    if (month < 10) month = '0' + month;
    if (monthBefore < 10) monthBefore = '0' + monthBefore;
    if (day < 10) day = '0' + day;
    if (type === 1) {
      return year + '-' + monthBefore + '-' + day;
    } else {
      return year + '-' + month + '-' + day;
    }
  };
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ProductionOrdersComponent);
