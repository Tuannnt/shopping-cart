import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {AppStyles, AppColors} from '@theme';
import {connect} from 'react-redux';
import {API_ProductionPlan} from '../../../network';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../../utils';
import {ScrollView} from 'react-native-gesture-handler';
import configApp from '../../../configApp';

const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class ProductionPlan_Open_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      Data: props.Data !== undefined ? props.Data : null,
      //hiển thị quy trình công nghệ
      ViewBOMWorkpieces: false,
      transIcon_ViewBOMWorkpieces: new Animated.Value(0),
      DataBOMWorkpieces: [], // Quy trình công nghệ
      DataBOMWorkpieces: [], //định mực phôi
    };
  }
  componentDidMount() {
    this.Skill();
    // this.setEventBOMWorkpieces();
  }
  //#region Skill
  Skill = () => {
    //#region đóng mở tab quy trình công nghệ
    this.Animated_on_BOMWorkpieces = Animated.timing(
      this.state.transIcon_ViewBOMWorkpieces,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_BOMWorkpieces = Animated.timing(
      this.state.transIcon_ViewBOMWorkpieces,
      {toValue: 0, duration: 333},
    );
    //#endregion
  };
  //#endregion

  render() {
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          {this.state.Data !== null ? (
            <TabBar_Title
              title={'Chi tiết kế hoạch bộ phận'}
              callBack={() => this.callBackList()}
            />
          ) : null}
          <Divider />
          <View style={{flex: 1}}>
            {this.state.Data !== null
              ? this.CustomFormDetailV2(this.state.Data)
              : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  CustomFormDetailV2 = item => {
    return (
      <ScrollView style={{flexDirection: 'column'}}>
        <View style={{flexDirection: 'row', padding: 5}}>
          <View
            style={{
              borderWidth: 0.5,
              borderColor: AppColors.gray,
              marginRight: 5,
            }}>
            <Image
              style={{width: 100, height: 100}}
              source={
                item.UrlImage ? {uri: item.UrlImage} : require('./no_image.png')
              }
            />
          </View>
          <View style={{flex: 1}}>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Số ĐH</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>: {item.SaleOrderNo}</Text>
              </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Mã SP (NB)</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>: {item.ItemId}</Text>
              </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Mã bản vẽ</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  : {item.ItemIdbyProvider}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <Divider />
        <View style={{padding: 5}}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Tên SP:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>{item.ItemName}</Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Loại SP:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>{item.TypeId}</Text>
              </View>
            </View>
          </View>

          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Đặc điểm:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>{item.Specification}</Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>ĐVT:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>{item.UnitName}</Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>S/L:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.addPeriod(item.Quantity)}
                </Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày cần:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.ConDate(item.RequireDate, 0)}
                </Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày XN:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.ConDate(item.ConfirmDateDeliveryDates, 0)}
                </Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ưu tiên:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {' '}
                  {item.Priority === 'THAP'
                    ? 'Thấp'
                    : item.Priority === 'TB'
                    ? 'Trung bình'
                    : item.Priority === 'CAO'
                    ? 'Cao'
                    : ''}
                </Text>
              </View>
            </View>
          </View>
          {/* <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 2, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Mã phôi</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>: {item.MaPhoi}</Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
              }}>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Labeldefault}>Phôi</Text>
              </View>
              <View style={{flex: 2, flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>:</Text>
                <View style={{flex: 1, marginLeft: 5}}>
                  <Text style={AppStyles.Textdefault}>
                    {this.customFloat(item.InventoryQuantity)}
                  </Text>
                </View>
              </View>
            </View>
          </View> */}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>K.hàng:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>{item.ObjectID}</Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>KD phụ trách:</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>{item.EmployeeName}</Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Ngày YC :</Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.ConDate(item.ConfirmDate, 0)}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  };

  // Tab danh sách nguyên vật liệu

  //Các hàm dùng chung
  //#region đóng mở các tab
  setViewOpen = val => {
    this.setState({
      ViewBOMWorkpieces: false,
    });
    if (val === 'ViewBOMWorkpieces') {
      this.setState({ViewBOMWorkpieces: !this.state.ViewBOMWorkpieces});
      if (this.state.ViewBOMWorkpieces === false) {
        Animated.sequence([this.Animated_on_BOMWorkpieces]).start();
      } else {
        Animated.sequence([this.Animated_off_BOMWorkpieces]).start();
      }
    }
  };
  //#endregion
  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null && val !== '') {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        var _string2 = string[1].substring(0, 2);
        if (_string2 !== '00') {
          return valview + ',' + string[1].substring(0, 2) === '00';
        } else {
          return valview;
        }
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ProductionPlan_Open_Component);
