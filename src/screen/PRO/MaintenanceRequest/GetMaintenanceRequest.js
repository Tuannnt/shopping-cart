import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  StyleSheet,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  ToastAndroid,
  Alert,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {
  DatePicker,
  Label,
  Item,
  Input,
  Container,
  Content,
  Picker,
  Button,
} from 'native-base';
import ComboboxV2 from '../../component/ComboboxV2';
import PopupHandleVote from '../../component/PopupHandleVote';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_HR, API} from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, ListItem, Header, Icon} from 'react-native-elements';
import Fonts from '../../../theme/fonts';
import {Actions} from 'react-native-router-flux';
import TabBarBottom from '../../component/TabBarBottom';
import Combobox from '../../component/Combobox';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import Toast from 'react-native-simple-toast';

const styles = StyleSheet.create({
  styletextLable: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  styletext: {
    fontSize: 14,
    padding: 5,
  },
  textLableCol2: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  textCol2: {
    fontSize: 14,
    padding: 5,
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
  },
  containerContent: {
    flex: 10,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
});
const options = [
  {
    component: <Text style={{color: 'crimson', fontSize: 20}}>Đóng</Text>,
  },
  {
    component: <Text style={{color: 'blue', fontSize: 20}}>Duyệt phiếu</Text>,
    height: 80,
  },
  {
    component: <Text style={{color: 'blue', fontSize: 20}}>Trả lại</Text>,
    height: 80,
  },
];

class GetMaintenanceRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      editItem: null,
      list: [],
      position: null,
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      selected2: [],
      ListUnits: [],
      lstDeleteFile: [],
      DeleteDetail: [],
      lstTitlefile: [],
      viewId: '',
      Evenpopup: false,
    };
    this.ListComboboxAtt = [
      {
        value: 'NG',
        text: 'NG',
      },
      {
        value: 'OK',
        text: 'OK',
      },
    ];
    this.listtabbarBotom = [
      {
        Title: 'Trang chủ',
        Icon: 'edit',
        Type: 'antdesign',
        Value: 'edit',
        OffEditcolor: true,
        Checkbox: false,
      },
      {
        Title: 'Danh sách',
        Icon: 'Safety',
        Type: 'antdesign',
        Value: 'success',
        OffEditcolor: true,
        Checkbox: false,
      },
      {
        Title: 'Cập nhật',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'approve',
        OffEditcolor: true,
        Checkbox: false,
      },
      {
        Title: 'Ý kiến',
        Icon: 'message1',
        Type: 'antdesign',
        Value: 'comment',
        OffEditcolor: true,
        Checkbox: false,
      },
    ];
    this.btns = [
      {
        name: 'Duyệt',
        value: 'D',
        color: '#3366CC',
      },
      {
        name: 'Hủy',
        value: 'H',
        color: 'red',
      },
    ];
  }
  setPickUpLocation(PickUpLocation) {
    this.state.Data.PickUpLocation = PickUpLocation;
    this.setState({Data: this.state.Data});
  }

  componentDidMount(): void {
    if (this.props.RecordGuid !== undefined) {
      this.state.viewId = this.props.RecordGuid;
    } else {
      this.state.viewId = this.props.viewId;
    }
    this.GetDetail();
    this.getItem();
    this.GetUnitMeasures();
  }
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'back', ActionTime: new Date().getTime()});
  }
  onPressBack() {
    Actions.MaintenanceRequest();
  }
  onAttachment() {
    Actions.attachmentComponent({
      ModuleId: '5',
      RecordGuid: this.state.viewId,
    });
  }
  GetUnitMeasures() {
    API_HR.GetUnitMeasures()
      .then(rs => {
        var _lstUnits = JSON.parse(rs.data.data);
        var _reListUnits = [];
        for (var i = 0; i < _lstUnits.length; i++) {
          _reListUnits.push({
            text: _lstUnits[i].UnitName,
            value: _lstUnits[i].UnitId,
          });
        }
        this.setState({
          ListUnits: _reListUnits,
        });
      })
      .catch(error => {
        console.log(error.data.data);
      });
  }
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_HR.deleteTrainingResults(id.Id)
      .then(response => {
        console.log(
          '==============ketquaXoa-errorCode: ' + response.data.Error,
        );
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            TrainingResultGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.onPressBack();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  GetDetail = () => {
    let id = this.state.viewId;
    var obj = {
      MaintenanceRequestId: this.state.viewId,
    };
    API_HR.GetItemListDetail(obj)
      .then(res => {
        this.setState({list: JSON.parse(res.data.data)});
        debugger;
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  SubmitMB = index => {
    var model = {
      RowGuid: this.state.list[index].RowGuid,
      MaintenanceContentId: this.state.list[index].MaintenanceContentId,
      Times: this.state.list[index].Times,
      Note: this.state.list[index].Note,
      Results: this.state.list[index].Results,
      MaterialCode: this.state.list[index].MaterialCode,
      Quantity: this.state.list[index].Quantity,
      UnitPrice: this.state.list[index].UnitPrice,
      UnitMeasureId: this.state.list[index].UnitMeasureId,
      TotalMoney: this.state.list[index].TotalMoney,
    };
    let _data = new FormData();

    _data.append('insert', JSON.stringify(model));
    API_HR.UpdateMB(_data)
      .then(res => {
        //Actions.GetMaintenanceRequest();
      })
      .catch(error => {
        console.log(error);
      });
  };
  cleanForm = index => {
    this.SubmitMB(index);
  };
  OnChangeTab(data) {
    if (data == '1') {
      this.setState({
        tabCT: false,
        tabTTC: true,
      });
    } else if (data == '2') {
      this.setState({
        tabTTC: false,
        tabCT: true,
      });
    }
  }
  getItem() {
    let id = this.state.viewId;
    var obj = {
      RowGuid: this.state.viewId,
    };
    API_HR.MaintenanceRequest_Items(obj)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
        });
        API.getProfile().then(rs => {
          console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
          this.setState({LoginName: rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
      });
  }
  ProblemResultApply() {
    let _obj = {
      MaintenanceRequestId: this.state.viewId,
      MaintainTypeId: 1,
      IsConfirm: 1,
      IsCompleted: 1,
      Body: this.state.Quantity,
    };
    let _data = new FormData();
    _data.append('insert', JSON.stringify(_obj));
    if (_obj.Body === null || _obj.Body === undefined || _obj.Body === '') {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập ý kiến',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    API_HR.ProblemResultApply(_data)
      .then(rs => {
        var _rs = rs.data;
        if ((_rs.data = 'ok')) {
          console.log(_rs);
          alert('Duyệt thành công');
        } else {
          alert('Duyệt không thành công');
          //Actions.Registereats ();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }
  ProblemResultApplyofSX() {
    let _obj = {
      MaintenanceRequestId: this.state.viewId,
      MaintainTypeId: 1,
      IsConfirm: 1,
      IsCompleted: 1,
      Body: this.state.Quantity,
    };
    let _data = new FormData();
    _data.append('insert', JSON.stringify(_obj));
    if (_obj.Body === null || _obj.Body === undefined || _obj.Body === '') {
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập ý kiến',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return;
    }
    API_HR.ProblemResultApplyofSX(_data)
      .then(rs => {
        var _rs = rs.data;
        if ((_rs.data = 'ok')) {
          console.log(_rs);
          alert('Duyệt thành công');
        } else {
          alert('Duyệt không thành công');
          //Actions.Registereats ();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }
  showActionSheet = () => this.actionSheet.show();
  getActionSheetRef = ref => (this.actionSheet = ref);
  CustomView = item => (
    <ScrollView style={{flex: 1, backgroundColor: '#fff'}}>
      {/*hiển thị nội dung chính*/}
      <View style={{padding: 15}}>
        {/*Người tạo phiếu*/}
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>Mã phiếu :</Text>
          </View>
          <View style={{flex: 2}}>
            <Text style={[AppStyles.Textdefault]}>
              {item.MaintenanceRequestId}
            </Text>
          </View>
        </View>
        {/*Phòng ban*/}
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>Tên thiết bị :</Text>
          </View>
          <View style={{flex: 2}}>
            <Text style={[AppStyles.Textdefault]}>{item.DeviceName}</Text>
          </View>
        </View>
        {/*Tiêu đề*/}
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>Năm :</Text>
          </View>
          <View style={{flex: 2}}>
            <Text style={[AppStyles.Textdefault]}>{item.Year}</Text>
          </View>
        </View>
        {/*Chi phí đào tạo*/}
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>Ngày tiếp nhận :</Text>
          </View>
          <View style={{flex: 2}}>
            <Text style={[AppStyles.Textdefault]}>
              {item.ReceptionDateString}
            </Text>
          </View>
        </View>
        {/*Địa diểm đào tạo*/}
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>Ngày dự kiến :</Text>
          </View>
          <View style={{flex: 2}}>
            <Text style={[AppStyles.Textdefault]}>{item.StartDateString}</Text>
          </View>
        </View>

        {/*Ngày tạo phiếu*/}
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>Ngày tạo :</Text>
          </View>
          <View style={{flex: 2}}>
            <Text style={[AppStyles.Textdefault]}>
              {this.customDate(item.CreatedDate)}
            </Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>BP sản xuất :</Text>
          </View>
          <View style={{flex: 2}}>
            <Text style={[AppStyles.Textdefault]}>
              {item.ApproveStatus === 1 ? 'Đã duyệt' : 'Chưa duyệt'}
            </Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>BP bảo dưỡng :</Text>
          </View>
          <View style={{flex: 2}}>
            <Text style={[AppStyles.Textdefault]}>
              {item.Approve === 1 ? 'Đã duyệt' : 'Chưa duyệt'}
            </Text>
          </View>
        </View>
        {/*nội dung phiếu*/}
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', padding: 5}}>
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Textdefault]}>{item.Note}</Text>
          </View>
        </View>
      </View>
      <View style={{flex: 4}}>
        <Divider style={{marginTop: 3}} />
        <View style={{flexDirection: 'row', paddingRight: 10}}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              marginLeft: 10,
              flex: 1,
            }}>
            Chi tiết
          </Text>
        </View>
        <View style={{flex: 1, padding: 5}}>
          {this.state.list.length > 0 ? (
            <FlatList
              style={{marginBottom: 80, height: '100%'}}
              refreshing={this.state.isFetching}
              ref={ref => {
                this.ListView_Ref = ref;
              }}
              ListEmptyComponent={this.ListEmpty}
              //ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={item => item.BankGuid}
              data={this.state.list}
              renderItem={({item, index}) => (
                <TouchableOpacity style={{flexDirection: 'column', padding: 5}}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <Text style={[AppStyles.Textdefault]}>Số lượng: </Text>
                      <TextInput
                        underlineColorAndroid="transparent"
                        keyboardType="numeric"
                        autoCapitalize="none"
                        multiline={true}
                        value={
                          item.Quantity != null && item.Quantity != ''
                            ? item.Quantity.toString()
                            : '0'
                        }
                        onEndEditing={() => this.cleanForm(index)}
                        onChangeText={value => this.setQuantity(value, index)}
                      />
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <Text style={[AppStyles.Textdefault]}>Thời gian: </Text>
                      <TextInput
                        underlineColorAndroid="transparent"
                        keyboardType="numeric"
                        autoCapitalize="none"
                        multiline={true}
                        onEndEditing={() => this.cleanForm(index)}
                        value={item.Times != null ? item.Times.toString() : 0}
                        onChangeText={value => this.setTime(value, index)}
                      />
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <Text style={[AppStyles.Textdefault]}>Đơn giá: </Text>
                      <TextInput
                        underlineColorAndroid="transparent"
                        keyboardType="numeric"
                        autoCapitalize="none"
                        multiline={true}
                        value={
                          item.UnitPrice != null && item.UnitPrice != ''
                            ? item.UnitPrice.toString()
                            : '0'
                        }
                        onEndEditing={() => this.cleanForm(index)}
                        onChangeText={value => this.setUnitPrice(value, index)}
                      />
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity onPress={() => this.openResult(index)}>
                        <Text style={[AppStyles.Textdefault]}>Kết quả: </Text>
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => this.openResult(index)}>
                        <Text style={[AppStyles.Textdefault]}>
                          {item.Results}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() => this.onActionComboboxUnits(index)}
                        style={[AppStyles.Textdefault, {flex: 1}]}>
                        <Text>Đơn vị tính: {item.UnitMeasureId}</Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <Text style={[AppStyles.Textdefault]}>Thành tiền: </Text>
                      <TextInput
                        underlineColorAndroid="transparent"
                        keyboardType="numeric"
                        autoCapitalize="none"
                        multiline={true}
                        value={
                          item.TotalMoney != null
                            ? item.TotalMoney.toString()
                            : 0
                        }
                        onChangeText={value => this.setQuantity(value, index)}
                      />
                    </View>
                  </View>
                  <Divider style={{marginTop: 5}} />
                </TouchableOpacity>
              )}
              onEndReachedThreshold={0.5}
            />
          ) : null}
        </View>
      </View>
    </ScrollView>
  );
  //Begin cretaed popup
  renderPopup(title, btns) {
    return (
      <PopupHandleVote
        onSubmit={(callback, object) => this.callbackSubmit(callback, object)}
        onClick={callback => this.clickPopup(callback)}
        title={title}
        desciption={'Nhập nội dung'}
        btns={btns}
      />
    );
  }
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            <TabBar_Title
              title={'Chi tiết phiếu'}
              callBack={() => this.callBackList()}
            />
            <Divider />
            {this.state.Data ? this.CustomView(this.state.Data) : null}
            {this.state.Evenpopup === true
              ? this.renderPopup('Duyệt phiếu ', this.btns)
              : null}

            {/* {this.state.DataView !== null
              ? <TabBarBottomCustom
                  ListData={this.listtabbarBotom}
                  onCallbackValueBottom={callback =>
                    this.CallbackValueBottom (callback)}
                />
              : null} */}
            <ComboboxV2
              callback={this.ChangeResult}
              data={this.ListComboboxAtt}
              eOpen={this.openCombobox_Result}
            />
            {this.state.ListUnits.length > 0 ? (
              <Combobox
                TypeSelect={'single'} // single or multiple
                //value={this.state.UnitBefore}
                callback={this.ChangeUnit}
                data={this.state.ListUnits}
                nameMenu={'Chọn đơn vị tính'}
                eOpen={this.openComboboxUnits}
                position={'bottom'}
              />
            ) : null}
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
  setQuantity(value, index) {
    this.state.list[index].Quantity =
      value !== '' && value !== null ? parseFloat(value) : 0;
    this.state.list[index].TotalMoney =
      this.state.list[index].Quantity * this.state.list[index].UnitPrice;
    this.setState({
      list: this.state.list,
    });
  }
  setTotalMoney(value, index) {
    this.state.list[index].TotalMoney =
      value !== '' && value !== null ? parseFloat(value) : 0;
    this.setState({
      list: this.state.list,
    });
  }
  setTime(value, index) {
    this.state.list[index].Times =
      value !== '' && value !== null ? parseFloat(value) : 0;
    this.setState({
      list: this.state.list,
    });
  }

  setUnitPrice(value, index) {
    this.state.list[index].UnitPrice =
      value !== '' && value !== null ? parseFloat(value) : 0;
    this.state.list[index].TotalMoney =
      this.state.list[index].Quantity * this.state.list[index].UnitPrice;
    this.setState({
      list: this.state.list,
    });
  }
  callbackPopup() {}
  clickPopup(callback) {
    this.callbackPopup = callback;
  }
  // unit
  ChangeUnit = value => {
    if (value.value !== null) {
      var obj = this.state.list[this.state.position];
      var rs = this.state.ListUnits.find(x => x.value === value.value);
      if (rs !== null && rs !== undefined) {
        obj.UnitMeasureId = value.value;
        obj.UnitName = rs.text;
        this.setState({
          list: this.state.list,
        });
      }
      this.SubmitMB(this.state.position);
    }
  };
  _openComboboxUnits() {}
  openComboboxUnits = d => {
    this._openComboboxUnits = d;
  };
  onActionComboboxUnits(index) {
    this.setState({
      position: index,
    });
    this._openComboboxUnits();
  }
  //resutl
  openCombobox_Result() {}
  openCombobox_Result = d => {
    this._openCombobox_Result = d;
  };
  openResult(index) {
    this.setState({
      position: index,
    });
    this._openCombobox_Result();
  }
  ChangeResult = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'NG':
          if (rs.value !== null) {
            var obj = this.state.list[this.state.position];
            var rs = this.ListComboboxAtt.find(x => x.value === rs.value);
            if (rs !== null && rs !== undefined) {
              obj.Results = rs.value;
              this.setState({
                list: this.state.list,
              });
            }

            this.SubmitMB(this.state.position);
          }
          break;
        case 'OK':
          if (rs.value !== null) {
            var obj = this.state.list[this.state.position];
            var rs = this.ListComboboxAtt.find(x => x.value === rs.value);
            if (rs !== null && rs !== undefined) {
              obj.Results = rs.value;
              this.setState({
                list: this.state.list,
              });
            }
            this.SubmitMB(this.state.position);
          }
          break;
        default:
          break;
      }
    }
  };
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  CallbackValueBottom = value => {
    debugger;
    switch (value) {
      case 'approve':
        this.setState({Evenpopup: true});
        break;
      case 'attachment':
        break;
      case 'delete':
        break;
      case 'success':
        break;
      case 'comment':
        break;
      default:
        break;
    }
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  //định dạng ngày và giờ
  customDatetime(strDateTime) {
    if (strDateTime != null) {
      var strSplitDateTime = String(strDateTime).split(' ');
      var datetime = new Date(strSplitDateTime[0]);
      // alert(date);
      var HH = datetime.getHours();
      var MM = datetime.getMinutes();
      var dd = datetime.getDate();
      var mm = datetime.getMonth() + 1; //January is 0!
      var yyyy = datetime.getFullYear();
      if (HH < 10) {
        HH = '0' + HH;
      }
      if (MM < 10) {
        MM = '0' + MM;
      }
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      datetime = dd + '-' + mm + '-' + yyyy + ' ' + HH + ':' + MM;
      return datetime.toString();
    } else {
      return '';
    }
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetMaintenanceRequest);
