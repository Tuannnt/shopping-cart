import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  RefreshControl,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Divider, Icon, ListItem} from 'react-native-elements';
import {API_TM_TASKS, API_TM_CALENDAR, API, API_HR} from '@network';
import TabBar from '../../component/TabBar';
import {AppStyles, AppColors} from '@theme';
import FormSearch from '../../component/FormSearch';
import SearchDay from '../../component/SearchDay';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import * as Progress from 'react-native-progress';
import MenuActionCompoment from '../../component/MenuActionCompoment';
import {FuncCommon} from '../../../utils';
import {WebView} from 'react-native-webview';
import LoadingComponent from '../../component/LoadingComponent';
import {Container} from 'native-base';
import Combobox from '../../component/Combobox';
import sizes from '../../../theme/sizes';
import fonts from '../../../theme/fonts';
import {bool} from 'prop-types';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const DataMenuBottom = [
  {
    key: 'details',
    name: 'Xem',
    icon: {
      name: 'eye-outline',
      type: 'material-community',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'attachment',
    name: 'Đính kèm',
    icon: {
      name: 'attachment',
      type: 'entypo',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'comments',
    name: 'Ý kiến',
    icon: {
      name: 'message1',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
];
const DataMenuBottomKanban = [
  {
    key: 'waiting',
    name: 'Chờ giao',
    icon: {
      name: 'user',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'notstart',
    name: 'Chờ xử lý',
    icon: {
      name: 'layers',
      type: 'feather',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'process',
    name: 'Đang thực hiện',
    icon: {
      name: 'pushpino',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'complete',
    name: 'Hoàn thành',
    icon: {
      name: 'Safety',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'waitsomeone',
    name: 'Chờ người khác',
    icon: {
      name: 'user-check',
      type: 'feather',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'expiry',
    name: 'Quá hạn',
    icon: {
      name: 'warning',
      type: 'entypo',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'deferred',
    name: 'Hoãn lại',
    icon: {
      name: 'flag',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
];
const MenuBottom = [
  {
    key: 'Kanban',
    name: 'Kanban',
    icon: {
      name: 'eye-outline',
      type: 'material-community',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'X',
    name: 'Chờ giao',
    icon: {
      name: 'edit',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'F',
    name: 'Theo dõi',
    icon: {
      name: 'attachment',
      type: 'entypo',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'DG',
    name: 'Đã giao',
    icon: {
      name: 'message1',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'CVL',
    name: 'Công việc lưu',
    icon: {
      name: 'calendar-check-o',
      type: 'font-awesome',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'W',
    name: 'Chờ người khác',
    icon: {
      name: 'delete',
      type: 'antdesign',
      color: AppColors.Maincolor,
    },
  },
  {
    key: 'D',
    name: 'Hoãn lại',
    icon: {
      name: 'check-circle',
      type: 'feather',
      color: AppColors.Maincolor,
    },
  },
];
class MaintenanceRequestComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingSearch: false,
      offload: false,
      loading: false,
      // In4 loginName
      Login_EmployeeGuid: null,
      Login_LoginName: '',
      Login_OrganizationGuid: null,
      //
      EvenCalendar: false,
      EvenKanban: false,
      EvenFromSearch: false,
      NameMenuCalendar: '',
      refreshing: false,
      ChoiceDate: '',
      ListTask: [],
      ListTaskKHN: [],
      OffSearchDay: false,
      selectItem: null,
      listtabbarBotomView: [],
      ListTask_ParentId: [],
      //calendar
      ListCalendar: [],
      selectItemCalendar: null,
      //kanban
      KanbanView: [],
      Count_Tab: 0,
      SearchKanban: false,
      ClickStatus: false,
      Month: null,
      MonthName: '',
      Year: null,
      YearName: '',
      DropdownMonth: [],
      DropdownYear: [],
    };
    this._search = {
      StartDate: null,
      EndDate: null,
      Number: 0,
      Title: '',
      Length: 30,
      Approve: null,
      ApproveStatus: null,
      Query: 'Date DESC',
      Month: null,
      Year: null,
      search: {value: ''},
      CurrentPage: 1,
    };
    this.listtabbarBotom = [
      {
        Title: 'KH Năm',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'E',
        Checkbox: false,
      },
      {
        Title: 'Chờ BD',
        Icon: 'layers',
        Type: 'feather',
        Value: 'waiting',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Chờ SX',
        Icon: 'pushpino',
        Type: 'antdesign',
        Value: 'waitingSX',
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'Safety',
        Type: 'antdesign',
        Value: 'OK',
        Checkbox: false,
        Badge: 0,
      },
    ];
  }
  componentDidMount() {
    let lstMontht = [];
    let lstYear = [];
    for (var i = 1; i <= 12; i++) {
      lstMontht.push({value: i, text: i.toString()});
    }
    for (var i = 2019; i <= 2050; i++) {
      lstYear.push({value: i, text: i.toString()});
    }

    this.setState({
      loading: true,
      DropdownMonth: lstMontht,
      DropdownYear: lstYear,
    });

    API.getProfile()
      .then(rs => {
        let d = new Date();
        let n = d.getFullYear();
        let m = d.getMonth() + 1;
        this._search.Month = m;
        this._search.Year = n;
        this.setState({
          Month: m,
          MonthName: m.toString(),
          Year: n,
          YearName: n.toString(),
          Login_EmployeeGuid: rs.data.EmployeeGuid,
          Login_LoginName: rs.data.LoginName,
          Login_OrganizationGuid: rs.data.OrganizationGuid,
        });
        this.CountTask();
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
    // this.ListTask_Kanban()
  }
  //load lại trang sau khi xử lí từ trang khác
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Taskadd') {
      API.getProfile()
        .then(rs => {
          this.setState({
            Login_EmployeeGuid: rs.data.EmployeeGuid,
            Login_LoginName: rs.data.LoginName,
            Login_OrganizationGuid: rs.data.OrganizationGuid,
          });
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error.data);
        });
      if (nextProps.Data !== null) {
        this.Calendar_GetAll();
      } else {
      }
    } else if (nextProps.moduleId === 'Calendar_Back') {
      this.setState({EvenCalendar: true});
      this.Calendar_GetAll();
    }
    this.nextPage(true);
  }
  //#region Search
  updateSearch = search => {
    this._search.Search = search;
    this.nextPage(true);
  };
  //#endregion

  //#region Hàm đếm từng trạng thái của task
  CountTask() {
    var obj = {
      OrganizationGuid: this.state.Login_OrganizationGuid,
      LoginName: this.state.Login_LoginName,
      EmployeeGuid: this.state.Login_EmployeeGuid,
    };
    API_HR.CountMaintenanceRequest(obj)
      .then(rss => {
        var listBadge = this.listtabbarBotom;
        var _data = JSON.parse(rss.data.data);
        for (let i = 0; i < _data.length; i++) {
          // if (_data[i].Name == 'waiting') {
          //   listBadge[0].Badge = _data[i].Value;
          // } else
          if (_data[i].Name == 'waitingSX') {
            listBadge[1].Badge += _data[i].Value;
          } else if (_data[i].Name == 'complete') {
            listBadge[2].Badge += _data[i].Value;
          } else {
            // listBadge[4].Badge += _data[i].Value
          }
        }
        this.setState({listtabbarBotomView: listBadge});
        this.listtabbarBotom = listBadge;
        this.nextPage(false);
      })
      .catch(error => {
        this.setState({loading: false});
        console.log('Error when call API Mobile.');
      });
  }
  //#endregion

  //#region Lấy danh sách Task

  nextPage(status) {
    if (status) {
      this._search.Number = 1;
      this.setState({
        loadingSearch: true,
        ListTask: [],
      });
    } else {
      this._search.Number++;
    }
    API_HR.getAllMaintenanceRequest(this._search)
      .then(rs => {
        var _ListTask = this.state.ListTask;
        var _data = JSON.parse(rs.data.data).data;
        if (_data.length > 0) {
          for (let i = 0; i < _data.length; i++) {
            //_data[i].Rate =  parseInt(_data[i].Rate) ;
            _ListTask.push(_data[i]);
          }
        }
        for (let i = 0; i < _data.length; i++) {
          if (_data[i].ApproveStatus == '0' && _data[i].Approve == '0') {
            _data[i].Icon = 'check';
            _data[i].TypeIcon = 'antdesign';
          } else {
            _data[i].Icon = 'circle';
            _data[i].TypeIcon = 'entypo';
          }
          //_ListTask.push (_data[i]);
        }
        this.setState({
          ListTask: _ListTask,
          refreshing: false,
          loading: false,
          loadingSearch: false,
        });
      })
      .catch(error => {
        this.setState({loading: false});
        console.log('Error when call API Mobile.');
      });
  }
  ProblemResultApplyofSX(item, index) {
    let _obj = {
      MaintenanceRequestId: item.RowGuid,
    };
    let _data = new FormData();
    _data.append('insert', JSON.stringify(_obj));
    API_HR.ProblemResultApplyofSX(_data)
      .then(rs => {
        if (rs.data.errorCode == 200) {
          this.state.ListTask[index].ApproveStatus = '1';
          this.setState({
            ListTask: this.state.ListTask,
          });
        }
        debugger;
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }

  loadMoreData() {
    if (this.state.offload === false) {
      this.nextPage(false);
    }
  }
  //#endregion

  // lấy danh sách kế hoạch năm
  nextPageKHN(status) {
    console.log('Tháng 777:DATA' + this._search.Month);
    if (status) {
      this._search.CurrentPage = 1;
      this.setState({
        loadingSearch: true,
        ListTaskKHN: [],
      });
      var obj = {
        search: {value: this._search.Search},
        Year: this._search.Year,
        Month: this._search.Month,
        CurrentPage: this._search.CurrentPage,
        Length: 30,
        Status: 1,
      };
    } else {
      this._search.CurrentPage++;
      var obj = {
        search: {value: this._search.Search},
        Year: this._search.Year,
        Month: this._search.Month,
        CurrentPage: this._search.CurrentPage,
        Length: 30,
        Status: 1,
      };
    }

    API_HR.getAllMaintenanceManagement(obj)
      .then(rs => {
        var _ListTask = this.state.ListTaskKHN;
        var _data = JSON.parse(rs.data.data).data;
        if (_data.length > 0) {
          for (let i = 0; i < _data.length; i++) {
            //_data[i].Rate =  parseInt(_data[i].Rate) ;
            _ListTask.push(_data[i]);
          }
        }
        this.setState({
          ListTaskKHN: _ListTask,
          refreshing: false,
          loading: false,
          loadingSearch: false,
        });
      })
      .catch(error => {
        this.setState({loading: false});
        console.log('Error when call API Mobile.');
      });
  }

  loadMoreDataKHN() {
    if (this.state.offload === false) {
      this.nextPageKHN(false);
    }
  }

  // Kế hoạch năm
  CustomListAllKHN = item => (
    <FlatList
      data={item}
      renderItem={({item, index}) => (
        <ListItem
          key={index}
          subtitle={() => {
            return (
              <View style={{flexDirection: 'column', marginTop: -20}}>
                <View style={{flexDirection: 'row'}}>
                  {/* icon */}

                  <View style={{flex: 5, flexDirection: 'column'}}>
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={{flex: 4}}
                        onPress={() => this.onViewItemKHN(item.MaintainGuid)}>
                        <Text style={AppStyles.Titledefault}>
                          {item.ProductCode}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          alignItems: 'flex-end',
                          justifyContent: 'center',
                        }}>
                        <Text style={{fontSize: 12}}>
                          {this.getParsedDate(item.DeliveryDate)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        margin: 4,
                      }}
                    />
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flexDirection: 'row', flex: 5}}>
                        <View style={{flexDirection: 'row', flex: 1}}>
                          <TouchableOpacity
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginRight: 10,
                            }}>
                            <Icon
                              name="calculator"
                              type="font-awesome"
                              size={15}
                              color={AppColors.gray}
                            />
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {
                                  color: AppColors.gray,
                                },
                              ]}>
                              {' '}
                              {item.TotalTimes}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginRight: 10,
                            }}>
                            <Icon
                              name="clock-o"
                              type="font-awesome"
                              size={15}
                              color={AppColors.gray}
                            />
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {
                                  color: AppColors.gray,
                                },
                              ]}>
                              {' '}
                              {item.Times}
                            </Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginRight: 10,
                            }}
                            onPress={() =>
                              Actions.commentComponent({
                                ModuleId: '31',
                                ModuleIdDocument: '29',
                                RecordGuid: item.MaintainGuid,
                              })
                            }>
                            <Icon
                              name="comments-o"
                              type="font-awesome"
                              size={15}
                              color={AppColors.gray}
                            />
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {
                                  color: AppColors.gray,
                                },
                              ]}>
                              {' '}
                              {item.CountComment}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginRight: 10,
                            }}
                            onPress={() =>
                              Actions.attachmentComponent({
                                ModuleId: '50',
                                RecordGuid: item.MaintainGuid,
                              })
                            }>
                            <Icon
                              name="attachment"
                              type="entypo"
                              size={15}
                              color={AppColors.gray}
                            />
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {color: AppColors.gray},
                              ]}>
                              {' '}
                              {item.CountAtt}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            );
          }}
          bottomDivider
          //chevron
          onPress={() => null}
        />
      )}
      onEndReached={() => (item.length >= 10 ? this.loadMoreDataKHN() : null)}
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={[AppColors.Maincolor]}
        />
      }
    />
  );
  // combobox tháng
  _openComboboxMonth() {}

  openComboboxMonth = d => {
    this._openComboboxMonth = d;
  };

  onActionComboboxMonth() {
    this._openComboboxMonth();
  }

  // combobox năm
  _openComboboxYear() {}

  openComboboxYear = d => {
    this._openComboboxYear = d;
  };

  onActionComboboxYear() {
    this._openComboboxYear();
  }

  onValueChangeTypeMonth = data => {
    this._search.Month = data.value;
    this.setState({
      MonthName: data.text,
      Month: data.value,
    });
    this.nextPageKHN(true);
  };
  onValueChangeTypeYear = value => {
    this._search.Year = value.value;
    this.setState({
      YearName: value.text,
      Year: value.value,
    });
    this.nextPageKHN(true);
  };
  //Dẫn đến form chi tiết kế hoạch năm
  onViewItemKHN(item) {
    console.log('Click vào phiếu :' + item);
    Actions.openMaintenanceManagement({viewId: item});
  }
  //#region Search kế hoạch năm
  updateSearchKHN = search => {
    this._search.Search = search;
    this.nextPageKHN(true);
  };
  //End kế hoạch năm

  //#region Lọc ngày
  Searchday(value) {
    this.setState({OffSearchDay: false, loadingSearch: true});
    var _item = value.toString();
    if (_item != undefined && _item != '') {
      var yyyy = _item.substring(0, 4);
      var MM = _item.substring(4, 6);
      if (MM.substring(0, 1) == 0) {
        MM = MM.substring(1, 2);
      }
      var dd = _item.substring(6, 8);
      if (dd.substring(0, 1) == 0) {
        dd = dd.substring(1, 2);
      }
      var _startDate = yyyy + '-' + MM + '-' + dd;
      var _EndDate = yyyy + '-' + MM + '-' + dd;
      this._search.StartDate = _startDate;
      this._search.EndDate = _EndDate;
      this.nextPage(true);
    }
  }
  //#endregion
  //#region View Task
  CustomListAll = item => (
    <FlatList
      data={item}
      renderItem={({item, index}) => (
        <ListItem
          key={index}
          subtitle={() => {
            return (
              <View style={{flexDirection: 'column', marginTop: -20}}>
                <View style={{flexDirection: 'row'}}>
                  {/* icon */}
                  <TouchableOpacity
                    style={{
                      flex: 0.5,
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}
                    onPress={() => this.ProblemResultApplyofSX(item, index)}>
                    {item.ApproveStatus == '1' ? (
                      <Icon
                        name={'check'}
                        type="entypo"
                        color={AppColors.ColorEdit}
                        size={20}
                      />
                    ) : (
                      <Icon
                        name={'square-o'}
                        type="font-awesome"
                        color={AppColors.IconcolorTabbar}
                        size={20}
                      />
                    )}
                  </TouchableOpacity>

                  <View style={{flex: 5, flexDirection: 'column'}}>
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={{flex: 4}}
                        onPress={() => this.onViewItem(item.RowGuid)}>
                        <Text style={AppStyles.Titledefault}>
                          {item.DeviceName}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={{
                          flex: 0.5,
                          alignItems: 'flex-end',
                          justifyContent: 'center',
                        }}
                        onPress={() => this.onAction(item)}>
                        <Icon
                          name="dots-three-horizontal"
                          type="entypo"
                          size={15}
                          color={
                            item.ApproveStatus == '1' && item.Approve == '1'
                              ? AppColors.green
                              : AppColors.red
                          }
                        />
                      </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Progress.Bar
                        style={{marginBottom: 0, marginRight: 0}}
                        progress={item.Rate}
                        height={2}
                        width={250}
                        color={
                          item.Rate == '100' ? AppColors.green : AppColors.red
                        }
                      />
                      <Text
                        style={[
                          AppStyles.Textdefault,
                          {
                            color: AppColors.StatusTask_CG,
                          },
                        ]}>
                        {' '}
                        {item.Rate} %
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flexDirection: 'row', flex: 5}}>
                        <View style={{flexDirection: 'row', flex: 1}}>
                          <TouchableOpacity
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginRight: 10,
                            }}
                            onPress={() =>
                              Actions.taskComment({
                                data: {
                                  TaskGuid: item.TaskGuid,
                                  TaskName: item.Subject,
                                },
                              })
                            }>
                            <Icon
                              name="comments-o"
                              type="font-awesome"
                              size={15}
                              color={
                                item.CountComment > 0
                                  ? AppColors.red
                                  : AppColors.gray
                              }
                            />
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {
                                  color:
                                    item.CountComment > 0
                                      ? AppColors.red
                                      : AppColors.gray,
                                },
                              ]}>
                              {' '}
                              {item.CountComment}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginRight: 10,
                            }}
                            onPress={() =>
                              Actions.attachmentComponent({
                                ModuleId: '18',
                                RecordGuid: item.RowGuid,
                              })
                            }>
                            <Icon
                              name="attachment"
                              type="entypo"
                              size={15}
                              color={AppColors.gray}
                            />
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {color: AppColors.gray},
                              ]}>
                              {' '}
                              {item.CountAtt}
                            </Text>
                          </TouchableOpacity>
                        </View>
                        <View style={{flex: 1, alignItems: 'flex-end'}}>
                          <Text style={AppStyles.Textdefault}>
                            {FuncCommon.ConDate(item.EndDate, 0)}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            );
          }}
          bottomDivider
          //chevron
          onPress={() => null}
        />
      )}
      onEndReached={() => (item.length >= 10 ? this.loadMoreData() : null)}
      keyExtractor={(item, index) => index.toString()}
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          tintColor="#f5821f"
          titleColor="#fff"
          colors={[AppColors.Maincolor]}
        />
      }
    />
  );
  //#endregion
  //#region View Tổng phân biệt các View
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={AppStyles.container}>
          <TabBar
            title={'Kết quả bảo dưỡng'}
            BackModuleByCode={'TM'}
            //Formcalendar={true}
            //FormTask={true}
            FormSearch={true}
            CallbackFormTask={() => this.setState({EvenKanban: false})}
            CallbackFormcalendar={callback =>
              this.CallbackFormcalendar(callback)
            }
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
          />
          <Divider />
          {this.state.ClickStatus == false ? (
            <View style={{flexDirection: 'row', padding: 10, paddingBottom: 0}}>
              <View style={{flexDirection: 'column', flex: 3}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Tháng</Text>
                  <Text style={{color: 'red'}} />
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput, {marginRight: 5}]}
                  onPress={() => this.onActionComboboxMonth()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.state.Month == '' || this.state.Month === null
                        ? {color: AppColors.gray}
                        : {color: 'black'},
                    ]}>
                    {this.state.Month !== '' && this.state.Month !== null
                      ? this.state.MonthName
                      : 'Chọn Tháng...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      color="#888888"
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', flex: 3}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Năm</Text>
                  <Text style={{color: 'red'}} />
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionComboboxYear()}>
                  <Text
                    style={[
                      AppStyles.TextInput,
                      this.state.Year == '' || this.state.Year == null
                        ? {color: AppColors.gray}
                        : {color: 'black'},
                    ]}>
                    {this.state.Year !== '' && this.state.Year !== null
                      ? this.state.YearName
                      : 'Chọn Năm...'}
                  </Text>
                  <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icon
                      color="#888888"
                      name={'chevron-thin-down'}
                      type="entypo"
                      size={20}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
          {this.state.ClickStatus == false &&
          this.state.EvenFromSearch == true ? (
            <FormSearch
              CallbackSearch={callback => this.updateSearchKHN(callback)}
            />
          ) : null}
          {this.state.ClickStatus == false && this.state.loading !== true ? (
            <View style={{flex: 6}}>
              {this.state.ListTaskKHN.length > 0 ? (
                this.CustomListAllKHN(this.state.ListTaskKHN)
              ) : (
                //View không có dữ liệu task
                <View style={[AppStyles.centerAligned]}>
                  <ImageBackground
                    source={require('../../../images/TM/TaskNull.jpg')}
                    style={[
                      AppStyles.centerAligned,
                      {width: '100%', height: '70%'},
                    ]}
                  />
                  <Text style={[AppStyles.Titledefault]}>
                    Không tìm thấy kế hoạch năm nào
                  </Text>
                </View>
              )}
            </View>
          ) : null}

          {this.state.ClickStatus == true ? (
            <SearchDay CallBackValue={value => this.Searchday(value)} />
          ) : null}
          {this.state.ClickStatus == true &&
          this.state.EvenFromSearch == true ? (
            <FormSearch
              CallbackSearch={callback => this.updateSearch(callback)}
            />
          ) : null}
          {this.state.ClickStatus == true && this.state.loading !== true ? (
            <View style={{flex: 6}}>
              {this.state.ListTask.length > 0 ? (
                this.CustomListAll(this.state.ListTask)
              ) : (
                //View không có dữ liệu task
                <View style={[AppStyles.centerAligned]}>
                  <ImageBackground
                    source={require('../../../images/TM/TaskNull.jpg')}
                    style={[
                      AppStyles.centerAligned,
                      {width: '100%', height: '70%'},
                    ]}
                  />
                  <Text style={[AppStyles.Titledefault]}>
                    Không tìm thấy kết quả bảo dưỡng nào
                  </Text>
                </View>
              )}
            </View>
          ) : null}
          <View style={AppStyles.StyleTabvarBottom}>
            {this.state.listtabbarBotomView.length > 0 ? (
              <TabBarBottomCustom
                ListData={this.listtabbarBotom}
                onCallbackValueBottom={callback =>
                  this.CallbackValueBottom(callback)
                }
              />
            ) : null}
          </View>
          {this.state.DropdownMonth.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.onValueChangeTypeMonth}
              data={this.state.DropdownMonth}
              nameMenu={'Chọn Tháng'}
              eOpen={this.openComboboxMonth}
              position={'bottom'}
            />
          ) : null}
          {this.state.DropdownYear.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.onValueChangeTypeYear}
              data={this.state.DropdownYear}
              nameMenu={'Chọn Năm'}
              eOpen={this.openComboboxYear}
              position={'bottom'}
            />
          ) : null}
          <MenuActionCompoment
            callback={this.actionMenuCallback}
            data={DataMenuBottom}
            eOpen={this.openMenu}
            position={'bottom'}
          />

          <MenuActionCompoment
            callback={this.CallbackMenuBottom}
            data={MenuBottom}
            nameMenu={'Tuỳ chọn'}
            eOpen={this.openMenuBottom}
            position={'bottom'}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  //#endregion
  //#region Chức năng dùng chung cho API
  //
  //Dẫn đến form add của các module
  CallbackFormAdd() {
    this.state.EvenCalendar
      ? Actions.calendarAdd({TypeForm: 'add'})
      : Actions.taskAdd({TypeForm: 'add'});
  }
  //Mở dropdow
  _openMenuKanban() {}
  openMenuKanban = d => {
    this._openMenuKanban = d;
  };
  onActionKanban(item) {
    this.setState({
      selectItem: item,
    });
    this._openMenuKanban();
  }
  //update tiến độ
  actionMenuCallbackKanban = d => {
    switch (d) {
      case 'waiting':
        this.Submit_Kanban('X', 0);
        break;
      case 'notstart':
        this.Submit_Kanban('N', 0);
        break;
      case 'process':
        this.Submit_Kanban('P', 50);
        break;
      case 'complete':
        this.Submit_Kanban('C', 100);
        break;
      case 'waitsomeone':
        this.Submit_Kanban('W', 0);
        break;
      case 'expiry':
        this.Submit_Kanban('E', 0);
        break;
      case 'deferred':
        this.Submit_Kanban('D', 0);
        break;
      default:
        break;
    }
    this.ListTask_Kanban();
  };
  //Độ dài văn bản
  checkText(str, i) {
    try {
      if (str !== null && str !== '') {
        var splitted = str.split(' ');
        var strreturn = '';
        var j = 0;
        for (var a = 0; a < splitted.length; a++) {
          j = j + 1;
          if (j == i) {
            strreturn = strreturn + ' ' + splitted[a] + '...';
            break;
          } else {
            strreturn = strreturn + ' ' + splitted[a];
          }
        }
        return strreturn.trim();
      } else {
        return 'Không có nội dung';
      }
    } catch (error) {
      return 'Không có nội dung';
    }
  }
  // Tuỳ chỉnh thời gian
  customdateTime(DataTime) {
    if (DataTime != null) {
      var date = FuncCommon.ConDate(DataTime, 1);
      var time = date.split(' ');
      return time[1];
    } else {
      return '';
    }
  }
  _openMenuBottom() {}
  openMenuBottom = d => {
    this._openMenuBottom = d;
  };
  onActionMenuBottom() {
    this._openMenuBottom();
  }
  CallbackMenuBottom = d => {
    this.CallbackValueBottom(d);
  };
  //#endregion

  //#region Chức năng Task

  //load lại danh sách
  _onRefresh = () => {
    this.nextPage();
    // this.setState({ refreshing: true });
  };

  //Dẫn đến form chi tiết
  onViewItem(item) {
    Actions.GetMaintenanceRequest({viewId: item});
  }

  //Bật chế độ ghim theo dõi
  UpdateFollow = item => {
    var obj = {
      IdS: [item.TaskGuid.toString()],
    };
    API_TM_TASKS.UpdateFollow(obj)
      .then(rs => {
        if (rs.data.errorCode == 200) {
          var _list = this.state.ListTask;
          for (let i = 0; i < _list.length; i++) {
            if (_list[i].TaskGuid == item.TaskGuid) {
              _list[i].IsFollow = !_list[i].IsFollow;
            }
          }
          this.setState({
            ListTask: _list,
          });
        }
      })
      .catch(error => {
        console.log('Error Update Follow');
      });
  };
  //Chức năng của menu bottom
  CallbackValueBottom = value => {
    switch (value) {
      case 'T':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this.onActionMenuBottom();
        break;
      case 'T':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this.onActionMenuBottom();
        break;
      case 'N':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        console.log(this.state);
        this._search.StartDate = null;
        this._search.EndDate = null;
        (this._search.Process = value), this.nextPage(true);
        break;
      case 'P':
        this.setState({ClickStatus: true, OffSearchDay: false, loading: true});
        this._search.StartDate = null;
        this._search.EndDate = null;
        (this._search.Process = value), this.nextPage(true);
        break;
      case 'C':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this._search.StartDate = null;
        this._search.EndDate = null;
        this._search.Process = value;
        this.nextPage(true);
        break;
      case 'X':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this._search.StartDate = null;
        this._search.EndDate = null;
        this._search.Process = value;
        // this.hideMenuTop();
        this.nextPage(true);
        break;
      case 'D':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this._search.StartDate = null;
        this._search.EndDate = null;
        this._search.Process = value;
        // this.hideMenuTop();
        this.nextPage(true);
        break;
      case 'W':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this._search.StartDate = null;
        this._search.EndDate = null;
        this._search.Process = value;
        // this.hideMenuTop();
        this.nextPage(true);
        break;
      case 'F':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this._search.StartDate = null;
        this._search.EndDate = null;
        this._search.Process = value;
        // this.hideMenuTop();
        this.nextPage(true);
        break;
      case 'waiting':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this._search.ApproveStatus = 0;
        this.nextPage(true);
        break;
      case 'waitingSX':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this._search.Approve = 0;
        this._search.Process = value;
        this.nextPage(true);
        break;
      case 'OK':
        this.setState({ClickStatus: true, OffSearchDay: true, loading: true});
        this._search.Approve = 1;
        this._search.ApproveStatus = 1;
        this._search.Process = value;
        this.nextPage(true);
        break;
      case 'E':
        this.setState({ClickStatus: false, OffSearchDay: true, loading: true});
        this._search.StartDate = null;
        this._search.EndDate = null;
        this._search.Process = value;
        // this.hideMenuTop();
        this.nextPage(true);
        break;
      case 'Kanban':
        this.ListTask_Kanban();
        this.setState({ClickStatus: true, EvenKanban: true});
        break;
      default:
        break;
    }
  };
  //Chức năng trong menu Task
  actionMenuCallback = d => {
    switch (d) {
      case 'details':
        this.onViewItem(this.state.selectItem.RowGuid);
        break;
      case 'edit':
        Actions.taskAdd({
          TypeForm: 'edit',
          data: {RowGuid: this.state.selectItem.RowGuid},
        });
        break;
      case 'delete':
        Alert.alert(
          'Thông báo',
          'Bạn có muốn xóa ' + this.state.selectItem.Subject + ' không?',
          [
            {
              text: 'Xác nhận',
              onPress: () => this.DeleteTask(this.state.selectItem),
            },
            {text: 'Huỷ', onPress: () => console.log('No Pressed')},
          ],
          {cancelable: true},
        );
        break;
      case 'comments':
        Actions.taskComment({
          data: {
            TaskGuid: this.state.selectItem.RowGuid,
            TaskName: this.state.selectItem.Subject,
          },
        });
        break;
      case 'attachment':
        var obj = {
          ModuleId: '18',
          RecordGuid: this.state.selectItem.RowGuid,
        };
        Actions.attachmentComponent(obj);
        break;
      case 'Calendar':
        this.InsertToCalenderOfUser(this.state.selectItem);
        break;
      case 'UpdateProcess':
        Actions.taskUpdateProcess({
          data: {TaskGuid: this.state.selectItem.TaskGuid},
        });
        break;
      default:
        break;
    }
  };
  //Xoá 1 bản ghi task
  DeleteTask = item => {
    let obj = {
      IdS: [item.TaskGuid],
    };
    API_TM_TASKS.Delete(obj)
      .then(rs => {
        this.updateSearch('');
        Alert.alert(
          'Thông báo',
          rs.data.Title,
          [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
          {cancelable: true},
        );
      })
      .catch(error => {
        console.log('Error when call API Mobile.');
      });
  };

  //thêm mới vào Calendar
  InsertToCalenderOfUser = item => {
    let obj = {
      IdS: [item.TaskGuid],
    };
    API_TM_TASKS.InsertToCalenderOfUser(obj)
      .then(rs => {
        if (rs.data.Error) {
          Alert.alert(
            'Thông báo',
            rs.data.Title,
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
        } else {
          Alert.alert(
            'Thông báo',
            rs.data.Title,
            [{text: 'Đóng', onPress: () => console.log('No Pressed')}],
            {cancelable: true},
          );
        }
      })
      .catch(error => {
        console.log('Error when call API Mobile.');
      });
  };
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //Mở dropdow
  _openMenu() {}
  openMenu = d => {
    this._openMenu = d;
  };
  onAction(item) {
    this.setState({
      selectItem: item,
    });
    this._openMenu();
  }
  //#endregion
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(MaintenanceRequestComponent);
