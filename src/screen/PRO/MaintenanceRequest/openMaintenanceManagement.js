import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  StyleSheet,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  ToastAndroid,
  Alert,
} from 'react-native';
import {
  DatePicker,
  Label,
  Item,
  Input,
  Container,
  Content,
  Picker,
  Button,
  Thumbnail,
} from 'native-base';
import PopupHandleVote from '../../component/PopupHandleVote';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_HR, API} from '@network';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, ListItem, Header, Icon} from 'react-native-elements';
import Fonts from '../../../theme/fonts';
import {Actions} from 'react-native-router-flux';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';

const styles = StyleSheet.create({
  styletextLable: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  styletext: {
    fontSize: 14,
    padding: 5,
  },
  textLableCol2: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 5,
  },
  textCol2: {
    fontSize: 14,
    padding: 5,
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
    color: 'black',
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
  },
  containerContent: {
    flex: 10,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
});
const options = [
  {
    component: <Text style={{color: 'crimson', fontSize: 20}}>Đóng</Text>,
  },
  {
    component: <Text style={{color: 'blue', fontSize: 20}}>Duyệt phiếu</Text>,
    height: 80,
  },
  {
    component: <Text style={{color: 'blue', fontSize: 20}}>Trả lại</Text>,
    height: 80,
  },
];

class openMaintenanceManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: null,
      list: [],
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      selected2: [],
      //StartTime: null,
      viewId: '',
      Evenpopup: false,
    };
    this.listtabbarBotom = [
      {
        Title: 'Trang chủ',
        Icon: 'edit',
        Type: 'antdesign',
        Value: 'edit',
        OffEditcolor: true,
        Checkbox: false,
      },
      {
        Title: 'Danh sách',
        Icon: 'Safety',
        Type: 'antdesign',
        Value: 'success',
        OffEditcolor: true,
        Checkbox: false,
      },
      {
        Title: 'Duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'approve',
        OffEditcolor: true,
        Checkbox: false,
      },
      {
        Title: 'Ý kiến',
        Icon: 'message1',
        Type: 'antdesign',
        Value: 'comment',
        OffEditcolor: true,
        Checkbox: false,
      },
    ];
    this.btns = [
      {
        name: 'Duyệt',
        value: 'D',
        color: '#3366CC',
      },
      {
        name: 'Hủy',
        value: 'H',
        color: 'red',
      },
    ];
  }
  setPickUpLocation(PickUpLocation) {
    this.state.Data.PickUpLocation = PickUpLocation;
    this.setState({Data: this.state.Data});
  }

  componentDidMount(): void {
    if (this.props.RecordGuid !== undefined) {
      this.state.viewId = this.props.RecordGuid;
    } else {
      this.state.viewId = this.props.viewId;
    }
    this.GetDetail();
    this.getItem();
  }
  onPressBack() {
    Actions.MaintenanceRequest();
  }
  onAttachment() {
    Actions.attachmentComponent({
      ModuleId: '5',
      RecordGuid: this.state.viewId,
    });
  }
  Delete = () => {
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_HR.deleteTrainingResults(id.Id)
      .then(response => {
        console.log(
          '==============ketquaXoa-errorCode: ' + response.data.Error,
        );
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            TrainingResultGuid: this.state.viewId,
          });
          ToastAndroid.showWithGravity(
            'Xóa thành công',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
          this.onPressBack();
        } else {
          ToastAndroid.showWithGravity(
            'Có lỗi khi xóa',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data);
      });
  };
  GetDetail = () => {
    let id = this.state.viewId;
    var obj = {
      MaintainGuid: this.state.viewId,
    };
    API_HR.getDetailMaintenanceManagement(obj)
      .then(res => {
        this.state.list = JSON.parse(res.data.data);
        this.setState({list: JSON.parse(res.data.data)});
      })
      .catch(error => {
        console.log('===========> error');
        console.log(error);
      });
  };
  getItem() {
    let id = this.state.viewId;
    var obj = {
      MaintainGuid: this.state.viewId,
    };
    API_HR.getItemMaintenanceManagement(obj)
      .then(res => {
        this.setState({
          Data: JSON.parse(res.data.data),
        });
        API.getProfile().then(rs => {
          console.log('callback==_______________++>>>>>>' + rs.data.LoginName);
          this.setState({LoginName: rs.data.LoginName});
        });
      })
      .catch(error => {
        console.log(error.data);
        console.log('=======' + error.data);
      });
  }

  showActionSheet = () => this.actionSheet.show();
  getActionSheetRef = ref => (this.actionSheet = ref);
  CustomView = item => (
    <TouchableWithoutFeedback
      style={{flex: 1}}
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <TabBar_Title
          title={'Chi tiết kế hoạch năm'}
          callBack={() => this.onPressBack()}
        />
        <Divider />
        {/*hiển thị nội dung chính*/}
        <View style={{flex: 2, padding: 15}}>
          {/*Người tạo phiếu*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Số KH Năm :</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={[AppStyles.Textdefault]}>{item.MaintainNumber}</Text>
            </View>
          </View>
          {/*Phòng ban*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Mã thiết bị :</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={[AppStyles.Textdefault]}>{item.ProductCode}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Tên thiết bị :</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={[AppStyles.Textdefault]}>{item.ProductName}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Ngày bắt đầu :</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.getParsedDate(item.DeliveryDate)}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Tần suất BD :</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={[AppStyles.Textdefault]}>{item.FrequencyYear}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={[AppStyles.Textdefault]}>{item.Content}</Text>
            </View>
          </View>
          {/*nội dung phiếu*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Ghi chú :</Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={[AppStyles.Textdefault]}>{item.Note}</Text>
            </View>
          </View>
        </View>
        <View style={{flex: 4}}>
          <Divider style={{marginTop: 3, marginBottom: 3}} />
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                marginLeft: 10,
                flex: 1,
              }}>
              Kế hoạch bảo dưỡng
            </Text>
          </View>
          <View style={{flex: 1, padding: 5}}>
            {this.state.list.length > 0 ? (
              <FlatList
                style={{marginBottom: 10, height: '100%'}}
                refreshing={this.state.isFetching}
                ref={ref => {
                  this.ListView_Ref = ref;
                }}
                ListEmptyComponent={this.ListEmpty}
                //ListHeaderComponent={this.renderHeader}
                ListFooterComponent={this.renderFooter}
                keyExtractor={item => item.BankGuid}
                data={this.state.list}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    style={{flexDirection: 'row', padding: 5}}
                    onPress={() => this.onViewItem(item.MaintenanceRequestId)}>
                    <View style={{flex: 3}}>
                      {/*Người tạo phiếu*/}
                      <View style={{flexDirection: 'row', padding: 5}}>
                        <View style={{flex: 0.5}}>
                          <Thumbnail
                            source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                          />
                        </View>
                        <View style={{flex: 1.5}}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.EmployeeName}
                          </Text>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.MaintainTypeId == 1
                              ? 'Bảo dưỡng'
                              : item.MaintainTypeId == 2
                              ? 'Sửa chữa'
                              : item.MaintainTypeId == 3
                              ? 'Cơ sở hạ tầng'
                              : ''}
                          </Text>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.Body}
                          </Text>
                        </View>
                        <View style={{flex: 0.75}}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.StartDateString}
                          </Text>
                        </View>
                      </View>
                    </View>

                    <Divider style={{marginTop: 5}} />
                  </TouchableOpacity>
                )}
                onEndReachedThreshold={0.5}
              />
            ) : null}
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
  //Begin cretaed popup
  renderPopup(title, btns) {
    return (
      <PopupHandleVote
        onSubmit={(callback, object) => this.callbackSubmit(callback, object)}
        onClick={callback => this.clickPopup(callback)}
        title={title}
        desciption={'Nhập nội dung'}
        btns={btns}
      />
    );
  }
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }

  //Dẫn đến form chi tiết
  onViewItem(item) {
    if (item != null && item != undefined && item != '') {
      Actions.GetMaintenanceRequest({viewId: item});
    }
  }
  callbackPopup() {}
  clickPopup(callback) {
    this.callbackPopup = callback;
  }
  callbackSubmit(callback, object) {
    this.setState({
      Evenpopup: false,
    });
    if (callback != 'H') {
      this.callbackSubmit1(this.state.Eventitle, object.text);
    }
  }
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  CallbackValueBottom = value => {
    debugger;
    switch (value) {
      case 'approve':
        this.setState({Evenpopup: true});
        break;
      case 'attachment':
        break;
      case 'delete':
        break;
      case 'success':
        break;
      case 'comment':
        break;
      default:
        break;
    }
  };
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '-' + mm + '-' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //định dạng tiền tệ
  addPeriod(nStr) {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  }
  formatValue(value) {
    return Tolls.formatMoney(parseFloat(value) / 100, '$ ');
  }
  //định dạng ngày và giờ
  getParsedDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  //trạng thái
  customStatus(csStatus) {
    var stringstatus = '';
    if ((csStatus = 'W')) {
      stringstatus = 'Chờ duyệt';
    } else {
      stringstatus = 'Đã duyệt';
    }
    return stringstatus.toString();
  }
}
//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(openMaintenanceManagement);
