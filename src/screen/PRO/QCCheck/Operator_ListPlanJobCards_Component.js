import React, { Component } from 'react';
import { FlatList, Keyboard, Animated, Text, TouchableWithoutFeedback, RefreshControl, Image, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../../utils';
import FormSearch from '../../../component/FormSearch';
import MenuSearchDate from '../../../component/MenuSearchDate';
import Combobox from '../../../component/Combobox';
import { ScrollView } from 'react-native-gesture-handler';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
//#region Danh sách lọc trạng thái
const ListPercent = [
    {
        value: "1",
        text: "Đang sản xuất"
    },
    {
        value: "2",
        text: "Hoàn thành"
    }
];
//#endregion
class Operator_ListPlanJobCards_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            ValueSearchDate: "7",//Ngày hiện tại
            EvenFromSearch: true,
            Data_GetDataView: {
                NumberPage: 0,
                length: 30,
                search: { value: "" },
                StartDate: new Date(),
                EndDate: new Date(),
                LocationCode: "",
                EmployeeId: "",
                Percent: 1,
            },
            Percent_View: "",
            LocationCode_View: "",
            EmployeeId_View: "",
            ListView: [],
            ListLocation: [],
            ListEmpJobCards: [],
            searchPe: false,
            searchLo: false,
            searchEmp: false
        };
    }
    componentDidMount() {
        this.setState({ loading: true })
        this.GetLocation();
        this.GetEmpJobCards();
    }
    //#region  GetLocation
    GetLocation = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetLocation().then(res => {
                    FuncCommon.Data_Offline_Set('GetLocation', res);
                    this._GetLocation(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('GetLocation');
                this._GetLocation(x);
            }
        });
    };
    _GetLocation = (res) => {
        var list = [{value:'', text:'---Bỏ chọn---'}]
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
            list.push({
                value: data[i].LocationCode,
                text: data[i].Name,
            });
        }
        this.setState({
            ListLocation: list
        });
    }
    //#endregion
    //#region  GetEmpJobCards
    GetEmpJobCards = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetEmpJobCards().then(res => {
                    FuncCommon.Data_Offline_Set('GetEmpJobCards', res);
                    this._GetEmpJobCards(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('GetEmpJobCards');
                this._GetEmpJobCards(x);
            }
        });
    };
    _GetEmpJobCards = (res) => {
        var list = [{ value: "", text: '--- Tất cả ---' }]
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
            list.push({
                value: data[i].EmployeeId,
                text: data[i].EmployeeName,
            });
        }
        this.setState({
            ListEmpJobCards: list
        });
    }
    //#endregion

    //#region GetDataView
    Search = (txt) => {
        this.state.Data_GetDataView.search.value = txt;
        this.setState({ Data_GetDataView: this.state.Data_GetDataView });
        this.GetDataView(true, "search");
    };
    GetDataView = (s, check) => {
        console.log(check)
        if (s === true) {
            this.state.Data_GetDataView.NumberPage = 1;
            this.setState(
                {
                    loadingsearch: true,
                    ListView: [],
                }
            );
        } else {
            this.state.Data_GetDataView.NumberPage = ++this.state.Data_GetDataView.NumberPage
        }
        this.setState({ Data_GetDataView: this.state.Data_GetDataView });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.ListPlanJobCards(this.state.Data_GetDataView).then(res => {
                    FuncCommon.Data_Offline_Set('QC_ListPlanJobCards', res);
                    this._QC_ListPlanJobCards(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('QC_ListPlanJobCards');
                this._QC_ListPlanJobCards(x);
            }
        });
    };
    _QC_ListPlanJobCards = (res) => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            this.state.ListView.push(data[i]);
        }
        this.setState({
            ListView: this.state.ListView,
            loading: false,
            loadingsearch: false
        });
    };
    //#endregion
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"DANH SÁCH CÔNG VIỆC ĐANG SẢN XUẤT"}
                            FormSearch={true}
                            CallbackFormSearch={(callback) => this.setState({ EvenFromSearch: callback })}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        {this.state.EvenFromSearch === true ?
                            <View style={{ flexDirection: 'column',marginBottom:10 }}>
                                <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                                    <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                                        {/* <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu</Text> */}
                                        <TouchableOpacity style={[AppStyles.FormInput]} onPress={() => this.setState({ setEventStartDate: true })}>
                                            <Text>{FuncCommon.ConDate(this.state.Data_GetDataView.StartDate, 0)}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                                        {/* <Text style={AppStyles.Labeldefault}>Thời gian kết thúc</Text> */}
                                        <TouchableOpacity style={[AppStyles.FormInput]} onPress={() => this.setState({ setEventEndDate: true })}>
                                            <Text>{FuncCommon.ConDate(this.state.Data_GetDataView.EndDate, 0)}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: 'column', padding: 5 }} >
                                        {/* <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>Lọc</Text> */}
                                        <TouchableOpacity style={[AppStyles.FormInput]} onPress={() => this.onActionSearchDate()}>
                                            <Icon name={"down"} type={"antdesign"} size={18} color={AppColors.gray} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                                    <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                                        <TouchableOpacity style={[AppStyles.FormInput]} onPress={() => this.onActionComboboxLocation()}>
                                            <Text style={this.state.Data_GetDataView.LocationCode !== "" ? { color: AppColors.black } : { color: AppColors.gray }}>
                                                {this.state.Data_GetDataView.LocationCode !== "" ? this.state.LocationCode_View : "Chọn công đoạn"}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                                        <TouchableOpacity style={[AppStyles.FormInput]} onPress={() => this.onActionComboboxPercent()}>
                                            <Text>{this.state.Percent_View}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                                    <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                                        <TouchableOpacity style={[AppStyles.FormInput]} onPress={() => this.onActionComboboxEmpJobCards()}>
                                            <Text style={this.state.Data_GetDataView.EmployeeId !== null ? { color: AppColors.black } : { color: AppColors.gray }}>
                                                {this.state.Data_GetDataView.EmployeeId !== null ? this.state.EmployeeId_View : "Chọn người làm"}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <FormSearch CallbackSearch={(callback) => this.Search(callback)} />
                            </View>
                            : null}
                        <View style={{ flex: 1 }}>
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã phiếu</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Máy</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>C.đoạn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 140 }]}><Text style={AppStyles.Labeldefault}>Người làm</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Start</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>SL KH</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>SL thực</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>SL OK</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>SL NG</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Còn lại</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>%HT</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListView.length > 0 ?
                                        <FlatList
                                            data={this.state.ListView}
                                            renderItem={({ item, index }) => (
                                                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() =>
                                                    // this.ViewItemPrint(item)
                                                }>
                                                    <View style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.JobCardId}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.MarchineName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.PartItemName !== "" ? item.PartItemName : item.ItemName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.LocationName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 140 }]}><Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{FuncCommon.ConDate(item.StartTime, 0)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.customFloat(item.QuantityPlan)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.customFloat(item.QuantityDelivered)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.customFloat(item.QuantityOK)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.customFloat(item.QuantityNG)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.customQuantityCL(item.QuantityPlan, item.QuantityOK)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.customHT(item.QuantityPlan, item.QuantityOK)}</Text></View>
                                                </TouchableOpacity>
                                            )}
                                            onEndReached={() => this.state.ListView.length >= (30 * this.state.Data_GetDataView.NumberPage) ? this.GetDataView(false, "nextpage") : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        // refreshControl={
                                        //     <RefreshControl
                                        //         refreshing={this.state.loading}
                                        //         onRefresh={this._onRefresh}
                                        //         tintColor="#f5821f"
                                        //         titleColor="#fff"
                                        //         colors={[AppColors.Maincolor]}
                                        //     />
                                        // }
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <Combobox
                            value={this.state.Data_GetDataView.Percent.toString()}
                            TypeSelect={"single"}// single or multiple
                            callback={this.ChangePercent}
                            data={ListPercent}
                            nameMenu={'Chọn trạng thái'}
                            eOpen={this.openComboboxPercent}
                            position={'bottom'}
                        />
                        {this.state.ListLocation.length > 0 ?
                            <Combobox
                                value={this.state.Data_GetDataView.EmployeeId.toString()}
                                TypeSelect={"single"}// single or multiple
                                callback={this.ChangeLocation}
                                data={this.state.ListLocation}
                                nameMenu={'Chọn công đoạn'}
                                eOpen={this.openComboboxLocation}
                                position={'bottom'}
                            />
                            : null}
                        {this.state.ListEmpJobCards.length > 0 ?
                            <Combobox
                                value={this.state.Data_GetDataView.EmployeeId.toString()}
                                TypeSelect={"single"}// single or multiple
                                callback={this.ChangeEmpJobCards}
                                data={this.state.ListEmpJobCards}
                                nameMenu={'Chọn người làm'}
                                eOpen={this.openComboboxEmpJobCards}
                                position={'bottom'}
                            />
                            : null}

                        <MenuSearchDate
                            value={this.state.ValueSearchDate}
                            callback={this.CallbackSearchDate}
                            eOpen={this.openMenuSearchDate}
                        />
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    //#region chức năng lọc này cố định
    _openMenuSearchDate() { }
    openMenuSearchDate = (d) => {
        this._openMenuSearchDate = d;
    }
    onActionSearchDate = () => {
        this._openMenuSearchDate();
    }
    CallbackSearchDate = (callback) => {
        if (callback.start !== undefined) {
            this.state.Data_GetDataView.StartDate = new Date(callback.start);
            this.state.Data_GetDataView.EndDate = new Date(callback.end);
            this.setState({ Data_GetDataView: this.state.Data_GetDataView, ValueSearchDate: callback.value });
            this.GetDataView(true, "searchDay");
        }
    }
    //#endregion
    //#region  combobox Percent
    _openComboboxPercent() { }
    openComboboxPercent = (d) => {
        this._openComboboxPercent = d;
    }
    onActionComboboxPercent() {
        this.setState({ searchPe: true });
        this._openComboboxPercent();
    }
    ChangePercent = (rs) => {
        if (rs !== null) {
            this.state.Data_GetDataView.Percent = parseInt(rs.value);
            this.setState({ Data_GetDataView: this.state.Data_GetDataView, Percent_View: rs.text });
            if (this.state.searchPe === true) {
                this.GetDataView(true, "searchPercent");
            }
        }
    }
    //#endregion
    //#region  combobox Location
    _openComboboxLocation() { }
    openComboboxLocation = (d) => {
        this._openComboboxLocation = d;
    }
    onActionComboboxLocation() {
        this.setState({ searchLo: true });
        this._openComboboxLocation();
    }
    ChangeLocation = (rs) => {
        if (rs !== null) {
            this.state.Data_GetDataView.LocationCode = rs.value;
            this.setState({ Data_GetDataView: this.state.Data_GetDataView, LocationCode_View: rs.text })
            if (this.state.searchLo === true) {
                this.GetDataView(true, "searchLo");
            };
        }
    }
    //#endregion
    //#region  combobox EmpJobCards
    _openComboboxEmpJobCards() { }
    openComboboxEmpJobCards = (d) => {
        this.setState({ searchEmp: true });
        this._openComboboxEmpJobCards = d;
    }
    onActionComboboxEmpJobCards() {
        this._openComboboxEmpJobCards();
    }
    ChangeEmpJobCards = (rs) => {
        if (rs !== null) {
            this.state.Data_GetDataView.EmployeeId = rs.value;
            this.setState({ Data_GetDataView: this.state.Data_GetDataView, EmployeeId_View: rs.text });
            if (this.state.searchEmp === true) {
                this.GetDataView(true, "EmpJobCards");
            };
        }
    }
    //#endregion

    //#region customQuantityCL && customHT
    customQuantityCL = (QuantityPlan, QuantityOK) => {
        var qtyPlan = QuantityPlan === "" ? 0 : parseFloat(QuantityPlan);
        var qtyOK = QuantityOK === "" ? 0 : parseFloat(QuantityOK);
        return this.customFloat(qtyPlan - qtyOK);
    }
    customHT = (QuantityPlan, QuantityOK) => {
        var qtyPlan = QuantityPlan === "" ? 0 : parseFloat(QuantityPlan);
        var qtyOK = QuantityOK === "" ? 0 : parseFloat(QuantityOK);
        return this.customFloat(qtyOK / qtyPlan * 100) + ' %';
    }
    //#endregion 
















    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_ListPlanJobCards_Component);
