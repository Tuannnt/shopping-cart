import React, {Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  Keyboard,
  Alert,
} from 'react-native';
import {ToastAndroid} from 'react-native';
import {AppStyles, AppColors} from '@theme';
import {Actions} from 'react-native-router-flux';
import {API_ListProposePurchases} from '@network';
import {ListItem, Thumbnail, Text, Left, Body, Picker} from 'native-base';
import {connect} from 'react-redux';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import _ from 'lodash';
import {FuncCommon} from '../../../utils';
import TabBar_Title from '../../component/TabBar_Title';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import RNPickerSelect from 'react-native-picker-select';
import Dialog from 'react-native-dialog';
import RequiredText from '../../component/RequiredText';

const Dropdown = [
  {
    value: 'NO',
    label: 'Nghỉ ốm, chữa bệnh, nghỉ dưỡng thai',
  },
  {
    value: 'TN',
    label: 'Nghỉ tai nạn',
  },
  {
    value: 'KL',
    label: 'Nghỉ không lương',
  },
  {
    value: 'NP',
    label: 'Nghỉ phép',
  },
  {
    value: 'VR',
    label: 'Nghỉ việc riêng có lương',
  },
];

const pickerSelectStyles = {
  inputIOS: {
    fontSize: 20,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 15, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    color: 'black',
    fontSize: 10,
  },
};
const statusObj = {
  'Chưa đặt hàng': {
    color: AppColors.UntreatedColor,
  },
  'Đã đặt hàng': {
    color: AppColors.Maincolor,
  },
  'Không mua được': {
    color: AppColors.PendingColor,
  },
  'Tồn đủ': {
    color: AppColors.PendingColor,
  },
  'Chưa duyệt': {
    color: AppColors.TextColorMSS,
  },
  'Hàng về': {
    color: AppColors.AcceptColor,
  },
};

class ItemPuschaseOderProgress2Component extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 10;
    this.TotalRow = 0;
    this.state = {
      check: false,
      txtComment: '',
      checkLogin: '',
      status: null,
      ApplyLeaveGuid: null,
      StartTime: null,
      EndTime: null,
      Type: null,
      Description: null,
      Total: null,
      FullName: null,
      loading: false,
      ApplyLeaveItem: [],
      listWofflowData: [],
      isModalVisible: false,
      isModalDelete: false,
      visible: false,
      TongNgayNghi: 0,
      TongDaNghiNV: 0,
      PhepConLai: 0,
      NewStatus: null,
    };
    this.listtabbarBotom = [
      {
        Title: 'Cập nhật trạng thái',
        Icon: 'checksquareo',
        Type: 'antdesign',
        Value: 'checksquareo',
        Checkbox: false,
        OffEditcolor: true,
      },
      {
        Title: 'Phân việc',
        Icon: 'user',
        Type: 'antdesign',
        Value: 'user',
        Checkbox: false,
        OffEditcolor: true,
      },
    ];
  }
  onPressback() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  keyExtractor = (item, index) => index.toString();
  renderItem = ({item}) => {
    return (
      <ListItem avatar>
        <Left>
          <Thumbnail
            style={{width: 40, height: 40}}
            source={API_HR.GetPicApplyLeaves(item.EmployeeAskGuid)}
          />
        </Left>
        <Body style={{marginTop: -3}}>
          <Text note numberOfLines={1}>
            Mã phiếu: {item.ApplyLeaveId}
          </Text>
          <Text note numberOfLines={1}>
            Từ ngày: {item.StartTimeString}
          </Text>
          <Text note numberOfLines={1}>
            Đến ngày: {item.EndTimeString}
          </Text>
          {/* <Text note numberOfLines={1}>
            Ý kiến: {item.Comment}
          </Text> */}
        </Body>
      </ListItem>
    );
  };
  getStatus = value => {
    if (!value) {
      return '';
    }
    let res = Dropdown.findIndex(i => i.value == value);
    if (res === -1) {
      return '';
    }
    return Dropdown[res].label;
  };
  handleNameStatus = () => {
    if (this.props.Data.StatusView) {
      return this.props.Data.StatusView;
    }
    return '';
  };
  handleColorStatus = () => {
    if (this.props.Data.StatusView) {
      return statusObj[this.props.Data.StatusView].color;
    }
    return 'yellow';
  };
  render() {
    const isEdit = this.props.Data.PermissEdit === 1 ? true : false;
    const IS_NV = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'NV';
    const ListEmp = this.props.ListEmp.map(emp => ({
      ...emp,
      label: emp.text,
    }));
    return (
      <View style={{backgroundColor: '#fff', flex: 1}}>
        <TabBar_Title
          title={'Chi tiết tiến độ đơn hàng'}
          callBack={() => this.onPressback()}
        />
        <ScrollView>
          <View style={{flexDirection: 'column', flex: 1}}>
            {/* Hình ảnh chân dung và tên */}
            <View style={{padding: 10}}>
              {/*<Text style={{fontWeight:'bold',paddingBottom:3,paddingTop:5}}>Thông tin chung</Text>*/}
              {/* <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={[AppStyles.Labeldefault]}>Mã TP :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault, { fontWeight: 'bold' }]}>
                    {this.props.Data.ItemIDTP}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={[AppStyles.Labeldefault]}>Tên TP :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.ItemNameTP}
                  </Text>
                </View>
              </View> */}
              {/* <View style={{ flexDirection: 'row', padding: 5 }}>
                <View style={{ flex: 1 }}>
                  <Text style={[AppStyles.Labeldefault]}>Mã SO :</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.SaleOrderNo}
                  </Text>
                </View>
              </View> */}
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Mã TP :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.ItemIDTP}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Tên TP :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.ItemNameTP}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Mã đơn hàng bán :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.SaleOrderNo}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Tên sản phẩm :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.ItemName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Số ĐNM :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.ProposedPurchaseId}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Tiêu chuẩn KT :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.Specification}
                  </Text>
                </View>
              </View>

              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>NCC :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.ObjectName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>SL ĐH :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.addPeriod(this.props.Data.Quantity)}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>SL ĐN :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.addPeriod(this.props.Data.QuantityDN)}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>ĐVT :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.UnitName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Ngày đặt :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.PodateString}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Ngày cần :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.DateNeedString}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Người đặt hàng :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.EmployeeName}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>
                    Quy cách chi tiết :
                  </Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.SpecificationDetail}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Quy cách phôi :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.SpecificationBillet}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Vật liệu :</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.Material}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Dự án PO:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.PONumber || ''}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Ngày về:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {this.props.Data.CreatedDateString || ''}
                  </Text>
                </View>
              </View>

              <View style={{flexDirection: 'row', padding: 5}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
                </View>
                <View style={{flex: 2}}>
                  {/* {this.props.Data.StatusCode == 'B' ? (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.AcceptColor},
                      ]}>
                      {this.props.Data.StatusView}
                    </Text>
                  ) : this.props.Data.StatusCode == 'A' ? (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.UntreatedColor},
                      ]}>
                      {this.props.Data.StatusView}
                    </Text>
                  ) : (
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: AppColors.PendingColor},
                      ]}>
                      {this.props.Data.StatusView}
                    </Text>
                  )} */}
                  {
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        {color: this.handleColorStatus()},
                      ]}>
                      {this.handleNameStatus()}
                    </Text>
                  }
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  onYes = () => {
    const {NewStatus} = this.state;
    if (!NewStatus) {
      Alert.alert('Thông báo', 'Yêu cầu chọn trạng thái', [{text: 'Đóng'}], {
        cancelable: true,
      });
      return;
    }
    let data = [
      {
        RowGuid: this.props.Data.RowGuid,
        Status: NewStatus,
      },
    ];
    API_ListProposePurchases.UpdateStatus(data)
      .then(res => {
        let errorCode = JSON.parse(res.data.errorCode);
        if (errorCode == 200) {
          this.setState({
            visible: false,
          });
          ToastAndroid.showWithGravity(
            'Cập nhật thành công',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
          this.onPressback();
        } else {
          ToastAndroid.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
        }
      })
      .catch(error => {
        this.setState({
          visible: true,
        });
      });
  };
  onAssign = () => {
    const {EmpAssign} = this.state;
    if (!EmpAssign) {
      Alert.alert('Thông báo', 'Yêu cầu chọn nhân viên', [{text: 'Đóng'}], {
        cancelable: true,
      });
      return;
    }
    // let data = [
    //   {
    //     RowGuid: this.props.Data.RowGuid,
    //     Status: EmpAssign,
    //   },
    // ];
    // API_ListProposePurchases.UpdateStatus(data)
    //   .then(res => {
    //     let errorCode = JSON.parse(res.data.errorCode);
    //     if (errorCode == 200) {
    //       this.setState({
    //         visible: false,
    //       });
    //       ToastAndroid.showWithGravity(
    //         'Cập nhật thành công',
    //         ToastAndroid.SHORT,
    //         ToastAndroid.CENTER,
    //       );
    //       this.onPressback();
    //     } else {
    //       ToastAndroid.showWithGravity(
    //         'Có lỗi khi xử lý phiếu',
    //         ToastAndroid.SHORT,
    //         ToastAndroid.CENTER,
    //       );
    //     }
    //   })
    //   .catch(error => {
    //     this.setState({
    //       visible: true,
    //     });
    //   });
  };
  onClose = () => {
    this.setState({
      visible: false,
      visibleModalAssign: false,
    });
  };
  onChangeState = (key, value) => {
    this.setState({[key]: value});
  };
  onConfirm(data) {
    if (data === 'checkcircleo') {
      this.setState({
        visible: true,
      });
    } else {
      this.setState({
        visibleModalAssign: true,
      });
    }
    // if (this.state.model.Status === 'Y') {
    //   Alert.alert(
    //     'Thông báo',
    //     'Phiếu đã duyệt',
    //     [{text: 'OK', onPress: () => Keyboard.dismiss()}],
    //     {cancelable: false},
    //   );
    //   return;
    // }
  }
  submitWorkFlow(callback, CommentWF) {
    if (callback !== 'D') {
      this.saveKhongduyet(CommentWF);
      return;
    }
    let obj = {
      ApplyLeaveGuid: this.props.Data.ApplyLeaveGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
      DepartmentGuid: this.props.Data.DepartmentGuid,
      WorkFlowGuid: this.props.Data.WorkFlowGuid,
    };
    API_HR.approveApplyLeaves(obj)
      .then(response => {
        let errorCode = JSON.parse(JSON.stringify(response.data.errorCode));
        if (errorCode == 200) {
          ToastAndroid.showWithGravity(
            'Xử lý thành công',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
          this.onPressback();
        } else {
          ToastAndroid.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
        }
      })
      .catch(error => {
        console.log(error.data);
      });
  }

  callbackOpenUpdate = () => {
    Actions.applyLeavesadd({itemData: this.props.Data});
  };
  DeleteApplyLeavesById = () => {
    let obj = {
      ApplyLeaveGuid: this.props.RecordGuid,
    };
    API_HR.DeleteApplyLeavesById(obj)
      .then(response => {
        let errorCode = JSON.parse(response.data.errorCode);
        if (errorCode == 200) {
          this.setState({
            isModalDelete: !this.state.isModalDelete,
          });
          ToastAndroid.showWithGravity(
            'Xóa thành công',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
          this.onPressback();
        } else {
          ToastAndroid.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
        }
      })
      .catch(error => {
        console.log(error.data);
      });
  };

  saveKhongduyet(CommentWF) {
    let obj = {
      ApplyLeaveGuid: this.props.Data.ApplyLeaveGuid,
      LoginName: this.state.LoginName,
      Comment: CommentWF,
      WorkFlowGuid: this.props.Data.WorkFlowGuid,
    };
    API_HR.NotApproveApplyLeaves(obj)
      .then(response => {
        console.log(
          '==============ketquaKhongduỵet: ' +
            JSON.stringify(response.data.Data),
        );
        let errorCode = JSON.parse(JSON.stringify(response.data.errorCode));
        console.log('==============ketquaduỵet-errorCode: ' + errorCode);
        if (errorCode == 200) {
          ToastAndroid.showWithGravity(
            'Trả lại thành công',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
          this.onPressback();
        } else {
          ToastAndroid.showWithGravity(
            'Có lỗi khi xử lý phiếu',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
        }
      })
      .catch(error => {
        console.log(error.data);
      });
  }
  GetApplyLeavesById = Id => {
    const IS_NV = global.__appSIGNALR.SIGNALR_object.USER.LocationId === 'NV';
    let obj = {
      ApplyLeaveGuid: Id,
    };
    API_HR.GetApplyLeavesById(obj)
      .then(response => {
        let data = JSON.parse(response.data.data);
        data.StartTimeRead = this.getParsedDate(data.StartTime, 0);
        data.EndTimeRead = this.getParsedDate(data.EndTime, 0);
        this.setState({
          ApplyLeaveItem: data,
          Description: data.Description,
          listWofflowData: [],
        });
        if (!IS_NV) {
          this.getAllCommentPage(this.page);
        }
        let objCheck = {
          ApplyLeaveGuid: this.props.RecordGuid,
          LoginName: this.state.LoginName,
          WorkFlowGuid: this.props.Data.WorkFlowGuid,
        };
        API_HR.CheckLogin(objCheck)
          .then(response => {
            this.setState({
              checkLogin: response.data.data,
            });
          })
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log(error);
      });
  };
  _onRefresh = () => {
    this.setState({listWofflowData: []}, () => {
      this.getAllCommentPage(1);
    });
  };
  getAllCommentPage = Page => {
    this.page = Page;
    let obj = {
      Page: Page,
      ItemPage: this.Length,
      EmployeeAskGuid: this.props.Data.EmployeeAskGuid,
    };
    API_HR.CountApplyLeavesInYear(obj)
      .then(response => {
        let listData = this.state.listWofflowData;
        let data1 = JSON.parse(
          JSON.stringify(
            JSON.parse(JSON.parse(JSON.stringify(response.data)).data),
          ),
        );
        let data = listData.concat(data1);
        let TotalRow = JSON.parse(
          JSON.parse(JSON.stringify(response.data)).data,
        );
        this.TotalRow = TotalRow.Count;
        this.setState({listWofflowData: data});
      })
      .catch(error => {
        console.log(error.data);
      });
  };
  handleLoadMore = () => {
    if (this.TotalRow > this.state.listWofflowData.length) {
      this.page = this.page + 1;
      this.getAllCommentPage(this.page);
    }
  };
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <Text style={{color: '#000'}} />;
  };
  getParsedDate(strDate, data) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!
      var hours = date.getHours();
      var min = date.getMinutes();
      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (data == '1') {
        // "1":  dd-mm-yyy HH:mm
        if (hours < 10) {
          hours = '0' + hours;
        }
        if (min < 10) {
          min = '0' + min;
        }
        date = dd + '-' + mm + '-' + yyyy + ' ' + hours + ':' + min;
        return date.toString();
      } else {
        date = dd + '-' + mm + '-' + yyyy;
        return date.toString();
      }
    } else {
      return '';
    }
  }
}

const styles = StyleSheet.create({
  tabNavigatorItem: {
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemIcon: {
    fontWeight: '600',
    fontSize: 30,
  },
  itemIconClick: {
    fontWeight: '600',
    fontSize: 30,
    color: '#f6b801',
  },
  itemQR: {
    borderRadius: 15,
    backgroundColor: '#f6b801',
    fontWeight: '600',
    fontSize: 30,
    color: '#fff',
    padding: 10,
  },
  headline: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: AppColors.blueFacebook,
    fontSize: 18,
    marginTop: 8,
    height: 40,
    backgroundColor: AppColors.white,
  },
  container: {
    flex: 1,
    marginTop: '5%',
    paddingHorizontal: '5%',
  },
  fixToText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  button1: {
    borderColor: 'transparent',
    backgroundColor: AppColors.blueFacebook,
    width: '40%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ItemPuschaseOderProgress2Component);
