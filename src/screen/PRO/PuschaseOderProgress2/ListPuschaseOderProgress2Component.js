import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, Icon, ListItem, SearchBar, Badge} from 'react-native-elements';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import TabNavigator from 'react-native-tab-navigator';
import {API_ListProposePurchases} from '@network';
import {AppStyles, getStatusBarHeight, AppSizes, AppColors} from '@theme';
import TabBar from '../../component/TabBar';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import Combobox from '../../component/Combobox';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import {FuncCommon} from '../../../utils';
const ListStatus = [
  {
    value: 'ALL',
    text: 'Chọn tất cả',
  },
  {
    value: 'B',
    text: 'Đã đặt hàng',
  },
  {
    value: 'C',
    text: 'Quá hạn',
  },
  {
    value: 'D',
    text: 'Đến hạn',
  },
  {
    value: 'E',
    text: 'Đã nhận một phần',
  },
  {
    value: 'F',
    text: 'Hoàn tất',
  },
  {
    value: '2',
    text: 'Không mua được',
  },
  {
    value: '3',
    text: 'Tồn đủ',
  },
];
const SCREEN_WIDTH = Dimensions.get('window').width;
class ListFollowPurchaseOrderComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ValueSearchDate: '4', // 30 ngày trước
      model: {},
      list: [],
      isFetching: false,
      openSearch: false,
      ListEmp: [],
      dTypeOfRequest: [
        {
          value: 'ALL',
          text: 'Chọn tất cả',
        },
        {
          value: 'TM',
          text: 'Thương mại',
        },
        {
          value: 'GCN',
          text: 'Gia công ngoài',
        },
        {
          value: 'SX',
          text: 'Sản xuất',
        },
        {
          value: 'DV',
          text: 'CCDC',
        },
        {
          value: 'VPP',
          text: 'Văn phòng phẩm',
        },
      ],
    };
    this.staticParam = {
      CurrentPage: 1,
      Length: 10,
      search: {value: ''},
      QueryOrderBy: 'ProposedPurchaseID DESC',
      StartDate: new Date(),
      EndDate: new Date(),
      StatusCode: 'ALL',
      TypeOfRequest: 'ALL',
    };
    this.listtabbarBotom = [
      {
        Title: 'Chờ duyệt',
        Icon: 'profile',
        Type: 'antdesign',
        Value: 'profile',
        Checkbox: true,
      },
      {
        Title: 'Đã duyệt',
        Icon: 'checkcircleo',
        Type: 'antdesign',
        Value: 'checkcircleo',
        Checkbox: false,
      },
    ];
    this.Total = 0;
  }
  componentDidMount = () => {
    // this.getAllEmployee();
  };
  GetAll = () => {
    this.setState({refreshing: true}, () => {
      API_ListProposePurchases.PuschaseOderProgress2ListAll(this.staticParam)
        .then(res => {
          console.log(res);
          let _data = JSON.parse(res.data.data);
          if (!_data || _data.data.length === 0) {
            this.setState({refreshing: false});
            return;
          }
          let _list = _data.data;
          if (this.staticParam.CurrentPage == 1) {
            this.state.list = _list;
          } else {
            this.state.list = this.state.list.concat(_list);
          }

          this.Total = _data.recordsTotal || 0;
          this.listtabbarBotom[0].Badge = _data.countStt_W;
          this.setState({list: this.state.list, refreshing: false});
        })
        .catch(error => {
          console.log(error);
          this.setState({refreshing: false});
        });
    });
  };
  getAllEmployee = () => {
    // API_ListProposePurchases.getAllEmployee({})
    //   .then(res => {
    //     console.log(res);
    //     let _data = JSON.parse(res.data.data).map(emp => ({
    //       ...emp,
    //       value: emp.EmployeeId,
    //       text: emp.EmployeeId + '-' + emp.FullName
    //     }))
    //     this.setState({ ListEmp: _data || [] });
    //   })
    //   .catch(error => {
    //     console.log(error);
    //   });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.updateSearch('');
    }
  }
  ListEmpty = () => {
    if (this.state.list.length > 0) {
      return null;
    }
    return (
      <View style={AppStyles.centerAligned}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  _openComboboxType() {}
  openComboboxType = d => {
    this._openComboboxType = d;
  };
  onActionComboboxType() {
    this._openComboboxType();
  }
  _openComboboxEmp() {}
  openComboboxEmp = d => {
    this._openComboboxEmp = d;
  };
  onActionComboboxEmp() {
    this._openComboboxEmp();
  }
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.staticParam.StartDate = new Date(callback.start);
      this.staticParam.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.updateSearch('');
    }
  };
  setStartDate = date => {
    this.staticParam.StartDate = date;
    this.updateSearch('');
  };
  setEndDate = date => {
    this.staticParam.EndDate = date;
    this.updateSearch('');
  };
  updateSearch = text => {
    this.staticParam.search.value = text;
    this.staticParam.CurrentPage = 1;
    this.setState(
      {
        list: [],
      },
      () => {
        this.GetAll();
      },
    );
  };
  renderFooter = () => {
    if (!this.state.isFetching) {
      return null;
    }
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  handleLoadMore = () => {
    if (this.Total > this.state.list.length) {
      this.nextPage();
    }
  };
  render() {
    return (
      <View style={AppStyles.container}>
        <TabBar
          title={'Tiến độ đơn hàng'}
          FormSearch={true}
          CallbackFormSearch={callback => this.setState({openSearch: callback})}
          BackModuleByCode={'PRO'}
          // addForm={true}
          // CallbackFormAdd={() => Actions.bmitemAddProposedPurchase()}
        />
        {this.state.openSearch == true ? (
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  padding: 5,
                }}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian bắt đầu
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventStartDate: true})}>
                  <Text>
                    {FuncCommon.ConDate(this.staticParam.StartDate, 0)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                  Thời gian kết thúc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.setState({setEventEndDate: true})}>
                  <Text>{FuncCommon.ConDate(this.staticParam.EndDate, 0)}</Text>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'column', padding: 5}}>
                <Text
                  style={[
                    AppStyles.Labeldefault,
                    {textAlign: 'center'},
                    styles.timeHeader,
                  ]}>
                  Lọc
                </Text>
                <TouchableOpacity
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionSearchDate()}>
                  <Icon
                    name={'down'}
                    type={'antdesign'}
                    size={18}
                    color={AppColors.gray}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              style={[AppStyles.FormInput, {marginHorizontal: 10}]}
              onPress={() => this.onActionComboboxType()}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.staticParam.StatusCodeName
                    ? {color: 'black'}
                    : {color: AppColors.gray},
                ]}>
                {this.staticParam.StatusCodeName
                  ? this.staticParam.StatusCodeName
                  : 'Chọn trạng thái...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  color={AppColors.gray}
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                AppStyles.FormInput,
                {marginHorizontal: 10, marginTop: 5},
              ]}
              onPress={() => this.onActionComboboxEmp()}>
              <Text
                style={[
                  AppStyles.TextInput,
                  this.staticParam.TypeOfRequestName
                    ? {color: 'black'}
                    : {color: AppColors.gray},
                ]}>
                {this.staticParam.TypeOfRequestName
                  ? this.staticParam.TypeOfRequestName
                  : 'Chọn hình thức mua...'}
              </Text>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon
                  color={AppColors.gray}
                  name={'chevron-thin-down'}
                  type="entypo"
                  size={20}
                />
              </View>
            </TouchableOpacity>
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.updateSearch(text)}
              value={this.staticParam.search.value}
            />
          </View>
        ) : null}
        <View style={{flex: 14}}>
          <FlatList
            style={{height: '100%'}}
            refreshing={this.state.isFetching}
            ref={ref => {
              this.ListView_Ref = ref;
            }}
            ListFooterComponent={this.renderFooter}
            ListEmptyComponent={this.ListEmpty}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.list}
            renderItem={({item, index}) => (
              <ListItem
                title={() => {
                  return (
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <Text style={[{flex: 1}, AppStyles.Labeldefault]}>
                        {item.ProposedPurchaseId}
                      </Text>
                      <View
                        style={[
                          {
                            flex: 1,
                            textAlign: 'right',
                            justifyContent: 'flex-end',
                            alignItems: 'flex-end',
                          },
                        ]}>
                        {item.StatusView == 'Hàng về' ? (
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {color: AppColors.AcceptColor},
                            ]}>
                            {item.StatusView}
                          </Text>
                        ) : item.StatusView == 'Chưa đặt hàng' ? (
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {
                                color: AppColors.UntreatedColor,
                              },
                            ]}>
                            {item.StatusView}
                          </Text>
                        ) : (
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {color: AppColors.PendingColor},
                            ]}>
                            {item.StatusView}
                          </Text>
                        )}
                      </View>
                    </View>
                  );
                }}
                subtitle={() => {
                  return (
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 3}}>
                        <Text style={AppStyles.Textdefault}>
                          Tên sản phẩm:{' '}
                          {FuncCommon.handleLongText(item.ItemName, 50)}
                        </Text>
                        {/* <Text style={AppStyles.Textdefault}>
                          Tiêu chuẩn KT:{item.Specification}
                        </Text> */}
                        {/* <Text style={AppStyles.Textdefault}>
                          SL ĐH:{FuncCommon.addPeriod(item.Quantity)}
                        </Text> */}
                      </View>
                      <View
                        style={{
                          flex: 2,
                          alignItems: 'flex-end',
                          justifyContent: 'flex-start',
                        }}>
                        <Text style={AppStyles.Textdefault}>
                          {item.RequestDateString}
                        </Text>
                      </View>
                    </View>
                  );
                }}
                bottomDivider
                titleStyle={AppStyles.Titledefault}
                onPress={() => this.openView(item)}
              />
            )}
            onEndReachedThreshold={0.5}
            onEndReached={this.handleLoadMore}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
          {/* <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback => this.onClick(callback)}
            />
          </View> */}
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.updateSearch(''),
                    this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.staticParam.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {ListStatus.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeType}
              data={ListStatus}
              nameMenu={'Chọn trạng thái'}
              eOpen={this.openComboboxType}
              position={'bottom'}
              value={undefined}
            />
          ) : null}
          {this.state.dTypeOfRequest.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeEmp}
              data={this.state.dTypeOfRequest}
              nameMenu={'Chọn loại đơn hàng'}
              eOpen={this.openComboboxEmp}
              position={'bottom'}
              value={undefined}
            />
          ) : null}
        </View>
      </View>
    );
  }

  ChangeType = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.staticParam.StatusCode = rs.value;
    this.staticParam.StatusCodeName = rs.text;
    this.updateSearch('');
  };
  ChangeEmp = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.staticParam.TypeOfRequest = rs.value;
    this.staticParam.TypeOfRequestName = rs.text;
    this.updateSearch('');
  };

  nextPage() {
    this.staticParam.CurrentPage++;
    this.GetAll();
  }
  //Load lại
  onRefresh = () => {
    this.setState({
      list: [],
    });
    this.updateSearch('');
  };
  onClick(data) {
    if (data == 'checkcircleo') {
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.search.value = '';
      this.staticParam.Status = 'Y';
      this.updateSearch('');
    } else if (data == 'profile') {
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.search.value = '';
      this.staticParam.Status = 'W';
      this.updateSearch('');
    } else if (data == 'export2') {
      this.staticParam.CurrentPage = 1;
      this.staticParam.Length = 25;
      this.staticParam.search.value = '';
      this.staticParam.Status = 'M';
      this.updateSearch('');
    } else if (data == 'combobox') {
      this.onActionCB();
    } else if (data === 'home') {
      Actions.app();
    }
  }
  //Lọc loại đề nghị
  onValueChange = value => {
    this.state.list = [];
    this.staticParam.CurrentPage = 1;
    this.staticParam.Length = 25;
    this.staticParam.search.value = '';
    this.staticParam.Type = value.value;
    this.setState({
      list: this.state.list,
    });
    this.GetAll();
  };
  OnChangeStatus() {
    this.state.list = [];
    this.staticParam.CurrentPage = 1;
    this.staticParam.Length = 25;
    this.staticParam.search.value = '';
    this.staticParam.Status = 'SX';
    this.setState({
      staticParam: {
        CurrentPage: this.staticParam.CurrentPage,
        Length: this.staticParam.Length,
        search: this.staticParam.search,
        QueryOrderBy: this.staticParam.QueryOrderBy,
        StartDate: this.staticParam.StartDate,
        EndDate: this.staticParam.EndDate,
        Type: this.staticParam.Type,
        Status: this.staticParam.Status,
      },
    });
    this.GetAll();
  }
  _openCombobox() {}
  openCombobox = d => {
    this._openCombobox = d;
  };
  onActionCB(item) {
    this._openCombobox();
  }
  //định dạng ngày
  customDate(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = dd + '/' + mm + '/' + yyyy;
      return date.toString();
    } else {
      return '';
    }
  }
  openView(data) {
    Actions.ItemPuschaseOderProgress2({
      Data: data,
      ListEmp: this.state.ListEmp,
    });
  }
}

const styles = StyleSheet.create({
  TabarPending: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabarApprove: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 80,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TabCenter: {
    flex: 1,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 0.5,
    paddingTop: 10,
    paddingBottom: 10,
  },
});
//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListFollowPurchaseOrderComponent);
