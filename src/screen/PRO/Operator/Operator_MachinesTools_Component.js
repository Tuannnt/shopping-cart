import React, { Component } from 'react';
import { FlatList, Keyboard, Animated, Text, TouchableWithoutFeedback, RefreshControl, Image, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../network";
import TabBar_Title from "../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../utils';
import { ScrollView } from 'react-native-gesture-handler';

const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_MachinesTools_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            Data_JTableMachines: {
                length: 20,
                search: {
                    value: ""
                },
                NumberPage: 0,
                lstDepartmentID: []
            },
            Data_JTableTools: {
                length: 20,
                search: {
                    value: ""
                },
                NumberPage: 0,
            },
            ListMachines: [],
            ListTools: [],
        };
        //#region tabbarbottom
        this.listtabbarBotom = [
            {
                Title: "Báo hỏng",
                Icon: "server",
                Type: "feather",
                Value: "0",
                Checkbox: true,
                Badge: 0
            },
            {
                Title: "QL Cơ điện",
                Icon: "server",
                Type: "feather",
                Value: "1",
                Checkbox: false,
                Badge: 0
            },
            {
                Title: "Update tình trạng",
                Icon: "server",
                Type: "feather",
                Value: "4",
                Checkbox: false,
                Badge: 0
            }
        ];
        //#endregion
    }
    componentDidMount() {
        this.JTableMachines(true);
        this.JTableTools(true);
    }
    JTableMachines = (s) => {
        if (s === true) {
            this.state.Data_JTableMachines.NumberPage = 0;
            this.setState(
                {
                    loadingsearch: true,
                    ListMachines: [],
                }
            );
        }
        this.state.Data_JTableMachines.NumberPage = ++this.state.Data_JTableMachines.NumberPage
        this.setState({ Data_JTableMachines: this.state.Data_JTableMachines });
        API_Operator.JTableMachines(this.state.Data_JTableMachines).then(res => {
            var data = JSON.parse(res.data.data).data;
            for (let i = 0; i < data.length; i++) {
                this.state.ListMachines.push(data[i]);
            }
            this.setState({
                ListMachines: this.state.ListMachines,
                loading: false,
                loadingsearch: false
            });
        })
            .catch(error => {
                this.setState({ loading: false, loadingsearch: false });
                console.log(error);
            });
    }
    JTableTools = (s) => {
        if (s === true) {
            this.state.Data_JTableTools.NumberPage = 0;
            this.setState(
                {
                    loadingsearch: true,
                    ListTools: [],
                }
            );
        }
        this.state.Data_JTableTools.NumberPage = ++this.state.Data_JTableTools.NumberPage
        this.setState({ Data_JTableTools: this.state.Data_JTableTools });
        API_Operator.JTableTools(this.state.Data_JTableTools).then(res => {
            var data = JSON.parse(res.data.data).data;
            for (let i = 0; i < data.length; i++) {
                this.state.ListTools.push(data[i]);
            }
            this.setState({
                ListTools: this.state.ListTools,
                loading: false,
                loadingsearch: false
            });
        })
            .catch(error => {
                this.setState({ loading: false, loadingsearch: false });
                console.log(error);
            });
    }
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"Máy móc"}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        <View style={{ flex: 1 }}>
                            <Text style={[AppStyles.Labeldefault, { padding: 10 }]}>Máy móc:</Text>
                            <Divider />
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã máy</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên máy</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>TG cần</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>TG có</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Trạng thái</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListMachines.length > 0 ?
                                        <FlatList
                                            data={this.state.ListMachines}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.MarchineId}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.MarchineName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.RequireTime}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.ProductionTime}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.Status}</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            onEndReached={() => this.state.ListMachines.length >= (20 * this.state.Data_JTableMachines.NumberPage) ? this.JTableMachines(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        // refreshControl={
                                        //     <RefreshControl
                                        //         refreshing={this.state.loading}
                                        //         onRefresh={this._onRefresh}
                                        //         tintColor="#f5821f"
                                        //         titleColor="#fff"
                                        //         colors={[AppColors.Maincolor]}
                                        //     />
                                        // }
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={[AppStyles.Labeldefault, { padding: 10 }]}>Công cụ dụng cụ:</Text>
                            <Divider />
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã CCDC</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên CCDC</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>Bàn giao</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>Còn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Trạng thái</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListTools.length > 0 ?
                                        <FlatList
                                            data={this.state.ListTools}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemId}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.ExportQuantity}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.InventoryQuantity}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.Status}</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            onEndReached={() => this.state.ListTools.length >= (20 * this.state.Data_JTableTools.NumberPage) ? this.JTableTools(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        // refreshControl={
                                        //     <RefreshControl
                                        //         refreshing={this.state.loading}
                                        //         onRefresh={this._onRefresh}
                                        //         tintColor="#f5821f"
                                        //         titleColor="#fff"
                                        //         colors={[AppColors.Maincolor]}
                                        //     />
                                        // }
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={AppStyles.StyleTabvarBottom}>
                            <TabBarBottomCustom
                                ListData={this.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    CallbackValueBottom = (callback) => {

    }

    //Các hàm dùng chung
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_MachinesTools_Component);
