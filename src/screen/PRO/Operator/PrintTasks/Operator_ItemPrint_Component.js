import React, { Component } from 'react';
import { FlatList, Keyboard, TextInput, ScrollView, Text, TouchableWithoutFeedback, Animated, Image, View, Dimensions, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import { FuncCommon } from '../../../../utils';
import Combobox from '../../../component/Combobox';
import ViewQRCode from '../../../component/ViewQRCode';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
const Level = [
    { value: 'CA1', text: 'Ca 1' },
    { value: 'CA2', text: 'Ca 2' },
    { value: 'CA3', text: 'Ca 3' },
]
class Operator_ItemPrint_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataQR: "",
            loading: false,
            DataView: null,
            listEMP: [],
            listMachines: [],
            ListTable: [],
            DataTable: {
                NumberPage: 0,
                Length: 10,
                PlanDayGuid: this.props.PlanGuid
            },
            CheckAll: false,
            //Hiển phiếu nhận/ giao việc
            ViewForm: false,
            transIcon_ViewForm: new Animated.Value(0),
            //trạng thái công việc
            ViewTable: false,
            transIcon_ViewTable: new Animated.Value(0),
        };
        //#region tabbarbottom
        this.listtabbarBotom = [
            {
                Title: "Tạm dừng/Huỷ",
                Icon: "pause-circle",
                Type: "feather",
                Value: "0",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            },
            // {
            //     Title: "Thêm ghi chú",
            //     Icon: "comment",
            //     Type: "evilicons",
            //     Value: "1",
            //     Checkbox: false,
            //     OffEditcolor: true,
            //     Badge: 0
            // },
            // {
            //     Title: "In nhóm phiếu",
            //     Icon: "printer",
            //     Type: "feather",
            //     Value: "4",
            //     Checkbox: false,
            //     OffEditcolor: true,
            //     Badge: 0
            // }
        ];
        //#endregion
    }
    componentDidMount() {
        this.setState({ loading: true })
        this.GetItemPrint();
        this.GetEmployeeSanXuat();
        this.GetMachines();
        this.Skill();
        this.setViewOpen("ViewForm")
    }
    //#region Skill
    Skill = () => {
        //#region đóng mở tab
        this.Animated_on_ViewForm = Animated.timing(this.state.transIcon_ViewForm, { toValue: 0, duration: 333 });
        this.Animated_off_ViewForm = Animated.timing(this.state.transIcon_ViewForm, { toValue: 1, duration: 333 });
        this.Animated_on_ViewTable = Animated.timing(this.state.transIcon_ViewTable, { toValue: 0, duration: 333 });
        this.Animated_off_ViewTable = Animated.timing(this.state.transIcon_ViewTable, { toValue: 1, duration: 333 });
        //#endregion
    }
    //#endregion

    //#region GetItemPrint
    GetItemPrint = () => {
        var obj = {
            Guid: this.props.PlanGuid
        }
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetItemPrint(obj).then(res => {
                    FuncCommon.Data_Offline_Set('GetItemPrint' + this.props.PlanGuid, res);
                    this._GetItemPrint(res);
                })
                    .catch(error => {
                        this.setState({ loading: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('GetItemPrint' + this.props.PlanGuid);
                this._GetItemPrint(x);
            }
        });
    }
    _GetItemPrint = (res) => {
        var data = JSON.parse(res.data.data);
        this.state.DataView = data.Header;
        this.state.DataView.MachineId = data.Header.MachineId === null ? "" : data.Header.MachineId;
        this.state.DataView.JobCardId = data.JobCardId;
        this.state.DataView.ImageUrlView = this.props.UrlImage;
        this.state.DataView.StartTime = null;
        this.state.DataView.EndTime = null;
        this.state.DataView.EmployeeGuid = "";
        this.state.DataView.EmployeeID = null;
        this.state.DataView.EmployeeName = data.Header.EmployeeName === undefined ? "" : data.Header.EmployeeName;
        this.state.DataView.DeparmentGuid = null;
        this.state.DataView.DepartmentID = null;
        this.state.DataView.SupportorGuid = "";
        this.state.DataView.SupportorID = null;
        this.state.DataView.SupportorName = data.Header.SupportorName === undefined ? "" : data.Header.SupportorName;
        this.state.DataView.MarchineName = "";
        this.state.DataView.ShiftID = "";
        this.state.DataView.ShiftName = "";
        this.state.DataView.QuantityPlan = data.Header.Quantity - data.QuantityResources;
        this.state.DataView.DatePrinrSanXuat = FuncCommon.ConDate(this.addMinutes(new Date(), data.Header.ProductionTime), 1);
        this.state.DataView.QuantityPlanMaximum = data.Header.Quantity - data.QuantityResources;
        this.state.DataView.ItemNameView = (data.Header.PartItemName === "" || data.Header.PartItemName === null) ? data.Header.ItemName : data.Header.PartItemName;
        this.state.DataView.TimeOnQuantity = data.Header.ProductionTime / (data.Header.Quantity - data.QuantityResources);
        this.setState({
            DataView: this.state.DataView,
            loading: false
        });
    }
    //#endregion

    //#region GetEmployeeSanXuat
    GetEmployeeSanXuat = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetEmployeeSanXuat().then(res => {
                    FuncCommon.Data_Offline_Set('GetEmployeeSanXuat_Operator', res);
                    this._GetEmployeeSanXuat(res);
                })
                    .catch(error => {
                        this.setState({ loading: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('GetEmployeeSanXuat_Operator');
                this._GetEmployeeSanXuat(x);
            }
        });
    }
    _GetEmployeeSanXuat = (res) => {
        var list = [];
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
            list.push({
                value: data[i].EmployeeGuid,
                text: "[ " + data[i].EmployeeID + " ]-" + data[i].FullName,
                EmployeeID: data[i].EmployeeID,
                DeparmentGuid: data[i].DeparmentGuid,
                DepartmentID: data[i].DepartmentID
            })
        }
        this.setState({
            listEMP: list,
            loading: false
        });
    }
    //#endregion

    //#region GetMachines
    GetMachines = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetMachines().then(res => {
                    FuncCommon.Data_Offline_Set('GetMachines_Operator', res);
                    this._GetMachines(res);
                })
                    .catch(error => {
                        this.setState({ loading: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('GetMachines_Operator');
                this._GetMachines(x);
            }
        });
    }
    _GetMachines = (res) => {
        var list = [];
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
            list.push({
                value: data[i].MarchineId,
                text: "[ " + data[i].MarchineId + " ]-" + data[i].MarchineName,
                OrganizationGuid: data[i].OrganizationGuid,
            })
        }
        this.setState({
            listMachines: list,
            loading: false
        });
    }
    //#endregion
    addMinutes = (date, minutes) => {
        return new Date(date.getTime() + minutes * 60000);
    };
    setEventTable = (s) => {
        if (this.state.ViewTable === false) {
            this.APIJtable(s);
        }
        this.setViewOpen("ViewTable");
    }
    APIJtable = (s) => {
        if (s === false) {
            this.state.DataTable.NumberPage = 0;
        }
        this.state.DataTable.NumberPage = ++this.state.DataTable.NumberPage;
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTablePlanJobCards(this.state.DataTable).then(res => {
                    FuncCommon.Data_Offline_Set('JTablePlanJobCards', res);
                    this._GetJTablePlanJobCards(res);
                }).catch(error => {
                    console.log(error);
                });

            } else {
                var x = await FuncCommon.Data_Offline_Get('JTablePlanJobCards');
                this._GetJTablePlanJobCards(x);
            }
        });

    }
    _GetJTablePlanJobCards = (res) => {
        var list = [];
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            data[i].CheckBox = false;
            list.push(data[i]);
        }
        this.setState({
            ListTable: list,
            loading: false
        });
    }
    render() {
        const rotateStart_ViewForm = this.state.transIcon_ViewForm.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '180deg']
        })
        const rotateStart_ViewTable = this.state.transIcon_ViewTable.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '180deg']
        })
        return (
            <TouchableWithoutFeedback
                style={{ flex: 1 }}
                onPress={() => {
                    Keyboard.dismiss();
                }}>
                <View style={[AppStyles.container]}>
                    <TabBar_Title
                        title={"In phiếu công việc"}
                        callBack={() => this.callBackList()} />
                    <Divider />
                    <ScrollView>
                        <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }} onPress={() => this.setViewOpen("ViewForm")}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={AppStyles.Labeldefault}>PHIẾU NHẬN/GIAO VIỆC</Text>
                            </View>
                            <Animated.View style={{ transform: [{ rotate: rotateStart_ViewForm }, { perspective: 4000 }], paddingRight: 10, paddingLeft: 10 }}>
                                <Icon type={"feather"} name={"chevron-down"} />
                            </Animated.View>
                        </TouchableOpacity>
                        <Divider />
                        {this.state.ViewForm === true ?
                            <View>
                                <View style={[AppStyles.containerCentered, , { flexDirection: 'row', padding: 10 }]}>
                                    <View style={[AppStyles.containerCentered, { flex: 1 }]}>
                                        {this.state.DataView !== null ?
                                            <Image style={{ width: 100, height: 100 }} source={{ uri: this.state.DataView.ImageUrlView }} />
                                            : null}
                                    </View>
                                </View>
                                <Divider />
                                {/* Table */}
                                <ScrollView horizontal={true}>
                                    <View style={{ flexDirection: 'column', flex: 1 }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Nội dung</Text></TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_th, { width: 250 }]}><Text style={AppStyles.Labeldefault}>Scan mã</Text></TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_th, { width: 250 }]}><Text style={AppStyles.Labeldefault}>Tên thành phẩm/ Bán thành phẩm</Text></TouchableOpacity>
                                        </View>
                                        {this.state.DataView !== null ?
                                            <ScrollView>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>Mã công việc</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250 }]}><Text style={[AppStyles.Textdefault]}>{this.state.DataView.OrderPlanDay}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250 }]}><Text style={[AppStyles.Textdefault]}>{this.state.DataView.ItemName}</Text></TouchableOpacity>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>Số lượng/CĐ</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250, flexDirection: 'row' }]}>
                                                        <View style={{ flex: 1 }}>
                                                            <TextInput style={AppStyles.FormInput}
                                                                underlineColorAndroid="transparent"
                                                                value={this.state.DataView.QuantityPlan.toString()}
                                                                placeholder={'Nhập số lượng . . .'}
                                                                // autoFocus={true}
                                                                autoCapitalize="none"
                                                                onChangeText={text =>
                                                                    this.setQuantityPlan(text)
                                                                } />
                                                        </View>
                                                        <View style={{ marginLeft: 10, justifyContent: 'center' }}><Text style={[AppStyles.Textdefault]}>{this.state.DataView.UnitName}</Text></View>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250 }]}><Text style={[AppStyles.Textdefault]}>{this.state.DataView.LocationName}</Text></TouchableOpacity>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>Người làm</Text></TouchableOpacity>
                                                    <View style={[AppStyles.table_td, { width: 250 }]}>
                                                        <TouchableOpacity style={AppStyles.FormInput} onPress={() => this.onActionComboboxEmployeeSX()}>
                                                            <Text style={[AppStyles.Textdefault, this.state.DataView.EmployeeName === "" ? { color: AppColors.gray } : { color: AppColors.black }]}>
                                                                {this.state.DataView.EmployeeName !== "" ?
                                                                    this.state.DataView.EmployeeName
                                                                    : "Chọn người làm..."}
                                                            </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250 }]}><Text style={[AppStyles.Textdefault]}>{this.state.DataView.EmployeeName}</Text></TouchableOpacity>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>Người hỗ trợ</Text></TouchableOpacity>
                                                    <View style={[AppStyles.table_td, { width: 250 }]}>
                                                        <TouchableOpacity style={AppStyles.FormInput} onPress={() => this.onActionComboboxEmployeeHT()}>
                                                            <Text style={[AppStyles.Textdefault, this.state.DataView.SupportorName === "" ? { color: AppColors.gray } : { color: AppColors.black }]}>
                                                                {this.state.DataView.SupportorName !== "" ?
                                                                    this.state.DataView.SupportorName
                                                                    : "Chọn người hỗ trợ..."}
                                                            </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250 }]}><Text style={[AppStyles.Textdefault]}>{this.state.DataView.SupportorName}</Text></TouchableOpacity>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>Máy-vị trí</Text></TouchableOpacity>
                                                    <View style={[AppStyles.table_td, { width: 250 }]}>
                                                        <TouchableOpacity style={AppStyles.FormInput} onPress={() => this.onActionComboboxMachines()}>
                                                            <Text style={[AppStyles.Textdefault, this.state.DataView.MarchineName === "" ? { color: AppColors.gray } : { color: AppColors.black }]}>
                                                                {this.state.DataView.MarchineName !== "" ?
                                                                    this.state.DataView.MarchineName
                                                                    : "Chọn máy..."}
                                                            </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <View style={[AppStyles.table_td, { width: 250 }]}>
                                                        <TouchableOpacity style={AppStyles.FormInput} onPress={() => this.onActionComboboxShift()}>
                                                            <Text style={[AppStyles.Textdefault, this.state.DataView.ShiftName === "" ? { color: AppColors.gray } : { color: AppColors.black }]}>
                                                                {this.state.DataView.ShiftName !== "" ?
                                                                    this.state.DataView.ShiftName
                                                                    : "Chọn ca làm việc..."}
                                                            </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>Tổng thời gian</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250 }]}><Text style={[AppStyles.Textdefault]}>{this.state.DataView.ProductionTime}(phút)</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250 }]}><Text style={[AppStyles.Textdefault]}>Dự kiến HT: {this.state.DataView.DatePrinrSanXuat}</Text></TouchableOpacity>
                                                </View>
                                            </ScrollView>
                                            : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                        }
                                    </View>
                                </ScrollView>
                                <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.Submit()}>
                                    {/* <Icon type={"antdesign"} name={"save"} /> */}
                                    <Text style={[AppStyles.Textdefault, { color: 'black' }]}>LƯU</Text>
                                </TouchableOpacity>
                            </View>
                            : null
                        }
                        <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }} onPress={() => this.setEventTable(false)}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={AppStyles.Labeldefault}>TRẠNG THÁI CÔNG VIỆC</Text>
                                {this.state.DataView !== null ?
                                    <Text style={AppStyles.Labeldefault}>{this.state.DataView.ItemName}</Text>
                                    : null}
                            </View>
                            <Animated.View style={{ transform: [{ rotate: rotateStart_ViewTable }, { perspective: 4000 }], paddingRight: 10, paddingLeft: 10 }}>
                                <Icon type={"feather"} name={"chevron-down"} />
                            </Animated.View>
                        </TouchableOpacity>
                        <Divider />
                        {/* Table */}
                        {this.state.ViewTable === true ?
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]} onPress={() => this.setCheckAll()}>
                                            {this.state.CheckAll ?
                                                <Icon
                                                    name={"check"}
                                                    type="entypo"
                                                    color={AppColors.ColorEdit}
                                                    size={20} />
                                                :
                                                <Icon
                                                    name={"square-o"}
                                                    type="font-awesome"
                                                    color={AppColors.IconcolorTabbar}
                                                    size={20} />
                                            }
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Mã phiếu</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Số SO</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 250 }]}><Text style={AppStyles.Labeldefault}>Tên TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Công đoạn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Người làm</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Ca</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Bắt đầu</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>SL KH</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>SL SX</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Còn lại</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>% HT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Điều khiển</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListTable.length > 0 ?
                                        <FlatList
                                            data={this.state.ListTable}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => this.CheckBoxItem(index)}>
                                                        {item.CheckBox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                        }
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 150 }]} onPress={() => this.onActionopenQR(item.JobCardId)}><Text style={[AppStyles.Textdefault, { color: 'blue' }]}>{item.JobCardId}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.OrderNumber}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 250 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.LocationName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.ShiftName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{(item.StartTime === "" || item.StartTime === null || item.StartTime === undefined) ? "Chưa in" : this.convertDate24h(item.StartTime)}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.Quantity}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.QuantityOk}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{this.qtyOk(item)}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{this.per(item)}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 150 }]}><Icon name={"delete"} type={"antdesign"} /></TouchableOpacity>
                                                </View>
                                            )}
                                            onEndReached={() => this.state.ListTable.length >= (10 * this.state.DataTable.NumberPage) ? this.APIJtable(true) : null}
                                            keyExtractor={(rs, index) => index.toString()}

                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                            : null}
                    </ScrollView>
                    <View style={AppStyles.StyleTabvarBottom}>
                        <TabBarBottomCustom
                            ListData={this.listtabbarBotom}
                            onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                        />
                    </View>
                    {this.state.DataView !== null && this.state.listEMP.length > 0 ?
                        <Combobox
                            value={this.state.DataView.EmployeeGuid}
                            TypeSelect={"single"}// single or multiple
                            callback={this.ChangeEmployeeSX}
                            data={this.state.listEMP}
                            nameMenu={'Chọn người làm'}
                            eOpen={this.openComboboxEmployeeSX}
                            position={'bottom'}
                        />
                        : null}
                    {this.state.DataView !== null && this.state.listEMP.length > 0 ?
                        <Combobox
                            value={this.state.DataView.SupportorGuid}
                            TypeSelect={"single"}// single or multiple
                            callback={this.ChangeEmployeeHT}
                            data={this.state.listEMP}
                            nameMenu={'Chọn người hỗ trợ'}
                            eOpen={this.openComboboxEmployeeHT}
                            position={'bottom'}
                        />
                        : null}
                    {this.state.DataView !== null && this.state.listMachines.length > 0 ?
                        <Combobox
                            value={this.state.DataView.MachineId}
                            TypeSelect={"single"}// single or multiple
                            callback={this.ChangeMachines}
                            data={this.state.listMachines}
                            nameMenu={'Chọn máy'}
                            eOpen={this.openComboboxMachines}
                            position={'bottom'}
                        />
                        : null}
                    {this.state.DataView !== null ?
                        <Combobox
                            value={this.state.DataView.ShiftID}
                            TypeSelect={"single"}// single or multiple
                            callback={this.ChangeShift}
                            data={Level}
                            nameMenu={'Chọn ca làm việc'}
                            eOpen={this.openComboboxShift}
                            position={'bottom'}
                        />
                        : null}
                    <ViewQRCode
                        data={this.state.dataQR}
                        eOpen={this.openQR}
                    />
                </View>
            </TouchableWithoutFeedback >

        );
    }
    CallbackValueBottom = (key) => {
        var list = this.state.ListTable;
        var ListSub = [];
        for (let i = 0; i < list.length; i++) {
            if (list[i].CheckBox === true) {
                ListSub.push(list[i].RowGuid);
            }
        };
        switch (key) {
            case "0":
                if (ListSub.length > 0) {
                    Alert.alert("Xác nhận",
                    "Bạn có muốn Tạm dừng - Chuyển kế hoạch không?",
                    [
                        { text: "Xác nhận", onPress: () => this.APIPlanJobCardsCancel(ListSub) },
                        {
                            text: "Đóng",
                            onPress: () => null,
                            style: "cancel"
                        }
    
                    ],
                    { cancelable: false });
                } else {
                    Alert.alert("Thông báo", "Yêu cầu chọn công việc.");
                }
                break;
            default:
                break;
        }
    }
    APIPlanJobCardsCancel = (listSub) => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.PlanJobCardsCancel(listSub).then(res => {
                    if (res.data.errorCode !== 200) {
                        Alert.alert("Thông báo", res.data.message);
                    } else {
                        Alert.alert("Thông báo", res.data.message);
                        this.callBackList();
                    }
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                Alert.alert("Thông báo", "Yêu cầu kết nối Internet");
            }
        });
    }
    //#region CheckAll
    setCheckAll = () => {
        var list = this.state.ListTable;
        for (let i = 0; i < list.length; i++) {
            list[i].CheckBox = !this.state.CheckAll
        };
        this.setState({ ListTable: list, CheckAll: !this.state.CheckAll });
    }
    CheckBoxItem = (index) => {
        this.state.ListTable[index].CheckBox = !this.state.ListTable[index].CheckBox;
        var obj = this.state.ListTable.find(x => x.CheckBox === false);
        if (obj === undefined) {
            this.state.CheckAll = true;
        } else {
            this.state.CheckAll = false;
        }
        this.setState({ ListTable: this.state.ListTable, CheckAll: this.state.CheckAll });
    }
    //#endregion

    //#region  ViewQRCode
    _openQR() { }
    openQR = (d) => {
        this._openQR = d;
    }
    onActionopenQR(qr) {
        this.setState({ dataQR: qr });
        this._openQR();
    }
    //#endregion
    //#region seQuantityPlan
    setQuantityPlan = (text) => {
        this.state.DataView.QuantityPlan = text;
        this.setState({ DataView: this.state.DataView });
    }
    //#endregion
    //#region đóng mở các tab
    setViewOpen = (val) => {
        this.setState({
            ViewForm: false,
            ViewTable: false,
        });
        if (val === "ViewForm") {
            this.setState({ ViewForm: !this.state.ViewForm });
            if (this.state.ViewForm === false) {
                Animated.sequence([this.Animated_on_ViewForm]).start();
                Animated.sequence([this.Animated_off_ViewTable]).start();
            } else {
                Animated.sequence([this.Animated_off_ViewForm]).start();
            }
        } else
            if (val === "ViewTable") {
                this.setState({ ViewTable: !this.state.ViewTable });
                if (this.state.ViewTable === false) {
                    Animated.sequence([this.Animated_on_ViewTable]).start();
                    Animated.sequence([this.Animated_off_ViewForm]).start();
                } else {
                    Animated.sequence([this.Animated_off_ViewTable]).start();
                }
            }

    }
    //#endregion
    //#region  combobox EmployeeSX
    _openComboboxEmployeeSX() { }
    openComboboxEmployeeSX = (d) => {
        this._openComboboxEmployeeSX = d;
    }
    onActionComboboxEmployeeSX() {
        this._openComboboxEmployeeSX();
    }
    ChangeEmployeeSX = (rs) => {
        if (rs !== null) {
            this.state.DataView.EmployeeGuid = rs.value;
            this.state.DataView.EmployeeName = rs.text.split("]-")[1];
            this.state.DataView.EmployeeID = rs.EmployeeID;
            this.state.DataView.DepartmentID = rs.DepartmentID;
            this.state.DataView.DeparmentGuid = rs.DeparmentGuid;
            this.setState({ DataView: this.state.DataView })
        }

    }
    //#endregion
    //#region  combobox EmployeeHT
    _openComboboxEmployeeHT() { }
    openComboboxEmployeeHT = (d) => {
        this._openComboboxEmployeeHT = d;
    }
    onActionComboboxEmployeeHT() {
        this._openComboboxEmployeeHT();
    }
    ChangeEmployeeHT = (rs) => {
        if (rs !== null) {
            this.state.DataView.SupportorGuid = rs.value;
            this.state.DataView.SupportorName = rs.text.split("]-")[1];
            this.state.DataView.SupportorID = rs.EmployeeID;
            this.setState({ DataView: this.state.DataView })
        }

    }
    //#endregion
    //#region  combobox Machines
    _openComboboxMachines() { }
    openComboboxMachines = (d) => {
        this._openComboboxMachines = d;
    }
    onActionComboboxMachines() {
        this._openComboboxMachines();
    }
    ChangeMachines = (rs) => {
        if (rs !== null) {
            this.state.DataView.MachineId = rs.value;
            this.state.DataView.MarchineName = rs.text.split("]-")[1];
            this.setState({ DataView: this.state.DataView })
        }
    }
    //#endregion
    //#region  combobox Shift
    _openComboboxShift() { }
    openComboboxShift = (d) => {
        this._openComboboxShift = d;
    }
    onActionComboboxShift() {
        this._openComboboxShift();
    }
    ChangeShift = (rs) => {
        if (rs !== null) {
            this.state.DataView.ShiftID = rs.value;
            this.state.DataView.ShiftName = rs.text;
            this.setState({ DataView: this.state.DataView })
        }

    }
    //#endregion
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
    convertDate24h = (datetime) => {
        if (datetime === "" || datetime === null || datetime === undefined) {
            return "";
        }
        var dateSplit = datetime.split(' ')[2];
        if (dateSplit === "CH" || dateSplit === "PM") {
            var hourse = parseInt(datetime.split(' ')[1].split(':')[0]);
            hourse = hourse + 12;
            if (hourse < 24) {
                return hourse + ":" + datetime.split(' ')[1].split(':')[1] + ":" + datetime.split(' ')[1].split(':')[2];
            }
            else {
                return "12:" + datetime.split(' ')[1].split(':')[1] + ":" + datetime.split(' ')[1].split(':')[2];
            }
        }
        else {
            return datetime.split(' ')[1];
        }
    };
    qtyOk = (full) => {
        var qtyOk = full.QuantityOk === "" ? 0 : parseFloat(full.QuantityOk);
        var qty = parseFloat(full.Quantity) - parseFloat(qtyOk);
        if (qty <= 0) {
            return 0;
        }
        return this.addPeriod(qty);
    }
    per = (full) => {
        var Quantity = full.Quantity === "" ? 0 : parseFloat(full.Quantity);
        var QuantityOk = full.QuantityOk === "" ? 0 : parseFloat(full.QuantityOk);
        var per = parseInt(QuantityOk / Quantity * 100);
        if (per < 100) {
            return this.addPeriod(per) + " %";
        }
        else {
            return this.addPeriod(per) + " %";
        }
    }

    //#region Submit
    Submit = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                this.state.DataView.QuantityPlan = parseInt(this.state.DataView.QuantityPlan);
                this.state.DataView.IsRun = true;
                this.setState({ DataView: this.state.DataView });
                API_Operator.InsertPlanJobCards(this.state.DataView).then(res => {
                    this.GetItemPrint();
                    this.setEventTable(false)
                })
                    .catch(error => {
                        this.setState({ loading: false });
                        console.log(error);
                    });
            } else {
                Alert.alert(
                    "Thông báo",
                    "Yêu cầu kết nối mạng",
                    [
                        { text: "Đóng", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                );
            }
        });
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_ItemPrint_Component);
