import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_Operator, API_HR} from '../../../../network';
import TabBar_Title from '../../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import {FuncCommon} from '../../../../utils';
import {ScrollView} from 'react-native-gesture-handler';
import Combobox from '../../../component/Combobox';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const ListStatus = [
  {value: 'OK', text: 'Hoạt động'},
  {value: 'HO', text: 'Hỏng'},
  {value: 'TL', text: 'Thất lạc'},
  {value: 'BD', text: 'Bảo dưỡng'},
];
class Operator_UpdateMachidnes_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      loadingsearch: false,
      Data_GetDataView: {
        length: 20,
        NumberPage: 0,
        search: {
          value: '',
        },
        LiGuid: props.value,
      },
      indexCbx: null,
      ListView: [],
    };
  }
  componentDidMount() {
    this.GetDataView(true);
  }
  GetDataView = s => {
    if (s === true) {
      this.state.Data_GetDataView.NumberPage = 0;
      this.setState({
        loadingsearch: true,
        ListView: [],
      });
    }
    this.state.Data_GetDataView.NumberPage = ++this.state.Data_GetDataView
      .NumberPage;
    this.setState({Data_GetDataView: this.state.Data_GetDataView});
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.LoadMachinesUpdate(this.state.Data_GetDataView)
          .then(res => {
            FuncCommon.Data_Offline_Set('LoadMachinesUpdate', res);
            this._GetLoadMachinesUpdate(res);
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('LoadMachinesUpdate');
        this._GetLoadMachinesUpdate(x);
      }
    });
  };
  _GetLoadMachinesUpdate = res => {
    var data = JSON.parse(res.data.data).data;
    for (let i = 0; i < data.length; i++) {
      this.state.ListView.push(data[i]);
    }
    this.setState({
      ListView: this.state.ListView,
      loading: false,
      loadingsearch: false,
    });
  };
  render() {
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar_Title
            title={'CẬP NHẬT TRẠNG THÁI MÁY MÓC'}
            callBack={() => this.callBackList()}
          />
          <Divider />
          <View style={{flex: 1}}>
            {/* Table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column', flex: 1}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Mã Máy</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Tên máy</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[AppStyles.table_th, {width: DRIVER.width}]}>
                    <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 120}]}>
                    <Text style={AppStyles.Labeldefault}>Trạng thái</Text>
                  </TouchableOpacity>
                </View>
                {this.state.ListView.length > 0 ? (
                  <FlatList
                    data={this.state.ListView}
                    renderItem={({item, index}) => (
                      <View
                        style={{flexDirection: 'row'}}
                        onPress={() => this.ViewItemPrint(item)}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'center'},
                            ]}>
                            {index + 1}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.MarchineId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.MarchineName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: DRIVER.width}]}>
                          <TextInput
                            style={AppStyles.FormInput}
                            underlineColorAndroid="transparent"
                            value={item.Description}
                            placeholder={'Nhập ghi chú'}
                            autoCapitalize="none"
                            onChangeText={text =>
                              this.setDescription(text, index)
                            }
                          />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 120}]}>
                          <TouchableOpacity
                            style={[
                              AppStyles.FormInput,
                              {justifyContent: 'center'},
                            ]}
                            onPress={() => {
                              this.onActionCombobox_Status(),
                                this.setState({indexCbx: index});
                            }}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.Status === 'OK'
                                ? 'Hoạt động'
                                : item.Status === 'HO'
                                ? 'Hỏng'
                                : item.Status === 'TL'
                                ? 'Thất lạc'
                                : item.Status === 'BD'
                                ? 'Bảo dưỡng'
                                : ''}
                            </Text>
                          </TouchableOpacity>
                        </TouchableOpacity>
                      </View>
                    )}
                    onEndReached={() =>
                      this.state.ListView.length >=
                      20 * this.state.Data_GetDataView.NumberPage
                        ? this.GetDataView(false)
                        : null
                    }
                    keyExtractor={(rs, index) => index.toString()}
                  />
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
          <TouchableOpacity
            style={[
              AppStyles.FormInput,
              {
                justifyContent: 'center',
                margin: 10,
                backgroundColor: '#bed9f6',
              },
            ]}
            onPress={() => this.UpdateMachidnes()}>
            <Text style={[AppStyles.Textdefault, {color: 'black'}]}>LƯU</Text>
          </TouchableOpacity>
          <Combobox
            value={''}
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeStatus}
            data={ListStatus}
            nameMenu={'Chọn trạng thái'}
            eOpen={this.openCombobox_Status}
            position={'bottom'}
          />
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  //Các hàm dùng chung
  ViewItemPrint = item => {
    Actions.Operator_ItemPrint({
      PlanGuid: item.PlanGuid,
      UrlImage: item.HINHANH,
    });
  };
  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Emp', ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null && val !== '') {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        var _string2 = string[1].substring(0, 2);
        if (_string2 !== '00') {
          return valview + ',' + string[1].substring(0, 2) === '00';
        } else {
          return valview;
        }
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  combobox Status
  _openCombobox_Status() {}
  openCombobox_Status = d => {
    this._openCombobox_Status = d;
  };
  onActionCombobox_Status() {
    this._openCombobox_Status();
  }
  ChangeStatus = rs => {
    if (rs !== null) {
      this.state.ListView[this.state.indexCbx].Status = rs.value;
      this.setState({ListView: this.state.ListView});
    }
  };
  //#endregion
  //#region setDescription
  setDescription = (val, index) => {
    this.state.ListView[index].Description = val;
    this.setState({ListView: this.state.ListView});
  };
  //#endregion
  //#region UpdateMachidnes
  UpdateMachidnes = () => {
    var listSubmit = [];
    var list = this.state.ListView;
    for (let i = 0; i < list.length; i++) {
      listSubmit.push({
        MarchineId: list[i].MarchineId,
        Description: list[i].Description,
        Status: list[i].Status,
      });
    }
    var _ConfirmDate = {
      ConfirmDate: FuncCommon.ConDate(new Date(), 2),
    };
    var _data = new FormData();
    _data.append('lstMachinesupdate', JSON.stringify(listSubmit));
    _data.append('editmarchines', JSON.stringify(_ConfirmDate));
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.UpdateMachidnes(_data)
          .then(res => {
            if (res.data.errorCode === 200) {
              Alert.alert(
                'Thông báo',
                'Cập nhật thành công',
                [{text: 'OK', onPress: () => console.log('OK Pressed')}],
                {cancelable: false},
              );
              this.callBackList();
            }
          })
          .catch(error => {
            this.callBackList();
            Alert.alert('Thông báo', 'Đã có lỗi xảy ra');
            console.log(error.data.data);
          });
      } else {
        Alert.alert(
          'Thông báo',
          'Yêu cầu kết nối mạng',
          [{text: 'Đóng', onPress: () => console.log('OK Pressed')}],
          {cancelable: false},
        );
      }
    });
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Operator_UpdateMachidnes_Component);
