import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_Operator, API_HR} from '../../../../network';
import TabBar_Title from '../../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../../../utils';
import {ScrollView} from 'react-native-gesture-handler';
import Combobox from '../../../component/Combobox';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const ListStatus = [
  {value: 'OK', text: 'Hoạt động'},
  {value: 'HO', text: 'Hỏng'},
  {value: 'TL', text: 'Thất lạc'},
  {value: 'BD', text: 'Bảo dưỡng'},
];
class Operator_AlertMachines_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      Data_jTableMachinesUpdate: {
        Length: 10000,
        NumberPage: 0,
        search: {
          value: '',
        },
      },
      Data_jTableToolsUpdate: {
        Length: 10000,
        NumberPage: 0,
        search: {
          value: '',
        },
      },
      indexCbx: null,
      keyCbx: '',
      ListViewMachines: [],
      ListViewTools: [],
      //hiển thị thông tin chung
      ViewMachines: false,
      transIcon_ViewMachines: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewTools: false,
      transIcon_ViewTools: new Animated.Value(0),
      //Hiển thị đính kèm
      ViewMachiV2: false,
      transIcon_ViewMachiV2: new Animated.Value(0),
    };
  }
  componentDidMount() {
    this.jTableMachinesUpdate(true);
    this.jTableToolsUpdate(true);
    this.Skill();
    this.setViewOpen('ViewMachines');
  }
  //#region Skill
  Skill = () => {
    //#region đóng mở tab
    this.Animated_on_ViewMachines = Animated.timing(
      this.state.transIcon_ViewMachines,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewMachines = Animated.timing(
      this.state.transIcon_ViewMachines,
      {toValue: 0, duration: 333},
    );
    this.Animated_on_ViewTools = Animated.timing(
      this.state.transIcon_ViewTools,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewTools = Animated.timing(
      this.state.transIcon_ViewTools,
      {toValue: 0, duration: 333},
    );
    this.Animated_on_ViewMachiV2 = Animated.timing(
      this.state.transIcon_ViewMachiV2,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewMachiV2 = Animated.timing(
      this.state.transIcon_ViewMachiV2,
      {toValue: 0, duration: 333},
    );
    //#endregion
  };
  //#endregion
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewMachines') {
      if (this.state.ViewMachines === false) {
        Animated.sequence([this.Animated_on_ViewMachines]).start();
        Animated.sequence([this.Animated_off_ViewMachiV2]).start();
        Animated.sequence([this.Animated_off_ViewTools]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewMachines]).start();
      }
      this.setState({
        ViewMachines: !this.state.ViewMachines,
        ViewTools: false,
        ViewMachiV2: false,
      });
    } else if (val === 'ViewTools') {
      if (this.state.ViewTools === false) {
        Animated.sequence([this.Animated_on_ViewTools]).start();
        Animated.sequence([this.Animated_off_ViewMachiV2]).start();
        Animated.sequence([this.Animated_off_ViewMachines]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewTools]).start();
      }
      this.setState({
        ViewTools: !this.state.ViewTools,
        ViewMachines: false,
        ViewMachiV2: false,
      });
    } else if (val === 'ViewMachiV2') {
      if (this.state.ViewMachiV2 === false) {
        Animated.sequence([this.Animated_on_ViewMachiV2]).start();
        Animated.sequence([this.Animated_off_ViewTools]).start();
        Animated.sequence([this.Animated_off_ViewMachines]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewMachiV2]).start();
      }
      this.setState({
        ViewMachiV2: !this.state.ViewMachiV2,
        ViewMachines: false,
        ViewTools: false,
      });
    }
  };
  //#endregion
  //#region jTableMachinesUpdate
  jTableMachinesUpdate = s => {
    if (s === true) {
      this.state.Data_jTableMachinesUpdate.NumberPage = 0;
      this.setState({
        ListViewMachines: [],
      });
    }
    this.state.Data_jTableMachinesUpdate.NumberPage = ++this.state
      .Data_jTableMachinesUpdate.NumberPage;
    this.setState({
      Data_jTableMachinesUpdate: this.state.Data_jTableMachinesUpdate,
    });
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.JTableMachinesUpdate(this.state.Data_jTableMachinesUpdate)
          .then(res => {
            FuncCommon.Data_Offline_Set('JTableMachinesUpdate', res);
            this._GetJTableMachinesUpdate(res);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('JTableMachinesUpdate');
        this._GetJTableMachinesUpdate(x);
      }
    });
  };
  _GetJTableMachinesUpdate = res => {
    var data = JSON.parse(res.data.data).data;
    for (let i = 0; i < data.length; i++) {
      this.state.ListViewMachines.push(data[i]);
    }
    this.setState({
      ListViewMachines: this.state.ListViewMachines,
      loading: false,
    });
  };
  //#endregion
  //#region jTableToolsUpdate
  jTableToolsUpdate = s => {
    if (s === true) {
      this.state.Data_jTableToolsUpdate.NumberPage = 0;
      this.setState({
        ListViewTools: [],
      });
    }
    this.state.Data_jTableToolsUpdate.NumberPage = ++this.state
      .Data_jTableToolsUpdate.NumberPage;
    this.setState({Data_jTableToolsUpdate: this.state.Data_jTableToolsUpdate});
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.JTableToolsUpdate(this.state.Data_jTableToolsUpdate)
          .then(res => {
            FuncCommon.Data_Offline_Set('jTableToolsUpdate', res);
            this._GetjTableToolsUpdate(res);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('jTableToolsUpdate');
        this._GetjTableToolsUpdate(x);
      }
    });
  };
  _GetjTableToolsUpdate = res => {
    var data = JSON.parse(res.data.data);
    for (let i = 0; i < data.length; i++) {
      this.state.ListViewTools.push(data[i]);
    }
    this.setState({
      ListViewTools: this.state.ListViewTools,
      loading: false,
    });
  };
  //#endregion
  render() {
    const rotateStart_ViewMachines = this.state.transIcon_ViewMachines.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    const rotateStart_ViewTools = this.state.transIcon_ViewTools.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewMachiV2 = this.state.transIcon_ViewMachiV2.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar_Title
            title={'QUẢN LÝ CƠ ĐIỆN ,BẢO DƯỠNG'}
            callBack={() => this.callBackList()}
          />
          <Divider />
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewMachines')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>Danh sách máy móc</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewMachines},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewMachines === true ? (
            <View style={{flex: 1}}>
              {/* Table */}
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'column', flex: 1}}>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                      <Text style={AppStyles.Labeldefault}>STT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Mã máy</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Tên máy</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 120}]}>
                      <Text style={AppStyles.Labeldefault}>Trạng thái</Text>
                    </TouchableOpacity>
                  </View>
                  {this.state.ListViewMachines.length > 0 ? (
                    <FlatList
                      data={this.state.ListViewMachines}
                      renderItem={({item, index}) => (
                        <View style={{flexDirection: 'row'}}>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 40}]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'center'},
                              ]}>
                              {index + 1}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 100}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.MarchineId}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 150}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.MarchineName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 120}]}>
                            <TouchableOpacity
                              style={[
                                AppStyles.FormInput,
                                {justifyContent: 'center'},
                              ]}
                              onPress={() =>
                                this.onActionCombobox_Status('Machines', index)
                              }>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.Status === 'OK'
                                  ? 'Hoạt động'
                                  : item.Status === 'HO'
                                  ? 'Hỏng'
                                  : item.Status === 'TL'
                                  ? 'Thất lạc'
                                  : item.Status === 'BD'
                                  ? 'Bảo dưỡng'
                                  : ''}
                              </Text>
                            </TouchableOpacity>
                          </TouchableOpacity>
                        </View>
                      )}
                      onEndReached={() =>
                        this.state.ListViewMachines.length >=
                        20 * this.state.Data_jTableMachinesUpdate.NumberPage
                          ? this.jTableMachinesUpdate(false)
                          : null
                      }
                      keyExtractor={(rs, index) => index.toString()}
                    />
                  ) : (
                    <TouchableOpacity style={[AppStyles.table_foot]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                        Không có dữ liệu
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </ScrollView>
            </View>
          ) : null}
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewTools')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>Danh sách công cụ</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewTools},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewTools === true ? (
            <View style={{flex: 1}}>
              {/* Table */}
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'column', flex: 1}}>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                      <Text style={AppStyles.Labeldefault}>STT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Mã CCDC</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Tên CCDC</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 120}]}>
                      <Text style={AppStyles.Labeldefault}>Trạng thái</Text>
                    </TouchableOpacity>
                  </View>
                  {this.state.ListViewTools.length > 0 ? (
                    <FlatList
                      data={this.state.ListViewTools}
                      renderItem={({item, index}) => (
                        <View style={{flexDirection: 'row'}}>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 40}]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'center'},
                              ]}>
                              {index + 1}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 100}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.ItemId}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 150}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.ItemName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 120}]}>
                            <TouchableOpacity
                              style={[
                                AppStyles.FormInput,
                                {justifyContent: 'center'},
                              ]}
                              onPress={() =>
                                this.onActionCombobox_Status('Tools', index)
                              }>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.Status === 'OK'
                                  ? 'Hoạt động'
                                  : item.Status === 'HO'
                                  ? 'Hỏng'
                                  : item.Status === 'TL'
                                  ? 'Thất lạc'
                                  : item.Status === 'BD'
                                  ? 'Bảo dưỡng'
                                  : ''}
                              </Text>
                            </TouchableOpacity>
                          </TouchableOpacity>
                        </View>
                      )}
                      onEndReached={() =>
                        this.state.ListViewTools.length >=
                        20 * this.state.Data_jTableToolsUpdate.NumberPage
                          ? this.jTableToolsUpdate(false)
                          : null
                      }
                      keyExtractor={(rs, index) => index.toString()}
                    />
                  ) : (
                    <TouchableOpacity style={[AppStyles.table_foot]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                        Không có dữ liệu
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </ScrollView>
            </View>
          ) : null}
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewMachiV2')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>
                Theo dõi số lần máy hỏng
              </Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewMachiV2},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewMachiV2 === true ? (
            <View style={{flex: 1}}>
              {/* Table */}
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'column', flex: 1}}>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                      <Text style={AppStyles.Labeldefault}>STT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Mã máy</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 140}]}>
                      <Text style={AppStyles.Labeldefault}>Tên máy</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>Lần hỏng</Text>
                    </TouchableOpacity>
                  </View>
                  {this.state.ListViewMachines.length > 0 ? (
                    <FlatList
                      data={this.state.ListViewMachines}
                      renderItem={({item, index}) => (
                        <View style={{flexDirection: 'row'}}>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 40}]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'center'},
                              ]}>
                              {index + 1}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 100}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.MarchineId}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 140}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.MarchineName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 80}]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'center'},
                              ]}>
                              {item.Numberbroken}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      )}
                      onEndReached={() =>
                        this.state.ListViewMachines.length >=
                        20 * this.state.Data_jTableMachinesUpdate.NumberPage
                          ? this.jTableMachinesUpdate(false)
                          : null
                      }
                      keyExtractor={(rs, index) => index.toString()}
                    />
                  ) : (
                    <TouchableOpacity style={[AppStyles.table_foot]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                        Không có dữ liệu
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </ScrollView>
            </View>
          ) : null}
          <TouchableOpacity
            style={[
              AppStyles.FormInput,
              {
                justifyContent: 'center',
                margin: 10,
                backgroundColor: '#bed9f6',
              },
            ]}
            onPress={() => this.Update()}>
            <Text style={[AppStyles.Textdefault, {color: 'black'}]}>LƯU</Text>
          </TouchableOpacity>
          <Combobox
            value={''}
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeStatus}
            data={ListStatus}
            nameMenu={'Chọn trạng thái'}
            eOpen={this.openCombobox_Status}
            position={'bottom'}
          />
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null && val !== '') {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        var _string2 = string[1].substring(0, 2);
        if (_string2 !== '00') {
          return valview + ',' + string[1].substring(0, 2) === '00';
        } else {
          return valview;
        }
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  combobox Status
  _openCombobox_Status() {}
  openCombobox_Status = d => {
    this._openCombobox_Status = d;
  };
  onActionCombobox_Status(key, index) {
    this.setState({keyCbx: key, indexCbx: index});
    this._openCombobox_Status();
  }
  ChangeStatus = rs => {
    if (rs !== null) {
      if (this.state.keyCbx === 'Tools') {
        this.state.ListViewTools[this.state.indexCbx].Status = rs.value;
        this.setState({ListViewTools: this.state.ListViewTools});
      } else {
        this.state.ListViewMachines[this.state.indexCbx].Status = rs.value;
        this.setState({ListViewMachines: this.state.ListViewMachines});
      }
    }
  };
  //#endregion
  //#region Update
  Update = () => {
    var listSubmitTools = [];
    var list = this.state.ListViewTools;
    for (let i = 0; i < list.length; i++) {
      listSubmitTools.push({
        ItemId: list[i].ItemId,
        Status: list[i].Status,
      });
    }
    var listSubmitMachines = [];
    var list = this.state.ListViewMachines;
    for (let i = 0; i < list.length; i++) {
      listSubmitMachines.push({
        MarchineId: list[i].MarchineId,
        Status: list[i].Status,
      });
    }
    var _ConfirmDate = {
      ConfirmDate: FuncCommon.ConDate(new Date(), 2),
    };
    var _data = new FormData();
    _data.append('lstTools', JSON.stringify(listSubmitTools));
    _data.append('lstMachines', JSON.stringify(listSubmitMachines));
    _data.append('edit', JSON.stringify(_ConfirmDate));
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.Update_MachinesTolss(_data)
          .then(res => {
            debugger;
            if (res.data.Error === false) {
              Alert.alert(
                'Thông báo',
                'Cập nhật thành công',
                [{text: 'OK', onPress: () => console.log('OK Pressed')}],
                {cancelable: false},
              );
              this.callBackList();
            } else {
              Alert.alert(
                'Thông báo',
                'Đã có lỗi khi cập nhật',
                [{text: 'OK', onPress: () => console.log('OK Pressed')}],
                {cancelable: false},
              );
            }
          })
          .catch(error => {
            this.callBackList();
            Alert.alert('Thông báo', 'Đã có lỗi xảy ra');
            console.log(error.data.data);
          });
      } else {
        Alert.alert(
          'Thông báo',
          'Yêu cầu kết nối mạng',
          [{text: 'Đóng', onPress: () => console.log('OK Pressed')}],
          {cancelable: false},
        );
      }
    });
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Operator_AlertMachines_Component);
