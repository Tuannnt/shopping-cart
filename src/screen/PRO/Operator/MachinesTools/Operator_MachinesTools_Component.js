import React, { Component } from 'react';
import { FlatList, Keyboard, Animated, Text, TouchableWithoutFeedback, RefreshControl, Image, View, Dimensions, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../../utils';
import { ScrollView } from 'react-native-gesture-handler';

const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_MachinesTools_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            Data_JTableMachines: {
                length: 20,
                search: {
                    value: ""
                },
                NumberPage: 0,
                lstDepartmentID: []
            },
            Data_JTableTools: {
                length: 20,
                search: {
                    value: ""
                },
                NumberPage: 0,
            },
            ListMachines: [],
            ListTools: [],
        };
        //#region tabbarbottom
        this.listtabbarBotom = [
            {
                Title: "Báo hỏng",
                Icon: "bell",
                Type: "feather",
                Value: "0",
                Checkbox: true,
                OffEditcolor: true,
                Badge: 0
            },
            {
                Title: "QL Cơ điện",
                Icon: "clipboard",
                Type: "feather",
                Value: "1",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            },
            {
                Title: "Update tình trạng",
                Icon: "refresh-cw",
                Type: "feather",
                Value: "2",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            }
        ];
        //#endregion
    }
    componentDidMount() {
        this.setState({ loading: true })
        this.JTableMachines(true);
        this.JTableTools(true);
    }
    //#region JTableMachines
    JTableMachines = (s) => {
        if (s === true) {
            this.state.Data_JTableMachines.NumberPage = 0;
            this.setState(
                {
                    loadingsearch: true,
                    ListMachines: [],
                }
            );
        }
        this.state.Data_JTableMachines.NumberPage = ++this.state.Data_JTableMachines.NumberPage
        this.setState({ Data_JTableMachines: this.state.Data_JTableMachines });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTableMachines(this.state.Data_JTableMachines).then(res => {
                    FuncCommon.Data_Offline_Set('JTableMachines', res);
                    this._GetJTableMachines(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('JTableMachines');
                this._GetJTableMachines(x);
            }
        });

    }
    _GetJTableMachines = (res) => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            data[i].Checkbox = false;
            this.state.ListMachines.push(data[i]);
        }
        this.setState({
            ListMachines: this.state.ListMachines,
            loading: false,
            loadingsearch: false
        });
    }
    //#endregion

    //#region JTableTools
    JTableTools = (s) => {
        if (s === true) {
            this.state.Data_JTableTools.NumberPage = 0;
            this.setState(
                {
                    loadingsearch: true,
                    ListTools: [],
                }
            );
        }
        this.state.Data_JTableTools.NumberPage = ++this.state.Data_JTableTools.NumberPage
        this.setState({ Data_JTableTools: this.state.Data_JTableTools });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTableTools(this.state.Data_JTableTools).then(res => {
                    FuncCommon.Data_Offline_Set('JTableTools', res);
                    this._GetJTableTools(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('JTableTools');
                this._GetJTableTools(x);
            }
        });

    }
    _GetJTableTools = (res) => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            this.state.ListTools.push(data[i]);
        }
        this.setState({
            ListTools: this.state.ListTools,
            loading: false,
            loadingsearch: false
        });
    }
    //#endregion
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"Máy móc"}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>MÁY MÓC</Text>
                                </View>
                            </TouchableOpacity>
                            <Divider />
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]} />
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã máy</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên máy</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>TG cần</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>TG có</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Trạng thái</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListMachines.length > 0 ?
                                        <FlatList
                                            data={this.state.ListMachines}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => this.CheckItemMachines(index)}>
                                                        {item.Checkbox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                        }
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.MarchineId}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.MarchineName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.RequireTime}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.ProductionTime}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.Status}</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            onEndReached={() => this.state.ListMachines.length >= (20 * this.state.Data_JTableMachines.NumberPage) ? this.JTableMachines(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>CÔNG CỤ DỤNG CỤ</Text>
                                </View>
                            </TouchableOpacity>
                            <Divider />
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]} />
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã CCDC</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên CCDC</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>Bàn giao</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>Còn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Trạng thái</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListTools.length > 0 ?
                                        <FlatList
                                            data={this.state.ListTools}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => this.CheckItemTools(index)}>
                                                        {item.Checkbox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                        }
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemId}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.ExportQuantity}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.InventoryQuantity}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.Status}</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            onEndReached={() => this.state.ListTools.length >= (20 * this.state.Data_JTableTools.NumberPage) ? this.JTableTools(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={AppStyles.StyleTabvarBottom}>
                            <TabBarBottomCustom
                                ListData={this.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    CallbackValueBottom = (key) => {
        var list = this.state.ListTools;
        var ListToolsSub = [];
        for (let i = 0; i < list.length; i++) {
            if (list[i].Checkbox === true) {
                ListToolsSub.push(list[i]);
            }
        }
        var listMachines = this.state.ListMachines;
        var ListMachinesSub = [];
        for (let i = 0; i < listMachines.length; i++) {
            if (listMachines[i].Checkbox === true) {
                ListMachinesSub.push(listMachines[i]);
            }
        }
        switch (key) {
            case "0":
                if (ListToolsSub.length > 0) {
                    var dataUpdateTool = "";
                    for (let i = 0; i < ListToolsSub.length; i++) {
                        if (dataUpdateTool === "") {
                            dataUpdateTool = ListToolsSub[i].ItemId
                        } else {
                            dataUpdateTool = dataUpdateTool + ";" + ListToolsSub[i].ItemId
                        }
                    }
                    Actions.Operator_EditTools({ value: dataUpdateTool });
                } else {
                    Alert.alert("Thông báo", "Chưa có công cụ dụng cụ nào được chọn.");
                }
                break;
            case "1":
                Actions.Operator_AlertMachines();
                break;
            case "2":
                if (ListMachinesSub.length > 0) {
                    var dataUpdate = "";
                    for (let i = 0; i < ListMachinesSub.length; i++) {
                        if (dataUpdate === "") {
                            dataUpdate = ListMachinesSub[i].RowGuid
                        } else {
                            dataUpdate = dataUpdate + ";" + ListMachinesSub[i].RowGuid
                        }
                    }
                    Actions.Operator_UpdateMachidnes({ value: dataUpdate })
                } else {
                    Alert.alert("Thông báo", "Chưa có máy móc nào được chọn.");
                }

                break;

            default:
                break;
        }
    }
    CheckItemMachines = (index) => {
        this.state.ListMachines[index].Checkbox = !this.state.ListMachines[index].Checkbox;
        this.setState({ ListMachines: this.state.ListMachines });
    }
    CheckItemTools = (index) => {
        this.state.ListTools[index].Checkbox = !this.state.ListTools[index].Checkbox;
        this.setState({ ListTools: this.state.ListTools });
    }
    //Các hàm dùng chung
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_MachinesTools_Component);
