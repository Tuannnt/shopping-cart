import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  TextInput,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  Animated,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {connect} from 'react-redux';
import {API_Operator, API} from '../../../../network';
import TabBar_Title from '../../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../../../utils';
import Combobox from '../../../component/Combobox';
import DatePicker from 'react-native-date-picker';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class Operator_addMaterialErrors_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data_GetDataView: {
        StartDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
        EndDate: new Date(
          new Date().getFullYear(),
          new Date().getMonth() + 1,
          0,
        ),
      },
      setEventStartDate: false,
      setEventEndDate: false,
      //Hiển phiếu nhận/ giao việc
      ViewForm: false,
      transIcon_ViewForm: new Animated.Value(0),
      //trạng thái công việc
      ViewTable: false,
      transIcon_ViewTable: new Animated.Value(0),
      loading: false,
      ListTable: [],
      ListMaterialErrors: [],
      offsetdate: false,
    };
  }
  componentDidMount() {
    this.setState({loading: true});
    this.Skill();
    this.setViewOpen('ViewForm');
  }
  //#region Skill
  Skill = () => {
    //#region đóng mở tab
    this.Animated_on_ViewForm = Animated.timing(this.state.transIcon_ViewForm, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_off_ViewForm = Animated.timing(
      this.state.transIcon_ViewForm,
      {toValue: 1, duration: 333},
    );
    this.Animated_on_ViewTable = Animated.timing(
      this.state.transIcon_ViewTable,
      {toValue: 0, duration: 333},
    );
    this.Animated_off_ViewTable = Animated.timing(
      this.state.transIcon_ViewTable,
      {toValue: 1, duration: 333},
    );
    //#endregion
    this.LoadData();
  };
  //#endregion
  //#region LoadDataTable
  LoadData = () => {
    if (this.props.list !== undefined) {
      for (let i = 0; i < this.props.list.length; i++) {
        var val = this.props.list[i];
        if (parseInt(val.Quantity) > 0) {
          this.state.ListTable.push({
            ItemId: val.ItemId,
            ItemName: val.ItemName,
            UnitId: val.UnitId,
            UnitName: val.UnitName,
            DepartmentID: val.DepartmentName,
            Quantity: val.Quantity,
            Comment: '',
          });
        }
      }
      this.setState({ListTable: this.state.ListTable, loading: false});
    }
  };
  GetAllMaterialErrors = () => {
    var data = {
      StartDate: FuncCommon.ConDate(this.state.Data_GetDataView.StartDate, 2),
      EndDate: FuncCommon.ConDate(this.state.Data_GetDataView.EndDate, 2),
    };
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetAllMaterialErrors(data)
          .then(res => {
            FuncCommon.Data_Offline_Set('GetAllMaterialErrors', res);
            this._GetAllMaterialErrors(res);
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetAllMaterialErrors');
        this._GetAllMaterialErrors(x);
      }
    });
  };
  _GetAllMaterialErrors = res => {
    var data = JSON.parse(res.data.data);
    for (let i = 0; i < data.length; i++) {
      this.state.ListMaterialErrors.push(data[i]);
    }
    this.setState({
      ListMaterialErrors: this.state.ListMaterialErrors,
      loading: false,
      loadingsearch: false,
    });
    this.setViewOpen('ViewTable');
  };
  //#endregion
  render() {
    const rotateStart_ViewForm = this.state.transIcon_ViewForm.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewTable = this.state.transIcon_ViewTable.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar_Title
            title={'BÁO LỖI TÌNH TRẠNG VẬT TƯ'}
            callBack={() => this.callBackList()}
          />
          <Divider />
          <ScrollView>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                padding: 10,
                backgroundColor: '#bed9f6',
              }}
              onPress={() => this.setViewOpen('ViewForm')}>
              <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={AppStyles.Labeldefault}>
                  Thêm mới lỗi tình trạng vật tư
                </Text>
              </View>
              <Animated.View
                style={{
                  transform: [
                    {rotate: rotateStart_ViewForm},
                    {perspective: 4000},
                  ],
                  paddingRight: 10,
                  paddingLeft: 10,
                }}>
                <Icon type={'feather'} name={'chevron-down'} />
              </Animated.View>
            </TouchableOpacity>
            <Divider />
            {this.state.ViewForm === true ? (
              <View style={{flex: 1, flexDirection: 'column'}}>
                <ScrollView horizontal={true}>
                  <View style={{flexDirection: 'column', flex: 1}}>
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 40}]}>
                        <Text style={AppStyles.Labeldefault}>STT</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 120}]}>
                        <Text style={AppStyles.Labeldefault}>Mã vật tư</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 150}]}>
                        <Text style={AppStyles.Labeldefault}>Tên vật tư</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 80}]}>
                        <Text style={AppStyles.Labeldefault}>ĐVT</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 150}]}>
                        <Text style={AppStyles.Labeldefault}>
                          Bộ phận xử lý
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 80}]}>
                        <Text style={AppStyles.Labeldefault}>Số lượng</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 150}]}>
                        <Text style={AppStyles.Labeldefault}>Lý do</Text>
                      </TouchableOpacity>
                    </View>
                    {this.state.ListTable.length > 0 ? (
                      <FlatList
                        data={this.state.ListTable}
                        renderItem={({item, index}) => (
                          <View style={{flexDirection: 'row'}}>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 40}]}>
                              <Text
                                style={[
                                  AppStyles.Textdefault,
                                  {textAlign: 'center'},
                                ]}>
                                {index + 1}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 120}]}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.ItemId}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 150}]}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.ItemName}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 80}]}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.UnitName}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 150}]}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.DepartmentID}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 80}]}>
                              <Text
                                style={[
                                  AppStyles.Textdefault,
                                  {textAlign: 'right'},
                                ]}>
                                {this.customFloat(item.Quantity)}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 150}]}>
                              <TextInput
                                value={item.Comment}
                                autoCapitalize="none"
                                onChangeText={txt =>
                                  this.setComment(txt, index)
                                }
                              />
                            </TouchableOpacity>
                          </View>
                        )}
                        keyExtractor={(rs, index) => index.toString()}
                      />
                    ) : (
                      <TouchableOpacity style={[AppStyles.table_foot]}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'center'},
                          ]}>
                          Không có dữ liệu
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </ScrollView>
                <TouchableOpacity
                  style={[
                    AppStyles.FormInput,
                    {
                      justifyContent: 'center',
                      margin: 10,
                      backgroundColor: '#bed9f6',
                      flexDirection: 'row',
                      alignItems: 'center',
                    },
                  ]}
                  onPress={() => this.Submit()}>
                  <Text style={[AppStyles.Textdefault, {color: 'black'}]}>
                    LƯU
                  </Text>
                </TouchableOpacity>
              </View>
            ) : null}

            <TouchableOpacity
              style={{
                flexDirection: 'row',
                padding: 10,
                backgroundColor: '#bed9f6',
              }}
              onPress={() => this.GetAllMaterialErrors()}>
              <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={AppStyles.Labeldefault}>
                  Danh sách lỗi tình trạng vật tư
                </Text>
              </View>
              <Animated.View
                style={{
                  transform: [
                    {rotate: rotateStart_ViewTable},
                    {perspective: 4000},
                  ],
                  paddingRight: 10,
                  paddingLeft: 10,
                }}>
                <Icon type={'feather'} name={'chevron-down'} />
              </Animated.View>
            </TouchableOpacity>
            <Divider />
            {this.state.ViewTable === true ? (
              <View>
                <View style={{flexDirection: 'column'}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      paddingLeft: 10,
                      paddingRight: 10,
                    }}>
                    <View
                      style={{flex: 1, flexDirection: 'column', padding: 5}}>
                      {/* <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu</Text> */}
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() =>
                          this.setState({
                            setEventStartDate: true,
                            setEventEndDate: false,
                          })
                        }>
                        <Text>
                          {FuncCommon.ConDate(
                            this.state.Data_GetDataView.StartDate,
                            0,
                          )}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{flex: 1, flexDirection: 'column', padding: 5}}>
                      {/* <Text style={AppStyles.Labeldefault}>Thời gian kết thúc</Text> */}
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() =>
                          this.setState({
                            setEventStartDate: false,
                            setEventEndDate: true,
                          })
                        }>
                        <Text>
                          {FuncCommon.ConDate(
                            this.state.Data_GetDataView.EndDate,
                            0,
                          )}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                {/* Table */}

                <ScrollView horizontal={true}>
                  <View style={{flexDirection: 'column', flex: 1}}>
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 40}]}>
                        <Text style={AppStyles.Labeldefault}>STT</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 120}]}>
                        <Text style={AppStyles.Labeldefault}>Mã vật tư</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 150}]}>
                        <Text style={AppStyles.Labeldefault}>Tên vật tư</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 80}]}>
                        <Text style={AppStyles.Labeldefault}>ĐVT</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 150}]}>
                        <Text style={AppStyles.Labeldefault}>
                          Bộ phận xử lý
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 80}]}>
                        <Text style={AppStyles.Labeldefault}>Số lượng</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_th, {width: 150}]}>
                        <Text style={AppStyles.Labeldefault}>Lý do</Text>
                      </TouchableOpacity>
                    </View>
                    {this.state.ListMaterialErrors.length > 0 ? (
                      <FlatList
                        data={this.state.ListMaterialErrors}
                        renderItem={({item, index}) => (
                          <View style={{flexDirection: 'row'}}>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 40}]}>
                              <Text
                                style={[
                                  AppStyles.Textdefault,
                                  {textAlign: 'center'},
                                ]}>
                                {index + 1}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 120}]}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.ItemId}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 150}]}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.ItemName}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 80}]}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.UnitName}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 150}]}>
                              <Text style={[AppStyles.Textdefault]}>
                                {item.DepartmentId}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 80}]}>
                              <Text
                                style={[
                                  AppStyles.Textdefault,
                                  {textAlign: 'right'},
                                ]}>
                                {this.customFloat(item.Quantity)}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[AppStyles.table_td, {width: 150}]}>
                              <TextInput
                                value={item.Comment}
                                autoCapitalize="none"
                                onChangeText={txt =>
                                  this.setComment(txt, index)
                                }
                              />
                            </TouchableOpacity>
                          </View>
                        )}
                        keyExtractor={(rs, index) => index.toString()}
                      />
                    ) : (
                      <TouchableOpacity style={[AppStyles.table_foot]}>
                        <Text
                          style={[
                            AppStyles.Textdefault,
                            {textAlign: 'center'},
                          ]}>
                          Không có dữ liệu
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </ScrollView>
              </View>
            ) : null}
          </ScrollView>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.Data_GetDataView.StartDate}
                mode="date"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.Data_GetDataView.EndDate}
                mode="date"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  setStartDate = txt => {
    this.state.Data_GetDataView.StartDate = txt;
    this.setState({Data_GetDataView: this.state.Data_GetDataView});
    this.GetAllMaterialErrors();
  };
  setEndDate = txt => {
    this.state.Data_GetDataView.EndDate = txt;
    this.setState({Data_GetDataView: this.state.Data_GetDataView});
    this.GetAllMaterialErrors();
  };
  setComment = (txt, i) => {
    this.state.ListTable[i].Comment = txt;
    this.setState({ListTable: this.state.ListTable});
  };

  //#region đóng mở các tab
  setViewOpen = val => {
    this.setState({
      ViewForm: false,
      ViewTable: false,
    });
    if (val === 'ViewForm') {
      this.setState({ViewForm: !this.state.ViewForm});
      if (this.state.ViewForm === false) {
        Animated.sequence([this.Animated_on_ViewForm]).start();
        Animated.sequence([this.Animated_off_ViewTable]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewForm]).start();
      }
    } else if (val === 'ViewTable') {
      this.setState({ViewTable: !this.state.ViewTable});
      if (this.state.ViewTable === false) {
        Animated.sequence([this.Animated_on_ViewTable]).start();
        Animated.sequence([this.Animated_off_ViewForm]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewTable]).start();
      }
    }
  };
  //#endregion
  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Emp', ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null && val !== '') {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        var _string2 = string[1].substring(0, 2);
        if (_string2 !== '00') {
          return valview + ',' + string[1].substring(0, 2) === '00';
        } else {
          return valview;
        }
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion

  //#region Submit
  Submit = () => {
    var fd = new FormData();
    fd.append('MaterialErrors', JSON.stringify(this.state.ListTable));
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.InsertMaterialErrors(fd)
          .then(res => {
            if (res.data.errorCode === 200) {
              Alert.alert('Thông báo', res.data.message);
            } else {
              Alert.alert('Thông báo', res.data.message);
            }
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        Alert.alert(
          'Thông báo',
          'Yêu cầu kết nối mạng',
          [{text: 'Đóng', onPress: () => console.log('OK Pressed')}],
          {cancelable: false},
        );
      }
    });
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Operator_addMaterialErrors_Component);
