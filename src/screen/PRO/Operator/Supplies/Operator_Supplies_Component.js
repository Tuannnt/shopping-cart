import React, { Component } from 'react';
import { FlatList, Keyboard, Animated, Text, TouchableWithoutFeedback, RefreshControl, Alert, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../../utils';
import { ScrollView } from 'react-native-gesture-handler';

const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_Supplies_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            CheckAll: false,
            ITems_CheckAll: false,
            loadingsearch: false,
            Data_JTable: {
                Guid: props.item.PlanGuid,
                Quantity: parseInt(props.item.Quantity),
                length: 20,
                search: {
                    value: ""
                },
                NumberPage: 0
            },
            Data_JTableItems: {
                Length: 20,
                NumberPage: 0,
            },
            ListTable: [],
            ListItems: [],
        };
        //#region tabbarbottom
        this.listtabbarBotom = [
            {
                Title: "Xuất NVL SX",
                Icon: "log-out",
                Type: "feather",
                Value: "0",
                Checkbox: true,
                OffEditcolor: true,
                Badge: 0
            },
            {
                Title: "Xuất VT tiêu hao",
                Icon: "log-out",
                Type: "feather",
                Value: "1",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            },
            {
                Title: "Báo lỗi",
                Icon: "bell",
                Type: "feather",
                Value: "2",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            }
        ];
        //#endregion
    }
    componentDidMount() {
        this.setState({ loading: true })
        this.JTableSupplies(true);
    }
    //#region JTableSupplies
    JTableSupplies = (s) => {
        if (s === true) {
            this.state.Data_JTable.NumberPage = 0;
            this.setState(
                {
                    loadingsearch: true,
                    ListTable: [],
                }
            );
        }
        this.state.Data_JTable.NumberPage = ++this.state.Data_JTable.NumberPage
        this.setState({ Data_JTable: this.state.Data_JTable });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTableSupplies(this.state.Data_JTable).then(res => {
                    FuncCommon.Data_Offline_Set('JTableSupplies', res);
                    this._GetJTableSupplies(res);
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                var x = await FuncCommon.Data_Offline_Get('JTableSupplies');
                this._GetJTableSupplies(x);
            }
        });

    };
    _GetJTableSupplies = (res) => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            data[i].CheckBox = false;
            this.state.ListTable.push(data[i]);
        }
        this.setState({
            ListTable: this.state.ListTable,
            loadingsearch: false
        });
        this.JTableItemSupplies(true);
    }
    //#endregion

    //#region JTableItemSupplies
    JTableItemSupplies = (s) => {
        if (s === true) {
            this.state.Data_JTableItems.NumberPage = 0;
            this.setState(
                {
                    loadingsearch: true,
                    ListItems: [],
                }
            );
        }
        this.state.Data_JTableItems.NumberPage = ++this.state.Data_JTableItems.NumberPage
        this.setState({ Data_JTableItems: this.state.Data_JTableItems });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTableItemSupplies(this.state.Data_JTableItems).then(res => {
                    FuncCommon.Data_Offline_Set('JTableItemSupplies', res);
                    this._GetJTableItemSupplies(res);
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                var x = await FuncCommon.Data_Offline_Get('JTableItemSupplies');
                this._GetJTableItemSupplies(x);
            }
        });

    };
    _GetJTableItemSupplies = (res) => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            data[i].CheckBox = false;
            this.state.ListItems.push(data[i]);
        }
        this.setState({
            ListItems: this.state.ListItems,
            loading: false,
            loadingsearch: false
        });
    }
    //#endregion
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"VẬT TƯ"}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>DANH SÁCH VẬT TƯ ĐƠN HÀNG</Text>
                                </View>
                            </TouchableOpacity>
                            <Divider />
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]} onPress={() => this.setCheckAll()}>
                                            {this.state.CheckAll ?
                                                <Icon
                                                    name={"check"}
                                                    type="entypo"
                                                    color={AppColors.ColorEdit}
                                                    size={20} />
                                                :
                                                <Icon
                                                    name={"square-o"}
                                                    type="font-awesome"
                                                    color={AppColors.IconcolorTabbar}
                                                    size={20} />
                                            }
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã vật tư</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên vật tư</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Quy cách</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Đơn vị</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Tồn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Cần</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListTable.length > 0 ?
                                        <FlatList
                                            data={this.state.ListTable}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => this.CheckBoxItem(index)}>
                                                        {item.CheckBox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                        }
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.ProductItemID}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.MateritalID}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.Description}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.UnitName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: "right" }]}>{this.customFloat(item.InventoryQuantity)}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: "right" }]}>{this.customFloat(item.QuantityC)}</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            onEndReached={() => this.state.ListTable.length >= (20 * this.state.Data_JTable.NumberPage) ? this.JTableSupplies(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        // refreshControl={
                                        //     <RefreshControl
                                        //         refreshing={this.state.loading}
                                        //         onRefresh={this._onRefresh}
                                        //         tintColor="#f5821f"
                                        //         titleColor="#fff"
                                        //         colors={[AppColors.Maincolor]}
                                        //     />
                                        // }
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>DANH SÁCH VẬT TƯ TIÊU HAO,BHLĐ</Text>
                                </View>
                            </TouchableOpacity>
                            <Divider />
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]} onPress={() => this.setCheckAllI_tems()}>
                                            {this.state.ITems_CheckAll ?
                                                <Icon
                                                    name={"check"}
                                                    type="entypo"
                                                    color={AppColors.ColorEdit}
                                                    size={20} />
                                                :
                                                <Icon
                                                    name={"square-o"}
                                                    type="font-awesome"
                                                    color={AppColors.IconcolorTabbar}
                                                    size={20} />
                                            }
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã BP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 120 }]}><Text style={AppStyles.Labeldefault}>Mã vật tư</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên vật tư</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Quy cách</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Đơn vị</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Tồn</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListItems.length > 0 ?
                                        <FlatList
                                            data={this.state.ListItems}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => this.CheckBoxItem_Items(index)}>
                                                        {item.CheckBox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                        }
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.DepartmentName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 120 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemId}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.Description}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.UnitName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: "right" }]}>{this.customFloat(item.Quantity)}</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            onEndReached={() => this.state.ListItems.length >= (20 * this.state.Data_JTableItems.NumberPage) ? this.JTableItemSupplies(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        // refreshControl={
                                        //     <RefreshControl
                                        //         refreshing={this.state.loading}
                                        //         onRefresh={this._onRefresh}
                                        //         tintColor="#f5821f"
                                        //         titleColor="#fff"
                                        //         colors={[AppColors.Maincolor]}
                                        //     />
                                        // }
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={AppStyles.StyleTabvarBottom}>
                            <TabBarBottomCustom
                                ListData={this.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    //#region CheckAll
    setCheckAll = () => {
        var list = this.state.ListTable;
        for (let i = 0; i < list.length; i++) {
            list[i].CheckBox = !this.state.CheckAll
        };
        this.setState({ ListTable: list, CheckAll: !this.state.CheckAll });
    }
    CheckBoxItem = (index) => {
        this.state.ListTable[index].CheckBox = !this.state.ListTable[index].CheckBox;
        var obj = this.state.ListTable.find(x => x.CheckBox === false);
        if (obj === undefined) {
            this.state.CheckAll = true;
        } else {
            this.state.CheckAll = false;
        }
        this.setState({ ListTable: this.state.ListTable, CheckAll: this.state.CheckAll });
    }
    setCheckAllI_tems = () => {
        var list = this.state.ListItems;
        for (let i = 0; i < list.length; i++) {
            list[i].CheckBox = !this.state.ITems_CheckAll
        };
        this.setState({ ListItems: list, ITems_CheckAll: !this.state.ITems_CheckAll });
    }
    CheckBoxItem_Items = (index) => {
        this.state.ListItems[index].CheckBox = !this.state.ListItems[index].CheckBox;
        var obj = this.state.ListItems.find(x => x.CheckBox === false);
        if (obj === undefined) {
            this.state.ITems_CheckAll = true;
        } else {
            this.state.ITems_CheckAll = false;
        }
        this.setState({ ListItems: this.state.ListItems, ITems_CheckAll: this.state.ITems_CheckAll });
    }
    //#endregion

    CallbackValueBottom = (key) => {
        var listTable = this.state.ListTable;
        var ListTableSub = [];
        for (let i = 0; i < listTable.length; i++) {
            if (listTable[i].CheckBox === true) {
                ListTableSub.push(listTable[i]);
            }
        }
        var listItem = this.state.ListItems;
        var ListItemSub = [];
        for (let i = 0; i < listItem.length; i++) {
            if (listItem[i].CheckBox === true) {
                ListItemSub.push(listItem[i]);
            }
        }
        switch (key) {
            case "0":
                if (ListTableSub.length > 0) {
                    Actions.Operator_addTicketRequestsExport({ list: ListTableSub });
                } else {
                    Alert.alert("Thông báo", "Yêu cầu chọn nguyên vật liệu");
                }
                break;
            case "1":
                if (ListItemSub.length > 0) {
                    Actions.Operator_addTicketRequestsExportVT({ list: ListItemSub });
                } else {
                    Alert.alert("Thông báo", "Yêu cầu chọn vật tư");    
                }
                break;
            case "2":
                if (ListTableSub.length > 0) {
                    Actions.Operator_addMaterialErrors({ list: ListTableSub });
                } else {
                    Alert.alert("Thông báo", "Yêu cầu chọn nguyên vật liệu");
                }
                break;
            default:
                break;
        }
    }

    //Các hàm dùng chung
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_Supplies_Component);
