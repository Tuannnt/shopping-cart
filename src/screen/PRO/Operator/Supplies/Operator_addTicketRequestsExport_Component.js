import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  TextInput,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  Animated,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {connect} from 'react-redux';
import {API_Operator, API} from '../../../../network';
import TabBar_Title from '../../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../../../utils';
import Combobox from '../../../component/Combobox';
import DatePicker from 'react-native-date-picker';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class Operator_addTicketRequestsExport_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modelauto: {
        Value: '',
        IsEdit: false,
      },
      //Hiển phiếu nhận/ giao việc
      ViewForm: false,
      transIcon_ViewForm: new Animated.Value(0),
      //trạng thái công việc
      ViewTable: false,
      transIcon_ViewTable: new Animated.Value(0),
      loading: false,
      DataView: {
        Title: '',
        VoucherType: 'D',
        RequestType: 'X',
        DepartmentGuid: null,
        DepartmentId: '',
        DepartmentName: '',
        RequestDate: new Date(),
        ObjectName: props.list[0].ObjectName,
        ObjectId: props.list[0].ObjectID,
        ObjectView: props.list[0].ObjectID + ' - ' + props.list[0].ObjectName,
        VoucherTypeView: 'Xuất dùng nội bộ và sản xuất',
        TicketRequestNo: '',
        Description: '',
      },
      ListTable: [],
      offsetdate: false,
    };
  }
  componentDidMount() {
    this.setState({loading: true});
    this.Skill();
    this.InforUserName();
    this.setViewOpen('ViewForm');
  }
  //#region  thông tin người đăng nhập
  InforUserName() {
    API.getProfile()
      .then(rs => {
        this.state.DataView.DepartmentGuid = rs.data.DepartmentGuid;
        this.state.DataView.DepartmentId = rs.data.DepartmentId;
        this.state.DataView.DepartmentName = rs.data.DepartmentName;
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  }
  //#endregion
  //#region Skill
  Skill = () => {
    //#region đóng mở tab
    this.Animated_on_ViewForm = Animated.timing(this.state.transIcon_ViewForm, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_off_ViewForm = Animated.timing(
      this.state.transIcon_ViewForm,
      {toValue: 1, duration: 333},
    );
    this.Animated_on_ViewTable = Animated.timing(
      this.state.transIcon_ViewTable,
      {toValue: 0, duration: 333},
    );
    this.Animated_off_ViewTable = Animated.timing(
      this.state.transIcon_ViewTable,
      {toValue: 1, duration: 333},
    );
    //#endregion
    this.GetNumberAutoAM();
  };
  //#endregion
  //#region LoadDataTable
  LoadData = () => {
    if (this.props.list !== undefined) {
      for (let i = 0; i < this.props.list.length; i++) {
        var val = this.props.list[i];
        this.state.ListTable.push({
          ItemGuid: val.ItemGuid,
          ItemId: val.ItemId,
          ItemName: val.ItemName,
          Note: val.Description,
          OrderNumberId: val.OrderNumber,
          UnitBefore: val.UnitId,
          UnitName: val.UnitName,
          Quantity: val.QuantityC,
          InventoryQuantity: val.InventoryQuantity,
          QuantityRemain: 0,
          QuantityActual: 0,
          UnitPriceBefore: 0,
          Amount: 0,
          SortOrder: '',
          VoucherGuid: '',
          QuantityProduct: '',
          ProductName: '',
        });
      }
      this.setState({ListTable: this.state.ListTable, loading: false});
    }
  };
  //#endregion
  //#region GetNumberAutoAM
  GetNumberAutoAM = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetNumberAutoAM({
          AliasId: 'QTDSCT_DNX',
          VoucherType: this.state.DataView.VoucherType,
        })
          .then(res => {
            if (res.data.errorCode !== 200) {
              Alert.alert('Thông báo', res.data.message);
            } else {
              var data = JSON.parse(res.data.data);
              this.state.DataView.TicketRequestNo = data.Value;
              this.state.modelauto.Value = data.Value;
              this.state.modelauto.IsEdit = data.IsEdit;
              this.setState({
                DataView: this.state.DataView,
                modelauto: this.state.modelauto,
              });
              this.LoadData();
            }
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        Alert.alert('Thông báo', 'Yêu cầu kết nối Internet');
      }
    });
  };
  //#endregion
  render() {
    const rotateStart_ViewForm = this.state.transIcon_ViewForm.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewTable = this.state.transIcon_ViewTable.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar_Title
            title={'THÊM MỚI ĐỀ NGHỊ XUẤT'}
            callBack={() => this.callBackList()}
          />
          <Divider />
          <ScrollView>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                padding: 10,
                backgroundColor: '#bed9f6',
              }}
              onPress={() => this.setViewOpen('ViewForm')}>
              <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={AppStyles.Labeldefault}>Thông tin chung</Text>
              </View>
              <Animated.View
                style={{
                  transform: [
                    {rotate: rotateStart_ViewForm},
                    {perspective: 4000},
                  ],
                  paddingRight: 10,
                  paddingLeft: 10,
                }}>
                <Icon type={'feather'} name={'chevron-down'} />
              </Animated.View>
            </TouchableOpacity>
            <Divider />
            {this.state.ViewForm === true ? (
              <View style={{flex: 1, padding: 10, flexDirection: 'column'}}>
                <ScrollView>
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Số phiếu</Text>
                    </View>
                    {this.state.modelauto.IsEdit === true ? (
                      <TextInput
                        style={AppStyles.FormInput}
                        value={this.state.DataView.TicketRequestNo}
                        autoCapitalize="none"
                        onChangeText={txt => this.setTicketRequestNo(txt)}
                      />
                    ) : (
                      <TouchableOpacity style={[AppStyles.FormInput]}>
                        <Text style={[AppStyles.TextInput]}>
                          {this.state.DataView.TicketRequestNo}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                  <View style={{marginTop: 10}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Ngày yêu cầu</Text>
                    </View>
                    <TouchableOpacity
                      style={[AppStyles.FormInput, {justifyContent: 'center'}]}
                      onPress={() => this.setState({offsetdate: true})}>
                      <Text style={[AppStyles.Textdefault]}>
                        {FuncCommon.ConDate(this.state.DataView.RequestDate, 0)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{marginTop: 10}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Tên đối tượng</Text>
                    </View>
                    <TouchableOpacity style={[AppStyles.FormInput]}>
                      <Text style={[AppStyles.TextInput]}>
                        {this.state.DataView.ObjectView}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{marginTop: 10}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Diễn giải</Text>
                    </View>
                    <TextInput
                      style={[AppStyles.FormInput, {maxHeight: 100}]}
                      underlineColorAndroid="transparent"
                      value={this.state.DataView.Description}
                      numberOfLines={5}
                      multiline={true}
                      onChangeText={txt => this.setDescription(txt)}
                    />
                  </View>
                </ScrollView>
              </View>
            ) : null}
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                padding: 10,
                backgroundColor: '#bed9f6',
              }}
              onPress={() => this.setViewOpen('ViewTable')}>
              <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={AppStyles.Labeldefault}>Chi tiết sản phẩm</Text>
              </View>
              <Animated.View
                style={{
                  transform: [
                    {rotate: rotateStart_ViewTable},
                    {perspective: 4000},
                  ],
                  paddingRight: 10,
                  paddingLeft: 10,
                }}>
                <Icon type={'feather'} name={'chevron-down'} />
              </Animated.View>
            </TouchableOpacity>
            <Divider />
            {/* Table */}
            {this.state.ViewTable === true ? (
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'column', flex: 1}}>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                      <Text style={AppStyles.Labeldefault}>STT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Mã vật tư</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Tên vật tư</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Quy cách</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>ĐV ĐN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>SL ĐN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>SL tồn</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 130}]}>
                      <Text style={AppStyles.Labeldefault}>Tên thành phẩm</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 120}]}>
                      <Text style={AppStyles.Labeldefault}>Số SO(KH)</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>Số lượng SP</Text>
                    </TouchableOpacity>
                  </View>
                  {this.state.ListTable.length > 0 ? (
                    <FlatList
                      data={this.state.ListTable}
                      renderItem={({item, index}) => (
                        <View style={{flexDirection: 'row'}}>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 40}]}>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                {textAlign: 'center'},
                              ]}>
                              {index + 1}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 150}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.ItemId}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 150}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.ItemName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 150}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.Note}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 80}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.UnitName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 80}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.Quantity}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 80}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.InventoryQuantity}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 130}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.ProductName}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 120}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.OrderNumberId}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[AppStyles.table_td, {width: 80}]}>
                            <Text style={[AppStyles.Textdefault]}>
                              {item.QuantityProduct}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      )}
                      keyExtractor={(rs, index) => index.toString()}
                    />
                  ) : (
                    <TouchableOpacity style={[AppStyles.table_foot]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                        Không có dữ liệu
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </ScrollView>
            ) : null}
          </ScrollView>
          <TouchableOpacity
            style={[
              AppStyles.FormInput,
              {
                justifyContent: 'center',
                margin: 10,
                backgroundColor: '#bed9f6',
                flexDirection: 'row',
                alignItems: 'center',
              },
            ]}
            onPress={() => this.Submit()}>
            <Text style={[AppStyles.Textdefault, {color: 'black'}]}>LƯU</Text>
          </TouchableOpacity>
          {this.state.offsetdate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetdate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataView.RequestDate === ''
                    ? new Date()
                    : this.state.DataView.RequestDate
                }
                mode="date"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setRequestDate(setDate)}
              />
            </View>
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  setTicketRequestNo = txt => {
    this.state.DataView.TicketRequestNo = txt;
    this.setState({DataView: this.state.DataView});
  };
  setRequestDate = txt => {
    this.state.DataView.RequestDate = txt;
    this.setState({DataView: this.state.DataView});
  };
  setDescription = txt => {
    this.state.DataView.Description = txt;
    this.setState({DataView: this.state.DataView});
  };
  setUnitPriceBefore = (txt, i) => {
    this.state.ListTable[i].UnitPriceBefore = txt;
    this.state.ListTable[i].Amount =
      parseInt(this.state.ListTable[i].Quantity) * parseInt(txt);
    this.setState({ListTable: this.state.ListTable});
  };

  //#region đóng mở các tab
  setViewOpen = val => {
    this.setState({
      ViewForm: false,
      ViewTable: false,
    });
    if (val === 'ViewForm') {
      this.setState({ViewForm: !this.state.ViewForm});
      if (this.state.ViewForm === false) {
        Animated.sequence([this.Animated_on_ViewForm]).start();
        Animated.sequence([this.Animated_off_ViewTable]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewForm]).start();
      }
    } else if (val === 'ViewTable') {
      this.setState({ViewTable: !this.state.ViewTable});
      if (this.state.ViewTable === false) {
        Animated.sequence([this.Animated_on_ViewTable]).start();
        Animated.sequence([this.Animated_off_ViewForm]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewTable]).start();
      }
    }
  };
  //#endregion
  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Emp', ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null && val !== '') {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        var _string2 = string[1].substring(0, 2);
        if (_string2 !== '00') {
          return valview + ',' + string[1].substring(0, 2) === '00';
        } else {
          return valview;
        }
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion

  //#region Submit
  Submit = () => {
    var ListTicketRequests = [];
    var data = this.state.ListTable;
    for (let i = 0; i < data.length; i++) {
      var val = data[i];
      ListTicketRequests.push({
        ItemId: val.ItemId,
        ItemName: val.ItemName,
        Note: val.Note,
        OrderNumberId: val.OrderNumberId,
        UnitBefore: val.UnitBefore,
        UnitName: val.UnitName,
        Quantity: val.Quantity,
        QuantityRemain: val.Quantity,
        QuantityActual: val.QuantityActual,
        UnitPriceBefore: val.UnitPriceBefore,
        Amount: val.Amount,
        SortOrder: val.SortOrder,
        VoucherGuid: val.VoucherGuid,
        QuantityProduct: val.QuantityProduct,
        ProductName: val.ProductName,
      });
    }
    var fd = new FormData();
    fd.append('model', JSON.stringify(this.state.DataView));
    fd.append('TicketRequestDetails', JSON.stringify(ListTicketRequests));
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.InsertTicketRequests(fd)
          .then(res => {
            if (res.data.errorCode === 200) {
              Alert.alert('Thông báo', res.data.message);
              this.callBackList();
            } else {
              Alert.alert('Thông báo', res.data.message);
            }
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        Alert.alert(
          'Thông báo',
          'Yêu cầu kết nối mạng',
          [{text: 'Đóng', onPress: () => console.log('OK Pressed')}],
          {cancelable: false},
        );
      }
    });
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Operator_addTicketRequestsExport_Component);
