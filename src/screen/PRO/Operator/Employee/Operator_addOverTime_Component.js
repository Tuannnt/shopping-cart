import React, {Component} from 'react';
import {
  TouchableOpacity,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  View,
  Alert,
  TextInput,
  ScrollView,
  Dimensions,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import configApp from '../../../../configApp';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {ErrorHandler} from '@error';
import {Actions} from 'react-native-router-flux';
import {API_ApplyOverTimes, API_Operator} from '../../../../network';
import TabBar_Title from '../../../component/TabBar_Title';
import {Divider, Icon} from 'react-native-elements';
import DatePicker from 'react-native-date-picker';
import LoadingComponent from '../../../component/LoadingComponent';
import Combobox from '../../../component/Combobox';
import {FuncCommon} from '../../../../utils';
import ComboboxV2 from '../../../component/ComboboxV2';
import OpenPhotoLibrary from '../../../component/OpenPhotoLibrary';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const options = {
  title: 'Chọn ảnh',
  cancelButtonTitle: 'Huỷ bỏ',
  // takePhotoButtonTitle: 'Chụp ảnh',
  chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
  chooseWhichLibraryTitle: 'Chọn thư viện',
};
const ListComboboxAtt = [
  {
    value: 'img',
    text: 'Chọn ảnh từ thư viện',
  },
  {
    value: 'file',
    text: 'Chọn file',
  },
  // {
  //   value: 'camera',
  //   text: 'Chụp ảnh',
  // },
];
class Operator_addOverTime_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modelauto: {
        Value: '',
        IsEdit: false,
      },
      Index: null,
      listEmp: [],
      ListShifts: [],
      ListOrdersAM: [],
      offsetdate: false,
      offsetStartTime: false,
      offsetEndTime: false,
      offsetActualStartTime: false,
      offsetActualEndTime: false,
      DataView: {
        ApplyOvertimeId: '',
        Title: '',
        ApplyDate: new Date(),
        Type: null,
        Description: '',
      },
      DataDetail: [
        {
          Id: '',
          ApplyOvertimeGuid: '',
          EmployeeGuid: '',
          Employee_View: '',
          JobTitleName: '',
          ShiftID: '',
          Shift_View: '',
          WorkDayRate: '',
          StartTime: '',
          EndTime: '',
          ActualStartTime: '',
          ActualEndTime: '',
          TotalTime: '',
          ActualTotalTime: '',
          Result: '',
          ItemID: '',
          ItemID_View: '',
          OrderNumber: '',
          Description: '',
          ItemIDByProvider: '',
        },
      ],
      Attachment: [],
      //hiển thị thông tin chung
      ViewForm: false,
      transIcon_ViewForm: new Animated.Value(0),
      //Hiển thị chi tiết
      ViewTable: false,
      transIcon_ViewTable: new Animated.Value(0),
      //Hiển thị đính kèm
      ViewAtt: false,
      transIcon_ViewAtt: new Animated.Value(0),
    };
    this.FileAttackments = {
      renderImage(check) {
        if (check !== '') {
          var _check = check.split('.');
          if (_check.length > 1) {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              _check[_check.length - 1] +
              '-icon.png'
            );
          } else {
            return (
              configApp.url_icon_chat +
              configApp.link_type_icon +
              'default' +
              '-icon.png'
            );
          }
        }
      },
    };
  }
  async componentDidMount() {
    await this.request_storage_runtime_permission();
    this.GetNumberAutoHr();
    this.setApplyDate(new Date());
    this.Skill();
    this.setViewOpen('ViewForm');
    this.GetShifts();
    this.GetOrders_AM();
    if (this.props.Data !== undefined) {
      if (this.props.Data.DataDetail.length > 0) {
        for (let i = 0; i < this.props.Data.DataDetail.length; i++) {
          this.state.DataDetail.push({
            Id: '',
            ApplyOvertimeGuid: '',
            EmployeeGuid: this.props.Data.DataDetail[i],
            Employee_View: '',
            JobTitleName: '',
            ShiftID: '',
            Shift_View: '',
            WorkDayRate: '',
            StartTime: '',
            EndTime: '',
            ActualStartTime: '',
            ActualEndTime: '',
            TotalTime: '',
            ActualTotalTime: '',
            Result: '',
            ItemID: '',
            ItemID_View: '',
            OrderNumber: '',
            Description: '',
            ItemIDByProvider: '',
          });
        }
        this.setState({DataDetail: this.state.DataDetail});
      }
      if (this.props.Data.listEmp.length > 0) {
        for (let i = 0; i < this.props.Data.listEmp.length; i++) {
          this.state.listEmp.push({
            value: this.props.Data.listEmp[i].id,
            text: this.props.Data.listEmp[i].name,
            JobTitleName: this.props.Data.listEmp[i].JobTitleName,
          });
        }
        this.setState({listEmp: this.state.listEmp});
      }
    }
  }
  //#region GetNumberAutoHr
  GetNumberAutoHr = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetNumberAutoHr({AliasId: 'QTDSCT_LT'})
          .then(res => {
            if (res.data.errorCode !== 200) {
              Alert.alert('Thông báo', res.data.message);
            } else {
              var data = JSON.parse(res.data.data);
              this.state.DataView.ApplyOvertimeId = data.Value;
              this.state.modelauto.Value = data.Value;
              this.state.modelauto.IsEdit = data.IsEdit;
              this.setState({
                DataView: this.state.DataView,
                modelauto: this.state.modelauto,
              });
            }
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        Alert.alert('Thông báo', 'Yêu cầu kết nối Internet');
      }
    });
  };
  //#endregion
  //#region GetShifts
  GetShifts = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetShifts()
          .then(res => {
            if (res.data.errorCode !== 200) {
              Alert.alert('Thông báo', res.data.message);
            } else {
              FuncCommon.Data_Offline_Set('GetShifts_Operator', res);
              this._GetShifts_Operator(res);
            }
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetShifts_Operator');
        this._GetShifts_Operator(x);
      }
    });
  };
  _GetShifts_Operator = res => {
    this.setState({ListShifts: JSON.parse(res.data.data)});
  };
  //#endregion
  //#region GetOrders_AM
  GetOrders_AM = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetOrders_AM()
          .then(rs => {
            if (rs.data.errorCode !== 200) {
              Alert.alert('Thông báo', rs.data.message);
            } else {
              FuncCommon.Data_Offline_Set('GetOrders_AM_Operator', rs);
              this._GetOrders_AM_Operator(rs);
            }
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetOrders_AM_Operator');
        this._GetOrders_AM_Operator(x);
      }
    });
  };
  _GetOrders_AM_Operator = rs => {
    var data = JSON.parse(rs.data.data);
    for (let i = 0; i < data.length; i++) {
      this.state.ListOrdersAM.push({
        value: data[i].id,
        text: data[i].name,
        OrderNumber: data[i].OrderNumber,
      });
    }
  };
  //#endregion
  //#region Skill
  Skill = () => {
    //#region đóng mở tab
    this.Animated_on_ViewForm = Animated.timing(this.state.transIcon_ViewForm, {
      toValue: 0,
      duration: 333,
    });
    this.Animated_off_ViewForm = Animated.timing(
      this.state.transIcon_ViewForm,
      {toValue: 1, duration: 333},
    );
    this.Animated_on_ViewTable = Animated.timing(
      this.state.transIcon_ViewTable,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewTable = Animated.timing(
      this.state.transIcon_ViewTable,
      {toValue: 0, duration: 333},
    );
    this.Animated_on_ViewAtt = Animated.timing(this.state.transIcon_ViewAtt, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_ViewAtt = Animated.timing(this.state.transIcon_ViewAtt, {
      toValue: 0,
      duration: 333,
    });
    //#endregion
  };
  //#endregion
  setApplyDate = date => {
    let obj = {date: this.convertDateNow(date)};
    API_ApplyOverTimes.ApplyOverTimes_CheckHoliday(obj)
      .then(res => {
        if (res.data.data === 'null') {
          this.state.DataView.Type = 'O';
        } else {
          this.state.DataView.Type = res.data.data.HolidayType;
        }
        this.state.DataView.ApplyDate = date;
        this.setState({DataView: this.state.DataView});
      })
      .catch(error => {
        console.log(error.data.data);
        console.log('=======' + error.data);
      });
  };
  Submit() {
    if (
      this.state.modelauto.Value.toUpperCase() ===
      this.state.DataView.ApplyOvertimeId.toUpperCase()
    ) {
      this.state.modelauto.IsEdit = false;
    } else {
      this.state.modelauto.IsEdit = true;
    }
    this.setState({modelauto: this.state.modelauto});
    var DataSub = {
      Status: 'W',
      ApplyDate: FuncCommon.ConDate(this.state.DataView.ApplyDate, 2),
      FileAttachments: [],
      TypeName: this.state.DataView.Type,
      ApplyOvertimeId: this.state.DataView.ApplyOvertimeId,
      Title: this.state.DataView.Title,
      Description: this.state.DataView.Description,
    };
    var DataDetail = [];
    for (let i = 0; i < this.state.DataDetail.length; i++) {
      if (
        this.state.DataDetail[i].EmployeeGuid !== '' &&
        this.state.DataDetail[i].EmployeeGuid !== null
      ) {
        DataDetail.push({
          Id: null,
          EmployeeGuid: this.state.DataDetail[i].EmployeeGuid,
          JobTitleName: this.state.DataDetail[i].JobTitleName,
          StartTime: FuncCommon.ConDate(this.state.DataDetail[i].StartTime, 6),
          EndTime: FuncCommon.ConDate(this.state.DataDetail[i].EndTime, 6),
          ActualStartTime: FuncCommon.ConDate(
            this.state.DataDetail[i].ActualStartTime,
            6,
          ),
          ActualEndTime: FuncCommon.ConDate(
            this.state.DataDetail[i].ActualEndTime,
            6,
          ),
          TotalTime: this.state.DataDetail[i].TotalTime,
          ActualTotalTime: this.state.DataDetail[i].ActualTotalTime,
          Result: this.state.DataDetail[i].Result,
          ShiftID: this.state.DataDetail[i].ShiftID,
          WorkDayRate: this.state.DataDetail[i].WorkDayRate,
          ItemIDByProvider: this.state.DataDetail[i].ItemIDByProvider,
          OrderNumber: this.state.DataDetail[i].OrderNumber,
          Description: this.state.DataDetail[i].Description,
        });
      }
    }
    var fd = new FormData();
    fd.append('submit', JSON.stringify(DataSub));
    fd.append('lstDetails', JSON.stringify(DataDetail));
    fd.append('auto', JSON.stringify(this.state.modelauto));
    fd.append('DeleteAttach', JSON.stringify([]));
    fd.append('lstTitlefile', JSON.stringify([]));
    for (var i = 0; i < this.state.Attachment.length; i++) {
      fd.append(this.state.Attachment[i].FileName, {
        name: this.state.Attachment[i].FileName,
        size: this.state.Attachment[i].size,
        type: this.state.Attachment[i].type,
        uri: this.state.Attachment[i].uri,
      });
    }
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.Submit(fd)
          .then(res => {
            if (res.data.errorCode !== 200) {
              Alert.alert('Thông báo', res.data.message);
            } else {
              Alert.alert('Thông báo', res.data.message);
              this.callBackList();
            }
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        Alert.alert('Thông báo', 'Yêu cầu kết nối Internet');
      }
    });
  }
  render() {
    const rotateStart_ViewForm = this.state.transIcon_ViewForm.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewTable = this.state.transIcon_ViewTable.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    const rotateStart_ViewAtt = this.state.transIcon_ViewAtt.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar_Title
            title="THÊM MỚI PHIẾU ĐĂNG KÝ TĂNG CA"
            callBack={() => this.callBackList()}
          />
          <Divider />
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewForm')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>THÔNG TIN CHUNG</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewForm},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-down'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewForm === true ? (
            <View style={{flex: 1, padding: 10, flexDirection: 'column'}}>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Mã phiếu</Text>
                </View>
                {this.state.modelauto.IsEdit === true ? (
                  <TextInput
                    style={AppStyles.FormInput}
                    value={this.state.DataView.ApplyOvertimeId}
                    autoCapitalize="none"
                    placeholder="Nhập tiêu đề phiếu"
                    onChangeText={txt => this.setApplyOvertimeId(txt)}
                  />
                ) : (
                  <TouchableOpacity style={[AppStyles.FormInput]}>
                    <Text style={[AppStyles.TextInput]}>
                      {this.state.DataView.ApplyOvertimeId}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
              <View style={{marginTop: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Tiêu đề phiếu</Text>
                </View>
                <TextInput
                  style={AppStyles.FormInput}
                  value={this.state.DataView.Title}
                  autoCapitalize="none"
                  placeholder="Nhập tiêu đề phiếu"
                  onChangeText={txt => this.setTitle(txt)}
                />
              </View>
              <View style={{marginTop: 10}}>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={AppStyles.Labeldefault}>Ngày đăng ký</Text>
                </View>
                <TouchableOpacity
                  style={[AppStyles.FormInput, {justifyContent: 'center'}]}
                  onPress={() => this.setState({offsetdate: true})}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.ConDate(this.state.DataView.ApplyDate, 0)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{marginTop: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Loại ca làm thêm</Text>
                </View>
                <View
                  style={[AppStyles.FormInput]}
                  onPress={() => this.onActionCombobox()}>
                  <Text style={[AppStyles.TextInput]}>
                    {this.TxtType(this.state.DataView.Type)}
                  </Text>
                </View>
              </View>
              <View style={{marginTop: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Nội dung </Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {maxHeight: 100}]}
                  underlineColorAndroid="transparent"
                  value={this.state.DataView.Description}
                  numberOfLines={5}
                  multiline={true}
                  onChangeText={txt => this.setDescription(txt)}
                />
              </View>
            </View>
          ) : null}
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewTable')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewTable},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewTable === true ? (
            <View style={{flex: 1}}>
              {/* Table */}
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'column', flex: 1}}>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                      <Text style={AppStyles.Labeldefault}>STT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 200}]}>
                      <Text style={AppStyles.Labeldefault}>Nhân viên</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Ca đăng ký</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>% Ca</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>TG vào DK</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>TG ra DK</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>TG vào thực</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>TG ra thực</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Tổng TG DK</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Tổng TG thực</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Kết quả</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 200}]}>
                      <Text style={AppStyles.Labeldefault}>Mã SP</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Số SO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                    </TouchableOpacity>
                  </View>
                  {this.state.DataDetail.map((item, index) => (
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[
                          AppStyles.table_td,
                          {width: 40, alignItems: 'center'},
                        ]}>
                        <Text style={AppStyles.Labeldefault}>{index + 1}</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 200}]}>
                        <TouchableOpacity
                          style={AppStyles.FormInput}
                          onPress={() => this.onActionComboboxEmployee(index)}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              item.EmployeeGuid === ''
                                ? {color: AppColors.gray}
                                : {color: AppColors.black},
                            ]}>
                            {item.EmployeeGuid !== ''
                              ? item.Employee_View
                              : 'Chọn nhân viên'}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 100}]}>
                        <TouchableOpacity
                          style={AppStyles.FormInput}
                          onPress={() => {
                            this.onActionComboboxShifts(index);
                          }}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              item.ShiftID === ''
                                ? {color: AppColors.gray}
                                : {color: AppColors.black},
                            ]}>
                            {item.ShiftID !== ''
                              ? item.Shift_View
                              : 'Ca đăng ký'}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[
                          AppStyles.table_td,
                          {width: 80, alignItems: 'flex-end'},
                        ]}>
                        <Text style={AppStyles.Textdefault}>
                          {item.WorkDayRate}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 80}]}>
                        <TouchableOpacity
                          style={[AppStyles.FormInput, {alignItems: 'center'}]}
                          onPress={() =>
                            this.openFormDateTime(index, 'StartTime')
                          }>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.convertdatetime(item.StartTime)}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 80}]}>
                        <TouchableOpacity
                          style={[AppStyles.FormInput, {alignItems: 'center'}]}
                          onPress={() =>
                            this.openFormDateTime(index, 'EndTime')
                          }>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.convertdatetime(item.EndTime)}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 100}]}>
                        <TouchableOpacity
                          style={[AppStyles.FormInput, {alignItems: 'center'}]}
                          onPress={() =>
                            this.openFormDateTime(index, 'ActualStartTime')
                          }>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.convertdatetime(item.ActualStartTime)}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 80}]}>
                        <TouchableOpacity
                          style={[AppStyles.FormInput, {alignItems: 'center'}]}
                          onPress={() =>
                            this.openFormDateTime(index, 'ActualEndTime')
                          }>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.convertdatetime(item.ActualEndTime)}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[
                          AppStyles.table_td,
                          {width: 100, alignItems: 'flex-end'},
                        ]}>
                        <Text style={AppStyles.Textdefault}>
                          {this.customFloat(item.TotalTime)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[
                          AppStyles.table_td,
                          {width: 100, alignItems: 'flex-end'},
                        ]}>
                        <Text style={AppStyles.Textdefault}>
                          {this.customFloat(item.ActualTotalTime)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 150}]}>
                        <View style={{flex: 1}}>
                          <TextInput
                            style={AppStyles.FormInput}
                            underlineColorAndroid="transparent"
                            value={item.Result}
                            autoCapitalize="none"
                            onChangeText={txt => this.setResult(txt, index)}
                          />
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 200}]}>
                        <TouchableOpacity
                          style={AppStyles.FormInput}
                          onPress={() => this.onActionComboboxOrdersAM(index)}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              item.ItemID === ''
                                ? {color: AppColors.gray}
                                : {color: AppColors.black},
                            ]}>
                            {item.ItemID !== '' ? item.ItemID_View : 'Mã SP'}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 150}]}>
                        <Text style={AppStyles.Textdefault}>
                          {item.OrderNumber}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 150}]}>
                        <View style={{flex: 1}}>
                          <TextInput
                            style={AppStyles.FormInput}
                            underlineColorAndroid="transparent"
                            value={item.Description}
                            autoCapitalize="none"
                            onChangeText={txt =>
                              this.setDescriptionDetail(txt, index)
                            }
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  ))}
                </View>
              </ScrollView>
            </View>
          ) : null}
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewAtt')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>ĐÍNH KÈM</Text>
            </View>
            <Animated.View
              style={{
                transform: [{rotate: rotateStart_ViewAtt}, {perspective: 4000}],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewAtt === true ? (
            <View style={{flex: 1}}>
              {this.state.Attachment.length > 0
                ? this.state.Attachment.map((para, i) => (
                    <View
                      key={i}
                      style={[{flexDirection: 'row', marginBottom: 3}]}>
                      <View
                        style={[AppStyles.containerCentered, {padding: 10}]}>
                        <Image
                          style={{width: 30, height: 30}}
                          source={{
                            uri: this.FileAttackments.renderImage(
                              para.FileName,
                            ),
                          }}
                        />
                      </View>
                      <TouchableOpacity
                        key={i}
                        style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={AppStyles.Textdefault}>
                          {para.FileName}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.containerCentered, {padding: 10}]}
                        onPress={() => this.removeAttactment(para)}>
                        <Icon
                          name="close"
                          type="antdesign"
                          size={20}
                          color={AppColors.ColorDelete}
                        />
                      </TouchableOpacity>
                    </View>
                  ))
                : null}
              <View>
                <TouchableOpacity
                  style={[
                    AppStyles.FormInput,
                    {
                      justifyContent: 'center',
                      margin: 10,
                      backgroundColor: '#bed9f6',
                    },
                  ]}
                  onPress={() => this.openAttachment()}>
                  <Text style={[AppStyles.Textdefault, {color: 'black'}]}>
                    Thêm mới đính kèm
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
          <TouchableOpacity
            style={[
              AppStyles.FormInput,
              {
                justifyContent: 'center',
                margin: 10,
                backgroundColor: '#bed9f6',
              },
            ]}
            onPress={() => this.Submit()}>
            <Text style={[AppStyles.Textdefault, {color: 'black'}]}>LƯU</Text>
          </TouchableOpacity>

          {this.state.listEmp.length > 0 ? (
            <Combobox
              value={''}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeEmployee}
              data={this.state.listEmp}
              nameMenu={'Chọn nhân viên'}
              eOpen={this.openComboboxEmployee}
              position={'bottom'}
            />
          ) : null}
          {this.state.ListShifts.length > 0 ? (
            <Combobox
              value={''}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeShifts}
              data={this.state.ListShifts}
              nameMenu={'Chọn ca làm việc'}
              eOpen={this.openComboboxShifts}
              position={'bottom'}
            />
          ) : null}
          {this.state.ListOrdersAM.length > 0 ? (
            <Combobox
              value={''}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeOrdersAM}
              data={this.state.ListOrdersAM}
              nameMenu={'Chọn mã sản phẩm'}
              eOpen={this.openComboboxOrdersAM}
              position={'bottom'}
            />
          ) : null}
          {this.state.offsetdate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetdate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataView.ApplyDate === ''
                    ? new Date()
                    : this.state.DataView.ApplyDate
                }
                mode="date"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setApplyDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.offsetStartTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetStartTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataDetail[this.state.Index].StartTime === ''
                    ? new Date()
                    : this.state.DataDetail[this.state.Index].StartTime
                }
                mode="time"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setStartTime(setDate)}
              />
            </View>
          ) : null}
          {this.state.offsetEndTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetEndTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataDetail[this.state.Index].EndTime === ''
                    ? new Date()
                    : this.state.DataDetail[this.state.Index].EndTime
                }
                mode="time"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setEndTime(setDate)}
              />
            </View>
          ) : null}
          {this.state.offsetActualStartTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetActualStartTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataDetail[this.state.Index].ActualStartTime === ''
                    ? new Date()
                    : this.state.DataDetail[this.state.Index].ActualStartTime
                }
                mode="time"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setActualStartTime(setDate)}
              />
            </View>
          ) : null}
          {this.state.offsetActualEndTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetActualEndTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataDetail[this.state.Index].ActualEndTime === ''
                    ? new Date()
                    : this.state.DataDetail[this.state.Index].ActualEndTime
                }
                mode="time"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setActualEndTime(setDate)}
              />
            </View>
          ) : null}
          <ComboboxV2
            callback={this.ChoiceAtt}
            data={ListComboboxAtt}
            eOpen={this.openCombobox_Att}
          />
          <OpenPhotoLibrary
            callback={this.callbackLibarary}
            openLibrary={this.openLibrary}
          />
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  setApplyOvertimeId = txt => {
    this.state.DataView.ApplyOvertimeId = txt;
    this.setState({DataView: this.state.DataView});
  };
  setTitle = txt => {
    this.state.DataView.Title = txt;
    this.setState({DataView: this.state.DataView});
  };
  setDescription = txt => {
    this.state.DataView.Description = txt;
    this.setState({DataView: this.state.DataView});
  };
  TxtType = val => {
    if (val === 'T') {
      return 'Tăng ca ngày lễ';
    } else if (val === 'L') {
      return 'Tăng ca cuối tuần';
    } else {
      return 'Tăng ca ngày thường';
    }
  };
  setApplyDate = date => {
    this.state.DataView.ApplyDate = date;
    this.setState({DataView: this.state.DataView});
  };
  openFormDateTime = (i, para) => {
    this.setState({Index: i});
    switch (para) {
      case 'StartTime':
        this.setState({
          offsetStartTime: true,
          offsetEndTime: false,
          offsetActualStartTime: false,
          offsetActualEndTime: false,
        });
        break;
      case 'EndTime':
        this.setState({
          offsetStartTime: false,
          offsetEndTime: true,
          offsetActualStartTime: false,
          offsetActualEndTime: false,
        });
        break;
      case 'ActualStartTime':
        this.setState({
          offsetStartTime: false,
          offsetEndTime: false,
          offsetActualStartTime: true,
          offsetActualEndTime: false,
        });
        break;
      case 'ActualEndTime':
        this.setState({
          offsetStartTime: false,
          offsetEndTime: false,
          offsetActualStartTime: false,
          offsetActualEndTime: true,
        });
        break;
      default:
        break;
    }
  };
  setStartTime = time => {
    this.state.DataDetail[this.state.Index].StartTime = time;
    var _start = time;
    var _end = this.state.DataDetail[this.state.Index].EndTime;
    if (_start !== '' && _end !== '') {
      var s = FuncCommon.ConDate(_start, 6);
      var e = FuncCommon.ConDate(_end, 6);
      var sSplit = s.split(':');
      var eSplit = e.split(':');
      if (new Date(_start) <= new Date(_end)) {
        this.state.DataDetail[this.state.Index].TotalTime =
          (new Date(2019, 1, 1, parseInt(eSplit[0]), parseInt(eSplit[1])) -
            new Date(2019, 1, 1, parseInt(sSplit[0]), parseInt(sSplit[1]))) /
          (60 * 60 * 1000);
      } else {
        this.state.DataDetail[this.state.Index].TotalTime =
          (new Date(2019, 1, 2, hours, minutes) -
            new Date(2019, 1, 1, startHours, startMinutes) +
            (new Date(2019, 1, 2, endHours, endMinutes) -
              new Date(2019, 1, 2, hours, minutes))) /
          (60 * 60 * 1000);
      }
    }
    this.setState({DataDetail: this.state.DataDetail});
  };
  setEndTime = time => {
    this.state.DataDetail[this.state.Index].EndTime = time;
    var _end = time;
    var _start = this.state.DataDetail[this.state.Index].StartTime;
    if (_start !== '' && _end !== '') {
      var hours = new Date('2019-12-11 23:59:00').getHours();
      var minutes = new Date('2019-12-11 24:00:00').getMinutes();
      var s = FuncCommon.ConDate(_start, 6);
      var e = FuncCommon.ConDate(_end, 6);
      var sSplit = s.split(':');
      var eSplit = e.split(':');
      if (new Date(_start) <= new Date(_end)) {
        this.state.DataDetail[this.state.Index].TotalTime =
          (new Date(2019, 1, 1, parseInt(eSplit[0]), parseInt(eSplit[1])) -
            new Date(2019, 1, 1, parseInt(sSplit[0]), parseInt(sSplit[1]))) /
          (60 * 60 * 1000);
      } else {
        this.state.DataDetail[this.state.Index].TotalTime =
          (new Date(2019, 1, 2, hours, minutes) -
            new Date(2019, 1, 1, parseInt(sSplit[0]), parseInt(sSplit[1])) +
            (new Date(2019, 1, 2, parseInt(eSplit[0]), parseInt(eSplit[1])) -
              new Date(2019, 1, 2, hours, minutes))) /
          (60 * 60 * 1000);
      }
    }
    this.setState({DataDetail: this.state.DataDetail});
  };
  setActualStartTime = time => {
    this.state.DataDetail[this.state.Index].ActualStartTime = time;
    var _start = time;
    var _end = this.state.DataDetail[this.state.Index].ActualEndTime;
    if (_start !== '' && _end !== '') {
      var hours = new Date('2019-12-11 23:59:00').getHours();
      var minutes = new Date('2019-12-11 24:00:00').getMinutes();
      var s = FuncCommon.ConDate(_start, 6);
      var e = FuncCommon.ConDate(_end, 6);
      var sSplit = s.split(':');
      var eSplit = e.split(':');
      if (new Date(_start) <= new Date(_end)) {
        this.state.DataDetail[this.state.Index].ActualTotalTime =
          (new Date(2019, 1, 1, parseInt(eSplit[0]), parseInt(eSplit[1])) -
            new Date(2019, 1, 1, parseInt(sSplit[0]), parseInt(sSplit[1]))) /
          (60 * 60 * 1000);
      } else {
        this.state.DataDetail[this.state.Index].ActualTotalTime =
          (new Date(2019, 1, 2, hours, minutes) -
            new Date(2019, 1, 1, parseInt(sSplit[0]), parseInt(sSplit[1])) +
            (new Date(2019, 1, 2, parseInt(eSplit[0]), parseInt(eSplit[1])) -
              new Date(2019, 1, 2, hours, minutes))) /
          (60 * 60 * 1000);
      }
    }
    this.setState({DataDetail: this.state.DataDetail});
  };
  setActualEndTime = time => {
    this.state.DataDetail[this.state.Index].ActualEndTime = time;
    var _end = time;
    var _start = this.state.DataDetail[this.state.Index].ActualStartTime;
    if (_start !== '' && _end !== '') {
      var hours = new Date('2019-12-11 23:59:00').getHours();
      var minutes = new Date('2019-12-11 24:00:00').getMinutes();
      var s = FuncCommon.ConDate(_start, 6);
      var e = FuncCommon.ConDate(_end, 6);
      var sSplit = s.split(':');
      var eSplit = e.split(':');
      if (new Date(_start) <= new Date(_end)) {
        this.state.DataDetail[this.state.Index].ActualTotalTime =
          (new Date(2019, 1, 1, parseInt(eSplit[0]), parseInt(eSplit[1])) -
            new Date(2019, 1, 1, parseInt(sSplit[0]), parseInt(sSplit[1]))) /
          (60 * 60 * 1000);
      } else {
        this.state.DataDetail[this.state.Index].ActualTotalTime =
          (new Date(2019, 1, 2, hours, minutes) -
            new Date(2019, 1, 1, parseInt(sSplit[0]), parseInt(sSplit[1])) +
            (new Date(2019, 1, 2, parseInt(eSplit[0]), parseInt(eSplit[1])) -
              new Date(2019, 1, 2, hours, minutes))) /
          (60 * 60 * 1000);
      }
    }
    this.setState({DataDetail: this.state.DataDetail});
  };
  setResult = (txt, index) => {
    this.state.DataDetail[index].Result = txt;
    this.setState({DataDetail: this.state.DataDetail});
  };
  setDescriptionDetail = (txt, index) => {
    this.state.DataDetail[index].Description = txt;
    this.setState({DataDetail: this.state.DataDetail});
  };
  removeAttactment(para) {
    var list = this.state.Attachment;
    var listNewValue = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].FileName != para.FileName) {
        listNewValue.push(list[i]);
      }
    }
    this.setState({
      Attachment: listNewValue,
    });
  }
  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewForm') {
      if (this.state.ViewForm === false) {
        Animated.sequence([this.Animated_on_ViewForm]).start();
        Animated.sequence([this.Animated_off_ViewAtt]).start();
        Animated.sequence([this.Animated_off_ViewTable]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewForm]).start();
      }
      this.setState({
        ViewForm: !this.state.ViewForm,
        ViewTable: false,
        ViewAtt: false,
      });
    } else if (val === 'ViewTable') {
      if (this.state.ViewTable === false) {
        Animated.sequence([this.Animated_on_ViewTable]).start();
        Animated.sequence([this.Animated_off_ViewAtt]).start();
        Animated.sequence([this.Animated_off_ViewForm]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewTable]).start();
      }
      this.setState({
        ViewTable: !this.state.ViewTable,
        ViewForm: false,
        ViewAtt: false,
      });
    } else if (val === 'ViewAtt') {
      if (this.state.ViewAtt === false) {
        Animated.sequence([this.Animated_on_ViewAtt]).start();
        Animated.sequence([this.Animated_off_ViewTable]).start();
        Animated.sequence([this.Animated_off_ViewForm]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewAtt]).start();
      }
      this.setState({
        ViewAtt: !this.state.ViewAtt,
        ViewForm: false,
        ViewTable: false,
      });
    }
  };
  //#endregion
  //#region  combobox Employee
  _openComboboxEmployee() {}
  openComboboxEmployee = d => {
    this._openComboboxEmployee = d;
  };
  onActionComboboxEmployee(index) {
    this.setState({Index: index});
    this._openComboboxEmployee();
  }
  ChangeEmployee = rs => {
    if (rs !== null) {
      this.state.DataDetail[this.state.Index].EmployeeGuid = rs.value;
      this.state.DataDetail[this.state.Index].Employee_View = rs.text;
      this.state.DataDetail[this.state.Index].JobTitleName = rs.JobTitleName;
      this.state.DataDetail.push({
        Id: '',
        ApplyOvertimeGuid: '',
        EmployeeGuid: '',
        Employee_View: '',
        JobTitleName: '',
        ShiftID: '',
        Shift_View: '',
        WorkDayRate: '',
        StartTime: '',
        EndTime: '',
        ActualStartTime: '',
        ActualEndTime: '',
        TotalTime: '',
        ActualTotalTime: '',
        Result: '',
        ItemID: '',
        OrderNumber: '',
        Description: '',
        ItemIDByProvider: '',
      });
      this.setState({DataDetail: this.state.DataDetail});
    }
  };
  //#endregion
  //#region  combobox Shifts
  _openComboboxShifts() {}
  openComboboxShifts = d => {
    this._openComboboxShifts = d;
  };
  onActionComboboxShifts(index) {
    this.setState({Index: index});
    this._openComboboxShifts();
  }
  ChangeShifts = rs => {
    if (rs !== null) {
      this.state.DataDetail[this.state.Index].ShiftID = rs.value;
      this.state.DataDetail[this.state.Index].Shift_View = rs.text;
      this.setState({DataDetail: this.state.DataDetail});

      var obj = {Start: rs.value};
      FuncCommon.Data_Offline(async d => {
        if (d) {
          API_Operator.CheckShift(obj)
            .then(rs => {
              if (rs.data.errorCode !== 200) {
                Alert.alert('Thông báo', rs.data.message);
              } else {
                var data = JSON.parse(rs.data.data);
                if (data.Type === 'L') {
                  this.state.DataDetail[this.state.Index].WorkDayRate =
                    data.OvertimeHoliday;
                } else if (data.Type === 'T') {
                  this.state.DataDetail[this.state.Index].WorkDayRate =
                    data.OvertimeWeekend;
                } else {
                  this.state.DataDetail[this.state.Index].WorkDayRate =
                    data.OvertimeDay;
                }
                this.setState({DataDetail: this.state.DataDetail});
              }
            })
            .catch(error => {
              this.setState({loading: false, loadingsearch: false});
              console.log(error);
            });
        } else {
          Alert.alert('Thông báo', 'Yêu cầu cần có internet.');
        }
      });
    }
  };
  //#endregion
  //#region  combobox OrdersAM
  _openComboboxOrdersAM() {}
  openComboboxOrdersAM = d => {
    this._openComboboxOrdersAM = d;
  };
  onActionComboboxOrdersAM(index) {
    this.setState({Index: index});
    this._openComboboxOrdersAM();
  }
  ChangeOrdersAM = rs => {
    if (rs !== null) {
      this.state.DataDetail[this.state.Index].ItemID = rs.value;
      this.state.DataDetail[this.state.Index].ItemIDByProvider = rs.value;
      this.state.DataDetail[this.state.Index].ItemID_View = rs.text;
      this.state.DataDetail[this.state.Index].OrderNumber = rs.OrderNumber;
      this.setState({DataDetail: this.state.DataDetail});

      var obj = {Start: rs.value};
      FuncCommon.Data_Offline(async d => {
        if (d) {
          API_Operator.CheckShift(obj)
            .then(rs => {
              if (rs.data.errorCode !== 200) {
                Alert.alert('Thông báo', rs.data.message);
              } else {
                var data = JSON.parse(rs.data.data);
                if (data.Type === 'L') {
                  this.state.DataDetail[this.state.Index].WorkDayRate =
                    data.OvertimeHoliday;
                } else if (data.Type === 'T') {
                  this.state.DataDetail[this.state.Index].WorkDayRate =
                    data.OvertimeWeekend;
                } else {
                  this.state.DataDetail[this.state.Index].WorkDayRate =
                    data.OvertimeDay;
                }
                this.setState({DataDetail: this.state.DataDetail});
              }
            })
            .catch(error => {
              this.setState({loading: false, loadingsearch: false});
              console.log(error);
            });
        } else {
          Alert.alert('Thông báo', 'Yêu cầu cần có internet.');
        }
      });
    }
  };
  //#endregion

  callBackList = () => {
    Actions.pop();
  };
  //định dạng ngày
  customDatesql(strDate) {
    if (strDate != null) {
      var strSplitDate = String(strDate).split(' ');
      var date = new Date(strSplitDate[0]);
      // alert(date);
      var dd = date.getDate();
      var mm = date.getMonth() + 1; //January is 0!

      var yyyy = date.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      date = yyyy + '-' + mm + '-' + dd;
      return date.toString();
    } else {
      return '';
    }
  }
  convertDateNow(datetime) {
    try {
      if (datetime !== null) {
        var newdate = new Date(datetime);
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();
        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;
        return year + '-' + month + '-' + day;
      }
    } catch (ex) {
      return '';
    }
    return '';
  }
  convertdatetime = datetime => {
    if (datetime !== '' && datetime !== null) {
      var time = FuncCommon.ConDate(datetime, 6);
      var _time = time.toString().split(':');
      return _time[0] + ':' + _time[1];
    } else {
      return '';
    }
  };
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null) {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        return valview + ',' + string[1].substring(0, 2);
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
  //#region openAttachment
  _openCombobox_Att() {}
  openCombobox_Att = d => {
    this._openCombobox_Att = d;
  };
  openAttachment() {
    this._openCombobox_Att();
  }
  ChoiceAtt = rs => {
    if (rs.value !== null) {
      switch (rs.value) {
        case 'img':
          this.openPhotoLibrary();
          break;
        case 'file':
          this.choiceFile();
          break;
        case 'camera':
          this.ChoicePicture();
          break;
        default:
          break;
      }
    }
  };

  //#region open img
  _open() {}
  openLibrary = d => {
    this._open = d;
  };
  openPhotoLibrary() {
    this._open();
  }
  callbackLibarary = rs => {
    for (let i = 0; i < rs.length; i++) {
      this.state.Attachment.push({
        FileName: rs[i].name.toLowerCase(),
        size: rs[i].size,
        type: rs[i].type.toLowerCase(),
        uri: rs[i].uri,
      });
    }
    this.setState({Attachment: this.state.Attachment});
  };
  //#endregion

  //#region trỏ đến thư mục file trong máy
  async choiceFile() {
    try {
      const dirs = RNFetchBlob.fs.dirs;

      RNFetchBlob.fs
        .ls(dirs.PictureDir)
        .then(stats => {
          console.log('dirs:..........', stats);
        })
        .catch(err => {
          console.log('dirs:..........', err);
        });
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      var _liFiles = this.state.Attachment;
      for (let index = 0; index < res.length; index++) {
        _liFiles.push({
          FileName: res[index].name.toLowerCase(),
          size: res[index].size,
          type: res[index].type.toLowerCase(),
          uri: res[index].uri,
        });
        await RNFetchBlob.fs
          .stat(res[index].uri)
          .then(s => {
            _liFiles[_liFiles.length - 1].FileName = s.filename;
            // _liFiles[_liFiles.length - 1].type ="."+ s.filename.split(".")[s.filename.split(".").length - 1];
          })
          .catch(err => {
            console.log('Error get infor file');
          });
      }
      this.setState({Attachment: this.state.Attachment});
    } catch (err) {
      this.setState({loading: false});
      console.log('Error from pick file');
    }
  }
  //#endregion

  //#region trỏ đến camera
  async request_storage_runtime_permission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'ReactNativeCode Storage Permission',
          message:
            'ReactNativeCode App needs access to your storage to download Photos.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Storage Permission Granted.');
      } else {
        console.log('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async ChoicePicture() {
    try {
      var _liFiles = this.state.Attachment;
      await ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          //  console.log('ImagePicker Error: ', response.error);
        } else {
          var _type = response.type.split('/')[1].toLowerCase();
          _liFiles.push({
            FileName:
              response.fileName == null
                ? _type + '.' + _type
                : response.fileName.toLowerCase(),
            size: response.fileSize,
            type: response.type.toLowerCase(),
            uri: response.uri,
          });
        }
        this.setState({
          Attachment: this.state.Attachment,
        });
      });
    } catch (err) {
      if (ImagePicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  }
  //#endregion

  //#endregion
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Operator_addOverTime_Component);
