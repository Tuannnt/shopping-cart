import React, { Component } from 'react';
import { FlatList, Keyboard, ScrollView, Text, TouchableWithoutFeedback, Alert, Image, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../../utils';

const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_Employee_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            //danh sách nhân sự 
            Data_JTableEmp: {
                Status: [
                    "OM"
                ],
                length: 20,
                search: {
                    value: ""
                },
                NumberPage: 0,
                StartDate: null,
                EndDate: null,
                lstDepartmentID: []
            },
            ListEmp: [],
        };
        //#region tabbarbottom
        this.listtabbarBotom = [
            {
                Title: "Đăng ký LT",
                Icon: "clock",
                Type: "feather",
                Value: "0",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            },
            {
                Title: "Thêm LĐTV",
                Icon: "plus-square",
                Type: "feather",
                Value: "1",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            },
            {
                Title: "Cập nhật gửi BC",
                Icon: "refresh-cw",
                Type: "feather",
                Value: "2",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            }
        ];
        //#endregion
    }
    componentDidMount() {
        this.setState({ loading: true });
        this.JTableEmp(true);
    }
    JTableEmp = (s) => {
        if (s === true) {
            this.state.Data_JTableEmp.NumberPage = 1;
            this.setState(
                {
                    loadingsearch: true,
                    ListEmp: [],
                }
            );
        } else {
            this.state.Data_JTableEmp.NumberPage = ++this.state.Data_JTableEmp.NumberPage
        }
        this.setState({ Data_JTableEmp: this.state.Data_JTableEmp });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTableEmp(this.state.Data_JTableEmp).then(res => {
                    FuncCommon.Data_Offline_Set('JTableEmp_Operator', res);
                    this._GetJTableEmp_Operator(res);

                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                var x = await FuncCommon.Data_Offline_Get('JTableEmp_Operator');
                this._GetJTableEmp_Operator(x);
            }
        });
    }
    _GetJTableEmp_Operator = (res) => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            data[i].Checkbox = false;
            this.state.ListEmp.push(data[i]);
        }
        this.setState({
            ListEmp: this.state.ListEmp,
            loading: false,
            loadingsearch: false
        });
    }
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"Nhân sự"}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        <View style={{ flex: 1 }}>
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Icon name={"square-o"} type="font-awesome" color={AppColors.IconcolorTabbar} size={20} /></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Mã NV</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>Hình ảnh</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Tên nhân viên</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>TG vào</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>TG ra</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Ca</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListEmp.length > 0 ?
                                        <FlatList
                                            data={this.state.ListEmp}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => this.CheckItem(index)}>
                                                        {item.Checkbox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                        }
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.EmployeeID}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70, alignItems: "center" }]}><Image style={{ width: 60, height: 60 }} source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} /></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.FullName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.ShiftStartTime}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.ShiftEndTime}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.ShiftName}</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            onEndReached={() => this.state.ListEmp.length >= (20 * this.state.Data_JTableEmp.NumberPage) ? this.JTableEmp(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        // refreshControl={
                                        //     <RefreshControl
                                        //         refreshing={this.state.loading}
                                        //         onRefresh={this._onRefresh}
                                        //         tintColor="#f5821f"
                                        //         titleColor="#fff"
                                        //         colors={[AppColors.Maincolor]}
                                        //     />
                                        // }
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={AppStyles.StyleTabvarBottom}>
                            <TabBarBottomCustom
                                ListData={this.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    CallbackValueBottom = (key) => {
        var list = this.state.ListEmp;
        var listSub = [];
        for (let i = 0; i < list.length; i++) {
            if (list[i].Checkbox === true) {
                listSub.push(list[i]);
            }
        }
        switch (key) {
            case "0":
                if (listSub.length > 0) {
                    FuncCommon.Data_Offline(async (d) => {
                        if (d) {
                            API_Operator.GetEmployeeByDepartment({}).then(rs => {
                                if (rs.Error) {
                                    Alert.alert("Thông báo", rs.Error);
                                } else {
                                    var liEmployees = rs.data;
                                    var DataInsert = [];
                                    if (rs != null) {
                                        var liEmployeesTital = "";
                                        for (let i = 0; i < listSub.length; i++) {
                                            var obj = liEmployees.find(x => x.id === listSub[i].EmployeeGuid);
                                            if (obj === undefined) {
                                                liEmployeesTital += listSub[i].FullName + "; "
                                            } else {
                                                DataInsert.push(obj.id);
                                            }
                                        }
                                        if (liEmployeesTital != "") {
                                            Alert.alert("Thông báo", "Công nhân " + liEmployeesTital + "không cùng khối sản xuất của người đăng ký");
                                        }
                                    }
                                    Actions.Operator_addOverTime({ Data: { listEmp: liEmployees, DataDetail: DataInsert } });
                                }
                            }).catch(error => {
                                this.setState({ loading: false, loadingsearch: false });
                                console.log(error);
                            });
                        } else {
                            Alert.alert("Thông báo", "Yêu cầu cần có internet để kiểm tra phòng ban công nhân");
                        }
                    });

                } else {
                    Alert.alert("Thông báo", "Chưa có nhân viên nào được chọn");
                }
                break;
            case "1":
                Actions.Operator_addEmployee();
                break;
            case "2":
                if (listSub.length > 0) {
                    Actions.Operator_sendBCEmployee({ listEmp: listSub });
                } else {
                    Alert.alert("Thông báo", "Chưa có nhân viên nào được chọn");
                }
                break;
            default:
                break;
        }
    }
    //#region ChckItem
    CheckItem = (index) => {
        this.state.ListEmp[index].Checkbox = !this.state.ListEmp[index].Checkbox;
        this.setState({ ListEmp: this.state.ListEmp });
    }
    //#endregion
    //Các hàm dùng chung
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_Employee_Component);
