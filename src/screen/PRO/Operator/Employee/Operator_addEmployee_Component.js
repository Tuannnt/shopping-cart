import React, { Component } from 'react';
import { TouchableOpacity, Keyboard, Animated, Text, TouchableWithoutFeedback, View, Alert, TextInput, ScrollView, Dimensions, Image } from 'react-native';
import { connect } from 'react-redux';
import configApp from '../../../../configApp';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { ErrorHandler } from '@error';
import { Actions } from 'react-native-router-flux';
import { API_ApplyOverTimes, API_Operator } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import Combobox from '../../../component/Combobox';
import { FuncCommon } from '../../../../utils';

class Operator_addEmployee_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            ListJobTitle: [],
            ListDepartment: [],
            DataView: {
                EmployeeId: "",
                FullName: "",
                JobTitleId: "",
                JobTitle_View: "",
                DepartmentGuid: "",
                Department_View: "",
                Note: ""
            },
        };
    }
    componentDidMount() {
        this.setState({ loading: true });
        this.getEmployeeIdAuto();
    };

    //#region GetJobTitle
    getEmployeeIdAuto = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.getEmployeeIdAuto().then(res => {
                    if (res.data.errorCode !== 200) {
                        Alert.alert("Thông báo", res.data.message);
                    } else {
                        this.state.DataView.EmployeeId = JSON.parse(res.data.data);
                        this.setState({ DataView: this.state.DataView });
                        this.GetJobTitle();
                    }
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                Alert.alert("Thông báo", "Yêu cầu kết nối Internet");
            }
        });
    };
    //#endregion
    //#region GetJobTitle
    GetJobTitle = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetJobTitle().then(res => {
                    if (res.data.errorCode !== 200) {
                        Alert.alert("Thông báo", res.data.message);
                    } else {
                        FuncCommon.Data_Offline_Set('GetJobTitle_Operator', res);
                        this._GetJobTitle_Operator(res);
                    }
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                var x = await FuncCommon.Data_Offline_Get('GetJobTitle_Operator');
                this._GetJobTitle_Operator(x);
            }
        });
    }
    _GetJobTitle_Operator = (res) => {
        var data = JSON.parse(res.data.data);
        var list = [{
            value: "null",
            text: "Bỏ chọn"
        }]
        for (let i = 0; i < data.length; i++) {
            list.push(data[i]);
        }
        this.setState({ ListJobTitle: list });
        this.GetDepartment();
    }
    //#endregion
    //#region GetDepartment
    GetDepartment = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                var obj = {
                    Guid: null
                }
                API_Operator.GetTreeDataAdd(obj).then(res => {
                    FuncCommon.Data_Offline_Set('GetTreeDataAdd_Operator', res);
                    this._GetTreeDataAdd_Operator(res);
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                var x = await FuncCommon.Data_Offline_Get('GetTreeDataAdd_Operator');
                this._GetTreeDataAdd_Operator(x);
            }
        });
    }
    _GetTreeDataAdd_Operator = (res) => {
        var data = res.data;
        for (let i = 0; i < data.length; i++) {
            this.state.ListDepartment.push({
                value: data[i].DepartmentGuid,
                text: data[i].Title,
            })
        }
        this.setState({ ListDepartment: this.state.ListDepartment, loading: false });
    }
    //#endregion
    Submit() {
        if (this.state.DataView.FullName === "") {
            Alert.alert("Thông báo", "Bạn cần nhập đầy đủ họ và tên");
            return;
        }
        if (this.state.DataView.DepartmentGuid === "") {
            Alert.alert("Thông báo", "Bạn cần nhập phòng ban/ bộ phận");
            return;
        }

        this.setState({ modelauto: this.state.modelauto });
        var DataSub = {
            IsActive: true,
            LoginName: "",
            Kiemnhiem: false,
            MaritalStatus: null,
            EducationId: null,
            JobTitleId: this.state.DataView.JobTitleId,
            DepartmentGuid: this.state.DataView.DepartmentGuid,
            ProfestionalD: null,
            SpecializesId: null,
            ShiftID: null,
            EthnicId: null,
            CandidateId: null,
            SalaryMethod: null,
            StatusOfWork: "PT",
            BankCode: null,
            SalaryPayBy: null,
            Gender: "M",
            Uniform: null,
            Language1: [],
            imageLink: "/images/male.jpeg",
            GenitiveId: null,
            SalaryTierId: null,
            ShiftId1: [],
            easonLeaveId: null,
            OrganizationId: null,
            EmployeeId: this.state.DataView.EmployeeId,
            FullName: this.state.DataView.FullName,
            FirstName: this.fName(this.state.DataView.FullName).first,
            MiddleName: this.fName(this.state.DataView.FullName).mid,
            LastName: this.fName(this.state.DataView.FullName).last,
            HomeNumber: "",
            WardName: "",
            DistrictID: "",
            ProvinceID: "",
            CountryID: "",
            PermanentAddress: ""
        };

        var fd = new FormData();
        fd.append('Insert', JSON.stringify(DataSub));
        fd.append('PositionsConcurrently', JSON.stringify([]));
        fd.append('ShiftOfEmployees', JSON.stringify([]));
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.Insert_Employees(fd).then(res => {
                    if (res.data.Error === true) {
                        Alert.alert("Thông báo", res.data.Title);
                        this.setState({ loading: true });
                        this.getEmployeeIdAuto();
                    } else {
                        Alert.alert("Thông báo", res.data.Title);
                        this.callBackList();
                    }
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                Alert.alert("Thông báo", "Yêu cầu kết nối Internet");
            }
        });
    }
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title title="THÊM MỚI NHÂN VIÊN"
                            callBack={() => this.callBackList()}
                        />
                        <Divider />
                        <View style={{ flex: 1, padding: 10, flexDirection: 'column' }}>
                            <View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={AppStyles.Labeldefault}>Mã Nhân viên</Text>
                                </View>
                                <View style={[AppStyles.FormInput]}>
                                    <Text style={[AppStyles.TextInput]}>{this.state.DataView.EmployeeId}</Text>
                                </View>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={AppStyles.Labeldefault}>Họ và tên</Text>
                                    <Text style={[AppStyles.Labeldefault, { color: AppColors.red }]}> *</Text>
                                </View>
                                <TextInput style={AppStyles.FormInput}
                                    value={this.state.DataView.FullName}
                                    autoCapitalize="none"
                                    placeholder="Nhập họ và tên"
                                    onChangeText={(txt) => this.setFullName(txt)} />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={AppStyles.Labeldefault}>Chức danh</Text>
                                </View>
                                <TouchableOpacity style={AppStyles.FormInput} onPress={() => this.onActionComboboxJobTitle()}>
                                    <Text style={[AppStyles.Textdefault, this.state.DataView.JobTitleId === "" ? { color: AppColors.gray } : { color: AppColors.black }]}>
                                        {this.state.DataView.JobTitleId !== "" ? this.state.DataView.JobTitle_View : "Chọn chức vụ"}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={AppStyles.Labeldefault}>Phòng ban/Bộ phận</Text>
                                    <Text style={[AppStyles.Labeldefault, { color: AppColors.red }]}> *</Text>
                                </View>
                                <TouchableOpacity style={AppStyles.FormInput} onPress={() => this.onActionComboboxDepartment()}>
                                    <Text style={[AppStyles.Textdefault, this.state.DataView.DepartmentGuid === "" ? { color: AppColors.gray } : { color: AppColors.black }]}>
                                        {this.state.DataView.DepartmentGuid !== "" ? this.state.DataView.Department_View : "Chọn phòng ban/ bộ phận"}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                                </View>
                                <TextInput
                                    style={[AppStyles.FormInput, { maxHeight: 100 }]}
                                    underlineColorAndroid="transparent"
                                    value={this.state.DataView.Note}
                                    numberOfLines={5}
                                    multiline={true}
                                    onChangeText={(txt) => this.setNote(txt)}
                                />
                            </View>
                            {
                                this.state.LiFiles && this.state.LiFiles.length > 0 ? this.renderSelectedFile() : null
                            }
                        </View>
                        <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6' }]} onPress={() => this.Submit()}>
                            <Text style={[AppStyles.Textdefault, { color: 'black' }]}>LƯU</Text>
                        </TouchableOpacity>
                        {this.state.ListJobTitle.length > 0 ?
                            <Combobox
                                value={this.state.DataView.JobTitleId}
                                TypeSelect={"single"}// single or multiple
                                callback={this.ChangeJobTitle}
                                data={this.state.ListJobTitle}
                                nameMenu={'Chọn chức danh'}
                                eOpen={this.openComboboxJobTitle}
                                position={'bottom'}
                            />
                            : null
                        }
                        {this.state.ListDepartment.length > 0 ?
                            <Combobox
                                value={this.state.DataView.DepartmentGuid}
                                TypeSelect={"single"}// single or multiple
                                callback={this.ChangeDepartment}
                                data={this.state.ListDepartment}
                                nameMenu={'Chọn phòng ban/ bộ phận'}
                                eOpen={this.openComboboxDepartment}
                                position={'bottom'}
                            />
                            : null
                        }
                    </View >
                </TouchableWithoutFeedback>
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    setFullName = (txt) => {
        this.state.DataView.FullName = txt;
        this.setState({ DataView: this.state.DataView });
    };
    setNote = (txt) => {
        this.state.DataView.Note = txt;
        this.setState({ DataView: this.state.DataView });
    };
    //#region  combobox JobTitle
    _openComboboxJobTitle() { }
    openComboboxJobTitle = (d) => {
        this._openComboboxJobTitle = d;
    }
    onActionComboboxJobTitle() {
        this._openComboboxJobTitle();
    }
    ChangeJobTitle = (rs) => {
        if (rs !== null) {
            this.state.DataView.JobTitleId = rs.value !== "null" ? parseInt(rs.value) : "";
            this.state.DataView.JobTitle_View = rs.value !== "null" ? rs.text : "";
            this.setState({ DataView: this.state.DataView });
        }
    };
    //#endregion
    //#region  combobox Department
    _openComboboxDepartment() { }
    openComboboxDepartment = (d) => {
        this._openComboboxDepartment = d;
    }
    onActionComboboxDepartment() {
        this._openComboboxDepartment();
    }
    ChangeDepartment = (rs) => {
        if (rs !== null) {
            this.state.DataView.DepartmentGuid = rs.value;
            this.state.DataView.Department_View = rs.text;
            this.setState({ DataView: this.state.DataView });
        }
    }
    //#endregion

    callBackList = () => {
        Actions.pop();
    }
    fName = (name) => {
        try {
            if (name.length <= 0) {
                return { first: "", mid: "", last: "" };
            } else {
                var xtemp = name.split(" ");
                var itemp = xtemp.length;
                if (itemp == 1) {
                    return { first: xtemp[0], mid: "", last: "" };
                }
                if (itemp == 2) {
                    return { first: xtemp[0], mid: "", last: xtemp[1] };
                }
                var midName = "";
                for (var i = 1; i < itemp - 1; i++) {
                    if (i == itemp - 2) {
                        midName += xtemp[i];
                    } else {
                        midName += xtemp[i] + " ";
                    }

                }

                return { first: xtemp[0], mid: midName, last: xtemp[itemp - 1] };
            }
        } catch (ex) {
            return { first: "", mid: "", last: "" };
        }
    }

}
const mapStateToProps = state => ({
})
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_addEmployee_Component);
