import React, {Component} from 'react';
import {
  TouchableOpacity,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  View,
  Alert,
  TextInput,
  ScrollView,
  Dimensions,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import configApp from '../../../../configApp';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {ErrorHandler} from '@error';
import {Actions} from 'react-native-router-flux';
import {API_Operator, API_HR} from '../../../../network';
import TabBar_Title from '../../../component/TabBar_Title';
import {Divider, Icon} from 'react-native-elements';
import DatePicker from 'react-native-date-picker';
import LoadingComponent from '../../../component/LoadingComponent';
import Combobox from '../../../component/Combobox';
import {FuncCommon} from '../../../../utils';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

class Operator_sendBCEmployee_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      CheckAprove: true,
      DataView: {
        StartTime: '',
        EndTime: '',
        Date: new Date(),
        ShiftId: '',
        Shift_View: '',
        Status: 'Y',
        Status_View: '',
        Comment: '',
      },
      //Hiển thị chi tiết
      ViewTable: false,
      transIcon_ViewTable: new Animated.Value(0),
      ListShifts: [],
      ListStatus: [
        {value: 'Y', text: 'Có mặt'},
        {value: 'N', text: 'Vắng mặt'},
      ],
      offsetdate: false,
      offsetStartTime: false,
      offsetEndTime: false,
      offsetStartTime_Detail: false,
      offsetEndTime_Detail: false,
      DataDetail: [],
      Index: null,
    };
  }
  componentDidMount() {
    this.setState({loading: true});
    this.Skill();
    this.GetShifts();
    this.setDataDetail(this.props.listEmp);
  }
  //#region Skill
  Skill = () => {
    //#region đóng mở tab
    this.Animated_on_ViewTable = Animated.timing(
      this.state.transIcon_ViewTable,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ViewTable = Animated.timing(
      this.state.transIcon_ViewTable,
      {toValue: 0, duration: 333},
    );
    //#endregion
  };
  //#endregion
  //#region GetShifts
  GetShifts = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetShifts()
          .then(res => {
            if (res.data.errorCode !== 200) {
              Alert.alert('Thông báo', res.data.message);
            } else {
              FuncCommon.Data_Offline_Set('GetShifts_Operator', res);
              this._GetShifts_Operator(res);
            }
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetShifts_Operator');
        this._GetShifts_Operator(x);
      }
    });
  };
  _GetShifts_Operator = res => {
    this.setState({ListShifts: JSON.parse(res.data.data)});
  };
  //#endregion
  //#region setDataDetail
  setDataDetail = data => {
    for (let i = 0; i < data.length; i++) {
      this.state.DataDetail.push({
        Id: data[i].EmployeeGuid,
        EmployeeId: data[i].EmployeeID,
        EmployeeName: data[i].FullName,
        CheckIn: '',
        CheckOut: '',
        ShiftId: data[i].ShiftId,
        Shift_View: '',
        Status: 'Y',
        Status_View: '',
        Comment: '',
        CheckAprove: true,
      });
    }
    this.setState({DataDetail: this.state.DataDetail, loading: false});
  };
  //#endregion
  render() {
    const rotateStart_ViewTable = this.state.transIcon_ViewTable.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar_Title
            title="CẬP NHẬT GỬI BÁO CÁO NHÂN SỰ"
            callBack={() => this.callBackList()}
          />
          <Divider />
          {this.state.ViewTable === false ? (
            <View style={{padding: 10, flexDirection: 'column'}}>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1, paddingRight: 2}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Bắt đầu</Text>
                  </View>
                  <TouchableOpacity
                    style={[AppStyles.FormInput, {justifyContent: 'center'}]}
                    onPress={() => this.openFormDateTime('StartTime')}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.convertdatetime(this.state.DataView.StartTime)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, paddingLeft: 2}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Kết thúc</Text>
                  </View>
                  <TouchableOpacity
                    style={[AppStyles.FormInput, {justifyContent: 'center'}]}
                    onPress={() => this.openFormDateTime('EndTime')}>
                    <Text style={[AppStyles.Textdefault]}>
                      {this.convertdatetime(this.state.DataView.EndTime)}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{flex: 1, paddingRight: 2}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Ngày chấm công</Text>
                    <Text
                      style={[AppStyles.Labeldefault, {color: AppColors.red}]}>
                      {' '}
                      *
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={[AppStyles.FormInput, {justifyContent: 'center'}]}
                    onPress={() => this.openFormDateTime('Date')}>
                    <Text style={[AppStyles.Textdefault]}>
                      {FuncCommon.ConDate(this.state.DataView.Date, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, paddingLeft: 2}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={AppStyles.Labeldefault}>Trạng thái</Text>
                  </View>
                  <TouchableOpacity
                    style={AppStyles.FormInput}
                    onPress={() => {
                      this.onActionComboboxStatus();
                    }}>
                    <Text
                      style={[
                        AppStyles.Textdefault,
                        this.state.DataView.Status === ''
                          ? {color: AppColors.gray}
                          : {color: AppColors.black},
                      ]}>
                      {this.state.DataView.Status !== ''
                        ? this.state.DataView.Status_View
                        : 'Chọn trạng thái'}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={{marginTop: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Ca</Text>
                  <Text
                    style={[AppStyles.Labeldefault, {color: AppColors.red}]}>
                    {' '}
                    *
                  </Text>
                </View>
                <TouchableOpacity
                  style={AppStyles.FormInput}
                  onPress={() => {
                    this.onActionComboboxShifts();
                  }}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      this.state.DataView.ShiftId === ''
                        ? {color: AppColors.gray}
                        : {color: AppColors.black},
                    ]}>
                    {this.state.DataView.ShiftId !== ''
                      ? this.state.DataView.Shift_View
                      : 'Chọn ca làm việc'}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={{marginTop: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                </View>
                <TextInput
                  style={[AppStyles.FormInput, {maxHeight: 100}]}
                  underlineColorAndroid="transparent"
                  value={this.state.DataView.Comment}
                  onChangeText={txt => this.setComment(txt)}
                />
              </View>
              <TouchableOpacity
                style={[
                  AppStyles.FormInput,
                  {
                    justifyContent: 'center',
                    margin: 10,
                    backgroundColor: '#bed9f6',
                  },
                ]}
                onPress={() => this.AddDataAll()}>
                <Text style={[AppStyles.Textdefault, {color: 'black'}]}>
                  Cập nhật
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#bed9f6',
            }}
            onPress={() => this.setViewOpen('ViewTable')}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={AppStyles.Labeldefault}>CHI TIẾT</Text>
            </View>
            <Animated.View
              style={{
                transform: [
                  {rotate: rotateStart_ViewTable},
                  {perspective: 4000},
                ],
                paddingRight: 10,
                paddingLeft: 10,
              }}>
              <Icon type={'feather'} name={'chevron-up'} />
            </Animated.View>
          </TouchableOpacity>
          <Divider />
          {this.state.ViewTable === true ? (
            <View style={{flex: 1}}>
              {/* Table */}
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'column', flex: 1}}>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                      <Text style={AppStyles.Labeldefault}>STT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 50}]}
                      onPress={() => this.setAllCheckAprove()}>
                      {this.state.CheckAprove ? (
                        <Icon
                          name={'check'}
                          type="entypo"
                          color={AppColors.ColorEdit}
                          size={20}
                        />
                      ) : (
                        <Icon
                          name={'square-o'}
                          type="font-awesome"
                          color={AppColors.IconcolorTabbar}
                          size={20}
                        />
                      )}
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Mã nhân viên</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 70}]}>
                      <Text style={AppStyles.Labeldefault}>Hình ảnh</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 150}]}>
                      <Text style={AppStyles.Labeldefault}>Tên nhân viên</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>TG vào</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                      <Text style={AppStyles.Labeldefault}>TG ra</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Ca</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 100}]}>
                      <Text style={AppStyles.Labeldefault}>Trạng thái</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 200}]}>
                      <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                    </TouchableOpacity>
                  </View>
                  {this.state.DataDetail.map((item, index) => (
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[
                          AppStyles.table_td,
                          {width: 40, alignItems: 'center'},
                        ]}>
                        <Text style={AppStyles.Labeldefault}>{index + 1}</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 50}]}
                        onPress={() => this.setCheckAprove(index)}>
                        {item.CheckAprove ? (
                          <Icon
                            name={'check'}
                            type="entypo"
                            color={AppColors.ColorEdit}
                            size={20}
                          />
                        ) : (
                          <Icon
                            name={'square-o'}
                            type="font-awesome"
                            color={AppColors.IconcolorTabbar}
                            size={20}
                          />
                        )}
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 100}]}>
                        <Text style={AppStyles.Textdefault}>
                          {item.EmployeeId}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[
                          AppStyles.table_td,
                          {width: 70, alignItems: 'center'},
                        ]}>
                        <Image
                          style={{width: 60, height: 60}}
                          source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 150}]}>
                        <Text style={AppStyles.Textdefault}>
                          {item.EmployeeName}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 80}]}>
                        <TouchableOpacity
                          style={[AppStyles.FormInput, {alignItems: 'center'}]}
                          onPress={() =>
                            this.openFormDateTime_Detail(index, 'StartTime')
                          }>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.convertdatetime(item.CheckIn)}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 80}]}>
                        <TouchableOpacity
                          style={[AppStyles.FormInput, {alignItems: 'center'}]}
                          onPress={() =>
                            this.openFormDateTime_Detail(index, 'EndTime')
                          }>
                          <Text style={[AppStyles.Textdefault]}>
                            {this.convertdatetime(item.CheckOut)}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 100}]}>
                        <TouchableOpacity
                          style={AppStyles.FormInput}
                          onPress={() => {
                            this.onActionComboboxShifts_Detail(index);
                          }}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              item.ShiftId === ''
                                ? {color: AppColors.gray}
                                : {color: AppColors.black},
                            ]}>
                            {item.ShiftId !== ''
                              ? item.Shift_View
                              : 'Ca đăng ký'}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 100}]}>
                        <TouchableOpacity
                          style={AppStyles.FormInput}
                          onPress={() => {
                            this.onActionComboboxStatus_Detail(index);
                          }}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              item.Status === ''
                                ? {color: AppColors.gray}
                                : {color: AppColors.black},
                            ]}>
                            {item.Status !== ''
                              ? item.Status_View
                              : 'Ca đăng ký'}
                          </Text>
                        </TouchableOpacity>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_td, {width: 200}]}>
                        <TextInput
                          style={AppStyles.FormInput}
                          underlineColorAndroid="transparent"
                          value={item.Comment}
                          autoCapitalize="none"
                          onChangeText={txt =>
                            this.setComment_Detail(txt, index)
                          }
                        />
                      </TouchableOpacity>
                    </View>
                  ))}
                </View>
              </ScrollView>
            </View>
          ) : null}
          <TouchableOpacity
            style={[
              AppStyles.FormInput,
              {
                justifyContent: 'center',
                margin: 10,
                backgroundColor: '#bed9f6',
              },
            ]}
            onPress={() => this.Submit()}>
            <Text style={[AppStyles.Textdefault, {color: 'black'}]}>LƯU</Text>
          </TouchableOpacity>
          {this.state.ListShifts.length > 0 ? (
            <Combobox
              value={this.state.DataView.ShiftId}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeShifts}
              data={this.state.ListShifts}
              nameMenu={'Chọn ca làm việc'}
              eOpen={this.openComboboxShifts}
              position={'bottom'}
            />
          ) : null}
          {this.state.ListShifts.length > 0 ? (
            <Combobox
              value={''}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeShifts_Detail}
              data={this.state.ListShifts}
              nameMenu={'Chọn ca làm việc'}
              eOpen={this.openComboboxShifts_Detail}
              position={'bottom'}
            />
          ) : null}
          <Combobox
            value={this.state.DataView.Status}
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeStatus}
            data={this.state.ListStatus}
            nameMenu={'Chọn Trạng thái'}
            eOpen={this.openComboboxStatus}
            position={'bottom'}
          />
          <Combobox
            value={''}
            TypeSelect={'single'} // single or multiple
            callback={this.ChangeStatus_Detail}
            data={this.state.ListStatus}
            nameMenu={'Chọn trạng thái'}
            eOpen={this.openComboboxStatus_Detail}
            position={'bottom'}
          />
          {this.state.offsetdate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetdate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataView.Date === ''
                    ? new Date()
                    : this.state.DataView.Date
                }
                mode="date"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.offsetStartTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetStartTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataView.StartTime === ''
                    ? new Date()
                    : this.state.DataView.StartTime
                }
                mode="time"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setStartTime(setDate)}
              />
            </View>
          ) : null}
          {this.state.offsetEndTime === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetEndTime: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataView.EndTime === ''
                    ? new Date()
                    : this.state.DataView.EndTime
                }
                mode="time"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setEndTime(setDate)}
              />
            </View>
          ) : null}
          {this.state.offsetStartTime_Detail === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetStartTime_Detail: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataDetail[this.state.Index].CheckIn === ''
                    ? new Date()
                    : this.state.DataDetail[this.state.Index].CheckIn
                }
                mode="time"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setStartTime_Detail(setDate)}
              />
            </View>
          ) : null}
          {this.state.offsetEndTime_Detail === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetEndTime_Detail: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.DataDetail[this.state.Index].CheckOut === ''
                    ? new Date()
                    : this.state.DataDetail[this.state.Index].CheckOut
                }
                mode="time"
                style={{width: DRIVER.width}}
                onDateChange={setDate => this.setsetEndTime_Detail(setDate)}
              />
            </View>
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }

  //#region đóng mở các tab
  setViewOpen = val => {
    if (val === 'ViewTable') {
      if (this.state.ViewTable === false) {
        Animated.sequence([this.Animated_on_ViewTable]).start();
      } else {
        Animated.sequence([this.Animated_off_ViewTable]).start();
      }
      this.setState({ViewTable: !this.state.ViewTable});
    }
  };
  //#endregion

  setComment = txt => {
    this.state.DataView.Comment = txt;
    this.setState({DataView: this.state.DataView});
  };
  setDate = date => {
    this.state.DataView.Date = date;
    this.setState({DataView: this.state.DataView});
  };
  setStartTime = date => {
    this.state.DataView.StartTime = date;
    this.setState({DataView: this.state.DataView});
  };
  setEndTime = date => {
    this.state.DataView.EndTime = date;
    this.setState({DataView: this.state.DataView});
  };
  setComment_Detail = (txt, i) => {
    this.state.DataDetail[i].Comment = txt;
    this.setState({DataDetail: this.state.DataDetail});
  };
  //#region  combobox Shifts
  _openComboboxShifts() {}
  openComboboxShifts = d => {
    this._openComboboxShifts = d;
  };
  onActionComboboxShifts() {
    this._openComboboxShifts();
  }
  ChangeShifts = rs => {
    if (rs !== null) {
      this.state.DataView.ShiftId = rs.value;
      this.state.DataView.Shift_View = rs.text;
      this.setState({DataView: this.state.DataView});
    }
  };
  //#endregion
  //#region  combobox Status
  _openComboboxStatus() {}
  openComboboxStatus = d => {
    this._openComboboxStatus = d;
  };
  onActionComboboxStatus() {
    this._openComboboxStatus();
  }
  ChangeStatus = rs => {
    if (rs !== null) {
      this.state.DataView.Status = rs.value;
      this.state.DataView.Status_View = rs.text;
      this.setState({DataView: this.state.DataView});
    }
  };
  //#endregion
  //#region OpenFormDate
  openFormDateTime = para => {
    switch (para) {
      case 'StartTime':
        this.setState({
          offsetStartTime: true,
          offsetEndTime: false,
          offsetdate: false,
        });
        break;
      case 'EndTime':
        this.setState({
          offsetStartTime: false,
          offsetEndTime: true,
          offsetdate: false,
        });
        break;
      case 'Date':
        this.setState({
          offsetStartTime: false,
          offsetEndTime: false,
          offsetdate: true,
        });
        break;
      default:
        break;
    }
  };
  openFormDateTime_Detail = (i, para) => {
    this.setState({Index: i});
    switch (para) {
      case 'StartTime':
        this.setState({
          offsetStartTime_Detail: true,
          offsetEndTime_Detail: false,
        });
        break;
      case 'EndTime':
        this.setState({
          offsetStartTime_Detail: false,
          offsetEndTime_Detail: true,
        });
        break;
      default:
        break;
    }
  };
  //#endregion
  setStartTime_Detail = time => {
    this.state.DataDetail[this.state.Index].CheckIn = time;
    this.setState({DataDetail: this.state.DataDetail});
  };
  setsetEndTime_Detail = time => {
    this.state.DataDetail[this.state.Index].CheckOut = time;
    this.setState({DataDetail: this.state.DataDetail});
  };
  //#region  combobox Shifts_Detail
  _openComboboxShifts_Detail() {}
  openComboboxShifts_Detail = d => {
    this._openComboboxShifts_Detail = d;
  };
  onActionComboboxShifts_Detail(index) {
    this.setState({Index: index});
    this._openComboboxShifts_Detail();
  }
  ChangeShifts_Detail = rs => {
    if (rs !== null) {
      this.state.DataDetail[this.state.Index].ShiftId = rs.value;
      this.state.DataDetail[this.state.Index].Shift_View = rs.text;
      this.setState({DataDetail: this.state.DataDetail});
    }
  };
  //#endregion
  //#region  combobox Status_Detail
  _openComboboxStatus_Detail() {}
  openComboboxStatus_Detail = d => {
    this._openComboboxStatus_Detail = d;
  };
  onActionComboboxStatus_Detail(index) {
    this.setState({Index: index});
    this._openComboboxStatus_Detail();
  }
  ChangeStatus_Detail = rs => {
    if (rs !== null) {
      this.state.DataDetail[this.state.Index].Status = rs.value;
      this.state.DataDetail[this.state.Index].Status_View = rs.text;
      this.setState({DataDetail: this.state.DataDetail});
    }
  };
  //#endregion
  callBackList = () => {
    Actions.pop();
  };

  convertdatetime = datetime => {
    if (datetime !== '' && datetime !== null) {
      var time = FuncCommon.ConDate(datetime, 6);
      var _time = time.toString().split(':');
      return _time[0] + ':' + _time[1];
    } else {
      return '';
    }
  };
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null) {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        return valview + ',' + string[1].substring(0, 2);
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
  //#region AddDataAll
  AddDataAll = () => {
    var data = this.state.DataView;
    var list = this.state.DataDetail;
    for (let i = 0; i < list.length; i++) {
      list[i].CheckIn = data.StartTime;
      list[i].CheckOut = data.EndTime;
      list[i].ShiftId = data.ShiftId;
      list[i].Shift_View = data.Shift_View;
      list[i].Status = data.Status;
      list[i].Status_View = data.Status_View;
      list[i].Comment = data.Comment;
    }
    this.setState({DataDetail: list});
  };
  //#endregion
  //#region setCheckAprove
  setCheckAprove = i => {
    this.state.DataDetail[i].CheckAprove = !this.state.DataDetail[i]
      .CheckAprove;
    this.setState({DataDetail: this.state.DataDetail});
    var obj = this.state.DataDetail.find(x => x.CheckAprove === false);
    if (obj !== undefined) {
      this.setState({CheckAprove: false});
    } else {
      this.setState({CheckAprove: true});
    }
  };
  setAllCheckAprove = () => {
    for (let i = 0; i < this.state.DataDetail.length; i++) {
      this.state.DataDetail[i].CheckAprove = !this.state.CheckAprove;
    }
    this.setState({
      DataDetail: this.state.DataDetail,
      CheckAprove: !this.state.CheckAprove,
    });
  };
  //#endregion
  //#region Submit
  Submit = () => {
    if (this.state.DataView.ShiftId === '') {
      Alert.alert('Thông báo', 'Yêu cầu chọn ca làm việc');
      return;
    }
    var listSub = [];
    for (let i = 0; i < this.state.DataDetail.length; i++) {
      if (this.state.DataDetail[i].CheckAprove === true) {
        var value = this.state.DataDetail[i];
        listSub.push({
          EmployeeGuid: value.Id,
          StartTime: this.convertdatetime(value.CheckIn),
          EndTime: this.convertdatetime(value.CheckOut),
          Date: this.state.DataView.Date,
          ShiftId: parseInt(value.ShiftId),
          Status: value.Status,
          Comment: value.Comment,
        });
      }
    }
    if (listSub.length > 0) {
      Alert.alert(
        'Xác nhận',
        'Bạn có chắc chắn muốn gửi báo cáo nhân sự không ?',
        [
          {text: 'Xác nhận', onPress: () => this.APISubmit(listSub)},
          {
            text: 'Đóng',
            onPress: () => null,
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    }
  };
  APISubmit = listSub => {
    var fd = new FormData();
    fd.append('submit', JSON.stringify(listSub));
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.sendBCEmployee(fd)
          .then(res => {
            if (res.data.errorCode !== 200) {
              Alert.alert('Thông báo', res.data.message);
            } else {
              Alert.alert('Thông báo', res.data.message);
              this.callBackList();
            }
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        Alert.alert('Thông báo', 'Yêu cầu kết nối Internet');
      }
    });
  };
  //#endregion
}
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Operator_sendBCEmployee_Component);
