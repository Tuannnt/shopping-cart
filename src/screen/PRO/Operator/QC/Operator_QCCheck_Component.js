import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
  TextInput,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {connect} from 'react-redux';
import {API_Operator, API_HR, API} from '../../../../network';
import TabBar_Title from '../../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../../../utils';
import Combobox from '../../../component/Combobox';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class Operator_QCCheck_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ListReasons: [],
      ListErrors: [],
      loading: false,
      listEMP: [],
      listMachines: [],
      loadingsearch: false,
      gmodel: null,
      model: null,
      jmodel: {},
      FullName: '',
      DataTable: {
        NumberPage: 0,
        Length: 30,
      },
      ListView: [],
      modelday: {
        Day2: this.setmodelday(this.CalenderReturnDate(new Date()).sdate),
        Day3: this.setmodelday(
          this.addDays(this.CalenderReturnDate(new Date()).sdate, 1),
        ),
        Day4: this.setmodelday(
          this.addDays(this.CalenderReturnDate(new Date()).sdate, 2),
        ),
        Day5: this.setmodelday(
          this.addDays(this.CalenderReturnDate(new Date()).sdate, 3),
        ),
        Day6: this.setmodelday(
          this.addDays(this.CalenderReturnDate(new Date()).sdate, 4),
        ),
        Day7: this.setmodelday(
          this.addDays(this.CalenderReturnDate(new Date()).sdate, 5),
        ),
        Day8: this.setmodelday(this.CalenderReturnDate(new Date()).edate),
      },
      modelsx: {
        Quantity: 0,
        Quantity2: 0,
        Quantity3: 0,
        Quantity4: 0,
        Quantity5: 0,
        Quantity6: 0,
        Quantity7: 0,
        Quantity8: 0,
      },
      modelkt: {
        Quantity: 0,
        Quantity2: 0,
        Quantity3: 0,
        Quantity4: 0,
        Quantity5: 0,
        Quantity6: 0,
        Quantity7: 0,
        Quantity8: 0,
      },
      modelng: {
        Quantity: 0,
        Quantity2: 0,
        Quantity3: 0,
        Quantity4: 0,
        Quantity5: 0,
        Quantity6: 0,
        Quantity7: 0,
        Quantity8: 0,
      },
    };
    //#region tabbarbottom
    this.listtabbarBotom = [
      {
        Title: 'List CV đang SX',
        Icon: 'list',
        Type: 'feather',
        Value: '0',
        Checkbox: false,
        OffEditcolor: true,
        Badge: 0,
      },
      {
        Title: 'Báo cáo- vấn đề SX',
        Icon: 'bell',
        Type: 'feather',
        Value: '1',
        Checkbox: false,
        OffEditcolor: true,
        Badge: 0,
      },
      {
        Title: 'Báo dừng SX',
        Icon: 'bell',
        Type: 'feather',
        Value: '2',
        Checkbox: false,
        OffEditcolor: true,
        Badge: 0,
      },
    ];
    //#endregion
  }
  componentDidMount() {
    this.JTablePlanJobCardsBaoCao(true);
    this.GetErrors();
    this.QCCheck();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'QRCode') {
      var Id = nextProps.Data.Value.data; // dữ liệu QR quét được
      this.GetItemPlanJobCards(Id);
    }
  }
  //#region QCCheck
  QCCheck = () => {
    var obj1 = {
      DepartmentId: null,
      ItemId: null,
      Type: 1,
    };
    var obj2 = {
      DepartmentId: null,
      ItemId: null,
      Type: 2,
    };
    var obj3 = {
      DepartmentId: null,
      ItemId: null,
      Type: 2,
    };
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.QCCheck(obj1)
          .then(res => {
            console.log(res);
            FuncCommon.Data_Offline_Set('QCCheck_QCCheck' + 'Type:1', res);
            this._GetQCCheckSX(res);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });

        API_Operator.QCCheck(obj2)
          .then(rs => {
            console.log(rs);
            FuncCommon.Data_Offline_Set('QCCheck_QCCheck' + 'Type:2', rs);
            this._GetQCCheckKT(rs);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });

        API_Operator.QCCheck(obj3)
          .then(r => {
            console.log(r);
            FuncCommon.Data_Offline_Set('QCCheck_QCCheck' + 'Type:3', r);
            this._GetQCCheckNG(r);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('QCCheck_QCCheck' + 'Type:1');
        this._GetQCCheckSX(x);
        var y = await FuncCommon.Data_Offline_Get('QCCheck_QCCheck' + 'Type:2');
        this._GetQCCheckKT(y);
        var z = await FuncCommon.Data_Offline_Get('QCCheck_QCCheck' + 'Type:3');
        this._GetQCCheckNG(z);
      }
    });
  };
  _GetQCCheckSX = res => {
    var rs = JSON.parse(res.data.data);
    this.state.modelsx.Quantity =
      rs.Quantity2 +
      rs.Quantity3 +
      rs.Quantity4 +
      rs.Quantity5 +
      rs.Quantity6 +
      rs.Quantity7 +
      rs.Quantity8;
    this.state.modelsx.Quantity2 = rs.Quantity2;
    this.state.modelsx.Quantity3 = rs.Quantity3;
    this.state.modelsx.Quantity4 = rs.Quantity4;
    this.state.modelsx.Quantity5 = rs.Quantity5;
    this.state.modelsx.Quantity6 = rs.Quantity6;
    this.state.modelsx.Quantity7 = rs.Quantity7;
    this.state.modelsx.Quantity8 = rs.Quantity8;
    this.setState({modelsx: this.state.modelsx});
  };
  _GetQCCheckKT = res => {
    var rs = JSON.parse(res.data.data);
    this.state.modelkt.Quantity =
      rs.Quantity2 +
      rs.Quantity3 +
      rs.Quantity4 +
      rs.Quantity5 +
      rs.Quantity6 +
      rs.Quantity7 +
      rs.Quantity8;
    this.state.modelkt.Quantity2 = rs.Quantity2;
    this.state.modelkt.Quantity3 = rs.Quantity3;
    this.state.modelkt.Quantity4 = rs.Quantity4;
    this.state.modelkt.Quantity5 = rs.Quantity5;
    this.state.modelkt.Quantity6 = rs.Quantity6;
    this.state.modelkt.Quantity7 = rs.Quantity7;
    this.state.modelkt.Quantity8 = rs.Quantity8;
    this.setState({modelkt: this.state.modelkt});
  };
  _GetQCCheckNG = res => {
    var rs = JSON.parse(res.data.data);
    this.state.modelng.Quantity =
      rs.Quantity2 +
      rs.Quantity3 +
      rs.Quantity4 +
      rs.Quantity5 +
      rs.Quantity6 +
      rs.Quantity7 +
      rs.Quantity8;
    this.state.modelng.Quantity2 = rs.Quantity2;
    this.state.modelng.Quantity3 = rs.Quantity3;
    this.state.modelng.Quantity4 = rs.Quantity4;
    this.state.modelng.Quantity5 = rs.Quantity5;
    this.state.modelng.Quantity6 = rs.Quantity6;
    this.state.modelng.Quantity7 = rs.Quantity7;
    this.state.modelng.Quantity8 = rs.Quantity8;
    this.setState({modelng: this.state.modelng});
  };
  //#endregion
  //#region GetErrors
  GetErrors = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        var obj = {Type: 'L'};
        API_Operator.GetErrors(obj)
          .then(res => {
            FuncCommon.Data_Offline_Set('GetErrors_QCCheck' + 'Type:L', res);
            this._GetListErrors(res);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
        var objListReasons = {Type: 'N'};
        API_Operator.GetErrors(objListReasons)
          .then(rs => {
            FuncCommon.Data_Offline_Set('GetErrors_QCCheck' + 'Type:N', rs);
            this._GetListReasons(rs);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get(
          'GetErrors_QCCheck' + 'Type:L',
        );
        this._GetListErrors(x);
        var y = await FuncCommon.Data_Offline_Get(
          'GetErrors_QCCheck' + 'Type:N',
        );
        this._GetListReasons(y);
      }
    });
  };
  _GetListErrors = res => {
    var list = [{value: 'null', text: 'Bỏ chọn'}];
    var data = JSON.parse(res.data.data);
    for (let i = 0; i < data.length; i++) {
      list.push({
        value: data[i].ScrapReasonId,
        text: data[i].Name,
      });
    }
    this.setState({
      ListErrors: list,
    });
  };
  _GetListReasons = rs => {
    var list = [{value: 'null', text: 'Bỏ chọn'}];
    var data = JSON.parse(rs.data.data);
    for (let i = 0; i < data.length; i++) {
      list.push({
        value: data[i].ScrapReasonId,
        text: data[i].Name,
      });
    }
    this.setState({
      ListReasons: list,
    });
  };

  //#endregion
  //#region ProfileLogin
  ProfileLogin = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API.getProfile()
          .then(rs => {
            FuncCommon.Data_Offline_Set('getProfile_QCCheck', rs);
            this._GetProfile_QCCheck(rs);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error.data.data);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('getProfile_QCCheck');
        this._GetProfile_QCCheck(x);
      }
    });
  };
  _GetProfile_QCCheck = rs => {
    this.state.FullName = rs.data.FullName;
    this.setState({FullName: rs.data.FullName});
  };
  //#endregion
  //#region GetItemPlanJobCards
  GetItemPlanJobCards = id => {
    var obj = {OrderPlan: id};
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetItemPlanJobCards(obj)
          .then(res => {
            FuncCommon.Data_Offline_Set('GetItemPlanJobCards' + id, res);
            this._GetItemPlanJobCards(res);
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetItemJobCard' + id);
        this._GetItemPlanJobCards(x);
      }
    });
  };
  _GetItemPlanJobCards = res => {
    if (res.data.errorCode === 200) {
      var rs = JSON.parse(res.data.data);
      this.state.gmodel = rs;
      this.state.model = rs;
      this.state.model.QuantityQC = rs.QuantityOK;
      this.state.model.QuantityQC_Default = rs.QuantityOK;
      this.state.model.CheckQuantityOK = rs.QuantityOK;
      this.state.model.CheckQuantityNG = rs.QuantityNGMachine;
      this.state.model.CheckQuantityCancel = 1;
      this.state.model.CheckDate = new Date();
      this.state.model.CheckBy = this.state.FullName;
      this.state.jmodel.SupportorID = rs.SupportorID;
      this.state.jmodel.SupportorName = '';
      this.state.jmodel.CreatedByQC = rs.EmployeeID;
      this.state.jmodel.CreatedByQCName = '';
      this.state.jmodel.ScrapReasonId = '';
      this.state.jmodel.ScrapReasonName = '';
      this.state.jmodel.ReasonId = '';
      this.state.jmodel.ReasonName = '';
      this.state.model.RateNG = 0;
      this.setState({
        gmodel: this.state.gmodel,
        model: this.state.model,
        jmodel: this.state.jmodel,
        loading: false,
        loadingsearch: false,
      });
      this.GetEmployees();
      this.GetMachines();
      this.ProfileLogin();
    } else {
      alert('Thông báo', res.data.message);
      this.callBackList();
    }
  };
  //#endregion
  //#region GetEmployees
  GetEmployees = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetEmployees()
          .then(res => {
            FuncCommon.Data_Offline_Set('GetEmployees_QCCheck', res);
            this._GetEmployees(res);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetEmployees_QCCheck');
        this._GetEmployees(x);
      }
    });
  };
  _GetEmployees = res => {
    var list = [];
    var data = JSON.parse(res.data.data);
    for (let i = 0; i < data.length; i++) {
      list.push({
        value: data[i].EmployeeId,
        text: data[i].FullName,
        DeparmentGuid: data[i].DeparmentGuid,
        DepartmentId: data[i].DepartmentId,
      });
    }
    this.setState({
      listEMP: list,
    });
  };
  //#endregion
  //#region GetMachines
  GetMachines = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetMachines()
          .then(res => {
            FuncCommon.Data_Offline_Set('GetMachines_Operator', res);
            this._GetMachines(res);
          })
          .catch(error => {
            this.setState({loading: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetMachines_Operator');
        this._GetMachines(x);
      }
    });
  };
  _GetMachines = res => {
    var list = [];
    var data = JSON.parse(res.data.data);
    for (let i = 0; i < data.length; i++) {
      list.push({
        value: data[i].MarchineId,
        text: '[ ' + data[i].MarchineId + ' ]- ' + data[i].MarchineName,
        OrganizationGuid: data[i].OrganizationGuid,
      });
    }
    this.setState({
      listMachines: list,
      loading: false,
    });
  };
  //#endregion
  //#region  trạng thái công việc
  JTablePlanJobCardsBaoCao = s => {
    if (s === true) {
      this.state.DataTable.NumberPage = 1;
      this.setState({
        loadingsearch: true,
        ListView: [],
      });
    } else {
      this.state.DataTable.NumberPage = ++this.state.DataTable.NumberPage;
    }
    this.setState({DataTable: this.state.DataTable});
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.JTablePlanJobCardsBaoCao(this.state.DataTable)
          .then(res => {
            FuncCommon.Data_Offline_Set('JTablePlanJobCardsBaoCao', res);
            this._JTablePlanJobCardsBaoCao(res);
          })
          .catch(error => {
            this.setState({loading: false, loadingsearch: false});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('JTablePlanJobCardsBaoCao');
        this._JTablePlanJobCardsBaoCao(x);
      }
    });
  };
  _JTablePlanJobCardsBaoCao = res => {
    var data = JSON.parse(res.data.data).data;
    for (let i = 0; i < data.length; i++) {
      this.state.ListView.push(data[i]);
    }
    this.setState({
      ListView: this.state.ListView,
      loading: false,
      loadingsearch: false,
    });
  };
  //#endregion
  render() {
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar_Title
            title={'QC Check'}
            callBack={() => this.callBackList()}
            FormQR={true}
          />
          <Divider />
          <View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                padding: 10,
                backgroundColor: '#bed9f6',
              }}>
              <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={AppStyles.Labeldefault}>DANH SÁCH KIỂM TRA</Text>
              </View>
            </TouchableOpacity>
            <Divider />
            {/* Table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column', flex: 1}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 120}]}>
                    <Text style={AppStyles.Labeldefault}>Mã SP</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Tên SP</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Người làm</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Người hỗ trợ</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Máy</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>S/LKH</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Bộ phận</Text>
                  </TouchableOpacity>
                </View>
                {this.state.gmodel !== null ? (
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 120}]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                        {this.state.gmodel.ItemId}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 200}]}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.gmodel.ItemName}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 200}]}>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionComboboxEmployee()}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.jmodel.CreatedByQC === null ||
                          this.state.jmodel.CreatedByQC === ''
                            ? 'Chọn người làm'
                            : this.state.jmodel.CreatedByQCName}
                        </Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 200}]}>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionComboboxSupportor()}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.jmodel.SupportorID === null ||
                          this.state.jmodel.SupportorID === ''
                            ? 'Chọn người hỗ trợ'
                            : this.state.jmodel.SupportorName}
                        </Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 150}]}>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionComboboxMachines()}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.gmodel.MarchineId === null ||
                          this.state.gmodel.MarchineId === ''
                            ? 'Chọn máy'
                            : this.state.gmodel.MarchineName}
                        </Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_td, {width: 80}]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'right'}]}>
                        {this.addPeriod(this.state.gmodel.QuantityPlan)}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 100}]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                        {this.state.jmodel.DepartmentId}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 120}]}>
                    <Text style={AppStyles.Labeldefault}>Thời gian KT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Người kiểm tra</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Số lượng đã SX</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Slg kiểm tra</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>OK</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>NGSD</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Huỷ</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>%NG</Text>
                  </TouchableOpacity>
                </View>
                {this.state.model !== null ? (
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 120}]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                        {FuncCommon.ConDate(
                          this.state.model.CheckDate,
                          6,
                        ).split(':')[0] +
                          ' : ' +
                          FuncCommon.ConDate(
                            this.state.model.CheckDate,
                            6,
                          ).split(':')[1]}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 200}]}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.FullName}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 200}]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'right'}]}>
                        {this.addPeriod(this.state.gmodel.QuantitySX)}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 100}]}>
                      <TextInput
                        style={[AppStyles.FormInput, {textAlign: 'right'}]}
                        underlineColorAndroid="transparent"
                        value={this.state.model.QuantityQC.toString()}
                        autoCapitalize="none"
                        autoCompleteType={'cc-number'}
                        onChangeText={text => this.setQuantityQC(text)}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 100}]}>
                      <Text style={[AppStyles.Textdefault]}>
                        {this.state.model.CheckQuantityOK}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 150}]}>
                      <TextInput
                        style={[AppStyles.FormInput, {textAlign: 'right'}]}
                        underlineColorAndroid="transparent"
                        value={this.state.model.CheckQuantityNG.toString()}
                        autoCapitalize="none"
                        autoCompleteType={'cc-number'}
                        onChangeText={text => this.setCheckQuantityNG(text)}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity style={[AppStyles.table_td, {width: 80}]}>
                      <TextInput
                        style={[AppStyles.FormInput, {textAlign: 'right'}]}
                        underlineColorAndroid="transparent"
                        value={this.state.model.CheckQuantityCancel.toString()}
                        autoCapitalize="none"
                        autoCompleteType={'cc-number'}
                        onChangeText={text => this.setCheckQuantityCancel(text)}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 100}]}>
                      <Text
                        style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                        {this.state.model.RateNG === null
                          ? '0'
                          : this.state.model.RateNG}{' '}
                        %
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
                {this.state.model !== null &&
                (this.state.model.CheckQuantityNG > 0 ||
                  this.state.model.CheckQuantityCancel > 0) ? (
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 120}]}>
                      <Text style={AppStyles.Labeldefault}>Dạng lỗi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 400}]}>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionComboboxScrapReason()}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.jmodel.ScrapReasonId === null ||
                          this.state.jmodel.ScrapReasonId === ''
                            ? 'Chọn dạng lỗi'
                            : this.state.jmodel.ScrapReasonName}
                        </Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 200}]}>
                      <Text style={AppStyles.Labeldefault}>Nguyên nhân</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 330}]}>
                      <TouchableOpacity
                        style={[AppStyles.FormInput]}
                        onPress={() => this.onActionComboboxReasons()}>
                        <Text style={[AppStyles.Textdefault]}>
                          {this.state.jmodel.ReasonId === null ||
                          this.state.jmodel.ReasonId === ''
                            ? 'Chọn nguyên nhân'
                            : this.state.jmodel.ReasonName}
                        </Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  </View>
                ) : null}
                {this.state.model !== null &&
                (this.state.model.CheckQuantityNG > 0 ||
                  this.state.model.CheckQuantityCancel > 0) ? (
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                      style={[AppStyles.table_th, {width: 120}]}>
                      <Text style={AppStyles.Labeldefault}>
                        Khắc phục sử lý
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 750}]}>
                      <TextInput
                        style={[AppStyles.FormInput]}
                        underlineColorAndroid="transparent"
                        value={this.state.model.Solutions}
                        autoCapitalize="none"
                        onChangeText={text => this.setSolutions(text)}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[AppStyles.table_td, {width: 180}]}>
                      <Text style={AppStyles.Labeldefault} />
                    </TouchableOpacity>
                  </View>
                ) : null}
              </View>
            </ScrollView>
            <TouchableOpacity
              style={[
                AppStyles.FormInput,
                {
                  justifyContent: 'center',
                  margin: 10,
                  backgroundColor: '#bed9f6',
                  flexDirection: 'row',
                  alignItems: 'center',
                },
              ]}
              onPress={() => this.Submit()}>
              <Text style={[AppStyles.Textdefault, {color: 'black'}]}>LƯU</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex: 1}}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                padding: 10,
                backgroundColor: '#bed9f6',
              }}>
              <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={AppStyles.Labeldefault}>BÁO CÁO KIỂM TRA</Text>
              </View>
            </TouchableOpacity>
            <Divider />
            {/* Table */}
            <ScrollView horizontal={true}>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 1,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Hạng mục</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Tuần</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelday.Day2}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelday.Day3}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelday.Day4}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelday.Day5}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelday.Day6}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelday.Day7}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelday.Day8}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>S/L sản xuất</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelsx.Quantity}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelsx.Quantity2}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelsx.Quantity3}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelsx.Quantity4}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelsx.Quantity5}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelsx.Quantity6}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelsx.Quantity7}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelsx.Quantity8}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>S/L kiểm tra</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelkt.Quantity}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelkt.Quantity2}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelkt.Quantity3}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelkt.Quantity4}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelkt.Quantity5}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelkt.Quantity6}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelkt.Quantity7}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelkt.Quantity8}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>S/L NG</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelng.Quantity}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelng.Quantity2}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelng.Quantity3}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelng.Quantity4}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelng.Quantity5}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelng.Quantity6}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelng.Quantity7}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.state.modelng.Quantity8}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>% kiểm tra</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelkt.Quantity,
                          this.state.modelsx.Quantity,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelkt.Quantity2,
                          this.state.modelsx.Quantity2,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelkt.Quantity3,
                          this.state.modelsx.Quantity3,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelkt.Quantity4,
                          this.state.modelsx.Quantity4,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelkt.Quantity5,
                          this.state.modelsx.Quantity5,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelkt.Quantity6,
                          this.state.modelsx.Quantity6,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelkt.Quantity7,
                          this.state.modelsx.Quantity7,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelkt.Quantity8,
                          this.state.modelsx.Quantity8,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>% NG</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelng.Quantity,
                          this.state.modelsx.Quantity,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelng.Quantity2,
                          this.state.modelsx.Quantity2,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelng.Quantity3,
                          this.state.modelsx.Quantity3,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelng.Quantity4,
                          this.state.modelsx.Quantity4,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelng.Quantity5,
                          this.state.modelsx.Quantity5,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelng.Quantity6,
                          this.state.modelsx.Quantity6,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelng.Quantity7,
                          this.state.modelsx.Quantity7,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>
                      {this.addPeriod(
                        this.CheckValue(
                          this.state.modelng.Quantity8,
                          this.state.modelsx.Quantity8,
                        ),
                      )}
                      %
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Mục tiêu</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>3%</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>3%</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>3%</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>3%</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>3%</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>3%</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>3%</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      AppStyles.table_td,
                      {width: 80, alignItems: 'center'},
                    ]}>
                    <Text style={AppStyles.Labeldefault}>3%</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>

          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.CallbackValueBottom(callback)
              }
            />
          </View>

          {this.state.listEMP.length > 0 ? (
            <Combobox
              value={
                this.state.jmodel.CreatedByQC === undefined
                  ? ''
                  : this.state.jmodel.CreatedByQC
              }
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeEmployee}
              data={this.state.listEMP}
              nameMenu={'Chọn người làm'}
              eOpen={this.openComboboxEmployee}
              position={'bottom'}
            />
          ) : null}
          {this.state.listEMP.length > 0 ? (
            <Combobox
              value={
                this.state.jmodel.SupportorID === undefined
                  ? ''
                  : this.state.jmodel.SupportorID
              }
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeSupportor}
              data={this.state.listEMP}
              nameMenu={'Chọn người hỗ trợ'}
              eOpen={this.openComboboxSupportor}
              position={'bottom'}
            />
          ) : null}
          {this.state.listMachines.length > 0 ? (
            <Combobox
              value={this.state.gmodel.MarchineId}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeMachines}
              data={this.state.listMachines}
              nameMenu={'Chọn máy'}
              eOpen={this.openComboboxMachines}
              position={'bottom'}
            />
          ) : null}
          {this.state.ListErrors.length > 0 ? (
            <Combobox
              value={this.state.jmodel.ScrapReasonId}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeScrapReason}
              data={this.state.ListErrors}
              nameMenu={'Chọn dạng lỗi'}
              eOpen={this.openComboboxScrapReason}
              position={'bottom'}
            />
          ) : null}
          {this.state.ListReasons.length > 0 ? (
            <Combobox
              value={this.state.jmodel.ReasonId}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeReasons}
              data={this.state.ListReasons}
              nameMenu={'Chọn nguyên nhân'}
              eOpen={this.openComboboxReasons}
              position={'bottom'}
            />
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  setPauseNote = txt => {
    this.state.StopProduction.PauseNote = txt;
    this.setState({StopProduction: this.state.StopProduction});
  };
  SubmitStopProduction = () => {
    console.log('fjwhiohrtưiehtửeiothủeơpthủeo');
    this.onActionopenCustomView();
  };
  CallbackValueBottom = key => {
    switch (key) {
      case '0':
        Actions.Operator_ListPlanJobCards();
        break;
      case '2':
        Actions.Operator_StopProduction();
        // this.onActionopenCustomView();
        break;

      default:
        break;
    }
  };
  //#region CustomView
  _openCustomView() {}
  openCustomView = d => {
    this._openCustomView = d;
  };
  onActionopenCustomView() {
    this._openCustomView();
  }
  //#endregion
  //#region setSolutions
  setSolutions = txt => {
    this.state.model.Solutions = txt;
    this.setState({model: this.state.model});
  };
  //#endregion
  //#region setQuantityQC
  setQuantityQC = txt => {
    this.state.model.QuantityQC = txt;
    this.setState({
      model: this.state.model,
    });
  };
  //#endregion
  //#region setCheckQuantityNG
  setCheckQuantityNG = txt => {
    this.state.model.CheckQuantityNG = txt;
    this.setState({
      model: this.state.model,
    });
  };
  //#endregion
  //#region setCheckQuantityCancel
  setCheckQuantityCancel = txt => {
    this.state.model.CheckQuantityCancel = txt;
    this.setState({
      model: this.state.model,
    });
  };
  //#endregion

  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Emp', ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region CheckValue
  CheckValue = (quantityNG, quantitySX) => {
    if (quantitySX === 0) {
      return 0;
    } else {
      return (quantityNG / quantitySX) * 100;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null && val !== '') {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        var _string2 = string[1].substring(0, 2);
        if (_string2 !== '00') {
          return valview + ',' + string[1].substring(0, 2) === '00';
        } else {
          return valview;
        }
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  // $scope.$watch('model', function (newvalue, oldvalue) {
  //     if (newvalue !== oldvalue) {
  //         $scope.model.CheckQuantityOK = parseFloat(($scope.model.QuantityQC === null ? 0 : parseFloat($scope.model.QuantityQC)) - ($scope.model.CheckQuantityNG === null ? 0 : parseFloat($scope.model.CheckQuantityNG)) - ($scope.model.CheckQuantityCancel === null ? 0 : parseFloat($scope.model.CheckQuantityCancel)), 2);
  //         if (($scope.model.CheckQuantityNG === null ? 0 : parseFloat($scope.model.CheckQuantityNG)) + ($scope.model.CheckQuantityCancel === null ? 0 : parseFloat($scope.model.CheckQuantityCancel)) > 0) {
  //             $scope.model.RateNG = (($scope.model.CheckQuantityNG === null ? 0 : parseFloat($scope.model.CheckQuantityNG)) + ($scope.model.CheckQuantityCancel === null ? 0 : parseFloat($scope.model.CheckQuantityCancel))) / ($scope.model.QuantityQC === null ? 0 : parseFloat($scope.model.QuantityQC)) * 100, 2;
  //         } else {
  //             $scope.model.RateNG = 0;
  //         }
  //     }
  // }, true);
  //#endregion
  //#region  combobox Employee
  _openComboboxEmployee() {}
  openComboboxEmployee = d => {
    this._openComboboxEmployee = d;
  };
  onActionComboboxEmployee() {
    this._openComboboxEmployee();
  }
  ChangeEmployee = rs => {
    if (rs !== null) {
      if (rs.value !== 'null') {
        this.state.jmodel.CreatedByQC = rs.value;
        this.state.jmodel.CreatedByQCName = rs.text;
        this.state.jmodel.DepartmentId = rs.DepartmentId;
      } else {
        this.state.jmodel.CreatedByQC = null;
        this.state.jmodel.CreatedByQCName = '';
        this.state.jmodel.DepartmentId = null;
      }
      this.setState({jmodel: this.state.jmodel});
    }
  };
  //#endregion
  //#region  combobox Supportor
  _openComboboxSupportor() {}
  openComboboxSupportor = d => {
    this._openComboboxSupportor = d;
  };
  onActionComboboxSupportor() {
    this._openComboboxSupportor();
  }
  ChangeSupportor = rs => {
    if (rs !== null) {
      if (rs.value !== 'null') {
        this.state.jmodel.SupportorID = rs.value;
        this.state.jmodel.SupportorName = rs.text;
      } else {
        this.state.jmodel.SupportorID = null;
        this.state.jmodel.SupportorName = '';
      }
      this.setState({jmodel: this.state.jmodel});
    }
  };
  //#endregion

  //#region combobox ScrapReason
  _openComboboxScrapReason() {}
  openComboboxScrapReason = d => {
    this._openComboboxScrapReason = d;
  };
  onActionComboboxScrapReason() {
    this._openComboboxScrapReason();
  }
  ChangeScrapReason = rs => {
    if (rs !== null) {
      this.state.jmodel.ScrapReasonId = rs.value;
      this.state.jmodel.ScrapReasonName = rs.text;
      this.setState({jmodel: this.state.jmodel});
    }
  };
  //#endregion
  //#region combobox Reasons
  _openComboboxReasons() {}
  openComboboxReasons = d => {
    this._openComboboxReasons = d;
  };
  onActionComboboxReasons() {
    this._openComboboxReasons();
  }
  ChangeReasons = rs => {
    if (rs !== null) {
      this.state.jmodel.ReasonId = rs.value;
      this.state.jmodel.ReasonName = rs.text;
      this.setState({jmodel: this.state.jmodel});
    }
  };
  //#endregion
  //#region  combobox Machines
  _openComboboxMachines() {}
  openComboboxMachines = d => {
    this._openComboboxMachines = d;
  };
  onActionComboboxMachines() {
    this._openComboboxMachines();
  }
  ChangeMachines = rs => {
    if (rs !== null) {
      this.state.gmodel.MarchineId = rs.value;
      this.state.gmodel.MarchineName = rs.text;
      this.setState({gmodel: this.state.gmodel});
    }
  };
  //#endregion
  //#region Date
  HoursNow = () => {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (mm < 10) {
      mm = '0' + mm;
    }
    if (dd < 10) {
      dd = '0' + dd;
    }
    return h + ':' + m + ':' + s;
  };
  convertDate24h = datetime => {
    if (datetime === '' || datetime === null || datetime === undefined) {
      return '';
    }
    var dateSplit = datetime.split(' ')[2];
    if (dateSplit === 'CH' || dateSplit === 'PM') {
      var hourse = parseInt(datetime.split(' ')[1].split(':')[0]);
      hourse = hourse + 12;
      if (hourse < 24) {
        return (
          hourse +
          ':' +
          datetime.split(' ')[1].split(':')[1] +
          ':' +
          datetime.split(' ')[1].split(':')[2]
        );
      } else {
        return (
          '12:' +
          datetime.split(' ')[1].split(':')[1] +
          ':' +
          datetime.split(' ')[1].split(':')[2]
        );
      }
    } else {
      return datetime.split(' ')[1];
    }
  };
  converQTT = full => {
    var QuantityOk = full.QuantityOk === '' ? 0 : parseFloat(full.QuantityOk);
    var Quantity = full.Quantity === '' ? 0 : parseFloat(full.Quantity);
    return this.addPeriod(parseInt((QuantityOk / Quantity) * 100)) + ' %';
  };
  CalenderReturnDate = date => {
    //xác định tuần hiện tại
    var d = date;
    var day = d.getDay(),
      fday = d.getDate() - day + 1,
      lday = fday + 6;
    var data = {
      sdate: FuncCommon.ConDate(
        new Date(d.getFullYear(), d.getMonth(), fday),
        0,
      ),
      edate: FuncCommon.ConDate(
        new Date(d.getFullYear(), d.getMonth(), lday),
        0,
      ),
    };
    return data;
  };
  addDays = (d, days) => {
    var _d = d.split('/');
    var _string = _d[2] + '-' + _d[1] + '-' + _d[0];
    var date = new Date(_string);
    date.setDate(date.getDate() + days);
    return FuncCommon.ConDate(date, 0);
  };
  //#endregion
  //#region setmodelday
  setmodelday = val => {
    if (val !== null) {
      var _val = val.toString();
      var _string = _val.split('/');
      return _string[0] + '/' + _string[1];
    }
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Operator_QCCheck_Component);
