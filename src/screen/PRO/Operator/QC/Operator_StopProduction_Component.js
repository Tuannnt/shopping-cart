import React, { Component } from 'react';
import { FlatList, Keyboard, TextInput, Text, TouchableWithoutFeedback, Alert, Image, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import CustomView from '../../../component/CustomView';
import { FuncCommon } from '../../../../utils';
import { ScrollView } from 'react-native-gesture-handler';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_StopProduction_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            Data_GetDataView: {
                length: 20,
                NumberPage: 0,
                Code: "",
                Keyword: "",
            },
            ListView: [],
            CheckAll: false,
            StopProduction: {
                PauseNote: ""
            },
            ListSubmit: []
        };
    }
    componentDidMount() {
        this.setState({ loading: true })
        this.GetDataView(true);
    }
    GetDataView = (s) => {
        if (s === true) {
            this.state.Data_GetDataView.NumberPage = 1;
            this.setState(
                {
                    loadingsearch: true,
                    ListView: [],
                }
            );
        } else {
            this.state.Data_GetDataView.NumberPage = ++this.state.Data_GetDataView.NumberPage
        }
        this.setState({ Data_GetDataView: this.state.Data_GetDataView });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetDataView(this.state.Data_GetDataView).then(res => {
                    FuncCommon.Data_Offline_Set('Print_GetDataView', res);
                    this._GetAll_Print_GetDataView(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('Print_GetDataView');
                this._GetAll_Print_GetDataView(x);
            }
        });
    }
    _GetAll_Print_GetDataView = (res) => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            data[i].CheckBox = false;
            this.state.ListView.push(data[i]);
        }
        this.setState({
            ListView: this.state.ListView,
            loading: false,
            loadingsearch: false
        });
    }
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"In phiếu công việc"}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        <View style={{ flex: 1 }}>
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]} onPress={() => this.setCheckAll()}>
                                            {this.state.CheckAll ?
                                                <Icon
                                                    name={"check"}
                                                    type="entypo"
                                                    color={AppColors.ColorEdit}
                                                    size={20} />
                                                :
                                                <Icon
                                                    name={"square-o"}
                                                    type="font-awesome"
                                                    color={AppColors.IconcolorTabbar}
                                                    size={20} />
                                            }
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>Ảnh</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Số SO</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Mã TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Tên TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>C.đoạn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>ĐVT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>S/LKH</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>KQSX</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Tỉ lệ %</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Mã CV</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Người làm</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListView.length > 0 ?
                                        <FlatList
                                            data={this.state.ListView}
                                            renderItem={({ item, index }) => (
                                                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.ViewItemPrint(item)}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => this.CheckBoxItem(index)}>
                                                        {item.CheckBox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                        }
                                                    </TouchableOpacity>
                                                    <View style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 70, alignItems: "center" }]}><Image style={{ width: 60, height: 60 }} source={{ uri: item.HINHANH }} /></View>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.OrderNumber}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.MaTP}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.LocationName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.UnitName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.Quantity}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.KQSX}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.Percent}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.MCV}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text></View>
                                                </TouchableOpacity>
                                            )}
                                            onEndReached={() => this.state.ListView.length >= (20 * this.state.Data_GetDataView.NumberPage) ? this.GetDataView(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                            <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.onActionopenCustomView()}>
                                <Text style={[AppStyles.Textdefault, { color: 'black' }]}>BÁO DỪNG SẢN XUẤT</Text>
                            </TouchableOpacity>
                        </View>
                        <CustomView eOpen={this.openCustomView}>
                            <View style={{ height: DRIVER.height / 2.2, width: DRIVER.width - 50, borderRadius: 10 }}>
                                <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={AppStyles.Labeldefault}>BÁO DỪNG SẢN XUẤT</Text>
                                    </View>
                                </TouchableOpacity>
                                <Divider />
                                <ScrollView>
                                    <View style={{ flexDirection: 'column', padding: 10 }}>
                                        <Text style={AppStyles.Labeldefault}>Lý do dừng sản xuất</Text>
                                        <TextInput style={[AppStyles.FormInput]}
                                            underlineColorAndroid="transparent"
                                            value={this.state.StopProduction.PauseNote}
                                            autoCapitalize="none"
                                            numberOfLines={10}
                                            multiline={true}
                                            onChangeText={text =>
                                                this.setPauseNote(text)
                                            }
                                        />
                                    </View>
                                </ScrollView>
                                <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.SubmitStopProduction()}>
                                    <Text style={[AppStyles.Textdefault, { color: 'black' }]}>LƯU</Text>
                                </TouchableOpacity>
                            </View>
                        </CustomView>

                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    //#region CustomView
    _openCustomView() { }
    openCustomView = (d) => {
        this._openCustomView = d;
    }
    onActionopenCustomView() {
        var list = this.state.ListView;
        var listsub = [];
        for (let i = 0; i < list.length; i++) {
            if (list[i].CheckBox === true) {
                listsub.push(list[i].PlanGuid);
            };
        }
        this.setState({ ListSubmit: listsub });
        if (listsub.length > 0) {
            this._openCustomView();
        } else {
            Alert.alert("Thông báo", "Chưa có sản phẩm nào được chọn.");
        }
    }
    //#endregion
    setPauseNote = (txt) => {
        this.state.StopProduction.PauseNote = txt;
        this.setState({ StopProduction: this.state.StopProduction });
    };
    SubmitStopProduction = () => {
        var obj = {
            PlanGuid: this.state.ListSubmit,
            PauseNote: this.state.StopProduction.PauseNote
        }
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.UpdatePlans(obj).then(res => {
                    Alert.alert("Thông báo", res.data.message);
                })
                    .catch(error => {
                        this.setState({ loading: false });
                        console.log(error);
                    });
            } else {
                Alert.alert("Thông báo", "Yêu cầu kết nối Internet");
            }
        });
        this.onActionopenCustomView();
    }
    //#region CheckAll
    setCheckAll = () => {
        var list = this.state.ListView;
        for (let i = 0; i < list.length; i++) {
            list[i].CheckBox = !this.state.CheckAll
        };
        this.setState({ ListView: list, CheckAll: !this.state.CheckAll });
    }
    CheckBoxItem = (index) => {
        this.state.ListView[index].CheckBox = !this.state.ListView[index].CheckBox;
        var obj = this.state.ListView.find(x => x.CheckBox === false);
        if (obj === undefined) {
            this.state.CheckAll = true;
        } else {
            this.state.CheckAll = false;
        }
        this.setState({ ListView: this.state.ListView, CheckAll: this.state.CheckAll });
    }
    //#endregion
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_StopProduction_Component);
