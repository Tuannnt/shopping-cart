import React, { Component } from 'react';
import { FlatList, Keyboard, Animated, Text, TouchableWithoutFeedback, RefreshControl, TextInput, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../../utils';
import { ScrollView } from 'react-native-gesture-handler';
import { Alert } from 'react-native';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_Warehouses_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            ListView: [],
            ListTable: [],
            CheckAll: false,
        };
        //#region tabbarbottom
        this.listtabbarBotom = [
            // {
            //     Title: "In phiếu NK",
            //     Icon: "printer",
            //     Type: "feather",
            //     Value: "0",
            //     Checkbox: false,
            //     OffEditcolor: true,
            //     Badge: 0
            // },
            // {
            //     Title: "In tem SP",
            //     Icon: "printer",
            //     Type: "feather",
            //     Value: "1",
            //     Checkbox: false,
            //     OffEditcolor: true,
            //     Badge: 0
            // },
            {
                Title: "Yêu cầu KT OQC",
                Icon: "tag",
                Type: "feather",
                Value: "2",
                Checkbox: false,
                OffEditcolor: true,
                Badge: 0
            }
        ];
        //#endregion
    }
    componentDidMount() {
        this.QRCheck([{ data: "KH230221-CAT-GOT0-002" }, { data: "KH230221-CAT-PHA-010-002" }]);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.moduleId == "QRCode") {
            this.QRCheck(nextProps.Data.Value);
        } else {
            //this.nextPage();
        }
    }
    QRCheck = (value) => {
        for (let i = 0; i < value.length; i++) {
            FuncCommon.Data_Offline(async (d) => {
                if (d) {
                    var obj = { QRCode: value[i].data }
                    API_Operator.GetQRCode(obj).then(res => {
                        FuncCommon.Data_Offline_Set('GetQRCode_Operator_Warehouses' + value[i].data, res);
                        this._GetQRCode(res);
                    })
                        .catch(error => {
                            this.setState({ loading: false });
                            console.log(error);
                        });
                } else {
                    var x = await FuncCommon.Data_Offline_Get('GetQRCode_Operator_Warehouses' + value[i].data);
                    this._GetQRCode(x);
                }
            });
        }

    }
    _GetQRCode = (res) => {
        var rs = JSON.parse(res.data.data);
        for (let i = 0; i < rs.length; i++) {
            this.state.ListView.unshift({
                CheckBox: false,
                ItemGuid: rs[i].ItemGuid,
                ItemId: rs[i].ItemId,
                ItemName: rs[i].ItemName,
                Quantity: rs[i].Quantity,
                Qty: rs[i].Quantity,
                UnitGuid: rs[i].UnitGuid,
                UnitId: rs[i].UnitId,
                UnitName: rs[i].UnitName,
                OrderNumbers: rs[i].OrderNumber,
                VenderCode: rs[i].VenderCode,
                SaleOrderNo: rs[i].SaleOrderNo,
                CreatedDate: new Date(),
                isSelected: 'es-iSeleted',
                PlanGuid: rs[i].PlanGuid,
                OrderDetailGuid: rs[i].OrderDetailGuid
            });
        }
        this.setState({
            ListView: this.state.ListView,
            loading: false
        });
    }
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"Nhập kho"}
                            callBack={() => this.callBackList()}
                            FormQR={true}
                            QRMultiple={true} />
                        <Divider />
                        <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={AppStyles.Labeldefault}>DANH SÁCH THÀNH PHẨM</Text>
                            </View>
                        </TouchableOpacity>
                        <Divider />
                        <View style={{ flex: 1 }}>
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Mã ID</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Số lượng</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>Qty</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>ĐVT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Số SOKH</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Code nhà máy</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}></Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListView.length > 0 ?
                                        <ScrollView style={{ flexDirection: 'column' }}>
                                            {this.state.ListView.map((item, index) => (
                                                <View style={{ flexDirection: 'row' }} onPress={() => this.ViewItemPrint(item)}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemId}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}>
                                                        <TextInput style={AppStyles.FormInput}
                                                            underlineColorAndroid="transparent"
                                                            value={item.Quantity.toString()}
                                                            placeholder={'Số phút'}
                                                            // autoFocus={true}
                                                            autoCapitalize="none"
                                                            onChangeText={text =>
                                                                this.setQuantity(text, index)
                                                            }
                                                        />
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.Qty}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.UnitName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.OrderNumbers}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.VenderCode}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]} onPress={() => this.DeleteItem(index)}><Icon name={"delete"} type={""} /></TouchableOpacity>
                                                </View>
                                            ))}
                                        </ScrollView>
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                            {this.state.ListView.length > 0 ?
                                <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.Submit()}>
                                    {/* <Icon type={"antdesign"} name={"save"} /> */}
                                    <Text style={[AppStyles.Textdefault, { color: 'black' }]}>LƯU</Text>
                                </TouchableOpacity>
                                : null}
                        </View>
                        <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={AppStyles.Labeldefault}>DANH SÁCH CHỜ NHẬP KHO</Text>
                            </View>
                        </TouchableOpacity>
                        <Divider />
                        <View style={{ flex: 1 }}>
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]} onPress={() => this.setCheckAll()}>
                                            {this.state.CheckAll ?
                                                <Icon
                                                    name={"check"}
                                                    type="entypo"
                                                    color={AppColors.ColorEdit}
                                                    size={20} />
                                                :
                                                <Icon
                                                    name={"square-o"}
                                                    type="font-awesome"
                                                    color={AppColors.IconcolorTabbar}
                                                    size={20} />
                                            }
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Mã ID</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 200 }]}><Text style={AppStyles.Labeldefault}>Tên TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Số lượng</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>ĐVT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Số SOKH</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Code nhà máy</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListTable.length > 0 ?
                                        <ScrollView style={{ flexDirection: 'column' }}>
                                            {this.state.ListTable.map((item, index) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => this.CheckBoxItem(index)}>
                                                        {item.CheckBox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                        }
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemId}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 200 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}>
                                                        <TextInput style={AppStyles.FormInput}
                                                            underlineColorAndroid="transparent"
                                                            value={item.Quantity.toString()}
                                                            placeholder={'Số phút'}
                                                            // autoFocus={true}
                                                            autoCapitalize="none"
                                                            onChangeText={text =>
                                                                this.setQuantity(text, index)
                                                            }
                                                        />
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 70 }]}><Text style={[AppStyles.Textdefault]}>{item.UnitName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.OrderNumbers}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.VenderCode}</Text></TouchableOpacity>
                                                </View>
                                            ))}
                                        </ScrollView>
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={AppStyles.StyleTabvarBottom}>
                            <TabBarBottomCustom
                                ListData={this.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    //#region CheckAll
    setCheckAll = () => {
        var list = this.state.ListTable;
        for (let i = 0; i < list.length; i++) {
            list[i].CheckBox = !this.state.CheckAll
        };
        this.setState({ ListTable: list, CheckAll: !this.state.CheckAll });
    }
    CheckBoxItem = (index) => {
        this.state.ListTable[index].CheckBox = !this.state.ListTable[index].CheckBox;
        var obj = this.state.ListTable.find(x => x.CheckBox === false);
        if (obj === undefined) {
            this.state.CheckAll = true;
        } else {
            this.state.CheckAll = false;
        }
        this.setState({ ListTable: this.state.ListTable, CheckAll: this.state.CheckAll });
    }
    //#endregion
    //#region 
    CallbackValueBottom = (key) => {
        var list = this.state.ListTable;
        var listSub = [];
        for (let i = 0; i < list.length; i++) {
            if (list[i].CheckBox === true) {
                listSub.push(list[i]);
            }
        }
        switch (key) {
            case "2":
                if (listSub.length > 0) {
                    Actions.Operator_addTicketRequests({ list: listSub });
                } else {
                    Alert.alert("Thông báo", "Yêu cầu chọn Thành phẩm/BTP")
                }
                break;
            default:
                break;
        }
    }
    //#endregion



    DeleteItem = (index) => {
        var list = this.state.ListView;
        var listView = [];
        for (let i = 0; i < list.length; i++) {
            if (i !== index) {
                listView.push(list[i]);
            }
        }
        this.setState({ ListView: listView });
    }
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region setQuantity
    setQuantity = (val, index) => {
        this.state.ListView[index].Quantity = val;
        this.setState({
            ListView: this.state.ListView
        })
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
    //#region Submit
    Submit = () => {
        var list = this.state.ListView;
        for (let i = 0; i < list.length; i++) {
            list[i].Check = false;
            list[i].SortOrder = this.state.ListTable.length + 1;
            list[i].RowGuid = null;
            this.state.ListTable.push(list[i]);
        }
        this.setState({
            ListTable: this.state.ListTable,
            ListView: []
        })
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_Warehouses_Component);
