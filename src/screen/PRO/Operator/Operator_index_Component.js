import React, { Component } from 'react';
import {
    Alert,
    FlatList,
    Image,
    RefreshControl,
    Text,
    View,
    Keyboard,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback, Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import { Divider, Icon, ListItem, SearchBar } from 'react-native-elements';
import { API_BomCategories } from '../../../network';
import TabBar from '../../component/TabBar';
import { FuncCommon } from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import { AppStyles, AppColors } from '@theme';
import FormSearch from '../../component/FormSearch';
import MenuSearchDate from '../../component/MenuSearchDate';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import DatePicker from 'react-native-date-picker';
import { ScrollView } from 'react-native';
const SCREEN_WIDTH = Dimensions.get('window').width;

class Operator_index_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };

    }
    componentDidMount() {
    }

    //#region View tổng
    render() {
        return (
            <TouchableWithoutFeedback
                style={{ flex: 1 }}
                onPress={() => {
                    Keyboard.dismiss();
                }}>
                <View style={[AppStyles.container]}>
                    <TabBar
                        title={'Triển khai sản xuất'}
                        BackModuleByCode={'PRO'}
                    />
                    <ScrollView style={[{ flex: 1, padding: 10}]}>
                        <View style={[{ flex: 1, flexDirection: "row" }]}>
                            <View style={[{ flex: 1, flexDirection: "column" ,marginRight:5}]}>
                                <TouchableOpacity style={[AppStyles.containerCentered, styles.tabs, { backgroundColor: "#33cccc" }]} onPress={()=>this.nextTab("Operator_Employee")}>
                                    <Icon name={"users"} type={"feather"} color={"#fff"}  iconStyle={styles.StyIcon}/>
                                    <Text style={[AppStyles.Labeldefault,styles.StyTLable]}>NHÂN LỰC</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, styles.tabs, { backgroundColor: "#92d050" }]}onPress={()=>this.nextTab("Operator_indexDocument")}>
                                    <Icon name={"book-open"} type={"feather"} color={"#fff"}  iconStyle={styles.StyIcon}/>
                                    <Text style={[AppStyles.Labeldefault,styles.StyTLable]}>TÀI LIỆU</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, styles.tabs, { backgroundColor: "#ab9ac0" }]} onPress={()=>this.nextTab("Operator_Print")}>
                                    <Icon name={"printer"} type={"feather"} color={"#fff"}  iconStyle={styles.StyIcon}/>
                                    <Text style={[AppStyles.Labeldefault,styles.StyTLable]}>IN PHIẾU CV</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, styles.tabs, { backgroundColor: "#ff0000" }]} onPress={()=>this.nextTab("Operator_QCCheck")}>
                                    <Icon name={"zoom-in"} type={"feather"} color={"#fff"} iconStyle={styles.StyIcon}/>
                                    <Text style={[AppStyles.Labeldefault,styles.StyTLable]}>QC CHECK</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[{ flex: 1, flexDirection: "column"  ,marginLeft:5}]}>
                                <TouchableOpacity style={[AppStyles.containerCentered, styles.tabs, { backgroundColor: "#6699ff" }]} onPress={()=>this.nextTab("Operator_MachinesTools")}>
                                    <Icon name={"settings"} type={"feather"} color={"#fff"} iconStyle={styles.StyIcon}/>
                                    <Text style={[AppStyles.Labeldefault,styles.StyTLable]}>MÁY MÓC</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, styles.tabs, { backgroundColor: "#7f7f7f" }]} onPress={()=>this.nextTab("Operator_indexSupplies")}>
                                    <Icon name={"briefcase"} type={"feather"} color={"#fff"} iconStyle={styles.StyIcon}/>
                                    <Text style={[AppStyles.Labeldefault,styles.StyTLable]}>VẬT TƯ</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, styles.tabs, { backgroundColor: "#ffc000" }]} onPress={()=>this.nextTab("Operator_ItemJobCard")}>
                                    <Icon name={"check-square"} type={"feather"} color={"#fff"} iconStyle={styles.StyIcon}/>
                                    <Text style={[AppStyles.Labeldefault,styles.StyTLable]}>BÁO CÁO CV</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[AppStyles.containerCentered, styles.tabs, { backgroundColor: "#00b050" }]} onPress={()=>this.nextTab("Operator_Warehouses")}>
                                    <Icon name={"download"} type={"feather"} color={"#fff"} iconStyle={styles.StyIcon}/>
                                    <Text style={[AppStyles.Labeldefault,styles.StyTLable]}>NHẬP KHO</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </TouchableWithoutFeedback>
        );
    }
    //#endregion

    nextTab=(val)=>{
        Actions.push(val);
    }


    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
}
const styles = StyleSheet.create({
    tabs: {
        height: SCREEN_WIDTH /2.5,
        marginBottom:10
    },
    StyIcon:{
        fontSize:50
    },
    StyTLable:{
        color: '#fff',
        fontSize:20
    }
});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_index_Component);
