import React, { Component } from 'react';
import { FlatList, Keyboard, Animated, Text, TouchableWithoutFeedback, RefreshControl, Image, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../network";
import TabBar_Title from "../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';

const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_Document_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            //danh sách nhân sự 
            Data_JTableEmp: {
                Status: [
                    "OM"
                ],
                length: 20,
                search: {
                    value: ""
                },
                NumberPage: 0,
                StartDate: null,
                EndDate: null,
                lstDepartmentID: []
            },
            ListEmp: [],
        };
        //#region tabbarbottom
        this.listtabbarBotom = [
            {
                Title: "Đăng ký LT",
                Icon: "server",
                Type: "feather",
                Value: "0",
                Checkbox: true,
                Badge: 0
            },
            {
                Title: "Thêm LĐTV",
                Icon: "server",
                Type: "feather",
                Value: "1",
                Checkbox: false,
                Badge: 0
            },
            {
                Title: "Cập nhật gửi BC",
                Icon: "server",
                Type: "feather",
                Value: "4",
                Checkbox: false,
                Badge: 0
            }
        ];
        //#endregion
    }
    componentDidMount() {
        this.JTableEmp(true);
    }
    JTableEmp = (s) => {
        if (s === true) {
            this.state.Data_JTableEmp.NumberPage = 1;
            this.setState(
                {
                    loadingsearch: true,
                    ListEmp: [],
                }
            );
        } else {
            this.state.Data_JTableEmp.NumberPage = ++this.state.Data_JTableEmp.NumberPage
        }
        this.setState({ Data_JTableEmp: this.state.Data_JTableEmp });
        API_Operator.JTableEmp(this.state.Data_JTableEmp).then(res => {
            var data = JSON.parse(res.data.data).data;
            for (let i = 0; i < data.length; i++) {
                this.state.ListEmp.push(data[i]);
            }
            this.setState({
                ListEmp: this.state.ListEmp,
                loading: false,
                loadingsearch: false
            });
        })
            .catch(error => {
                this.setState({ loading: false, loadingsearch: false });
                console.log(error);
            });
    }
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"Tài liệu"}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        <View style={{ flex: 1 }}>
                        <Image
                            source={require('../../../images/filetest.png')}
                            style={[{width:"100%",height:250}]}

                        />
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }

    //Các hàm dùng chung
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_Document_Component);
