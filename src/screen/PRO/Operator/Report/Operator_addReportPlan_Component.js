import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_Operator, API_HR} from '../../../../network';
import TabBar_Title from '../../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import {FuncCommon} from '../../../../utils';
import Combobox from '../../../component/Combobox';
import DatePicker from 'react-native-date-picker';
import {ScrollView} from 'react-native-gesture-handler';
import {stubFalse} from 'lodash';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class Operator_addReportPlan_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      ListView: [],
      offsetDate: false,
      Index: null,
      ListLocaton: [],
      ListScrapReasons: [],
    };
  }
  componentDidMount() {
    this.setState({loading: true});
    this.GetLocaton();
  }
  //#region GetLocaton
  GetLocaton = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetLocaton()
          .then(res => {
            FuncCommon.Data_Offline_Set('GetLocaton', res);
            this._GetLocaton(res);
          })
          .catch(error => {
            this.setState({loading: stubFalse});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetLocaton');
        this._GetLocaton(x);
      }
    });
  };
  _GetLocaton = res => {
    var data = JSON.parse(res.data.data);
    for (let i = 0; i < data.length; i++) {
      this.state.ListLocaton.push({
        value: data[i].LocationId,
        text: data[i].Name,
        LocationCode: data[i].LocationCode,
        Code: data[i].Code,
        OrganizationGuid: data[i].OrganizationGuid,
      });
    }
    this.setState({
      ListLocaton: this.state.ListLocaton,
    });
    this.GetScrapReasonsBaoCao();
  };
  //#endregion
  //#region GetScrapReasonsBaoCao
  GetScrapReasonsBaoCao = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.GetScrapReasonsBaoCao()
          .then(res => {
            FuncCommon.Data_Offline_Set('GetScrapReasonsBaoCao', res);
            this._GetScrapReasonsBaoCao(res);
          })
          .catch(error => {
            this.setState({loading: stubFalse});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('GetScrapReasonsBaoCao');
        this._GetScrapReasonsBaoCao(x);
      }
    });
  };
  _GetScrapReasonsBaoCao = res => {
    var data = JSON.parse(res.data.data);
    for (let i = 0; i < data.length; i++) {
      this.state.ListScrapReasons.push({
        value: data[i].ScrapReasonId,
        text: data[i].Title,
      });
    }
    this.setState({
      ListScrapReasons: this.state.ListScrapReasons,
    });
    this.GetDataView();
  };
  //#endregion
  //#region GetDataView
  GetDataView = () => {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.PlanJobCardsReport()
          .then(res => {
            FuncCommon.Data_Offline_Set('PlanJobCardsReport', res);
            this._PlanJobCardsReport(res);
          })
          .catch(error => {
            this.setState({loading: stubFalse});
            console.log(error);
          });
      } else {
        var x = await FuncCommon.Data_Offline_Get('PlanJobCardsReport');
        this._PlanJobCardsReport(x);
      }
    });
  };
  _PlanJobCardsReport = res => {
    var data = JSON.parse(res.data.data);
    for (let i = 0; i < data.length; i++) {
      data[i].ReportLocationCode = data[i].LocationID;
      var obj = this.state.ListLocaton.find(
        x => x.value === data[i].LocationID,
      );
      if (obj !== undefined) {
        data[i].ReportLocationName = obj.text;
      } else {
        data[i].ReportLocationName = '';
      }
      var obj2 = this.state.ListScrapReasons.find(
        x => x.value === data[i].ScrapReasonId,
      );
      if (obj2 !== undefined) {
        data[i].ReportReasonNG = obj2.text;
      } else {
        data[i].ReportReasonNG = '';
      }
      this.state.ListView.push(data[i]);
    }
    this.setState({
      ListView: this.state.ListView,
      loading: false,
    });
  };
  //#endregion

  render() {
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar_Title
            title={'BÁO CÁO - VẤN ĐỀ SẢN XUẤT'}
            callBack={() => this.callBackList()}
          />
          <Divider />
          <View style={{flex: 1}}>
            {/* Table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column', flex: 1}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Mã Code</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Số SO</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Tên đơn hàng</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Khách hàng</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 120}]}>
                    <Text style={AppStyles.Labeldefault}>Số lượng theo SO</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>PIC</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Slg KH SX</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Slg thực Tế</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Công đoạn</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Ngày Dự Kiến HT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Lý Do Không Đạt Được KH
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Ghi chú</Text>
                  </TouchableOpacity>
                </View>
                {this.state.ListView.length > 0 ? (
                  <FlatList
                    data={this.state.ListView}
                    renderItem={({item, index}) => (
                      <View
                        style={{flexDirection: 'row'}}
                        onPress={() => this.ViewItemPrint(item)}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'center'},
                            ]}>
                            {index + 1}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.ItemID}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.OrderNumber}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.ItemName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.SortTitle}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 120}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(item.QuantityOrder)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {item.EmployeeName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(item.QuantityPL)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(item.QuantityOK)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <TouchableOpacity
                            style={AppStyles.FormInput}
                            onPress={() =>
                              this.onActionComboboxLocation(index)
                            }>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                item.ReportLocationCode === ''
                                  ? {color: AppColors.gray}
                                  : {color: AppColors.black},
                              ]}>
                              {item.ReportLocationCode !== ''
                                ? item.ReportLocationName
                                : 'chọn công đoạn'}
                            </Text>
                          </TouchableOpacity>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <TouchableOpacity
                            style={[
                              AppStyles.FormInput,
                              {alignItems: 'center'},
                            ]}
                            onPress={() =>
                              this.setState({offsetDate: true, Index: index})
                            }>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                item.ReportExpectedDate !== null
                                  ? {color: AppColors.black}
                                  : {color: AppColors.gray},
                              ]}>
                              {item.ReportExpectedDate !== null
                                ? FuncCommon.ConDate(item.ReportExpectedDate, 0)
                                : 'Đến ngày'}
                            </Text>
                          </TouchableOpacity>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <TouchableOpacity
                            style={AppStyles.FormInput}
                            onPress={() =>
                              this.onActionComboboxScrapReason(index)
                            }>
                            <Text
                              style={[
                                AppStyles.Textdefault,
                                item.ScrapReasonId === ''
                                  ? {color: AppColors.gray}
                                  : {color: AppColors.black},
                              ]}>
                              {item.ScrapReasonId !== ''
                                ? item.ReportReasonNG
                                : 'chọn lý do'}
                            </Text>
                          </TouchableOpacity>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <TextInput
                            style={AppStyles.FormInput}
                            underlineColorAndroid="transparent"
                            value={item.Description}
                            autoCapitalize="none"
                            onChangeText={txt =>
                              this.setDescriptionDetail(txt, index)
                            }
                          />
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
          <TouchableOpacity
            style={[
              AppStyles.FormInput,
              {
                justifyContent: 'center',
                margin: 10,
                backgroundColor: '#bed9f6',
              },
            ]}
            onPress={() => this.Submit()}>
            <Text style={[AppStyles.Textdefault, {color: 'black'}]}>LƯU</Text>
          </TouchableOpacity>
          {this.state.offsetDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 100,
                width: DRIVER.width,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.setState({offsetDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={
                  this.state.ListView[this.state.Index].ReportExpectedDate ===
                  null
                    ? new Date()
                    : this.state.ListView[this.state.Index].ReportExpectedDate
                }
                mode="date"
                style={{width: DRIVER.width}}
                onDateChange={d => this.setDate(d)}
              />
            </View>
          ) : null}
          {this.state.ListLocaton.length > 0 ? (
            <Combobox
              value={''}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeLocation}
              data={this.state.ListLocaton}
              nameMenu={'Chọn công đoạn'}
              eOpen={this.openComboboxLocation}
              position={'bottom'}
            />
          ) : null}
          {this.state.ListScrapReasons.length > 0 ? (
            <Combobox
              value={''}
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeScrapReason}
              data={this.state.ListScrapReasons}
              nameMenu={'Chọn lý do'}
              eOpen={this.openComboboxScrapReason}
              position={'bottom'}
            />
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  //#region  combobox Location
  _openComboboxLocation() {}
  openComboboxLocation = d => {
    this._openComboboxLocation = d;
  };
  onActionComboboxLocation(index) {
    this.setState({Index: index});
    this._openComboboxLocation();
  }
  ChangeLocation = rs => {
    if (rs !== null) {
      this.state.ListView[this.state.Index].ReportLocationCode = rs.value;
      this.state.ListView[this.state.Index].ReportLocationName = rs.text;
      this.setState({ListView: this.state.ListView});
    }
  };
  //#endregion
  //#region  combobox ScrapReason
  _openComboboxScrapReason() {}
  openComboboxScrapReason = d => {
    this._openComboboxScrapReason = d;
  };
  onActionComboboxScrapReason(index) {
    this.setState({Index: index});
    this._openComboboxScrapReason();
  }
  ChangeScrapReason = rs => {
    if (rs !== null) {
      this.state.ListView[this.state.Index].ScrapReasonId = rs.value;
      this.state.ListView[this.state.Index].ReportReasonNG = rs.text;
      this.setState({ListView: this.state.ListView});
    }
  };
  //#endregion
  //#region setDescriptionDetail
  setDescriptionDetail = (txt, index) => {
    this.state.ListView[index].Description = txt;
    this.setState({ListView: this.state.ListView});
  };
  //#endregion
  //#region setDate
  setDate = date => {
    this.state.ListView[this.state.Index].ReportExpectedDate = date;
    this.setState({ListView: this.state.ListView});
  };
  //#endregion
  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null && val !== '') {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        var _string2 = string[1].substring(0, 2);
        if (_string2 !== '00') {
          return valview + ',' + string[1].substring(0, 2) === '00';
        } else {
          return valview;
        }
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
  //#region Submit
  CustomDate = date => {
    if (date !== null && date !== '') {
      var _date = FuncCommon.ConDate(date, 0).split('/');
      return _date[2] + '-' + _date[1] + '-' + _date[0];
    } else {
      return null;
    }
  };
  Submit = () => {
    var listSub = [];
    for (let i = 0; i < this.state.ListView.length; i++) {
      listSub.push({
        Description: this.state.ListView[i].Description,
        LocationCode: this.state.ListView[i].LocationCode,
        ReportExpectedDate: this.CustomDate(
          this.state.ListView[i].ReportExpectedDate,
        ),
        ReportLocationName: this.state.ListView[i].ReportLocationName,
        ReportReasonNG: this.state.ListView[i].ReportReasonNG,
        RowGuid: this.state.ListView[i].RowGuid,
        ScrapReasonId: this.state.ListView[i].ScrapReasonId,
      });
    }
    FuncCommon.Data_Offline(async d => {
      if (d) {
        API_Operator.UpdatePlanJobCardsReport(listSub)
          .then(res => {
            Alert.alert('Thông báo', res.data.message);
          })
          .catch(error => {
            this.setState({loading: stubFalse});
            console.log(error);
          });
      } else {
        Alert.alert('Thông báo', 'Yêu cầu kết nối đường truyền Internet');
      }
    });
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(Operator_addReportPlan_Component);
