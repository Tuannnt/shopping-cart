import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { AppStyles, AppColors } from '@theme';
import { API_Operator } from "../../../../network";
import { FlatList, Keyboard, Text, TouchableWithoutFeedback, TextInput, ScrollView, View, Dimensions, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../../utils';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_ItemJobCard_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            DataViewJobCard: null,
            DataTable: {
                NumberPage: 0,
                Length: 30
            },
            ListView: [],
            CheckAll: false,
        };
        //#region tabbarbottom
        this.listtabbarBotom = [
            // {
            //     Title: "In-Kết quả CV",
            //     Icon: "server",
            //     Type: "feather",
            //     Value: "0",
            //     Checkbox: false,
            //     Badge: 0
            // },
            {
                Title: "Báo cáo- vấn đề SX",
                Icon: "bell",
                Type: "feather",
                Value: "1",
                Checkbox: false,
                Badge: 0
            },
            {
                Title: "Xác nhận- KQ CV",
                Icon: "check-square",
                Type: "feather",
                Value: "2",
                Checkbox: false,
                Badge: 0
            }
        ];
        //#endregion
    }
    componentDidMount() {
        this.JTablePlanJobCardsBaoCao(true);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.moduleId == "QRCode") {
            var Id = nextProps.Data.Value.data;// dữ liệu QR quét được
            this.GetItemJobCard(Id);
        }
    }
    //#region báo cáo công việc
    GetItemJobCard = (id) => {
        var obj = { Id: id }
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetItemJobCard(obj).then(res => {
                    FuncCommon.Data_Offline_Set('GetItemJobCard' + id, res);
                    this._GetItemJobCard(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('GetItemJobCard' + id);
                this._GetItemJobCard(x);
            }
        });
    }
    _GetItemJobCard = (res) => {
        var rs = JSON.parse(res.data.data);
        this.state.DataViewJobCard = rs;
        this.state.DataViewJobCard.JobCardId = rs.JobCardId;
        this.state.DataViewJobCard.RowGuid = rs.RowGuid;
        this.state.DataViewJobCard.ItemName = rs.PartItemName === "" ? rs.ItemName : rs.PartItemName;
        this.state.DataViewJobCard.QuantityPlan = rs.Quantity;
        this.state.DataViewJobCard.StartTime = rs.StartTime;
        this.state.DataViewJobCard.EndTime = this.HoursNow();
        this.state.DataViewJobCard.QuantityDelivered = rs.Quantity.toString();
        this.state.DataViewJobCard.EmployeeName = rs.EmployeeName;
        this.state.DataViewJobCard.LocationName = rs.LocationName;
        this.state.DataViewJobCard.ActualTotalMinute = rs.TotalMinuteActual < 0 ? 0 : rs.TotalMinuteActual.toString();
        this.state.DataViewJobCard.QuantityNG = "0";
        this.state.DataViewJobCard.QuantityRM = 0;
        this.setState({
            DataViewJobCard: this.state.DataViewJobCard,
            loading: false,
            loadingsearch: false
        });
    }
    //#endregion
    //#region  trạng thái công việc
    JTablePlanJobCardsBaoCao = (s) => {
        if (s === true) {
            this.state.DataTable.NumberPage = 1;
            this.setState(
                {
                    loadingsearch: true,
                    ListView: [],
                }
            );
        } else {
            this.state.DataTable.NumberPage = ++this.state.DataTable.NumberPage
        }
        this.setState({ DataTable: this.state.DataTable });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTablePlanJobCardsBaoCao(this.state.DataTable).then(res => {
                    FuncCommon.Data_Offline_Set('JTablePlanJobCardsBaoCao', res);
                    this._JTablePlanJobCardsBaoCao(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('JTablePlanJobCardsBaoCao');
                this._JTablePlanJobCardsBaoCao(x);
            }
        });
    }
    _JTablePlanJobCardsBaoCao = (res) => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            if (data[i].Status !== "XN") {
                data[i].CheckBox = false;
            }
            this.state.ListView.push(data[i]);
        }
        this.setState({
            ListView: this.state.ListView,
            loading: false,
            loadingsearch: false
        });
    }
    //#endregion
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"Báo cáo công việc"}
                            callBack={() => this.callBackList()}
                            FormQR={true}
                        />
                        <Divider />
                        <View style={{}}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>BÁO CÁO CÔNG VIỆC</Text>
                                </View>
                                <View style={{ justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>Ngày B.Cáo: {FuncCommon.ConDate(new Date(), 0)}</Text>
                                </View>
                            </TouchableOpacity>
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Mã phiếu</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Tên TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Người làm</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Công đoạn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Start</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>End</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Số phút</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Slg KH</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Slg thực</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Slg còn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>% HT</Text></TouchableOpacity>
                                    </View>
                                    {this.state.DataViewJobCard !== null ?
                                        <View style={{ flexDirection: 'row' }} >
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}>
                                                <Text style={[AppStyles.Textdefault]}>
                                                    {this.state.DataViewJobCard.JobCardId}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 150 }]}>
                                                <Text style={[AppStyles.Textdefault]}>
                                                    {this.state.DataViewJobCard.ItemName}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 150 }]}>
                                                <Text style={[AppStyles.Textdefault]}>
                                                    {this.state.DataViewJobCard.EmployeeName}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}>
                                                <Text style={[AppStyles.Textdefault]}>
                                                    {this.state.DataViewJobCard.LocationName}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}>
                                                <Text style={[AppStyles.Textdefault]}>
                                                    {this.convertDate24h(this.state.DataViewJobCard.StartTime)}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}>
                                                <Text style={[AppStyles.Textdefault]}>
                                                    {this.convertDate24h(this.state.DataViewJobCard.EndTime)}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}>
                                                <TextInput style={AppStyles.FormInput}
                                                    underlineColorAndroid="transparent"
                                                    value={this.state.DataViewJobCard.ActualTotalMinute}
                                                    placeholder={'Số phút'}
                                                    // autoFocus={true}
                                                    autoCapitalize="none"
                                                    onChangeText={text =>
                                                        this.setActualTotalMinute(text)
                                                    } />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}>
                                                <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                                                    {this.addPeriod(this.state.DataViewJobCard.QuantityPlan)}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}>
                                                <View style={{ flex: 1 }}>
                                                    <TextInput style={AppStyles.FormInput}
                                                        underlineColorAndroid="transparent"
                                                        value={this.state.DataViewJobCard.QuantityDelivered}
                                                        placeholder={'SL thực'}
                                                        // autoFocus={true}
                                                        autoCapitalize="none"
                                                        onChangeText={text =>
                                                            this.setQuantityDelivered(text)
                                                        } />
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}>
                                                <View style={{ flex: 1 }}>
                                                    <TextInput style={AppStyles.FormInput}
                                                        underlineColorAndroid="transparent"
                                                        value={this.state.DataViewJobCard.QuantityNG}
                                                        placeholder={'SL còn'}
                                                        // autoFocus={true}
                                                        autoCapitalize="none"
                                                        onChangeText={text =>
                                                            this.setQuantityNG(text)
                                                        } />
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}>
                                                <Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>
                                                    {this.state.DataViewJobCard.QuantityDelivered * 100 / this.state.DataViewJobCard.QuantityPlan}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                            {this.state.DataViewJobCard !== null ?
                                <TouchableOpacity style={[AppStyles.FormInput, { justifyContent: 'center', margin: 10, backgroundColor: '#bed9f6', flexDirection: 'row', alignItems: 'center' }]} onPress={() => this.Submit()}>
                                    {/* <Icon type={"antdesign"} name={"save"} /> */}
                                    <Text style={[AppStyles.Textdefault, { color: 'black' }]}>LƯU</Text>
                                </TouchableOpacity>
                                : null}
                        </View>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>TRẠNG THÁI CÁC CÔNG VIỆC</Text>
                                </View>
                            </TouchableOpacity>
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]} onPress={() => this.setCheckAll()}>
                                            {this.state.CheckAll ?
                                                <Icon
                                                    name={"check"}
                                                    type="entypo"
                                                    color={AppColors.ColorEdit}
                                                    size={20} />
                                                :
                                                <Icon
                                                    name={"square-o"}
                                                    type="font-awesome"
                                                    color={AppColors.IconcolorTabbar}
                                                    size={20} />
                                            }
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Mã phiếu</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Tên TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Công đoạn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Slg KH</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Slg thực</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Slg còn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>% HT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Số phút</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Start</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>End</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>XN</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Điều khiển</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListView.length > 0 ?
                                        <FlatList
                                            data={this.state.ListView}
                                            renderItem={({ item, index }) => (
                                                <TouchableOpacity style={{ flexDirection: 'row' }} >
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]} onPress={() => item.Status !== "XN" ? this.CheckBoxItem(index) : null}>
                                                        {item.Status !== "XN" ? item.CheckBox ?
                                                            <Icon
                                                                name={"check"}
                                                                type="entypo"
                                                                color={AppColors.ColorEdit}
                                                                size={20} />
                                                            :
                                                            <Icon
                                                                name={"square-o"}
                                                                type="font-awesome"
                                                                color={AppColors.IconcolorTabbar}
                                                                size={20} />
                                                            : null
                                                        }
                                                    </TouchableOpacity>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.JobCardId}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.PartItemName === "" ? item.ItemName : item.PartItemName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.LocationName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{item.Quantity}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{item.QuantityOk}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{item.QuantityNg}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.converQTT(item)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{item.TotalMinuteActual}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.convertDate24h(item.StartTime)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'right' }]}>{this.convertDate24h(item.EndTime)}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.Status === "XN" ? "XN" : "Chờ"}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}></Text></View>
                                                </TouchableOpacity>
                                            )}
                                            onEndReached={() => this.state.ListView.length >= (30 * this.state.DataTable.NumberPage) ? this.GetDataView(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        // refreshControl={
                                        //     <RefreshControl
                                        //         refreshing={this.state.loading}
                                        //         onRefresh={this._onRefresh}
                                        //         tintColor="#f5821f"
                                        //         titleColor="#fff"
                                        //         colors={[AppColors.Maincolor]}
                                        //     />
                                        // }
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={AppStyles.StyleTabvarBottom}>
                            <TabBarBottomCustom
                                ListData={this.listtabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    //Các hàm dùng chung
    CallbackValueBottom = (key) => {
        var list = this.state.ListView;
        var ListSub = [];
        for (let i = 0; i < list.length; i++) {
            if (list[i].CheckBox === true && list[i].Status !== "XN") {
                ListSub.push(list[i].RowGuid);
            }
        };
        switch (key) {
            case "2":
                if (ListSub.length > 0) {
                    Alert.alert("Xác nhận",
                        "Bạn có muốn Xác nhận - Kết quả công việc không?",
                        [
                            { text: "Xác nhận", onPress: () => this.APIApprovedJobCard(ListSub) },
                            {
                                text: "Đóng",
                                onPress: () => null,
                                style: "cancel"
                            }

                        ],
                        { cancelable: false });
                } else {
                    Alert.alert("Thông báo", "Yêu cầu chọn công việc.");
                }
                break;
            case "1":
                Actions.Operator_addReportPlan();
                break;
            default:
                break;
        }
    };
    APIApprovedJobCard = (listSub) => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.ApprovedJobCard(listSub).then(res => {
                    if (res.data.errorCode !== 200) {
                        Alert.alert("Thông báo", res.data.message);
                    } else {
                        Alert.alert("Thông báo", res.data.message);
                    }
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                Alert.alert("Thông báo", "Yêu cầu kết nối Internet");
            }
        });
    }
    //#region CheckAll
    setCheckAll = () => {
        var list = this.state.ListView;
        for (let i = 0; i < list.length; i++) {
            if (list[i].Status !== "XN") {
                list[i].CheckBox = !this.state.CheckAll
            }
        };
        this.setState({ ListView: list, CheckAll: !this.state.CheckAll });
    }
    CheckBoxItem = (index) => {
        this.state.ListView[index].CheckBox = !this.state.ListView[index].CheckBox;
        var obj = this.state.ListView.find(x => x.CheckBox === false);
        if (obj === undefined) {
            this.state.CheckAll = true;
        } else {
            this.state.CheckAll = false;
        }
        this.setState({ ListView: this.state.ListView, CheckAll: this.state.CheckAll });
    }
    //#endregion
    //#region setActualTotalMinute
    setActualTotalMinute = (text) => {
        this.state.DataViewJobCard.ActualTotalMinute = text;
        this.setState({ DataViewJobCard: this.state.DataViewJobCard });
    }
    setQuantityDelivered = (text) => {
        this.state.DataViewJobCard.QuantityDelivered = text;
        this.state.DataViewJobCard.QuantityNG = parseFloat(this.state.DataViewJobCard.QuantityPlan) - parseFloat(this.state.DataViewJobCard.QuantityDelivered);
        this.state.DataViewJobCard.QuantityNG = this.state.DataViewJobCard.QuantityNG < 0 ? 0 : this.state.DataViewJobCard.QuantityNG;
        this.setState({ DataViewJobCard: this.state.DataViewJobCard });
    }
    setQuantityNG = (text) => {
        this.state.DataViewJobCard.QuantityNG = text;
        this.state.DataViewJobCard.QuantityNG = parseFloat(this.state.DataViewJobCard.QuantityPlan) - parseFloat(this.state.DataViewJobCard.QuantityDelivered);
        this.state.DataViewJobCard.QuantityNG = this.state.DataViewJobCard.QuantityNG < 0 ? 0 : this.state.DataViewJobCard.QuantityNG;
        this.setState({ DataViewJobCard: this.state.DataViewJobCard });
    }
    //#endregion
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion

    //#region Submit
    Submit = () => {
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                if (this.state.DataViewJobCard.JobCardId === "" || this.state.DataViewJobCard.JobCardId === null || this.state.DataViewJobCard.JobCardId === undefined) {
                    Alert.alert(
                        'Thông báo',
                        "Yêu cầu nhập mã phiếu báo cáo công việc",
                        [
                            { text: 'Xác nhận', onPress: () => this.setState({ loading: false }) },
                        ],
                        { cancelable: true }
                    );
                    return;
                }
                if (this.state.DataViewJobCard.ActualTotalMinute <= 0) {
                    Alert.alert(
                        'Thông báo',
                        "Số phút sản xuất phải >= 0",
                        [
                            { text: 'Xác nhận', onPress: () => this.setState({ loading: false }) },
                        ],
                        { cancelable: true }
                    );
                    return;
                }
                API_Operator.UpdatePlanJobCards(this.state.DataViewJobCard).then(res => {
                    debugger
                })
                    .catch(error => {
                        this.setState({ loading: false });
                        console.log(error);
                    });
            } else {
                Alert.alert(
                    "Thông báo",
                    "Yêu cầu kết nối mạng",
                    [
                        { text: "Đóng", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                );
            }
        });








    };
    //#endregion
    HoursNow = () => {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (mm < 10) { mm = "0" + mm }
        if (dd < 10) { dd = "0" + dd }
        return h + ":" + m + ":" + s;
    };
    convertDate24h = (datetime) => {
        if (datetime === "" || datetime === null || datetime === undefined) {
            return "";
        }
        var dateSplit = datetime.split(' ')[2];
        if (dateSplit === "CH" || dateSplit === "PM") {
            var hourse = parseInt(datetime.split(' ')[1].split(':')[0]);
            hourse = hourse + 12;
            if (hourse < 24) {
                return hourse + ":" + datetime.split(' ')[1].split(':')[1] + ":" + datetime.split(' ')[1].split(':')[2];
            }
            else {
                return "12:" + datetime.split(' ')[1].split(':')[1] + ":" + datetime.split(' ')[1].split(':')[2];
            }
        }
        else {
            return datetime.split(' ')[1];
        }
    };
    converQTT = (full) => {
        var QuantityOk = full.QuantityOk === "" ? 0 : parseFloat(full.QuantityOk);
        var Quantity = full.Quantity === "" ? 0 : parseFloat(full.Quantity);
        return this.addPeriod(parseInt(QuantityOk / Quantity * 100)) + " %";
    }
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_ItemJobCard_Component);
