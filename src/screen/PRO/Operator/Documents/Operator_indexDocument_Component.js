import React, { Component } from 'react';
import { FlatList, Keyboard, Animated, Text, TouchableWithoutFeedback, RefreshControl, Image, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../../utils';
import { ScrollView } from 'react-native-gesture-handler';
const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_indexSupplies_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            Data_GetDataView: {
                length: 20,
                NumberPage: 0,
                Code: "",
                Keyword: "",
            },
            ListView: [],
        };
    }
    componentDidMount() {
        this.setState({loading:true})
        this.GetDataView(true);
    }
    GetDataView = (s) => {
        if (s === true) {
            this.state.Data_GetDataView.NumberPage = 1;
            this.setState(
                {
                    loadingsearch: true,
                    ListView: [],
                }
            );
        } else {
            this.state.Data_GetDataView.NumberPage = ++this.state.Data_GetDataView.NumberPage
        }
        this.setState({ Data_GetDataView: this.state.Data_GetDataView });
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.GetDataView(this.state.Data_GetDataView).then(res => {
                    FuncCommon.Data_Offline_Set('Print_GetDataView', res);
                    this._GetAll_Print_GetDataView(res);
                })
                    .catch(error => {
                        this.setState({ loading: false, loadingsearch: false });
                        console.log(error);
                    });
            } else {
                var x = await FuncCommon.Data_Offline_Get('Print_GetDataView');
                this._GetAll_Print_GetDataView(x);
            }
        });
    }
    _GetAll_Print_GetDataView=(res)=>{
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
            this.state.ListView.push(data[i]);
        }
        this.setState({
            ListView: this.state.ListView,
            loading: false,
            loadingsearch: false
        });
    }
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"TÀI LIỆU"}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        <View style={{ flex: 1 }}>
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 70 }]}><Text style={AppStyles.Labeldefault}>Ảnh</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Số SO</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Mã TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Tên TP/BTP</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>C.đoạn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>ĐVT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>S/LKH</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>KQSX</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>Tỉ lệ %</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Mã CV</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 150 }]}><Text style={AppStyles.Labeldefault}>Người làm</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListView.length > 0 ?
                                        <FlatList
                                            data={this.state.ListView}
                                            renderItem={({ item, index }) => (
                                                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.ViewOpen(item)}>
                                                    <View style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 70, alignItems: "center" }]}><Image style={{ width: 60, height: 60 }} source={{ uri: item.HINHANH }} /></View>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.OrderNumber}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>{item.MaTP}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.ItemName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.LocationName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.UnitName}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.Quantity}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.KQSX}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.Percent}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.MCV}</Text></View>
                                                    <View style={[AppStyles.table_td, { width: 150 }]}><Text style={[AppStyles.Textdefault]}>{item.EmployeeName}</Text></View>
                                                </TouchableOpacity>
                                            )}
                                            onEndReached={() => this.state.ListView.length >= (20 * this.state.Data_GetDataView.NumberPage) ? this.GetDataView(false) : null}
                                            keyExtractor={(rs, index) => index.toString()}
                                        // refreshControl={
                                        //     <RefreshControl
                                        //         refreshing={this.state.loading}
                                        //         onRefresh={this._onRefresh}
                                        //         tintColor="#f5821f"
                                        //         titleColor="#fff"
                                        //         colors={[AppColors.Maincolor]}
                                        //     />
                                        // }
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    //Các hàm dùng chung
    ViewOpen = (item) => {
        Actions.Operator_Document({ item: item});
    }
    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_indexSupplies_Component);
