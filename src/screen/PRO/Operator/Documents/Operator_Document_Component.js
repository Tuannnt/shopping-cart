import React, { Component } from 'react';
import { FlatList, Keyboard, Animated, Text, TouchableWithoutFeedback, RefreshControl, Alert, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { AppStyles, AppColors, AppFonts } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_Operator, API_HR } from "../../../../network";
import TabBar_Title from "../../../component/TabBar_Title";
import { Divider, ListItem, Icon } from "react-native-elements";
import LoadingComponent from '../../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../../utils';
import { ScrollView } from 'react-native-gesture-handler';

const DRIVER = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
};
class Operator_Document_Component extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingsearch: false,
            ListJTableQTCN: [],
            ListAttachment: [],
        };
    }
    componentDidMount() {
        this.setState({ loading: true })
        this.JTableQTCN();
    }
    //#region JTableQTCN
    JTableQTCN = () => {
        var obj = {
            Searchs: this.props.item.MaTP
        };
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTableQTCN(obj).then(res => {
                    FuncCommon.Data_Offline_Set('JTableQTCN'+this.props.item.MaTP, res);
                    this._GetJTableQTCN(res);
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                var x = await FuncCommon.Data_Offline_Get('JTableQTCN'+this.props.item.MaTP);
                this._GetJTableQTCN(x);
            }
        });

    };
    _GetJTableQTCN = (res) => {
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
            this.state.ListJTableQTCN.push(data[i]);
        }
        this.setState({
            ListJTableQTCN: this.state.ListJTableQTCN,
            loadingsearch: false
        });
        this.JTableAttachment();
    }
    //#endregion
    //#region Attachment
    JTableAttachment = () => {
        var obj = {
            Searchs: this.props.item.OrderDetailGuid
        };
        FuncCommon.Data_Offline(async (d) => {
            if (d) {
                API_Operator.JTableAttachment(obj).then(res => {
                    FuncCommon.Data_Offline_Set('JTableAttachment', res);
                    this._JTableAttachment(res);
                }).catch(error => {
                    this.setState({ loading: false, loadingsearch: false });
                    console.log(error);
                });
            } else {
                var x = await FuncCommon.Data_Offline_Get('JTableAttachment');
                this._JTableAttachment(x);
            }
        });

    };
    _JTableAttachment = (res) => {
        var data = JSON.parse(res.data.data);
        for (let i = 0; i < data.length; i++) {
            this.state.ListAttachment.push(data[i]);
        }
        this.setState({
            ListAttachment: this.state.ListAttachment,
            loadingsearch: false,
            loading:false
        });
    }
    //#endregion
    render() {
        return (
            this.state.loading !== true ?
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View style={[AppStyles.container]}>
                        <TabBar_Title
                            title={"TÀI LIỆU"}
                            callBack={() => this.callBackList()} />
                        <Divider />
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>QUY TRÌNH CÔNG NGHỆ</Text>
                                </View>
                            </TouchableOpacity>
                            <Divider />
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: DRIVER.width-200 }]}><Text style={AppStyles.Labeldefault}>Tên công đoạn</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>TG SX</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 80 }]}><Text style={AppStyles.Labeldefault}>TG Setup</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListJTableQTCN.length > 0 ?
                                        <FlatList
                                            data={this.state.ListJTableQTCN}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: DRIVER.width-200 }]}><Text style={[AppStyles.Textdefault]}>{item.locationName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.ActualResourceHrs}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 80 }]}><Text style={[AppStyles.Textdefault]}>{item.ActualSetupHrs}</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            keyExtractor={(rs, index) => index.toString()}
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10, backgroundColor: '#bed9f6' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={AppStyles.Labeldefault}>FILE ĐÍNH KÈM</Text>
                                </View>
                            </TouchableOpacity>
                            <Divider />
                            {/* Table */}
                            <ScrollView horizontal={true}>
                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 40 }]}><Text style={AppStyles.Labeldefault}>STT</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: DRIVER.width-140 }]}><Text style={AppStyles.Labeldefault}>Tên file đính kèm</Text></TouchableOpacity>
                                        <TouchableOpacity style={[AppStyles.table_th, { width: 100 }]}><Text style={AppStyles.Labeldefault}>Tải về</Text></TouchableOpacity>
                                    </View>
                                    {this.state.ListAttachment.length > 0 ?
                                        <FlatList
                                            data={this.state.ListAttachment}
                                            renderItem={({ item, index }) => (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 40 }]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>{index + 1}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width:DRIVER.width-140  }]}><Text style={[AppStyles.Textdefault]}>{item.FileName}</Text></TouchableOpacity>
                                                    <TouchableOpacity style={[AppStyles.table_td, { width: 100 }]}><Text style={[AppStyles.Textdefault]}>Tải về</Text></TouchableOpacity>
                                                </View>
                                            )}
                                            keyExtractor={(rs, index) => index.toString()}
                                        />
                                        : <TouchableOpacity style={[AppStyles.table_foot]}><Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>Không có dữ liệu</Text></TouchableOpacity>
                                    }
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </TouchableWithoutFeedback >
                : <LoadingComponent backgroundColor={'#fff'} />
        );
    }
    

    //#region Quay lại trang trước
    callBackList = () => {
        Keyboard.dismiss();
        Actions.pop();
        Actions.refresh({ moduleId: 'Emp', ActionTime: (new Date()).getTime() });
    }
    //#endregion
    //#region định dạng kiểu số
    addPeriod = (nStr) => {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //#endregion
    //#region  customFloat
    customFloat = (val) => {
        if (val !== null && val !== "") {
            var _val = val.toString();
            var string = _val.split(".");
            var valview = this.addPeriod(string[0]);
            if (string.length > 1) {
                var _string2 = string[1].substring(0, 2);
                if (_string2 !== "00") {
                    return valview + "," + string[1].substring(0, 2) === "00"
                } else {
                    return valview
                }
            } else {
                return valview
            }
        } else {
            return 0;
        }
    }
    //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { withRef: true },
)(Operator_Document_Component);
