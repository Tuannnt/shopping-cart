import React, { Component } from 'react';
import {
  Alert,
  FlatList,
  Image,
  RefreshControl,
  Text,
  View,
  Keyboard,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { ErrorHandler } from '@error';
import { Divider, Icon, ListItem, SearchBar } from 'react-native-elements';
import { API_PlanTotalV2 } from '../../../network';
import TabBar from '../../component/TabBar';
import { FuncCommon } from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import { AppStyles, AppColors } from '@theme';
import FormSearch from '../../component/FormSearch';
import MenuSearchDate from '../../component/MenuSearchDate';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import DatePicker from 'react-native-date-picker';
import Combobox from '../../component/Combobox';

const SCREEN_WIDTH = Dimensions.get('window').width;
const ListStatus = [
  {
    value: '0',
    text: 'Đang sản xuất',
  },
  {
    value: '3',
    text: 'Sắp đến hạn',
  },
  {
    value: '4',
    text: 'Quá hạn',
  },
  {
    value: '2',
    text: 'Hoàn thành',
  },
  {
    value: '1',
    text: 'Tạm dừng',
  },
];
class PlanTotal_List_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      setEventEndDate: false,
      setEventStartDate: false,
      ValueSearchDate: '4', // 30 ngày trước
      loadingsearch: true,
      loading: true,
      ListAll: [], //biến chứa danh sách
      EvenFromSearch: false, // on/off form search
      //#region dữ liệu để lấy ra list
      _search: {
        CurrentPage: 0,
        EndDate: new Date(),
        StartDate: new Date(),
        length: 50,
        search: { value: '' },
        QueryOrderBy: 'EndDateTime',
        length: 50,
        CurrentStatus: '',
        IsCancel: '0',
        ObjectID: '',
        OrderNumber: '',
        Title: '',
        TypeId: '',
      },
      //#endregion
    };
    //#region tabbarbottom
    this.listtabbarBotom = [
      {
        Title: 'Đang sản xuất',
        Icon: 'server',
        Type: 'feather',
        Value: 0,
        Checkbox: true,
        Badge: 0,
      },
      {
        Title: 'Hoàn thành',
        Icon: 'check-circle',
        Type: 'feather',
        Value: 2,
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Tạm dừng',
        Icon: 'flag',
        Type: 'feather',
        Value: 1,
        Checkbox: false,
        Badge: 0,
      },
    ];
    //#endregion
  }

  //#region Hàm tìm kiếm
  Search = txt => {
    this.state._search.search.value = txt;
    this.setState({ _search: this.state._search });
    this.nextPage(true);
  };
  //#endregion

  //#region Refresh lại danh sách
  _onRefresh = () => {
    this.nextPage(true);
  };
  //#endregion

  //#region API Danh sách
  nextPage(s) {
    if (s === true) {
      this.state._search.CurrentPage = 1;
      this.state.ListAll = [];
      this.setState({
        loadingsearch: true,
        ListAll: this.state.ListAll,
        _search: this.state._search,
      });
    } else {
      this.state._search.CurrentPage = ++this.state._search.CurrentPage;
      this.setState({ _search: this.state._search });
    }

    let _obj = {
      ...this.state._search,
      StartDate: FuncCommon.ConDate(this.state._search.StartDate, 15),
      EndDate: FuncCommon.ConDate(this.state._search.EndDate, 15),
    };
    API_PlanTotalV2.JTable(_obj)
      .then(res => {
        var data = JSON.parse(res.data.data).data;
        if (this.state._search.CurrentPage === 1) {
          this.state.ListAll = data;
        } else {
          for (let i = 0; i < data.length; i++) {
            this.state.ListAll.push(data[i]);
          }
        }
        this.setState({
          ListAll: this.state.ListAll,
          loading: false,
          loadingsearch: false,
        });
      })
      .catch(error => {
        this.setState({ loading: false, loadingsearch: false });
        console.log(error);
      });
  }
  //#endregion

  //#region View tổng
  render() {
    return (
      <View
        style={{ flex: 1 }}
      >
        <View style={[AppStyles.container]}>
          <TabBar
            title={'Kế hoạch tổng'}
            BackModuleByCode={'PRO'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({ EvenFromSearch: callback })
            }
          />
          {this.state.EvenFromSearch === true ? (
            <View style={{ flexDirection: 'column' }}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                  <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({ setEventStartDate: true })}>
                    <Text>
                      {FuncCommon.ConDate(this.state._search.StartDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
                  <Text style={AppStyles.Labeldefault}>Thời gian kết thúc</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({ setEventEndDate: true })}>
                    <Text>
                      {FuncCommon.ConDate(this.state._search.EndDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'column', padding: 5 }}>
                  <Text style={[AppStyles.Labeldefault, { textAlign: 'center' }]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity
                style={[
                  AppStyles.FormInput,
                  { marginHorizontal: 10, marginTop: 5 },
                ]}
                onPress={() => this.onActionComboboxStatus()}>
                <Text
                  style={[
                    AppStyles.TextInput,
                    this.state._search.IsCancelName
                      ? { color: 'black' }
                      : { color: AppColors.gray },
                  ]}>
                  {this.state._search.IsCancelName
                    ? this.state._search.IsCancelName
                    : 'Chọn trạng thái...'}
                </Text>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                  <Icon
                    color={AppColors.gray}
                    name={'chevron-thin-down'}
                    type="entypo"
                    size={20}
                  />
                </View>
              </TouchableOpacity>
              <FormSearch CallbackSearch={callback => this.Search(callback)} />
            </View>
          ) : null}
          {this.state.loading !== true ? (

            this.ViewList(this.state.ListAll)
          ) : (
            <LoadingComponent backgroundColor={'#fff'} />
          )}
          {/* <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.CallbackValueBottom(callback)
              }
            />
          </View> */}
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(true),
                    this.setState({ setEventStartDate: false });
                }}>
                <Text style={{ textAlign: 'right' }}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state._search.StartDate}
                mode="date"
                style={{ width: SCREEN_WIDTH }}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(true), this.setState({ setEventEndDate: false });
                }}>
                <Text style={{ textAlign: 'right' }}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state._search.EndDate}
                mode="date"
                style={{ width: SCREEN_WIDTH }}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
          {ListStatus.length > 0 ? (
            <Combobox
              TypeSelect={'single'} // single or multiple
              callback={this.ChangeStatus}
              data={ListStatus}
              nameMenu={'Chọn loại'}
              eOpen={this.openComboboxStatus}
              position={'bottom'}
              value={this.state._search.IsCancel}
            />
          ) : null}
        </View>
      </View>
    );
  }
  //#endregion
  ListEmpty = () => {
    if (this.state.ListAll && this.state.ListAll.length > 0) return null;
    return (
      //View to show when list is empty
      <View style={AppStyles.centerAligned}>
        <Text style={{ textAlign: 'center' }}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //#region View danh sách
  ViewList = item => {
    return (
      <FlatList
        data={item}
        ListEmptyComponent={this.ListEmpty}
        renderItem={({ item, index }) => (
          <ListItem
            key={index}
            leftAvatar={
              <Image
                style={{ width: 50, height: 50 }}
                source={{ uri: item.UrlImage }}
              />
            }
            title={
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <Text style={{ fontWeight: 'bold' }}>{item.PartListID}</Text>
                </View>
                <View style={{ flexDirection: 'row', flex: 1 }}>
                  <Text style={{ color: this.setstatus(item, 'C') }}>
                    {this.setstatus(item, 'T')}
                  </Text>
                </View>
              </View>
            }
            subtitle={() => {
              return (
                <View style={{ flexDirection: 'column' }}>
                  {/* <Text>
                    {item.ItemName.length > 35
                      ? item.ItemName.substring(0, 35) + '...'
                      : item.ItemName}
                  </Text> */}
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <Text>ĐVT: </Text>
                      <Text>{item.UnitName}</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <Text>SL: </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                        }}>
                        {FuncCommon.addPeriod(item.QuantityPartLists)}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <Text>BĐ: </Text>
                      <Text>{FuncCommon.ConDate(item.StartDateTine, 0)}</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <Text>KT: </Text>
                      <Text>{FuncCommon.ConDate(item.EndDateTime, 0)}</Text>
                    </View>
                  </View>
                </View>
              );
            }}
            bottomDivider
            onPress={() => this.ActionViewOpen(item)}
          />
        )}
        onEndReached={() =>
          item.length >= 50 * this.state._search.CurrentPage
            ? this.nextPage(false)
            : null
        }
        keyExtractor={(rs, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    );
  };
  //#endregion

  //#region thay đổi thời gian tìm kiếm
  setStartDate = date => {
    this.state._search.StartDate = date;
    this.setState({ _search: this.state._search });
    this.nextPage(true);
  };
  setEndDate = date => {
    this.state._search.EndDate = date;
    this.setState({ _search: this.state._search });
    this.nextPage(true);
  };
  //#endregion
  _openComboboxStatus() { }
  openComboboxStatus = d => {
    this._openComboboxStatus = d;
  };
  onActionComboboxStatus() {
    this._openComboboxStatus();
  }
  ChangeStatus = rs => {
    if (!rs) {
      return;
    }
    if (rs.value === undefined || rs.value === null) {
      return;
    }
    this.state._search.IsCancel = rs.value;
    this.state._search.IsCancelName = rs.text;
    this.setState({ _search: this.state._search });
    this.nextPage(true);
  };
  //#region chức năng lọc này cố định
  _openMenuSearchDate() { }
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.state._search.StartDate = new Date(callback.start);
      this.state._search.EndDate = new Date(callback.end);
      this.setState({
        _search: this.state._search,
        ValueSearchDate: callback.value,
      });
      this.nextPage(true);
    }
  };
  //#endregion

  //#region CallbackValueBottom
  CallbackValueBottom = callback => {
    if (callback !== '' && callback !== null) {
      this.state._search.IsCancel = callback;
      this.setState({ _search: this.state._search });
      this.nextPage(true);
    }
  };
  //#endregion

  //#region ViewOpen
  ActionViewOpen = item => {
    Actions.PlanTotal_Open({ Data: item });
  };
  //#endregion
  //#region Converdate
  ConvertDateSearch = type => {
    var newdate = new Date();
    var month = newdate.getMonth() + 1;
    var monthBefore = newdate.getMonth();
    var year = newdate.getFullYear();
    var day = newdate.getDate();
    if (month < 10) month = '0' + month;
    if (monthBefore < 10) monthBefore = '0' + monthBefore;
    if (day < 10) day = '0' + day;
    if (type === 1) {
      return year + '-' + monthBefore + '-' + day;
    } else {
      return year + '-' + month + '-' + day;
    }
  };
  //#endregion

  setstatus = (full, s) => {
    var datNoew = new Date();
    var year = datNoew.getFullYear();
    var dd = datNoew.getDate();
    var mm = datNoew.getMonth() + 1;
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    var dateNowInt = year + '' + mm + '' + dd;
    if (full.IsFinish === 'True') {
      if (parseInt(full.DateFinishInt) > parseInt(full.EndDateInt)) {
        if (s === 'C') {
          return 'red';
        } else {
          return 'Hoàn thành';
        }
      } else {
        if (s === 'C') {
          return 'blue';
        } else {
          return 'Hoàn thành';
        }
      }
    } else {
      if (
        parseInt(dateNowInt) + 5 >= parseInt(full.EndDateInt) &&
        parseInt(dateNowInt) < parseInt(full.EndDateInt)
      ) {
        return 'Sắp đến hạn';
      } else if (
        parseInt(full.QuantityKHSX) < parseInt(full.QuantityPartLists)
      ) {
        if (parseInt(dateNowInt) > parseInt(full.EndDateInt)) {
          if (s === 'C') {
            return 'red';
          } else {
            return 'Quá hạn';
          }
        } else if (parseInt(dateNowInt) === parseInt(full.EndDateInt)) {
          if (s === 'C') {
            return 'green';
          } else {
            return 'Đến hạn';
          }
        }
      }
    }
    return '';
  };
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(PlanTotal_List_Component);
