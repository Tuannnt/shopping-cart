import React, { Component } from 'react';
import {
  FlatList,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  RefreshControl,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { AppStyles, AppColors } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import { API_PlanTotalV2 } from '../../../network';
import TabBar_Title from '../../component/TabBar_Title';
import { Divider, ListItem, Icon } from 'react-native-elements';
import LoadingComponent from '../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import { FuncCommon } from '../../../utils';
const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class PlanTotal_Open_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transIcon: new Animated.Value(0),
      transIcon2: new Animated.Value(0),
      Data: props.Data !== undefined ? props.Data : null,
      DataBOM: [],
      loading: false,
      UrlImage: props.UrlImage,
      ViewDetailItem: true,
      ViewDetailBOM: true,
    };
    this.ListTabbarBotom = [
      {
        Title: 'Phân loại',
        Icon: 'retweet',
        Type: 'antdesign',
        Value: '1',
        OffEditcolor: true,
        Checkbox: false,
      },
      {
        Title: 'Chuyển KH tổng',
        Icon: 'telegram',
        Type: 'material-community',
        Value: '2',
        OffEditcolor: true,
        Checkbox: false,
      },
      {
        Title: 'Nhập quy cách',
        Icon: 'sharealt',
        Type: 'antdesign',
        Value: '3',
        OffEditcolor: true,
        Checkbox: false,
      },
    ];
  }
  componentDidMount() {
    this.GetListBOM();
  }
  //#region hàm lấy pastList
  GetListBOM = () => {
    this.setState({ loading: true });
    var obj = {
      BomNumber: this.props.Data.BOMNumber,
      Length: 200,
      CurrentPage: 1,
      Type: this.props.Data.Type,
      QueryOrderBy: '',
    };
    API_PlanTotalV2.JTableBOMCategories(obj)
      .then(res => {
        var data = JSON.parse(res.data.data).data;
        this.setState({ DataBOM: data, loading: false });
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error);
      });
  };
  //#endregion
  render() {
    return this.state.loading !== true ? (
      <View
        style={{ flex: 1 }}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          {this.state.Data !== null ? (
            <TabBar_Title
              title={this.state.Data.ItemID}
              callBack={() => this.callBackList()}
            />
          ) : null}
          <Divider />
          {this.state.Data !== null
            ? this.CustomFormDetail(this.state.Data)
            : null}
          {this.state.DataBOM.length > 0
            ? this.CustomJTableBOM(this.state.DataBOM)
            : null}
          {/* {this.state.Data !== null ?
                            <TabBarBottomCustom
                                ListData={this.ListTabbarBotom}
                                onCallbackValueBottom={(callback) => this.CallbackValueBottom(callback)}
                            />
                            : null
                        } */}
        </View>
      </View>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  setstatus = (full, s) => {
    var datNoew = new Date();
    var year = datNoew.getFullYear();
    var dd = datNoew.getDate();
    var mm = datNoew.getMonth() + 1;
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    var dateNowInt = year + '' + mm + '' + dd;
    if (full.IsFinish === 'True') {
      if (parseInt(full.DateFinishInt) > parseInt(full.EndDateInt)) {
        if (s === 'C') {
          return 'red';
        } else {
          return 'Hoàn thành';
        }
      } else {
        if (s === 'C') {
          return 'blue';
        } else {
          return 'Hoàn thành';
        }
      }
    } else {
      if (
        parseInt(dateNowInt) + 5 >= parseInt(full.EndDateInt) &&
        parseInt(dateNowInt) < parseInt(full.EndDateInt)
      ) {
        return 'Sắp đến hạn';
      } else if (
        parseInt(full.QuantityKHSX) < parseInt(full.QuantityPartLists)
      ) {
        if (parseInt(dateNowInt) > parseInt(full.EndDateInt)) {
          if (s === 'C') {
            return 'red';
          } else {
            return 'Quá hạn';
          }
        } else if (parseInt(dateNowInt) === parseInt(full.EndDateInt)) {
          if (s === 'C') {
            return 'green';
          } else {
            return 'Đến hạn';
          }
        }
      }
    }
    return '';
  };
  CustomFormDetail = item => {
    const rotateStart = this.state.transIcon.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return (
      <View style={{ flexDirection: 'column' }}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 5,
          }}>
          <View
            style={{
              borderWidth: 0.5,
              borderRadius: 10,
              borderColor: AppColors.gray,
            }}>
            <Image
              style={{ width: 100, height: 100 }}
              source={{ uri: item.UrlImage }}
            />
          </View>
          <View style={{ padding: 5 }}>
            <Text style={[AppStyles.Textdefault, { textAlign: 'center' }]}>
              {item.ItemName}
            </Text>
          </View>
        </View>
        <Divider />
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setViewDetailItem()}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={AppStyles.Labeldefault}>Thông tin chi tiết</Text>
          </View>
          <Animated.View
            style={{
              transform: [{ rotate: rotateStart }, { perspective: 4000 }],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-down'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewDetailItem === true ? (
          <View style={{ padding: 5 }}>
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Labeldefault}>Mã bản vẽ:</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>{item.ItemID}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Labeldefault}>Số kế hoạch :</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>{item.PartListID}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Labeldefault}>Số PO :</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>{item.SaleOrderNo}</Text>
              </View>
            </View>
            {/* <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Mã KH :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.ObjectID}</Text>
              </View>
            </View> */}
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Labeldefault}>Mã BOM:</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>{item.BOMNumber}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 2, flexDirection: 'row' }}>
                <Text style={AppStyles.Labeldefault}>ĐVT:</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>{item.UnitName}</Text>
              </View>
              {/* <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Loại GC:</Text>
                </View>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.Type === 'U'
                      ? 'GC Đồng'
                      : item.Type === 'K'
                      ? 'Cơ khí'
                      : 'Hoàn thiện'}
                  </Text>
                </View>
              </View> */}
            </View>
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 2 }}>
                <Text style={AppStyles.Labeldefault}>Còn lại:</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>
                  {item.QuantityPartLists - item.QuantityKHSX}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 2, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày BĐ:</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {FuncCommon.ConDate(item.StartDateTine, 0)}
                  </Text>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>S/L:</Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {FuncCommon.addPeriod(item.QuantityPartLists)}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: 'row', padding: 5 }}>
              <View style={{ flex: 2, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Ngày KT:</Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {FuncCommon.ConDate(item.EndDateTime, 0)}
                  </Text>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>KQSX:</Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.QuantityKHSX}
                  </Text>
                </View>
              </View>
            </View>

            {/* <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2, flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Cảnh báo:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text
                    style={[
                      AppStyles.Textdefault,
                      {color: this.setstatus(item, 'C')},
                    ]}>
                    {this.setstatus(item, 'T')}
                  </Text>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Còn lại:</Text>
                </View>
                <View style={{flex: 1}}>
                  <Text style={[AppStyles.Textdefault]}>
                    {item.QuantityPartLists - item.QuantityKHSX}
                  </Text>
                </View>
              </View>
            </View> */}

            {/* <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2, flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Ghi chú:</Text>
                </View>
                <View style={{flex: 2}}>
                  <Text style={AppStyles.Textdefault}>{item.Description}</Text>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'row'}} />
            </View> */}
          </View>
        ) : null}
      </View>
    );
  };
  handleType = data => {
    if (data === 'K') {
      return 'Cơ khí';
    } else if (data === 'D') {
      return 'Điện';
    } else if (data === 'U') {
      return 'Đồng';
    } else if (data === 'O') {
      return 'Phụ';
    }
    return '';
  };
  CustomJTableBOM = item => {
    const rotateStart2 = this.state.transIcon2.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Divider />
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setViewDetailBOM()}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={AppStyles.Labeldefault}>BOM chi tiết</Text>
          </View>
          <Animated.View
            style={{
              transform: [{ rotate: rotateStart2 }, { perspective: 4000 }],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-down'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewDetailBOM !== true ? null : (
          <FlatList
            data={item}
            renderItem={({ item, index }) => (
              <TouchableOpacity style={{ flexDirection: 'column', padding: 5 }}>
                {/* Tên */}
                <View style={{ flex: 1, padding: 2 }}>
                  <Text style={AppStyles.Labeldefault}>
                    {item.ItemName.trim()}
                  </Text>
                </View>
                {/* Mã BTP- ĐVT  */}
                <View style={{ flex: 1, flexDirection: 'row', padding: 2 }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Mã BTP: </Text>
                    <Text style={AppStyles.Textdefault}>
                      {' '}
                      {item.PartItemID || item.MaterialItemID}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>ĐVT: </Text>
                    <Text style={AppStyles.Textdefault}> {item.UnitName}</Text>
                  </View>
                </View>
                {/* Loại- số lượng */}
                <View style={{ flex: 1, flexDirection: 'row', padding: 2 }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Type: </Text>
                    <Text style={AppStyles.Textdefault}>
                      {' '}
                      {this.handleType(item.Type)}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Số lượng: </Text>
                    <Text style={AppStyles.Textdefault}>
                      {FuncCommon.addPeriod(item.Quantity)}
                    </Text>
                  </View>
                </View>
                {/* Kích thược- số lượng */}
                <View style={{ flex: 1, flexDirection: 'row', padding: 2 }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Kích thước: </Text>
                    <Text style={AppStyles.Textdefault}>{item.Size}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Tổng: </Text>
                    <Text style={AppStyles.Textdefault}>
                      {this.totalQtt(item)}
                    </Text>
                  </View>
                </View>
                {/* Spec-Tồn kho */}
                <View style={{ flex: 1, flexDirection: 'row', padding: 2 }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Spec: </Text>
                    <Text style={AppStyles.Textdefault}>
                      {item.Description}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={AppStyles.Labeldefault}>Tổn kho: </Text>
                    <Text style={AppStyles.Textdefault}>
                      {FuncCommon.addPeriod(item.InventoryQuantity)}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    padding: 2,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                    }}>
                    <Text style={AppStyles.Labeldefault}>QTCN (BTP): </Text>
                    <Text style={{ flex: 1, flexWrap: 'wrap' }}>
                      {item.WorkOrderCategoryRouting}
                    </Text>
                    <View />
                  </View>
                  {/* <View style={{ flexDirection: 'row' }} /> */}
                </View>
                <Divider style={{ marginTop: 5 }} />
              </TouchableOpacity>
            )}
            // onEndReached={() => item.length >= (50 * this.state.DataSearch.CurrentPage) ? this.nextPage(false) : null}
            keyExtractor={(rs, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this._onRefresh}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={[AppColors.Maincolor]}
              />
            }
          />
        )}
      </View>
    );
  };
  totalQtt = item => {
    var qtyPartList =
      this.state.Data.QuantityPartLists === ''
        ? 0
        : parseFloat(this.state.Data.QuantityPartLists);
    var qty = item.Quantity === '' ? 0 : parseFloat(item.Quantity);
    return FuncCommon.addPeriod(qty * qtyPartList);
  };
  //#region Quay lại trang trước đó
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'Detail', ActionTime: new Date().getTime() });
  };
  //#endregion

  //#region CallbackValueBottom
  CallbackValueBottom = val => {
    Alert.alert('Thông báo', 'Chức năng đang được nâng cấp.');
  };
  //#endregion

  //#region setViewDetailItem
  setViewDetailItem = () => {
    this.setState({ ViewDetailItem: !this.state.ViewDetailItem });
    if (this.state.ViewDetailItem === true) {
      Animated.timing(this.state.transIcon, {
        toValue: 1, // from value 0 to 100
        duration: 333, // Thời gian thay đổi
      }).start();
    } else {
      Animated.timing(this.state.transIcon, {
        toValue: 0, // from value 0 to 100
        duration: 333, // Thời gian thay đổi
      }).start();
    }
  };
  setViewDetailBOM = () => {
    this.setState({ ViewDetailBOM: !this.state.ViewDetailBOM });
    if (this.state.ViewDetailBOM === true) {
      Animated.timing(this.state.transIcon2, {
        toValue: 1, // from value 0 to 100
        duration: 333, // Thời gian thay đổi
      }).start();
    } else {
      Animated.timing(this.state.transIcon2, {
        toValue: 0, // from value 0 to 100
        duration: 333, // Thời gian thay đổi
      }).start();
    }
  };
  //#endregion
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(PlanTotal_Open_Component);
