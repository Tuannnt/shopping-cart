import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  RefreshControl,
  Text,
  View,
  Keyboard,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Divider, Icon, ListItem, SearchBar} from 'react-native-elements';
import {API_ProductionPlanDayV2} from '../../../network';
import TabBar from '../../component/TabBar';
import {FuncCommon} from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import {AppStyles, AppColors} from '@theme';
import FormSearch from '../../component/FormSearch';
import MenuSearchDate from '../../component/MenuSearchDate';
import DatePicker from 'react-native-date-picker';
import SearchDay from '../../component/SearchDay';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
const SCREEN_WIDTH = Dimensions.get('window').width;
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFCC33',
    color: 'black',
  },
  container: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 5,
    paddingVertical: 5,
    borderRadius: 1,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  black: {
    color: 'black',
  },
  subtitleStyle: {
    fontSize: 12,
    fontFamily: 'Arial',
  },
  content: {
    padding: 4,
  },
  card: {
    margin: 4,
  },
  CollapseHeader_Text: {
    color: '#0033FF',
    marginLeft: 5,
    marginRight: 10,
    padding: 5,
    fontFamily: 'bold',
  },
  CollapseHeader: {
    flexDirection: 'row',
    padding: 20,
  },
  Iconmenu: {
    width: '20%',
    alignItems: 'center',
  },
  timeHeader: {
    marginBottom: 3,
  },
});
class ProductionPlanDay_List_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ValueSearchDate: '7', //1 thang truoc
      setEventEndDate: false,
      setEventStartDate: false,
      ListAll: [], //biến chứa danh sách
      EvenFromSearch: false, // on/off form search
    };
    (this.DataSearch = {
      CurrentPage: 0,
      IsProcess: '1',
      LocationCode: '',
      MachineID: '',
      search: {
        value: '',
      },
      Length: 15,
      QueryOrderBy: '',
      // Date: new Date(),
      StartDate: new Date(),
      EndDate: new Date(),
    }),
      //#region tabbarbottom
      (this.listtabbarBotom = [
        {
          Title: 'Đang xử lý',
          Icon: 'server',
          Type: 'feather',
          Value: 1,
          Checkbox: true,
          Badge: 0,
        },
        {
          Title: 'Hoàn thành',
          Icon: 'check-circle',
          Type: 'feather',
          Value: 2,
          Checkbox: false,
          Badge: 0,
        },
        {
          Title: 'Dừng sản xuất',
          Icon: 'flag',
          Type: 'feather',
          Value: 3,
          Checkbox: false,
          Badge: 0,
        },
      ]);
    //#endregion
  }

  //#region Hàm tìm kiếm

  //#region Lọc ngày
  Searchday(value) {
    this.DataSearch.Date = value;
    this.nextPage(true);
  }
  //#endregion
  componentDidMount = () => {
    this.Search('');
  };
  Search = txt => {
    this.DataSearch.search.value = txt;
    this.nextPage(true);
  };
  //#endregion

  //#region Refresh lại danh sách
  _onRefresh = () => {
    this.nextPage(true);
  };
  //#endregion

  //#region API Danh sách
  getAll(s) {
    this.setState({loading: true}, () => {
      if (s === true) {
        this.DataSearch.CurrentPage = 1;
        this.state.ListAll = [];
      } else {
        this.DataSearch.CurrentPage++;
      }
      let obj = {
        ...this.DataSearch,
        StartDate: FuncCommon.ConDate(this.DataSearch.StartDate, 16),
        EndDate: FuncCommon.ConDate(this.DataSearch.EndDate, 16),
      };
      API_ProductionPlanDayV2.JTable(obj)
        .then(res => {
          var data = JSON.parse(res.data.data).data;
          if (this.DataSearch.CurrentPage === 1) {
            this.state.ListAll = data;
          } else {
            for (let i = 0; i < data.length; i++) {
              this.state.ListAll.push(data[i]);
            }
          }

          this.setState({
            ListAll: this.state.ListAll,
            loading: false,
          });
        })
        .catch(error => {
          this.setState({loading: false});
          console.log(error);
        });
    });
  }
  nextPage(s) {
    FuncCommon.Data_Offline(async d => {
      if (d) {
        this.getAll(s);
      } else {
        let data = await FuncCommon.Data_Offline_Get('ProductionPlanDay');
        this.setState({
          ListAll: data,
          loading: false,
        });
      }
    });
  }
  //#endregion
  //#region View tổng
  render() {
    return (
      <View
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar
            title={'Kế hoạch ngày'}
            BackModuleByCode={'PRO'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
          />
          {/* <SearchDay CallBackValue={value => this.Searchday(value)} /> */}
          {this.state.EvenFromSearch === true ? (
            <View style={{flexDirection: 'column'}}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    padding: 5,
                  }}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian bắt đầu
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventStartDate: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.DataSearch.StartDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={[AppStyles.Labeldefault, styles.timeHeader]}>
                    Thời gian kết thúc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventEndDate: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.DataSearch.EndDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column', padding: 5}}>
                  <Text
                    style={[
                      AppStyles.Labeldefault,
                      {textAlign: 'center'},
                      styles.timeHeader,
                    ]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <FormSearch CallbackSearch={callback => this.Search(callback)} />
            </View>
          ) : null}
          {this.ViewList(this.state.ListAll)}
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.CallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(true),
                    this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.DataSearch.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.nextPage(true), this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.DataSearch.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
        </View>
      </View>
    );
  }
  //#endregion
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.DataSearch.StartDate = new Date(callback.start);
      this.DataSearch.EndDate = new Date(callback.end);
      this.setState({
        ValueSearchDate: callback.value,
      });
      this.nextPage(true);
    }
  };
  ListEmpty = () => {
    if (this.state.ListAll && this.state.ListAll.length > 0) {
      return null;
    }
    return (
      //View to show when list is empty
      <View style={[AppStyles.centerAligned, {marginTop: 10}]}>
        <Text style={{textAlign: 'center'}}>Không có dữ liệu.</Text>
      </View>
    );
  };
  //#region View danh sách
  ViewList = item => {
    return (
      <FlatList
        data={item}
        ListEmptyComponent={this.ListEmpty}
        renderItem={({item, index}) => (
          <ListItem
            key={index}
            leftAvatar={
              <Image
                style={{width: 50, height: 50}}
                source={
                  item.ImageUrl
                    ? {uri: item.ImageUrl}
                    : require('./no_image.png')
                }
              />
            }
            title={
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Text style={{fontWeight: 'bold'}}>{item.OrderPlanDay}</Text>
                </View>
              </View>
            }
            subtitle={() => {
              return (
                <View style={{flexDirection: 'column'}}>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Mã vật liệu: </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        {item.PartItemId}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                      }}>
                      <Text style={[AppStyles.Textdefault]}>
                        {item.LocationName}
                      </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>Tỉ lệ: </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        {(parseInt(
                          item.QuantityOk === '' ? 0 : item.QuantityOk,
                        ) /
                          parseInt(item.Quantity)) *
                          100}{' '}
                        %
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                      }}>
                      <Text style={AppStyles.Labeldefault}>ĐVT: </Text>
                      <Text style={[AppStyles.Textdefault]}>{item.UnitId}</Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={{flex: 3, flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>S/L KH: </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        {parseInt(item.Quantity) + ''}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                      }}>
                      <Text style={AppStyles.Labeldefault}>KQSX: </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        {item.QuantityOk === ''
                          ? 0
                          : parseInt(item.QuantityOk) + ''}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={{flex: 2, flexDirection: 'row'}}>
                      <Text style={AppStyles.Labeldefault}>SL còn : </Text>
                      <Text style={[AppStyles.Textdefault]}>
                        {item.Quantity - item.QuantityOk}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
            bottomDivider
            onPress={() => this.ActionOpen(item)}
          />
        )}
        onEndReached={() =>
          item.length >= 50 * this.DataSearch.CurrentPage
            ? this.nextPage(false)
            : null
        }
        keyExtractor={(rs, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    );
  };
  //#endregion

  //#region Action open
  ActionOpen = item => {
    Actions.ProductionPlanDay_Open({Data: item});
  };
  //#endregion

  //#region thay đổi thời gian tìm kiếm
  setStartDate = date => {
    this.DataSearch.StartDate = date;
    this.nextPage(true);
  };
  setEndDate = date => {
    this.DataSearch.EndDate = date;
    this.nextPage(true);
  };
  //#endregion

  //#region chức năng lọc này cố định
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  //#endregion

  //#region CallbackValueBottom
  CallbackValueBottom = callback => {
    if (callback !== '' && callback !== null) {
      this.DataSearch.IsProcess = callback;
      this.nextPage(true);
    }
  };
  //#endregion

  ConvertDateSearch = type => {
    var newdate = new Date();
    var month = newdate.getMonth() + 1;
    var monthBefore = newdate.getMonth();
    var year = newdate.getFullYear();
    var day = newdate.getDate();
    if (month < 10) {
      month = '0' + month;
    }
    if (monthBefore < 10) {
      monthBefore = '0' + monthBefore;
    }
    if (day < 10) {
      day = '0' + day;
    }
    if (type === 1) {
      return year + '-' + monthBefore + '-' + day;
    } else {
      return year + '-' + month + '-' + day;
    }
  };
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ProductionPlanDay_List_Component);
