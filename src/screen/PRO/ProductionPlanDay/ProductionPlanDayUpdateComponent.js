import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Keyboard,
  StyleSheet,
  ToastAndroid,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Button} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {API_ProductionPlanDayV2} from '../../../network';
import {Container, Content} from 'native-base';
import {ErrorHandler} from '@error';
import TabBar_Title from '../../component/TabBar_Title';
import {AppStyles, AppColors} from '@theme';
import LoadingComponent from '../../component/LoadingComponent';
import RequiredText from '../../component/RequiredText';
import {FuncCommon} from '@utils';
import Toast from 'react-native-simple-toast';
class ProductionPlanDayUpdateComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: {},
      refreshing: false,
    };
  }
  componentDidMount() {
    this.getItem();
  }
  getItem = () => {
    let id = this.props.Guid;
    this.setState({refreshing: true}, () => {
      API_ProductionPlanDayV2.GetItem(id)
        .then(res => {
          console.log(res);
          if (!JSON.parse(res.data.data)) {
            return;
          }
          this.setState({model: JSON.parse(res.data.data), refreshing: false});
        })
        .catch(error => {
          console.log(error);
          this.setState({refreshing: false});
        });
    });
  };
  setQuantity = q => {
    this.setState({model: {...this.state.model, Quantity: q}});
  };
  render() {
    const {model} = this.state;
    return this.state.refreshing ? (
      <LoadingComponent />
    ) : (
      <Container>
        <ScrollView style={{flex: 30}}>
          <View style={{flex: 40}}>
            <TabBar_Title
              title={`Sửa số lượng kế hoạch`}
              callBack={() => this.onPressBack()}
            />
            <View style={styles.containerContent}>
              <Content padder>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Mã công việc</Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={model.OrderPlanDay}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Mã bán thành phẩm</Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={model.PartItemId}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Mã thành phẩm</Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={model.ItemId}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>
                    Số lượng sản xuất <RequiredText />
                  </Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    value={model.Quantity + ''}
                    onChangeText={Quantity => this.setQuantity(Quantity)}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Tên bán thành phẩm</Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={model.PartItemName}
                    editable={false}
                  />
                </View>
                <View style={{padding: 10}}>
                  <Text style={AppStyles.Labeldefault}>Tên thành phẩm</Text>
                  <TextInput
                    style={[AppStyles.FormInput, {color: 'black'}]}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={model.ItemName}
                    editable={false}
                  />
                </View>
              </Content>
            </View>
          </View>
        </ScrollView>
        <View>
          <Button
            buttonStyle={{backgroundColor: AppColors.ColorButtonSubmit}}
            title="Xác nhận"
            onPress={() => this.onSubmit()}
          />
        </View>
      </Container>
    );
  }
  onPressBackToList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'BackToList', ActionTime: new Date().getTime()});
  }
  onPressBack() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  Add = () => {
    if (!this.state.model.Quantity) {
      Toast.showWithGravity(
        'Yêu cầu nhập số lượng sản xuất',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let obj = {...this.state.model, Quantity: +this.state.model.Quantity};
    API_ProductionPlanDayV2.Update(obj)
      .then(res => {
        if (res.data.errorCode !== 200) {
          Toast.showWithGravity('Có lỗi xảy ra', Toast.SHORT, Toast.CENTER);
          return;
        }
        Toast.showWithGravity(
          'Chỉnh sửa thành công',
          Toast.SHORT,
          Toast.CENTER,
        );
        this.onPressBackToList();
      })
      .catch(error => {
        console.log(error.data);
        console.log('=======' + error.data);
      });
  };
  onSubmit() {
    FuncCommon.Data_Offline(async d => {
      if (!d) {
        Toast.showWithGravity(
          'Yêu cầu kết nối mạng',
          Toast.SHORT,
          Toast.CENTER,
        );
        return;
      } else {
        this.Add();
      }
    });
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductionPlanDayUpdateComponent);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 10,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 10,
  },
  buttonContainer: {
    margin: 10,
    paddingTop: 20,
  },
});
