import React, { Component } from 'react';
import {
  Keyboard,
  Animated,
  Text,
  Image,
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { AppStyles, AppColors } from '@theme';
import { connect } from 'react-redux';
import { ErrorHandler } from '@error';
import TabBar_Title from '../../component/TabBar_Title';
import { Divider, ListItem, Icon } from 'react-native-elements';
import LoadingComponent from '../../component/LoadingComponent';
import { Actions } from 'react-native-router-flux';
import TabBarBottom from '../../component/TabBarBottom';
import { API_ProductionPlanDayV2 } from '../../../network';
import { FuncCommon } from '../../../utils';
import { ToastAndroid } from 'react-native';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
class ProductionPlanDay_Open_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transIcon: new Animated.Value(0),
      Data: props.Data !== undefined ? props.Data : null,
      loading: false,
      ImageUrl: props.Data.ImageUrl,
      ViewDetailItem: true,
    };
  }
  callbackOpenUpdate = () => {
    const { QuantityOk = 0, Quantity = 0 } = this.state.Data;
    const per = Number(Quantity) - Number(QuantityOk) <= 0 ? true : false;
    if (per) {
      Toast.showWithGravity(
        'Công việc đã hoàn thành',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    Actions.ProductionPlanDayUpdate({
      Guid: this.props.Data.PlanDayGuid,
    });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId === 'BackToList') {
      this.callBackList();
    }
  }
  handleConfirm = () => {
    // const {QuantityOk = 0, Quantity = 0} = this.state.Data
    // const per = Number(Quantity) - Number(QuantityOk) <= 0 ? false : true
    let LoginName = global.__appSIGNALR.SIGNALR_object.USER.LoginName;
    if (
      LoginName === 'QC-01' ||
      LoginName === 'SX-01' ||
      LoginName === 'PKT-01'
    ) {
    } else {
      Toast.showWithGravity(
        'Bạn không có quyền cập nhật',
        Toast.SHORT,
        Toast.CENTER,
      );
    }
  };
  render() {
    return this.state.loading !== true ? (
      <View style={{ flex: 1 }}>
        <View style={[AppStyles.container]}>
          {this.state.Data !== null ? (
            <TabBar_Title
              title={this.state.Data.OrderPlanDay}
              callBack={() => this.callBackList()}
            />
          ) : null}
          <Divider />
          {this.state.Data !== null
            ? this.CustomFormDetail(this.state.Data)
            : null}
        </View>
        {/* <View
          style={[AppStyles.StyleTabvarBottom, {backgroundColor: '#ffffff'}]}>
          <TabBarBottom
            onDelete={() => this.onDelete()}
            Title={this.state.Data.OrderPlanDay}
            isHaveConfirm={true}
            handleConfirm={this.handleConfirm}
            callbackOpenUpdate={this.callbackOpenUpdate}
            btnsConfirm={[
              {
                name: 'Xác nhận hoàn thành',
                value: 'D',
                color: '#3366CC',
              },
              {
                name: 'Đóng',
                value: 'H',
                color: '#979797',
              },
            ]}
          />
        </View> */}
      </View>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  //xoa
  onDelete = () => {
    let id = { Id: this.props.Data.PlanDayGuid };
    this.setState({ loading: true }, () => {
      API_ProductionPlanDayV2.Delete(id.Id)
        .then(response => {
          if (response.data.errorCode === 200) {
            this.setState({
              loading: false,
            });
            Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
            this.callBackList();
          } else {
            Toast.showWithGravity(
              response.data.message || 'Có lỗi khi xóa',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.setState({
              loading: false,
            });
          }
        })
        .catch(error => {
          this.setState({ loading: false });
          console.log(error.data);
        });
    });
  };
  helperQuantity = item => {
    var QuantityOk = item.QuantityOk === '' ? 0 : parseInt(item.QuantityOk);
    var Quantity = item.Quantity === '' ? 0 : parseInt(item.Quantity);
    var per = Quantity - QuantityOk;
    return FuncCommon.addPeriod(per) || '';
  };
  CustomFormDetail = item => {
    const rotateStart = this.state.transIcon.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return (
      <View style={{ flexDirection: 'column' }}>
        <View style={{ flexDirection: 'row', padding: 5 }}>
          <View
            style={{
              borderWidth: 0.5,
              borderRadius: 10,
              borderColor: AppColors.gray,
              marginRight: 5,
            }}>
            <Image
              style={{ width: 100, height: 100 }}
              source={
                item.ImageUrl ? { uri: item.ImageUrl } : require('./no_image.png')
              }
            />
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', padding: 2 }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Mã CV :</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>{item.OrderPlanDay}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', padding: 2 }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Số SO :</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>{item.OrderNumber}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', padding: 2 }}>
              <View style={{ flex: 1 }}>
                <Text style={AppStyles.Labeldefault}>Tên Bản vẽ :</Text>
              </View>
              <View style={{ flex: 3 }}>
                <Text style={AppStyles.Textdefault}>{item.ItemName}</Text>
              </View>
            </View>
          </View>
        </View>
        <Divider />
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setViewDetailItem()}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={AppStyles.Labeldefault}>Thông tin chi tiết</Text>
          </View>
          <Animated.View
            style={{
              transform: [{ rotate: rotateStart }, { perspective: 4000 }],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-down'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewDetailItem === true && (
          <ScrollView>
            <View style={{ flexDirection: 'column', flex: 1, padding: 10 }}>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Mã bản vẽ: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>{item.ItemId}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>QTCN: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>{item.LocationName}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Tỉ lệ: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {(parseInt(item.QuantityOk === '' ? 0 : item.QuantityOk) /
                      parseInt(item.Quantity)) *
                      100}{' '}
                    %
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>TG SX: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {item.ProductionTime} phút
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>S/L KH: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {FuncCommon.addPeriod(item.Quantity)}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>KQSX: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {FuncCommon.addPeriod(item.QuantityOk)}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>SL còn: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {this.helperQuantity(item)}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>ĐVT: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>{item.UnitId}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Từ ngày: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {item.PlanDate &&
                      moment(item.PlanDate).format('DD/MM/YYYY')}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={AppStyles.Labeldefault}>Đến ngày: </Text>
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={AppStyles.Textdefault}>
                    {item.PlanDateEnd &&
                      moment(item.PlanDateEnd).format('DD/MM/YYYY')}
                  </Text>
                </View>
              </View>
              {/* <View style={{flexDirection: 'row', paddingBottom: 10}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Số SO: </Text>
              </View>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Textdefault}>{item.OrderNumber}</Text>
              </View>
            </View> */}

              {/* <View style={{flexDirection: 'row',paddingBottom: 10}}>

              <View style={{flex: 3, flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Labeldefault}>Tên BTP :</Text>
                </View>
                <View style={{flex: 1}}>
                  <Text style={AppStyles.Textdefault}>{item.PartItemName}</Text>
                </View>
              </View>
            </View> */}
            </View>
          </ScrollView>
        )}
      </View>
    );
  };

  //#region Quay lại trang trước đó
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({ moduleId: 'Back', ActionTime: new Date().getTime() });
  };
  //#endregion

  //#region CallbackValueBottom
  CallbackValueBottom = val => {
    Alert.alert('Thông báo', 'Chức năng đang được nâng cấp.');
  };
  //#endregion

  //#region setViewDetailItem
  setViewDetailItem = () => {
    this.setState({ ViewDetailItem: !this.state.ViewDetailItem });
    if (this.state.ViewDetailItem === true) {
      Animated.timing(this.state.transIcon, {
        toValue: 1, // from value 0 to 100
        duration: 333, // Thời gian thay đổi
      }).start();
    } else {
      Animated.timing(this.state.transIcon, {
        toValue: 0, // from value 0 to 100
        duration: 333, // Thời gian thay đổi
      }).start();
    }
  };

  //#endregion
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ProductionPlanDay_Open_Component);
