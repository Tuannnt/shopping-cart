import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  RefreshControl,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {AppStyles, AppColors, AppFonts} from '@theme';
import {connect} from 'react-redux';
import {ErrorHandler} from '@error';
import {API_BomCategories} from '../../../network';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import TabBarBottom from '../../component/TabBarBottom';
import {FuncCommon} from '../../../utils';
import {ScrollView} from 'react-native-gesture-handler';

const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class BomCategories_Open_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      BomPending: props.BomPending !== undefined ? props.BomPending : 1,
      loading: false,
      //hiển thị thông tin chi tiết
      ViewDetailItem: true,
      transIcon: new Animated.Value(0),
      Data: props.Data !== undefined ? props.Data : null,
      //hiển thị quy trình công nghệ
      ViewBOMWorkOrderRoutings: false,
      transIcon_ViewBOMWorkOrderRoutings: new Animated.Value(0),
      DataBOMWorkOrderRoutings: [], // Quy trình công nghệ
      DataBOMWorkpieces: [], //định mực phôi
      //hiển thị NVL-BTP
      ViewBOMMaterialNorms: false,
      transIcon_ViewBOMMaterialNorms: new Animated.Value(0),
      DataBOMMaterialNorms_VT: [],
      DataBOMMaterialNorms_BTP: [],
      DataBOMWorkpieces_BTP: [],
      //Hiển thị bao bì
      ViewProductCostNorms: false,
      transIcon_ViewProductCostNorms: new Animated.Value(0),
      DataProductCostNorms: [],
      //Hiển thị nhân công trực tiếp
      ViewLaborCosts: false,
      transIcon_ViewLaborCosts: new Animated.Value(0),
      DataLaborCosts: [],
      //hiển thị Chi phí máy
      ViewMachineCosts: false,
      transIcon_ViewMachineCosts: new Animated.Value(0),
      DataMachineCosts: [],
      //hiển thị chi phí CCDC
      ViewToolsCosts: false,
      transIcon_ViewToolsCosts: new Animated.Value(0),
      DataToolsCosts: [],
      //Gia công ngoài
      ViewProductCostNormsCosts: false,
      transIcon_ViewProductCostNormsCosts: new Animated.Value(0),
      DataProductCostNormsCosts: [],
      //Sản xuất chung
      ViewProductCostNormsCostsSX: false,
      transIcon_ViewProductCostNormsCostsSX: new Animated.Value(0),
      DataProductCostNormsCostsSX: [],
      //Chi phí tài chính
      ViewProductCostNormsCostsTC: false,
      transIcon_ViewProductCostNormsCostsTC: new Animated.Value(0),
      DataProductCostNormsCostsTC: [],
      //Tổng hợp chi phí
      ViewGetBOMCostNormHistories: false,
      transIcon_ViewGetBOMCostNormHistories: new Animated.Value(0),
      DataGetBOMCostNormHistories: [],
      //Quy cách đóng gói- giao hàng
      ViewItemById: false,
      transIcon_ViewItemById: new Animated.Value(0),
      DataItemById: null,
    };
  }
  componentDidMount() {
    this.Skill();
  }
  //#region Skill
  Skill = () => {
    //#region đóng mở thông tin chi tiết
    this.Animated_on_DetailItem = Animated.timing(this.state.transIcon, {
      toValue: 1,
      duration: 333,
    });
    this.Animated_off_DetailItem = Animated.timing(this.state.transIcon, {
      toValue: 0,
      duration: 333,
    });
    //#endregion

    //#region đóng mở tab quy trình công nghệ
    this.Animated_on_BOMWorkOrderRoutings = Animated.timing(
      this.state.transIcon_ViewBOMWorkOrderRoutings,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_BOMWorkOrderRoutings = Animated.timing(
      this.state.transIcon_ViewBOMWorkOrderRoutings,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab NVL-BTP
    this.Animated_on_BOMMaterialNorms = Animated.timing(
      this.state.transIcon_ViewBOMMaterialNorms,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_BOMMaterialNorms = Animated.timing(
      this.state.transIcon_ViewBOMMaterialNorms,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab Bao bì
    this.Animated_on_ProductCostNorms = Animated.timing(
      this.state.transIcon_ViewProductCostNorms,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ProductCostNorms = Animated.timing(
      this.state.transIcon_ViewProductCostNorms,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab Nhân công trực tiếp
    this.Animated_on_LaborCosts = Animated.timing(
      this.state.transIcon_ViewLaborCosts,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_LaborCosts = Animated.timing(
      this.state.transIcon_ViewLaborCosts,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab chi phí máy
    this.Animated_on_MachineCosts = Animated.timing(
      this.state.transIcon_ViewMachineCosts,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_MachineCosts = Animated.timing(
      this.state.transIcon_ViewMachineCosts,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab Chi phí CCDC
    this.Animated_on_ToolsCosts = Animated.timing(
      this.state.transIcon_ViewToolsCosts,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ToolsCosts = Animated.timing(
      this.state.transIcon_ViewToolsCosts,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab Gia công ngoài
    this.Animated_on_ProductCostNormsCosts = Animated.timing(
      this.state.transIcon_ViewProductCostNormsCosts,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ProductCostNormsCosts = Animated.timing(
      this.state.transIcon_ViewProductCostNormsCosts,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab sản xuất chung
    this.Animated_on_ProductCostNormsCostsSX = Animated.timing(
      this.state.transIcon_ViewProductCostNormsCostsSX,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ProductCostNormsCostsSX = Animated.timing(
      this.state.transIcon_ViewProductCostNormsCostsSX,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab chi phí tài chính
    this.Animated_on_ProductCostNormsCostsTC = Animated.timing(
      this.state.transIcon_ViewProductCostNormsCostsTC,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ProductCostNormsCostsTC = Animated.timing(
      this.state.transIcon_ViewProductCostNormsCostsTC,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab tổng hợp chi phí
    this.Animated_on_BOMCostNormHistories = Animated.timing(
      this.state.transIcon_ViewGetBOMCostNormHistories,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_BOMCostNormHistories = Animated.timing(
      this.state.transIcon_ViewGetBOMCostNormHistories,
      {toValue: 0, duration: 333},
    );
    //#endregion

    //#region đóng mở tab Quy cách đóng gói - giao hàng
    this.Animated_on_ItemById = Animated.timing(
      this.state.transIcon_ViewItemById,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_ItemById = Animated.timing(
      this.state.transIcon_ViewItemById,
      {toValue: 0, duration: 333},
    );
    //#endregion
  };
  //#endregion

  render() {
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          {this.state.Data !== null ? (
            <TabBar_Title
              title={'Chi tiết thiết lập BOM'}
              callBack={() => this.callBackList()}
            />
          ) : null}
          <Divider />
          <View style={{flex: 1}}>
            {this.state.Data !== null
              ? this.state.BomPending === 1
                ? this.CustomFormDetail(this.state.Data)
                : this.CustomFormDetailV2(this.state.Data)
              : null}
          </View>
          <View style={[AppStyles.StyleTabvarBottom]}>
            {this.state.checkInLogin !== '' ? (
              <TabBarBottom
                Title={this.state.Data.OrderNumber}
                onAttachment={() => this.openAttachments()}
              />
            ) : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  CustomFormDetail = item => {
    const rotateStart = this.state.transIcon.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return (
      <View style={{flexDirection: 'column'}}>
        <View style={{flexDirection: 'column', padding: 5}}>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <View
              style={{
                borderWidth: 0.5,
                borderRadius: 10,
                borderColor: AppColors.gray,
              }}>
              <Image
                style={{width: 100, height: 100}}
                source={{uri: item.UrlImage}}
              />
            </View>
            <View style={{padding: 5}}>
              <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                {item.ItemName}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection: 'row', marginRight: 10}}>
                <Text style={AppStyles.Labeldefault}>ĐVT: </Text>
                <Text style={AppStyles.Textdefault}>{item.UnitName}</Text>
              </View>
              <View style={{flexDirection: 'row', marginLeft: 10}}>
                <Text style={AppStyles.Labeldefault}>SL: </Text>
                <Text style={AppStyles.Textdefault}>{item.Quantity}</Text>
              </View>
            </View>
          </View>
        </View>
        <Divider />
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Thông tin chi tiết</Text>
          </View>
          <Animated.View
            style={{
              transform: [{rotate: rotateStart}, {perspective: 4000}],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-down'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewDetailItem === true ? (
          <View style={{padding: 5}}>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Số đơn hàng :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.OrderNumber}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Mã sản phẩm(NB) :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.ItemID}</Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>KD phụ trách :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.EmployeeName}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Khách hàng :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.ObjectID}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Ngày YC :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.ConDate(item.ConfirmDate, 0)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Ngày cần :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.ConDate(item.RequireDate, 0)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Ngày XN :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.ConDate(item.ConfirmDateDeliveryDates, 0)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Ưu điểm :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  {this.CusPriority(item.Priority)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Đặc điểm :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.Specification}</Text>
              </View>
            </View>
          </View>
        ) : null}
      </View>
    );
  };
  CustomFormDetailV2 = item => {
    return (
      <ScrollView style={{flexDirection: 'column'}}>
        <View style={{flexDirection: 'row', padding: 5}}>
          <View
            style={{
              borderWidth: 0.5,
              borderColor: AppColors.gray,
              marginRight: 5,
            }}>
            <Image
              style={{width: 100, height: 100}}
              source={{uri: item.UrlImage}}
            />
          </View>
          <View style={{flex: 1}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Mã BOM :</Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={AppStyles.Textdefault}>{item.BOMNumber}</Text>
              </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Mã SP(NB) :</Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={AppStyles.Textdefault}>{item.ItemID}</Text>
              </View>
            </View>
            {/* <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Mã SP(KH) :</Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={AppStyles.Textdefault}>
                  {item.ItemIDByProvider}
                </Text>
              </View>
            </View> */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Giá thành KH :</Text>
              </View>
              <View style={{flex: 3, alignItems: 'flex-end'}}>
                <Text style={AppStyles.Labeldefault}>
                  {this.addPeriod(item.Amount)}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <Divider />
        {/* thông tin chi tiết */}
        {this.CustomViewItem(item)}
        {/* Quy trình công nghệ */}
        {/* {this.CustomViewBOMWorkOrderRoutings()} */}
        {/* NVL-BTP */}
        {/* {this.CustomViewBOMMaterialNorms()} */}
        {/* bao bì */}
        {/* {this.CustomViewProductCostNorms()} */}
        {/* nhân công trực tiếp */}
        {/* {this.CustomViewLaborCosts()} */}
        {/* chi phí máy */}
        {/* {this.CustomViewMachineCosts()} */}
        {/* chi phí CCDC */}
        {/* {this.CustomViewToolsCosts()} */}
        {/* Gia công Ngoài */}
        {/* {this.CustomViewProductCostNormsCosts()} */}
        {/* Sản xuất chung */}
        {/* {this.CustomViewProductCostNormsCostsSX()} */}
        {/* Chi phí tài chính */}
        {/* {this.CustomViewProductCostNormsCostsTC()} */}
        {/* Tổng hợp chi phí */}
        {/* {this.CustomViewGetBOMCostNormHistories()} */}
        {/* Quy cách đóng gói - giao hàng */}
        {/* {this.CustomViewItemById()} */}
      </ScrollView>
    );
  };

  // thông tin chi tiết
  //#region setViewDetailItem
  setViewDetailItem = () => {
    this.setViewOpen('ViewDetailItem');
  };
  //#endregion
  //#region tab chi tiết
  CustomViewItem = item => {
    const rotateStart = this.state.transIcon.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}>
          {/* onPress={() => this.setViewDetailItem()} */}
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Thông tin chi tiết</Text>
          </View>
          <Animated.View
            style={{
              transform: [{rotate: rotateStart}, {perspective: 4000}],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewDetailItem === true ? (
          <TouchableOpacity style={{padding: 5}}>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Nhân viên bóc tách :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.EmployeeName}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Ngày bóc tách :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  {FuncCommon.ConDate(item.CreatedDate, 0)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>ĐVT :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.UnitName}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>TT VT điện :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {color: this.handleColorStatus(item.ElectricityStatus)},
                  ]}>
                  {this.handleStatus(item.ElectricityStatus)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>TT VT cơ khí :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {color: this.handleColorStatus(item.MechanicalStatus)},
                  ]}>
                  {this.handleStatus(item.MechanicalStatus)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>TT VT đồng :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {color: this.handleColorStatus(item.CopperStatus)},
                  ]}>
                  {this.handleStatus(item.CopperStatus)}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>TT VT phụ :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {color: this.handleColorStatus(item.OtherStatus)},
                  ]}>
                  {this.handleStatus(item.OtherStatus)}
                </Text>
              </View>
            </View>

            {/* <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Đặc điểm :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>{item.Description}</Text>
              </View>
            </View> */}
          </TouchableOpacity>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  // Tab Quy trình công nghệ
  //#region setEventBOMWorkOrderRoutings (quy trình công nghệ)
  setEventBOMWorkOrderRoutings = () => {
    if (this.state.ViewBOMWorkOrderRoutings === false) {
      var obj = {
        BomNumber: this.state.Data.BOMNumber,
      };
      API_BomCategories.GetBOMWorkOrderRoutings(obj)
        .then(res => {
          var data = JSON.parse(res.data.data);
          var list = [];
          var ProductionTime = 0;
          var SetupTime = 0;
          for (let i = 0; i < data.length; i++) {
            if (data[i].Type === 'P') {
              list.push(data[i]);
              ProductionTime = ProductionTime + data[i].ProductionTime;
              SetupTime = SetupTime + data[i].SetupTime;
            }
          }
          this.ProductionTime_BOMWorkOrderRoutings = this.customFloat(
            ProductionTime,
          );
          this.SetupTime_BOMWorkOrderRoutings = this.customFloat(SetupTime);
          this.setState({
            DataBOMWorkOrderRoutings: list,
          });
          // Định mức phôi
          API_BomCategories.GetBOMWorkpieces(obj)
            .then(res => {
              var data = JSON.parse(res.data.data);
              var list = [];
              for (let i = 0; i < data.length; i++) {
                if (
                  data[i].PartItemId === '' ||
                  data[i].PartItemId === null ||
                  data[i].PartItemId === undefined
                ) {
                  list.push(data[i]);
                }
              }
              this.setState({
                DataBOMWorkpieces: list,
              });
            })
            .catch(error => {
              console.log(error);
            });
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewBOMWorkOrderRoutings');
  };

  //#endregion
  openAttachments() {
    var obj = {
      ModuleId: '52',
      RecordGuid: this.state.Data.RowGuid,
    };
    Actions.attachmentComponent(obj);
  }
  //#region tab Quy trình công nghệ
  CustomViewBOMWorkOrderRoutings = () => {
    const rotateStart_ViewBOMWorkOrderRoutings = this.state.transIcon_ViewBOMWorkOrderRoutings.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventBOMWorkOrderRoutings()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Quy trình công nghệ (TP)</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewBOMWorkOrderRoutings},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewBOMWorkOrderRoutings === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              1. Quy trình công nghệ (Công đoạn):
            </Text>
            <Divider />
            {/* Table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Tên quy trình</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Thời gian SX(Phút)
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Thời gian Setup (Phút)
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Mã- tên máy</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataBOMWorkOrderRoutings.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataBOMWorkOrderRoutings.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.LocationId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.ProductionTime}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.SetupTime}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.MachineId}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(
                            this.ProductionTime_BOMWorkOrderRoutings,
                          )}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(
                            this.SetupTime_BOMWorkOrderRoutings,
                          )}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>

            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              2. Định mức phôi - NVL sử dụng:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <View style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Công đoạn</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Mã- Tên phôi</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Tlg riêng</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Cao</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Rộng</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Trong</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Ngoài</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Dài</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Tlg phôi (Kg)</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Đơn giá</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Thành tiền</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>ĐVT chính</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>ĐVT ĐN</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Hệ số</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>% Sử dụng</Text>
                  </View>
                </View>
                {this.state.DataBOMWorkpieces.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataBOMWorkpieces.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.LocationId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.MaterialItemId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.ApecificGravity}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.High}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.Wide}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.SizeOd}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.SizeId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.Length}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.Weight}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.UnitPrice)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Weight * para.UnitPrice)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.UnitName_NVL}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.UnitId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.ConvertRate}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {para.PercentUsed}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
            <Divider />
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Tab NVL-BTP
  //#region setEventBOMMaterialNorms (NVL- BTP)
  setEventBOMMaterialNorms = () => {
    if (this.state.ViewBOMMaterialNorms === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber};
      API_BomCategories.GetBOMMaterialNorms(obj)
        .then(res => {
          var data = JSON.parse(res.data.data);
          var listVT = [];
          var listBTP = [];
          var Amount = 0; // tổng thành tiền
          var QuantityLoss = 0; //tổng số lượng hao hụt
          var Quantity = 0; // tổng số lượng. set
          var Quantity_BTP = 0; // tổng số lượng. set (BTP)
          var PercentLoss = 0; // tổng số  % hao hụt
          for (let i = 0; i < data.length; i++) {
            if (data[i].Type === 'V') {
              listVT.push(data[i]);
              Amount = Amount + data[i].Amount;
              QuantityLoss = QuantityLoss + data[i].QuantityLoss;
              Quantity = Quantity + data[i].Quantity;
              PercentLoss = PercentLoss + data[i].PercentLoss;
            } else if (data[i].Type === 'B') {
              listBTP.push(data[i]);
              Quantity_BTP = Quantity_BTP + data[i].Quantity;
            }
          }
          this.QuantityLoss_BOMMaterialNorms = this.customFloat(QuantityLoss);
          this.Amount_BOMMaterialNorms = this.customFloat(Amount);
          this.Quantity_BOMMaterialNorms = this.customFloat(Quantity);
          this.Quantity_BOMMaterialNorms_BTP = this.customFloat(Quantity_BTP); //(BTP)
          this.PercentLoss_BOMMaterialNorms = this.customFloat(PercentLoss);
          this.setState({
            DataBOMMaterialNorms_VT: listVT,
            DataBOMMaterialNorms_BTP: listBTP,
          });
          //2.3 NVL-phôi sử dụng sản xuất bán thành phẩm
          API_BomCategories.GetBOMWorkpieces(obj)
            .then(res => {
              var data = JSON.parse(res.data.data);
              var list_WBTP = [];
              var Amount_BTP = 0; // tổng thành tiền
              for (let i = 0; i < data.length; i++) {
                if (
                  data[i].PartItemId !== '' &&
                  data[i].PartItemId !== null &&
                  data[i].PartItemId !== undefined
                ) {
                  list_WBTP.push(data[i]);
                  Amount_BTP = Amount_BTP + data[i].Amount;
                }
              }
              this.Amount_BOMWorkpieces_BTP = this.customFloat(Amount_BTP);
              this.setState({
                DataBOMWorkpieces_BTP: list_WBTP,
              });
            })
            .catch(error => {
              console.log(error);
            });
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewBOMMaterialNorms');
  };
  //#endregion
  //#region tab NVL-BTP
  CustomViewBOMMaterialNorms = () => {
    const rotateStart_ViewBOMMaterialNorms = this.state.transIcon_ViewBOMMaterialNorms.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventBOMMaterialNorms()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>NVL - BTP</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewBOMMaterialNorms},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewBOMMaterialNorms === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              1. NVL dùng chung để sản xuất bán thành phẩm:
            </Text>
            <Divider />
            {/* table  */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <View style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Mã vật tư</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Tên vật tư</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Đặc điểm</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Kích thước</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 50}]}>
                    <Text style={AppStyles.Labeldefault}>ĐVT</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>SLg set</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>% Hao hụt</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>SL hao hụt</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Đơn giá</Text>
                  </View>
                  <View style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Thành tiền</Text>
                  </View>
                </View>
                {this.state.DataBOMMaterialNorms_VT.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataBOMMaterialNorms_VT.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.MaterialItemId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.ItemName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.Description}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.Size}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 50}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.UnitName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Quantity)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.PercentLoss)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.QuantityLoss)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.UnitPrice)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Amount)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 50}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Quantity_BOMMaterialNorms)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.PercentLoss_BOMMaterialNorms)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.QuantityLoss_BOMMaterialNorms)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Amount_BOMMaterialNorms)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>

            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              2. Bán thành phẩm dùng để sản xuất thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Mã BTP</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Ảnh</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Tên BTP</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Đặc điểm</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Kích thước</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Quy trình công nghệ(BTP)
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>ĐVT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Slg/set</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataBOMMaterialNorms_BTP.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataBOMMaterialNorms_BTP.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.PartItemId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            AppStyles.table_td,
                            {width: 100, alignItems: 'center'},
                          ]}>
                          <Image
                            style={{width: 60, height: 60}}
                            source={{uri: para.Image}}
                          />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.ItemName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.Description}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.Size}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 200}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.LocationList}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.UnitName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Quantity)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 200}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Quantity_BOMMaterialNorms_BTP)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>

            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              3. NVL - Phôi sử dụng sản xuất bán thành phẩm của thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 120}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - Tên BTP</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Công đoạn</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 120}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - Tên phôi</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Tlg riêng</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Cao</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Rộng</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Trong</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Ngoài</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Dài</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Tlg phôi(Kg)</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Đơn giá</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 120}]}>
                    <Text style={AppStyles.Labeldefault}>Thành tiền</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>ĐVT chính</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>ĐVT ĐN</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Hệ số</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>% Sử dụng</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataBOMWorkpieces_BTP.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataBOMWorkpieces_BTP.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 120}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.PartItemId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.LocationId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 120}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.MaterialItemId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.ApecificGravity)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.High)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Wide)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.SizeOd)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.SizeId)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Length)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Weight)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 120}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.UnitPrice)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(
                              parseFloat(para.Weight) *
                                parseFloat(para.UnitPrice),
                            )}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.UnitName_BTP}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.UnitId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.ConvertRate)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.PercentUsed)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 120}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 120}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 120}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Amount_BOMWorkpieces_BTP)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Bao bì
  //#region setEventProductCostNorms (Bao bì)
  setEventProductCostNorms = () => {
    if (this.state.ViewProductCostNorms === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber};
      API_BomCategories.GetProductCostNorms(obj)
        .then(res => {
          var data = JSON.parse(res.data.data);
          var list = [];
          var Amount = 0;
          var Quantity = 0; // tổng số lượng. set
          for (let i = 0; i < data.length; i++) {
            if (data[i].Type === 'B') {
              list.push(data[i]);
              Amount = Amount + data[i].Amount;
              Quantity = Quantity + data[i].Quantity;
            }
          }
          this.Amount_ProductCostNorms = this.customFloat(Amount);
          this.Quantity_ProductCostNorms = this.customFloat(Quantity);
          this.setState({DataProductCostNorms: list});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewProductCostNorms');
  };
  //#endregion
  //#region tab Bao bì
  CustomViewProductCostNorms = () => {
    const rotateStart_ViewProductCostNorms = this.state.transIcon_ViewProductCostNorms.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventProductCostNorms()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Bao bì</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewProductCostNorms},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewProductCostNorms === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              Chi phí bao bì dùng cho thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - tên chi phí</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>ĐVT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>SL</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Đơn giá</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Thành tiền</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataProductCostNorms.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataProductCostNorms.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.CostId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.UnitName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Quantity)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.CostPrice)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Amount)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Quantity_ProductCostNorms)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Amount_ProductCostNorms)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Nhân công trực tiếp
  //#region setEventLaborCosts (Nhân công trực tiếp)
  setEventLaborCosts = () => {
    if (this.state.ViewLaborCosts === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber};
      API_BomCategories.GetLaborCosts(obj)
        .then(res => {
          var data = JSON.parse(res.data.data);
          var list = [];
          var ProductionTime = 0;
          var CostPrice = 0;
          var InsuranceCostPrice = 0;
          var Amount = 0;
          for (let i = 0; i < data.length; i++) {
            var amount =
              (data[i].CostPrice === null ? 0 : parseFloat(data[i].CostPrice)) +
              (data[i].InsuranceCostPrice === null
                ? 0
                : parseFloat(data[i].InsuranceCostPrice));
            list.push({
              LocationID: data[i].LocationID,
              ProductionTime:
                data[i].ProductionTime === null ? 0 : data[i].ProductionTime,
              CostPrice: parseInt(
                data[i].CostPrice === null ? 0 : data[i].CostPrice,
              ),
              InsuranceCostPrice: parseInt(
                data[i].InsuranceCostPrice === null
                  ? 0
                  : data[i].InsuranceCostPrice,
              ),
              Amount: parseInt(amount),
            });
            ProductionTime = ProductionTime + data[i].ProductionTime;
            CostPrice = CostPrice + data[i].CostPrice;
            InsuranceCostPrice =
              InsuranceCostPrice +
              (data[i].InsuranceCostPrice === null
                ? 0
                : data[i].InsuranceCostPrice);
            Amount = Amount + amount;
          }
          this.ProductionTime_LaborCosts = ProductionTime;
          this.CostPrice_LaborCosts = CostPrice;
          this.InsuranceCostPrice_LaborCosts = InsuranceCostPrice;
          this.Amount_LaborCosts = Amount;
          this.setState({DataLaborCosts: list});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewLaborCosts');
  };
  //#endregion
  //#region tab nhân công trực tiếp
  CustomViewLaborCosts = () => {
    const rotateStart_ViewLaborCosts = this.state.transIcon_ViewLaborCosts.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventLaborCosts()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Nhân công trực tiếp</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewLaborCosts},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewLaborCosts === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              Chi phí nhân công trực tiếp dùng cho thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - Công đoạn</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Tổng T.gian SX (phút)
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>CP Lương (phút)</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>
                      CP Bảo hiểm Cty đóng (phút)
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Tổng tiền</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataLaborCosts.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataLaborCosts.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 200}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.LocationID}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.ProductionTime)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.CostPrice)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.InsuranceCostPrice)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Amount)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 200}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.ProductionTime_LaborCosts)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.CostPrice_LaborCosts)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.InsuranceCostPrice_LaborCosts)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Amount_LaborCosts)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Chi phí máy
  //#region setEventMachineCosts (Chi phí máy)
  setEventMachineCosts = () => {
    if (this.state.ViewMachineCosts === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber};
      API_BomCategories.GetMachineCosts(obj)
        .then(res => {
          var rs = JSON.parse(res.data.data);
          var list = [];
          var ProductionTime = 0;
          var CostPrice = 0;
          for (let i = 0; i < rs.length; i++) {
            list.push({
              LocationID: rs[i].LocationID,
              ProductionTime: rs[i].ProductionTime,
              CostPrice: rs[i].CostPrice,
            });
            ProductionTime = ProductionTime + rs[i].ProductionTime;
            CostPrice = CostPrice + rs[i].CostPrice;
          }
          this.ProductionTime_MachineCosts = ProductionTime;
          this.CostPrice_MachineCosts = CostPrice;
          this.setState({DataMachineCosts: list});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewMachineCosts');
  };
  //#endregion
  //#region tab Chi phí máy
  CustomViewMachineCosts = () => {
    const rotateStart_ViewMachineCosts = this.state.transIcon_ViewMachineCosts.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventMachineCosts()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Chi phí máy</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewMachineCosts},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewMachineCosts === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              Chi phí máy trực tiếp dùng cho thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - Tên máy</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Tổng T.gian SX (phút)
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Giá trị định mức (phút)
                    </Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataMachineCosts.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataMachineCosts.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 200}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.LocationID}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.ProductionTime)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.CostPrice)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 200}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.ProductionTime_MachineCosts)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.CostPrice_MachineCosts)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //chi phí CCDC
  //#region setEventToolsCosts (chi phí CCDC)
  setEventToolsCosts = () => {
    if (this.state.ViewToolsCosts === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber};
      API_BomCategories.GetToolsCosts(obj)
        .then(res => {
          var rs = JSON.parse(res.data.data);
          var _items = [];
          var ProductionTime = 0;
          var CostPrice = 0;
          var Amount = 0;
          for (var i = 0; i < rs.length; i++) {
            var amount =
              parseFloat(
                rs[i].ProductionTime === null ? 0 : rs[i].ProductionTime,
              ) *
              parseFloat(
                rs[i].CostPrice === null ? 0 : parseFloat(rs[i].CostPrice),
              );
            _items.push({
              LocationID: rs[i].LocationID,
              ProductionTime: rs[i].ProductionTime,
              CostPrice: rs[i].CostPrice,
              Amount: amount,
            });
            ProductionTime = ProductionTime + rs[i].ProductionTime;
            CostPrice = CostPrice + rs[i].CostPrice;
            Amount = Amount + amount;
          }
          this.ProductionTime_ToolsCosts = ProductionTime;
          this.CostPrice_ToolsCosts = CostPrice;
          this.Amount_ToolsCosts = Amount;
          this.setState({DataToolsCosts: _items});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewToolsCosts');
  };
  //#endregion
  //#region tab chi phí CCDC
  CustomViewToolsCosts = () => {
    const rotateStart_ViewToolsCosts = this.state.transIcon_ViewToolsCosts.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventToolsCosts()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>CCDC</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewToolsCosts},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewToolsCosts === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              Chi phí CCDC trực tiếp dùng cho thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - Tên CCDC</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Tổng T.gian SX (phút)
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Giá trị định mức (phút)
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Thành tiền</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataToolsCosts.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataToolsCosts.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 200}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.LocationID}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.ProductionTime)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.CostPrice)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Amount)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 200}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.ProductionTime_ToolsCosts)}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Amount_ToolsCosts)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Gia công ngoài
  //#region setEventProductCostNormsCosts (Gia công ngoài)
  setEventProductCostNormsCosts = () => {
    if (this.state.ViewProductCostNormsCosts === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber, Type: 'G'};
      API_BomCategories.GetProductCostNormsCosts(obj)
        .then(res => {
          var rs = JSON.parse(res.data.data);
          var _items = [];
          var Amount = 0;
          for (var i = 0; i < rs.length; i++) {
            _items.push({
              CostId: rs[i].CostId,
              SupplierName: rs[i].SupplierName,
              Amount: rs[i].Amount,
            });
            Amount = Amount + rs[i].Amount;
          }
          this.Amount_ProductCostNormsCosts = Amount;
          this.setState({DataProductCostNormsCosts: _items});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewProductCostNormsCosts');
  };
  //#endregion
  //#region tab Gia công ngoài
  CustomViewProductCostNormsCosts = () => {
    const rotateStart_ViewProductCostNormsCosts = this.state.transIcon_ViewProductCostNormsCosts.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventProductCostNormsCosts()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Gia công ngoài</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewProductCostNormsCosts},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewProductCostNormsCosts === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              Chi phí gia công ngoài dùng cho thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>
                      Mã - Nội dung gia công ngoài
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Nhà cung cấp</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Giá trị định mức</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataProductCostNormsCosts.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataProductCostNormsCosts.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 200}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.CostId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.SupplierName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Amount)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 200}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(this.Amount_ProductCostNormsCosts)}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Sản xuất chung
  //#region setEventProductCostNormsCostsSX (Sản xuất chung)
  setEventProductCostNormsCostsSX = () => {
    if (this.state.ViewProductCostNormsCostsSX === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber, Type: 'C'};
      API_BomCategories.GetProductCostNormsCosts(obj)
        .then(res => {
          var rs = JSON.parse(res.data.data);
          var _items = [];
          var Amount = 0;
          for (var i = 0; i < rs.length; i++) {
            _items.push({
              CostId: rs[i].CostId,
              Amount: rs[i].Amount,
            });
            Amount = Amount + rs[i].Amount;
          }
          this.Amount_ProductCostNormsCostsSX = Amount;
          this.setState({DataProductCostNormsCostsSX: _items});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewProductCostNormsCostsSX');
  };
  //#endregion
  //#region tab sản xuất chung
  CustomViewProductCostNormsCostsSX = () => {
    const rotateStart_ViewProductCostNormsCostsSX = this.state.transIcon_ViewProductCostNormsCostsSX.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventProductCostNormsCostsSX()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Sản xuất chung</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewProductCostNormsCostsSX},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewProductCostNormsCostsSX === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              Chi phí sản xuất chung của thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - Tên chi phí</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Giá trị định mức</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataProductCostNormsCostsSX.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataProductCostNormsCostsSX.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 200}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.CostId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Amount)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 200}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(
                            this.Amount_ProductCostNormsCostsSX,
                          )}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Chi phí tài chính
  //#region setEventProductCostNormsCostsTC (Chi phí tài chính)
  setEventProductCostNormsCostsTC = () => {
    if (this.state.ViewProductCostNormsCostsTC === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber, Type: 'T'};
      API_BomCategories.GetProductCostNormsCosts(obj)
        .then(res => {
          var rs = JSON.parse(res.data.data);
          var _items = [];
          var Amount = 0;
          for (var i = 0; i < rs.length; i++) {
            _items.push({
              CostId: rs[i].CostId,
              Quantity: rs[i].Quantity,
              Amount: rs[i].Amount,
            });
            Amount = Amount + rs[i].Amount;
          }
          this.Amount_ProductCostNormsCostsTC = Amount;
          this.setState({DataProductCostNormsCostsTC: _items});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewProductCostNormsCostsTC');
  };
  //#endregion
  //#region tab chi phí tài chính
  CustomViewProductCostNormsCostsTC = () => {
    const rotateStart_ViewProductCostNormsCostsTC = this.state.transIcon_ViewProductCostNormsCostsTC.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventProductCostNormsCostsTC()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Chi phí tài chính</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewProductCostNormsCostsTC},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewProductCostNormsCostsTC === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              Chi phí tài chính của thành phẩm:
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - Tên chi phí</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>% Lãi</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Giá trị định mức</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataProductCostNormsCostsTC.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataProductCostNormsCostsTC.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 200}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.CostId}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Quantity)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Amount)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 200}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(
                            this.Amount_ProductCostNormsCostsTC,
                          )}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Tổng hợp chi phí
  //#region setEventGetBOMCostNormHistories (Tổng hợp chi phí)
  setEventGetBOMCostNormHistories = () => {
    if (this.state.ViewGetBOMCostNormHistories === false) {
      var obj = {BomNumber: this.state.Data.BOMNumber};
      API_BomCategories.GetBOMCostNormHistories(obj)
        .then(res => {
          var rs = JSON.parse(res.data.data);
          var _items = [];
          var nhanCong = rs.find(x => x.CostType === 'N');
          var may = rs.find(x => x.CostType === 'M');
          var baobi = rs.find(x => x.CostType === 'B');
          var gcn = rs.find(x => x.CostType === 'G');
          var ccdc = rs.find(x => x.CostType === 'D');
          var sxchung = rs.find(x => x.CostType === 'C');
          var taichinh = rs.find(x => x.CostType === 'T');
          var nvl = rs.find(x => x.CostType === 'V');
          var Amount =
            (nhanCong === undefined ? 0 : nhanCong.Amount) +
            (may === undefined ? 0 : may.Amount) +
            (baobi === undefined ? 0 : baobi.Amount) +
            (gcn === undefined ? 0 : gcn.Amount) +
            (ccdc === undefined ? 0 : ccdc.Amount) +
            (sxchung === undefined ? 0 : sxchung.Amount) +
            (taichinh === undefined ? 0 : taichinh.Amount) +
            (nvl === undefined ? 0 : nvl.Amount);
          _items.push(
            {
              ID: 'Chi phí nguyên vật liệu',
              Amount: nvl === undefined ? 0 : nvl.Amount,
            },
            {
              ID: 'Chi phí nhân công trực tiếp',
              Amount: nhanCong === undefined ? 0 : nhanCong.Amount,
            },
            {
              ID: 'Chi phí máy',
              Amount: may === undefined ? 0 : may.Amount,
            },
            {
              ID: 'Chi phí bao bì',
              Amount: baobi === undefined ? 0 : baobi.Amount,
            },
            {
              ID: 'Chi phí CCDC',
              Amount: ccdc === undefined ? 0 : ccdc.Amount,
            },
            {
              ID: 'Gia công ngoài',
              Amount: gcn === undefined ? 0 : gcn.Amount,
            },
            {
              ID: 'Sản xuất chung',
              Amount: sxchung === undefined ? 0 : sxchung.Amount,
            },
            {
              ID: 'Chi phí tài chính',
              Amount: taichinh === undefined ? 0 : taichinh.Amount,
            },
          );
          this.Amount_GetBOMCostNormHistories = Amount;
          this.setState({DataGetBOMCostNormHistories: _items});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewGetBOMCostNormHistories');
  };
  //#endregion
  //#region tab Tổng hợp chi phí
  CustomViewGetBOMCostNormHistories = () => {
    const rotateStart_ViewGetBOMCostNormHistories = this.state.transIcon_ViewGetBOMCostNormHistories.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventGetBOMCostNormHistories()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>Tổng hợp chi phí</Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewGetBOMCostNormHistories},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewGetBOMCostNormHistories === true ? (
          <View style={{flex: 1}}>
            <Text style={[AppStyles.Labeldefault, {padding: 10}]}>
              Tổng hợp chi phí:{' '}
            </Text>
            <Divider />
            {/* table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 200}]}>
                    <Text style={AppStyles.Labeldefault}>Mã - Tên chi phí</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Tổng tiền</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataGetBOMCostNormHistories.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataGetBOMCostNormHistories.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 200}]}>
                          <Text style={[AppStyles.Textdefault]}>{para.ID}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Amount)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 200}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.customFloat(
                            this.Amount_GetBOMCostNormHistories,
                          )}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Quy cách đóng gói - giao hàng
  //#region setEventItemById (Quy cách đóng gói - giao hàng)
  setEventItemById = () => {
    if (this.state.ViewItemById === false) {
      var obj = {Code: this.state.Data.ItemID};
      API_BomCategories.GetItemById(obj)
        .then(res => {
          var rs = JSON.parse(res.data.data);
          this.setState({DataItemById: rs});
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewItemById');
  };
  //#endregion
  //#region tab Quy cách đóng gói - giao hàng
  CustomViewItemById = () => {
    const rotateStart_ViewItemById = this.state.transIcon_ViewItemById.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    const model = this.state.DataItemById;
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventItemById()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>
              Quy cách đóng gói - Giao hàng
            </Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewItemById},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewItemById === true ? (
          model !== null ? (
            <View style={{flex: 1, flexDirection: 'row', padding: 10}}>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <View style={{flexDirection: 'row', padding: 3}}>
                  <View style={{flex: 2}}>
                    <Text style={AppStyles.Labeldefault}>Phương pháp</Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Textdefault}>
                      : {this.CustomSolution(model.Solution)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 3}}>
                  <View style={{flex: 2}}>
                    <Text style={AppStyles.Labeldefault}>Vận chuyển</Text>
                  </View>
                  <View style={{flex: 3}}>
                    <Text style={AppStyles.Textdefault}>
                      : {this.CustomTransport(model.Transport)}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 3}}>
                  <View style={{flex: 2}}>
                    <Text style={AppStyles.Labeldefault}>Thể tích(m3)</Text>
                  </View>
                  <View style={{flex: 3, flexDirection: 'row'}}>
                    <Text style={AppStyles.Textdefault}>:</Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {model.Volumn}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 3}}>
                  <View style={{flex: 2}}>
                    <Text style={AppStyles.Labeldefault}>Cân nặng(kg)</Text>
                  </View>
                  <View style={{flex: 3, flexDirection: 'row'}}>
                    <Text style={AppStyles.Textdefault}>:</Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {model.Weight}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flexDirection: 'row', padding: 3}}>
                  <View style={{flex: 2}}>
                    <Text style={AppStyles.Labeldefault}>Slg/Kiện</Text>
                  </View>
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <Text style={AppStyles.Textdefault}>:</Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {model.Package}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 3}}>
                  <View style={{flex: 2}}>
                    <Text style={AppStyles.Labeldefault}>Rộng</Text>
                  </View>
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <Text style={AppStyles.Textdefault}>:</Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Text style={[AppStyles.Textdefault]}>{model.Wide}</Text>
                    </View>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 3}}>
                  <View style={{flex: 2}}>
                    <Text style={AppStyles.Labeldefault}>Dài</Text>
                  </View>
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <Text style={AppStyles.Textdefault}>:</Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Text style={[AppStyles.Textdefault]}>
                        {model.Lenght}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 3}}>
                  <View style={{flex: 2}}>
                    <Text style={AppStyles.Labeldefault}>Cao</Text>
                  </View>
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <Text style={AppStyles.Textdefault}>:</Text>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Text style={[AppStyles.Textdefault]}>{model.High}</Text>
                    </View>
                  </View>
                </View>
              </View>

              <Divider />
            </View>
          ) : (
            <TouchableOpacity style={[AppStyles.table_foot]}>
              <Text style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                Không có dữ liệu
              </Text>
            </TouchableOpacity>
          )
        ) : null}
      </ScrollView>
    );
  };
  //#endregion
  //#region CustomSolution
  CustomSolution = val => {
    if (val === 'PVC') {
      return 'Cuốn màng PVC';
    } else if (val === 'GOXK') {
      return 'Thùng gỗ xuất khẩu';
    } else if (val === 'Danpla') {
      return 'Thùng danpla';
    } else if (val === 'GIAYXK') {
      return 'Thùng giấy xuất khẩu';
    } else if (val === 'GIAYTHUONG') {
      return 'Thùng giấy thường';
    } else if (val === 'XEDAY') {
      return 'Xe đẩy';
    } else if (val === 'KHAC') {
      return 'Khác';
    } else {
      return '';
    }
  };
  //#endregion
  //#region CustomTransport
  CustomTransport = val => {
    if (val === 'XETAI') {
      return 'Xe tải';
    } else if (val === 'MAYBAY') {
      return 'Máy bay';
    } else if (val === 'ContainerND') {
      return 'Container nội địa';
    } else if (val === 'SHIPContainer') {
      return 'Ship container';
    } else {
      return '';
    }
  };
  //#endregion
  handleStatus = data => {
    if (data === 1) {
      return 'Chờ duyệt';
    } else if (data === 2) {
      return 'Đã duyệt';
    } else if (data === 3) {
      return 'Không duyệt';
    } else if (data === 4) {
      return 'Chờ xác nhận sửa';
    } else if (data === 5) {
      return 'Đã xác nhận sửa';
    }
    return '';
  };
  handleColorStatus = data => {
    if (data === 1) {
      return AppColors.PendingColor;
    } else if (data === 2) {
      return AppColors.ColorLink;
    } else if (data === 3) {
      return AppColors.UntreatedColor;
    } else if (data === 4) {
      return AppColors.PendingColor;
    } else if (data === 5) {
      return AppColors.AcceptColor;
    }
    return 'black';
  };
  //Các hàm dùng chung
  //#region đóng mở các tab
  setViewOpen = val => {
    this.setState({
      ViewDetailItem: false,
      ViewBOMWorkOrderRoutings: false,
      ViewBOMMaterialNorms: false,
      ViewProductCostNorms: false,
      ViewLaborCosts: false,
      ViewMachineCosts: false,
      ViewToolsCosts: false,
      ViewProductCostNormsCosts: false,
      ViewProductCostNormsCostsSX: false,
      ViewProductCostNormsCostsTC: false,
      ViewGetBOMCostNormHistories: false,
      ViewItemById: false,
    });
    if (val === 'ViewDetailItem') {
      this.setState({ViewDetailItem: !this.state.ViewDetailItem});
      if (this.state.ViewDetailItem === false) {
        Animated.sequence([this.Animated_on_DetailItem]).start();
        // Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_DetailItem]).start();
      }
    } else if (val === 'ViewBOMWorkOrderRoutings') {
      this.setState({
        ViewBOMWorkOrderRoutings: !this.state.ViewBOMWorkOrderRoutings,
      });
      if (this.state.ViewBOMWorkOrderRoutings === false) {
        Animated.sequence([this.Animated_on_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        // Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
      }
    } else if (val === 'ViewBOMMaterialNorms') {
      this.setState({ViewBOMMaterialNorms: !this.state.ViewBOMMaterialNorms});
      if (this.state.ViewBOMMaterialNorms === false) {
        Animated.sequence([this.Animated_on_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        // Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
      }
    } else if (val === 'ViewProductCostNorms') {
      this.setState({ViewProductCostNorms: !this.state.ViewProductCostNorms});
      if (this.state.ViewProductCostNorms === false) {
        Animated.sequence([this.Animated_on_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        // Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
      }
    } else if (val === 'ViewLaborCosts') {
      this.setState({ViewLaborCosts: !this.state.ViewLaborCosts});
      if (this.state.ViewLaborCosts === false) {
        Animated.sequence([this.Animated_on_LaborCosts]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        // Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_LaborCosts]).start();
      }
    } else if (val === 'ViewMachineCosts') {
      this.setState({ViewMachineCosts: !this.state.ViewMachineCosts});
      if (this.state.ViewMachineCosts === false) {
        Animated.sequence([this.Animated_on_MachineCosts]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        // Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_MachineCosts]).start();
      }
    } else if (val === 'ViewToolsCosts') {
      this.setState({ViewToolsCosts: !this.state.ViewToolsCosts});
      if (this.state.ViewToolsCosts === false) {
        Animated.sequence([this.Animated_on_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        // Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
      }
    } else if (val === 'ViewProductCostNormsCosts') {
      this.setState({
        ViewProductCostNormsCosts: !this.state.ViewProductCostNormsCosts,
      });
      if (this.state.ViewProductCostNormsCosts === false) {
        Animated.sequence([this.Animated_on_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        // Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
      }
    } else if (val === 'ViewProductCostNormsCostsSX') {
      this.setState({
        ViewProductCostNormsCostsSX: !this.state.ViewProductCostNormsCostsSX,
      });
      if (this.state.ViewProductCostNormsCostsSX === false) {
        Animated.sequence([this.Animated_on_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        // Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
      }
    } else if (val === 'ViewProductCostNormsCostsTC') {
      this.setState({
        ViewProductCostNormsCostsTC: !this.state.ViewProductCostNormsCostsTC,
      });
      if (this.state.ViewProductCostNormsCostsTC === false) {
        Animated.sequence([this.Animated_on_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        // Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
      }
    } else if (val === 'ViewGetBOMCostNormHistories') {
      this.setState({
        ViewGetBOMCostNormHistories: !this.state.ViewGetBOMCostNormHistories,
      });
      if (this.state.ViewGetBOMCostNormHistories === false) {
        Animated.sequence([this.Animated_on_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        // Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
      }
    } else if (val === 'ViewItemById') {
      this.setState({ViewItemById: !this.state.ViewItemById});
      if (this.state.ViewItemById === false) {
        Animated.sequence([this.Animated_on_ItemById]).start();
        Animated.sequence([this.Animated_off_DetailItem]).start();
        Animated.sequence([this.Animated_off_BOMWorkOrderRoutings]).start();
        Animated.sequence([this.Animated_off_BOMMaterialNorms]).start();
        Animated.sequence([this.Animated_off_ProductCostNorms]).start();
        Animated.sequence([this.Animated_off_LaborCosts]).start();
        Animated.sequence([this.Animated_off_MachineCosts]).start();
        Animated.sequence([this.Animated_off_ToolsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCosts]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsSX]).start();
        Animated.sequence([this.Animated_off_ProductCostNormsCostsTC]).start();
        Animated.sequence([this.Animated_off_BOMCostNormHistories]).start();
        // Animated.sequence([this.Animated_off_ItemById]).start();
      } else {
        Animated.sequence([this.Animated_off_ItemById]).start();
      }
    }
  };
  //#endregion
  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Detail', ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region  CusPriority
  CusPriority = data => {
    if (data == 'CAO') {
      return 'Cao';
    } else if (data == 'THAP') {
      return 'Thấp';
    } else if (data == 'TB') {
      return 'Trung bình';
    }
    return '';
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null) {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        return valview + ',' + string[1].substring(0, 2);
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(BomCategories_Open_Component);
