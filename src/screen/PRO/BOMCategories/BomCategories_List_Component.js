import React, {Component} from 'react';
import {
  Alert,
  FlatList,
  Image,
  RefreshControl,
  Text,
  View,
  Keyboard,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {ErrorHandler} from '@error';
import {Divider, Icon, ListItem, SearchBar} from 'react-native-elements';
import {API_BomCategories} from '../../../network';
import TabBar from '../../component/TabBar';
import {FuncCommon} from '../../../utils';
import LoadingComponent from '../../component/LoadingComponent';
import {AppStyles, AppColors} from '@theme';
import FormSearch from '../../component/FormSearch';
import MenuSearchDate from '../../component/MenuSearchDate';
import TabBarBottomCustom from '../../component/TabBarBottomCustom';
import DatePicker from 'react-native-date-picker';
const SCREEN_WIDTH = Dimensions.get('window').width;

class BomCategories_List_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      setEventEndDate: false,
      setEventStartDate: false,
      ValueSearchDate: '1', // 30 ngày trước
      loadingsearch: true,
      setJtable: 1,
      loading: true,
      ListAll: [], //biến chứa danh sách
      ListAllV2: [], //biến chứa danh sách
      EvenFromSearch: false, // on/off form search
      //#region dữ liệu để lấy ra list
      JTableBom: {
        EndDate: new Date(),
        StartDate: new Date(),
        length: 50,
        search: {
          value: '',
        },
        CurrentPage: 1,
        EmployeeId: '',
      },
      //#endregion
    };
    //#region tabbarbottom
    this.listtabbarBotom = [
      {
        Title: 'Chờ bóc tách',
        Icon: 'server',
        Type: 'feather',
        Value: 1,
        Checkbox: true,
        Badge: 0,
      },
      {
        Title: 'Đang bóc tách',
        Icon: 'server',
        Type: 'feather',
        Value: 3,
        Checkbox: false,
        Badge: 0,
      },
      {
        Title: 'Hoàn thành',
        Icon: 'check-circle',
        Type: 'feather',
        Value: 2,
        Checkbox: false,
        Badge: 0,
      },
    ];
    //#endregion
  }
  componentDidMount() {}

  //#region Hàm tìm kiếm
  Search = txt => {
    this.state.JTableBom.search.value = txt;
    this.setState({JTableBom: this.state.JTableBom});
    if (this.state.setJtable === 1) {
      this.JTableBomPending(true);
    } else if (this.state.setJtable === 2) {
      this.JTable(true, 'CHO');
    } else {
      this.JTable(true, 'HT');
    }
  };
  //#endregion

  //#region Refresh lại danh sách
  _onRefresh = () => {
    if (this.state.setJtable === 1) {
      this.JTableBomPending(true);
    } else {
      this.JTable(true);
    }
  };
  //#endregion

  //#region API Danh sách
  JTableBomPending(s) {
    if (s === true) {
      this.state.JTableBom.CurrentPage = 1;
      this.setState({
        loadingsearch: true,
        ListAll: [],
      });
    } else {
      this.state.JTableBom.CurrentPage = ++this.state.JTableBom.CurrentPage;
    }
    this.state.JTableBom.StartDate = FuncCommon.ConDate(
      this.state.JTableBom.StartDate,
      2,
    );
    this.state.JTableBom.EndDate = FuncCommon.ConDate(
      this.state.JTableBom.EndDate,
      2,
    );
    this.setState({JTableBom: this.state.JTableBom});
    API_BomCategories.JTableBomPending(this.state.JTableBom)
      .then(res => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
          this.state.ListAll.push(data[i]);
        }
        this.setState({
          ListAll: this.state.ListAll,
          loading: false,
          loadingsearch: false,
        });
      })
      .catch(error => {
        this.setState({loading: false, loadingsearch: false});
        console.log(error);
      });
  }
  JTable(s, type = 'HT') {
    if (s === true) {
      this.state.JTableBom.CurrentPage = 1;

      this.setState({
        loadingsearch: true,
        ListAllV2: [],
      });
    } else {
      this.state.JTableBom.CurrentPage = ++this.state.JTableBom.CurrentPage;
    }
    this.state.JTableBom.StartDate = FuncCommon.ConDate(
      this.state.JTableBom.StartDate,
      2,
    );
    this.state.JTableBom.EndDate = FuncCommon.ConDate(
      this.state.JTableBom.EndDate,
      2,
    );
    this.state.JTableBom.Status = type;
    this.setState({JTableBom: this.state.JTableBom});
    API_BomCategories.JTable(this.state.JTableBom)
      .then(res => {
        var data = JSON.parse(res.data.data).data;
        for (let i = 0; i < data.length; i++) {
          this.state.ListAllV2.push(data[i]);
        }
        this.setState({
          ListAllV2: this.state.ListAllV2,
          loading: false,
          loadingsearch: false,
        });
      })
      .catch(error => {
        this.setState({loading: false, loadingsearch: false});
        console.log(error);
      });
  }
  //#endregion

  //#region View tổng
  render() {
    return (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          <TabBar
            title={'Thiết lập BOM'}
            BackModuleByCode={'PRO'}
            FormSearch={true}
            CallbackFormSearch={callback =>
              this.setState({EvenFromSearch: callback})
            }
          />
          {this.state.EvenFromSearch === true ? (
            <View style={{flexDirection: 'column'}}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                }}>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={AppStyles.Labeldefault}>Thời gian bắt đầu</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventStartDate: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.state.JTableBom.StartDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, flexDirection: 'column', padding: 5}}>
                  <Text style={AppStyles.Labeldefault}>Thời gian kết thúc</Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.setState({setEventEndDate: true})}>
                    <Text>
                      {FuncCommon.ConDate(this.state.JTableBom.EndDate, 0)}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column', padding: 5}}>
                  <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
                    Lọc
                  </Text>
                  <TouchableOpacity
                    style={[AppStyles.FormInput]}
                    onPress={() => this.onActionSearchDate()}>
                    <Icon
                      name={'down'}
                      type={'antdesign'}
                      size={18}
                      color={AppColors.gray}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <FormSearch CallbackSearch={callback => this.Search(callback)} />
            </View>
          ) : null}
          {this.state.loading !== true ? (
            this.state.setJtable === 1 ? (
              this.state.ListAll.length > 0 ? (
                this.ViewList(this.state.ListAll)
              ) : this.state.loadingsearch !== true ? (
                <View style={{flex: 1, alignItems: 'center'}}>
                  <Text style={{fontWeight: 'bold'}}>Không có dữ liệu</Text>
                </View>
              ) : (
                <View style={{flex: 1}} />
              )
            ) : this.state.ListAllV2.length > 0 ? (
              this.ViewListV2(this.state.ListAllV2)
            ) : this.state.loadingsearch !== true ? (
              <View style={{flex: 1, alignItems: 'center'}}>
                <Text style={{fontWeight: 'bold'}}>Không có dữ liệu</Text>
              </View>
            ) : (
              <View style={{flex: 1}} />
            )
          ) : (
            <LoadingComponent backgroundColor={'#fff'} />
          )}
          <View style={AppStyles.StyleTabvarBottom}>
            <TabBarBottomCustom
              ListData={this.listtabbarBotom}
              onCallbackValueBottom={callback =>
                this.CallbackValueBottom(callback)
              }
            />
          </View>
          {this.state.setEventStartDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.JTable(true), this.setState({setEventStartDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.JTableBom.StartDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setStartDate(setDate)}
              />
            </View>
          ) : null}
          {this.state.setEventEndDate === true ? (
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                position: 'absolute',
                bottom: 0,
                zIndex: 1000,
                width: SCREEN_WIDTH,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 0.5,
                  borderColor: AppColors.gray,
                  padding: 10,
                }}
                onPress={() => {
                  this.JTable(true), this.setState({setEventEndDate: false});
                }}>
                <Text style={{textAlign: 'right'}}>Xong</Text>
              </TouchableOpacity>
              <DatePicker
                locale="vie"
                date={this.state.JTableBom.EndDate}
                mode="date"
                style={{width: SCREEN_WIDTH}}
                onDateChange={setDate => this.setEndDate(setDate)}
              />
            </View>
          ) : null}
          <MenuSearchDate
            value={this.state.ValueSearchDate}
            callback={this.CallbackSearchDate}
            eOpen={this.openMenuSearchDate}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  //#endregion

  //#region View danh sách
  ViewList = item => {
    return (
      <FlatList
        data={item}
        renderItem={({item, index}) => (
          <TouchableOpacity onPress={() => this.ActionViewOpen(item)}>
            <ListItem
              key={index}
              leftAvatar={
                <Image
                  style={{width: 50, height: 50}}
                  source={{uri: item.UrlImage}}
                />
              }
              title={
                <View style={{flexDirection: 'column'}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>Số ĐH: </Text>
                    <Text style={{fontWeight: 'bold'}}>{item.OrderNumber}</Text>
                  </View>
                </View>
              }
              subtitle={() => {
                return (
                  <View style={{flexDirection: 'column'}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={{fontWeight: 'bold'}}>Mã SP(NB): </Text>
                      <Text>{item.ItemID}</Text>
                    </View>
                    {/* <View style={{flexDirection: 'row'}}>
                      <Text style={{fontWeight: 'bold'}}>Mã SP(KH): </Text>
                      <Text>{item.ItemIDByProvider}</Text>
                    </View> */}
                    <Text style={{fontWeight: 'bold'}}>
                      {item.ItemName?.length > 35
                        ? item.ItemName.substring(0, 35) + '...'
                        : item.ItemName}
                    </Text>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={{fontWeight: 'bold'}}>ĐVT: </Text>
                        <Text>{item.UnitName}</Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={{fontWeight: 'bold'}}>SL: </Text>
                        <Text>{item.Quantity}</Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={{fontWeight: 'bold'}}>YC: </Text>
                        <Text>{FuncCommon.ConDate(item.OrderDate, 0)}</Text>
                      </View>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={{fontWeight: 'bold'}}>XN: </Text>
                        <Text>
                          {FuncCommon.ConDate(item.ConfirmDateDeliveryDates, 0)}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              }}
              bottomDivider
            />
          </TouchableOpacity>
        )}
        onEndReached={() =>
          item?.length >= 50 * this.state.JTableBom.CurrentPage
            ? this.JTable(false)
            : null
        }
        keyExtractor={(rs, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    );
  };
  //#endregion

  //#region View danh sách đã bóc tách
  ViewListV2 = item => {
    return (
      <FlatList
        data={item}
        renderItem={({item, index}) => (
          <TouchableOpacity onPress={() => this.ActionViewOpen(item)}>
            <ListItem
              key={index}
              leftAvatar={
                <Image
                  style={{width: 50, height: 50}}
                  source={{uri: item.UrlImage}}
                />
              }
              title={
                <View style={{flexDirection: 'column'}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>Mã BOM: </Text>
                    <Text style={{fontWeight: 'bold'}}>{item.BOMNumber}</Text>
                  </View>
                </View>
              }
              subtitle={() => {
                return (
                  <View style={{flexDirection: 'column'}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={{fontWeight: 'bold'}}>Mã SP(NB): </Text>
                      <Text>{item.ItemID}</Text>
                    </View>
                    <Text style={{fontWeight: 'bold'}}>
                      {item.ItemName?.length > 35
                        ? item.ItemName.substring(0, 35) + '...'
                        : item.ItemName}
                    </Text>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={{fontWeight: 'bold'}}>ĐVT: </Text>
                        <Text>{item.UnitName}</Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={{fontWeight: 'bold'}}>Giá thành KH: </Text>
                        <Text>{this.addPeriod(item.Amount)}</Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={{fontWeight: 'bold'}}>Ngày BT: </Text>
                        <Text>{FuncCommon.ConDate(item.CreatedDate, 0)}</Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'flex-end',
                        }}>
                        {/* <Text
                          style={
                            item.ApprovedStatus === 'Y'
                              ? {color: AppColors.blue}
                              : {color: AppColors.UntreatedColor}
                          }>
                          {this.CheckStatus(item.ApprovedStatus)}
                        </Text> */}
                      </View>
                    </View>
                  </View>
                );
              }}
              bottomDivider
            />
          </TouchableOpacity>
        )}
        onEndReached={() =>
          item?.length >= 50 * this.state.JTableBom.CurrentPage
            ? this.JTable(false)
            : null
        }
        keyExtractor={(rs, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this._onRefresh}
            tintColor="#f5821f"
            titleColor="#fff"
            colors={[AppColors.Maincolor]}
          />
        }
      />
    );
  };
  //#endregion

  //#region thay đổi thời gian tìm kiếm
  setStartDate = date => {
    this.state.JTableBom.StartDate = date;
    this.setState({JTableBom: this.state.JTableBom});
    this.JTable(true);
  };
  setEndDate = date => {
    this.state.JTableBom.EndDate = date;
    this.setState({JTableBom: this.state.JTableBom});
    this.JTable(true);
  };
  //#endregion
  //#region chức năng lọc này cố định
  _openMenuSearchDate() {}
  openMenuSearchDate = d => {
    this._openMenuSearchDate = d;
  };
  onActionSearchDate = () => {
    this._openMenuSearchDate();
  };
  CallbackSearchDate = callback => {
    if (callback.start !== undefined) {
      this.state.JTableBom.StartDate = new Date(callback.start);
      this.state.JTableBom.EndDate = new Date(callback.end);
      this.setState(
        {
          JTableBom: this.state.JTableBom,
          ValueSearchDate: callback.value,
          ListAll: [],
          ListAllV2: [],
        },
        () => {
          this.Search('');
        },
      );
    }
  };
  //#endregion
  //#region CallbackValueBottom
  CallbackValueBottom = callback => {
    this.setState({setJtable: callback});
    if (callback === 1) {
      this.JTableBomPending(true);
    } else if (callback === 2) {
      this.JTable(true, 'HT');
    } else {
      this.JTable(true, 'CHO');
    }
  };
  //#endregion
  //#region ViewOpen
  ActionViewOpen = item => {
    Actions.BomCategories_Open({Data: item, BomPending: this.state.setJtable});
  };
  //#endregion
  //#region Converdate
  ConvertDateSearch = type => {
    var newdate = new Date();
    var month = newdate.getMonth() + 1;
    var monthBefore = newdate.getMonth();
    var year = newdate.getFullYear();
    var day = newdate.getDate();
    if (month < 10) month = '0' + month;
    if (monthBefore < 10) monthBefore = '0' + monthBefore;
    if (day < 10) day = '0' + day;
    if (type === 1) {
      return year + '-' + monthBefore + '-' + day;
    } else {
      return year + '-' + month + '-' + day;
    }
  };
  //#endregion
  //#region CheckStatus
  CheckStatus = data => {
    if (data === 'Y') {
      return 'Đã duyệt';
    } else if (data === 'N') {
      return 'Chưa duyệt';
    } else if (data === 'NY') {
      return 'Không duyệt';
    } else {
      return 'Bản nháp';
    }
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(BomCategories_List_Component);
