import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import {
  Header,
  Divider,
  Button,
  Input,
  SearchBar,
  CheckBox,
} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions, ActionConst} from 'react-native-router-flux';
import {Back} from '@component';
import {API_HR} from '@network';
import {ErrorHandler} from '@error';
import {FuncCommon} from '../../../utils';
import {
  Container,
  Content,
  List,
  Left,
  Right,
  Form,
  Item,
  Picker,
  ListItem,
  Thumbnail,
  Body,
  Label,
  DatePicker,
} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import {AppStyles, AppSizes, AppColors} from '@theme';
import MultiSelect_Component from '../../component/MultiSelect_Component';
import TabBar from '../../component/TabBar';
import RNPickerSelect from 'react-native-picker-select';
class AttendancesComponent extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.Length = 15;
    this.TotalRow = 0;
    this.state = {
      name: '',
      loading: false,
      refreshing: false,
      employeeData: [],
      DataShift: [],
      bankDataItem: null,
      search: '',
      StatusOfWork: ['OM'],
      EvenFromSearch: false,
      Date: new Date(),
      ShiftId: null,
      Countcheck: 0,
      StartTime: null,
      EndTime: null,
      DataView: [],
      StatusOfWorkData: [
        {
          id: 'OM',
          name: 'Nhân viên chính thức',
        },
        {
          id: 'PE',
          name: 'Nhân viên thử việc',
        },
        {
          id: 'PT',
          name: 'Nhân viên thời vụ',
        },
        {
          id: 'ML',
          name: 'Nghỉ thai sản',
        },
        {
          id: 'OL',
          name: 'Nghỉ khác',
        },
        {
          id: 'LJ',
          name: 'Nhân viên nghỉ việc',
        },
        {
          id: 'EC',
          name: 'Hết hạn hợp đồng',
        },
      ],
    };
  }
  componentDidMount(): void {
    this.getAllEmployeePage(this.page);
    this.GetShifts();
    //this.Submit();
  }
  keyExtractor = (item, index) => index.toString();

  renderItem = ({item, index}) => {
    return (
      <ListItem avatar onPress={() => this.editItem(item)}>
        <Left>
          <Thumbnail source={API_HR.GetPicApplyLeaves(item.EmployeeGuid)} />
        </Left>
        <Body>
          <Text style={[AppStyles.Titledefault]}>{item.EmployeeName}</Text>
          <Text style={[AppStyles.Textdefault]}>
            Mã nhân viên: {item.EmployeeId != null ? item.EmployeeId : ''}
          </Text>
          <Text style={[AppStyles.Textdefault]}>Ca: {item.ShiftName}</Text>
          <View style={{flexDirection: 'row', padding: 1}}>
            <View style={{flex: 1}}>
              <Text style={[AppStyles.Textdefault]}>Vào: {item.StartTime}</Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={[AppStyles.Textdefault]}>Ra: {item.EndTime}</Text>
            </View>
          </View>
        </Body>
        <Right>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <CheckBox
              checked={this.state.employeeData[index].CheckAprove}
              onPress={() => this.onCheckBox(item.EmployeeGuid, index)}
            />
          </View>
        </Right>
      </ListItem>
    );
  };
  _onRefresh = () => {
    this.getAllEmployeePage(this.page);
  };
  searchFilterFunction = text => {
    this.setState({employeeData: []});
    this.state.search = text;
    this.getAllEmployeePage(1);
  };
  onPressback() {
    Actions.app();
  }
  EvenFromSearch() {
    if (this.state.EvenFromSearch === false) {
      this.setState({EvenFromSearch: true});
    } else {
      this.setState({EvenFromSearch: false});
    }
  }
  render() {
    let arrayholder = [];
    return (
      <Container>
        <TabBar
          title={'Danh sách điểm danh'}
          FormSearch={true}
          CallbackFormSearch={callback =>
            this.setState({EvenFromSearch: callback})
          }
        />
        <View>
          {this.state.EvenFromSearch == true ? (
            <SearchBar
              placeholder="Tìm kiếm..."
              lightTheme
              round
              inputContainerStyle={{backgroundColor: '#e1ecf4'}}
              containerStyle={AppStyles.FormSearchBar}
              onChangeText={text => this.searchFilterFunction(text)}
              value={this.state.search}
            />
          ) : null}
        </View>
        <View style={{marginStart: 10, marginEnd: 10}}>
          {this.state.EvenFromSearch == true ? (
            <MultiSelect_Component
              ListCombobox={this.state.StatusOfWorkData}
              CallBackListCombobox={callback =>
                this.onValueChangeGroup(callback)
              }
            />
          ) : null}
        </View>
        <View style={{marginStart: 10, marginEnd: 10}}>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <Item>
                <Label>Ngày:</Label>
                <DatePicker
                  locale="vie"
                  defaultDate={new Date()}
                  // minimumDate={new Date()}
                  // maximumDate={new Date(2018, 12, 31)}
                  locale={'vi'}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={'fade'}
                  androidMode={'default'}
                  textStyle={{color: 'green'}}
                  placeHolderTextStyle={{color: '#d3d3d3'}}
                  onDateChange={date => this.setStartDate(date)}
                  disabled={false}
                />
              </Item>
            </View>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <Item>
                <Label>Ca:</Label>
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{width: undefined}}
                  placeholder="Chọn ca"
                  placeholderStyle={{color: '#bfc6ea'}}
                  placeholderIconColor="#007aff"
                  selectedValue={this.state.ShiftId}
                  onValueChange={this.onShiftIdChangeGroup.bind(this)}>
                  {this.shiftList()}
                </Picker>
                <Label>Vào: </Label>
                <Label>{this.state.StartTime}</Label>
                <Label>Ra:</Label>
                <Label>{this.state.EndTime}</Label>
              </Item>
            </View>
          </View>
        </View>
        {this.state.employeeData ? (
          <FlatList
            keyExtractor={this.keyExtractor}
            data={this.state.employeeData}
            refreshing={this.state.loading}
            renderItem={this.renderItem}
            onEndReached={this.handleLoadMore}
            onRefresh={this._onRefresh}
            ListFooterComponent={this.renderFooter.bind(this)}
            onEndReachedThreshold={0.4}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                tintColor="#f5821f"
                titleColor="#fff"
                colors={['red', 'green', 'blue']}
              />
            }
          />
        ) : null}
      </Container>
    );
  }
  setStartDate(Date) {
    this.setState({Date: Date});
    this.getAllEmployeePage(0);
  }
  employeeList = () => {
    return this.state.employeeData.map((x, i) => {
      return (
        <Picker.Item label={x.EmployeeName} key={i} value={x.EmployeeId} />
      );
    });
  };
  shiftList = () => {
    return this.state.DataShift.map((x, i) => {
      return <Picker.Item label={x.ShiftName} key={i} value={x.ShiftId} />;
    });
  };
  clickBack() {
    Actions.app();
  }
  onValueChangeGroup(value) {
    if (value.includes(',')) {
      let stamp = value.split(',');
      this.setState({
        StatusOfWork: stamp,
        employeeData: [],
      });
      this.state.StatusOfWork = stamp;
    } else {
      this.setState({
        StatusOfWork: value,
        employeeData: [],
      });
      this.state.StatusOfWork = value;
    }
    console.log('==============StatusOfWork: ' + this.state.StatusOfWork);
    this.getAllEmployeePage(1);
  }
  onShiftIdChangeGroup(value) {
    for (let i = 0; i < this.state.DataShift.length; i++) {
      if (value === this.state.DataShift[i].ShiftId) {
        this.setState({
          StartTime: this.state.DataShift[i].StartTime.split('', 5),
          EndTime: this.state.DataShift[i].EndTime.split('', 5),
          ShiftId: value,
        });
      }
    }
  }
  StatusOfWorkData = () => {
    return this.state.StatusOfWorkData.map((x, i) => {
      return <Picker.Item label={x.text} key={i} value={x.value} />;
    });
  };
  editItem(item) {
    // Actions.TimeKeepingbyMonth({EmployeeItem: item});
  }
  addItem() {
    Actions.additem({EmployeeGuid: ''});
  }
  onCheckBox(para, index) {
    var _countcheck = 0;
    var _list = this.state.employeeData;
    _list[index].CheckAprove = !_list[index].CheckAprove;
    this.setState({
      employeeData: _list,
    });
    var _dataView = this.state.DataView;
    if (_list[index].CheckAprove == true) {
      _dataView.push({
        Date: this.state.Date,
        EmployeeGuid: _list[index].EmployeeGuid,
        ShiftId: this.state.ShiftId,
        CheckAprove: true,
      });
    } else {
      _dataView.push({
        Date: this.state.Date,
        EmployeeGuid: _list[index].EmployeeGuid,
        ShiftId: this.state.ShiftId,
        CheckAprove: false,
      });
    }
    this.Submit(_dataView);
  }
  GetShifts() {
    let _obj = {};
    API_HR.GetShifts(_obj)
      .then(rs => {
        let _listReason = JSON.parse(JSON.parse(JSON.stringify(rs.data.data)));
        this.setState({
          DataShift: _listReason,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
  Submit(_data) {
    console.log('testlog', _data);
    API_HR.AddEmployeeSX(_data)
      .then(rs => {
        var _rs = rs.data;
        if ((_rs.data = 'ok')) {
          console.log('testRS', _rs);
          //Actions.Attendances();
        } else {
          Actions.Attendances();
        }
      })
      .catch(error => {
        console.log('Error when call API Task GetEmployee Mobile.');
        console.log(error);
      });
  }
  getAllEmployeePage = NumberPage => {
    let obj = {
      Date: FuncCommon.ConDate(this.state.Date, 4),
    };
    this.setState({loading: true});
    API_HR.getAllEmployeeSX(obj)
      .then(response => {
        console.log('==============ketqua: ', obj);
        let listData = this.state.employeeData;
        let data = response.data;
        data.EmployeeId = data.EmployeeId != null ? data.EmployeeId : '';
        this.setState({employeeData: data, loading: false});
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  handleLoadMore = () => {
    if (!this.state.loading && this.TotalRow > this.state.employeeData.length) {
      this.page = this.page + 1; // increase page by 1
      this.getAllEmployeePage(this.page); // method for API call
    }
  };
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return <Text style={{color: '#000'}} />;
  };
}
const colors = {
  loginGreen: '#347F16',
};
const styles = StyleSheet.create({
  loginContainer: {
    marginTop: 10,
    marginEnd: 10,
    alignItems: 'flex-end',
  },
  button: {
    height: AppSizes.buttonHeight,
    paddingHorizontal: 25,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: AppSizes.borderRadius,
  },
  border: {
    backgroundColor: AppColors.transparent,
    borderColor: colors.loginGreen,
    borderWidth: AppSizes.borderWith,
  },
  buttonText: {
    ...AppStyles.boldText,
  },
  containerContent: {
    flex: 20,
    flexDirection: 'row',
    marginBottom: 10,
    backgroundColor: 'white',
  },
});

//Redux
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AttendancesComponent);
