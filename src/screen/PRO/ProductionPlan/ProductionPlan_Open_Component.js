import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  Animated,
  Text,
  TouchableWithoutFeedback,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {AppStyles, AppColors} from '@theme';
import {connect} from 'react-redux';
import {API_ProductionPlan} from '../../../network';
import TabBar_Title from '../../component/TabBar_Title';
import {Divider, ListItem, Icon} from 'react-native-elements';
import LoadingComponent from '../../component/LoadingComponent';
import {Actions} from 'react-native-router-flux';
import {FuncCommon} from '../../../utils';
import {ScrollView} from 'react-native-gesture-handler';
import configApp from '../../../configApp';

const DRIVER = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
class ProductionPlan_Open_Component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      Data: props.Data !== undefined ? props.Data : null,
      //hiển thị quy trình công nghệ
      ViewBOMWorkpieces: false,
      transIcon_ViewBOMWorkpieces: new Animated.Value(0),
      DataBOMWorkpieces: [], // Quy trình công nghệ
      DataBOMWorkpieces: [], //định mực phôi
    };
  }
  componentDidMount() {
    this.Skill();
    this.setEventBOMWorkpieces();
  }
  //#region Skill
  Skill = () => {
    //#region đóng mở tab quy trình công nghệ
    this.Animated_on_BOMWorkpieces = Animated.timing(
      this.state.transIcon_ViewBOMWorkpieces,
      {toValue: 1, duration: 333},
    );
    this.Animated_off_BOMWorkpieces = Animated.timing(
      this.state.transIcon_ViewBOMWorkpieces,
      {toValue: 0, duration: 333},
    );
    //#endregion
  };
  //#endregion

  render() {
    return this.state.loading !== true ? (
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={[AppStyles.container]}>
          {this.state.Data !== null ? (
            <TabBar_Title
              title={'Chi tiết kế hoạch bộ phận'}
              callBack={() => this.callBackList()}
            />
          ) : null}
          <Divider />
          <View style={{flex: 1}}>
            {this.state.Data !== null
              ? this.CustomFormDetailV2(this.state.Data)
              : null}
          </View>
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <LoadingComponent backgroundColor={'#fff'} />
    );
  }
  CustomFormDetailV2 = item => {
    return (
      <ScrollView style={{flexDirection: 'column'}}>
        <View style={{flexDirection: 'row', padding: 5}}>
          <View
            style={{
              borderWidth: 0.5,
              borderColor: AppColors.gray,
              marginRight: 5,
            }}>
            <Image
              style={{width: 100, height: 100}}
              source={
                item.UrlImage
                  ? {uri: configApp.url_pro_image + item.UrlImage}
                  : require('./no_image.png')
              }
            />
          </View>
          <View style={{flex: 1}}>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Mã KH</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>: {item.PartItemID}</Text>
              </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Tên bản vẽ</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  : {item.OrderItemName}
                </Text>
              </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Mã bản vẽ</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>: {item.OrderItemId}</Text>
              </View>
            </View>
            {/* <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Max</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  : {item.PartItemID}
                </Text>
              </View>
            </View> */}
          </View>
        </View>
        <Divider />
        <View style={{padding: 5}}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 2, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Ngày BĐ</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  : {FuncCommon.ConDate(item.StartDate, 0)}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
              }}>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Labeldefault}>S/L</Text>
              </View>
              <View style={{flex: 2, flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>:</Text>
                <View style={{flex: 1, marginLeft: 5}}>
                  <Text style={AppStyles.Textdefault}>
                    {this.customFloat(item.Quantity)}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 2, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Ngày KT</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  : {FuncCommon.ConDate(item.EndDate, 0)}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
              }}>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Labeldefault}>KQSX</Text>
              </View>
              <View style={{flex: 2, flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>:</Text>
                <View style={{flex: 1, marginLeft: 5}}>
                  <Text style={AppStyles.Textdefault}>
                    {this.customFloat(item.KQSX)}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 2, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>QTCN</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>
                  : {item.LocationTitle}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
              }}>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Labeldefault}>Còn lại</Text>
              </View>
              <View style={{flex: 2, flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>:</Text>
                <View style={{flex: 1, marginLeft: 5}}>
                  <Text style={AppStyles.Textdefault}>
                    {this.customFloat(item.REMAIN)}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 2, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Số PO</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>: {item.OrderNumber}</Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
              }}>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Labeldefault}>ĐVT</Text>
              </View>
              <View style={{flex: 2, flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>:</Text>
                <View style={{flex: 1, marginLeft: 5}}>
                  <Text style={AppStyles.Textdefault}>{item.UnitName}</Text>
                </View>
              </View>
            </View>
          </View>
          {/* <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 2, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>Mã phôi</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>: {item.MaPhoi}</Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
              }}>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Labeldefault}>Phôi</Text>
              </View>
              <View style={{flex: 2, flexDirection: 'row'}}>
                <Text style={AppStyles.Labeldefault}>:</Text>
                <View style={{flex: 1, marginLeft: 5}}>
                  <Text style={AppStyles.Textdefault}>
                    {this.customFloat(item.InventoryQuantity)}
                  </Text>
                </View>
              </View>
            </View>
          </View> */}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 2, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text style={AppStyles.Labeldefault}>KH Đơn hàng</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={AppStyles.Textdefault}>: {item.OrderPlanID}</Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
              }}
            />
          </View>
        </View>
        {/* {this.CustomViewBOMWorkpieces()} */}
      </ScrollView>
    );
  };

  // Tab danh sách nguyên vật liệu
  //#region setEventBOMWorkpieces (quy trình công nghệ)
  setEventBOMWorkpieces = () => {
    if (this.state.ViewBOMWorkpieces === false) {
      var qty =
        this.state.Data.Quantity === ''
          ? 0
          : parseInt(this.state.Data.Quantity) === 0
          ? 1
          : parseInt(this.state.Data.Quantity);
      var qtyMN =
        this.state.Data.QuantityMN === ''
          ? 0
          : parseInt(this.state.Data.QuantityMN) === 0
          ? 1
          : parseInt(this.state.Data.QuantityMN);
      var obj = {
        BomNumber: this.state.Data.BOMNumber,
        LocationCode: this.state.Data.LocationID,
        PartItemID: this.state.Data.PartItemID,
        Quantity: qty * qtyMN,
        CurrentPage: 1,
        search: {value: ''},
        Length: 1000,
      };
      API_ProductionPlan.JTableStore_BOMWorkpieces(obj)
        .then(res => {
          var data = JSON.parse(res.data.data).data;
          var list = [];
          var Weight = 0;
          var QuantityInventory = 0;
          var QuantityExport = 0;
          var QuantityRemain = 0;
          for (let i = 0; i < data.length; i++) {
            list.push(data[i]);
            Weight =
              Weight + parseFloat(data[i].Weight === '' ? 0 : data[i].Weight);
            QuantityInventory =
              QuantityInventory +
              parseFloat(
                data[i].QuantityInventory === ''
                  ? 0
                  : data[i].QuantityInventory,
              );
            QuantityExport =
              QuantityExport +
              parseFloat(
                data[i].QuantityExport === '' ? 0 : data[i].QuantityExport,
              );
            QuantityRemain =
              QuantityRemain +
              parseFloat(
                data[i].QuantityRemain === '' ? 0 : data[i].QuantityRemain,
              );
          }
          this.Weight_BOMWorkpieces = this.customFloat(Weight.toString());
          this.QuantityInventory_BOMWorkpieces = this.customFloat(
            QuantityInventory.toString(),
          );
          this.QuantityExport_BOMWorkpieces = this.customFloat(
            QuantityExport.toString(),
          );
          this.QuantityRemain_BOMWorkpieces = this.customFloat(
            QuantityRemain.toString(),
          );
          this.setState({
            DataBOMWorkpieces: list,
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setViewOpen('ViewBOMWorkpieces');
  };
  //#endregion
  //#region tab Quy trình công nghệ
  CustomViewBOMWorkpieces = () => {
    const rotateStart_ViewBOMWorkpieces = this.state.transIcon_ViewBOMWorkpieces.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      },
    );
    return (
      <ScrollView>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#bed9f6',
          }}
          onPress={() => this.setEventBOMWorkpieces()}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={AppStyles.Labeldefault}>
              Danh sách nguyên vật liệu
            </Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {rotate: rotateStart_ViewBOMWorkpieces},
                {perspective: 4000},
              ],
              paddingRight: 10,
              paddingLeft: 10,
            }}>
            <Icon type={'feather'} name={'chevron-up'} />
          </Animated.View>
        </TouchableOpacity>
        <Divider />
        {this.state.ViewBOMWorkpieces === true ? (
          <View style={{flex: 1}}>
            {/* Table */}
            <ScrollView horizontal={true}>
              <View style={{flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 40}]}>
                    <Text style={AppStyles.Labeldefault}>STT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>Loại</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>Mã NVL - Phôi</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 150}]}>
                    <Text style={AppStyles.Labeldefault}>Tên NVL - Phôi</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 80}]}>
                    <Text style={AppStyles.Labeldefault}>ĐVT</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>SL cần dùng</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>
                      SL tồn kho đáp ứng
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>SL đã xuất dùng</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[AppStyles.table_th, {width: 100}]}>
                    <Text style={AppStyles.Labeldefault}>SL còn thiếu</Text>
                  </TouchableOpacity>
                </View>
                {this.state.DataBOMWorkpieces.length > 0 ? (
                  <View style={{flexDirection: 'column'}}>
                    {this.state.DataBOMWorkpieces.map((para, i) => (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 40}]}>
                          <Text style={[AppStyles.Textdefault]}>{i + 1}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.Type}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.MaterialItemID}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 150}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.ItemName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 80}]}>
                          <Text style={[AppStyles.Textdefault]}>
                            {para.UnitName}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.Weight)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.QuantityInventory)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.QuantityExport)}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[AppStyles.table_td, {width: 100}]}>
                          <Text
                            style={[
                              AppStyles.Textdefault,
                              {textAlign: 'right'},
                            ]}>
                            {this.customFloat(para.QuantityRemain)}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 40}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 150}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 80}]}
                      />
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.Weight_BOMWorkpieces}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.QuantityInventory_BOMWorkpieces}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.QuantityExport_BOMWorkpieces}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[AppStyles.table_foot, {width: 100}]}>
                        <Text
                          style={[
                            AppStyles.Labeldefault,
                            {textAlign: 'right'},
                          ]}>
                          {this.QuantityRemain_BOMWorkpieces}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity style={[AppStyles.table_foot]}>
                    <Text
                      style={[AppStyles.Textdefault, {textAlign: 'center'}]}>
                      Không có dữ liệu
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </ScrollView>
          </View>
        ) : null}
      </ScrollView>
    );
  };
  //#endregion

  //Các hàm dùng chung
  //#region đóng mở các tab
  setViewOpen = val => {
    this.setState({
      ViewBOMWorkpieces: false,
    });
    if (val === 'ViewBOMWorkpieces') {
      this.setState({ViewBOMWorkpieces: !this.state.ViewBOMWorkpieces});
      if (this.state.ViewBOMWorkpieces === false) {
        Animated.sequence([this.Animated_on_BOMWorkpieces]).start();
      } else {
        Animated.sequence([this.Animated_off_BOMWorkpieces]).start();
      }
    }
  };
  //#endregion
  //#region Quay lại trang trước
  callBackList = () => {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  };
  //#endregion
  //#region định dạng kiểu số
  addPeriod = nStr => {
    if (nStr !== null && nStr !== '' && nStr !== undefined) {
      nStr = Math.round(parseFloat(nStr) * 100) / 100;
      nStr += '';
      if (nStr.indexOf('.') >= 0) {
        var x = nStr.split('.');
      } else {
        var x = nStr.split(',');
      }
      var x1 = x[0];
      var x2 = x.length > 1 && parseInt(x[1]) > 0 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      var Total = x1 + x2;
      return Total;
    } else {
      return 0;
    }
  };
  //#endregion
  //#region  customFloat
  customFloat = val => {
    if (val !== null && val !== '') {
      var _val = val.toString();
      var string = _val.split('.');
      var valview = this.addPeriod(string[0]);
      if (string.length > 1) {
        var _string2 = string[1].substring(0, 2);
        if (_string2 !== '00') {
          return valview + ',' + string[1].substring(0, 2) === '00';
        } else {
          return valview;
        }
      } else {
        return valview;
      }
    } else {
      return 0;
    }
  };
  //#endregion
}
const styles = StyleSheet.create({});
const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {withRef: true},
)(ProductionPlan_Open_Component);
