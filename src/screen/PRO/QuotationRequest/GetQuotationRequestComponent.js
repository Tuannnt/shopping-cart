import React, {Component} from 'react';
import {
  Keyboard,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  FlatList,
  StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import {API_QuotationRequest} from '@network';
import {Actions} from 'react-native-router-flux';
import {Divider} from 'react-native-elements';
import TabBarBottom from '../../component/TabBarBottom';
import {AppStyles, AppColors} from '@theme';
import TabBar_Title from '../../component/TabBar_Title';
import _ from 'lodash';
import {FuncCommon} from '../../../utils';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import controller from './controller';
import listCombobox from './listCombobox';

const headerTable = [
  {title: 'STT', width: 40},
  {title: 'Mã sản phẩm', width: 200},
  {title: 'Tên sản phẩm', width: 200},
  {title: 'Xuất xứ', width: 200},
  {title: 'ĐVT', width: 200},
  {title: 'Số lượng', width: 200},
  {title: 'Hành động', width: 80, hideInDetail: true},
  {title: 'Đơn giá', width: 200},
  {title: 'Thành tiền', width: 200},
];
const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  containerContent: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 50,
    backgroundColor: 'white',
    marginLeft: 0,
  },
  input: {
    margin: 5,
    height: 40,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    borderWidth: 1,
    borderRadius: 5,
  },
  textArea: {
    height: 80,
    justifyContent: 'flex-start',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonContainer: {
    margin: 5,
    paddingTop: 20,
    borderWidth: 1,
    borderRadius: 5,
  },
  totalText: {
    fontWeight: 'bold',
  },
});
class GetQuotationRequestComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [],
      Type: 'A',
      Data: null,
      DataDetail: null,
      Title: '',
      checkInLogin: '',
      LoginName: '',
      Quantity: '',
      viewId: '',
      Type: '',
      ListEmployeeBG: [],
      ListEmployee: [],
    };
  }

  componentDidMount = () => {
    Promise.all([
      this.getItem(),
      this.getEmployees(),
      this.getPeople_Quotation(),
    ]);
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId == 'Back') {
      this.getItem();
    }
  }
  getEmployees = () => {
    controller.getEmployees(rs => {
      if (!rs) {
        return;
      }
      let data = rs.map(item => ({
        ...item,
        value: item.EmployeeId,
        label: item.FullName,
      }));
      this.setState({ListEmployeeBG: data});
    });
  };
  getPeople_Quotation = () => {
    controller.getPeople_Quotation(rs => {
      if (!rs) {
        return;
      }
      let data = rs.map(item => ({
        value: item.value,
        label: item.text,
      }));
      this.setState({ListEmployee: data});
    });
  };
  onClickBack() {
    Actions.pop();
  }
  Delete = () => {
    if (this.state.Data.Status == 1 || this.state.Data.Status == 3) {
      Toast.showWithGravity(
        'Phiếu đã xác nhận không được xóa',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    if (
      this.state.Data.CreatedBy.split('#')[0] !==
      global.__appSIGNALR.SIGNALR_object.USER.LoginName
    ) {
      Toast.showWithGravity(
        'Bạn không có quyền xóa',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    let id = {Id: this.state.viewId};
    this.setState({loading: true});
    API_QuotationRequest.Delete(id.Id)
      .then(response => {
        if (response.data.errorCode == 200) {
          this.setState({
            loading: false,
            RegisterEatGuid: this.state.viewId,
          });
          Toast.showWithGravity('Xóa thành công', Toast.SHORT, Toast.CENTER);
          this.callBackList();
        } else {
          Toast.showWithGravity('Có lỗi khi xóa', Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error.data.data);
      });
  };
  getItem() {
    let viewId = '';
    if (this.props.RecordGuid !== undefined) {
      viewId = this.props.RecordGuid;
    } else {
      viewId = this.props.viewId;
    }
    API_QuotationRequest.GetItem(this.props.RecordGuid)
      .then(res => {
        let _data = JSON.parse(res.data.data);
        API_QuotationRequest.GetItemDetail(this.props.RecordGuid)
          .then(res => {
            let rows = JSON.parse(res.data.data).map(x => {
              x.UnitPriceNet += '';
              x.AmountOc += '';
              x.Amount += '';
              return x;
            });
            this.setState({
              Data: _data,
              rows,
              viewId,
              LoginName: global.__appSIGNALR.SIGNALR_object.USER.LoginName,
            });
          })
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log(error);
      });
  }
  helperStatus = status => {
    switch (status) {
      case 0:
        return 'Chờ xác nhận';
      case 1:
        return 'Đã nhận thông tin';
      case 3:
        return 'Đã hoàn thành';
      case 2:
        return 'Không xác nhận';
      default:
        return '';
    }
  };
  handleEmployeeBG = val => {
    if (!val) {
      return '';
    }
    let res = this.state.ListEmployeeBG.find(x => x.value === val);
    if (!res) {
      return '';
    }
    return res.label || '';
  };
  handleEmployee = val => {
    if (!val) {
      return '';
    }
    let res = this.state.ListEmployee.find(x => x.value === val);
    if (!res) {
      return '';
    }
    return res.label || '';
  };
  itemFlatList = (row, index) => {
    return (
      <View style={{flexDirection: 'row'}} key={index}>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 40}]}>
          <Text style={[AppStyles.Labeldefault, {textAlign: 'center'}]}>
            {Number(index) + 1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.ItemId}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.ItemName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.Origin}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'center'}]}>
            {' '}
            {row.UnitName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'right'}]}>
            {' '}
            {FuncCommon.addPeriod(row.Quantity)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'right'}]}>
            {' '}
            {FuncCommon.addPeriod(row.UnitPriceNet)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[AppStyles.table_td_custom, {width: 200}]}>
          <Text style={[AppStyles.TextTable, {textAlign: 'right'}]}>
            {' '}
            {FuncCommon.addPeriod(row.AmountOc)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  CustomViewDetailRoutings = () => {
    return (
      <View style={{flex: 1}}>
        <View style={{padding: 15}}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Số yêu cầu dự toán :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.QuotationRequestNo}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Mã khách hàng :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.CustomerId}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Nhân viên kinh doanh :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.handleEmployee(this.state.Data.EmployeeId)}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Khách hàng :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.CustomerName}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Địa chỉ :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.Address}
              </Text>
            </View>
          </View>

          {/* {this.state.Data.Phone && (
            <View style={{flexDirection: 'row', padding: 5}}>
              <View style={{flex: 1}}>
                <Text style={AppStyles.Labeldefault}>Số điện thoại :</Text>
              </View>
              <View style={{flex: 3}}>
                <Text style={[AppStyles.Textdefault]}>
                  {this.state.Data.Phone}
                </Text>
              </View>
            </View>
          )} */}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Nhân viên dự toán :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.handleEmployeeBG(this.state.Data.EmployeeIdBG)}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>
                Ngày xác nhận hoàn thành :
              </Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.ConfirmDate
                  ? moment(this.state.Data.ConfirmDate).format('DD/MM/YYYY')
                  : ''}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>
                Ngày hoàn thành thực tế :
              </Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.FinishDate
                  ? moment(this.state.Data.FinishDate).format('DD/MM/YYYY')
                  : ''}
              </Text>
            </View>
          </View>
          {/*Ngày tạo phiếu*/}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Ngày yêu cầu :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.RequireDate
                  ? moment(this.state.Data.RequireDate).format('DD/MM/YYYY')
                  : ''}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Hạn yêu cầu :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.TimeRequired
                  ? moment(this.state.Data.TimeRequired).format('DD/MM/YYYY')
                  : ''}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Loại sản phẩm :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.ProductGroupId === 1
                  ? 'Thương mại'
                  : 'Sản xuất'}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Trạng thái :</Text>
            </View>
            <View style={{flex: 3}}>
              {this.state.Data.Status == 1 || this.state.Data.Status == 3 ? (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {
                      justifyContent: 'center',
                      color: AppColors.AcceptColor,
                    },
                  ]}>
                  {this.helperStatus(this.state.Data.Status)}
                </Text>
              ) : (
                <Text
                  style={[
                    AppStyles.Textdefault,
                    {
                      justifyContent: 'center',
                      color: AppColors.PendingColor,
                    },
                  ]}>
                  {this.helperStatus(this.state.Data.Status)}
                </Text>
              )}
            </View>
          </View>
          {/* tieu de */}
          <View style={{flexDirection: 'row', padding: 5}}>
            <View style={{flex: 1}}>
              <Text style={AppStyles.Labeldefault}>Nội dung :</Text>
            </View>
            <View style={{flex: 3}}>
              <Text style={[AppStyles.Textdefault]}>
                {this.state.Data.Title}
              </Text>
            </View>
          </View>
        </View>
        <Text style={{fontSize: 16, fontWeight: 'bold', padding: 10}}>
          Chi tiết phiếu
        </Text>
        <ScrollView horizontal={true}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              {headerTable.map(item => {
                if (item.hideInDetail) {
                  return null;
                }
                return (
                  <TouchableOpacity
                    key={item.title}
                    style={[AppStyles.table_th, {width: item.width}]}>
                    <Text style={AppStyles.Labeldefault}>{item.title}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
            <View>
              {this.state.rows.length > 0 ? (
                <View>
                  <FlatList
                    data={this.state.rows}
                    renderItem={({item, index}) => {
                      return this.itemFlatList(item, index);
                    }}
                    keyExtractor={(rs, index) => index.toString()}
                  />
                  {this.rowTotal()}
                </View>
              ) : (
                <TouchableOpacity style={[AppStyles.table_foot]}>
                  <Text style={[AppStyles.Textdefault, {textAlign: 'left'}]}>
                    Không có dữ liệu!
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };

  CustomView = item => (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TabBar_Title
        title={'Chi tiết YCDT'}
        callBack={() => this.callBackList()}
      />
      <Divider />
      {/*hiển thị nội dung chính*/}
      <ScrollView>{this.CustomViewDetailRoutings()}</ScrollView>
      {/*nút xử lý*/}
      <View style={{maxHeight: 50}}>
        <TabBarBottom
          //key để quay trở lại danh sách
          onDelete={() => this.Delete()}
          callbackOpenUpdate={this.callbackOpenUpdate}
          // tiêu đề hiển thị trong popup xử lý phiếu
          Title={this.state.Data.QuotationRequestNo}
          //kiểm tra quyền xử lý
          Permisstion={1}
          //kiểm tra bước đầu quy trình
          checkInLogin={1}
          onAttachment={() => this.openAttachments()}
        />
      </View>
    </View>
  );
  openAttachments() {
    var obj = {
      ModuleId: '51',
      RecordGuid: this.props.RecordGuid,
      notEdit:
        this.state.Data.Status == 1 || this.state.Data.Status == 3
          ? true
          : false,
    };
    Actions.attachmentComponent(obj);
  }
  render() {
    return this.state.Data ? this.CustomView(this.state.Data) : null;
  }
  callbackOpenUpdate = () => {
    if (this.state.Data.Status) {
      Toast.showWithGravity(
        'Yêu cầu dự toán đã xác nhận thông tin',
        Toast.SHORT,
        Toast.CENTER,
      );
      return;
    }
    Actions.AddQuotationRequestPro({
      itemData: this.state.Data,
      rowData: this.state.rows,
    });
  };
  callBackList() {
    Keyboard.dismiss();
    Actions.pop();
    Actions.refresh({moduleId: 'Back', ActionTime: new Date().getTime()});
  }
  rowTotal = () => {
    const {rows} = this.state;
    const list = [
      {width: 40},
      {width: 200},
      {width: 200},
      {width: 200},
      {width: 200},
      {width: 200, id: 'quantity'},
      {width: 200},
      {width: 200, id: 'total'},
    ];
    return (
      <View style={{flexDirection: 'row'}}>
        {list.map((item, index) => {
          if (item.id === 'quantity') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    styles.totalText,
                    {fontWeight: 'bold', textAlign: 'right'},
                  ]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.Quantity || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          if (item.id === 'total') {
            return (
              <TouchableOpacity
                key={index}
                style={[
                  AppStyles.table_td_custom,
                  {width: item.width, backgroundColor: '#f5f2f2'},
                ]}>
                <Text
                  style={[
                    AppStyles.Textdefault,
                    styles.totalText,
                    {fontWeight: 'bold', textAlign: 'right'},
                  ]}>
                  {FuncCommon.addPeriod(
                    rows.reduce(
                      (total, row) => total + (+row.AmountOc || 0),
                      0,
                    ),
                  )}
                </Text>
              </TouchableOpacity>
            );
          }
          return (
            <TouchableOpacity
              key={index}
              style={[
                AppStyles.table_td_custom,
                {width: item.width, backgroundColor: '#f5f2f2'},
              ]}
            />
          );
        })}
      </View>
    );
  };
  //sự kiện duyệt phiếu :callback = B là trình phiếu lên , CommentWF là nội dung ý kiến
  submitWorkFlow(callback, CommentWF) {
    if (callback == 'D') {
      let obj = {
        RowGuid: this.state.viewId,
        Comment: CommentWF,
      };
      API_QuotationRequest.Approve(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Xử lý thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi xử lý', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    } else {
      let obj = {
        RowGuid: this.state.viewId,
        Comment: CommentWF,
      };
      API_QuotationRequest.NotApprove(obj)
        .then(res => {
          if (res.data.errorCode === 200) {
            Toast.showWithGravity(
              'Trả lại thành công',
              Toast.SHORT,
              Toast.CENTER,
            );
            this.callBackList();
          } else {
            Toast.showWithGravity('Lỗi khi trả lại', Toast.SHORT, Toast.CENTER);
            this.getItem();
          }
        })
        .catch(error => {
          console.log(error.data.data);
        });
    }
  }
}

//Redux
const mapStateToProps = state => ({
  Loginname: state.user.account,
});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {
    withRef: true,
  },
)(GetQuotationRequestComponent);
