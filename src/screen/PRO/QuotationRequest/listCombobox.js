export default {
  headerTable: [
    {title: 'STT', width: 40},
    {title: 'Mã sản phẩm', width: 200},
    {title: 'Tên sản phẩm', width: 200},
    {title: 'Xuất xứ', width: 200},
    {title: 'ĐVT', width: 200},
    {title: 'Số lượng', width: 200},
    {title: 'Đơn giá', width: 200},
    {title: 'Thành tiền', width: 200},
    {title: 'Hành động', width: 80, hideInDetail: true},
  ],
  list: [
    {width: 40},
    {width: 200},
    {width: 200},
    {width: 200},
    {width: 200},
    {width: 200, id: 'quantity'},
    {width: 200},
    {width: 200, id: 'total'},

    {width: 80},
  ],
  Ration: [
    {
      value: 'A',
      label: 'Xuất ăn 12k',
    },
    {
      value: 'B',
      label: 'Xuất ăn 15k',
    },
    {
      value: 'C',
      label: 'Xuất ăn 28k',
    },
  ],
  ListProductGroupID: [
    {
      value: 1,
      label: 'Thương mại',
    },
    {
      value: 2,
      label: 'Sản xuất',
    },
  ],
  options: {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Huỷ bỏ',
    // takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn ảnh từ thư viện',
    chooseWhichLibraryTitle: 'Chọn thư viện',
  },
  ListComboboxAtt: [
    {
      value: 'img',
      text: 'Chọn ảnh từ thư viện',
    },
    {
      value: 'file',
      text: 'Chọn file',
    },
    // {
    //   value: 'camera',
    //   text: 'Chụp ảnh',
    // },
  ],
  ListStatus: [
    {
      value: '0',
      label: 'Chờ xác nhận',
    },
    {
      value: '1',
      label: 'Đã nhận thông tin',
    },
    // {
    //   value: '3',
    //   label: 'Đã hoàn thành',
    // },
    // {
    //   value: '2',
    //   label: 'Không xác nhận',
    // },
  ],
};
