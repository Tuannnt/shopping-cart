import { LocalStorage } from '@utils';

const AccessTokenManager = { accessToken: null };
const ACCESS_TOKEN_KEY = 'accessToken';

AccessTokenManager.initialize = () => {
  return LocalStorage.get(ACCESS_TOKEN_KEY, (error, result) => {
    AccessTokenManager.accessToken = result;
  });
};

AccessTokenManager.saveAccessToken = (token) => {
  AccessTokenManager.accessToken = token;
  LocalStorage.set(ACCESS_TOKEN_KEY, token);
};

AccessTokenManager.clear = () => {
  AccessTokenManager.accessToken = null;
  LocalStorage.remove(ACCESS_TOKEN_KEY);
};

AccessTokenManager.getAccessToken = () => {
  return AccessTokenManager.accessToken;
};

export default AccessTokenManager;