import { LocalStorage } from '@utils';

const ProfileLogin = {
    dataProfile: {},
    };
const PROFILElOGIN = 'profile';

ProfileLogin.initialize = () => {
  return LocalStorage.get(ACCESS_TOKEN_KEY, (error, result) => {
    ProfileLogin.accessToken = result;
  });
};

ProfileLogin.saveProfile = (obj) => {
  ProfileLogin.dataProfile = obj;
  LocalStorage.set(PROFILElOGIN, obj);
};

ProfileLogin.getLoginName = () => {
  return ProfileLogin.dataProfile.LoginName;
};

export default ProfileLogin;