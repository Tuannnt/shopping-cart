import axios from 'axios';
import qs from 'qs';
import { mock, wrap } from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";


const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}

const API_Message = { instance: getInstance() };

API_Message.hasError = (data) => {
    return !(data && data.code === 1);
};
//-------------------------Begin ApplyOverTimes
//list danh sách
API_Message.Messenger_UserOfChatbox_ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_UserOfChatbox_ListAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//tìm kiếm
API_Message.Messenger_Employees_Search = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_Employees_Search', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//lấy tin nhắn của 1 nhóm chat
API_Message.Messenger_ChatboxMessager_ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ChatboxMessager_ListAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//lấy tin nhắn của 1 nhóm chat
API_Message.Messenger_ChatboxGroup_Insert = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ChatboxGroup_Insert', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Kiểm tra đã từng nhắn tin chưa
API_Message.Messenger_Check_UserOfChatbox = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_Check_UserOfChatbox', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//thêm mới tin nhắn
API_Message.Messenger_ChatboxMessages_Insert = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ChatboxMessages_Insert', data, {
        headers: { ...headers },
    });
};
//lấy ra chuỗi loginName in UserOfChatBoxGroup
API_Message.Messenger_UserSend = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_UserSend', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Update những tin nhắn chưa đọc
API_Message.Messenger_ChatboxMessages_Update = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ChatboxMessages_Update', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//lấy thông tin của người trong nhóm chat
API_Message.Messenger_Information_CheckGroup = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_Information_CheckGroup', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//nhóm chung
API_Message.Messenger_ViewAllGroup_ByLoginName = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ViewAllGroup_ByLoginName', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//thành viên trong nhóm
API_Message.Messenger_AllAccountOfGroup = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_AllAccountOfGroup', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//update Group đổi tên/ thêm thành viên/ rời nhóm
API_Message.Messenger_ChatboxGroup_Update = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ChatboxGroup_Update', data, {
        headers: { ...headers },
    });
};
//lấy tất cả ảnh trong nhóm chat
API_Message.Messenger_ViewAll_Image = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ViewAll_Image', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//xoá group
API_Message.Messenger_ChatBoxGroup_Delete = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ChatBoxGroup_Delete', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Cập nhật đã xem
API_Message.Messenger_ActiveRead = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_ActiveRead', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//thu hồi tin nhắn
API_Message.RecallMess = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/RecallMess', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//tìm kiếm
API_Message.Messenger_Search = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_Search', JSON.stringify(data), {
        headers: { ...headers },
    });
};
// load lại danh sách khi có tin nhắn mới đến
API_Message.Messenger_RefreshList = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_RefreshList', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//update yêu thích
API_Message.Messenger_FavoritesAction = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/Messenger_FavoritesAction', JSON.stringify(data), {
        headers: { ...headers },
    });
};

API_Message.GetIdChatGroup = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/GetIdChatGroup', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_Message.GetIdChat = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/GetIdChat', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_Message.GetUserChatForTags = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/Messenger/GetUserChatForTags', JSON.stringify(data), {
        headers: { ...headers },
    });
};

API_Message.SendNotifify = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Message.instance.post('/api/BaseAPI/SendNotifify', JSON.stringify(data), {
        headers: { ...headers },
    });
};

//trongtq@esvn.com.vn
export default API_Message;
