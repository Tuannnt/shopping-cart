//TinhLV
import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}

const API_Projects = {instance: getInstance()};
//List danh sách dự án
API_Projects.Project_GetProjects = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_Projects.instance.post('/api/projects/Project_GetProjects', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Xem chi tiết dự án
API_Projects.Project_GetProject = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_Projects.instance.post(`/api/projects/Project_GetProject/`, JSON.stringify(obj), {
        headers: {...headers},
    });
};
//List danh sách hạng mục
API_Projects.Project_GetProjectItems = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_Projects.instance.post('/api/projects/Project_GetProjectItems', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//get dự án
API_Projects.Project_GetProject_Select = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Projects.instance.post('api/projects/Project_GetProject_Select', JSON.stringify(data),  {
        headers: {...headers},
    });
};
//Xem chi tiết hạng mục
API_Projects.Project_GetProjectItem = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_Projects.instance.post(`/api/projects/Project_GetProjectItem/`, JSON.stringify(obj), {
        headers: {...headers},
    });
};
export default API_Projects;
