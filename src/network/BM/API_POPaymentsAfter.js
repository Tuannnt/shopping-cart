import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_POPaymentsAfter = {instance: getInstance()};

//POPaymentsAfter
API_POPaymentsAfter.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfter.instance.post(
    '/api/POPaymentsAfter/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsAfter.ListDetails = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfter.instance.post(
    '/api/POPaymentsAfter/ListDetails',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsAfter.GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfter.instance.post(
    '/api/POPaymentsAfter/GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_POPaymentsAfter.GetItemDetails = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfter.instance.post(
    '/api/POPaymentsAfter/GetItemDetails/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//WorkFlow

API_POPaymentsAfter.Approve = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfter.instance.post(
    '/api/POPaymentsAfter/Approve',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Trả lại
API_POPaymentsAfter.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfter.instance.post(
    '/api/POPaymentsAfter/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsAfter.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfter.instance.post(
    '/api/POPaymentsAfter/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_POPaymentsAfter.GetPOPaymentsBeforeCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfter.instance.post(
    '/api/POPaymentsAfter/GetPOPaymentsBeforeCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_POPaymentsAfter;
