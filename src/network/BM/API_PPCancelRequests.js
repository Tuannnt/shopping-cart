import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_PPCancelRequests = { instance: getInstance() };

//ProposedPurchases
API_PPCancelRequests.ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_PPCancelRequests.instance.post('/api/CancelRequests/ListAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//WorkFlow
API_PPCancelRequests.Confirm = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_PPCancelRequests.instance.post('/api/CancelRequests/Confirm', JSON.stringify(obj), {
        headers: { ...headers },
    });
}

//Trả lại
API_PPCancelRequests.NotConfirm = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_PPCancelRequests.instance.post('/api/CancelRequests/NotConfirm', JSON.stringify(obj), {
        headers: { ...headers },
    });
}
//đếm số
API_PPCancelRequests.GetCancelRequestsCount = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_PPCancelRequests.instance.post('/api/CancelRequests/GetCancelRequestsCount', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
export default API_PPCancelRequests;