import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_TicketRequests = {instance: getInstance()};

//TicketRequests
API_TicketRequests.TicketRequests_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetTicketRequestsAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

//TicketRequests with total and count status
API_TicketRequests.TicketRequests_ListAll_Total = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetTicketRequestsAll_BM',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_TicketRequests.GetTicketRequestsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get(
    '/api/TicketRequests/GetTicketRequestById/' + data,
    {
      headers: {...headers},
    },
  );
};

//Danh sách chi tiết đề nghị nhập/xuất
API_TicketRequests.GetTicketRequestsDetailsAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetTicketRequestsDetailsAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_TicketRequests.TicketRequests_GetItem = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/TicketRequests_GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_TicketRequests.TicketRequestDetails_ListDetail = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/TicketRequestDetails_ListDetail/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_TicketRequests.DeleteTicketRequestById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/IsDeleted',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_TicketRequests.TicketRequestsDetails_GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/TicketRequestsDetails_GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//WorkFlow
API_TicketRequests.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_TicketRequests.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//yêu cầu sửa hủy
API_TicketRequests.GetCancelReasons = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get(
    '/api/TicketRequests/GetCancelReasons',
    null,
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.GetCustomersAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetCustomersAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.CancelRequest = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/CancelRequest',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_TicketRequests.GetTicketRequestsCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetTicketRequestsCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.CheckStatus = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/CheckStatus',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_TicketRequests;
