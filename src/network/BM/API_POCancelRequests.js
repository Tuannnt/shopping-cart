import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_POCancelRequests = { instance: getInstance() };

//ProposedPurchases
API_POCancelRequests.ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_POCancelRequests.instance.post('/api/POCancelRequests/ListAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//WorkFlow
API_POCancelRequests.Confirm = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_POCancelRequests.instance.post('/api/POCancelRequests/Confirm', JSON.stringify(obj), {
        headers: { ...headers },
    });
}

//Trả lại
API_POCancelRequests.NotConfirm = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_POCancelRequests.instance.post('/api/POCancelRequests/NotConfirm', JSON.stringify(obj), {
        headers: { ...headers },
    });
}
//đếm số
API_POCancelRequests.GetCancelRequestsCount = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_POCancelRequests.instance.post('/api/POCancelRequests/GetCancelRequestsCount', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
export default API_POCancelRequests;