import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_CheckRequests = {instance: getInstance()};

API_CheckRequests.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CheckRequests.instance.post(
    '/api/CheckRequests/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_CheckRequests.GetCheck = Id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CheckRequests.instance.post(
    `/api/CheckRequests/GetCheck/${Id}`,
    {},
    {
      headers: {...headers},
    },
  );
};
API_CheckRequests.GetNGReasons = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CheckRequests.instance.get(
    '/api/CheckRequests/GetNGReasons',
    null,
    {
      headers: {...headers},
    },
  );
};
API_CheckRequests.Check = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CheckRequests.instance.post(
    '/api/CheckRequests/Check',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_CheckRequests.GetCheckRequestCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CheckRequests.instance.post(
    '/api/CheckRequests/GetCheckRequestCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Lấy quyền
API_CheckRequests.GetPermission = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CheckRequests.instance.get('/api/CheckRequests/GetPermission', {
    headers: {...headers},
  });
};
API_CheckRequests.GetAllDepartments = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CheckRequests.instance.get(
    '/api/CheckRequests/GetAllDepartments',
    {
      headers: {...headers},
    },
  );
};
export default API_CheckRequests;
