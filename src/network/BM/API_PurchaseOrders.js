import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_PurchaseOrders = {instance: getInstance()};

API_PurchaseOrders.PurchaseOrders_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/PurchaseOrders_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.PurchaseOrdersDetails_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/PurchaseOrdersDetails_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.GetPurchaseOrderDetails = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    `/api/PurchaseOrders/GetPurchaseOrderDetails/${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.PurchaseOrders_GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/PurchaseOrders_GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.PurchaseOrderDetails_GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    `/api/PurchaseOrders/PurchaseOrderDetails_GetItems/${id}`,
  );
};
//Lấy thông tin giao hàng
API_PurchaseOrders.GetPODeliveryDates = Id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.get(
    '/api/PurchaseOrders/GetPODeliveryDates/' + Id,
    {},
    {
      headers: {...headers},
    },
  );
};
//Xóa
API_PurchaseOrders.IsDeleted = Id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/IsDeleted/' + Id,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
//WorkFlow
API_PurchaseOrders.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_PurchaseOrders.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.GetEmployeesMH = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.get(
    '/api/PurchaseOrders/GetEmployeesMH/',
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_PurchaseOrders.GetPurchaseOrdersCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/GetPurchaseOrdersCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Lấy danh sách loại đơn hàng
API_PurchaseOrders.GetPOTypesAll = Id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.get(
    '/api/PurchaseOrders/GetPOTypesAll',
    {},
    {
      headers: {...headers},
    },
  );
};
//Lấy danh sách item kiểm tra
API_PurchaseOrders.GetPODetailsCheck = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.get(
    '/api/PurchaseOrders/GetPODetailsCheck/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.GetDepartmentsJexcel = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.get(
    '/api/PurchaseOrders/GetDepartmentsJexcel/',
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.CheckRequest = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/CheckRequest',
    data,
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.GetPlanDate = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/GetPlanDate',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_PurchaseOrders.GetWarehousesJexcel = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/GetWarehousesJexcel',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
// Lấy số phiếu

API_PurchaseOrders.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_PurchaseOrders.GetNumberAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/GetNumberAuto',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//update status
API_PurchaseOrders.UpdateStatus = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_PurchaseOrders.instance.post(
    '/api/PurchaseOrders/UpdateStatus',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
export default API_PurchaseOrders;
