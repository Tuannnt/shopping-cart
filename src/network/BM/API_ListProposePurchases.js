import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_ListProposePurchases = {instance: getInstance()};

//ProposedPurchases
API_ListProposePurchases.ProposedPurchases_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/JTable',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

//Get Number Auto
API_ListProposePurchases.GetNumberAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/GetNumberAuto',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_ListProposePurchases.ProposedPurchaseDetails_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/ProposedPurchaseDetails_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_ListProposePurchases.ProposedPurchases_GetItem = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/ProposedPurchases_GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_ListProposePurchases.PuschaseOderProgress2ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/PurchaseOrders/JTableDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_ListProposePurchases.DeleteProposedPurchaseById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/IsDeleted',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_ListProposePurchases.ProposedPurchaseDetails_GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/ProposedPurchaseDetails_GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//WorkFlow
API_ListProposePurchases.UpdateStatus = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/UpdateIsStatus',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_ListProposePurchases.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_ListProposePurchases.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//yêu cầu sửa hủy
API_ListProposePurchases.GetCancelReasons = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.get(
    '/api/ProposedPurchases/GetCancelReasons',
    null,
    {
      headers: {...headers},
    },
  );
};
API_ListProposePurchases.CancelRequest = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/CancelRequest',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_ListProposePurchases.GetProposedPurchasesCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ListProposePurchases.instance.post(
    '/api/ProposedPurchases/GetProposedPurchasesCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_ListProposePurchases;
