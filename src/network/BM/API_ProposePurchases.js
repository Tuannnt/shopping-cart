import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_ProposePurchases = {instance: getInstance()};

//ProposedPurchasesPro
API_ProposePurchases.ProposedPurchasesPro_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/Purchases/JTable',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//ProposedPurchases
API_ProposePurchases.ProposedPurchases_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/ProposedPurchases_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Get Number Auto
API_ProposePurchases.GetNumberAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/GetNumberAuto',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_ProposePurchases.ProposedPurchaseDetails_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/ProposedPurchaseDetails_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_ProposePurchases.ProposedPurchases_GetItem = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/ProposedPurchases_GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_ProposePurchases.DeleteProposedPurchaseById = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    `/api/ProposedPurchases/IsDeleted/${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};

API_ProposePurchases.ProposedPurchaseDetails_GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/ProposedPurchaseDetails_GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//WorkFlow
API_ProposePurchases.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_ProposePurchases.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_ProposePurchases.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//yêu cầu sửa hủy
API_ProposePurchases.GetCancelReasons = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.get(
    '/api/ProposedPurchases/GetCancelReasons',
    null,
    {
      headers: {...headers},
    },
  );
};
API_ProposePurchases.CancelRequest = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/CancelRequest',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_ProposePurchases.GetProposedPurchasesCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProposePurchases.instance.post(
    '/api/ProposedPurchases/GetProposedPurchasesCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_ProposePurchases;
