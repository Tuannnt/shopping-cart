import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_POPaymentsBefore = {instance: getInstance()};

//POPaymentsBefore
API_POPaymentsBefore.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//POPaymentsBeforeBM

API_POPaymentsBefore.ListAllBM = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsBefore.ListDetails = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/ListDetails',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsBefore.GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_POPaymentsBefore.GetItemDetails = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/GetItemDetails/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//WorkFlow
API_POPaymentsBefore.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_POPaymentsBefore.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsBefore.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_POPaymentsBefore.GetPOPaymentsBeforeCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBefore.instance.post(
    '/api/POPaymentsBefore/GetPOPaymentsBeforeCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_POPaymentsBefore;
