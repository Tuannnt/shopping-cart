import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import { addAccessToken } from '../interceptors/accessToken';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_ProFollow,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected,
    );
    return instance;
};
const API_Follow = { instance: getInstance() };
API_Follow.GetDatatimes = data => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Follow.instance.post('/TRobo/GetDatatimes', JSON.stringify(data),
        {
            headers: { ...headers },
        },
    );
};
API_Follow.GetEventStates = data => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Follow.instance.post('/TRobo/GetEventStates', JSON.stringify(data),
        {
            headers: { ...headers },
        },
    );
};
API_Follow.GetMachines = data => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Follow.instance.post('/TRobo/GetMachines', JSON.stringify(data),
        {
            headers: { ...headers },
        },
    );
};
API_Follow.GetMachinetypes = data => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Follow.instance.post('/TRobo/GetMachinetypes', JSON.stringify(data),
        {
            headers: { ...headers },
        },
    );
};
API_Follow.GetEventStates = data => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Follow.instance.post('/TRobo/GetEventStates', JSON.stringify(data),
        {
            headers: { ...headers },
        },
    );
};
API_Follow.GetStates = data => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Follow.instance.post('/TRobo/GetStates', JSON.stringify(data),
        {
            headers: { ...headers },
        },
    );
};
API_Follow.GetDataToSend = data => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Follow.instance.get('/TRobo/GetDataToSend', JSON.stringify(data),
        {
            headers: { ...headers },
        },
    );
};
export default API_Follow;
