import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import {addAccessToken} from '../interceptors/accessToken';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_PRO = {instance: getInstance()};
// //danh sách kết quả bảo dưỡng
// API_PRO.getAllMaintenanceRequest = (obj) => {
//     const headers = {
//         'Content-Type': 'application/json',
//         Authorization: 'Bearer ' + AccessTokenManager.accessToken,
//     };
//     //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
//     return API_PRO.instance.post('api/MaintenanceRequest/GetAll', JSON.stringify(obj), {
//         headers: {...headers},
//     });
// };
// tiecnq
API_PRO.ProductionOrdersGetAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken)
  return API_PRO.instance.post(
    '/api/ProductionOrders/GetAllPaging',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

export default API_PRO;
