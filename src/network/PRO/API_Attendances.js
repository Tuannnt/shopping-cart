import axios from 'axios';
import qs from 'qs';
import { mock, wrap } from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import API_AM from "../AM/API_AM";
import AccessTokenManager from "../../bussiness/AccessTokenManager";


const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}

const API_HR = { instance: getInstance() };

API_HR.hasError = (data) => {
    return !(data && data.code === 1);
};

API_HR.login = (username, password) => {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };
    const data = {
        Email: username,
        Password: password,
    };

    return API.instance.post('/accounts/login', qs.stringify(data), {
        headers: { ...headers },
    });
};

API_HR.logout = () => {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    return API_HR.instance.post('/accounts/logout', null, {
        headers: { ...headers },
    });
};

API_HR.addGroup = (name, description) => {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };
    const data = {
        name: name,
        description: description,
    };

    return API_HR.instance.post('/groups/add', qs.stringify(data), {
        headers: { ...headers },
    });
};

API_HR.updateGroup = (name, description, groupId) => {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };
    const data = {
        name: name,
        description: description,
    };

    return API_HR.instance.post(`/groups/${groupId}`, qs.stringify(data), {
        headers: { ...headers },
    });
};

API_HR.deleteGroup = (groupId) => {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    return API_HR.instance.post(`/groups/${groupId}/delete`, null, {
        headers: { ...headers },
    });
};

API_HR.getProfile = () => {
    return API_HR.instance.get('/accounts/getUser');
};

API_HR.getDevices = () => {
    return API_HR.instance.get('/devices');
};

API_HR.getGroups = () => {
    return API_HR.instance.get('/groups');
};

API_HR.getConfig = () => {
    const key = 'config';
    return mock(key).then(data => {
        if (!data) return wrap(key, API_HR.instance.get('/config'));
        return Promise.resolve(data);
    });
};
// ViewSalary
API_HR.getViewSalary = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_HR.instance.post('api/Hr/ViewSalary/ViewSalary_Items/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
// ViewUniforms
API_HR.getViewUniforms = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_HR.instance.post('api/Hr/Uniforms/UniformDetails_Items/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};

//-----Start----Hàm gọi bảng Attendances
API_HR.getAllEmployeePage = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_HR.instance.post('api/Hr/Employees/GetAllEmployee', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_HR.GetEmployeeById = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_HR.instance.post('api/Hr/Employees/GetEmployeeById', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_HR.DeleteEmployeeById = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_HR.instance.post('api/Hr/Employees/DeleteEmployeeById', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_HR.GetPic = (obj) => {
    //console.log('555 :' + obj.ImagePath + obj.EmployeeGuid)
    if (obj.PhotoTitle != null && obj.PhotoTitle != "") {
        let urlAvata = configApp.url_api_mb + '/api/Hr/Employees/GetPic?id=' + obj.EmployeeGuid;
        return { uri: urlAvata };
    }
    else{
        return require('@images/user.png');
    }
};
//-----End----Hàm gọi bảng Attendances

export default API_HR;
