import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import API_AM from '../AM/API_AM';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API_ApplyOutsides = {instance: getInstance()};

//trongtq@esvn.com.vn
//ApplyOutsides
//list danh sách
API_ApplyOutsides.ApplyOutsides_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(
    'token=======================>>>>>>>' + AccessTokenManager.accessToken,
  );
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/ApplyOutsides_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//list danh sách đi công tác của tôi
API_ApplyOutsides.ApplyOutsidesByMe_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(
    'token=======================>>>>>>>' + AccessTokenManager.accessToken,
  );
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/ApplyOutsidesByMe_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Xem chi tiết
API_ApplyOutsides.ApplyOutsides_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/ApplyOutsides_Items',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Duyệt phiếu
API_ApplyOutsides.approveApplyOutsides = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_ApplyOutsides.noapproveApplyOutsides = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//check người đăng nhập(kiểm tra bước đầu tiên thì không có trả lại)
API_ApplyOutsides.ApplyOutsides_checkin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//insert
API_ApplyOutsides.ApplyOutsides_Submit = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/ApplyOutsides_Submit',
    data,
    {
      headers: {...headers},
    },
  );
};
//Update
API_ApplyOutsides.ApplyOutsides_Update = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/ApplyOutsides_Update',
    data,
    {
      headers: {...headers},
    },
  );
};
//Delete
API_ApplyOutsides.ApplyOutsides_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOutsides.instance.post(
    '/api/ApplyOutsides/ApplyOutsides_Delete/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin chi tiết
API_ApplyOutsides.GetApplyOutsideDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOutsides.instance.get(
    '/api/ApplyOutsides/GetApplyOutsideDetailsById/' + data,
    {
      headers: {...headers},
    },
  );
};
// order
API_ApplyOutsides.getAllOrder = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOutsides.instance.get('/api/ApplyOutsides/GetVehicle/', {
    headers: {...headers},
  });
};
// order
API_ApplyOutsides.GetEmployeeByDepartment = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOutsides.instance.get(
    '/api/ApplyOutsides/GetEmployeeByDepartment/',
    {
      headers: {...headers},
    },
  );
};
//-------------------------End ApplyOutsides
//trongtq@esvn.com.vn
export default API_ApplyOutsides;
