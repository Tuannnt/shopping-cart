import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import API_AM from '../AM/API_AM';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_TimeKeepingsComfirm = {instance: getInstance()};

API_TimeKeepingsComfirm.getAllTimeKeepingsConfirm = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    'api/TimeKeepingsConfirm/TimeKeepingsConfirm_ListAll',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TimeKeepingsComfirm.getEmployeeJexcel = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    'api/TimeKeepingsConfirm/getEmployeeJexcel',
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
API_TimeKeepingsComfirm.GetEmployees = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    'api/TimeKeepingsConfirm/GetEmployees',
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
API_TimeKeepingsComfirm.CheckLoginTimeKeeping = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    'api/TimeKeepingsConfirm/CheckLogin',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TimeKeepingsComfirm.InsertResignationForms = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    'api/TimeKeepingsConfirm/Insert',
    data,
    {
      headers: {...headers},
    },
  );
};
API_TimeKeepingsComfirm.Insert_TimeKeepings = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    'api/TimeKeepingsConfirm/Insert_TimeKeepings',
    data,
    {
      headers: {...headers},
    },
  );
};
API_TimeKeepingsComfirm.Update = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    'api/TimeKeepingsConfirm/Update',
    data,
    {
      headers: {...headers},
    },
  );
};
API_TimeKeepingsComfirm.GetTimekeeping = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    'api/TimeKeepingsConfirm/GetTimekeeping',
    data,
    {
      headers: {...headers},
    },
  );
};
API_TimeKeepingsComfirm.TimeKeepingsConfirm_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TimeKeepingsComfirm.instance.post(
    '/api/TimeKeepingsConfirm/TimeKeepingsConfirm_Items',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_TimeKeepingsComfirm;
