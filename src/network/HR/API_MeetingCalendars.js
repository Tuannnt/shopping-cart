import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_MeetingCalendars = { instance: getInstance() };


API_MeetingCalendars.GetCarlenderCar = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_MeetingCalendars.instance.get('/api/MeetingCalendars/GetCarlenderCar', {
        headers: { ...headers },
    });
};
API_MeetingCalendars.GetItem = id => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_MeetingCalendars.instance.post(`api/MeetingCalendars/GetItem?id=${id}`, JSON.stringify({}), {
        headers: { ...headers },
    });
};
API_MeetingCalendars.Delete = id => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_MeetingCalendars.instance.post(`api/MeetingCalendars/Delete?id=${id}`, JSON.stringify({}), {
        headers: { ...headers },
    });
};

API_MeetingCalendars.Insert = obj => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_MeetingCalendars.instance.post(
        '/api/MeetingCalendars/Insert',
        obj,
        {
            headers: { ...headers },
        },
    );
};
API_MeetingCalendars.Update = data => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken);
    return API_MeetingCalendars.instance.post('/api/MeetingCalendars/Update', data, {
        headers: { ...headers },
    });
};
API_MeetingCalendars.GetAll = obj => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_MeetingCalendars.instance.post('/api/MeetingCalendars/GetAll', JSON.stringify(obj), {
        headers: { ...headers },
    });
};

export default API_MeetingCalendars;