import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_TrainingRequests = {instance: getInstance()};

//ProposedPurchases
API_TrainingRequests.ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TrainingRequests.instance.post('/api/TrainingRequests/ListAll', JSON.stringify(data), {
        headers: {...headers},
    });
};

API_TrainingRequests.ListDetail = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TrainingRequests.instance.post('/api/TrainingRequests/ListDetails', JSON.stringify(data), {
        headers: {...headers},
    });
};
API_TrainingRequests.GetItem = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TrainingRequests.instance.post('/api/TrainingRequests/GetItems/' +id,{}, {
        headers: {...headers},
    });
};

//WorkFlow
API_TrainingRequests.Approve =(obj)=>{
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TrainingRequests.instance.post('/api/TrainingRequests/Approve', JSON.stringify(obj), {
        headers: {...headers},
    });
}

//Trả lại
API_TrainingRequests.NotApprove =(obj)=>{
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TrainingRequests.instance.post('/api/TrainingRequests/NotApprove', JSON.stringify(obj), {
        headers: {...headers},
    });
}
API_TrainingRequests.CheckLogin = (obj)=>{
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TrainingRequests.instance.post('/api/TrainingRequests/CheckLogin', JSON.stringify(obj), {
        headers: {...headers},
    });
}

//đếm số
API_TrainingRequests.Count = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TrainingRequests.instance.post('/api/TrainingRequests/Count', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
export default API_TrainingRequests;