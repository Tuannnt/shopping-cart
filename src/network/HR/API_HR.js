import axios from 'axios';
import qs from 'qs';
import { mock, wrap } from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import API_AM from '../AM/API_AM';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API_HR = { instance: getInstance() };

API_HR.hasError = data => {
  return !(data && data.code === 1);
};

API_HR.login = (username, password) => {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
  };
  const data = {
    Email: username,
    Password: password,
  };

  return API.instance.post('/accounts/login', qs.stringify(data), {
    headers: { ...headers },
  });
};

API_HR.logout = () => {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
  };

  return API_HR.instance.post('/accounts/logout', null, {
    headers: { ...headers },
  });
};

API_HR.addGroup = (name, description) => {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
  };
  const data = {
    name: name,
    description: description,
  };

  return API_HR.instance.post('/groups/add', qs.stringify(data), {
    headers: { ...headers },
  });
};

API_HR.updateGroup = (name, description, groupId) => {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
  };
  const data = {
    name: name,
    description: description,
  };

  return API_HR.instance.post(`/groups/${groupId}`, qs.stringify(data), {
    headers: { ...headers },
  });
};

API_HR.deleteGroup = groupId => {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
  };

  return API_HR.instance.post(`/groups/${groupId}/delete`, null, {
    headers: { ...headers },
  });
};

API_HR.getProfile = () => {
  return API_HR.instance.get('/accounts/getUser');
};

API_HR.getDevices = () => {
  return API_HR.instance.get('/devices');
};

API_HR.getGroups = () => {
  return API_HR.instance.get('/groups');
};

API_HR.getConfig = () => {
  const key = 'config';
  return mock(key).then(data => {
    if (!data) {
      return wrap(key, API_HR.instance.get('/config'));
    }
    return Promise.resolve(data);
  });
};
// ViewSalary
API_HR.getViewSalary = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ViewSalary/ViewSalary_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// ViewUniforms
API_HR.getViewUniforms = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Uniforms/UniformDetails_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// ViewReward
API_HR.getDiscipline = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/Rewards/Discipline_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// ViewReward
API_HR.getRewards = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/Rewards/Rewards_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// ViewInsurances
API_HR.getInsurancesIsActive = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ViewInsuaran/ViewInsuaranActive_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// ViewInsurances
API_HR.getViewInsuaranHistory = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ViewInsuaran/ViewInsuaranHistory_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.getAllEmployees = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/Uniforms/GetAllEmployee',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//-----Start----Hàm gọi bảng kế hoạch tuyển dụng
API_HR.getAllRecruitmentPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    'api/Hr/RecruitmentPlans/JTable',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.getRecruitmentPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentPlans/RecruitmentPlans_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

API_HR.CheckLoginRecruitmentPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentPlans/CheckLogin',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.approveRecruitmentPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentPlans/Approve',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.notapproveRecruitmentPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentPlans/NotApprove',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//DELETE
API_HR.deleteRecruitmentPlans = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/hr/RecruitmentPlans/RecruitmentPlans_Delete/' + id,
    {},
    {
      headers: { ...headers },
    },
  );
};
//Lịch tuyển dụng
API_HR.getAllRecruitmentSchedules = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    'api/Hr/RecruitmentSchedules/Jtable',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

//DELETE
API_HR.deleteRecruitmentSchedules = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/hr/RecruitmentSchedules/RecruitmentSchedules_Delete/' + id,
    {},
    {
      headers: { ...headers },
    },
  );
};

API_HR.getRecruitmentSchedules = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentSchedules/RecruitmentSchedules_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//Kết quả tuyển dụng
API_HR.getAllTrainingResults = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    'api/Hr/TrainingResults/TrainingResults_ListAll',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

//DELETE
API_HR.deleteTrainingResults = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/hr/TrainingResults/TrainingResults_Delete/' + id,
    {},
    {
      headers: { ...headers },
    },
  );
};

API_HR.getTrainingResults = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/TrainingResults/TrainingResults_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

API_HR.approveTrainingResults = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/TrainingResults/Approve',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

API_HR.CheckStart = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/TrainingResults/CheckStart',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

//Yêu cầu tuyển dụng
API_HR.getAllRecruitmentRequests = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    'api/Hr/RecruitmentRequests/JTable',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

API_HR.getRecruitmentRequests = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentRequests/RecruitmentRequests_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

//DELETE
API_HR.deleteRecruitmentRequests = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/hr/RecruitmentRequests/RecruitmentRequests_Delete/' + id,
    {},
    {
      headers: { ...headers },
    },
  );
};

API_HR.CheckLoginRecruitmentRequests = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentRequests/CheckLogin',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.approveRecruitmentRequests = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentRequests/Approve',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.notapproveRecruitmentRequests = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RecruitmentRequests/NotApprove',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// Xác nhận chấm công
API_HR.getAllTimeKeepingsConfirm = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    'api/TimeKeepingsConfirm/TimeKeepingsConfirm_ListAll',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//Xem chi tiết xác nhận chấm công
API_HR.TimeKeepingsConfirm_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/TimeKeepingsConfirm/TimeKeepingsConfirm_Items',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// xem chi tiết
API_HR.TimeKeepingsConfirm_ListDetail = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/TimeKeepingsConfirm/getTimeKeepings_by_TimeConfirmGuid/' + id,
    JSON.stringify({}),
    {
      headers: { ...headers },
    },
  );
};
//duyệt xác nhận chấm công
API_HR.Approve_TimeKeepingsConfirm = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    'api/TimeKeepingsConfirm/Approve_TimeKeepingsConfirm',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// xoa
API_HR.TimeKeeping_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    `/api/TimeKeepingsConfirm/IsDelete/${id}`,
    JSON.stringify({}),
    {
      headers: { ...headers },
    },
  );
};
//trả lại xác nhận chấm công
API_HR.NotApprove_TimeKeepingsConfirm = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    'api/TimeKeepingsConfirm/NotApprove_TimeKeepingsConfirm',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.CheckTotalApp = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/ApplyLeaves/CheckTotalApp',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.CheckApplyLeaveInYear = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/ApplyLeaves/CheckApplyLeaveInYear',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// Bàn giao
API_HR.UpdateApplyLeavesTask = obj => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post('api/Hr/ApplyLeaves/UpdateTask', obj, {
    headers: { ...headers },
  });
};
// Đăng ký tài khoản
API_HR.SignUp = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post('/api/Users/SignUp', JSON.stringify(obj), {
    headers: { ...headers },
  });
};
// Đăng nhập tài khoản ảo
API_HR.SignIn = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post('/api/Users/SignIn', JSON.stringify(obj), {
    headers: { ...headers },
  });
};
//-----Start----Hàm gọi bảng đăng ký nghỉ việc
//Update eat
API_HR.ResignationForms_Update = data => {
  const headers = {
    'Content-Type': 'form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post('/api/ResignationForms/Update', data, {
    headers: { ...headers },
  });
};
//DELETE
API_HR.deleteResignationForms = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/ResignationForms/ResignationForms_Delete/' + id,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_HR.getAllResignationForms = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    'api/ResignationForms/ResignationForms_ListAll',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.ResignationForms_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/ResignationForms/ResignationForms_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.CheckLoginResignationForms = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/ResignationForms/CheckLogin',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.approveResignationForms = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/ResignationForms/Approve',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.notapproveResignationForms = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/ResignationForms/NotApprove',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//insert
API_HR.InsertResignationForms = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post('api/ResignationForms/InsertWorkFolow', data, {
    headers: { ...headers },
  });
};
//Get Reason
API_HR.GetReason = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.get('api/ResignationForms/GetReason', {
    headers: { ...headers },
  });
};
//end RegisterEats_Delete/{id}
//-----Start----Hàm gọi bảng đăng ký ăn
//UPDATE
//Update eat
API_HR.RegisterEat_Update = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post('/api/Hr/RegisterEats/Update', data, {
    headers: { ...headers },
  });
};
//DELETE
API_HR.deleteRegisterEat = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/RegisterEats/RegisterEats_Delete/' + id,
    {},
    {
      headers: { ...headers },
    },
  );
};

API_HR.CheckLoginEat = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RegisterEats/CheckLogin',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.approveRegisterEats = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RegisterEats/Approve',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.notapproveRegisterEats = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RegisterEats/NotApprove',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.getAllCommentPageRegisterEat = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/RegisterEats/GetAllCommentPage',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_HR.GetAllLoginWf = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/Hr/RegisterEats/GetAllLoginWf',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetUsersNotification = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/Hr/RegisterEats/GetUsersNotification',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_HR.getAllRegisterEatPage = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/Hr/RegisterEats/JTable',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_HR.getItemRegisterEat = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/RegisterEats/RegisterEats_Items/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetItemByDepEatsGuid = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/RegisterEats/GetItemByDepEatsGuid/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//update
API_HR.RegisterEats_Update = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post('/api/Hr/RegisterEats/Update', data, {
    headers: { ...headers },
  });
};
//get ca
API_HR.GetAllShift = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.get('api/Hr/RegisterEats/Status_RegisterEatDetail', {
    headers: { ...headers },
  });
};
API_HR.GetDepEatsGuid = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.get('api/Hr/RegisterEats/GetDepEatsGuid', {
    headers: { ...headers },
  });
};
API_HR.GetAllShiftEat = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.get('api/Hr/RegisterEats/GetShift', {
    headers: { ...headers },
  });
};
API_HR.GetEmp = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.get('api/Hr/RegisterEats/GetEmp', {
    headers: { ...headers },
  });
};
//insert
API_HR.InsertRegisterEats = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post('/api/Hr/RegisterEats/InsertNoEat', data, {
    headers: { ...headers },
  });
};

//-----End----Hàm gọi bảng đưang ký ăn

//-----Start----Hàm gọi bảng Employee
API_HR.getAllEmployeePage = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/Employees/GetAllEmployee',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetEmployeeById = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/Employees/GetEmployeeById',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.DeleteEmployeeById = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/Employees/DeleteEmployeeById',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetPic = obj => {
  //console.log('555 :' + obj.ImagePath + obj.EmployeeGuid)
  if (obj.PhotoTitle != null && obj.PhotoTitle != '') {
    let urlAvata =
      configApp.url_api_mb + '/api/Hr/Employees/GetPic?id=' + obj.EmployeeGuid;
    return { uri: urlAvata };
  } else {
    return require('@images/user.png');
  }
};
//-----End----Hàm gọi bảng Employee

//-----Start----Hàm gọi bảng ApplyLeaves
API_HR.getAllApplyLeavesPage = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/GetAllApplyLeaves',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// phiếu nghỉ phép của tôi
API_HR.getAllApplyLeavesByMe = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/GetAllApplyLeavesByMe',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetApplyLeavesById = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/GetApplyLeavesById',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.DeleteApplyLeavesById = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/DeleteApplyLeavesById',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetPicApplyLeaves = data => {
  let urlAvata =
    'http://gmt-apicm.easternsun.vn/api/Common/Employee/GetImage/' + data;
  if (urlAvata != null && urlAvata != '' && data != undefined && data != null) {
    return { uri: urlAvata };
  } else {
    return require('@images/user.png');
  }
};
// chi tiết đếm lịch sử nghỉ
API_HR.CountApplyLeavesInYear = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/CountApply',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.getAllCommentPage = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/GetAllCommentPage',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.approveApplyLeaves = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/Approve',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.NotApproveApplyLeaves = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/NotApprove',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/CheckLogin',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.InsertCommentApplyLeaves = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/InsertComments',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetEmployees = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.get('/api/Hr/ApplyLeaves/GetEmployees', {
    headers: { ...headers },
  });
};

API_HR.InsertApplyLeaves = obj => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post('api/Hr/ApplyLeaves/InsertWorkFolow', obj, {
    headers: { ...headers },
  });
};
API_HR.InserSendtWorkFolow = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/ApplyLeaves/InserSendtWorkFolow',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

API_HR.UpdateApplyLeaves = obj => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post('api/Hr/ApplyLeaves/Update', obj, {
    headers: { ...headers },
  });
};
//Jtable hợp đồng
API_HR.Contracts_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Contracts/Contracts_ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Jtable bảo hiểm
API_HR.Insurances_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Insurances/Insurances_ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Jtable Lương
API_HR.Salaries_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Salaries/Salaries_ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Jtable công tác
API_HR.Histories_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Histories/Histories_ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Jtable khen thưởng kỉ luật
API_HR.Rewards_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/RewardsMB/Rewards_ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Jtable chứng chỉ chứng nhận
API_HR.Certificates_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Certificates/Certificates_ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Jtable giảm trừ gia cảnh
API_HR.FamilyDeductions_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/FamilyDeductions/FamilyDeductions_ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Xem chi tiết hợp đồng
API_HR.Contracts_GetItem = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Contracts/Contracts_GetItem/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//Xem chi tiết bảo hiểm
API_HR.Insurances_GetItem = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Insurances/Insurances_GetItem/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//Xem chi tiết lương
API_HR.Salaries_GetItem = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Salaries/Salaries_GetItem/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//Xem chi tiết công tác
API_HR.Histories_GetItem = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Histories/Histories_GetItem/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//Xem chi tiết khen thưởng kỷ luật
API_HR.Rewards_GetItem = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/RewardsMB/Rewards_GetItem/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//Xem chi tiết chứng chỉ chứng nhận
API_HR.Certificates_GetItem = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Certificates/Certificates_GetItem/',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// xem phụ cấp khác
API_HR.EmployeeAllowances_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_HR.instance.post(
    '/api/Salaries/EmployeeAllowances_ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

//-----Start----Hàm gọi bảng chấm công TimeKeepings
API_HR.getAllTimeKeepingsPage = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/TimeKeepings/GetAllTimeKeepings',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// list bảng công trong tháng
API_HR.viewTimeKeeperListAll = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/ViewTimeKeeper/ViewTimeKeeper_Jtable',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetTimeKeepingsById = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/TimeKeepings/GetTimeKeepingsById',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.GetTotalLeaveInYear = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/TimeKeepings/GetTotalLeaveInYear',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};

//-----End----Hàm gọi bảng ApplyLeaves

// Xác nhận chấm công
//Get ca làm việc
API_HR.GetShifts = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.get('/api/Hr/Attendances/GetShift', {
    headers: { ...headers },
  });
};
// nguoi support
API_HR.GetSupport = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.get('/api/Hr/ApplyLeaves/GetSupport', {
    headers: { ...headers },
  });
};
// Ds Nhân viên khối sản xuất
API_HR.getAllEmployeeSX = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/Attendances/GetAllAttendances',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
// thêm/sửa chấm công
API_HR.AddEmployeeSX = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    'api/Hr/Attendances/CheckAttendances',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//thanhtt
API_HR.CountEmployees = () => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/Hr/Employees/CountEmployees',
    JSON.stringify({}),
    {
      headers: { ...headers },
    },
  );
};
//lấy dữ liệu
//danh sách kết quả bảo dưỡng
API_HR.MaintenanceRequest_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/MaintenanceRequest/MaintenanceRequest_Items',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//lấy dữ liệu
//danh sách kết quả bảo dưỡng chi tiết
API_HR.GetItemListDetail = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/MaintenanceRequest/GetItemListDetail',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//danh sách kết quả bảo dưỡng
API_HR.getAllMaintenanceRequest = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/MaintenanceRequest/GetAll',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//duyệt bảo dưỡng
API_HR.ProblemResultApply = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/MaintenanceRequest/ProblemResultApply',
    data,
    {
      headers: { ...headers },
    },
  );
};
//duyệt sản xuất
API_HR.ProblemResultApplyofSX = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/MaintenanceRequest/ProblemResultApplyofSX',
    data,
    {
      headers: { ...headers },
    },
  );
};
// đếm phiếu bảo dưỡng
API_HR.CountMaintenanceRequest = () => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/MaintenanceRequest/CountMaintenanceRequest',
    JSON.stringify({}),
    {
      headers: { ...headers },
    },
  );
};
// đếm phiếu bảo dưỡng
API_HR.GetUnitMeasures = () => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/MaintenanceRequest/GetUnitMeasures',
    JSON.stringify({}),
    {
      headers: { ...headers },
    },
  );
};
// update chi tiết bảo dưỡng
API_HR.UpdateMB = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post('/api/MaintenanceRequest/Update', data, {
    headers: { ...headers },
  });
};
// Lấy danh sách kế hoạch năm của bảo dưỡng
API_HR.getAllMaintenanceManagement = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/MaintenanceManagement/GetAll',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.getItemMaintenanceManagement = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/MaintenanceManagement/GetItem',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
API_HR.getDetailMaintenanceManagement = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  //stringify : chuyển đổi từ đối tượng sang kiểu chuỗi
  return API_HR.instance.post(
    '/api/MaintenanceManagement/GetDetail',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//#region trongtq@esvn.com.vn
API_HR.UniformsDetail_JTable = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_HR.instance.post(
    '/api/hr/Uniforms/UniformsDetail_JTable',
    JSON.stringify(obj),
    {
      headers: { ...headers },
    },
  );
};
//#endregion
export default API_HR;
