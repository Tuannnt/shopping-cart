//TinhLV
import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API_CompensationSlip = {instance: getInstance()};
//Jtable
API_CompensationSlip.CompensationSlip_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/CompensationSlip/Jtable',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Xem chi tiết
API_CompensationSlip.Regitercar_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/CompensationSlip/Regitercar_Items/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_CompensationSlip.ComboBoxDepartment = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/CompensationSlip/ComboBoxDepartment/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_CompensationSlip.ComboBoxType = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/CompensationSlip/ComboBoxType/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_CompensationSlip.ComboBoxVehicle = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/CompensationSlip/ComboBoxVehicle/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_CompensationSlip.ComboBoxJexcelEmployee = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/CompensationSlip/ComboBoxJexcelEmployee/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_CompensationSlip.GetByListReplate = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    `/api/CompensationSlip/GetByListReplate/${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
//duyệt
API_CompensationSlip.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/CompensationSlip/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//trả lại
API_CompensationSlip.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/CompensationSlip/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//comment
API_CompensationSlip.GetCommentCompensationSlip = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/CompensationSlip/GetAllCommentPage',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//insert
API_CompensationSlip.InsertCompensationSlip = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/CompensationSlip/Insert',
    data,
    {
      headers: {...headers},
    },
  );
};
//Update
API_CompensationSlip.CompensationSlip_Update = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/CompensationSlip/Update',
    data,
    {
      headers: {...headers},
    },
  );
};
//Get Cars
API_CompensationSlip.GetCars = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.get('api/CompensationSlip/GetCars', {
    headers: {...headers},
  });
};
API_CompensationSlip.GetBanksDetail = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.get('api/Advances/GetBanksDetail', {
    headers: {...headers},
  });
};

API_CompensationSlip.GetInfoEmp = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.get('api/Advances/GetInfoEmp/' + id, {
    headers: {...headers},
  });
};
API_CompensationSlip.GetEmPloyeesDetail = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/Advances/GetEmPloyeesDetail',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_CompensationSlip.GetEmp = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.get(
    'api/CompensationSlip/GetEmPloyees',
    {
      headers: {...headers},
    },
  );
};
//check login
API_CompensationSlip.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/CompensationSlip/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Delete
API_CompensationSlip.CompensationSlip_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/CompensationSlip/CompensationSlip_Delete/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//Jtable Advances
API_CompensationSlip.Advances_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/Advances/Advances_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Get Employee
API_CompensationSlip.GetEmpAdvances = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.get('api/Advances/GetEmpAdvances', {
    headers: {...headers},
  });
};
//insert Advances
API_CompensationSlip.Advances_InsertAPI = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post('api/Advances/Insert', data, {
    headers: {...headers},
  });
};
API_CompensationSlip.CompensationSlip_Items = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/CompensationSlip/CompensationSlip_Items',
    JSON.stringify(id),
    {
      headers: {...headers},
    },
  );
};
//Aprove
API_CompensationSlip.Approve_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/Advances/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//NotAprove
API_CompensationSlip.NotApprove_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/Advances/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Update advance
API_CompensationSlip.Advances_Update = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/Advances/Advances_Update',
    data,
    {
      headers: {...headers},
    },
  );
};
//check login
API_CompensationSlip.CheckLogin_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/Advances/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Delete Advances
API_CompensationSlip.Advances_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/Advances/Advances_Delete/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};
//thông báo Advances
API_CompensationSlip.GetUsersNotification = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/Advances/GetUsersNotification',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//thông báo Advances
API_CompensationSlip.GetAllLoginWf = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/Advances/GetAllLoginWf',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Jtable Candidates
API_CompensationSlip.Candidates_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/CandiDates/Candidates_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Xem chi tiết Candidates
API_CompensationSlip.Candidatdes_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/CandiDates/Candidatdes_Items/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//check login
API_CompensationSlip.CheckLogin_Candidates = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    'api/CandiDates/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Approver Candidates
API_CompensationSlip.Approve_Candidates = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/CandiDates/Approve_Candidates',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//NotAprove
API_CompensationSlip.NotApprove_Candidates = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/CandiDates/NotApprove_Candidates',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Jtable TrainingPlans
API_CompensationSlip.TrainingPlans_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/TrainingPlans/TrainingPlans_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Them moi ke hoach dao tao
API_CompensationSlip.TrainingPlans_InsertAPI = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post('api/TrainingPlans/Insert', data, {
    headers: {...headers},
  });
};
//Chinh sua ke hoach dao tao
API_CompensationSlip.TrainingPlans_UpdateAPI = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post('api/TrainingPlans/Update', data, {
    headers: {...headers},
  });
};
//Xem chi tiết kế hoạch đào tạo
API_CompensationSlip.TrainingPlans_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/TrainingPlans/TrainingPlans_Items',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//duyệt kế hoạch đào tạo
API_CompensationSlip.Approve_TrainingPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/TrainingPlans/Approve_TrainingPlans',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//trả lại kế hoạch đào tạo
API_CompensationSlip.NotApprove_TrainingPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    'api/TrainingPlans/NotApprove_TrainingPlans',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Danh sách chi tiết kế hoạch đào tạo
API_CompensationSlip.TrainingPlanDetails_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    '/api/TrainingPlans/TrainingPlanDetails_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Danh sách chi tiết tạm ứng
API_CompensationSlip.Advances_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    '/api/Advances/Advances_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin chi tiết kế hoạch đào tạo
API_CompensationSlip.GetTrainingPlanDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.get(
    '/api/TrainingPlans/GetTrainingPlanDetailsById/' + data,
    {
      headers: {...headers},
    },
  );
};
API_CompensationSlip.CountDashboard = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CompensationSlip.instance.post(
    '/api/HRDashBoad/CountDashboard',
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
API_CompensationSlip.GetItemDashboad = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_CompensationSlip.instance.post(
    '/api/HRDashBoad/GetItemDashboad',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_CompensationSlip;
