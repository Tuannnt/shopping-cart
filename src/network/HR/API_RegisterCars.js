//TinhLV
import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API_RegisterCars = {instance: getInstance()};
//Jtable
API_RegisterCars.Registercar_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    '/api/RegisterCars/Registercar_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Xem chi tiết
API_RegisterCars.Regitercar_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    `/api/RegisterCars/Regitercar_Items/`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_RegisterCars.GetByListReplate = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    `/api/RegisterCars/GetByListReplate/${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
//duyệt
API_RegisterCars.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    'api/RegisterCars/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//trả lại
API_RegisterCars.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    'api/RegisterCars/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//comment
API_RegisterCars.GetCommentRegisterCars = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/RegisterCars/GetAllCommentPage',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//insert
API_RegisterCars.InsertRegisterCars = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/RegisterCars/Registercars_InsertAPI',
    data,
    {
      headers: {...headers},
    },
  );
};
//Update
API_RegisterCars.Registercars_Update = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    'api/RegisterCars/Registercars_Update',
    data,
    {
      headers: {...headers},
    },
  );
};
//Get Cars
API_RegisterCars.GetCars = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.get('api/RegisterCars/GetCars', {
    headers: {...headers},
  });
};
API_RegisterCars.GetBanksDetail = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.get('api/Advances/GetBanksDetail', {
    headers: {...headers},
  });
};

API_RegisterCars.GetInfoEmp = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.get('api/Advances/GetInfoEmp/' + id, {
    headers: {...headers},
  });
};
API_RegisterCars.GetEmPloyeesDetail = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/Advances/GetEmPloyeesDetail',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_RegisterCars.GetEmp = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.get('api/RegisterCars/GetEmPloyees', {
    headers: {...headers},
  });
};
//check login
API_RegisterCars.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/RegisterCars/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Delete
API_RegisterCars.Registercars_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/RegisterCars/Registercars_Delete/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//Jtable Advances
API_RegisterCars.Advances_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    '/api/Advances/Advances_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Get Employee
API_RegisterCars.GetEmpAdvances = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.get('api/Advances/GetEmpAdvances', {
    headers: {...headers},
  });
};
//insert Advances
API_RegisterCars.Advances_InsertAPI = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post('api/Advances/Insert', data, {
    headers: {...headers},
  });
};
//open Advances
API_RegisterCars.Advances_Items = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    `/api/Advances/Advances_Items/${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
//Aprove
API_RegisterCars.Approve_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/Advances/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//NotAprove
API_RegisterCars.NotApprove_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    'api/Advances/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Update advance
API_RegisterCars.Advances_Update = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post('api/Advances/Advances_Update', data, {
    headers: {...headers},
  });
};
//check login
API_RegisterCars.CheckLogin_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/Advances/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Delete Advances
API_RegisterCars.Advances_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/Advances/Advances_Delete/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};
//thông báo Advances
API_RegisterCars.GetUsersNotification = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/Advances/GetUsersNotification',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//thông báo Advances
API_RegisterCars.GetAllLoginWf = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/Advances/GetAllLoginWf',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Jtable Candidates
API_RegisterCars.Candidates_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    '/api/CandiDates/Candidates_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Xem chi tiết Candidates
API_RegisterCars.Candidatdes_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    `/api/CandiDates/Candidatdes_Items/`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//check login
API_RegisterCars.CheckLogin_Candidates = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    'api/CandiDates/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Approver Candidates
API_RegisterCars.Approve_Candidates = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    'api/CandiDates/Approve_Candidates',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//NotAprove
API_RegisterCars.NotApprove_Candidates = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    'api/CandiDates/NotApprove_Candidates',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Jtable TrainingPlans
API_RegisterCars.TrainingPlans_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    '/api/TrainingPlans/TrainingPlans_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Them moi ke hoach dao tao
API_RegisterCars.TrainingPlans_InsertAPI = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post('api/TrainingPlans/Insert', data, {
    headers: {...headers},
  });
};
//Chinh sua ke hoach dao tao
API_RegisterCars.TrainingPlans_UpdateAPI = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post('api/TrainingPlans/Update', data, {
    headers: {...headers},
  });
};
//Xem chi tiết kế hoạch đào tạo
API_RegisterCars.TrainingPlans_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    `/api/TrainingPlans/TrainingPlans_Items`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//duyệt kế hoạch đào tạo
API_RegisterCars.Approve_TrainingPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    'api/TrainingPlans/Approve_TrainingPlans',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//trả lại kế hoạch đào tạo
API_RegisterCars.NotApprove_TrainingPlans = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    'api/TrainingPlans/NotApprove_TrainingPlans',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Danh sách chi tiết kế hoạch đào tạo
API_RegisterCars.TrainingPlanDetails_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    '/api/TrainingPlans/TrainingPlanDetails_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Danh sách chi tiết tạm ứng
API_RegisterCars.Advances_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    '/api/Advances/Advances_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin chi tiết kế hoạch đào tạo
API_RegisterCars.GetTrainingPlanDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.get(
    '/api/TrainingPlans/GetTrainingPlanDetailsById/' + data,
    {
      headers: {...headers},
    },
  );
};
API_RegisterCars.CountDashboard = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_RegisterCars.instance.post(
    '/api/HRDashBoad/CountDashboard',
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
API_RegisterCars.GetItemDashboad = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_RegisterCars.instance.post(
    `/api/HRDashBoad/GetItemDashboad`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_RegisterCars;
