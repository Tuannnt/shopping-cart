import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API_ApplyOverTimes = {instance: getInstance()};

API_ApplyOverTimes.hasError = data => {
  return !(data && data.code === 1);
};
//-------------------------Begin ApplyOverTimes
//list danh sách
API_ApplyOverTimes.ApplyOverTimes_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(
    'token=======================>>>>>>>' + AccessTokenManager.accessToken,
  );
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/ApplyOverTimes_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//list danh sách làm thêm của tôi
API_ApplyOverTimes.ApplyOverTimesByMe_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(
    'token=======================>>>>>>>' + AccessTokenManager.accessToken,
  );
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/ApplyOverTimesOfMe_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

//Xem chi tiết
API_ApplyOverTimes.ApplyOverTimes_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(
    'tokenItem=======================>>>>>>>' + AccessTokenManager.accessToken,
  );
  return API_ApplyOverTimes.instance.post(
    `/api/ApplyOverTimes/ApplyOverTimes_Items`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Duyệt phiếu
API_ApplyOverTimes.ApplyOverTimes_approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_ApplyOverTimes.ApplyOverTimes_noapprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//kiểm tra ngày đăng ký là ngày lễ
API_ApplyOverTimes.ApplyOverTimes_CheckHoliday = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/ApplyOverTimes_CheckHoliday',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//check người đăng nhập(kiểm tra bước đầu tiên thì không có trả lại)
API_ApplyOverTimes.ApplyOverTimes_checkin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//thêm mới
API_ApplyOverTimes.ApplyOverTimes_Submit = obj => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/ApplyOverTimes_Submit',
    obj,
    {
      headers: {...headers},
    },
  );
};
API_ApplyOverTimes.ApplyOverTimes_Edit = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post('/api/ApplyOverTimes/Edit', data, {
    headers: {...headers},
  });
};

//Xem chi tiết
API_ApplyOverTimes.ApplyOverTimes_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/ApplyOverTimes_Delete/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin chi tiết
API_ApplyOverTimes.GetApplyOverTimeDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.get(
    '/api/ApplyOverTimes/GetApplyOverTimeDetailsById/' + data,
    {
      headers: {...headers},
    },
  );
};
API_ApplyOverTimes.Type = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.get('/api/ApplyOverTimes/Type/', {
    headers: {...headers},
  });
};
API_ApplyOverTimes.GetEmployeeByDepartment = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.get(
    '/api/ApplyOverTimes/GetEmployeeByDepartment',
    {
      headers: {...headers},
    },
  );
};
API_ApplyOverTimes.getAllWorkShift = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.get('/api/ApplyOverTimes/GetShifts', {
    headers: {...headers},
  });
};
API_ApplyOverTimes.getAllOrder = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/GetOrders_AM',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_ApplyOverTimes.GetEmp = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.get(
    '/api/ApplyOverTimes/GetEmployeeByDepartment',
    {
      headers: {...headers},
    },
  );
};
// Lấy số phiếu
API_ApplyOverTimes.GetNumberAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ApplyOverTimes.instance.post(
    '/api/ApplyOverTimes/GetNumberAuto',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

//trongtq@esvn.com.vn
export default API_ApplyOverTimes;
