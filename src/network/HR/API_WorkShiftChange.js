import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API_WorkShiftChange = {instance: getInstance()};

API_WorkShiftChange.hasError = data => {
  return !(data && data.code === 1);
};
//list danh sách
API_WorkShiftChange.Jtable = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(
    'token=======================>>>>>>>' + AccessTokenManager.accessToken,
  );
  return API_WorkShiftChange.instance.post(
    '/api/WorkShiftChange/List_All',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};


//Xem chi tiết
API_WorkShiftChange.GetItem = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(
    'tokenItem=======================>>>>>>>' + AccessTokenManager.accessToken,
  );
  return API_WorkShiftChange.instance.get(
    `/api/WorkShiftChange/GetItem/${data}`,
    {
      headers: {...headers},
    },
  );
};

//Xem chi tiết details
API_WorkShiftChange.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(
    'tokenItem=======================>>>>>>>' + AccessTokenManager.accessToken,
  );
  return API_WorkShiftChange.instance.post(
    `/api/ApplyOverTimes/CheckLogin/`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Duyệt phiếu
API_WorkShiftChange.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post(
    '/api/WorkShiftChange/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_WorkShiftChange.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post(
    '/api/WorkShiftChange/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//kiểm tra ngày đăng ký là ngày lễ
API_WorkShiftChange.ApplyOverTimes_CheckHoliday = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post(
    '/api/ApplyOverTimes/ApplyOverTimes_CheckHoliday',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//check người đăng nhập(kiểm tra bước đầu tiên thì không có trả lại)
API_WorkShiftChange.ApplyOverTimes_checkin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post(
    '/api/ApplyOverTimes/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//thêm mới
API_WorkShiftChange.Add = obj => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post(
    '/api/WorkShiftChange/Add',
    obj,
    {
      headers: {...headers},
    },
  );
};
API_WorkShiftChange.Edit = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post('/api/WorkShiftChange/Edit', data, {
    headers: {...headers},
  });
};

//Xem chi tiết
API_WorkShiftChange.ApplyOverTimes_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post(
    '/api/ApplyOverTimes/ApplyOverTimes_Delete/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin chi tiết
API_WorkShiftChange.GetApplyOverTimeDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.get(
    '/api/ApplyOverTimes/GetApplyOverTimeDetailsById/' + data,
    {
      headers: {...headers},
    },
  );
};
API_WorkShiftChange.GetEmployeeByDepartment = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.get(
    '/api/ApplyOverTimes/GetEmployeeByDepartment',
    {
      headers: {...headers},
    },
  );
};
API_WorkShiftChange.getAllWorkShift = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.get('/api/ApplyOverTimes/GetShifts', {
    headers: {...headers},
  });
};
API_WorkShiftChange.Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post(`/api/WorkShiftChange/Delete/${id}`,null, {
    headers: {...headers},
  });
};

API_WorkShiftChange.GetEmp = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.get('/api/ApplyOverTimes/GetEmp', {
    headers: {...headers},
  });
};
// Lấy số phiếu
API_WorkShiftChange.GetNumberAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_WorkShiftChange.instance.post(
    '/api/ApplyOverTimes/GetNumberAuto',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

//trongtq@esvn.com.vn
export default API_WorkShiftChange;
