import { LocalStorage } from '@utils';

export function wrap(key: string, promise: Promise) {
  return promise.then(res => {
    LocalStorage.set(key, JSON.stringify(res));
    return Promise.resolve(res);
  }).catch(err => {
    return Promise.reject(err);
  });
}

export function mock(key: string) {
  return LocalStorage.get(key).then(
    data => {
      if (data) {
        return Promise.resolve(JSON.parse(data));
      }
      return Promise.resolve(data);
    }
  );

}