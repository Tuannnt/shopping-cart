import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_Comment = {instance: getInstance()};

API_Comment.getAllCommentPage = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Comment.instance.post('api/Comment/GetCommentAll', JSON.stringify(obj), {
        headers: {...headers},
    });
};
API_Comment.GetPicApplyLeaves = (data) => {
    let urlAvata = 'http://apicm.easternsun.vn/api/Common/Employee/GetImage/' + data;
    if (urlAvata != null && urlAvata != "" && data != undefined && data != null) {
        return { uri: urlAvata };
    }
    else {
        return require('@images/icon/user.png');
    }
};
//Thêm mới đính kèm
API_Comment.Insert = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Comment.instance.post('/api/Comment/Insert',data, {
        headers: {...headers},
    });
};
//Xóa file
API_Comment.DeleteAttachments = (data) => {
   const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Comment.instance.post('/api/Comment/DeleteAttachments', JSON.stringify(data), {
        headers: {...headers},
    });
}; 
//Tài file
API_Comment.DownloadAttachments = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Comment.instance.post('/api/Comment/DownloadAttachments', JSON.stringify(data), {
        headers: {...headers},
    });
};
export default API_Comment;