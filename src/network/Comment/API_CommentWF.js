import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_CommentWF = {instance: getInstance()};

API_CommentWF.getAllCommentPage = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CommentWF.instance.post(
    'api/CommentWF/GetCommentWF',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_CommentWF.getAllCommentPageByGuid = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CommentWF.instance.post(
    'api/CommentWF/GetCommentWFByGuid',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_CommentWF.getAllCommentPageOldProcess = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_CommentWF.instance.post(
    'api/CommentWF/GetCommentWF',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin người đăng nhập
export default API_CommentWF;
