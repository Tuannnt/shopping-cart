import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_Attachment = {instance: getInstance()};

//Lấy danh sách file đính kèm
API_Attachment.GetAttList = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_Attachment.instance.post('/api/Attachments/GetAttList',JSON.stringify(data), {
        headers: {...headers},
    });
};
//Tài file
API_Attachment.Download = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_Attachment.instance.post('/api/Attachments/Download', data, {
        headers: {...headers},
    });
};
//Thêm mới đính kèm
API_Attachment.Insert = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_Attachment.instance.post('/api/Attachments/Insert',data, {
        headers: {...headers},
    });
};
//Xóa file
API_Attachment.Delete = (data) => {
   const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_Attachment.instance.post('/api/Attachments/Delete', data, {
        headers: {...headers},
    });
};
API_Attachment.InsertFolder = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Attachment.instance.post('/api/Attachments/InsertFolder',JSON.stringify(data), {
        headers: {...headers},
    });
};
export default API_Attachment;