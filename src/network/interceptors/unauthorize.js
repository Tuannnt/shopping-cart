import { Actions } from 'react-native-router-flux';
import { AccessTokenManager } from '@bussiness';
import _ from 'lodash';

export function onFullfilled(response) {
  if (response.data && response.data.code == -1) {
    console.log('Access', AccessTokenManager);
    AccessTokenManager.clear();
    _.throttle(gotoLogin, 3000)();
    return Promise.reject(response);
  }
  return Promise.resolve(response);
}

export function onRejected(error) {
  return Promise.reject(error);
}

function gotoLogin() {
  Actions.reset('login');
}