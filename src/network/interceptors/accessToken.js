import { AccessTokenManager } from '@bussiness';

export function addAccessToken(config) {
  const accessToken = AccessTokenManager.getAccessToken();
  if (accessToken) {
    const headers = { ...config.headers, 'x-access-token': accessToken };
    config.headers = headers;
  }
  return config;
}

export function onRejected(error) {
  return Promise.reject(error);
}