import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";
const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_POContracts = { instance: getInstance() };
//Danh sách hàng hóa vật tư
API_POContracts.ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_POContracts.instance.post('/api/POContracts/ListAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Chi tiết hàng hóa vật tư
API_POContracts.GetItem = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_POContracts.instance.post('/api/POContracts/GetItems/' +id,{}, {
        headers: {...headers},
    });
};
//Lấy danh sách kho
API_POContracts.ListDetail = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_POContracts.instance.post('/api/POContracts/ListDetail', JSON.stringify(data), {
        headers: {...headers},
    });
};
export default API_POContracts;
