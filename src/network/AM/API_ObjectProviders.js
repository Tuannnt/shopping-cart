import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";
const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_ObjectProviders = { instance: getInstance() };
//Danh sách hàng hóa vật tư
API_ObjectProviders.ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_ObjectProviders.instance.post('/api/ObjectProviders/ListAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Chi tiết hàng hóa vật tư
API_ObjectProviders.GetItem = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_ObjectProviders.instance.post('/api/ObjectProviders/GetItems/' +id,{}, {
        headers: {...headers},
    });
};
API_ObjectProviders.GetObjectGroups = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_ObjectProviders.instance.get('/api/ObjectProviders/GetObjectGroups', null, {
        headers: { ...headers },
    });
};
API_ObjectProviders.GetItemContracts = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_ObjectProviders.instance.post('/api/ObjectProviders/GetItemContracts/' +id,{}, {
        headers: {...headers},
    });
};
API_ObjectProviders.GetBankAccounts = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_ObjectProviders.instance.post('/api/ObjectProviders/GetBankAccounts/' +id,{}, {
        headers: {...headers},
    });
};
export default API_ObjectProviders;
