import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';
const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_TicketRequests = {instance: getInstance()};
//Đếm đề nghị nhập/xuất
API_TicketRequests.GetTicketRequestCount = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetTicketRequestsCount',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Danh sách đề nghị nhập
API_TicketRequests.GetTicketRequestsAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetTicketRequestsAll_Total',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Danh sách đề nghị xuất
API_TicketRequests.GetTicketRequestsExport = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetTicketRequestsAll_Export',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Xóa
API_TicketRequests.DeleteTicketRequestById = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    `/api/TicketRequests/IsDeleted?id=${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};

//Danh sách chi tiết đề nghị nhập/xuất
API_TicketRequests.GetTicketRequestsDetailsAll = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get(
    '/api/TicketRequests/GetTicketRequestDetailsById/' + id,
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin đề nghị nhập
API_TicketRequests.GetTicketRequestsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get(
    '/api/TicketRequests/GetTicketRequestById/' + data,
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin đề nghị xuất
API_TicketRequests.GetTicketRequestsByIdExport = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get(
    '/api/TicketRequests/GetTicketRequestByIdExport/' + data,
    {
      headers: {...headers},
    },
  );
};
//Lấy danh sach nguoi nhan quy trinh
API_TicketRequests.GetAppovers = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get('/api/TicketRequests/GetAppovers/', {
    headers: {...headers},
  });
};
//Lấy thông tin chi tiết đề nghị nhập/xuất
API_TicketRequests.GetTicketRequestsDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get(
    '/api/TicketRequests/GetTicketRequestDetailsById/' + data,
    {
      headers: {...headers},
    },
  );
};
//Cập nhật trạng thái xác nhận thông tin
API_TicketRequests.CheckStatus = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/CheckStatus',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//WorkFlow
API_TicketRequests.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_TicketRequests.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.GetTRNumberAuto = RequestType => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get(
    '/api/TicketRequests/GetTRNumberAuto/' + RequestType,
    null,
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.GetCustomersAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetCustomersAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.GetItemsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetItemsById',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.GetItemById = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get(
    `/api/TicketRequests/GetItemById?q=${id}`,
    null,
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.GetUnitsAllJExcel = data => {
  var obj = {Search: data};
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetUnitsAllJExcel/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.GetOrdersAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetOrdersAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketRequests.Insert = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post('/api/TicketRequests/Insert', data, {
    headers: {...headers},
  });
};
API_TicketRequests.GetNumberAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetNumberAuto',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_TicketRequests.GetEmployeesMH = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.get('/api/TicketRequests/GetEmployeesMH', {
    headers: {...headers},
  });
};

API_TicketRequests.InsertTicketRequests = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/InsertTicketRequests',
    data,
    {
      headers: {...headers},
    },
  );
};

API_TicketRequests.GetOrderDeliveryDatesAllJExcel = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketRequests.instance.post(
    '/api/TicketRequests/GetOrderDeliveryDatesAllJExcel/',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
export default API_TicketRequests;
