import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_POPaymentsAfterAM = {instance: getInstance()};

//POPaymentsAfter
API_POPaymentsAfterAM.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfterAM.instance.post(
    '/api/AM/POPaymentsAfter/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsAfterAM.ListDetails = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfterAM.instance.post(
    '/api/AM/POPaymentsAfter/ListDetails',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsAfterAM.GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfterAM.instance.post(
    '/api/AM/POPaymentsAfter/GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_POPaymentsAfterAM.GetItemDetails = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfterAM.instance.post(
    '/api/AM/POPaymentsAfter/GetItemDetails/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//WorkFlow
API_POPaymentsAfterAM.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfterAM.instance.post(
    '/api/AM/POPaymentsAfter/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_POPaymentsAfterAM.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfterAM.instance.post(
    '/api/AM/POPaymentsAfter/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsAfterAM.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfterAM.instance.post(
    '/api/AM/POPaymentsAfter/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_POPaymentsAfterAM.GetPOPaymentsBeforeCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsAfterAM.instance.post(
    '/api/AM/POPaymentsAfter/GetPOPaymentsBeforeCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_POPaymentsAfterAM;
