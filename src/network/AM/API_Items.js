import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";
const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_Items = { instance: getInstance() };
//Danh sách hàng hóa vật tư
API_Items.ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Items.instance.post('/api/Items/ListAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Chi tiết hàng hóa vật tư
API_Items.GetItem = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Items.instance.post('/api/Items/GetItems/' +id,{}, {
        headers: {...headers},
    });
};
//Lấy danh sách kho
API_Items.GetWarehouse = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Items.instance.get('/api/Items/GetWarehouse', null, {
        headers: { ...headers },
    });
};
//Lấy danh sách kho
API_Items.GetItems_Thumb = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Items.instance.get('/api/Items/GetItems_Thumb', null, {
        headers: { ...headers },
    });
};
export default API_Items;
