import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';
const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_TicketChecking = {instance: getInstance()};
//Danh sách đề nghị nhập/xuất
API_TicketChecking.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    '/api/TicketCheking/TicketChekingListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin đề nghị nhập/xuất
API_TicketChecking.GetItem = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    `/api/TicketCheking/GetItem?guid=${data.TicketGuid}`,
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin chi tiết đề nghị nhập/xuất
API_TicketChecking.GetInfoItem = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    '/api/TicketCheking/GetInfoItem/',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    `/api/TicketCheking/Delete?guid=${id}`,
    null,
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.GetWahouse = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    '/api/TicketCheking/GetWahouse',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.GetItemsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    '/api/TicketChecking/GetItemsById',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.GetItemsByWareHouse = (id, date) => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.get(
    `/api/TicketCheking/GetItemsByWarehouse?wahouseId=${id}&dateTo=${date}`,
    null,
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.GetUnitsAllJExcel = q => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.get(
    '/api/TicketChecking/GetUnitsAllJExcel/' + q,
    null,
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.getAllObjectKH = q => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.get(
    '/api/Quotations/getAllObjectKH/',
    null,
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.Insert = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    '/api/TicketCheking/Insert',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.Update = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    '/api/TicketCheking/Update',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TicketChecking.GetNumberAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TicketChecking.instance.post(
    '/api/TicketCheking/GetNumberAuto',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
export default API_TicketChecking;
