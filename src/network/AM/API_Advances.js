//TinhLV
import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API_Advances = {instance: getInstance()};
//Jtable Advances
API_Advances.Advances_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Advances.instance.post(
    '/api/AdvancesAM/Advances_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_Advances.GetBanksDetail = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Advances.instance.get('api/AdvancesAM/GetBanksDetail', {
    headers: {...headers},
  });
};
//Get Employee
API_Advances.GetEmpAdvances = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Advances.instance.get('api/AdvancesAM/GetEmpAdvances', {
    headers: {...headers},
  });
};
//insert Advances
API_Advances.Advances_InsertAPI = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Advances.instance.post('api/AdvancesAM/Advances_InsertAPI', data, {
    headers: {...headers},
  });
};
//open Advances
API_Advances.Advances_Items = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Advances.instance.post(
    `/api/AdvancesAM/Advances_Items/${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
//Aprove
API_Advances.Approve_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Advances.instance.post(
    'api/AdvancesAM/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//NotAprove
API_Advances.NotApprove_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Advances.instance.post(
    'api/AdvancesAM/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Update advance
API_Advances.Advances_Update = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Advances.instance.post('api/AdvancesAM/Advances_Update', data, {
    headers: {...headers},
  });
};
//check login
API_Advances.CheckLogin_Advances = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Advances.instance.post(
    'api/AdvancesAM/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Delete Advances
API_Advances.Advances_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Advances.instance.post(
    'api/AdvancesAM/Advances_Delete/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//Danh sách chi tiết tạm ứng
API_Advances.Advances_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Advances.instance.post(
    '/api/AdvancesAM/Advances_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

export default API_Advances;
