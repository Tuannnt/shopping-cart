import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_AM = {instance: getInstance()};

API_AM.vouChersListAll = token => {
  return fetch(`${configApp.url_api_mb}/api/Vouchers/VouchersListAll`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: 'Bearer ' + token,
    },
  })
    .then(response => response)
    .then(response => {
      console.log('-----------------------------' + response);
      return response;
    })
    .catch(error => {
      throw error;
    });
};
API_AM.insert = data => {
  const headers = {
    'Content-Type': 'application/json',
  };
  return API_AM.instance.post('/Demo/Insert', JSON.stringify(data), {
    headers: {...headers},
  });
};
API_AM.update = data => {
  const headers = {
    'Content-Type': 'application/json',
  };
  return API_AM.instance.post('/Demo/Update', JSON.stringify(data), {
    headers: {...headers},
  });
};
API_AM.delete = data => {
  const headers = {
    'Content-Type': 'application/json',
  };
  return API_AM.instance.post('/Demo/Delete/' + data, {
    headers: {...headers},
  });
};
// đếm tiền tồn kho
API_AM.GetWarehouses = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_AM.instance.post(
    `/api/HomeAM/GetWarehouses`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
// đếm số dashboard
API_AM.GetHomeAM = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_AM.instance.get('/api/HomeAM/GetHomeReport', {
    headers: {...headers},
  });
};
// list đe nghi tt NCC
API_AM.RequirePayments_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_AM.instance.post(
    '/api/RequirePayments/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
// get đe nghi tt NCC
API_AM.RequirePayments_Items = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_AM.instance.post(
    `/api/RequirePayments/GetItem/${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
API_AM.RequirePayments_Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_AM.instance.post(
    '/api/RequirePayments/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_AM.RequirePayments_NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_AM.instance.post(
    '/api/RequirePayments/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_AM.CheckLogin_RequirePayment = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_AM.instance.post(
    '/api/RequirePayments/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_AM.RequirePayments_Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_AM.instance.post(
    `/api/RequirePayments/IsDeleted?Id=${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
export default API_AM;
