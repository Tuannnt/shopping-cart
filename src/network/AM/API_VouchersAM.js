import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_VouchersAM = {instance: getInstance()};

//lấy danh sách
API_VouchersAM.VouchersAM_GetAllPaging = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_VouchersAM.instance.post('/api/VouchersAM/VouchersAM_GetAllPaging', JSON.stringify(data), {
        headers: {...headers},
    });
};
//chi tiết phiếu
API_VouchersAM.VouchersAM_GetItem = (data) => {
  const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_VouchersAM.instance.post('/api/VouchersAM/VouchersAM_GetItem', JSON.stringify(data), {
      headers: {...headers},
  });
};
export default API_VouchersAM;
