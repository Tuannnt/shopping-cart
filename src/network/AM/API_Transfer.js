import axios from 'axios';
import qs from 'qs';
import {mock, wrap} from '../mock';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_Transfer = {instance: getInstance()};

//lấy danh sách
API_Transfer.Transfer_GetAllPaging = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Transfer.instance.post('/api/Transfer/Transfer_GetAllPaging', JSON.stringify(data), {
        headers: {...headers},
    });
};
//chi tiết phiếu
API_Transfer.Transfer_GetItem = (data) => {
  const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Transfer.instance.post('/api/Transfer/Transfer_GetItem', JSON.stringify(data), {
      headers: {...headers},
  });
};
export default API_Transfer;
