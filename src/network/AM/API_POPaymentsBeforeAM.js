import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_POPaymentsBeforeAM = {instance: getInstance()};

//POPaymentsBefore
API_POPaymentsBeforeAM.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//POPaymentsBeforeBM

API_POPaymentsBeforeAM.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsBeforeAM.ListDetails = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/ListDetails',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsBeforeAM.GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_POPaymentsBeforeAM.GetItemDetails = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/GetItemDetails/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//WorkFlow
API_POPaymentsBeforeAM.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_POPaymentsBeforeAM.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsBeforeAM.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_POPaymentsBeforeAM.GetPOPaymentsBeforeCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsBeforeAM.instance.post(
    '/api/AM/POPaymentsBefore/GetPOPaymentsBeforeCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

export default API_POPaymentsBeforeAM;
