import axios from 'axios';
import Config from '@config';
import configApp from '../configApp';
import qs from 'qs';
import * as UnauthorizeInterceptor from './interceptors/unauthorize';
import * as LogInterceptor from './interceptors/log';
import * as AccessTokenInterceptor from './interceptors/accessToken';
import { mock, wrap } from './mock';
import AccessTokenManager from '../bussiness/AccessTokenManager';
const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API = { instance: getInstance() };

API.hasError = data => {
  return !(data && data.code === 1);
};

API.logout = () => {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
  };

  return API.instance.post('/accounts/logout', null, {
    headers: { ...headers },
  });
};
API.getAll = () => {
  return API.instance.get('/Demo/GetAll');
};
API.insert = data => {
  const headers = {
    'Content-Type': 'application/json',
  };
  return API.instance.post('/Demo/Insert', JSON.stringify(data), {
    headers: { ...headers },
  });
};
API.update = data => {
  const headers = {
    'Content-Type': 'application/json',
  };
  return API.instance.post('/Demo/Update', JSON.stringify(data), {
    headers: { ...headers },
  });
};
API.delete = data => {
  const headers = {
    'Content-Type': 'application/json',
  };
  return API.instance.post('/Demo/Delete/' + data, {
    headers: { ...headers },
  });
};
//trongtq@esvn.com.vn
API.CheckToday = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/BaseAPI/CheckToday', JSON.stringify(null), {
    headers: { ...headers },
  });
};

API.ChangePassword = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/User/ChangePassword', JSON.stringify(data), {
    headers: { ...headers },
  });
};
API.CountNotification = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.get('/api/Alerts/CountNotification', {
    headers: { ...headers },
  });
};
API.GetNumberAutoSM = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post(
    '/api/BaseAPI/GetNumberAutoSM',
    JSON.stringify(data),
    { headers: { ...headers } },
  );
};
API.RemoveDevice = o => {
  const headers = {
    'Content-Type': 'application/json;',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/BaseAPI/RemoveDevice', JSON.stringify(o), {
    headers: { ...headers },
  });
};
//end trongtq
API.login = (username, password) => {
  var formData = new FormData();
  formData.append('grant_type', configApp.grant_type_login);
  formData.append('username', username);
  formData.append('password', password);
  formData.append('client_id', configApp.client_id);
  formData.append('client_secret', configApp.client_secret);
  formData.append('scope', configApp.scope);
  return fetch(`${configApp.url_sso}/connect/token`, {
    method: 'POST',
    body: formData,
    headers: configApp.configHeaders,
  })
    .then(response => response.json())
    .then(response => {
      return response;
    })
    .catch(error => {
      throw error;
    });
};

API.refreshToken = refreshToken => {
  let formData = new FormData();
  formData.append('grant_type', configApp.grant_type_refresh);
  formData.append('client_id', configApp.client_id);
  formData.append('client_secret', configApp.client_secret);
  formData.append('refresh_token', refreshToken);
  return (
    fetch(configApp.url_sso + '/connect/token', {
      method: 'POST',
      body: formData,
      headers: configApp.configHeaders,
    })
      //.then(response => response.json())
      .then(response => {
        return response;
      })
      .catch(error => {
        throw error;
      })
  );
};
API.GetClaims = token => {
  return fetch(`${configApp.url_api}/api/identity/claims`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: 'Bearer ' + token,
    },
  })
    .then(response => response)
    .then(response => {
      return response;
    })
    .catch(error => {
      throw error;
    });
};

API.getBanks = () => {
  return API.instance.get('/Common/Banks/GetAllBanks');
};
API.getProfileByToken = t => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + t,
  };
  return API.instance.post('/api/BaseAPI/Profile', JSON.stringify({}), {
    headers: { ...headers },
  });
};
API.getProfile = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/BaseAPI/Profile', JSON.stringify({}), {
    headers: { ...headers },
  });
};
//Begin created by habx@esvn.com.vn
API.sendNotification = (header, content) => {
  let headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: "Basic 'NWE5MmJhZjYtNjE3Ni00YTBkLWIyYmQtMTViY2NhMDM0MmI3'",
  };

  let endpoint = 'https://onesignal.com/api/v1/notifications';

  let params = {
    method: 'POST',
    headers: headers,
    body: JSON.stringify({
      app_id: '3459ec28-1ef3-45d5-914a-81716539984a',
      included_segments: ['All'],
      headings: { en: header },
      contents: { en: content },
    }),
  };
  fetch(endpoint, params).then(res => { });
};
API.SendNotiCommon = o => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/BaseAPI/SendNotiCommon', JSON.stringify(o), {
    headers: { ...headers },
  });
};
API.InitNotiCommon = o => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/BaseAPI/InitNotiCommon', JSON.stringify(o), {
    headers: { ...headers },
  });
};
//End created by habx@esvn.com.vn
API.CountNoification = () => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post(
    '/api/BaseAPI/CountMenuDocuments',
    JSON.stringify({}),
    {
      headers: { ...headers },
    },
  );
};
//thanhtt
API.CountStatus = () => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post(
    '/api/BaseAPI/CountRegisterEats',
    JSON.stringify({}),
    {
      headers: { ...headers },
    },
  );
};
API.CountMenu_AM = () => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/BaseAPI/CountMenu_AM', JSON.stringify({}), {
    headers: { ...headers },
  });
};
API.CountMenu_Pro = () => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/BaseAPI/CountMenu_Pro', JSON.stringify({}), {
    headers: { ...headers },
  });
};
API.Applications = () => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return fetch(
    `${configApp.url_api}/api/Permission/Resources/` + configApp.MB_PAGES,
    {
      method: 'GET',
      headers: headers,
    },
  )
    .then(response => response.json())
    .then(response => {
      return response;
    })
    .catch(error => {
      throw error;
    });
};
//Alert
API.GetAlertsByUser = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/Alerts/GetByUser', JSON.stringify(data), {
    headers: { ...headers },
  });
};
API.ReadAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/Alerts/ReadAll', JSON.stringify(data), {
    headers: { ...headers },
  });
};
//Alert
API.ListAnnouncements = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post(
    '/api/Announcements/ListAnnouncements',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Update IsRead
API.UpdateIsRead = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API.instance.post('/api/Alerts/UpdateIsRead', JSON.stringify(data), {
    headers: { ...headers },
  });
};

export default API;
