import axios from 'axios';
import * as UnauthorizeInterceptor from '../../interceptors/unauthorize';
import * as LogInterceptor from '../../interceptors/log';
import * as AccessTokenInterceptor from '../../interceptors/accessToken';
import configApp from "../../../configApp";
import AccessTokenManager from "../../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb + '/api',
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );
    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );
    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}

const API_TM_CALENDAR = { instance: getInstance() };
API_TM_CALENDAR.Calendar_GetAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_CALENDAR.instance.post('/Calendars/Calendar_GetAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_TM_CALENDAR.Calendar_Insert = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data;',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_CALENDAR.instance.post('/Calendars/Calendar_Insert', data, {
        headers: { ...headers },
    });
};
API_TM_CALENDAR.Calendar_Update = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_CALENDAR.instance.post('/Calendars/Calendar_Update',data, {
        headers: { ...headers },
    });
};
API_TM_CALENDAR.Calendar_Delete = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_CALENDAR.instance.post('/Calendars/Calendar_Delete/', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_TM_CALENDAR.Calendar_GetItem = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_CALENDAR.instance.post('/Calendars/Calendar_GetItem', JSON.stringify(data), {
        headers: { ...headers },
    });
};

API_TM_CALENDAR.GetAttachments = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_CALENDAR.instance.post('/Calendars/GetAttachments', JSON.stringify(data), {
        headers: { ...headers },
    });
};
export default API_TM_CALENDAR;
