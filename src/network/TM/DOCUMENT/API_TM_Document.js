import axios from 'axios';
import * as UnauthorizeInterceptor from '../../interceptors/unauthorize';
import * as LogInterceptor from '../../interceptors/log';
import * as AccessTokenInterceptor from '../../interceptors/accessToken';
import configApp from "../../../configApp";
import AccessTokenManager from "../../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb +'/api',
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}

const API_TM_Document = {instance: getInstance()};
API_TM_Document.GetFiles = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_Document.instance.post('/Document/GetFiles',JSON.stringify(data), {
        headers: {...headers},
    });
};

API_TM_Document.GetAttachmentByFolder = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_Document.instance.post('/Document/GetAttachmentByFolder',JSON.stringify(data), {
        headers: {...headers},
    });
};

API_TM_Document.InsertFolder = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_Document.instance.post('/Document/InsertFolder',JSON.stringify(data), {
        headers: {...headers},
    });
};

API_TM_Document.UpdateFolderFile = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_Document.instance.post('/Document/UpdateFolderFile',JSON.stringify(data), {
        headers: {...headers},
    });
};

API_TM_Document.DeleteDocument = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_Document.instance.post('/Document/DeleteDocument',JSON.stringify(data), {
        headers: {...headers},
    });
};

API_TM_Document.InsertDocument = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_Document.instance.post('/Document/InsertDocument',data , {
        headers: {...headers},
    });
};
//trongtq
API_TM_Document.GetCategory = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TM_Document.instance.post('/Document/GetCategory',JSON.stringify(data), {
        headers: {...headers},
    });
};

export default API_TM_Document;
