import axios from 'axios';
import * as UnauthorizeInterceptor from '../../interceptors/unauthorize';
import * as LogInterceptor from '../../interceptors/log';
import * as AccessTokenInterceptor from '../../interceptors/accessToken';
import configApp from "../../../configApp";
import AccessTokenManager from "../../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb + '/api',
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );
    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );
    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}

const API_TODO = { instance: getInstance() };

//Begin trongtq@esvn.com.vn

API_TODO.ListCategoryOfTasks = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/CategoryOfTasks/ListCategoryOfTasks/', JSON.stringify(obj), {
        headers: { ...headers },
    });
}
API_TODO.ListTasks = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/ListTasks/', JSON.stringify(obj), {
        headers: { ...headers },
    });
}
API_TODO.UpdateFollow = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateFollow/', JSON.stringify(obj), {
        headers: { ...headers },
    });
}
API_TODO.UpdateProcessSuccess = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateProcessSuccess/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_TODO.GetItemTasks = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/GetItemTasks/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_TODO.UpdateDescription = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateDescription/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_TODO.Delete = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/Delete/', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_TODO.UpdateCheckList = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateCheckList', data, {
        headers: { ...headers },
    });
};
API_TODO.InsertCategoryOfTasks = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/CategoryOfTasks/InsertCategoryOfTasks/', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_TODO.UpdateCategoryOfTasks = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/CategoryOfTasks/UpdateCategoryOfTasks/', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_TODO.AttInsert = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data;',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/AttInsert', data, {
        headers: { ...headers },
    });
};
API_TODO.DeleteAtt = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data;',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/DeleteAtt', data, {
        headers: { ...headers },
    });
};
API_TODO.GetAttList = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/GetAttList', data, {
        headers: { ...headers },
    });
};
API_TODO.UpdateDate = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateDate', data, {
        headers: { ...headers },
    });
};
API_TODO.UpdateRecurrence = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data;',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateRecurrence', data, {
        headers: { ...headers },
    });
};
API_TODO.UpdateMyDate = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateMyDate', data, {
        headers: { ...headers },
    });
};
API_TODO.UpdateTimeReminder = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateTimeReminder', data, {
        headers: { ...headers },
    });
};
API_TODO.ListTasks_AddMyDay = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/ListTasks_AddMyDay', data, {
        headers: { ...headers },
    });
};
API_TODO.UpdateSubject = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/UpdateSubject/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_TODO.InsertV2 = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data;',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/InsertV2', data, {
        headers: { ...headers },
    });
};
API_TODO.DeleteCategoryOfTasks = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/CategoryOfTasks/DeleteCategoryOfTasks/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};

API_TODO.InsertCheckList = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/InsertCheckList/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};

API_TODO.DeleteCheckList = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_TODO.instance.post('/Tasks/DeleteCheckList/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
//end trongtq@esvn.com.vn
export default API_TODO;
