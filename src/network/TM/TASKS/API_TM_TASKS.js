import axios from 'axios';
import * as UnauthorizeInterceptor from '../../interceptors/unauthorize';
import * as LogInterceptor from '../../interceptors/log';
import * as AccessTokenInterceptor from '../../interceptors/accessToken';
import configApp from '../../../configApp';
import AccessTokenManager from '../../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb + '/api',
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );
  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );
  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const getInstance_cm = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_cm + '/api/v1/',
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );
  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );
  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};

const API_TM_TASKS = {instance: getInstance(), instance_cm: getInstance_cm()};
API_TM_TASKS.InsertTimeKeepingsLog = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance_cm.post(
    '/hr/TimeKeepings/InsertTimeKeepingsLog',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.TimeKeeperLogs_addFix = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/ViewTimeKeeper/TimeKeeperLogs_addFix',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.TimeKeeperLogs_add = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/ViewTimeKeeper/TimeKeeperLogs_add',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.GetLocationCheckin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance_cm.post(
    '/hr/TimeKeepings/GetLocationCheckin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_TM_TASKS.GetTasks = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/GetTasks', JSON.stringify(data), {
    headers: {...headers},
  });
};
API_TM_TASKS.Insert = data => {
  const headers = {
    'Content-Type': 'multipart/form-data;',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/Insert', data, {
    headers: {...headers},
  });
};
API_TM_TASKS.Tasks_Update = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/Tasks_Update', data, {
    headers: {...headers},
  });
};
API_TM_TASKS.Delete = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/Delete/', JSON.stringify(data), {
    headers: {...headers},
  });
};
API_TM_TASKS.GetItemTask = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/GetItemTasks',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_TM_TASKS.GetEmployees = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.get(
    '/Tasks/GetEmployees',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_TM_TASKS.UpdateProcess = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/UpdateProcess', data, {
    headers: {...headers},
  });
};

API_TM_TASKS.AttInsert = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/AttInsert', data, {
    headers: {...headers},
  });
};

API_TM_TASKS.GetItemTaskOfUser = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/GetItemTaskOfUser', data, {
    headers: {...headers},
  });
};

API_TM_TASKS.GetAttList = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/GetAttList', data, {
    headers: {...headers},
  });
};

API_TM_TASKS.Download = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.get('/Tasks/Download/' + data, {
    headers: {...headers},
  });
};
//Begin trongtq@esvn.com.vn
API_TM_TASKS.CountTask = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/CountTask/', data, {
    headers: {...headers},
  });
};
API_TM_TASKS.ListCombobox_Employee = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/ListCombobox_Employee/',
    {},
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.GetTreeDataCOT = id => {
  var data = {Guid: id};
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/GetTreeDataCOT/',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.UpdateFollow = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/UpdateFollow/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.GetItemTasks = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/GetItemTasks/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.CommentList = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/CommentList/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.CommentInsert = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/CommentInsert', data, {
    headers: {...headers},
  });
};
API_TM_TASKS.InsertToCalenderOfUser = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/InsertToCalenderOfUser/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.GetChildTask = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/GetChildTask/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.GetAllChildTask = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/GetAllChildTask/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.UpdateProcessSuccess = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/UpdateProcessSuccess/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.UpdateCheckList = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/UpdateCheckList/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.TaskComment_ViewAll_Image = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/TaskComment_ViewAll_Image/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.UpdateAccept = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/UpdateAccept/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_TM_TASKS.GetLabels = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/GetLabels/', JSON.stringify(obj), {
    headers: {...headers},
  });
};

API_TM_TASKS.RecallTask = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post('/Tasks/RecallTask/', JSON.stringify(obj), {
    headers: {...headers},
  });
};

API_TM_TASKS.GetEmployeeByCategoryOfTask = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/GetEmployeeByCategoryOfTask/',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_TM_TASKS.CheckPermission = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_TM_TASKS.instance.post(
    '/Tasks/CheckPermission',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//end trongtq@esvn.com.vn
export default API_TM_TASKS;
