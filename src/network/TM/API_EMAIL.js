import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb + '/api',
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );
    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );
    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}

const API_EMAIL = { instance: getInstance() };

//Begin trongtq@esvn.com.vn

API_EMAIL.GetEmails = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_EMAIL.instance.post('/Emails/GetEmails/', JSON.stringify(obj), {
        headers: { ...headers },
    });
}
API_EMAIL.LoadMailConfig = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_EMAIL.instance.post('/Emails/LoadMailConfig/', JSON.stringify(obj), {
        headers: { ...headers },
    });
}
API_EMAIL.SendEmail = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_EMAIL.instance.post('/Emails/SendEmail', data, {
        headers: {...headers},
    });
};

API_EMAIL.GetItemEmail = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_EMAIL.instance.post('/Emails/GetItemEmail/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_EMAIL.DeleteEmail = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_EMAIL.instance.post('/Emails/DeleteEmail/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_EMAIL.UpdateFollowFlag = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_EMAIL.instance.post('/Emails/UpdateFollowFlag/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_EMAIL.UpdateReaded = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_EMAIL.instance.post('/Emails/UpdateReaded/', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
//end trongtq@esvn.com.vn
export default API_EMAIL;
