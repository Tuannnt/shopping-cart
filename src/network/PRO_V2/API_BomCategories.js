import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import { addAccessToken } from '../interceptors/accessToken';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_BomCategories = { instance: getInstance() };
//list danh sách
API_BomCategories.JTableBomPending = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/JTableBomPending', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.JTable = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/JTable', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetBOMWorkOrderRoutings = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetBOMWorkOrderRoutings', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetBOMWorkpieces = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetBOMWorkpieces', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetBOMMaterialNorms = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetBOMMaterialNorms', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetProductCostNorms = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetProductCostNorms', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetLaborCosts = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetLaborCosts', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetMachineCosts = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetMachineCosts', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetToolsCosts = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetToolsCosts', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetProductCostNormsCosts = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetProductCostNormsCosts', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetBOMCostNormHistories = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetBOMCostNormHistories', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_BomCategories.GetItemById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_BomCategories.instance.post('/api/BomCategories/GetItemById', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
export default API_BomCategories;
