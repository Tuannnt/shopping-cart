import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import { addAccessToken } from '../interceptors/accessToken';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_ProductionPlanDayV2 = { instance: getInstance() };
//list danh sách
API_ProductionPlanDayV2.JTable = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionPlanDayV2.instance.post('/api/ProductionPlanDay/JTable', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Update
API_ProductionPlanDayV2.Update = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionPlanDayV2.instance.post('/api/ProductionPlanDay/Update', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_ProductionPlanDayV2.GetLocation = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionPlanDayV2.instance.post('/api/ProductionPlanDay/GetLocation?type=' + id, null,
    {
      headers: { ...headers },
    },
  );
};
API_ProductionPlanDayV2.Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionPlanDayV2.instance.post('/api/ProductionPlanDay/Delete?id=' + id, null,
    {
      headers: { ...headers },
    },
  );
};
// get Item to Update
API_ProductionPlanDayV2.GetItem = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionPlanDayV2.instance.post('/api/ProductionPlanDay/GetItem?id=' + id, null,
    {
      headers: { ...headers },
    },
  );
};
export default API_ProductionPlanDayV2;
