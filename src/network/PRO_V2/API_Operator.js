import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import { addAccessToken } from '../interceptors/accessToken';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_Operator = { instance: getInstance() };
API_Operator.JTableEmp = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableEmp', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTableMachines = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableMachines', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTableTools = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableTools', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTableItem_Supplies = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableItem_Supplies', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetDataView = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetDataView', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetItemPrint = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetItemPrint', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_Operator.GetEmployeeSanXuat = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetEmployeeSanXuat', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetMachines = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetMachines', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTablePlanJobCards = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTablePlanJobCards', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetItemJobCard = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetItemJobCard', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTablePlanJobCardsBaoCao = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTablePlanJobCardsBaoCao', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_Operator.GetItemPlanJobCards = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetItemPlanJobCards', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetEmployees = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetEmployees', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetQRCode = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetQRCode', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.LoadMachinesUpdate = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/LoadMachinesUpdate', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.UpdateMachidnes = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/UpdateMachidnes', data, {
    headers: { ...headers },
  });
};

API_Operator.InsertPlanJobCards = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/InsertPlanJobCards', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.UpdatePlanJobCards = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/UpdatePlanJobCards', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetErrors = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetErrors', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.QCCheck = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/QCCheck', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetEmployeeByDepartment = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetEmployeeByDepartment', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetShifts = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetShifts', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.CheckShift = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/CheckShift', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetOrders_AM = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetOrders_AM', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetNumberAutoHr = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetNumberAutoHr', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.Submit = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/Submit', data, {
    headers: { ...headers },
  });
};

API_Operator.GetJobTitle = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetJobTitle', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetTreeDataAdd = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetTreeDataAdd', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.getEmployeeIdAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/getEmployeeIdAuto', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.Insert_Employees = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/Insert_Employees', data, {
    headers: { ...headers },
  });
};

API_Operator.sendBCEmployee = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/sendBCEmployee', data, {
    headers: { ...headers },
  });
};

API_Operator.LoadToolsUpdate = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/LoadToolsUpdate', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.UpdateTools = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/UpdateTools', data, {
    headers: { ...headers },
  });
};

API_Operator.JTableMachinesUpdate = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableMachinesUpdate', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTableToolsUpdate = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableToolsUpdate', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.Update_MachinesTolss = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/Update_MachinesTolss', data, {
    headers: { ...headers },
  });
};

API_Operator.PlanJobCardsCancel = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/PlanJobCardsCancel', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.ApprovedJobCard = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/ApprovedJobCard', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.PlanJobCardsReport = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/PlanJobCardsReport', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetLocaton = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetLocaton', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetScrapReasonsBaoCao = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetScrapReasonsBaoCao', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.UpdatePlanJobCardsReport = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/UpdatePlanJobCardsReport', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.ListPlanJobCards = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/ListPlanJobCards', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetLocation = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetLocation', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetEmpJobCards = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetEmpJobCards', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.UpdatePlans = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/UpdatePlans', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_Operator.GetNumberAutoAM = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetNumberAutoAM', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetCustomersAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetCustomersAll', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.Insert_TicketRequests = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/Insert_TicketRequests', data, {
    headers: { ...headers },
  });
};

API_Operator.JTableSupplies = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableSupplies', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTableItemSupplies = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableItemSupplies', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.InsertTicketRequests = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/InsertTicketRequests', data, {
    headers: { ...headers },
  });
};

API_Operator.GetAllMaterialErrors = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetAllMaterialErrors', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.InsertMaterialErrors = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/InsertMaterialErrors', data, {
    headers: { ...headers },
  });
};
API_Operator.GetOrdersAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetOrdersAll', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.GetTRNumberAuto = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/GetTRNumberAuto', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTableQTCN = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableQTCN', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};

API_Operator.JTableAttachment = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Operator.instance.post('/api/Operator/JTableAttachment', JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
export default API_Operator;
