import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import {addAccessToken} from '../interceptors/accessToken';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_ProductionOrdersV2 = {instance: getInstance()};
//list danh sách
API_ProductionOrdersV2.JTable = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionOrdersV2.instance.post(
    '/api/ProductionOrders/JTable',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//xem chi tiết
API_ProductionOrdersV2.Items = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionOrdersV2.instance.post(
    '/api/ProductionOrders/Items',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

//xem chi tiết JTablePartList
API_ProductionOrdersV2.JTablePartList = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionOrdersV2.instance.post(
    '/api/ProductionOrders/JTablePartList',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};

API_ProductionOrdersV2.GetOrderTypes = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_ProductionOrdersV2.instance.get(
    '/api/ProductionOrders/GetOrderTypes',
    {
      headers: {...headers},
    },
  );
};
export default API_ProductionOrdersV2;
