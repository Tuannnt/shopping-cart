/**
 * Networking Manager
 */

import API from './API';
import API_Orders from './SM/API_Orders';
//Begin Created by habx@esvn.com.vn
import API_TM_TASKS from './TM/TASKS/API_TM_TASKS';
//End Created by habx@esvn.com.vn
import API_AM from './AM/API_AM';
import API_HR from './HR/API_HR';
import API_ListProposePurchases from './BM/API_ListProposePurchases';
import API_ApplyOutsides from './HR/API_ApplyOutsides';
import API_ApplyOverTimes from './HR/API_ApplyOverTimes';
import API_WorkShiftChange from './HR/API_WorkShiftChange';
import API_ApplyLeaves from './HR/API_ApplyLeaves';
import API_ProductionOrders from './PRO/API_ProductionOrders';
import API_RegisterCars from './HR/API_RegisterCars';
import API_CompensationSlip from './HR/API_CompensationSlip';
import API_MeetingCalendars from './HR/API_MeetingCalendars';
import API_TimeKeepingsComfirm from './HR/API_TimeKeepingsComfirm';
import API_Message from './MS/API_Message';
import API_VouchersAM from './AM/API_VouchersAM';
import API_TicketChecking from './AM/API_TicketChecking';
import API_Transfer from './AM/API_Transfer';
import API_Advances from './AM/API_Advances';
import API_POPaymentsBeforeAM from './AM/API_POPaymentsBeforeAM';
import API_POPaymentsAfterAM from './AM/API_POPaymentsAfterAM';
import API_POPaymentsBefore from './BM/API_POPaymentsBefore';
import API_POPaymentsAfter from './BM/API_POPaymentsAfter';
import API_PurchaseOrders from './BM/API_PurchaseOrders';
import API_TicketRequests from './BM/API_TicketRequests';
import API_Projects from './PM/API_Projects';
import API_TM_CALENDAR from './TM/CALENDAR/API_TM_CALENDAR';
import API_TODO from './TM/TASKS/API_TODO';
import API_EMAIL from './TM/API_EMAIL';
import API_Leads from './SM/API_Leads';
import API_ConfirmPayments from './SM/API_ConfirmPayments';
import API_OrderDetailsSX from './SM/API_OrderDetailsSX';
import API_InvoiceOrders from './SM/API_InvoiceOrders';
import API_QuotationRequest from './SM/API_QuotationRequest';
import API_ProductionOrdersV2 from './PRO_V2/API_ProductionOrdersV2';
import API_PlanTotalV2 from './PRO_V2/API_PlanTotalV2';
import API_BomCategories from './PRO_V2/API_BomCategories';
import API_ProductionPlanDayV2 from './PRO_V2/API_ProductionPlanDayV2';
import API_Follow from './PRO/API_Follow';
import API_ProductionPlan from './PRO_V2/API_ProductionPlan';
import API_ProductionPlanCNC from './PRO_V2/API_ProductionPlanCNC';
import API_Operator from './PRO_V2/API_Operator';
import API_TM_Document from './TM/DOCUMENT/API_TM_Document';
import API_Announcements from './HR/API_Announcements';
import API_Attachment from './Attachment/API_Attachment';
import API_Customers from './SM/API_Customers';
import API_Quotations from './SM/API_Quotations';
import API_POPaymentsCustomer from './SM/API_POPaymentsCustomer';
export {
  API,
  API_HR,
  API_Orders,
  API_AM,
  API_TM_TASKS,
  API_ApplyOutsides,
  API_ApplyOverTimes,
  API_WorkShiftChange,
  API_ApplyLeaves,
  API_RegisterCars,
  API_CompensationSlip,
  API_ProductionOrders,
  API_Message,
  API_VouchersAM,
  API_Projects,
  API_Transfer,
  API_TM_CALENDAR,
  API_TODO,
  API_EMAIL,
  API_Leads,
  API_ProductionOrdersV2,
  API_Follow,
  API_PlanTotalV2,
  API_BomCategories,
  API_ProductionPlanDayV2,
  API_ProductionPlan,
  API_ProductionPlanCNC,
  API_Operator,
  API_TM_Document,
  API_Announcements,
  API_Attachment,
  API_Customers,
  API_ConfirmPayments,
  API_InvoiceOrders,
  API_OrderDetailsSX,
  API_TicketChecking,
  API_QuotationRequest,
  API_POPaymentsBeforeAM,
  API_POPaymentsAfterAM,
  API_PurchaseOrders,
  API_TicketRequests,
  API_ListProposePurchases,
  API_TimeKeepingsComfirm,
  API_MeetingCalendars,
  API_POPaymentsBefore,
  API_POPaymentsAfter,
  API_Quotations,
  API_Advances,
  API_POPaymentsCustomer,
};
