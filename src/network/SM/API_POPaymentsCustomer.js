import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_POPaymentsCustomer = {instance: getInstance()};

//POPaymentsCustomer
API_POPaymentsCustomer.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsCustomer.instance.post(
    '/api/POPaymentsCustomer/ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsCustomer.ListDetails = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsCustomer.instance.post(
    '/api/POPaymentsCustomer/ListDetails',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsCustomer.GetItems = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsCustomer.instance.post(
    '/api/POPaymentsCustomer/GetItems/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

API_POPaymentsCustomer.GetItemDetails = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsCustomer.instance.post(
    '/api/POPaymentsCustomer/GetItemDetails/' + id,
    {},
    {
      headers: {...headers},
    },
  );
};

//WorkFlow
API_POPaymentsCustomer.Approve = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsCustomer.instance.post(
    '/api/POPaymentsCustomer/Approve',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

//Trả lại
API_POPaymentsCustomer.NotApprove = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsCustomer.instance.post(
    '/api/POPaymentsCustomer/NotApprove',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_POPaymentsCustomer.CheckLogin = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsCustomer.instance.post(
    '/api/POPaymentsCustomer/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//đếm số
API_POPaymentsCustomer.GetPOPaymentsBeforeCount = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_POPaymentsCustomer.instance.post(
    '/api/POPaymentsCustomer/GetPOPaymentsBeforeCount',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_POPaymentsCustomer;
