import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import API_TM_TASKS from '../TM/TASKS/API_TM_TASKS';
import AccessTokenManager from '../../bussiness/AccessTokenManager';

const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_Orders = {instance: getInstance()};
//Danh sách đơn bán hàng
API_Orders.GetOrderAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    '/api/Orders/GetOrderAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Danh sách chi tiết đơn bán hàng
API_Orders.GetOrderDetailsAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    '/api/Orders/GetOrderDetailsAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin đơn hàng
API_Orders.GetOrderById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.get('/api/Orders/GetOrderById/' + data, {
    headers: {...headers},
  });
};
//Lấy thông tin chi tiết đơn hàng
API_Orders.GetOrderDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.get('/api/Orders/GetOrderDetailsById/' + data, {
    headers: {...headers},
  });
};
//Lấy danh sách chi tiết đơn hàng
API_Orders.GetListOrderDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.get(
    '/api/Orders/GetListOrderDetailsById/' + data,
    {
      headers: {...headers},
    },
  );
};
//Lấy danh sách file đính kèm
API_Orders.GetAttList = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post('/api/Attachments/GetAttList', data, {
    headers: {...headers},
  });
};
//Tài file
API_Orders.Download = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post('/api/Attachments/Download', data, {
    headers: {...headers},
  });
};
//Thêm mới đính kèm
API_Orders.InsertAttach = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post('/api/Attachments/Insert', data, {
    headers: {...headers},
  });
};
//Xóa file
API_Orders.DeleteAttach = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post('/api/Attachments/Delete', data, {
    headers: {...headers},
  });
};
//Lấy thông tin giao hàng
API_Orders.GetOrderDeliveryDates = Id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.get(
    '/api/Orders/GetOrderDeliveryDates/' + Id,
    {},
    {
      headers: {...headers},
    },
  );
};
//Jtable quotations
API_Orders.QuotationsAM_ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Orders.instance.post(
    '/api/Quotations/QuotationsAM_ListAll',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//Xem chi tiết báo giá
API_Orders.Quotations_Items = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Orders.instance.post(
    `/api/Quotations/Quotations_Items`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//checklogin
API_Orders.CheckLoginQuotation = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Orders.instance.post(
    `/api/Quotations/CheckLogin`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//duyệt báo giá
API_Orders.Approve_Quotations = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Orders.instance.post(
    'api/Quotations/Approve_Quotations',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//trả lại báo giá
API_Orders.NotApprove_Quotations = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  console.log(AccessTokenManager.accessToken);
  return API_Orders.instance.post(
    'api/Quotations/NotApprove_Quotations',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Lấy thông tin chi tiết báo giá
API_Orders.GetQuotationDetailsById = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.get(
    '/api/Quotations/GetQuotationDetailsById/' + data,
    {
      headers: {...headers},
    },
  );
};
//xóa báo giá
API_Orders.Quotations_Delete = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    '/api/Quotations/IsDeleted',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
//Danh sách chi tiết báo giá
API_Orders.QuotationDetails_ListDetail = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    '/api/Quotations/QuotationDetails_ListDetail',
    JSON.stringify(data),
    {
      headers: {...headers},
    },
  );
};
//check login
API_Orders.CheckLogin_Quotations = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Quotations/CheckLogin',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.GetHomeReport = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.get('/api/ChartDashboard_SM/GetHomeReport', {
    headers: {...headers},
  });
};
API_Orders.GetHomeReportOrders = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.get('/api/ChartDashboard_SM/GetHomeReportOrders', {
    headers: {...headers},
  });
};
API_Orders.GetHomeReportQuotations = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.get(
    '/api/ChartDashboard_SM/GetHomeReportQuotations',
    {
      headers: {...headers},
    },
  );
};

API_Orders.getIsApproval = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/IsApproval',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.Approval = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post('api/Orders/Approval', JSON.stringify(obj), {
    headers: {...headers},
  });
};
API_Orders.GetCustomersAll = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetCustomersAll',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.GetQuotationsAll = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetQuotationsAll',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.GetListPOVoucherDetails = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetListPOVoucherDetails',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_Orders.GetQuotationsByQuotationGuid = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetQuotationsByQuotationGuid',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_Orders.GetCustomerContacts = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetCustomerContacts',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.GetItemsAllJExcel = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetItemsAllJExcel',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_Orders.GetItemsByItemId = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetItemsByItemId',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_Orders.GetInventoryByItemId = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetInventoryByItemId',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.GetUnitByItemIdJExcel = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetUnitByItemIdJExcel',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.GetUnitsByUnitId = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetUnitsByUnitId',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.Insert = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post('/api/Orders/Insert', data, {
    headers: {...headers},
  });
};

API_Orders.GetObjectProvidersAll = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetObjectProvidersAll',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};

API_Orders.GetWarehousesAllJExcel = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetWarehousesAllJExcel',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.UpdateStatus = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    `api/Orders/UpdateStatus/${id}`,
    JSON.stringify({}),
    {
      headers: {...headers},
    },
  );
};
API_Orders.UpdateStatusQuotation = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    `api/Quotations/UpdateStatus`,
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
API_Orders.IsDeleted = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post('api/Orders/IsDeleted', JSON.stringify(obj), {
    headers: {...headers},
  });
};

API_Orders.GetOrderAndQuotations = obj => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Orders.instance.post(
    'api/Orders/GetOrderAndQuotations',
    JSON.stringify(obj),
    {
      headers: {...headers},
    },
  );
};
export default API_Orders;
