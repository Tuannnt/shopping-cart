import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_Quotations = { instance: getInstance() };
API_Quotations.getAllObjectKH = (txt) => {
    var obj = { txtSearch: txt }
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.post('/api/Quotations/getAllObjectKH', JSON.stringify(obj), {
        headers: { ...headers },
    });
};

API_Quotations.getKHbyCustomerId = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_Quotations.instance.post('/api/Quotations/getKHbyCustomerId', JSON.stringify(data), {
        headers: { ...headers },
    });
};

API_Quotations.getPeople_Quotation = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.get('/api/Quotations/getPeople_Quotation', {
        headers: { ...headers },
    });
};

API_Quotations.GetAllCurrency = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.get('/api/Quotations/GetAllCurrency', {
        headers: { ...headers },
    });
};

API_Quotations.GetRateByCurrencies = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.get(`/api/Quotations/GetRateByCurrencies/${data}`, {
        headers: { ...headers },
    });
};
API_Quotations.GetItems = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_Quotations.instance.post('/api/Quotations/GetItems', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_Quotations.GetCountries = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.get('/api/Quotations/GetCountries', {
        headers: { ...headers },
    });
};
API_Quotations.GetUnitsAllJExcel = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.get('/api/Quotations/GetUnitsAllJExcel', {
        headers: { ...headers },
    });
};
API_Quotations.ThueGTGT = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.get('/api/Quotations/ThueGTGT', {
        headers: { ...headers },
    });
};

API_Quotations.GetItemsByItemId = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_Quotations.instance.post('/api/Quotations/GetItemsByItemId', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_Quotations.GetAllTerms = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.get('/api/Quotations/GetAllTerms', {
        headers: { ...headers },
    });
};

API_Quotations.Insert = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data;',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.post('/api/Quotations/Insert', data, {
        headers: { ...headers },
    });
};

API_Quotations.GetUnitPriceOfProduct = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Quotations.instance.get('/api/Quotations/GetUnitPriceOfProduct', {
        headers: { ...headers },
    });
};
export default API_Quotations;

