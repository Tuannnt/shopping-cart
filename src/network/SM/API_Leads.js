import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import AccessTokenManager from "../../bussiness/AccessTokenManager";
const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_Leads = { instance: getInstance() };
API_Leads.GetSteps = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Leads.instance.post('/api/Leads/GetStepOfCompaignsByCompaignGuid', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Chi tiết hàng hóa vật tư
API_Leads.GetCompaignsAll = (CompaignPlanGuid) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Leads.instance.get('/api/Leads/GetCompaignsAll/' + (CompaignPlanGuid ? CompaignPlanGuid : ''), {}, {
        headers: { ...headers },
    });
};
API_Leads.GetLeads = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Leads.instance.post('/api/Leads/GetLeads', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_Leads.GetItems = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Leads.instance.get('/api/Leads/GetItems/' + id, {}, {
        headers: { ...headers },
    });
};
API_Leads.GetContacts = (CustomerGuid) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Leads.instance.get('/api/Leads/GetContacts/' + CustomerGuid, {}, {
        headers: { ...headers },
    });
};
API_Leads.UpdateStatus = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Leads.instance.post('/api/Leads/UpdateStatus', JSON.stringify(data), {
        headers: { ...headers },
    });
};
API_Leads.Delete = (Id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_Leads.instance.get('/api/Leads/Delete/' + Id, {}, {
        headers: { ...headers },
    });
};
export default API_Leads;
