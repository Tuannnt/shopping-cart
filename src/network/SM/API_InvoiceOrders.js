import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import API_TM_TASKS from "../TM/TASKS/API_TM_TASKS";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_InvoiceOrders = {instance: getInstance()};
//Danh sách khách hàng
API_InvoiceOrders.GetCustomerAll = () => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.get('/api/InvoiceOrders/GetCustomers',null, {
        headers: {...headers},
    });
}; 
//Danh sách chi tiết đơn bán hàng
API_InvoiceOrders.GetAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.post('/api/InvoiceOrders/ListAll',JSON.stringify(data), {
        headers: {...headers},
    });
};
//Lấy thông tin đơn hàng
API_InvoiceOrders.GetById = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.get('/api/InvoiceOrders/GetById/'+data, {
        headers: {...headers},
    })
};
//Lấy thông tin chi tiết
API_InvoiceOrders.GetDetails = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.get('/api/InvoiceOrders/GetDetails/'+data, {
        headers: {...headers},
    })
};
//Lấy danh sách chi tiết đơn hàng
API_InvoiceOrders.GetListOrderDetailsById = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.get('/api/Orders/GetListOrderDetailsById/'+data, {
        headers: {...headers},
    })
};
//Lấy danh sách file đính kèm
API_InvoiceOrders.GetAttList = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.post('/api/Attachments/GetAttList',data, {
        headers: {...headers},
    });
};
//Tài file
API_InvoiceOrders.Download = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.post('/api/Attachments/Download', data, {
        headers: {...headers},
    });
};
//Thêm mới đính kèm
API_InvoiceOrders.Confirm = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.post('/api/InvoiceOrders/Confirm',data, {
        headers: {...headers},
    });
};
//Xóa file
API_InvoiceOrders.DeleteAttach = (data) => {
   const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.post('/api/Attachments/Delete', data, {
        headers: {...headers},
    });
};
//Lấy thông tin giao hàng
API_InvoiceOrders.GetOrderDeliveryDates = (Id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_InvoiceOrders.instance.get('/api/Orders/GetOrderDeliveryDates/' +Id,{}, {
        headers: {...headers},
    });
};
//Jtable quotations
API_InvoiceOrders.QuotationsAM_ListAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_InvoiceOrders.instance.post('/api/Quotations/QuotationsAM_ListAll', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Xem chi tiết báo giá
API_InvoiceOrders.Quotations_Items = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_InvoiceOrders.instance.post(`/api/Quotations/Quotations_Items`, JSON.stringify(obj), {
        headers: {...headers},
    });
};
//duyệt báo giá
API_InvoiceOrders.Approve_Quotations = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_InvoiceOrders.instance.post('api/Quotations/Approve_Quotations', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
//trả lại báo giá
API_InvoiceOrders.NotApprove_Quotations = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_InvoiceOrders.instance.post('api/Quotations/NotApprove_Quotations', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
//Lấy thông tin chi tiết báo giá
API_InvoiceOrders.GetQuotationDetailsById = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.get('/api/Quotations/GetQuotationDetailsById/'+data, {
        headers: {...headers},
    })
};
//xóa báo giá
API_InvoiceOrders.Quotations_Delete = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.post('/api/Quotations/IsDeleted', JSON.stringify(obj), {
        headers: {...headers},
    })
};
//Danh sách chi tiết báo giá
API_InvoiceOrders.QuotationDetails_ListDetail = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.post('/api/Quotations/QuotationDetails_ListDetail',JSON.stringify(data), {
        headers: {...headers},
    });
};
//check login
API_InvoiceOrders.CheckLogin_Quotations = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_InvoiceOrders.instance.post('api/Quotations/CheckLogin', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_InvoiceOrders.GetHomeReport = () => {
    const headers = {
        'Content-Type': 'application/json',
      Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_InvoiceOrders.instance.get('/api/ChartDashboard_SM/GetHomeReport', {
      headers: { ...headers },
    });
  }
  API_InvoiceOrders.GetHomeReportOrders = () => {
    const headers = {
        'Content-Type': 'application/json',
      Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_InvoiceOrders.instance.get('/api/ChartDashboard_SM/GetHomeReportOrders', {
      headers: { ...headers },
    });
  }
  API_InvoiceOrders.GetHomeReportQuotations = () => {
    const headers = {
        'Content-Type': 'application/json',
      Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_InvoiceOrders.instance.get('/api/ChartDashboard_SM/GetHomeReportQuotations', {
      headers: { ...headers },
    });
  }
export default API_InvoiceOrders;
