import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from "../../configApp";
import API_TM_TASKS from "../TM/TASKS/API_TM_TASKS";
import AccessTokenManager from "../../bussiness/AccessTokenManager";

const getInstance = () => {
    const instance = axios.create({
        baseURL: configApp.url_api_mb,
        timeout: 30000,
    });

    instance.interceptors.response.use(
        UnauthorizeInterceptor.onFullfilled,
        UnauthorizeInterceptor.onRejected,
    );

    instance.interceptors.request.use(
        LogInterceptor.requestLog,
        LogInterceptor.requestError,
    );

    instance.interceptors.response.use(
        LogInterceptor.responseLog,
        LogInterceptor.responseError,
    );

    instance.interceptors.request.use(
        AccessTokenInterceptor.addAccessToken,
        AccessTokenInterceptor.onRejected
    );
    return instance;
}
const API_OrderDetailsSX = {instance: getInstance()};
//Danh sách khách hàng
API_OrderDetailsSX.GetCustomerAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('/api/OrderDetailsSX/GetObjectsAll',JSON.stringify(data), {
        headers: {...headers},
    });
};
//Danh sách chi tiết đơn bán hàng
API_OrderDetailsSX.GetAll = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('/api/OrderDetailsSX/ListAll',JSON.stringify(data), {
        headers: {...headers},
    });
};
//Lấy thông tin đơn hàng
API_OrderDetailsSX.GetOrderById = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.get('/api/Orders/GetOrderById/'+data, {
        headers: {...headers},
    })
};
//Lấy thông tin chi tiết
API_OrderDetailsSX.getItem = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.get('/api/OrderDetailsSX/GetItem/'+data, {
        headers: {...headers},
    })
};
//Lấy danh sách chi tiết đơn hàng
API_OrderDetailsSX.GetListOrderDetailsById = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.get('/api/Orders/GetListOrderDetailsById/'+data, {
        headers: {...headers},
    })
};
//Lấy danh sách file đính kèm
API_OrderDetailsSX.GetAttList = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('/api/Attachments/GetAttList',data, {
        headers: {...headers},
    });
};
//Tài file
API_OrderDetailsSX.Download = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('/api/Attachments/Download', data, {
        headers: {...headers},
    });
};
//Thêm mới đính kèm
API_OrderDetailsSX.Confirm = (data) => {
    const headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('/api/OrderDetailsSX/Confirm',data, {
        headers: {...headers},
    });
};
//Xóa file
API_OrderDetailsSX.DeleteAttach = (data) => {
   const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('/api/Attachments/Delete', data, {
        headers: {...headers},
    });
};
//Lấy thông tin giao hàng
API_OrderDetailsSX.GetOrderDeliveryDates = (Id) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_OrderDetailsSX.instance.get('/api/Orders/GetOrderDeliveryDates/' +Id,{}, {
        headers: {...headers},
    });
};
//Jtable quotations
API_OrderDetailsSX.CheckBox = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_OrderDetailsSX.instance.post('/api/OrderDetailsSX/CheckBox', JSON.stringify(data), {
        headers: { ...headers },
    });
};
//Xem chi tiết báo giá
API_OrderDetailsSX.Quotations_Items = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_OrderDetailsSX.instance.post(`/api/Quotations/Quotations_Items`, JSON.stringify(obj), {
        headers: {...headers},
    });
};
//duyệt báo giá
API_OrderDetailsSX.Approve_Quotations = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_OrderDetailsSX.instance.post('api/Quotations/Approve_Quotations', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
//trả lại báo giá
API_OrderDetailsSX.NotApprove_Quotations = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    console.log(AccessTokenManager.accessToken)
    return API_OrderDetailsSX.instance.post('api/Quotations/NotApprove_Quotations', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
//Lấy thông tin chi tiết báo giá
API_OrderDetailsSX.GetQuotationDetailsById = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.get('/api/Quotations/GetQuotationDetailsById/'+data, {
        headers: {...headers},
    })
};
//xóa báo giá
API_OrderDetailsSX.Quotations_Delete = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('/api/Quotations/IsDeleted', JSON.stringify(obj), {
        headers: {...headers},
    })
};
//Danh sách chi tiết báo giá
API_OrderDetailsSX.QuotationDetails_ListDetail = (data) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('/api/Quotations/QuotationDetails_ListDetail',JSON.stringify(data), {
        headers: {...headers},
    });
};
//check login
API_OrderDetailsSX.CheckLogin_Quotations = (obj) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AccessTokenManager.accessToken
    };
    return API_OrderDetailsSX.instance.post('api/Quotations/CheckLogin', JSON.stringify(obj), {
        headers: { ...headers },
    });
};
API_OrderDetailsSX.GetHomeReport = () => {
    const headers = {
        'Content-Type': 'application/json',
      Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_OrderDetailsSX.instance.get('/api/ChartDashboard_SM/GetHomeReport', {
      headers: { ...headers },
    });
  }
  API_OrderDetailsSX.GetHomeReportOrders = () => {
    const headers = {
        'Content-Type': 'application/json',
      Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_OrderDetailsSX.instance.get('/api/ChartDashboard_SM/GetHomeReportOrders', {
      headers: { ...headers },
    });
  }
  API_OrderDetailsSX.GetHomeReportQuotations = () => {
    const headers = {
        'Content-Type': 'application/json',
      Authorization: 'Bearer ' + AccessTokenManager.accessToken,
    };
    return API_OrderDetailsSX.instance.get('/api/ChartDashboard_SM/GetHomeReportQuotations', {
      headers: { ...headers },
    });
  }
export default API_OrderDetailsSX;
