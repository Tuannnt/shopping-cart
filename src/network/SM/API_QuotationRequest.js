import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';
const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_QuotationRequest = { instance: getInstance() };
//Danh sách hàng hóa vật tư
API_QuotationRequest.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Chi tiết hàng hóa vật tư
API_QuotationRequest.GetItem = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.get(
    '/api/QuotationRequest/GetItem/' + id,
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.GetItemDetail = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.get(
    '/api/QuotationRequest/GetItemDetail/' + id,
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.Delete = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/IsDeleted/' + id,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.getKHbyCustomerId = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/getKHbyCustomerId',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.GetEmployeeOfGroups = CustomerGuid => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/GetEmployeeOfGroups/' + CustomerGuid,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.GetSocialNetworks = CustomerGuid => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/GetSocialNetworks/' + CustomerGuid,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.GetCustomerContacts = CustomerGuid => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/GetCustomerContacts/' + CustomerGuid,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.GetCalendars = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/GetCalendars',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.getAllProduct = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/GetItems',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.GetItemsByItemId = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/GetItemsByItemId',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.Jtable = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    '/api/QuotationRequest/Jtable',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
// Lấy số phiếu
API_QuotationRequest.getPeople_Quotation = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.get(
    '/api/QuotationRequest/getPeople_Quotation',
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.GetUnitsAllJExcel = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.get(
    '/api/QuotationRequest/GetUnitsAllJExcel',
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.Insert = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    'api/QuotationRequest/Insert',
    data,
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.Update = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.post(
    'api/QuotationRequest/Update',
    data,
    {
      headers: { ...headers },
    },
  );
};

API_QuotationRequest.GetAllStatusQuotationRequest = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.get(
    '/api/QuotationRequest/GetAllStatusQuotationRequest',
    {
      headers: { ...headers },
    },
  );
};
API_QuotationRequest.getEmployees = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_QuotationRequest.instance.get(
    '/api/QuotationRequest/getEmployees',
    {
      headers: { ...headers },
    },
  );
};
export default API_QuotationRequest;
