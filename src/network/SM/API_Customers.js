import axios from 'axios';
import * as UnauthorizeInterceptor from '../interceptors/unauthorize';
import * as LogInterceptor from '../interceptors/log';
import * as AccessTokenInterceptor from '../interceptors/accessToken';
import configApp from '../../configApp';
import AccessTokenManager from '../../bussiness/AccessTokenManager';
const getInstance = () => {
  const instance = axios.create({
    baseURL: configApp.url_api_mb,
    timeout: 30000,
  });

  instance.interceptors.response.use(
    UnauthorizeInterceptor.onFullfilled,
    UnauthorizeInterceptor.onRejected,
  );

  instance.interceptors.request.use(
    LogInterceptor.requestLog,
    LogInterceptor.requestError,
  );

  instance.interceptors.response.use(
    LogInterceptor.responseLog,
    LogInterceptor.responseError,
  );

  instance.interceptors.request.use(
    AccessTokenInterceptor.addAccessToken,
    AccessTokenInterceptor.onRejected,
  );
  return instance;
};
const API_Customers = { instance: getInstance() };
//Danh sách hàng hóa vật tư
API_Customers.ListAll = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post(
    '/api/Customers/ListAll',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
//Chi tiết hàng hóa vật tư
API_Customers.GetItem = id => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post(
    '/api/Customers/GetItems/' + id,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_Customers.DeletedKH = (data) => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post('/api/Customers/DeletedKH', JSON.stringify(data), {
    headers: { ...headers },
  });
};
API_Customers.GetEmployeeOfGroups = CustomerGuid => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post(
    '/api/Customers/GetEmployeeOfGroups/' + CustomerGuid,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_Customers.GetSocialNetworks = CustomerGuid => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post(
    '/api/Customers/GetSocialNetworks/' + CustomerGuid,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_Customers.GetCustomerContacts = CustomerGuid => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post(
    '/api/Customers/GetCustomerContacts/' + CustomerGuid,
    {},
    {
      headers: { ...headers },
    },
  );
};
API_Customers.GetCalendars = data => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post(
    '/api/Customers/GetCalendars',
    JSON.stringify(data),
    {
      headers: { ...headers },
    },
  );
};
// Lấy số phiếu
API_Customers.GetTreeGroups = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.get('/api/Customers/GetTreeGroups', {
    headers: { ...headers },
  });
};

API_Customers.Insert = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post('api/Customers/Insert', data, {
    headers: { ...headers },
  });
};
API_Customers.Update = data => {
  const headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.post('api/Customers/Update', data, {
    headers: { ...headers },
  });
};

API_Customers.GetAllStatusCustomers = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.get('/api/Customers/GetAllStatusCustomers', {
    headers: { ...headers },
  });
};
API_Customers.GetAllCustomerResources = () => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + AccessTokenManager.accessToken,
  };
  return API_Customers.instance.get('/api/Customers/GetAllCustomerResources', { headers: { ...headers }, },);
};
export default API_Customers;
